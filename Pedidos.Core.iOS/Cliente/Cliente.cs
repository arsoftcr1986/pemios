﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLiteBase;
using System.Data;
using BI.Shared;
//using Softland.ERP.FR.Mobile.Cls.Corporativo;
//using Softland.ERP.FR.Mobile.Cls.Documentos;
//using Softland.ERP.FR.Mobile.Cls.Utilidad;
//using Softland.ERP.FR.Mobile.Cls.FRCliente.FRVisita;
//using Softland.ERP.FR.Mobile.Cls.Cobro;

namespace Pedidos.Core
{
    /// <summary>
    /// Representa un cliente con todas sus companias
    /// </summary>
    public class Cliente : ClienteBase
    {
        
        #region Variables y Propiedades de instancia

        public double latitud = 0;
        /// <summary>
        /// Variable que almacena la latitud geográfica del cliente en el mapa. 
        /// </summary>
        public double Latitud
        {
            get { return latitud; }
            set { latitud = value; }
        }

        public double longitud = 0;
        /// <summary>
        /// Variable que almacena la longitud geográfica del cliente en el mapa. 
        /// </summary>
        public double Longitud
        {
            get { return longitud; }
            set { longitud = value; }
        }

        private List<ClienteCia> clienteCompania = new List<ClienteCia>();
        /// <summary>
        /// Lista de sus caracteristicas especificas en cada compania
        /// </summary>
        public List<ClienteCia> ClienteCompania
        {
            get { return clienteCompania; }
            set { clienteCompania = value; }
        }        
        
        /// <summary>
        /// Determina si el cliente tiene niveles de precio definido
        /// </summary>
        public bool NivelesPreciosDefinidos
        {
            get
            {
                bool hayNivelesDefinidos = false;
                foreach (ClienteCia clt in clienteCompania)
                {
                    clt.CargarNivelPrecio();
                    if (clt.NivelPrecio.Nivel != string.Empty && !clt.NivelPrecio.Codigo.Equals("99") )
                        hayNivelesDefinidos = true;
                }
                return hayNivelesDefinidos;
            }
        }

        /// <summary>
        /// Verifica si cliente tiene limite de credito excedido.
        /// </summary>
        /// <returns></returns>
        
        
        // Se agrega el parametro con el monto de la factura en proceso para que haga la validacion
        public bool LimiteCreditoExcedido(string cia,decimal montoFactActual)
        {
            //Realizar funcion para ver si ya exedio el limite de credito
            //return Factura.ValidaLimiteCredito(this.Codigo, cia, montoFactActual);
            return false;
        }
        //public bool LimiteCreditoExcedido(string cia)
        //{
        //    return Factura.ValidaLimiteCredito(this.Codigo, cia);
        //}       

        
        #region Montos del cliente

        private ClienteMontos montos;
        /// <summary>
        /// Montos de los reportes
        /// </summary>
        public ClienteMontos Montos
        {
            get { return montos; }
            set { montos = value; }
        }

        #endregion

        #endregion

        #region Acceso Datos

        /// <summary>
        /// Cargar los datos del cliente
        /// </summary>
        public void Cargar() 
        {
            string sentencia =
                " SELECT NOM_CLT,DIR_CLT FROM " + Table.ERPADMIN_CLIENTE +
                " WHERE COD_CLT = @CLIENTE";

            SQLiteDataReader reader = null;
            SQLiteParameterList parametros = new SQLiteParameterList();
            parametros.Add("@CLIENTE", Codigo);

            try
            {
                reader = GestorDatos.EjecutarConsulta(sentencia,parametros);

                if (reader.Read())
                {
                    Nombre = reader.GetString(0);
                    Direccion = reader.GetString(1);
                }
                else
                    throw new Exception("No se encontró información del cliente.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }        
        }

        /// <summary>
        /// Cargar los datos del cliente
        /// </summary>
        public void Cargar(SQLiteConnection cnx)
        {
            string sentencia =
                " SELECT NOM_CLT,DIR_CLT FROM " + Table.ERPADMIN_CLIENTE +
                " WHERE COD_CLT = @CLIENTE";

            SQLiteDataReader reader = null;
            SQLiteParameterList parametros = new SQLiteParameterList();
            parametros.Add("@CLIENTE", Codigo);

            try
            {
                reader = GestorDatos.EjecutarConsulta(sentencia, parametros,cnx);

                if (reader.Read())
                {
                    Nombre = reader.GetString(0);
                    Direccion = reader.GetString(1);
                }
                else
                    throw new Exception("No se encontró información del cliente.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        /// <summary>
        /// Cargar los datos de geolocalización del cliente
        /// </summary>
        public void CargarGeolocalizacion()
        {
            string sentencia =
                " SELECT LATITUD,LONGITUD FROM " + Table.ERPADMIN_CLIENTE_GEOLOCALIZACION +
                " WHERE COD_CLT = @CLIENTE " +
                " AND COD_CIA = @COMPANIA ";

            SQLiteDataReader reader = null;
            SQLiteParameterList parametros = new SQLiteParameterList();
            parametros.Add("@CLIENTE", Codigo);
            parametros.Add("@COMPANIA", Compania);

            try
            {
                reader = GestorDatos.EjecutarConsulta(sentencia, parametros);

                if (reader.Read())
                {
                    Latitud = Convert.ToDouble(reader.GetDecimal(0));
                    Longitud = Convert.ToDouble(reader.GetDecimal(1));
                }
                else { }
                    //throw new Exception("No se encontró información de geolocalización del cliente.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        /// <summary>
        /// Cargar los datos de geolocalización del cliente
        /// </summary>
        public static List<ClienteGeoLocaizacion> CargarListaGeolocalizaciones(SQLiteConnection cnxSync = null)
        {
            string sentencia =
                " SELECT COD_CLT,COD_CIA,LATITUD,LONGITUD FROM " + Table.ERPADMIN_CLIENTE_GEOLOCALIZACION +
                " WHERE SINCRONIZADO = 0 ";

            SQLiteDataReader reader = null;
            List<ClienteGeoLocaizacion> listado = new List<ClienteGeoLocaizacion>();

            try
            {
                if (cnxSync != null)
                {
                    reader = GestorDatos.EjecutarConsulta(sentencia, cnxSync);
                }
                else
                {
                    reader = GestorDatos.EjecutarConsulta(sentencia);
                }

                
                if (reader.Read())
                {
                    var cliente = new ClienteGeoLocaizacion();
                    cliente.Cliente = reader.GetString(0);
                    cliente.Compania = reader.GetString(1);
                    cliente.Latitud = reader.GetDecimal(2);
                    cliente.Longitud = reader.GetDecimal(3);

                    listado.Add(cliente);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return listado;
        }

        /// <summary>
        /// Guardar los datos de geolocalización del cliente
        /// </summary>
        public void GuardarGeolocalizacion(double Lat, double Long)
        {
            //Se procede a guardar la localización.

            string sentencia = "";

            // Si el cliente aún no tiene almacenada una geolocalización se hace un insert, de lo contrario un update.
            if (Latitud == 0 && Longitud == 0)
            {
                sentencia =
                    " INSERT INTO " + Table.ERPADMIN_CLIENTE_GEOLOCALIZACION +
                    " ( COD_CIA,  COD_CLT, LATITUD, LONGITUD, SINCRONIZADO ) " +
                    " VALUES ( @COMPANIA, @CLIENTE, @LATITUD, @LONGITUD, @SINCRONIZADO) ";
            }
            else
            {
                sentencia =
                    " UPDATE " + Table.ERPADMIN_CLIENTE_GEOLOCALIZACION +
                    " SET LATITUD = @LATITUD, LONGITUD = @LONGITUD, SINCRONIZADO = @SINCRONIZADO" +
                    " WHERE COD_CIA = @COMPANIA " +
                    " AND   COD_CLT = @CLIENTE ";
            }

            Latitud = Lat;
            Longitud = Long;

            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                GestorDatos.SQLiteParameter("@CLIENTE",SqlDbType.NVarChar, Codigo),
                GestorDatos.SQLiteParameter("@COMPANIA",SqlDbType.NVarChar, Compania),
                GestorDatos.SQLiteParameter("@LATITUD",SqlDbType.NVarChar, Latitud),
                GestorDatos.SQLiteParameter("@LONGITUD",SqlDbType.NVarChar, Longitud),
                GestorDatos.SQLiteParameter("@SINCRONIZADO",SqlDbType.Int, 0)
            });

            int resultado = GestorDatos.EjecutarComando(sentencia, parametros);

            if (resultado != 1)
            {
                Latitud = 0;
                Longitud = 0;

                throw new Exception("No se puede guardar la localización del cliente.");
            }
        }

        /// <summary>
        /// Actualiza los clientes para saber si se sincronizó la geolocalizacion.
        /// </summary>
        public static void ActualizarGeolocalizaionesSinc(List<ClienteGeoLocaizacion> clientesPorSinc, SQLiteConnection cnxSync = null)
        {
            //Se procede a guardar la localización.
            string sentencia =
                " UPDATE " + Table.ERPADMIN_CLIENTE_GEOLOCALIZACION +
                " SET SINCRONIZADO = @SINCRONIZADO" +
                " WHERE COD_CIA = @COMPANIA " +
                " AND   COD_CLT = @CLIENTE ";
            foreach (ClienteGeoLocaizacion clienteGeo in clientesPorSinc)
            {
                // Si el cliente aún no tiene almacenada una geolocalización se hace un insert, de lo contrario un update.
                SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                GestorDatos.SQLiteParameter("@CLIENTE",SqlDbType.NVarChar, clienteGeo.Cliente),
                GestorDatos.SQLiteParameter("@COMPANIA",SqlDbType.NVarChar, clienteGeo.Compania),
                GestorDatos.SQLiteParameter("@SINCRONIZADO",SqlDbType.Int, 1)
                });

                int resultado = 0;
                if (cnxSync != null)
                {
                    resultado = GestorDatos.EjecutarComando(sentencia, parametros, cnxSync);
                }
                else
                {
                    resultado = GestorDatos.EjecutarComando(sentencia, parametros);
                }
            }
        }

        /// <summary>
        /// Obtener la lista de los clientes que han sido visitados en ruta
        /// </summary>
        /// <returns>lista de clientes visitados</returns>
        public static List<Cliente> ObtenerClientesVisitados()
        {
            SQLiteDataReader reader = null;
            List<Cliente> clientes = new List<Cliente>();
            clientes.Clear();
            try
            {
                string sentencia =
                    " SELECT DISTINCT C.COD_CLT, C.NOM_CLT " +
                    " FROM " + Table.ERPADMIN_VISITA + " V, " + Table.ERPADMIN_CLIENTE + " C " +
                    " WHERE C.COD_CLT = V.CLIENTE " +
                    " AND julianday(date(INICIO)) = julianday(date('now','localtime'))";

                SQLiteParameterList parametros = new  SQLiteParameterList();
                parametros.Add("@HOY", DateTime.Now.ToString("yyyyMMdd"));

                reader = GestorDatos.EjecutarConsulta(sentencia, parametros);

                while (reader.Read())
                {
                    Cliente cliente = new Cliente();
                    cliente.Codigo = reader.GetString(0);
                    cliente.Nombre = reader.GetString(1);
                    clientes.Add(cliente);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error cargando los clientes visitados." + ex.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            return clientes;
        }

        /// <summary>
        /// Obtener lista de clientes con los reportes de montos cargados
        /// </summary>
        /// <returns>lista de clientes</returns>
        public static List<Cliente> CargaReporteMontos()
        {
            List<Cliente> clientes = ObtenerClientesVisitados();
            foreach (Cliente cliente in clientes)
            {
                cliente.montos.CargarMontoCobrado(cliente.Codigo);
                cliente.montos.CargarMontoVendido(cliente.Codigo);
            }
            return clientes;
        }
       
        /// <summary>
        /// ABC Caso 35771
        /// Carga los clientes visitados con sus respectivos montos
        /// de los de productos devueltos.
        /// </summary>
        /// <returns>ArrayList con objetos Cliente</returns>
        public static List<Cliente> CargaReporteMontosDevueltos()
        {
            List<Cliente> clientes = ObtenerClientesVisitados();
            foreach (Cliente cliente in clientes)
            {
                try
                {
                    cliente.montos.CargarMontoDevuelto(cliente);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error cargando el monto devuelto por el cliente '" + cliente.Codigo + "'. " + ex.Message);
                }

            }

            return clientes;
        }

        #endregion

        #region Busquedas
        
        /// <summary>
        /// Cargar los clientes en compania
        /// </summary>
        public void ObtenerClientesCia()
        {
            if (this.clienteCompania.Count == 0)
                clienteCompania = ClienteCia.ObtenerClientes(this.Codigo, false, Compania);
        }

        /// <summary>
        /// Cargar los clientes en compania
        /// </summary>
        public void ObtenerClientesCia(SQLiteConnection cnx)
        {
            if (this.clienteCompania.Count == 0)
                clienteCompania = ClienteCia.ObtenerClientes(this.Codigo, false, Compania,cnx);
        }

        /// <summary>
        /// Determina si un cliente utiliza consignacion
        /// </summary>
        /// <returns></returns>
        public bool UsaConsignacion()
        { 
            ObtenerClientesCia();
            foreach (ClienteCia clt in this.clienteCompania)
            {
                if (clt.Bodega != FRdConfig.NoDefinido)
                    return true;
            }
            return false;
        }
        
        /// <summary>
        /// obtener un cliente para una compania especifica
        /// </summary>
        /// <param name="cia">compania</param>
        /// <returns>cliente en compania</returns>
        public ClienteCia ObtenerClienteCia(string cia)
        {
            foreach (ClienteCia clt in this.clienteCompania)
            {
                if (clt.Compania.ToUpper().Equals(cia.ToUpper()))
                {
                    clt.CargarNivelPrecio();
                    //Si el cliente es dolar se cambia la variable global a true para que cambien los signos
                    GestorUtilitario.esDolar = clt.Moneda == TipoMoneda.DOLAR;
                    return clt;
                }
            }
            return null;
        }

        /// <summary>
        /// obtener un cliente para una compania especifica
        /// </summary>
        /// <param name="cia">compania</param>
        /// <returns>cliente en compania</returns>
        public ClienteCia ObtenerClienteCia(string cia, SQLiteConnection cnx)
        {
            foreach (ClienteCia clt in this.clienteCompania)
            {
                if (clt.Compania.ToUpper().Equals(cia.ToUpper()))
                {
                    clt.CargarNivelPrecio(cnx);
                    //Si el cliente es dolar se cambia la variable global a true para que cambien los signos
                    GestorUtilitario.esDolar = clt.Moneda == TipoMoneda.DOLAR;
                    return clt;
                }
            }
            return null;
        }
        
        /// <summary>
        /// Cargar las direcciones de entrega de un cliente en una compania
        /// </summary>
        /// <param name="cia">compania del cliente a cargar</param>
        public void ObtenerDireccionesEntrega(string cia)
        {
            foreach (ClienteCia clt in this.clienteCompania)
            {
                if (clt.Compania.Equals(cia))
                {
                    clt.ObtenerDireccionesEntrega();
                }
            }
        }

        /// <summary>
        /// Obtiene los dias que el cliente debe ser visitado
        /// </summary>
        /// <returns>Una cadena de letras que representan un dia de la semana separadas por coma</returns>
        public List<Dias> ObtenerDiasVisita(string zona)
        {
            return Ruta.DiasVisita(this.Codigo, zona);
        }
        
        /// <summary>
        /// Obtener una lista de clientes por un criterio de busqueda
        /// </summary>
        /// <param name="c">criterio a buscar</param>
        /// <returns>lista de clientes</returns>
        public static List<Cliente> CargarClientes(CriterioBusquedaCliente c,string pCompania)
        {
            List<Cliente> clientes = new List<Cliente>();
            clientes.Clear();
            SQLiteDataReader reader = null;

            string sentencia = string.Empty;
            SQLiteParameterList parametros = new SQLiteParameterList();

            if (!string.IsNullOrEmpty(pCompania))
            {
                sentencia = CrearSentenciaBusquedaCliente(c);
                parametros = CrearParametrosBusquedaCliente(c, pCompania);
            }
            else
            {
                sentencia = CrearSentenciaBusquedaCliente();
            }

            try
            {
                reader = GestorDatos.EjecutarConsulta(sentencia,parametros);
                while (reader.Read())
                {
                    if (clientes.Count > 0)
                    {
                        string cod=reader.GetString(0);
                        if (!clientes.Exists(x => x.Codigo == cod))
                        {
                            Cliente cliente = new Cliente();
                            cliente.Codigo = reader.GetString(0);
                            cliente.Nombre = reader.GetString(1);
                            cliente.Direccion = reader.GetString(2);
                            cliente.Zona = reader.GetString(3);
                            cliente.Compania = reader.GetString(5);
                            clientes.Add(cliente);
                        }
                    }
                    else
                    {
                        Cliente cliente = new Cliente();
                        cliente.Codigo = reader.GetString(0);
                        cliente.Nombre = reader.GetString(1);
                        cliente.Direccion = reader.GetString(2);
                        cliente.Zona = reader.GetString(3);
                        cliente.Compania = reader.GetString(5);
                        clientes.Add(cliente);
                    }                    
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error cargando los clientes. " + ex.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            return clientes; 
        }
        
        /// <summary>
        /// Crear los parametros de busqueeda para el cliente segun los criterios
        /// </summary>
        /// <param name="c">criterios de busqueda</param>
        /// <returns>parametros</returns>
        private static SQLiteParameterList CrearParametrosBusquedaCliente(CriterioBusquedaCliente c,string pCompania)
        {
            SQLiteParameterList parametros = new SQLiteParameterList();
            parametros.Clear();

            parametros.Add("@VALOR", c.Valor);
            if (c.Criterio == CriterioCliente.Zona)
            {
                //parametros.Add("@HOY", DateTime.Now.ToString("YYYY-MM-DD"));
                if (c.Dias != Dias.T)
                    parametros.Add("@DIA", c.Dias.ToString());
            }
            parametros.Add("@COD_CIA", pCompania.ToUpper());
            return parametros;
        }
        
        /// <summary>
        /// Generar la sentencia de busqueda de cliente segun el criterio
        /// </summary>
        /// <param name="c">criterios de busqueda</param>
        /// <returns></returns>
        private static string CrearSentenciaBusquedaCliente(CriterioBusquedaCliente c)
        {
            string sentencia=string.Empty;

            if(c.Criterio == CriterioCliente.Zona)
            {
                if(c.Visita.Equals("Visitados"))
                {
                    sentencia +=
                        " SELECT DISTINCT C.COD_CLT, C.NOM_CLT, C.DIR_CLT, C.COD_ZON, " +
                        " V.INICIO AS FECHA_VISITA,O.DIA,O.ORDEN,C.FEC_PLN " +
                        " FROM " + Table.ERPADMIN_CLIENTE + " C " +                        
                        " INNER JOIN " + Table.ERPADMIN_VISITA + " V " +
                        " ON (C.COD_CLT = V.CLIENTE " +
                        " AND julianday(date(V.INICIO)) = julianday(date('now','localtime'))) ";                    
                    sentencia += (c.Alfabetico ? " ORDER BY C.NOM_CLT":" ORDER BY FECHA_VISITA,C.FEC_PLN");                
                }

                else if(c.Visita.Equals("No Visitados"))
                {
                    sentencia +=
                        " SELECT DISTINCT C.COD_CLT, C.NOM_CLT, C.DIR_CLT, C.COD_ZON, O.DIA,O.ORDEN,C.FEC_PLN" +
                        " FROM " + Table.ERPADMIN_CLIENTE + " C ";
                    sentencia += " WHERE " +
                        " C.COD_CLT NOT IN (SELECT CLIENTE FROM " + Table.ERPADMIN_VISITA +
                                          " WHERE julianday(date(INICIO)) = julianday(date('now','localtime'))) ";						
			
			        sentencia += (c.Alfabetico ? " ORDER BY C.NOM_CLT":" ORDER BY C.FEC_PLN");                
                }
                else if(c.Visita.Equals("Todos"))
                {
                    sentencia +=
                        " SELECT DISTINCT C.COD_CLT, C.NOM_CLT, C.DIR_CLT, C.COD_ZON, " +
                        " V.INICIO AS FECHA_VISITA,O.DIA,O.ORDEN,C.FEC_PLN " +
                        " FROM " + Table.ERPADMIN_CLIENTE + " C " +                        
                        " LEFT OUTER JOIN " + Table.ERPADMIN_VISITA + " V " +
				        " ON (C.COD_CLT = V.CLIENTE " +
                        " AND julianday(date(V.INICIO)) = julianday(date('now','localtime'))) ";
                    sentencia += (c.Alfabetico ? " ORDER BY C.NOM_CLT" : " ORDER BY FECHA_VISITA,C.FEC_PLN");                    
                }
            }
            else
            {
                sentencia +=
                        " SELECT DISTINCT C.COD_CLT, C.NOM_CLT, C.DIR_CLT, C.COD_ZON, C.FEC_PLN,C.COD_CIA " +
                        " FROM " + Table.ERPADMIN_CLIENTE + " C ";
               
                if(c.Criterio == CriterioCliente.Nombre)
                    sentencia+= " WHERE C.NOM_CLT "+(c.Agil? "=" : "LIKE")+" @VALOR AND UPPER(COD_CIA)=@COD_CIA";
                else if(c.Criterio == CriterioCliente.Codigo)
                    sentencia += " WHERE C.COD_CLT " + (c.Agil ? "=" : "LIKE") + "  @VALOR  AND UPPER(COD_CIA)=@COD_CIA";
                else
                    sentencia += " WHERE UPPER(COD_CIA)=@COD_CIA";

                sentencia += (c.Alfabetico ? " ORDER BY C.NOM_CLT": " ORDER BY C.COD_ZON, C.FEC_PLN");             
            }
            return sentencia;
        }

        /// <summary>
        /// Generar la sentencia de busqueda todos los clientes en la base de datos.
        /// </summary>
        /// <returns></returns>
        private static string CrearSentenciaBusquedaCliente()
        {
            string sentencia = string.Empty;

            sentencia +=
                    " SELECT DISTINCT C.COD_CLT, C.NOM_CLT, C.DIR_CLT, C.COD_ZON, C.FEC_PLN,C.COD_CIA " +
                    " FROM " + Table.ERPADMIN_CLIENTE + " C ";

            sentencia += (" ORDER BY C.COD_ZON, C.FEC_PLN");

            return sentencia;
        }

        #endregion

        #region IPrintable Members

        public override object GetField(string name)
        {
            switch (name)
            {
                case "COBRADO": return montos.MontoPagado;
                case "NOTAS_CREDITO": return montos.MontoNotasAplicadas;
                case "VENDIDO": return montos.MontoComprado;
                //LDS Caso 27409 20/03/2007
                case "PREVENTA": return montos.MontoPreventa;
                case "FACTURADO": return montos.MontoFacturas;
                //ABC Caso 35771 
                case "DEVUELTOSINDOC":return montos.MontoDevueltoSinDoc;
                case "DEVUELTOCONDOC":return montos.MontoDevueltoConDoc;
                case "DEVUELTOTOTAL":return montos.MontoDevuelto;
                default: return base.GetField(name);
            }
        }

        #endregion
    }
}
