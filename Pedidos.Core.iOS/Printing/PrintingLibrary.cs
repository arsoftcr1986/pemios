using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Foundation;
using UIKit;

namespace Pedidos.Core
{
    public class PrintingLibrary
    {

	    // android built in classes for bluetooth operations

        //BluetoothAdapter mBluetoothAdapter;
        //BluetoothSocket mmSocket;
        //BluetoothDevice mmDevice;
		private static readonly string SPP_UUID = "00001101-0000-1000-8000-00805F9B34FB";
		String dataEnviada;			
		//String   folder = Environment. getExternalStorageDirectory().getPath();
		//File logPrint;
        //OutputStream mmOutputStream;
        //InputStream mmInputStream;
        Thread workerThread;			
		byte[] readBuffer;
		int readBufferPosition;
		int counter;
		volatile bool stopWorker;
			
	    public PrintingLibrary() {	
            //logPrint = new File(folder,"ImpresionLog.txt");
            //if(logPrint.length()>0)
            //{
            //    logPrint.delete();
            //}
            //logPrint = new File(folder,"ImpresionLog.txt");
		    // TODO Auto-generated constructor stub
	    }
	
	    private void EscribirLog(String msj)
	    {
            //FileWriter writer;
            //try {
            //    writer = new FileWriter(logPrint, true);
            //    writer.write(msj+"\n");
            //    writer.close();
            //} catch (IOException e) {
            //    // TODO Auto-generated catch block
            //    System. e.Message();
            //}
         
	    }
	
	    public bool Inicializar(String deviceName)
	    {
		    this.EscribirLog("Empieza metodo Inicializar");
		    bool result=false;
		    result=this.findBT(deviceName);
		    if(result)
		    {
			    result=this.openBT();
		    }
		    this.EscribirLog("Resultado Inicializar "+result);		
		    return result;
	    }
	
	    public bool findBT(String deviceName) {

            //try 
            //{
            //    this.EscribirLog("Empieza metodo findBT");
            //    mBluetoothAdapter = null;
            //    mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            //    if (mBluetoothAdapter == null) {
            //        return false;
            //    }

            //    if (!mBluetoothAdapter.isEnabled()) {
            //        //Intent enableBluetooth = new Intent(
            //        //		BluetoothAdapter.ACTION_REQUEST_ENABLE);	
            //        return false;
            //    }

            //    Set<BluetoothDevice> pairedDevices = mBluetoothAdapter
            //            .getBondedDevices();
            //    if (pairedDevices.size() > 0) {
            //        for (BluetoothDevice device : pairedDevices) {
					
            //            // deviceName is the name of the bluetooth printer device
            //            if (device.getName().trim().equals(deviceName)) {
            //                mmDevice = device;
            //                this.EscribirLog("Encontro el dispositivo");
            //                return true;
            //            }
            //            /*if ((device.getName().trim().equals("Mobile Printer")) || (device.getName().trim().equals("MobilePrinter")) || (device.getName().trim().equals("Printer"))) {
            //                mmDevice = device;
            //                return true;
            //            }*/
            //        }
            //    }
            //    this.EscribirLog("No Encontro el dispositivo");
            //    return false;
			
            //} catch (NullPointerException e) {
            //    this.EscribirLog("findBT Error "+e.getMessage());
            //    //e.printStackTrace();
            //    return false;
            //} catch (Exception e) {
            //    this.EscribirLog("findBT Error "+e.getMessage());
            //    //e.printStackTrace();
            //    return false;
            //}
            return false;
	    }
	
	    public bool openBT(){
            //try {
            //    this.EscribirLog("Inicializa metodo openBT");
            //    // Standard SerialPortService ID
            //    //UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            //        this.EscribirLog("Escribe UUID "+SPP_UUID);
            //        mmSocket = mmDevice.createRfcommSocketToServiceRecord(SPP_UUID);
            //        Method m = mmDevice.getClass().getMethod("createRfcommSocket", new Class[] {int.class});
            //        mmSocket = (BluetoothSocket) m.invoke(mmDevice, 1);
            //    if(stopWorker)
            //    {
            //        stopWorker=false;
            //    }
            //    /*try
            //    {
            //        mmSocket.close();
            //    }
            //    catch(Exception ex)
            //    {
				
            //    }	*/			
            //    mmSocket.connect();
            //    mmOutputStream = mmSocket.getOutputStream();
            //    mmInputStream = mmSocket.getInputStream();
            //    this.EscribirLog("Inicializa metodo beginListenForData");
            //    beginListenForData();
            //    return true;
            //} catch (NullPointerException e) {
            //    this.EscribirLog("openBT Error "+e.getMessage());
            //    //e.printStackTrace();
            //    return false;
            //} catch (Exception e) {
            //    //e.printStackTrace();
            //    this.EscribirLog("openBT Error "+e.getMessage());
            //    return false;
            //}
            return false;
	    }
	
	    public void beginListenForData() {
            //try {
            //    final Handler handler = new Handler();
			
            //    // This is the ASCII code for a newline character
            //    final byte delimiter = 10;

            //    if(stopWorker)
            //    {
            //        stopWorker=false;
            //    }
            //    readBufferPosition = 0;
            //    readBuffer = new byte[1024];
			
            //    workerThread = new Thread(new Runnable() {
            //        public void run() {
            //            while (!Thread.currentThread().isInterrupted()
            //                    && !stopWorker) {
						
            //                try {
							
            //                    int bytesAvailable = mmInputStream.available();
            //                    if (bytesAvailable > 0) {
            //                        byte[] packetBytes = new byte[bytesAvailable];
            //                        mmInputStream.read(packetBytes);
            //                        for (int i = 0; i < bytesAvailable; i++) {
            //                            byte b = packetBytes[i];
            //                            if (b == delimiter) {
            //                                byte[] encodedBytes = new byte[readBufferPosition];
            //                                System.arraycopy(readBuffer, 0,
            //                                        encodedBytes, 0,
            //                                        encodedBytes.length);
            //                                final String data = new String(
            //                                        encodedBytes, "ISO-8859-1");
            //                                readBufferPosition = 0;

            //                                handler.post(new Runnable() {
            //                                    public void run() {
            //                                        dataEnviada=(data);
            //                                    }
            //                                });
            //                            } else {
            //                                readBuffer[readBufferPosition++] = b;
            //                            }
            //                        }
            //                    }
							
            //                } catch (IOException ex) {
            //                    stopWorker = true;
            //                }
						
            //            }
            //        }
            //    });

            //    workerThread.start();
            //    this.EscribirLog("Termina metodo beginListenForData");
            //} catch (NullPointerException e) {
            //    this.EscribirLog("beginListenForData Error "+e.getMessage());
            //    //e.printStackTrace();
            //} catch (Exception e) {
            //    this.EscribirLog("beginListenForData Error "+e.getMessage());
            //    //e.printStackTrace();
            //}
	    }
	
	    public bool sendData(String texto) {
    //        try {
			
    //            // the text typed by the user
    //            String msg = texto;
    //            msg += "\n";
			
    ////			mmOutputStream.write(convertExtendedAscii(msg));
    //            mmOutputStream.write(msg.getBytes("ISO-8859-1"));
    //            return true;				
			
    //        } catch (NullPointerException e) {
    //            this.EscribirLog("sendData Error "+e.getMessage());
    //            //e.printStackTrace();
    //            return false;
    //        } catch (Exception e) {
    //            this.EscribirLog("sendData Error "+e.getMessage());
    //            //e.printStackTrace();
    //            return false;
    //        }
            return false;
	    }
	
	    /**
	     * Converts a text string with extended ASCII characters
	     * into a Java-friendly byte array.
	     * @param input
	     * @return
	     */
	    public byte[] convertExtendedAscii(String input)
	    {
	            int length = input.Length;
	            byte[] retVal = new byte[length];
	       
	            for(int i=0; i<length; i++)
	            {
	                      char c = input.ElementAt(i);
	                 
	                      if (c < 127)
	                      {
	                              retVal[i] = (byte)c;
	                      }
	                      else
	                      {
	                              retVal[i] = (byte)(c - 256);
	                      }
	            }
	       
	            return retVal;
	    }

        public bool closeBT()
        {
            //try 
            //{
            //    this.EscribirLog("Inicializa metodo closeBT");
            //        stopWorker = true;
            //        try
            //        {
            //            mmSocket.close();
            //        }
            //        catch(Exception e)
            //        {
					
            //        }
            //        mmOutputStream.close();
            //        mmInputStream.close();
            //        this.EscribirLog("Termina metodo closeBT");
            //    return true;
            //} catch (NullPointerException e) {
            //    this.EscribirLog("closeBT Error "+e.getMessage());
            //    //e.printStackTrace();
            //    return false;
            //} catch (Exception e) {
            //    this.EscribirLog("closeBT Error "+e.getMessage());
            //    //e.printStackTrace();
            //    return false;
            //}
            return false;
	    }

    }
}