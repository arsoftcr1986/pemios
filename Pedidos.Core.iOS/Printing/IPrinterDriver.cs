using System;

namespace Pedidos.Core
{
	/// <summary>
	/// Summary description for IPrinterDriver.
	/// </summary>
	public interface IPrinterDriver
	{

		string ChangeFontStyle(bool bold, bool italic);
		string ChangeFont(string fontName,int fontSize);
		
		void Open();
		void Print(string text,string tipo);
		void Configure();
		void Close();
	}

    /// <summary>
    /// Summary description for IPrintable.
    /// </summary>
    public interface IPrintable
    {
        string GetObjectName();
        object GetField(string name);
    }
}
