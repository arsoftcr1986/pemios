//using System;
//using System.Text;
//using EMF.Printing.RDL;
//using EMF.Printing.Text;
//
//namespace EMF.Printing.Commands
//{
//	/// <summary>
//	/// Summary description for cmdPrint.
//	/// </summary>
//	public class PrintFCommand:RDLCommand
//	{
//		public PrintFCommand(StringTokenizer tokenizer):base(tokenizer)
//		{
//			this.type = CommandType.PrintF;
//			this.argumentKind =  new TokenKind[]{TokenKind.Word,TokenKind.QuotedString};
//			ParseArguments(tokenizer);
//		}
//
//		public override string Execute(RDLEngine engine, RDLContext context)
//		{			
//			StringBuilder output = new StringBuilder();	
//			string word;
//			string format;			
//			string[] words;
//			IPrintable symbol;
//			object symbolValue;
//
//			word = ((Token)argumentList[0]).Value;
//			format = ((Token)argumentList[1]).Value;
//			format = format.Substring(1,format.Length-2);	
//			words = word.Split('.');
//			symbol = context[words[0]];
//
//			if (symbol!=null)
//			{	
//				symbolValue = symbol.GetField(words[1]);
//
//				try
//				{				
//					if (symbolValue.GetType()==typeof(int))
//						output.Append(((int)symbolValue).ToString(format));
//
//					if (symbolValue.GetType()==typeof(long))
//						output.Append(((long)symbolValue).ToString(format));
//				
//					if (symbolValue.GetType()==typeof(float))
//						output.Append(((float)symbolValue).ToString(format));
//
//					if (symbolValue.GetType()==typeof(double))
//						output.Append(((double)symbolValue).ToString(format));
//
//					if (symbolValue.GetType()==typeof(DateTime))
//						output.Append(((DateTime)symbolValue).ToString(format));
//
//					if (symbolValue.GetType()==typeof(string))
//					{
//						try
//						{
//							int charIndex=0;
//							char[] valueChars = ((string)symbolValue).ToCharArray();
//							
//							foreach (char character in format)
//							{								
//								if (charIndex<valueChars.Length)
//								{
//									output.Append(valueChars[charIndex].ToString());
//									charIndex++;
//								}
//								else
//								{
//									output.Append(character);
//								}
//							}
//							
//						}
//						catch
//						{
//							output.Append(((string)symbolValue));
//						}
//					}
//
//				}
//				catch
//				{
//					throw new Exception("Error formating symbol");
//				}
//			}
//			else
//				throw new Exception("Symbol " + word + " not found!");
//			
//			return output.ToString();
//		}
//
//	}
//}
