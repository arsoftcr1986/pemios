using BI.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pedidos.Core
{
    public static class ReportHelper
    {
        /// <summary>
        /// Folder donde se ponen los reportes
        /// </summary>
        private const string FolderReportes = "Reportes";

        /// <summary>
        /// Se crea la ruta completa del reporte, a partir del FRmConfig.FullAppPath
        /// y se asume que los reportes van a estar localizados en el subfolder Reportes
        /// y se concatena la extensi?n .rdl
        /// </summary>
        /// <param name="nombreReporte">nombre del archivo del reporte sin extensi?n</param>
        /// <returns></returns>
        internal static string CrearRutaReporte(Rdl nombreReporte)
        {
#if DEBUG
            if (FRmConfig.TamañoPapel.Equals(2))
            {
                return System.IO.Path.Combine(FRmConfig.FullAppPath, ReportHelper.FolderReportes + "2", nombreReporte.ToString() + ".rdl");
            }
            if (FRmConfig.TamañoPapel == 3)
            {
                return System.IO.Path.Combine(FRmConfig.FullAppPath, ReportHelper.FolderReportes + "3", nombreReporte.ToString() + ".rdl");
            }
            return System.IO.Path.Combine(FRmConfig.FullAppPath, ReportHelper.FolderReportes, nombreReporte.ToString() + ".rdl");
#else
            string folder = "";
            //if (FRmConfig.GuardarenSDRep)
            //{
            //    folder = System.IO.Path.Combine(Softland.ERP.FR.Mobile.App.ObtenerRutaSD(), "FR.DROID.REPORTS");
            //}
            //else
            //{
            //    folder = FRmConfig.FullAppPath;
            //}
            //if (FRmConfig.TamañoPapel.Equals(2))
            //{
            //    return System.IO.Path.Combine(folder, ReportHelper.FolderReportes + "2", nombreReporte.ToString() + ".rdl");
            //}
            //if (FRmConfig.TamañoPapel == 3)
            //{
            //    return System.IO.Path.Combine(folder, ReportHelper.FolderReportes + "3", nombreReporte.ToString() + ".rdl");
            //}
            return System.IO.Path.Combine(folder, ReportHelper.FolderReportes, nombreReporte.ToString() + ".rdl");
#endif
        }

        internal static string CrearRutaReporteStr(string nombreReporte)
        {
#if DEBUG
            if (FRmConfig.TamañoPapel.Equals(2))
            {
                return System.IO.Path.Combine(FRmConfig.FullAppPath, ReportHelper.FolderReportes + "2", nombreReporte.ToString() + ".rdl");
            }
            if (FRmConfig.TamañoPapel == 3)
            {
                return System.IO.Path.Combine(FRmConfig.FullAppPath, ReportHelper.FolderReportes + "3", nombreReporte.ToString() + ".rdl");
            }
            return System.IO.Path.Combine(FRmConfig.FullAppPath, ReportHelper.FolderReportes, nombreReporte.ToString() + ".rdl");
#else
            string folder = "";
            //if (FRmConfig.GuardarenSDRep)
            //{
            //    folder = System.IO.Path.Combine(Softland.ERP.FR.Mobile.App.ObtenerRutaSD(), "FR.DROID.REPORTS");
            //}
            //else
            //{
            //    folder = FRmConfig.FullAppPath;
            //}
            //if (FRmConfig.TamañoPapel.Equals(2))
            //{
            //    return System.IO.Path.Combine(folder, ReportHelper.FolderReportes + "2", nombreReporte.ToString() + ".rdl");
            //}
            //if (FRmConfig.TamañoPapel == 3)
            //{
            //    return System.IO.Path.Combine(folder, ReportHelper.FolderReportes + "3", nombreReporte.ToString() + ".rdl");
            //}
            return System.IO.Path.Combine(folder, ReportHelper.FolderReportes, nombreReporte.ToString() + ".rdl");
#endif
        }

        public static string DevolverRutaReportes()
        {
#if DEBUG
            if (FRmConfig.TamañoPapel.Equals(2))
            {
                return System.IO.Path.Combine(FRmConfig.FullAppPath, ReportHelper.FolderReportes + "2");
            }
            if (FRmConfig.TamañoPapel == 3)
            {
                return System.IO.Path.Combine(FRmConfig.FullAppPath, ReportHelper.FolderReportes + "3");
            }
            return System.IO.Path.Combine(FRmConfig.FullAppPath, ReportHelper.FolderReportes);
#else
            string folder = "";
            //if (FRmConfig.GuardarenSDRep)
            //{
            //    folder = System.IO.Path.Combine(Softland.ERP.FR.Mobile.App.ObtenerRutaSD(), "FR.DROID.REPORTS");
            //}
            //else
            //{
            //    folder = FRmConfig.FullAppPath;
            //}
            //if (FRmConfig.TamañoPapel.Equals(2))
            //{
            //    return System.IO.Path.Combine(folder, ReportHelper.FolderReportes + "2");
            //}
            //if (FRmConfig.TamañoPapel == 3)
            //{
            //    return System.IO.Path.Combine(folder, ReportHelper.FolderReportes + "3");
            //}
            return System.IO.Path.Combine(folder, ReportHelper.FolderReportes);
#endif
        }
    }
}