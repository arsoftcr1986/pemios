using System;
using System.Xml;
using System.IO;

namespace Pedidos.Core
{
	/// <summary>
	/// Summary description for Impresora.
	/// </summary>
	public class Impresora
	{
		#region Constantes
		//Caso 32256 LDS 24/04/2008
		/// <summary>
		/// Debe ser utilizada para impresoras Ascii.
		/// </summary>
		public const string ImpresoraGenerica = "GENERICA";
		#endregion

		#region Variables de clase

		#region Variables privadas
		
		/// <summary>
		/// Cantidad de copias.
		/// </summary>
		protected static int cantidadCopias = 0;
		/// <summary>
		/// Cantidad de l�neas de detalle.
		/// </summary>
        // mvega: si cantidad lineas es cero ponerle 50 para que no de error mientrastanto
		protected static int cantidadLineas = 50;
        //Caso 35723 LJR 04/08/2009
        /// <summary>
        /// Sugerir Imprimir.
        /// </summary>
        private static bool sugerirImprimir = true;

		#endregion

		#region Variables p�blicas
		//Caso 32256 LDS 24/04/2008
		public static bool UtilizaImpresoraGenerica = false;
		#endregion

		#endregion

		#region Propiedades de la clase

		//Caso 25452 LDS 18/10/2007
		public static int CantidadCopias
		{
			get
			{
				return cantidadCopias;
			}
			set
			{
				cantidadCopias = value;
			}
		}
		//Caso 32426 ABC 13/05/2008
		public static int CantidadLineas
		{
			get
			{
				return cantidadLineas;
			}
			set
			{
				cantidadLineas = value;
			}
		}
        //Caso 35723 LJR 04/08/2009
        public static bool SugerirImprimir
        {
            get 
            { 
                return sugerirImprimir; 
            }
            set 
            { 
                sugerirImprimir = value; 
            }
        }
		#endregion
		
		#region Metodos de configuracion de impresora y puerto
		/// <summary>
		/// Funcion encargada de identificar la impresora seleccionada.
		/// </summary>
		/// <param name="impresora">Nombre de la impresora seleccionada</param>
		public static void IndicaImpresora(string impresora)
		{
			Impresora.UtilizaImpresoraGenerica = impresora.Equals(Impresora.ImpresoraGenerica);
            if (Impresora.UtilizaImpresoraGenerica)
                return;     
            return;
		}

		/// <summary>
		/// Obtiene el driver con el puerto indicado para realizar la impresi�n.
		/// </summary>
		/// <returns></returns>
		public static IPrinterDriver ObtenerDriver()
		{
			//Caso 32256 LDS 24/04/2008
            if (Impresora.UtilizaImpresoraGenerica)
                //return new PrinterCEDriver(Impresora.Puerto);
                return new AndroidDriver(FRmConfig.Impresora);
            else
            {

                //Caso 32426 ABC 13/05/2008
                //return new PrinterCEDriver(Impresora.Tipo, Impresora.Puerto, Impresora.CantidadLineas);
                //return new AndroidDriver(Impresora.Tipo.ToString(), Impresora.CantidadLineas);
                return new AndroidDriver(FRmConfig.Impresora, Impresora.CantidadLineas);
            }
		}

		#endregion
    }
}
