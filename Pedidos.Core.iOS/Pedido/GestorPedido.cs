using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BI.Shared;
using BI.iOS;
using System.Threading.Tasks;
using System.Globalization;
using System.Data.SQLiteBase;

namespace Pedidos.Core
{
    public static class GestorPedido
    {
        public static PedidosCollection PedidosCollection = new PedidosCollection();
        public static List<Articulo> carrito;
        public static string codigoArticuloInvalido;
        public static string descripcionArticuloInvalido;
        public static string errorArticuloInvalido;
        public static bool agregandoArticulos = false;

        private static DetallePedido DetalleSeleccionado;


        private static string codigoConsecutivo = "";
        /// <summary>
        /// Variable que almacena el c�digo del consecutivo a usar para los pedidos.
        /// </summary>
        public static string CodigoConsecutivo
        {
            get { return codigoConsecutivo; }
            set { codigoConsecutivo = value; }
        }

        #region M�todos

        /// <summary>
        /// M�todo que se encarga de agregar o modificar los detalles del pedido y hacer los c�lculos correspondientes.
        /// </summary>
        /// <returns></returns>
        public static bool AgregarModificarDetalles()
        {
            try
            {
                foreach (Articulo art in carrito)
                {
                    Articulo artTemp = (Articulo)art.Clone();
                    Articulo artBonifTemp = null;
                    decimal existencias = 0;
                    decimal TotalAlmacenBonif = 0;
                    decimal CantBonifAlmacen = 0;
                    decimal CantBonifDetalle = 0;
                    decimal PorcentajeDesc = 0;
                    decimal DescA = 0;

                    codigoArticuloInvalido = artTemp.Codigo;
                    descripcionArticuloInvalido = artTemp.Descripcion;

                    DetalleSeleccionado = GestorPedido.PedidosCollection.BuscarDetalle(artTemp);
                    
                    if (DetalleSeleccionado == null)
                    {
                        DetalleSeleccionado = new DetallePedido();
                    }
                    
                    CargarPrecioArticulo(artTemp);
                    DetalleSeleccionado.Precio = artTemp.Precio;
                    CargarExistenciasArticulo(artTemp, ref existencias);

                    if (artTemp.CantidadAlmacen == 0)
                    {
                        artTemp.CantidadAlmacen = 1;
                    }
                    
                    CargarBonifDesc(artTemp, ref artBonifTemp, ref TotalAlmacenBonif, ref CantBonifAlmacen, ref CantBonifDetalle, ref PorcentajeDesc);
                    
                    if (DetalleSeleccionado.LineaBonificada == null)
                    {
                        CantBonifAlmacen = 0;
                        CantBonifDetalle = 0;
                    }
                    else
                    {
                        CantBonifAlmacen = DetalleSeleccionado.LineaBonificada.UnidadesAlmacen;
                        CantBonifDetalle = DetalleSeleccionado.LineaBonificada.UnidadesDetalle;
                    }

                    if (DetalleSeleccionado.Descuento != null)
                    {
                        DescA = DetalleSeleccionado.Descuento.Monto;
                    }
                    else
                    {
                        DetalleSeleccionado.MontoDescuento = 0;
                    }

                    //Agregamos o modificamos el detalle que contiene el art�culo
                    TotalAlmacenBonif = CantBonifAlmacen + (CantBonifDetalle / artTemp.UnidadEmpaque);
                    GestorPedido.PedidosCollection.Gestionar(artTemp, GlobalUI.RutaActual.Codigo, new Precio(artTemp.PrecioMaximo, artTemp.PrecioMinimo), artTemp.CantidadAlmacen, artTemp.CantidadDetalle, 0, 0, true, DetalleSeleccionado.Tope);
                    GestionarPaquetesReglas(false, artTemp);

                    #region Refrescar articulo

                    //Volvemos a refrescar el articulo luego de aplicar las reglas de paquetes
                    
                    DetalleSeleccionado = GestorPedido.PedidosCollection.BuscarDetalle(artTemp);

                    if (DetalleSeleccionado == null)
                    {
                        DetalleSeleccionado = new DetallePedido();
                    }
                   
                    CargarPrecioArticulo(artTemp);
                    DetalleSeleccionado.Precio = artTemp.Precio;
                    CargarExistenciasArticulo(artTemp, ref existencias);

                    if (GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.UsaLocalizacion)
                    {
                        artTemp.Bodega.Localizacion = artTemp.Bodega.Obtenerlocalizaciones(artTemp)[0];
                    }

                    if (artTemp.CantidadAlmacen == 0)
                    {
                        artTemp.CantidadAlmacen = 1;
                    }
                    
                    CargarBonifDesc(artTemp, ref artBonifTemp, ref TotalAlmacenBonif, ref CantBonifAlmacen, ref CantBonifDetalle, ref PorcentajeDesc);
                    
                    if (DetalleSeleccionado.LineaBonificada == null)
                    {
                        CantBonifAlmacen = 0;
                        CantBonifDetalle = 0;
                    }
                    else
                    {
                        CantBonifAlmacen = DetalleSeleccionado.LineaBonificada.UnidadesAlmacen;
                        CantBonifDetalle = DetalleSeleccionado.LineaBonificada.UnidadesDetalle;
                    }
                    if (DetalleSeleccionado.Descuento != null)
                    {
                        DescA = DetalleSeleccionado.Descuento.Monto;
                    }
                    
                    #endregion
                }

                Pedido pedido = GestorPedido.PedidosCollection.Buscar(GlobalUI.ClienteActual.Compania);

                if (pedido != null)
                {
                    carrito = (pedido.Detalles.Lista.Select(x => x.Articulo).ToList());
                }

                codigoArticuloInvalido = "";
                descripcionArticuloInvalido = "";
                errorArticuloInvalido = "";

                return true;
            }
            catch(Exception ex)
            {
                errorArticuloInvalido = ex.Message;

                return false;
            }
        }

        /// <summary>
        /// M�todo que se encarga de agregar o modificar los detalles del pedido y hacer los c�lculos correspondientes.
        /// </summary>
        /// <returns></returns>
        public static bool AgregarModificarDetalle(Articulo art)
        {
            try
            {
                Articulo artTemp = (Articulo)art.Clone();
                Articulo artBonifTemp = null;
                decimal existencias = 0;
                decimal TotalAlmacenBonif = 0;
                decimal CantBonifAlmacen = 0;
                decimal CantBonifDetalle = 0;
                decimal PorcentajeDesc = 0;
                decimal DescA = 0;

                codigoArticuloInvalido = artTemp.Codigo;
                descripcionArticuloInvalido = artTemp.Descripcion;

                DetalleSeleccionado = GestorPedido.PedidosCollection.BuscarDetalle(artTemp);

                if (DetalleSeleccionado == null)
                {
                    DetalleSeleccionado = new DetallePedido();
                }

                DetalleSeleccionado.Precio = artTemp.Precio;
                CargarExistenciasArticulo(artTemp, ref existencias);
                
                if (artTemp.CantidadAlmacen == 0)
                {
                    artTemp.CantidadAlmacen = 1;
                }

                CargarBonifDesc(artTemp, ref artBonifTemp, ref TotalAlmacenBonif, ref CantBonifAlmacen, 
                                    ref CantBonifDetalle, ref PorcentajeDesc);
                
                if (DetalleSeleccionado.LineaBonificada == null)
                {
                    CantBonifAlmacen = 0;
                    CantBonifDetalle = 0;
                }
                else
                {
                    CantBonifAlmacen = DetalleSeleccionado.LineaBonificada.UnidadesAlmacen;
                    CantBonifDetalle = DetalleSeleccionado.LineaBonificada.UnidadesDetalle;
                }

                if (DetalleSeleccionado.Descuento != null)
                {
                    DescA = DetalleSeleccionado.Descuento.Monto;
                }
                else
                {
                    DetalleSeleccionado.MontoDescuento = 0;
                }

                //Agregamos o modificamos el detalle que contiene el art�culo
                TotalAlmacenBonif = CantBonifAlmacen + (CantBonifDetalle / artTemp.UnidadEmpaque);
                GestorPedido.PedidosCollection.Gestionar(artTemp, GlobalUI.RutaActual.Codigo, new Precio(artTemp.PrecioMaximo, artTemp.PrecioMinimo), artTemp.CantidadAlmacen, artTemp.CantidadDetalle, 0, 0, true, DetalleSeleccionado.Tope);
                GestionarPaquetesReglas(false, artTemp);

                #region Refrescar articulo

                //Volvemos a refrescar el articulo luego de aplicar las reglas de paquetes

                DetalleSeleccionado = GestorPedido.PedidosCollection.BuscarDetalle(artTemp);

                if (DetalleSeleccionado == null)
                {
                    DetalleSeleccionado = new DetallePedido();
                }

                DetalleSeleccionado.Precio = artTemp.Precio;
                CargarExistenciasArticulo(artTemp, ref existencias);

                if (GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.UsaLocalizacion && string.IsNullOrEmpty(artTemp.Bodega.Localizacion))
                {
                    artTemp.Bodega.Localizacion = artTemp.Bodega.Obtenerlocalizaciones(artTemp)[0];
                }

                if (artTemp.CantidadAlmacen == 0)
                {
                    artTemp.CantidadAlmacen = 1;
                }

                CargarBonifDesc(artTemp, ref artBonifTemp, ref TotalAlmacenBonif, ref CantBonifAlmacen, ref CantBonifDetalle, ref PorcentajeDesc);

                if (DetalleSeleccionado.LineaBonificada == null)
                {
                    CantBonifAlmacen = 0;
                    CantBonifDetalle = 0;
                }
                else
                {
                    CantBonifAlmacen = DetalleSeleccionado.LineaBonificada.UnidadesAlmacen;
                    CantBonifDetalle = DetalleSeleccionado.LineaBonificada.UnidadesDetalle;
                }
                if (DetalleSeleccionado.Descuento != null)
                {
                    DescA = DetalleSeleccionado.Descuento.Monto;
                }

                #endregion

                codigoArticuloInvalido = "";
                descripcionArticuloInvalido = "";
                errorArticuloInvalido = "";

                return true;
            }
            catch (Exception ex)
            {
                errorArticuloInvalido = ex.Message;

                return false;
            }
        }

        /// <summary>
        /// Retira el detalle seleccionado del documento
        /// </summary>
        public static void RetirarDetalle(Articulo pArt)
        {
            DetalleSeleccionado = GestorPedido.PedidosCollection.BuscarDetalle(pArt);

            if (DetalleSeleccionado == null)
                return;

            try
            {
                GestorPedido.PedidosCollection.EliminarDetalle(pArt);
            }
            catch (Exception exc)
            {
                throw new Exception("Error al gestionar el pedido. " + exc.Message);
            }
        }

        /// <summary>
        /// Gestiona los paquetes de relas de descuentos y bonificaciones
        /// </summary>
        /// <param name="allDetails"></param>
        private static void GestionarPaquetesReglas(bool allDetails, Articulo pArt)
        {
            Pedido pedido = GestorPedido.PedidosCollection.Buscar(GlobalUI.ClienteActual.Compania);
            DetallePedido detalle = allDetails ? null : pedido.Detalles.Buscar(pArt.Codigo);
            ConfigDocCia config = GestorPedido.PedidosCollection.ObtenerConfiguracionVenta(GlobalUI.ClienteActual.Compania);
            String mensaje = String.Empty;
            bool resultado = Regalias.AplicarRegalias(ref pedido, ref detalle, config.Nivel.Nivel, GlobalUI.RutaActual.Codigo, out mensaje);
            //tieneBonificaiones = Regalias.TieneBonificaciones;
            //tieneBonificaiones = true;
            if (!resultado)
            {
                //TODO
                //ShowMessage sm = new ShowMessage(this, (String.Format("No se puedieron aplicar los paquete y reglas de descuentos-bonificaci�n.Error:{0}.", mensaje)), true);
                //RunOnUiThread(() => sm.Mostrar());
            }
            GestorPedido.PedidosCollection.SacarMontosTotales();
        }

        private static void CargarBonifDesc(Articulo pArt, ref Articulo pArtBonif, ref decimal TotalAlmacenBonif, ref decimal pCantBonifAlmacen, ref decimal pcantBonifDetalle, ref decimal pPorcentaje)
        {
            try
            {
                ProcesarBonifcaciones(pArt, ref pArtBonif, ref TotalAlmacenBonif, ref pCantBonifAlmacen, ref pcantBonifDetalle);
                ProcesarDescuentos(pArt, ref pPorcentaje);
            }
            catch (Exception ex)
            {
                throw new Exception("Error cargando bonificaciones y descuentos.");
            }

        }

        /// <summary>
        /// Procesa las bonificaciones
        /// </summary>
        private static void ProcesarBonifcaciones(Articulo pArt, ref Articulo pArtBonif, ref decimal TotalAlmacenBonif, ref decimal pCantBonifAlmacen, ref decimal pcantBonifDetalle)
        {
            try
            {
                decimal cantBonifAlmacen = decimal.Zero;
                decimal cantBonifDetalle = decimal.Zero;
                TotalAlmacenBonif = decimal.Zero;
                Pedido pedido = null;
                ClienteCia cliente = new ClienteCia();
                ConfigDocCia config = GestorPedido.PedidosCollection.ObtenerConfiguracionVenta(GlobalUI.ClienteActual.Compania);
                if (config.Nivel != null)
                {
                    cliente.NivelPrecio = config.Nivel;
                }
                else
                {
                    pedido = GestorPedido.PedidosCollection.BuscarNivelPrecio(pArt);
                    config.Nivel = pedido.Configuracion.Nivel;
                    cliente.NivelPrecio = pedido.Configuracion.Nivel;
                }
                cliente.Codigo = GlobalUI.ClienteActual.Codigo;
                GestorBonificacionDescuento.Bonificacion(cliente, pArt,
                pArt.CantidadAlmacen.ToString(), pArt.CantidadDetalle.ToString(), ref pArtBonif,
                ref cantBonifAlmacen, ref cantBonifDetalle, ref TotalAlmacenBonif);

                if (!PedidosCollection.CambiarBonificacion)
                {
                    pCantBonifAlmacen = cantBonifAlmacen;
                    pcantBonifDetalle = cantBonifDetalle;
                }
                else
                {
                    //if (this.txtCantBonifAlmacen.Text != GestorUtilitario.FormatDecimalAlmDet(decimal.Zero))
                    //    this.txtCantBonifAlmacen.Text = GestorUtilitario.FormatDecimalAlmDet(decimal.Zero);
                    //if (this.txtCantBonifDetalle.Text != GestorUtilitario.FormatDecimalAlmDet(decimal.Zero))
                    //    this.txtCantBonifDetalle.Text = GestorUtilitario.FormatDecimalAlmDet(decimal.Zero);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error procesando bonificaciones." + ex.Message);
            }

        }

        /// <summary>
        /// Procesa los descuentos
        /// </summary>
        private static void ProcesarDescuentos(Articulo pArt, ref decimal porcentaje)
        {
            decimal totalUnidadesAlmancen = decimal.Zero;
            porcentaje = decimal.Zero;
            totalUnidadesAlmancen = GestorUtilitario.TotalAlmacen(pArt.CantidadAlmacen.ToString(), pArt.CantidadDetalle.ToString(), pArt.UnidadEmpaque);
            ClienteCia cliente = new ClienteCia();
            ConfigDocCia config = GestorPedido.PedidosCollection.ObtenerConfiguracionVenta(GlobalUI.ClienteActual.Compania);
            cliente.Codigo = GlobalUI.ClienteActual.Codigo;
            if (config.Nivel != null)
                cliente.NivelPrecio = config.Nivel;
            porcentaje = GestorBonificacionDescuento.DescuentoMaximo(cliente, pArt, pArt.CantidadAlmacen.ToString(), pArt.CantidadDetalle.ToString());

            DetallePedido detalle = GestorPedido.PedidosCollection.BuscarDetalle(pArt);

            if (detalle != null)
            {
                // Si el detalle ya tiene un descuento aplicado por regla no se puede cambiar, de lo contrario el descuento 
                // ingresado por el usuario se utiliza.
                if (porcentaje == 0)
                {
                    if (detalle.Descuento != null && detalle.CalcularPorcentajeDescuento() > 0)
                    {
                        if (detalle.Descuento.Tipo == TipoDescuento.Porcentual)
                        {
                            porcentaje = detalle.CalcularPorcentajeDescuento();
                        }
                    }
                }

                if (PedidosCollection.CambiarDescuento)
                {
                    if (detalle.LineaBonificada != null)
                    {
                        GestorPedido.CambiosBonificacionDescuento(detalle.Articulo, porcentaje, detalle.LineaBonificada.UnidadesAlmacen, detalle.LineaBonificada.UnidadesDetalle);
                    }
                    else
                    {
                        GestorPedido.CambiosBonificacionDescuento(detalle.Articulo, porcentaje, 0, 0);
                    }
                }
                else
                {
                    if (detalle.LineaBonificada != null)
                    {
                        GestorPedido.CambiosBonificacionDescuento(detalle.Articulo, porcentaje, detalle.LineaBonificada.UnidadesAlmacen, detalle.LineaBonificada.UnidadesDetalle);
                    }
                    else
                    {
                        GestorPedido.CambiosBonificacionDescuento(detalle.Articulo, porcentaje, 0, 0);
                    }
                }
            }    
            //if (Pedidos.CambiarDescuento)
            //{
            //    txtDescB.Visibility = ViewStates.Visible;
            //    if (txtDescB.Text != GestorUtilitario.FormatPorcentaje(this.porcentaje))
            //        txtDescB.Text = GestorUtilitario.FormatPorcentaje(this.porcentaje);
            //    if (txtDescA.Text != GestorUtilitario.FormatPorcentaje(decimal.Zero))
            //        txtDescA.Text = GestorUtilitario.FormatPorcentaje(decimal.Zero);
            //}
            //else
            //{
            //    if (txtDescA.Text != GestorUtilitario.FormatPorcentaje(this.porcentaje))
            //        txtDescA.Text = GestorUtilitario.FormatPorcentaje(this.porcentaje);
            //    txtDescB.Visibility = ViewStates.Invisible;
            //}
        }

        /// <summary>
        /// Carga las existencias del articulo
        /// </summary>
        private static void CargarExistenciasArticulo(Articulo art, ref decimal existencias)
        {
            try
            {
                string primeraBodega = string.Empty;
                foreach (Ruta ruta in GlobalUI.RutasActuales)
                {
                    art.CargarExistenciaPedido(ruta.Bodega);
                    existencias = art.Bodega.Existencia;
                    if (existencias > 0)
                        break;
                    else
                    {
                        if (existencias > -1 && string.IsNullOrEmpty(primeraBodega))
                            primeraBodega = ruta.Bodega;
                    }
                }
                if (existencias <= 0)
                {
                    art.CargarExistenciaPedido(primeraBodega);
                    existencias = art.Bodega.Existencia;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error cargando las existencias en planta. " + ex.Message);
            }
        }

        /// <summary>
        /// Carga el precio del art�culo
        /// </summary>
        /// <param name="articulo"></param>
        private static void CargarPrecioArticulo(Articulo articulo)
        {
            try
            {
                List<NivelPrecio> niveles = NivelPrecio.ObtenerNivelesPrecio(articulo.Compania);
                NivelPrecio nivel = null;
                //nivel = niveles.Find(x => x.Nivel == NivelPrecio.Nivel);
                ConfigDocCia config = GestorPedido.PedidosCollection.ObtenerConfiguracionVenta(GlobalUI.ClienteActual.Compania);
                nivel = niveles.Find(x => x.Nivel == config.Nivel.Nivel);
                if (nivel != null)
                {
                    CargarPreciosArticuloContinuacion(articulo);
                }
                else
                {
                    ClienteCia cliente = GlobalUI.ClienteActual.ObtenerClienteCia(articulo.Compania);
                    config.Nivel = cliente.NivelPrecio;
                    CargarPreciosArticuloContinuacion(articulo);
                    //ShowMessage.MostrarConRetornoInfo(this, () =>
                    //{
                    //    ClienteCia cliente = GlobalUI.ClienteActual.ObtenerClienteCia(articulo.Compania);
                    //    config.Nivel = cliente.NivelPrecio;
                    //    CargarPreciosArticuloContinuacion(articulo);
                    //}, "No se encontr� el nivel de precios: " +
                    //        config.Nivel.Nivel + " en la compa��a: " + articulo.Compania + ". Se asignar� el nivel de precios del cliente.");
                }
            }
            catch
            {
                throw new Exception("Ocurri� un error al cargar el precio del art�culo " + articulo.Codigo + ".");
            }
        }

        /// <summary>
        /// Carga el precio del art�culo
        /// </summary>
        /// <param name="articulo"></param>
        private static void CargarPreciosArticuloContinuacion(Articulo articulo)
        {
            ConfigDocCia config = GestorPedido.PedidosCollection.ObtenerConfiguracionVenta(articulo.Compania);
            try
            {
                articulo.CargarPrecio(config.Nivel);
                if (FRdConfig.UsaEnvases && articulo.ArticuloEnvase != null)
                {
                    articulo.ArticuloEnvase.CargarPrecio(config.Nivel);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error cargando precios del art�culo. " + ex.Message);
            }
        }

        /// <summary>
        /// Cambio en la cantidad de bonificacion o descuento
        /// </summary>
        public static void CambiosBonificacionDescuento(Articulo pArticulo, decimal DescuentoA, decimal CantBonifAlmacen,decimal CantBonifDetalle)
        {
            try
            {
                bool estadoBonific = false;
                bool estadoDesc = false;
                int indiceBonifica = 0;
                int indiceDescuento = 0;
                DetallePedido LineaBonifica = null;
                DetallePedido LineaDescuento = null;

                //Pedido ped = GestorPedido.PedidosCollection.Buscar(pArticulo.Compania);
                GestorBonificacionDescuento gestor = new GestorBonificacionDescuento(PedidosCollection.Gestionados[0]);
                //if (ped == null)
                //    return;
                if (PedidosCollection.CambiarBonificacion)
                {
                    LineaBonifica = PedidosCollection.Gestionados[0].Detalles.BuscarBonificado(pArticulo.Codigo, out indiceBonifica);
                    estadoBonific = LineaBonifica != null;
                    if (!estadoBonific)
                    {
                        //this.LimpiarCambioBonificacion();
                    }

                }

                if (PedidosCollection.CambiarDescuento)
                {
                    LineaDescuento = PedidosCollection.Gestionados[0].Detalles.BuscarDescuento(pArticulo.Codigo, out indiceDescuento);
                    estadoDesc = LineaDescuento != null;
                    if (!estadoDesc && (LineaDescuento == null || LineaDescuento.RegaliaDescuentoLinea == null))
                    {
                        if (DescuentoA > 0)
                        {
                            estadoDesc = true;
                            LineaDescuento = GestorPedido.PedidosCollection.BuscarDetalle(pArticulo);
                            indiceDescuento = GestorPedido.PedidosCollection.BuscarIndiceDetalle(pArticulo);
                            LineaDescuento.Descuento = new Descuento();
                            LineaDescuento.Descuento.Monto = DescuentoA;
                            LineaDescuento.Descuento.Tipo = TipoDescuento.Porcentual;

                        }
                        //this.LimpiarCambioDescuento();
                    }
                }
                if ((estadoBonific && LineaBonifica != null && LineaBonifica.RegaliaBonificacion == null) ||
                    (estadoDesc && LineaDescuento != null && LineaDescuento.RegaliaDescuentoLinea == null && LineaDescuento.Descuento.Monto >= 0))
                {
                    DetallePedido detalle = GestorPedido.PedidosCollection.BuscarDetalle(pArticulo);
                    detalle.Articulo.Precio = new Precio(detalle.Precio.Maximo, detalle.Precio.Minimo, detalle.Precio.MargenUtilidad, detalle.Precio.PorcentajeRecargo);
                    gestor.CambiarBonificacionDescuento(detalle, indiceBonifica, indiceDescuento, estadoBonific, estadoDesc,
                                                        GestorPedido.PedidosCollection.Gestionados[0].Cliente, DescuentoA,
                                                        CantBonifAlmacen, CantBonifDetalle);
                    GestorPedido.PedidosCollection.SacarMontosTotales();
                }
                else
                {
                    //this.LimpiarCambioBonificacion();
                    //this.LimpiarCambioDescuento();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// M�todo que se encarga de obtener una lista de pedidos pendientes y armarlos en sentencias para sincronizarlos 
        /// con el servidor.
        /// </summary>
        /// <param name="cnx">Conexi�n a la base de datos.</param>
        /// <param name="sentencias">Referencia a las sentencias a enviar.</param>
        /// <param name="cantidadPedidos">Referencia al n�mero de pedidos pendientes.</param>
        public static void CrearSentenciasPedidos(SQLiteConnection cnx, ref Sentencias sentencias, ref int cantidadPedidos)
        {
            #region Columnas de las tablas.

            // Variable que lleva el nombre de las columnas a utilizar al ingresar el encabezado del pedido.

            string columnasEncPed = "PEDIDO,ESTADO,FECHA_PEDIDO,FECHA_PROMETIDA,FECHA_PROX_EMBARQU,EMBARCAR_A,DIREC_EMBARQUE,OBSERVACIONES,TOTAL_MERCADERIA,MONTO_ANTICIPO," +
                            "MONTO_FLETE,MONTO_SEGURO,MONTO_DOCUMENTACIO,TIPO_DESCUENTO1,TIPO_DESCUENTO2,MONTO_DESCUENTO1,MONTO_DESCUENTO2,PORC_DESCUENTO1,PORC_DESCUENTO2,TOTAL_IMPUESTO1,TOTAL_IMPUESTO2," +
                            "TOTAL_A_FACTURAR,PORC_COMI_COBRADOR,PORC_COMI_VENDEDOR,TOTAL_CANCELADO,TOTAL_UNIDADES,IMPRESO,FECHA_HORA,DESCUENTO_VOLUMEN,TIPO_PEDIDO,MONEDA_PEDIDO,VERSION_NP,AUTORIZADO,DOC_A_GENERAR," +
                            "CLASE_PEDIDO,MONEDA,NIVEL_PRECIO,COBRADOR,RUTA,USUARIO,CONDICION_PAGO,BODEGA,ZONA,VENDEDOR,CLIENTE,CLIENTE_DIRECCION,CLIENTE_CORPORAC,CLIENTE_ORIGEN," +
                            "PAIS,SUBTIPO_DOC_CXC,TIPO_DOC_CXC,BACKORDER,PORC_INTCTE,DESCUENTO_CASCADA,DIRECCION_FACTURA,TIPO_CAMBIO,FIJAR_TIPO_CAMBIO,ORIGEN_PEDIDO,DESC_DIREC_EMBARQUE,NOMBRE_CLIENTE," +
                            "TIPO_DOCUMENTO,FECHA_ULT_EMBARQUE,FECHA_ULT_CANCELAC,FECHA_ORDEN,ACTIVIDAD_COMERCIAL ";

            // Variable que lleva el nombre de las columnas a utilizar al ingresar una l�nea de detalle del pedido.
            string columnasDetalles = "PEDIDO,PEDIDO_LINEA,BODEGA,LOCALIZACION,ARTICULO,ESTADO," +
                            "FECHA_ENTREGA,LINEA_USUARIO,PRECIO_UNITARIO,CANTIDAD_PEDIDA,CANTIDAD_A_FACTURA,CANTIDAD_FACTURADA," +
                            "CANTIDAD_RESERVADA,CANTIDAD_BONIFICAD,CANTIDAD_CANCELADA,TIPO_DESCUENTO,MONTO_DESCUENTO," +
                            "PORC_DESCUENTO,DESCRIPCION,FECHA_PROMETIDA,TIPO_IMPUESTO1,TIPO_TARIFA1,PORC_IMPUESTO1,TIPO_IMPUESTO2,TIPO_TARIFA2,PORC_IMPUESTO2,CENTRO_COSTO,CUENTA_CONTABLE";

            // Variable que lleva el nombre de las columnas a utilizar al ingresar una l�nea de detalle del pedido
            // que no lleve centro costo.
            string columnasDetallesSinCentro = "PEDIDO,PEDIDO_LINEA,BODEGA,LOCALIZACION,ARTICULO,ESTADO," +
                       "FECHA_ENTREGA,LINEA_USUARIO,PRECIO_UNITARIO,CANTIDAD_PEDIDA,CANTIDAD_A_FACTURA,CANTIDAD_FACTURADA," +
                       "CANTIDAD_RESERVADA,CANTIDAD_BONIFICAD,CANTIDAD_CANCELADA,TIPO_DESCUENTO,MONTO_DESCUENTO," +
                       "PORC_DESCUENTO,DESCRIPCION,FECHA_PROMETIDA,TIPO_IMPUESTO1,TIPO_TARIFA1,PORC_IMPUESTO1,TIPO_IMPUESTO2,TIPO_TARIFA2,PORC_IMPUESTO2";

            // Variable que lleva el nombre de las columnas a utilizar al ingresar una l�nea de detalle bonificada del pedido.
            string columnasDetallesBonif = "PEDIDO,PEDIDO_LINEA,BODEGA,LOCALIZACION,ARTICULO,ESTADO," +
                            "FECHA_ENTREGA,LINEA_USUARIO,PRECIO_UNITARIO,CANTIDAD_PEDIDA,CANTIDAD_A_FACTURA,CANTIDAD_FACTURADA," +
                            "CANTIDAD_RESERVADA,CANTIDAD_BONIFICAD,CANTIDAD_CANCELADA,TIPO_DESCUENTO,MONTO_DESCUENTO," +
                            "PORC_DESCUENTO,DESCRIPCION,PEDIDO_LINEA_BONIF,FECHA_PROMETIDA,TIPO_IMPUESTO1,TIPO_TARIFA1,PORC_IMPUESTO1,TIPO_IMPUESTO2,TIPO_TARIFA2,PORC_IMPUESTO2,CENTRO_COSTO,CUENTA_CONTABLE";

            // Variable que lleva el nombre de las columnas a utilizar al ingresar una l�nea de detalle bonificada del pedido
            // que no lleve centro costo.
            string columnasDetallesBonifSinCentro = "PEDIDO,PEDIDO_LINEA,BODEGA,LOCALIZACION,ARTICULO,ESTADO," +
                            "FECHA_ENTREGA,LINEA_USUARIO,PRECIO_UNITARIO,CANTIDAD_PEDIDA,CANTIDAD_A_FACTURA,CANTIDAD_FACTURADA," +
                            "CANTIDAD_RESERVADA,CANTIDAD_BONIFICAD,CANTIDAD_CANCELADA,TIPO_DESCUENTO,MONTO_DESCUENTO," +
                            "PORC_DESCUENTO,DESCRIPCION,PEDIDO_LINEA_BONIF,FECHA_PROMETIDA,TIPO_IMPUESTO1,TIPO_TARIFA1,PORC_IMPUESTO1,TIPO_IMPUESTO2,TIPO_TARIFA2,PORC_IMPUESTO2";

            // Variable que lleva el nombre de las columnas a utilizar al ingresar una l�nea de detalle del pedido
            // que no lleve localizaci�n.
            string columnasDetallesSinLocalizacion = "PEDIDO,PEDIDO_LINEA,BODEGA,ARTICULO,ESTADO," +
                            "FECHA_ENTREGA,LINEA_USUARIO,PRECIO_UNITARIO,CANTIDAD_PEDIDA,CANTIDAD_A_FACTURA,CANTIDAD_FACTURADA," +
                            "CANTIDAD_RESERVADA,CANTIDAD_BONIFICAD,CANTIDAD_CANCELADA,TIPO_DESCUENTO,MONTO_DESCUENTO," +
                            "PORC_DESCUENTO,DESCRIPCION,FECHA_PROMETIDA,TIPO_IMPUESTO1,TIPO_TARIFA1,PORC_IMPUESTO1,TIPO_IMPUESTO2,TIPO_TARIFA2,PORC_IMPUESTO2,CENTRO_COSTO,CUENTA_CONTABLE";

            // Variable que lleva el nombre de las columnas a utilizar al ingresar una l�nea de detalle del pedido
            // que no lleve centro de costo ni localizaci�n.
            string columnasDetallesSinCentroSinLocalizacion = "PEDIDO,PEDIDO_LINEA,BODEGA,ARTICULO,ESTADO," +
                       "FECHA_ENTREGA,LINEA_USUARIO,PRECIO_UNITARIO,CANTIDAD_PEDIDA,CANTIDAD_A_FACTURA,CANTIDAD_FACTURADA," +
                       "CANTIDAD_RESERVADA,CANTIDAD_BONIFICAD,CANTIDAD_CANCELADA,TIPO_DESCUENTO,MONTO_DESCUENTO," +
                       "PORC_DESCUENTO,DESCRIPCION,FECHA_PROMETIDA,TIPO_IMPUESTO1,TIPO_TARIFA1,PORC_IMPUESTO1,TIPO_IMPUESTO2,TIPO_TARIFA2,PORC_IMPUESTO2";

            // Variable que lleva el nombre de las columnas a utilizar al ingresar una l�nea de detalle bonificada del pedido
            // que no lleve localizaci�n.
            string columnasDetallesBonifSinLocalizacion = "PEDIDO,PEDIDO_LINEA,BODEGA,ARTICULO,ESTADO," +
                            "FECHA_ENTREGA,LINEA_USUARIO,PRECIO_UNITARIO,CANTIDAD_PEDIDA,CANTIDAD_A_FACTURA,CANTIDAD_FACTURADA," +
                            "CANTIDAD_RESERVADA,CANTIDAD_BONIFICAD,CANTIDAD_CANCELADA,TIPO_DESCUENTO,MONTO_DESCUENTO," +
                            "PORC_DESCUENTO,DESCRIPCION,PEDIDO_LINEA_BONIF,FECHA_PROMETIDA,TIPO_IMPUESTO1,TIPO_TARIFA1,PORC_IMPUESTO1,TIPO_IMPUESTO2,TIPO_TARIFA2,PORC_IMPUESTO2,CENTRO_COSTO,CUENTA_CONTABLE";

            // Variable que lleva el nombre de las columnas a utilizar al ingresar una l�nea de detalle bonificada del pedido
            // que no lleve centro costo ni localizaci�n.
            string columnasDetallesBonifSinCentroSinLocalizacion = "PEDIDO,PEDIDO_LINEA,BODEGA,ARTICULO,ESTADO," +
                            "FECHA_ENTREGA,LINEA_USUARIO,PRECIO_UNITARIO,CANTIDAD_PEDIDA,CANTIDAD_A_FACTURA,CANTIDAD_FACTURADA," +
                            "CANTIDAD_RESERVADA,CANTIDAD_BONIFICAD,CANTIDAD_CANCELADA,TIPO_DESCUENTO,MONTO_DESCUENTO," +
                            "PORC_DESCUENTO,DESCRIPCION,PEDIDO_LINEA_BONIF,FECHA_PROMETIDA,TIPO_IMPUESTO1,TIPO_TARIFA1,PORC_IMPUESTO1,TIPO_IMPUESTO2,TIPO_TARIFA2,PORC_IMPUESTO2";

            string[] formats = {"M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt",
                                "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss",
                                "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt",
                                "M/d/yyyy h:mm", "M/d/yyyy h:mm",
                                "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm"};

            #endregion

            // Se inicializa la lista de sentencias.
            sentencias.listaSentencias = new List<Sentencias.Sentencia>();

            // StringBuilder con que se armar�n las sentencias a ejecutar.
            StringBuilder squery = null;

            // Variable que lleva la cuenta de la cantidad de par�metros ingresados por sentencia.
            int parametrosCount = 0;

            decimal montoNetoRedondeo = 0;

            decimal diferenciaDescuento1 = 0;

            bool redondearFactura = false;

            // Crear una sentencia por cada pedido pendiente.
            foreach (Pedido PedidoSeleccionado in Pedido.ObtenerPedidos(cnx))
            {
                // Se inicializan las variables de la sentencia por crear.
                Sentencias.Sentencia sentenciaPedido = new Sentencias.Sentencia();
                sentenciaPedido.Sqlparams = new List<Dictionary<string, object>>();
                sentenciaPedido.Sqls = new List<string>();
                sentenciaPedido.Tabla = PedidoSeleccionado.Compania;
                sentenciaPedido.TablaGlobal = false;
                sentenciaPedido.Tipo = TipoSentencia.InsertarPedido;

                // Se obtiene al cliente del pedido, se cargan sus datos y se asignan al pedido.
                Cliente cliente = new Cliente();
                cliente.Codigo = PedidoSeleccionado.Cliente;
                cliente.Compania = PedidoSeleccionado.Compania;
                cliente.Cargar(cnx);
                cliente.ObtenerClientesCia(cnx);

                PedidoSeleccionado.Configuracion.ClienteCia = cliente.ObtenerClienteCia(PedidoSeleccionado.Compania);
                PedidoSeleccionado.Configuracion.Compania = new Compania(PedidoSeleccionado.Compania);
                PedidoSeleccionado.Configuracion.Compania.Cargar(cnx);
                PedidoSeleccionado.Configuracion.Cargar(cnx);
                PedidoSeleccionado.Configuracion.ClienteCia.ObtenerDireccionesEntrega(cnx);

                redondearFactura = PedidoSeleccionado.Configuracion.Compania.UsaRedondeo && 
                                    PedidoSeleccionado.Configuracion.Compania.FactorRedondeo > 0;

                if (redondearFactura)
                {
                    montoNetoRedondeo = GestorUtilitario.TrunqueAFactor(PedidoSeleccionado.MontoNeto, 
                                            PedidoSeleccionado.Configuracion.Compania.FactorRedondeo);
                    diferenciaDescuento1 = PedidoSeleccionado.MontoNeto - montoNetoRedondeo;
                    diferenciaDescuento1 = Math.Abs(diferenciaDescuento1);
                }

                #region Agregar Encabezado

                // Si el pedido tiene Detalles se procede a crear las sentencias.
                if (PedidoSeleccionado.Detalles.Vacio())
                {
                    try
                    {
                        // Lista que contendr� los valores de los par�metros a pasar del pedido.
                        List<object> parametersValues = new List<object>();

                        string fecha1 = PedidoSeleccionado.FechaRealizacion.Year + "-";
                        if (PedidoSeleccionado.FechaRealizacion.Month < 10) fecha1 += "0" + PedidoSeleccionado.FechaRealizacion.Month + "-";
                        else fecha1 += PedidoSeleccionado.FechaRealizacion.Month + "-";

                        if (PedidoSeleccionado.FechaRealizacion.Day < 10) fecha1 += "0" + PedidoSeleccionado.FechaRealizacion.Day + "T00:00:00";
                        else fecha1 += PedidoSeleccionado.FechaRealizacion.Day + "T00:00:00";//PedidoSeleccionado.FechaRealizacion

                        string fecha2 = PedidoSeleccionado.FechaEntrega.Date.Year + "-";


                        if (PedidoSeleccionado.FechaEntrega.Date.Month < 10) fecha2 += "0" + PedidoSeleccionado.FechaEntrega.Date.Month + "-";
                        else fecha2 += +PedidoSeleccionado.FechaEntrega.Date.Month + "-";

                        if (PedidoSeleccionado.FechaEntrega.Date.Day < 10) fecha2 += "0" + PedidoSeleccionado.FechaEntrega.Date.Day + "T00:00:00";
                        else fecha2 += PedidoSeleccionado.FechaEntrega.Date.Day + "T00:00:00";//PedidoSeleccionado.FechaEntrega

                        // Se cargan los detalles del pedido.
                        PedidoSeleccionado.ObtenerDetalles(cnx);

                        // Se agregan todos los par�metros.

                        // Se agrega el c�digo del consecutivo del pedido para referencia en el Administrador.
                        parametersValues.Add(PedidoSeleccionado.Codigo);

                        parametersValues.Add(PedidoSeleccionado.Numero);    // PEDIDO
                        parametersValues.Add("N");    // ESTADO
                        parametersValues.Add(fecha1);    // FECHA_PEDIDO
                        parametersValues.Add(fecha2);    // FECHA_PROMETIDA
                        parametersValues.Add(fecha1);    // FECHA_PROX_EMBARQU
                        parametersValues.Add(cliente.Nombre);    // EMBARCAR_A
                        parametersValues.Add(PedidoSeleccionado.DireccionEntrega);    // DIREC_EMBARQUE
                        parametersValues.Add(PedidoSeleccionado.Notas);    // OBSERVACIONES
                        parametersValues.Add(PedidoSeleccionado.MontoBruto + PedidoSeleccionado.MontoBrutoBonificaciones);    // TOTAL_MERCADERIA
                        parametersValues.Add(0);    // MONTO_ANTICIPO
                        parametersValues.Add(0);    // MONTO_FLETE
                        parametersValues.Add(0);    // MONTO_SEGURO
                        parametersValues.Add(0);    // MONTO_DOCUMENTACIO
                        parametersValues.Add("P");    // TIPO_DESCUENTO1
                        parametersValues.Add("P");    // TIPO_DESCUENTO2
                        parametersValues.Add(redondearFactura ? Decimal.Round((PedidoSeleccionado.MontoDescuento1 + diferenciaDescuento1), 2) : PedidoSeleccionado.MontoDescuento1);    // MONTO_DESCUENTO1
                        parametersValues.Add(PedidoSeleccionado.MontoDescuento2);    // MONTO_DESCUENTO2
                        parametersValues.Add(PedidoSeleccionado.PorcentajeDescuento1);    // PORC_DESCUENTO1
                        parametersValues.Add(PedidoSeleccionado.PorcentajeDescuento2);    // PORC_DESCUENTO2
                        parametersValues.Add(PedidoSeleccionado.Impuesto.MontoImpuesto1);    // TOTAL_IMPUESTO1
                        parametersValues.Add(PedidoSeleccionado.Impuesto.MontoImpuesto2);    // TOTAL_IMPUESTO2
                        parametersValues.Add(redondearFactura ? montoNetoRedondeo : PedidoSeleccionado.MontoNeto);    // TOTAL_A_FACTURAR
                        parametersValues.Add(0);    // PORC_COMI_COBRADOR
                        parametersValues.Add(0);    // PORC_COMI_VENDEDOR
                        parametersValues.Add(0);    // TOTAL_CANCELADO
                        parametersValues.Add(PedidoSeleccionado.Detalles.TotalArticulos);    // TOTAL_UNIDADES  
                                  
                        if (PedidoSeleccionado.Impreso)
                            parametersValues.Add("S");    // IMPRESO
                        else
                            parametersValues.Add("N");    // IMPRESO

                        parametersValues.Add(PedidoSeleccionado.FechaRealizacion.ToString());    // FECHA_HORA
                        parametersValues.Add(PedidoSeleccionado.MontoBrutoBonificaciones);    // DESCUENTO_VOLUMEN
                        parametersValues.Add("N");    // TIPO_PEDIDO
                        parametersValues.Add(PedidoSeleccionado.Configuracion.ClienteCia.Moneda.ToString().Substring(0, 1));    // 
                        parametersValues.Add(PedidoSeleccionado.Configuracion.Nivel.Version);    // VERSION_NP
                        parametersValues.Add("N");    // AUTORIZADO
                        parametersValues.Add("F");    // DOC_A_GENERAR
                        parametersValues.Add("N");    // CLASE_PEDIDO
                        parametersValues.Add(PedidoSeleccionado.Configuracion.ClienteCia.Moneda.ToString().Substring(0, 1));    // MONEDA
                        parametersValues.Add(PedidoSeleccionado.NivelPrecio);    // NIVEL_PRECIO                                
                        parametersValues.Add(PedidoSeleccionado.Configuracion.ClienteCia.Cobrador);    // COBRADOR
                        parametersValues.Add(PedidoSeleccionado.Configuracion.ClienteCia.Ruta);    // RUTA
                        parametersValues.Add(PedidoSeleccionado.Usuario);    // USUARIO
                        parametersValues.Add(PedidoSeleccionado.Configuracion.ClienteCia.CondicionPago);    // CONDICION_PAGO
                        parametersValues.Add(PedidoSeleccionado.Bodega);    // BODEGA
                        parametersValues.Add(PedidoSeleccionado.Configuracion.ClienteCia.Zona);    // ZONA
                        parametersValues.Add(PedidoSeleccionado.Configuracion.ClienteCia.Vendedor);    // VENDEDOR
                        parametersValues.Add(PedidoSeleccionado.Cliente);    // CLIENTE
                        parametersValues.Add(PedidoSeleccionado.Cliente);    // CLIENTE_DIRECCION
                        parametersValues.Add(PedidoSeleccionado.Cliente);    // CLIENTE_CORPORAC
                        parametersValues.Add(PedidoSeleccionado.Cliente);    // CLIENTE_ORIGEN
                        parametersValues.Add(PedidoSeleccionado.Configuracion.ClienteCia.Pais); //PAIS
                        parametersValues.Add(0);    // SUBTIPO_DOC_CXC
                        parametersValues.Add("FAC");    // TIPO_DOC_CXC
                        parametersValues.Add(PedidoSeleccionado.Backorder);    // BACKORDER
                        parametersValues.Add(0);    // PORC_INTCTE

                        if (PedidoSeleccionado.DescuentoCascada)
                            parametersValues.Add("S");    // DESCUENTO_CASCADA
                        else
                            parametersValues.Add("N");    // DESCUENTO_CASCADA

                        parametersValues.Add(cliente.Direccion);    // DIRECCION_FACTURA
                        parametersValues.Add(0);    // TIPO_CAMBIO
                        parametersValues.Add("N");    // FIJAR_TIPO_CAMBIO
                        parametersValues.Add("F");    // ORIGEN_PEDIDO

                        string direccionEntrega = PedidoSeleccionado.Configuracion.ClienteCia.DireccionesEntrega.Find(
                            x => x.Codigo == PedidoSeleccionado.DireccionEntrega).Descripcion;

                        if (string.IsNullOrEmpty(direccionEntrega))
                        {
                            direccionEntrega = PedidoSeleccionado.Configuracion.ClienteCia.DireccionesEntrega.Find(
                                x => x.Codigo == PedidoSeleccionado.DireccionEntrega).Codigo;
                        }

                        if (string.IsNullOrEmpty(direccionEntrega)){
                            direccionEntrega = PedidoSeleccionado.Configuracion.ClienteCia.DireccionEntregaDefault;
                            if (direccionEntrega == null) direccionEntrega = "ND";
                        }

                        parametersValues.Add(direccionEntrega); // DESC_DIREC_EMBARQUE

                        parametersValues.Add(cliente.Nombre);    // NOMBRE_CLIENTE
                        parametersValues.Add("P");    // TIPO_DOCUMENTO

                        string fechaGestor = GestorUtilitario.GetMinDate().Year + "-";

                        if (GestorUtilitario.GetMinDate().Month < 10) fechaGestor += "0" + GestorUtilitario.GetMinDate().Month + "-";
                        else fechaGestor += GestorUtilitario.GetMinDate().Date.Month + "-";

                        if (GestorUtilitario.GetMinDate().Day < 10) fechaGestor += "0" + GestorUtilitario.GetMinDate().Day + "T00:00:00";
                        else fechaGestor += GestorUtilitario.GetMinDate().Date.Day + "T00:00:00";

                        parametersValues.Add(fechaGestor);//FECHA_ULT_EMBARQUE
                        parametersValues.Add(fechaGestor);//FECHA_ULT_CANCELAC
                        parametersValues.Add(fecha1);//FECHA_ORDEN
                        parametersValues.Add(PedidoSeleccionado.Configuracion.Compania.ActividadComercial);//ACTIVIDAD_COMERCIAL


                        // Se obtiene la cantidad de par�metros.
                        parametrosCount = parametersValues.Count;

                        // Se crea la Sentencia.

                        // Se inicializa la sentencia.
                        squery = new StringBuilder();
                        squery.Append(string.Format("INSERT INTO {0}.PEDIDO ( ", PedidoSeleccionado.Compania));
                        squery.Append(columnasEncPed);
                        squery.Append(") VALUES ( ");

                        // Variable que tendr� referencia de los par�metros con su llave para la sentencia del encabezado del pedido.
                        Dictionary<string, object> values = new Dictionary<string, object>();

                        // Se forma la sentencia agregando una llave y valor por cada par�metro ingresado.
                        for (int i = 0; i < parametrosCount; i++)
                        {
                            if ((i + 1) == parametrosCount)
                                squery.Append("@" + i + ")");
                            else
                                squery.Append("@" + i + ",");
                            bool res;
                            object obj = new object();
                            obj = parametersValues[i];

                            DateTime datetemp;
                            Decimal dectemp;

                            // Si se puede convertir en bool.
                            if (bool.TryParse(obj.ToString(), out res))
                            {
                                obj = Convert.ToBoolean(obj) ? 1 : 0;
                            }

                            // Si es de tipo DateTime se castea.
                            if (obj.GetType() == typeof(DateTime))
                            {
                                DateTime dtemp;

                                if (DateTime.TryParseExact(obj.ToString(), formats, new CultureInfo("en-US"),
                                        DateTimeStyles.None, out dtemp))
                                    values.Add("@" + i.ToString(), dtemp);
                                //datetemp = (DateTime)obj;
                                //values.Add("@" + i.ToString(), datetemp);
                            }
                            // Si es de tipo Decimal se castea para cambiar la ',' por '.' 
                            // permitiendo hacer la conversi�n del lado del servidor.
                            else if (obj.GetType() == typeof(Decimal))
                            {
                                dectemp = (Decimal)obj;
                                obj = Convert.ToString(dectemp).Replace(',', '.');
                                values.Add("@" + i.ToString(), obj);
                            }
                            // Ning�n otro tipo importa por lo que se pasa a string.
                            else
                            {
                                obj = obj.ToString();
                                values.Add("@" + i.ToString(), obj);
                            }
                        }

                        // Se agrega la sentencia a la lista de sentencias del pedido.
                        sentenciaPedido.Sqls.Add(squery.ToString());

                        // Se agrega la colecci�n de par�metros a la lista de par�metros del pedido.
                        sentenciaPedido.Sqlparams.Add(values);

                        #endregion

                        // Se procede a agregar los Detalles del pedido.                      
                        foreach (DetallePedido detalle in PedidoSeleccionado.Detalles.Lista)
                        {
                            #region Agregar Detalle

                            // Se reutiliza la lista de parametros.
                            parametersValues.Clear();

                            // Se limpia la memoria utilizada.
                            GC.Collect(GC.MaxGeneration);

                            // Se agregan todos los par�metros del detalle.

                            parametersValues.Add(PedidoSeleccionado.Numero);    //PEDIDO
                            parametersValues.Add(detalle.NumeroLinea);    //PEDIDO_LINEA
                            parametersValues.Add(detalle.Articulo.Bodega.Codigo);    //BODEGA

                            if (PedidoSeleccionado.Configuracion.Compania.UsaLocalizacion)
                            {
                                parametersValues.Add(detalle.Articulo.Bodega.Localizacion);    //LOCALIZACION
                            }

                            parametersValues.Add(detalle.Articulo.Codigo);    //ARTICULO
                            parametersValues.Add("N");    //ESTADO
                            parametersValues.Add(PedidoSeleccionado.FechaRealizacion.Date);    //FECHA_ENTREGA
                            parametersValues.Add(detalle.NumeroLinea - 1);    //LINEA_USUARIO
                            parametersValues.Add(detalle.Precio.Maximo);    //PRECIO_UNITARIO
                            parametersValues.Add(decimal.Round(detalle.TotalAlmacen, GestorUtilitario.CantidadDecimales));    //CANTIDAD_PEDIDA
                            parametersValues.Add(decimal.Round(detalle.TotalAlmacen, GestorUtilitario.CantidadDecimales));    //CANTIDAD_A_FACTURAR
                            parametersValues.Add(0);    //CANTIDAD_FACTURADA
                            parametersValues.Add(0);    //CANTIDAD_RESERVADA
                            parametersValues.Add(0);    //CANTIDAD_BONIFICADA
                            parametersValues.Add(0);    //CANTIDAD_CANCELADA
                            parametersValues.Add(detalle.Descuento.Tipo.ToString().Substring(0, 1));    //TIPO_DESCUENTO
                            parametersValues.Add(detalle.MontoDescuento);    //MONTO_DESCUENTO
                            parametersValues.Add(detalle.Descuento.Monto);    //PORCENTAJE_DESCUENTO
                            parametersValues.Add(detalle.Articulo.Descripcion);    //DESCRIPCION
                            parametersValues.Add(PedidoSeleccionado.FechaEntrega.Date);    //FECHA_ENTREGA

                            //Jarbis Ajustes IVA >>>>>
                            parametersValues.Add(detalle.Tipo_Impuesto1);//TIPO_IMPUESTO1
                            parametersValues.Add(detalle.Tipo_Tarifa1);//TIPO_TARIFA1
                            parametersValues.Add(detalle.Porc_Impuesto1);//PORC_IMPUESTO1

                            parametersValues.Add(detalle.Tipo_Impuesto2);//TIPO_IMPUESTO2
                            parametersValues.Add(detalle.Tipo_Tarifa2);//TIPO_TARIFA2
                            parametersValues.Add(detalle.Porc_Impuesto2);//PORC_IMPUESTO2
                            //Jarbis Ajustes IVA <<<<<

                            if (!string.IsNullOrEmpty(detalle.CentroCostoVenta) && 
                                !string.IsNullOrEmpty(detalle.CuentaContableCostoVenta))
                            {
                                parametersValues.Add(detalle.CentroCostoVenta);    //CENTRO_COSTO
                                parametersValues.Add(detalle.CuentaContableCostoVenta);    //CUENTA_CONTABLE
                            }

                            // Se obtiene la cantidad de par�metros.
                            parametrosCount = parametersValues.Count;

                            // Se crea la Sentencia.

                            // Se inicializa la sentencia.
                            squery = new StringBuilder();
                            squery.Append(string.Format("INSERT INTO {0}.PEDIDO_LINEA ( ", PedidoSeleccionado.Compania));

                            // Se utiliza la lista de columnas para el detalle apropiada seg�n el tipo de detalle que es.
                            if (PedidoSeleccionado.Configuracion.Compania.UsaLocalizacion)
                            {
                                if (!string.IsNullOrEmpty(detalle.CentroCostoVenta) && !string.IsNullOrEmpty(detalle.CuentaContableCostoVenta))
                                    squery.Append(columnasDetalles);
                                else
                                    squery.Append(columnasDetallesSinCentro);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(detalle.CentroCostoVenta) && !string.IsNullOrEmpty(detalle.CuentaContableCostoVenta))
                                    squery.Append(columnasDetallesSinLocalizacion);
                                else
                                    squery.Append(columnasDetallesSinCentroSinLocalizacion);
                            }

                            squery.Append(") VALUES ( ");

                            // Variable que tendr� referencia de los par�metros con su llave para la sentencia del detalle del pedido.
                            Dictionary<string, object> valuesDetalles = new Dictionary<string, object>();

                            for (int i = 0; i < parametrosCount; i++)
                            {
                                if ((i + 1) == parametrosCount)
                                    squery.Append("@" + i + ")");
                                else
                                    squery.Append("@" + i + ",");
                                bool res;
                                object obj = new object();
                                obj = parametersValues[i];
                                /*
                                 * jguzmanc
                                 * 15/02/2019
                                 * Caso CO7-04199-GM0H
                                 * Problema: Cuando se edita un pedido desde el PEM y se carga el pedido llega con la localizaci�n 
                                 * respectiva, pero sino se edita llega con ND
                                 * Soluci�n: Se cambia para que vaya nulo y se pueda hacer como FA.
                                 * Inicio
                                 */
                                if (obj != null)
                                    if (bool.TryParse(obj.ToString(), out res))
                                    {
                                        obj = Convert.ToBoolean(obj) ? 1 : 0;
                                    }
                                    else
                                    {
                                        obj = obj.ToString();
                                    }
                                if (obj != null)
                                {
                                    DateTime dtemp;
                                    if (DateTime.TryParseExact(obj.ToString(), formats, new CultureInfo("en-US"),
                                        DateTimeStyles.None, out dtemp))
                                        valuesDetalles.Add("@" + i.ToString(), dtemp);
                                    else
                                        valuesDetalles.Add("@" + i.ToString(), obj);
                                }
                                else
                                {
                                    valuesDetalles.Add("@" + i.ToString(), obj);
                                }
                                //fin caso CO7-04199-GM0H
                            }

                            // Se agrega la sentencia a la lista de sentencias del pedido.
                            sentenciaPedido.Sqls.Add(squery.ToString());

                            // Se agrega la colecci�n de par�metros a la lista de par�metros del pedido.
                            sentenciaPedido.Sqlparams.Add(valuesDetalles);

                            #endregion

                            #region Agregar L�nea bonificada

                            // Si el detalle actual tiene una l�nea bonificada se procede a crear una sentencia de esta.
                            if (detalle.LineaBonificada != null)
                            {
                                // Se reutiliza la lista de parametros.
                                parametersValues.Clear();

                                // Se limpia la memoria utilizada.
                                GC.Collect(GC.MaxGeneration);

                                // Se agregan todos los par�metros de la l�nea bonificada.

                                parametersValues.Add(PedidoSeleccionado.Numero);    //PEDIDO
                                parametersValues.Add(detalle.LineaBonificada.NumeroLinea);    //PEDIDO_LINEA
                                parametersValues.Add(detalle.LineaBonificada.Articulo.Bodega.Codigo);    //BODEGA

                                if (PedidoSeleccionado.Configuracion.Compania.UsaLocalizacion)
                                {
                                    parametersValues.Add(detalle.LineaBonificada.Articulo.Bodega.Localizacion); //LOCALIZACION
                                }
                                    
                                parametersValues.Add(detalle.LineaBonificada.Articulo.Codigo);    //ARTICULO
                                parametersValues.Add("N");    //ESTADO
                                parametersValues.Add(PedidoSeleccionado.FechaRealizacion.Date);    //FECHA_ENTREGA
                                parametersValues.Add(detalle.LineaBonificada.NumeroLinea - 1);    //LINEA_USUARIO
                                parametersValues.Add(detalle.LineaBonificada.Precio.Maximo);    //PRECIO_UNITARIO
                                parametersValues.Add(0);    //CANTIDAD_PEDIDA
                                parametersValues.Add(0);    //CANTIDAD_A_FACTURAR
                                parametersValues.Add(0);    //CANTIDAD_FACTURADA
                                parametersValues.Add(0);    //CANTIDAD_RESERVADA
                                parametersValues.Add(decimal.Round(detalle.LineaBonificada.TotalAlmacen, GestorUtilitario.CantidadDecimales));    //CANTIDAD_BONIFICADA
                                parametersValues.Add(0);    //CANTIDAD_CANCELADA
                                parametersValues.Add(detalle.LineaBonificada.Descuento.Tipo.ToString().Substring(0, 1));    //TIPO_DESCUENTO
                                parametersValues.Add(detalle.LineaBonificada.MontoDescuento);    //MONTO_DESCUENTO
                                parametersValues.Add(detalle.LineaBonificada.Descuento.Monto);    //PORCENTAJE_DESCUENTO
                                parametersValues.Add(detalle.LineaBonificada.Articulo.Descripcion);    //DESCRIPCION
                                parametersValues.Add(Convert.ToInt32(detalle.LineaBonificada.LineaBonifica));    //PEDIDO_LINEA_BONIF
                                parametersValues.Add(PedidoSeleccionado.FechaEntrega.Date);    //FECHA_ENTREGA

                                if (!string.IsNullOrEmpty(detalle.LineaBonificada.CentroCostoVenta) && 
                                    !string.IsNullOrEmpty(detalle.LineaBonificada.CuentaContableCostoVenta))
                                {
                                    parametersValues.Add(detalle.LineaBonificada.CentroCostoVenta);    //CENTRO_COSTO
                                    parametersValues.Add(detalle.LineaBonificada.CuentaContableCostoVenta);    //CUENTA_CONTABLE
                                }

                                // Se obtiene la cantidad de par�metros.
                                parametrosCount = parametersValues.Count;

                                // Se crea la Sentencia.

                                // Se inicializa la sentencia.
                                squery = new StringBuilder();
                                squery.Append(string.Format("INSERT INTO {0}.PEDIDO_LINEA ( ", PedidoSeleccionado.Compania));

                                // Se utiliza la lista de columnas para el detalle bonificado apropiada seg�n el tipo de que es.
                                if (PedidoSeleccionado.Configuracion.Compania.UsaLocalizacion)
                                {
                                    if (!string.IsNullOrEmpty(detalle.LineaBonificada.CentroCostoVenta) && !string.IsNullOrEmpty(detalle.LineaBonificada.CuentaContableCostoVenta))
                                        squery.Append(columnasDetallesBonif);
                                    else
                                        squery.Append(columnasDetallesBonifSinCentro);
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(detalle.LineaBonificada.CentroCostoVenta) && !string.IsNullOrEmpty(detalle.LineaBonificada.CuentaContableCostoVenta))
                                        squery.Append(columnasDetallesBonifSinLocalizacion);
                                    else
                                        squery.Append(columnasDetallesBonifSinCentroSinLocalizacion);
                                }

                                squery.Append(") VALUES ( ");

                                // Variable que tendr� referencia de los par�metros con su llave para la sentencia del detalle bonificado del pedido.
                                Dictionary<string, object> valuesDetallesBonif = new Dictionary<string, object>();

                                // Se forma la sentencia agregando una llave y valor por cada par�metro ingresado.
                                for (int i = 0; i < parametrosCount; i++)
                                {
                                    if ((i + 1) == parametrosCount)
                                        squery.Append("@" + i + ")");
                                    else
                                        squery.Append("@" + i + ",");
                                    bool res;
                                    object obj = new object();
                                    obj = parametersValues[i];

                                    DateTime datetemp;
                                    Decimal dectemp;

                                    // Si se puede convertir en bool.
                                    if (bool.TryParse(obj.ToString(), out res))
                                    {
                                        obj = Convert.ToBoolean(obj) ? 1 : 0;
                                    }

                                    // Si es de tipo DateTime se castea.
                                    if (obj.GetType() == typeof(DateTime))
                                    {
                                        datetemp = (DateTime)obj;
                                        valuesDetallesBonif.Add("@" + i.ToString(), datetemp);
                                    }
                                    // Si es de tipo Decimal se castea para cambiar la ',' por '.' 
                                    // permitiendo hacer la conversi�n del lado del servidor.
                                    else if (obj.GetType() == typeof(Decimal))
                                    {
                                        dectemp = (Decimal)obj;
                                        obj = Convert.ToString(dectemp).Replace(',', '.');
                                        valuesDetallesBonif.Add("@" + i.ToString(), obj);
                                    }
                                    // Ning�n otro tipo importa por lo que se pasa a string.
                                    else
                                    {
                                        obj = obj.ToString();
                                        valuesDetallesBonif.Add("@" + i.ToString(), obj);
                                    }
                                }

                                // Se agrega la sentencia a la lista de sentencias del pedido.
                                sentenciaPedido.Sqls.Add(squery.ToString());

                                // Se agrega la colecci�n de par�metros a la lista de par�metros del pedido.
                                sentenciaPedido.Sqlparams.Add(valuesDetallesBonif);
                            }

                            #endregion

                        }

                        // Se agrega la sentencia completa del pedido a la lista de sentencias.
                        sentencias.listaSentencias.Add(sentenciaPedido);
                    }
                    catch (Exception ex)
                    {
                        // Alg�n pedido no puede ser procesado, se omite que se present� alg�n error para sincronizar todos los que
                        // si puedan ser convertidos en sentencias.

                        //throw new Exception("No es posible crear una sentencia para el pedido: " + PedidoSeleccionado.Numero + 
                        //    ", se present� un error:" + ex.Message);
                    }
                }

                // Se aumenta la cuenta de pedidos procesados.
                cantidadPedidos++;
            }
        }

        #endregion
    }
}