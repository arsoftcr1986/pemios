﻿using System;
using System.Data;
using System.Data.SQLiteBase;
using System.Globalization;
//using Softland.ERP.FR.Mobile.Cls.Documentos.FRInventario;
using System.Collections.Generic;
using BI.Shared;

namespace Pedidos.Core
{
    /// <summary>
    /// Representa la bodega con existencia para un artículo. 
    /// </summary>
    public class Bodega 
    {

        #region Variables y Propiedades

        private string codigo = string.Empty;        
        /// <summary>
        /// Código de la Bodega.
        /// </summary>
        public string Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

        private string localizacion = string.Empty;
        /// <summary>
        /// Localización de la Bodega.
        /// </summary>
        public string Localizacion
        {
            get { return localizacion; }
            set { localizacion = value; }
        }

        private decimal existencia = decimal.Zero;
        /// <summary>
        /// Existencia de un determinado artículo en la bodega
        /// </summary>
        public decimal Existencia
        {
            get { return existencia; }
            //set { existencia = value; }
        }
        #endregion

        #region Constructor

        /// <summary>
        /// constructor de la bodeda
        /// </summary>
        /// <param name="bodega">codigo de la bodega a asociar</param>
        public Bodega(string bodega)
        {
            codigo = bodega;
            existencia = decimal.MinValue;     
        }

        /// <summary>
        /// constructor de la bodega
        /// </summary>
        /// <param name="articulo">Articulo de la bodega</param>
        /// <param name="bodega">Codigo de la Bodega</param>
        public Bodega(Articulo articulo, string bodega)
        {
            CargarBodegaArticulo(articulo, bodega);
        }

        /// <summary>
        /// Carga las existencias de un articulo que se encuentra en una bodega
        /// </summary>
        /// <param name="articulo">Articulo de la Bodega</param>
        /// <param name="bodega">Codigo de la Bodega</param>
        private void CargarBodegaArticulo(Articulo articulo, string bodega)
        {
            string sentencia =
                @" SELECT BODEGA, EXISTENCIA FROM " + Table.ERPADMIN_ARTICULO_EXISTENCIA  +
                @" WHERE ARTICULO = @ARTICULO" +
                @" AND UPPER(COMPANIA) = @COMPANIA" +
                @" AND BODEGA = @BODEGA";

            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                new SQLiteParameter("@ARTICULO",articulo.Codigo),
                new SQLiteParameter("@COMPANIA", articulo.Compania.ToUpper()),
                new SQLiteParameter("@BODEGA", bodega)});

            SQLiteDataReader reader = null;           

            try
            {
                reader = GestorDatos.EjecutarConsulta(sentencia, parametros);

                while (reader.Read())
                {
                    this.codigo = reader.GetString(0);
                    this.existencia = reader.GetDecimal(1);
                }
            }
            catch (Exception e)
            {
                string error = e.Message;
                this.codigo = "";
                this.existencia = decimal.MinValue;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        public object Clone()
        {
            Bodega clon = new Bodega(codigo);
            clon.existencia = this.existencia;
            return clon;
        }
        /// <summary>
        /// constructor de la bodeda
        /// </summary>
        public Bodega()
        {
            existencia = decimal.MinValue;
        }
        #endregion

        #region Logica

        /// <summary>
        /// Verificar si hay suficiente existencia
        /// </summary>
        /// <param name="cantidadSolicitada">cantidad a solicitar</param>
        /// <returns>si hay existencia</returns>
        public bool SuficienteExistencia(decimal cantidadSolicitada)
        {
            return ( existencia >= cantidadSolicitada);               
        }

        /// <summary>
        /// Sentencia que actualiza la existencia de los articulos en el servidor
        /// </summary>
        /// <param name="compania"></param>
        /// <param name="articulo"></param>
        /// <returns></returns>
        public string SentenciaActualizacion(string compania, string articulo)
        {
            return
                 " UPDATE %CIA.ARTICULO_EXISTENCIA " +
                @" SET EXISTENCIA = " + existencia.ToString(CultureInfo.InvariantCulture) + 
                @" WHERE ARTICULO = '" + articulo + "' " +
                @" AND COMPANIA = '" + compania + "' " +
                @" AND BODEGA   = '" + codigo + "'";
        }
        /// <summary>
        /// Cargar la existencia del articulo
        /// </summary>
        /// <param name="compania">compania asociada al articulo</param>
        /// <param name="articulo">codigo del articulo</param>
        public void CargarExistencia(string compania, string articulo)
        {
            ObtenerExistencia(compania, articulo);
        }

        /// <summary>
        /// Cargar la existencia del articulo
        /// </summary>
        /// <param name="compania">compania asociada al articulo</param>
        /// <param name="articulo">codigo del articulo</param>
        public void CargarExistencia(string compania, string articulo,SQLiteConnection cnx)
        {
            ObtenerExistencia(compania, articulo,cnx);
        }

        /// <summary>
        /// Cargar la existencia del articulo, cuando utiliza lotes
        /// </summary>
        /// <param name="compania">compania asociada al articulo</param>
        /// <param name="articulo">codigo del articulo</param>
        public void CargarExistenciaLote(string compania, string articulo)
        {
            ObtenerExistenciaLotes(compania, articulo);
        }

        #endregion

        #region Acceso Datos

        /// <summary>
        /// Obtiene las existencias del artículo en bodega para determinada compañía.
        /// </summary>
        /// <param name="compania">compania a obtener existencia</param>
        /// <param name="articulo">codigo del articulo a consultar</param>
        public string Obtenerlocalizacion(string compania, string articulo)
        {
            string result = "ND";
            SQLiteDataReader reader = null;
            try
            {
                string sentencia =
                @" SELECT LOCALIZACION FROM " + Table.ERPADMIN_ARTICULO_EXISTENCIA_LOTE +
                @" WHERE ARTICULO = @ARTICULO" +
                @" AND UPPER(COMPANIA) = @COMPANIA" +
                @" AND BODEGA = @BODEGA" +
                @" ORDER BY EXISTENCIA,LOCALIZACION DESC";

                SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                new SQLiteParameter("@ARTICULO",articulo),
                new SQLiteParameter("@COMPANIA", compania.ToUpper()),
                new SQLiteParameter("@BODEGA", codigo)});

                reader = GestorDatos.EjecutarConsulta(sentencia, parametros);

                while (reader.Read())
                {
                    result = reader.GetString(0);
                    break;
                }                
            }
            catch
            {
                throw new Exception("Error obteniendo la localización del artículo " + articulo + " bodega " + codigo + ".");
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }
            }
            return result;           
        }

        /// <summary>
        /// Obtiene las existencias del artículo en bodega para determinada compañía.
        /// </summary>
        /// <param name="compania">compania a obtener existencia</param>
        /// <param name="articulo">codigo del articulo a consultar</param>
        public string Obtenerlocalizacion(string compania, string articulo,SQLiteConnection cnx)
        {
            string result = "ND";
            SQLiteDataReader reader = null;
            try
            {
                string sentencia =
                @" SELECT LOCALIZACION FROM " + Table.ERPADMIN_ARTICULO_EXISTENCIA_LOTE +
                @" WHERE ARTICULO = @ARTICULO" +
                @" AND UPPER(COMPANIA) = @COMPANIA" +
                @" AND BODEGA = @BODEGA" +
                @" ORDER BY EXISTENCIA,LOCALIZACION DESC";

                SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                new SQLiteParameter("@ARTICULO",articulo),
                new SQLiteParameter("@COMPANIA", compania.ToUpper()),
                new SQLiteParameter("@BODEGA", codigo)});

                reader = GestorDatos.EjecutarConsulta(sentencia, parametros,cnx);

                while (reader.Read())
                {
                    result = reader.GetString(0);
                    break;
                }
            }
            catch
            {
                throw new Exception("Error obteniendo la localización del artículo " + articulo + " bodega " + codigo + ".");
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }
            }
            return result;
        }

        /// <summary>
        /// Obtiene las existencias del artículo en bodega para determinada compañía.
        /// </summary>
        /// <param name="compania">compania a obtener existencia</param>
        /// <param name="articulo">codigo del articulo a consultar</param>
        public List<string> Obtenerlocalizaciones(string compania, string articulo, SQLiteConnection cnx)
        {
            List<string> result = new List<string>();
            SQLiteDataReader reader = null;
            try
            {
                string sentencia =
                @" SELECT LOCALIZACION FROM " + Table.ERPADMIN_ARTICULO_EXISTENCIA_LOTE +
                @" WHERE ARTICULO = @ARTICULO" +
                @" AND UPPER(COMPANIA) = @COMPANIA" +
                @" AND BODEGA = @BODEGA" +
                @" ORDER BY EXISTENCIA,LOCALIZACION DESC";

                SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                new SQLiteParameter("@ARTICULO",articulo),
                new SQLiteParameter("@COMPANIA", compania.ToUpper()),
                new SQLiteParameter("@BODEGA", codigo)});

                reader = GestorDatos.EjecutarConsulta(sentencia, parametros, cnx);

                while (reader.Read())
                {
                    result.Add(reader.GetString(0));
                    
                }
                if (result.Count == 0)
                {
                    result.Add("ND");
                }
            }
            catch
            {
                throw new Exception("Error obteniendo la localización del artículo " + articulo + " bodega " + codigo + ".");
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }
            }
            return result;
        }

        /// <summary>
        /// Obtiene las existencias del artículo en bodega para determinada compañía.
        /// </summary>
        /// <param name="compania">compania a obtener existencia</param>
        /// <param name="articulo">codigo del articulo a consultar</param>
        public void ObtenerExistencia(string compania, string articulo)
        {
            if (!VerificarExistBodegaArticulo(compania, articulo))
            {
                existencia = -1;
                return;
            }

            string sentencia =
                @" SELECT EXISTENCIA FROM " + Table.ERPADMIN_ARTICULO_EXISTENCIA  +
                @" WHERE ARTICULO = @ARTICULO" +
                @" AND UPPER(COMPANIA) = @COMPANIA" +
                @" AND BODEGA = @BODEGA";

            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                new SQLiteParameter("@ARTICULO",articulo),
                new SQLiteParameter("@COMPANIA", compania.ToUpper()),
                new SQLiteParameter("@BODEGA", codigo)});

            object valor = GestorDatos.EjecutarScalar(sentencia, parametros);

            if (valor != DBNull.Value)
                existencia = Convert.ToDecimal(valor.ToString());
            else
                throw new Exception("No se encontró existencia en bodega para el artículo");
        }

        public void ObtenerExistencia(string compania, string articulo,SQLiteConnection cnx)
        {
            if (!VerificarExistBodegaArticulo(compania, articulo,cnx))
            {
                existencia = -1;
                return;
            }

            string sentencia =
                @" SELECT EXISTENCIA FROM " + Table.ERPADMIN_ARTICULO_EXISTENCIA +
                @" WHERE ARTICULO = @ARTICULO" +
                @" AND UPPER(COMPANIA) = @COMPANIA" +
                @" AND BODEGA = @BODEGA";

            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                new SQLiteParameter("@ARTICULO",articulo),
                new SQLiteParameter("@COMPANIA", compania.ToUpper()),
                new SQLiteParameter("@BODEGA", codigo)});

            object valor = GestorDatos.EjecutarScalar(sentencia, parametros,cnx);

            if (valor != DBNull.Value)
                existencia = Convert.ToDecimal(valor.ToString());
            else
                throw new Exception("No se encontró existencia en bodega para el artículo");
        }

        private bool VerificarExistBodegaArticulo(string compania, string articulo) 
        {
            try
            {
                bool result = false;
                string sentencia =
                    @" SELECT COUNT(0) FROM " + Table.ERPADMIN_ARTICULO_EXISTENCIA +
                    @" WHERE ARTICULO = @ARTICULO" +
                    @" AND UPPER(COMPANIA) = @COMPANIA" +
                    @" AND BODEGA = @BODEGA";

                SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                new SQLiteParameter("@ARTICULO",articulo),
                new SQLiteParameter("@COMPANIA", compania.ToUpper()),
                new SQLiteParameter("@BODEGA", codigo)});

                object valor = GestorDatos.EjecutarScalar(sentencia, parametros);

                if (valor != DBNull.Value)
                    result = Convert.ToInt32(valor.ToString()) > 0;
                else
                {
                    return false;
                }
                return result;
            }
            catch
            {
                throw new Exception("No se encontró existencia en bodega para el artículo");
            }
            
        }

        private bool VerificarExistBodegaArticulo(string compania, string articulo,SQLiteConnection cnx)
        {
            try
            {
                bool result = false;
                string sentencia =
                    @" SELECT COUNT(0) FROM " + Table.ERPADMIN_ARTICULO_EXISTENCIA +
                    @" WHERE ARTICULO = @ARTICULO" +
                    @" AND UPPER(COMPANIA) = @COMPANIA" +
                    @" AND BODEGA = @BODEGA";

                SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                new SQLiteParameter("@ARTICULO",articulo),
                new SQLiteParameter("@COMPANIA", compania.ToUpper()),
                new SQLiteParameter("@BODEGA", codigo)});

                object valor = GestorDatos.EjecutarScalar(sentencia, parametros,cnx);

                if (valor != DBNull.Value)
                    result = Convert.ToInt32(valor.ToString()) > 0;
                else
                {
                    return false;
                }
                return result;
            }
            catch
            {
                throw new Exception("No se encontró existencia en bodega para el artículo");
            }

        }

        /// <summary>
        /// Obtiene las existencias del artículo en bodega para determinada compañía cuando el articulo utiliza lotes
        /// </summary>
        /// <param name="compania">compania a obtener existencia</param>
        /// <param name="articulo">codigo del articulo a consultar</param>
        public void ObtenerExistenciaLotes(string compania, string articulo)
        {
            string sentencia =
                @" SELECT SUM(EXISTENCIA) AS EXISTENCIA FROM " + Table.ERPADMIN_ARTICULO_EXISTENCIA_LOTE +
                @" WHERE ARTICULO = @ARTICULO" +
                @" AND UPPER(COMPANIA) = @COMPANIA" +
                @" AND BODEGA = @BODEGA";
            
            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                new SQLiteParameter("@ARTICULO",articulo),
                new SQLiteParameter("@COMPANIA", compania.ToUpper()),
                new SQLiteParameter("@BODEGA", codigo)});

            object valor = GestorDatos.EjecutarScalar(sentencia, parametros);

            if (valor != DBNull.Value)
                existencia = Convert.ToDecimal(valor.ToString());
            else
                throw new Exception("No se encontró existencia en bodega para el artículo");
        }

        #region MejorasFRTostadoraElDorado600R6 JEV
        public void ObtenerExistenciaLotes(string compania, string articulo, string lote)
        {
            string sentencia =
                @" SELECT SUM(EXISTENCIA) AS EXISTENCIA FROM " + Table.ERPADMIN_ARTICULO_EXISTENCIA_LOTE +
                @" WHERE ARTICULO = @ARTICULO" +
                @" AND UPPER(COMPANIA) = @COMPANIA" +
                @" AND BODEGA = @BODEGA" +
                @" AND LOTE = @LOTE";

            try
            {

                SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                                            new SQLiteParameter("@ARTICULO",articulo),
                                            new SQLiteParameter("@COMPANIA", compania.ToUpper()),
                                            new SQLiteParameter("@BODEGA", codigo),
                                            new SQLiteParameter("@LOTE", lote),
                                          });

                object valor = GestorDatos.EjecutarScalar(sentencia, parametros);

                if (valor != DBNull.Value)
                    existencia = Convert.ToDecimal(valor.ToString());
                else
                    throw new Exception("No se encontró existencia en bodega para el artículo");
            }
            catch (Exception ex)
            {
                throw new Exception("No se encontró existencia en bodega para el artículo" + ex.Message);
            }
        }

        public bool ObtenerLocalizacionLote(string compania, string articulo, string lote, out string localizacion)
        {
            bool procesoExitoso = true;
            string sentencia = string.Empty;
            string loc = string.Empty;
                
            try
            {

                //Se crea la consulta para obtener la localizacion asociada a un lote
                sentencia = 
                        @" SELECT LOCALIZACION FROM " + Table.ERPADMIN_ARTICULO_EXISTENCIA_LOTE +
                        @" WHERE ARTICULO = @ARTICULO" +
                        @" AND UPPER(COMPANIA) = @COMPANIA" +
                        @" AND BODEGA = @BODEGA" +
                        @" AND LOTE = @LOTE";

                SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                                            new SQLiteParameter("@ARTICULO",articulo),
                                            new SQLiteParameter("@COMPANIA", compania.ToUpper()),
                                            new SQLiteParameter("@BODEGA", codigo),
                                            new SQLiteParameter("@LOTE", lote),
                                          });

                object valor = GestorDatos.EjecutarScalar(sentencia, parametros);

                if (valor != DBNull.Value)
                    loc = valor.ToString();
                else
                {
                    //throw new Exception(String.Format("No se encontró la localización asociada al lote: '{0}'.", lote));
                    procesoExitoso = false;
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                //throw new Exception(String.Format("No se encontró la localización asociada al lote: '{0}'. {1}", lote, ex.Message));
                procesoExitoso = false;
            }

            //Se asigna el valor de retorno 
            localizacion = loc;

            return procesoExitoso;
        }
        #endregion MejorasFRTostadoraElDorado600R6 JEV

        /// <summary>
        /// Aumenta o disminuye sobre la cantidad disponible actualmente
        /// </summary>
        /// <param name="compania">compania a la cual pertenece el articulo a actualizar</param>
        /// <param name="articulo">articulo a actualizar</param>
        /// <param name="cantidad">cantidad a actualizar</param>
        public void ActualizarExistencia(string compania, string articulo, decimal cantidad)
		{
            int registros=0;
			string sentencia =
                @" UPDATE " + Table.ERPADMIN_ARTICULO_EXISTENCIA +
                @" SET EXISTENCIA = EXISTENCIA + @CANTIDAD " +
                @" WHERE ARTICULO = @ARTICULO" +
                @" AND UPPER(COMPANIA) = @COMPANIA" +
                @" AND BODEGA = @BODEGA";

            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                GestorDatos.SQLiteParameter("@ARTICULO",SqlDbType.NVarChar,articulo),
                GestorDatos.SQLiteParameter("@COMPANIA",SqlDbType.NVarChar,compania.ToUpper()),
                GestorDatos.SQLiteParameter("@BODEGA",SqlDbType.NVarChar,codigo),
                GestorDatos.SQLiteParameter("@CANTIDAD",SqlDbType.Decimal,cantidad)});
            try
            {
                registros = GestorDatos.EjecutarComando(sentencia, parametros);
            }
            catch (Exception ex)
            {
                throw new Exception("Error actualizando existencias al artículo '" + articulo + "'. " + ex.Message);
            }	
			if (registros > 1)
				throw new Exception("Más de un registro afectado en Bodega.");
			else if (registros < 0)
				throw new Exception("Ningún registro afectado en Bodega.");			
		}
 
        #endregion
    }
}
