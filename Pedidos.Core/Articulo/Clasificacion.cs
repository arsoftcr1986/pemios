using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Data.SQLiteBase;
using BI.Shared;

namespace Pedidos.Core
{
    public class Clasificacion
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public int Agrupacion { get; set; }

        public Clasificacion(string pCodigo,string pDescripcion,int pAgrupacion) 
        {
            Codigo = pCodigo;
            Descripcion = pDescripcion;
            Agrupacion = pAgrupacion;
        }

        public static List<Clasificacion> ObtenerClasificaciones(string compania, int agrupacion,SQLiteConnection cnx)
        {
            List<Clasificacion> lista = new List<Clasificacion>();
            SQLiteDataReader reader = null;
            try
            {
                string sentencia = "SELECT CLASIFICACION,DESCRIPCION,AGRUPACION FROM " + Table.ERPADMIN_CLASIFICACION +
                    " WHERE COD_CIA=@COMPANIA AND AGRUPACION=@GRUPO";
                SQLiteParameterList parametros = new SQLiteParameterList();
                parametros.Add(new SQLiteParameter("@COMPANIA", System.Data.SqlDbType.NVarChar, compania));
                parametros.Add(new SQLiteParameter("@GRUPO", System.Data.SqlDbType.Int, agrupacion));
                reader = GestorDatos.EjecutarConsulta(sentencia, parametros, cnx);
                while(reader.Read())
                {
                    lista.Add(new Clasificacion(reader.GetString(0), reader.GetString(1), reader.GetInt32(2)));
                }
            }
            catch
            {
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }
                throw new Exception("Error consultando clasificaciones.");
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }
            }
            return lista;
        }

        public static bool HayClasificaciones(string compania, int agrupacion, SQLiteConnection cnx)
        {           
            
            try
            {
                string sentencia = "SELECT COUNT(0) FROM " + Table.ERPADMIN_CLASIFICACION +
                    " WHERE COD_CIA=@COMPANIA AND AGRUPACION=@GRUPO";
                SQLiteParameterList parametros = new SQLiteParameterList();
                parametros.Add(new SQLiteParameter("@COMPANIA", System.Data.SqlDbType.NVarChar, compania));
                parametros.Add(new SQLiteParameter("@GRUPO", System.Data.SqlDbType.Int, agrupacion));
                int result = Convert.ToInt32(GestorDatos.EjecutarScalar(sentencia, parametros, cnx));
                return result>0;     
            }
            catch
            {
                return false;   
            }
        }
    }
}