using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Pedidos.Core
{
    public class BodegaExistencia
    {
        public string Codigo { get; set; }
        public decimal Existencia { get; set; }

        public BodegaExistencia(string pCodigo, decimal pExistencia)
        {
            Codigo = pCodigo;
            Existencia = pExistencia;
        }
    }
}