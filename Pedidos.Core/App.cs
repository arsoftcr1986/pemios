using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Pedidos.Core
{
    class App
    {
        public static string ObtenerRutaSD()
        {
            string path = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
            if (new Java.IO.File("/storage/sdcard").Exists())
            {
                path = "/storage/sdcard";
            }
            else if (new Java.IO.File("/storage/sdcard0").Exists())
            {
                path = "/storage/sdcard0";
            }
            else if (new Java.IO.File("/storage/sdcard1").Exists())
            {
                path = "/storage/sdcard1";
            }
            return path;

        }
    }
}