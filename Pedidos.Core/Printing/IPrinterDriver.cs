using System;
using System.Threading.Tasks;

namespace Pedidos.Core
{
	/// <summary>
	/// Summary description for IPrinterDriver.
	/// </summary>
	public interface IPrinterDriver
	{

        string ChangeFontStyle(bool bold, bool italic);
        string ChangeFont(string fontName, int fontSize);

        void Open(string tipo);
        Task<bool> Print(string text, string tipo);
        void Configure();
        void Close(string tipo);
	}

    /// <summary>
    /// Summary description for IPrintable.
    /// </summary>
    public interface IPrintable
    {
        string GetObjectName();
        object GetField(string name);
    }
}
