﻿using Android.App;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pedidos.Core
{

    public class Report
    {
        private RDLEngine engine;
        private string reportFilename;
        private string errorLog;
        private string reportText;
        private ArrayList symbols = new ArrayList();
        private string outputText;

        public Report(string reportFilename, IPrinterDriver printerDriver)
        {
            this.engine = new RDLEngine(printerDriver);
            this.reportFilename = reportFilename;
            this.reportText = "";
        }

        public void AddObject(IPrintable symbol)
        {
            this.symbols.Add(symbol);
        }

        public bool PrintAll(ref string textoReporte, string tipo)
        {
            if (this.reportText == "")
            {

                FileStream reportReader = null;
                byte[] reportBuffer;

                this.errorLog = "";

                try
                {
                    reportReader = new FileStream(this.reportFilename, FileMode.Open, FileAccess.Read);
                    reportBuffer = new byte[reportReader.Length];
                    reportReader.Read(reportBuffer, 0, (int)reportBuffer.Length);
                    reportText = Encoding.UTF8.GetString(reportBuffer, 0, reportBuffer.Length);
                    reportReader.Close();
                }
                catch (Exception e)
                {
                    this.errorLog = e.Message;
                    return false;
                }
            }

            try
            {
                outputText = this.engine.PrintAll(this.reportText, this.symbols, tipo);
                textoReporte = textoReporte + outputText;
                //Thread.Sleep(4000); 
                return true;
            }
            catch (Exception e)
            {
                this.errorLog = e.Message;
                return false;
            }
        }

        public async Task<bool> Print(string tipo, Activity myActivity)
        {
            if (this.reportText == "")
            {

                FileStream reportReader = null;
                byte[] reportBuffer;

                this.errorLog = "";

                try
                {
                    reportReader = new FileStream(this.reportFilename, FileMode.Open, FileAccess.Read);
                    reportBuffer = new byte[reportReader.Length];
                    reportReader.Read(reportBuffer, 0, (int)reportBuffer.Length);
                    reportText = Encoding.UTF8.GetString(reportBuffer, 0, reportBuffer.Length);
                    reportReader.Close();
                }
                catch (Exception e)
                {
                    this.errorLog = e.Message;
                    return false;
                }
            }
            Android.App.ProgressDialog progressDialog = null;
            try
            {
                myActivity.RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(myActivity, "Imprimiendo", "Por favor espere...", true); });
                outputText = await this.engine.PrintTask(this.reportText, this.symbols, tipo);
                if (outputText.Contains("ERROREX"))
                {
                    myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                    this.errorLog = outputText.Replace("ERROREX", string.Empty);
                    return false;
                }
                else
                {
                    myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                    return true;
                }
            }
            catch (Exception e)
            {
                myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                this.errorLog = e.Message;
                return false;
            }
        }

        public async Task<bool> PrintText(string textoReporte, string tipo, Activity myActivity)
        {
            Android.App.ProgressDialog progressDialog = null;
            try
            {
                myActivity.RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(myActivity, "Imprimiendo", "Por favor espere...", true); });
                if (await this.engine.PrintTextTask(textoReporte, tipo))
                {
                    myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                    return true;
                }
                else
                {
                    myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                    return false;
                }
            }
            catch (Exception e)
            {
                myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                this.errorLog = e.Message;
                return false;
            }
        }

        public string ErrorLog
        {
            get
            {
                return this.errorLog;
            }

        }





    }
}