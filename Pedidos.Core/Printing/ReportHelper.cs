using BI.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pedidos.Core
{
    public static class ReportHelper
    {
        /// <summary>
        /// Folder donde se ponen los reportes
        /// </summary>
        private const string FolderReportes = "Reportes";

        /// <summary>
        /// Se crea la ruta completa del reporte, a partir del FRmConfig.FullAppPath
        /// y se asume que los reportes van a estar localizados en el subfolder Reportes
        /// y se concatena la extensión .rdl
        /// </summary>
        /// <param name="nombreReporte">nombre del archivo del reporte sin extensión</param>
        /// <returns></returns>
        internal static string CrearRutaReporte(Rdl nombreReporte,string path)
        {
            string folder = System.IO.Path.Combine(path, "PEM.DROID.REPORTS"); 
            if (FRmConfig.TamañoPapel.Equals(2))
            {
                return System.IO.Path.Combine(folder, ReportHelper.FolderReportes + "2", nombreReporte.ToString() + ".rdl");
            }
            if (FRmConfig.TamañoPapel == 3)
            {
                return System.IO.Path.Combine(folder, ReportHelper.FolderReportes + "3", nombreReporte.ToString() + ".rdl");
            }
            return System.IO.Path.Combine(folder, ReportHelper.FolderReportes, nombreReporte.ToString() + ".rdl");
        }

        internal static string CrearRutaReporteStr(string nombreReporte,string path)
        {
            string folder = System.IO.Path.Combine(path, "PEM.DROID.REPORTS");            
            if (FRmConfig.TamañoPapel.Equals(2))
            {
                return System.IO.Path.Combine(folder, ReportHelper.FolderReportes + "2", nombreReporte.ToString() + ".rdl");
            }
            if (FRmConfig.TamañoPapel == 3)
            {
                return System.IO.Path.Combine(folder, ReportHelper.FolderReportes + "3", nombreReporte.ToString() + ".rdl");
            }
            return System.IO.Path.Combine(folder, ReportHelper.FolderReportes, nombreReporte.ToString() + ".rdl");
        }
    }
}