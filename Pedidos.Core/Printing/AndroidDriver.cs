﻿using System;
using System.Drawing;
using Com.Printinglibrary;
using Com.Impresion;
using Java.IO;
using Java.Util;
using Java.Lang;
using System.Threading.Tasks;
using Com.Impresion.Utils;
using System.Collections.Generic;

namespace Pedidos.Core
{
    /// <summary>
    /// Clase que permite imprimir texto e imagenes a un impresora movil.
    /// </summary>
    public class AndroidDriver : IPrinterDriver
    {
        //const string LICENSE_KEY = "937S5H393B";
#pragma warning disable 169
        public static PrintingLibrary ptLibrary = new PrintingLibrary();
        public static PrintingUtils ptUtils = new PrintingUtils();
        public static bool isOpen = false;
        public static bool esDolar = false;
        public static string simboloMoneda;

        //private Printer Impresora = Printer.CANONBJ600;
        //private Port Puerto = Port.INFRARED;

        private int cantidadLineasXPagina = 10;

        private string deviceName = string.Empty;

        /// <summary>
        /// Cantidad de línea que se imprimen por página.
        /// Valor utilizado únicamente con PrinterCE driver.
        /// </summary>
        /// <exception cref="">
        /// ArgumentException cuando el valor no es mayor a cero.
        /// </exception>
        public int CantidadLineasXPagina
        {
            get
            {
                return this.cantidadLineasXPagina;
            }
            set
            {
                if (value < 1)
                    throw new ArgumentException("Solo se permiten valores mayores a cero.");

                this.cantidadLineasXPagina = value;
            }
        }

        /// <summary>
        /// Crea un nuevo driver tipo AsciiCE que solo permite imprimir texto.
        /// </summary>
        /// <param name="port"></param>
        /// <exception cref="">
        /// ArgumentException en caso de que el puerto seleccionado sea
        /// Port.NetIp, Port.NetPath o Port.Use_Current
        /// </exception>
        public AndroidDriver(string DeviceName)
        {
            this.deviceName = DeviceName;
        }

        /// <summary>
        /// Crea un nuevo driver tipo PrinterCE que permite imprimir texto e imagenes.
        /// </summary>
        /// <param name="printer"></param>
        /// <param name="port"></param>
        /// <param name="cantidadLineasXPagina">
        /// Cantidad de lineas que se imprimen por página.
        /// </param>
        /// <exception cref="">
        /// ArgumentException en caso de que el puerto seleccionado sea
        /// Port.PRINTERCE_SHARE.
        /// ArgumentException cuando el valor no es mayor a cero.
        /// </exception>
        public AndroidDriver(string DeviceName, int cantidadLineasXPagina)
        {
            this.deviceName = DeviceName;
            this.CantidadLineasXPagina = cantidadLineasXPagina;
        }

        #region IPrinterDriver Members

        public string ChangeFontStyle(bool bold, bool italic)
        {
            // TODO:  Add PrinterCEDriver.ChangeFontStyle implementation
            return null;
        }

        public string ChangeFont(string fontName, int fontSize)
        {

            string font = "";

            fontName = fontName.Replace("\"", "");

            if (fontName == "MF107")
                font = "&";
            if (fontName == "MF204")
                font = "!";
            if (fontName == "MF226")
                font = "%";
            if (fontName == "UNICD")
                font = "u";
            if (fontName == "MF340")
                font = ">";
            if (fontName == "MF185")
                font = "$";
            if (fontName == "MF102")
                font = " ";
            if (fontName == "MF072")
                font = "\"";
            if (fontName == "MF055")
                font = "#";
            if (fontName == "IS340")
                font = "[";
            if (fontName == "IS204")
                font = "P";

            //return "\x1B"+"R7\n";
            return "\x1B" + "w" + font + "\n";
        }

        public void Open(string tipo)
        {
            try
            {
                if (tipo.ToUpper().Contains("ZEBRA"))
                    isOpen = ptUtils.Inicializar(this.deviceName);
                else
                    isOpen = ptLibrary.Inicializar(this.deviceName);
                if (isOpen)
                {
                    Thread.Sleep(1500);
                }
            }
            catch (System.Exception ex)
            {
                string error = "Error no esperado al abrir impresora. " + ex.Message;
            }
            //Implementar
        }

        public async Task<bool> Print(string text, string tipo)
        {
            try
            {
                text += "\n";
                if (isOpen || !isOpen)
                {
                    //int index = text.IndexOf("wP");
                    //if (index != -1)
                    //{
                    //    string temp = text.Substring(0, index - 1);
                    //    text = temp + text.Substring(index + 3);
                    //}
                    if (tipo == "Generica")
                    {
                        if (!esDolar)
                        {
                            string temp = string.Empty;
                            switch (simboloMoneda)
                            {
                                case "¢": temp = "C"; break;
                                case "₡": temp = "C"; break;
                                case "RD": temp = "RD"; break;
                                case "rd": temp = "rd"; break;
                                case "MX": temp = "MX"; break;
                                case "mx": temp = "mx"; break;
                                default: temp = "$"; break;
                            }
                            text = text.Replace("$", temp);
                        }
                    }
                    string[] strarray = text.Split(Convert.ToChar("\n"));
                    List<string> listaLineas = new List<string>();
                    List<string> listaImprimir = new List<string>();
                    foreach (string str in strarray)
                    {
                        listaLineas.Add(str);
                        //ptLibrary.SendData(str);
                        //Thread.Sleep(100);
                    }
                    int lineas = listaLineas.Count;
                    int contadorMax = Convert.ToInt32(lineas / 3);
                    int contador = 0;
                    for (int i = 0; i <= contadorMax; i++)
                    {
                        string linea = string.Empty;
                        int limit = 3;
                        if ((contador + 3) > listaLineas.Count)
                            limit = listaLineas.Count - contador;
                        foreach (string sttr in listaLineas.GetRange(contador, limit))
                        {
                            linea = linea + sttr;
                        }
                        listaImprimir.Add(linea);
                        contador = (contador + 3);
                    }
                    if (tipo.ToUpper().Contains("ZEBRA"))
                    {
                        byte[] texto =
                                 ptUtils.SendData(text);
                        bool result = await ImprimirZebra(ptUtils.MacAdress, texto, listaImprimir.Count);
                        //bool result =await ImprimirZebraTextos(ptUtils.MacAdress, listaLineas);
                        return result;
                    }
                    else
                    {
                        bool result = await ImprimirNormal(text, listaImprimir.Count);
                        return result;
                    }
                    //ptLibrary.SendData(text);
                    //Thread.Sleep(1500);
                }
            }
            catch (System.Exception ex)
            {
                string error = "Error no esperado en la impresión del documento. " + ex.Message;
            }
            return false;
        }

        public void Configure()
        {
            // TODO:  Add PrinterCEDriver.Configure implementation
        }

        public async Task<bool> ImprimirNormal(string texto, int count)
        {
            try
            {

                bool result = await PrintNormal(texto);
                if (result)
                {
                    result = await DeeperSpleeper(500 * count);
                    return result;
                }
                else
                {
                    return result;
                }

            }
            catch (System.Exception ex)
            {
                string error = ex.Message;
            }
            return false;
        }

        public async Task<bool> ImprimirZebra(string MacAdress, byte[] bytes, int count)
        {
            try
            {
                Com.Zebra.Android.Comm.BluetoothPrinterConnection thePrinterConn = new Com.Zebra.Android.Comm.BluetoothPrinterConnection(MacAdress);
                thePrinterConn.Open();
                bool result = await DeeperSpleeper(1500);
                if (result)
                {
                    result = await PrintZebra(thePrinterConn, bytes);
                }
                if (result)
                {
                    result = await DeeperSpleeper(500 * count);
                    if (result)
                    {
                        thePrinterConn.Close();
                    }
                    return result;
                }
                else
                {
                    thePrinterConn.Close();
                    return result;
                }

            }
            catch (System.Exception ex)
            {
                string error = ex.Message;
            }
            return false;
        }

        public Task<bool> ImprimirZebraTextos(string MacAdress, List<string> str)
        {
            return Task.Run(() =>
            {
                try
                {
                    foreach (string stri in str)
                    {
                        Com.Zebra.Android.Comm.BluetoothPrinterConnection thePrinterConn = new Com.Zebra.Android.Comm.BluetoothPrinterConnection(MacAdress);
                        thePrinterConn.Open();
                        thePrinterConn.Write(ptUtils.SendData(stri));
                        Thread.Sleep(500);
                        thePrinterConn.Close();
                    }
                    return true;
                }
                catch
                {
                    return false;
                }
            });
        }

        public Task<bool> PrintZebra(Com.Zebra.Android.Comm.BluetoothPrinterConnection thePrinterConn, byte[] bytes)
        {
            return Task.Run(() =>
            {
                try
                {
                    thePrinterConn.Write(bytes);
                    return true;
                }
                catch
                {
                    return false;
                }
            });
        }

        public Task<bool> PrintNormal(string text)
        {
            return Task.Run(() =>
            {
                try
                {
                    ptLibrary.SendData(text);
                    return true;
                }
                catch
                {
                    return false;
                }
            });
        }

        public Task<bool> DeeperSpleeper(int miliseconds)
        {
            return Task.Run(() =>
            {
                Thread.Sleep(miliseconds);
                return true;
            });
        }

        public void Close(string tipo)
        {
            if (tipo.ToUpper().Contains("ZEBRA"))
            {
                Thread.Sleep(500);
                return;
            }
            try
            {
                Thread.Sleep(2000);
                if (ptLibrary.CloseBT())
                    isOpen = false;
                Thread.Sleep(2000);
            }
            catch (System.Exception ex)
            {
                string error = "Error no esperado al cerrar impresora. " + ex.Message;
            }
            //Implementar
        }

        #endregion
    }
}
