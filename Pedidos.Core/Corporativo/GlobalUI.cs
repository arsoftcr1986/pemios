using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pedidos.Core
{
    public static class GlobalUI
    {
        #region Variables de clase

        /// <summary>
        /// Variable que mantiene la informacion actual de la visita.
        /// </summary>
        //public static Softland.ERP.FR.Mobile.Cls.FRCliente.FRVisita.Visita VisitaActual;
        public static List<Ruta> Rutas;
        public static Ruta RutaActual;
        public static List<Ruta> RutasActuales;
        public static Cliente ClienteActual;        

        public static string NameCliente
        {
            get
            {
                return " Cod." + GlobalUI.ClienteActual.Codigo + "-" + GlobalUI.ClienteActual.Nombre;
            }
        }

        #endregion

        public static bool CargaParametrosX(bool logBitacora)
        {
            try
            {
                ParametroSistema.CargarParametros();
                if (logBitacora)
                {
                    //Bitacora.Escribir("Se van a leer los consecutivos luego de carga de datos");
                    //Bitacora.Escribir("Consecutivos luego de carga: " + ParametroSistema.GenerarSentenciaActualizacion());
                }
            }
            catch (System.Exception ex)
            {
                throw new Exception("Error al cargar los parametros del Sistema. " + ex.Message);
            }

            try
            {
                FRdConfig.CargarGlobales();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al cargar los parámetros globales. " + ex.Message);
            }

            return true;
        }

        /// <summary>
        /// Valida la configuración
        /// </summary>
        /// <returns></returns>
        public static bool ValidarConfiguracion()
        {
            bool exito = false;
            string mensaje = string.Empty;
            HandHeldConfig config = new HandHeldConfig();

            exito = config.cargarConfiguracionHandHeld(ref mensaje);
            if (exito)
                exito = config.cargarConfiguracionGlobalHandHeld(ref mensaje);
            if (!exito)
            {
                //TODO mensaje
            }

            return exito;
        }
    }
}