﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLiteBase;
using System.Data;
using System.Xml.Serialization;
using System.IO;
using Newtonsoft;
using BI.Shared;

namespace Pedidos.Core
{
    public class Pedido : EncabezadoDocumento, IPrintable
    {
        #region Propiedades Globales
        
        #endregion

        #region Variables y Propiedades de instancia

        //Lista para los descuentos de lineas de las devoluciones con documento en esquema descuento estándar
        public List<decimal> descLineasDevolucionDocumento = new List<decimal>();

        private DetallesPedido detalles = new DetallesPedido();
        /// <summary>
        /// Detalles del Pedido
        /// </summary>
        public DetallesPedido Detalles
        {
            get { return detalles; }
            set { detalles = value; }
        }

        private string usuario = string.Empty;
        /// <summary>
        /// Almacena el usuario que realizo el pedido.
        /// </summary>
        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        #endregion
        
        #region Fechas

        private DateTime fechaEntrega = DateTime.Now;
        /// <summary>
        /// Variable que indica la fecha prometida para la entrega del pedido
        /// </summary>
        public DateTime FechaEntrega
        {
            get { return fechaEntrega; }
            set { fechaEntrega = value; }
        }

        #endregion

        #region Configuracion Previa a la Toma
        
        private TipoPedido tipo;
        /// <summary>
        /// Tipo de documento generado para el pedido
        /// </summary>
        public TipoPedido Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        private ConfigDocCia configuracion;
        /// <summary>
        /// Configuracion de Pago
        /// </summary>
        public ConfigDocCia Configuracion
        {
            get { return configuracion; }
            set { configuracion = value; }
        }

        #region Modificaciones en funcionalidad de generacion de recibos de contado - KFC

        private CondicionPago condicionPago;
        /// <summary>
        /// Configuracion de Pago
        /// </summary>
        public CondicionPago CondicionPago
        {
            get { return condicionPago; }
            set { condicionPago = value; }
        }

        #endregion

        /// <summary>
        /// Porcentaje del descuento 1 expresando como entero (0 - 100)
        /// </summary>
        public override decimal PorcentajeDescuento1
        {
            get { return base.PorcentajeDescuento1; }
            set
            {
                base.PorcentajeDescuento1 = value;
                ReCalcularDescuentosGenerales();
            }
        }

        public override decimal PorcentajeDescuento2
        {
            get { return base.PorcentajeDescuento2; }
            set
            {
                base.PorcentajeDescuento2 = value;
                ReCalcularDescuentosGenerales();
            }
        }

        public decimal MontoBrutoBonificaciones
        {
            get
            {
                decimal monto=0;
                bool redondearFactura = this.Configuracion.Compania.UsaRedondeo && this.Configuracion.Compania.FactorRedondeo > 0 && this.Configuracion.Compania.RedondeoLinea;
                foreach(DetallePedido det in Detalles.Lista.Where(x=>x.LineaBonificada!=null))
                {
                    if (redondearFactura)
                    {
                        monto +=  Decimal.Round((det.LineaBonificada.UnidadesAlmacen * det.LineaBonificada.Articulo.PrecioMaximo) + ((det.LineaBonificada.UnidadesDetalle) * det.LineaBonificada.Articulo.PrecioMinimo),2);
                    }
                    else
                    {
                        monto += (det.LineaBonificada.UnidadesAlmacen * det.LineaBonificada.Articulo.PrecioMaximo) + ((det.LineaBonificada.UnidadesDetalle) * det.LineaBonificada.Articulo.PrecioMinimo);
                    }
                    
                }
                return decimal.Round(monto,2);
            }
        }

        /// <summary>
        /// Porcentaje de descuento general asignado al pedido.
        /// Porcentaje 1 + Porcentaje 2.
        /// </summary>
        public decimal PorcDescGeneral
        {
            get
            {			
                if (this.DescuentoCascada)
                {
                    if (this.MontoBruto > Decimal.Zero)
                    {
                        decimal Porcentaje = ((this.MontoBruto * (this.PorcentajeDescuento1 / 100)) 
                                           + ((this.MontoBruto - 
                                              (this.MontoBruto * (this.PorcentajeDescuento1 / 100))) 
                                           *  (this.PorcentajeDescuento2 / 100))) / this.MontoBruto;
                        return Porcentaje;
                    }
                    else
                    {
                        //ABC Caso 37017
                        return this.PorcentajeDescuento1 / 100;
                    }
                }
                else
                {
                    return (this.PorcentajeDescuento1 + this.PorcentajeDescuento2) / 100;
                }
            }
        }

        public bool Seleccionado { get; set; }

        #endregion

        #region Estado

        private EstadoPedido estado = EstadoPedido.Normal;
        /// <summary>
        /// Indica el estado de la linea.
        /// </summary>
        public EstadoPedido Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        #endregion

        #region Referencia Consignacion
        
        private bool esConsignacion = false;
        /// <summary>
        /// indica si es consignada la devolución si fue generada por el desglose de una boleta de venta en consignación.
        /// </summary>
        public bool EsConsignacion
        {
            get { return esConsignacion; }
            set { esConsignacion = value; }
        }
        private string numRef = string.Empty;
        /// <summary>
        /// indica numero de consignacion de la devolución si fue generada por el desglose de una boleta de venta en consignación.
        /// </summary>
        public string NumRef
        {
            get { return numRef; }
            set { numRef = value; }
        }
        #endregion

        #region otros

        private string direccionEntrega = string.Empty;
        /// <summary>
        /// Indica la direccion de entrega del pedido
        /// </summary>
        public string DireccionEntrega
        {
            get { return direccionEntrega; }
            set { direccionEntrega = value; }
        }
        
        #endregion

        #region Descuento

        private bool descuentoCascada;
        /// <summary>
        /// Indica si el pedido es realizado con descuento en cascada.
        /// </summary>
        public bool DescuentoCascada
        {
            get { return descuentoCascada; }
            set { descuentoCascada = value; }
        }

        private LineaRegalia regaliaDescuentoGeneral;
        public LineaRegalia RegaliaDescuentoGeneral
        {
            get { return regaliaDescuentoGeneral; }
            set { regaliaDescuentoGeneral = value; }
        }

        #endregion

        //KFC Sincronizacion de Listas de Precios
        private string moneda = string.Empty;
        /// <summary>
        /// Indica la moneda con la que se realiza el pedido
        /// </summary>
        public string Moneda 
        {
            get { return moneda; }
            set { moneda = value; }
        }

        private string nivelPrecioCod = string.Empty;
        /// <summary>
        /// Indica la moneda con la que se realiza el pedido
        /// </summary>
        public string NivelPrecioCod
        {
            get { return nivelPrecioCod; }
            set { nivelPrecioCod = value; }
        }

        #region Logica de Negocios

        public Pedido()
        {
            this.HoraInicio = DateTime.Now;
            this.FechaRealizacion = DateTime.Now;
        }
        /// <summary>
        /// Constructor del pedido en Devolucion con documento
        /// </summary>
        /// <param name="numero">Numero de factura</param>
        /// <param name="compania">Compania de la factura</param>
        /// <param name="cliente">Cliente asociado</param>
        /// <param name="zona">Zona asociada al cliente</param>
        public Pedido(string numero, string compania, string cliente, string zona)
        {
            Numero = numero;
            Compania = compania;
            Cliente = cliente;
            Zona = zona;
            Tipo = TipoPedido.Factura;
            Detalles.Encabezado =this;

        }

        public Pedido(string numero, string compania, string cliente, string zona, string ncf)
        {
            Numero = numero;
            Compania = compania;
            Cliente = cliente;
            Zona = zona;
            Tipo = TipoPedido.Factura;
            Detalles.Encabezado = this;            
        }

        /// <summary>
        /// Crear un pedido sencillo
        /// </summary>
        /// <param name="articulo"></param>
        /// <param name="zona"></param>
        public Pedido(Articulo articulo, string zona, ConfigDocCia configuracion)
        {
                this.Compania = articulo.Compania;
                this.Zona = zona;
                this.Configuracion = configuracion;
                this.Cliente = this.Configuracion.ClienteCia.Codigo;
                this.DescuentoCascada = this.Configuracion.Compania.DescuentoCascada;
                this.Bodega = articulo.Bodega.Codigo;
                this.DireccionEntrega = this.Configuracion.ClienteCia.DireccionEntregaDefault;
                this.PorcentajeDescuento1 = this.Configuracion.ClienteCia.Descuento;
                this.NivelPrecio = this.Configuracion.Nivel.Codigo;
                if(this.Configuracion.CondicionPago!=null)
                {
                    this.CondicionPago = Configuracion.CondicionPago;
                }                
                this.HoraInicio = DateTime.Now;
                this.FechaRealizacion = DateTime.Now;
                this.HoraFin = DateTime.Now;
                
                if (PedidosCollection.FacturarPedido)
                {
                    this.Estado = EstadoPedido.Facturado;
                    this.Tipo = TipoPedido.Factura;
                    this.FechaEntrega = this.FechaRealizacion; 
                    
                    this.Configuracion.Compania.Cargar();
                }
                else
                {
                    this.Estado = EstadoPedido.Normal;
                    this.Tipo = TipoPedido.Prefactura;
                    this.FechaEntrega = DateTime.Now.AddDays(1);
                }

                //KFC Sincronización listas de Precios
                this.NivelPrecioCod = this.Configuracion.Nivel.Nivel;
                this.Moneda = this.Configuracion.Nivel.Moneda.Equals(TipoMoneda.LOCAL) ? "L" : "D";
        }
        /// <summary>
        /// Retornar un Pedido en Consignacion
        /// </summary>
        public static Pedido Consignado(string consignacion,Articulo articulo, string zona, ConfigDocCia configuracion, bool descuentoCascada,
            decimal porcDesc1, decimal porcDesc2, string nota)
        {
            //Se asigna la configuración de la boleta de venta en consignación a la factura
            //que se va a generar por el desglose.
            Pedido pedido = new Pedido(articulo, zona, configuracion);
            
            //Se aplica el descuento en cascada en la factura siempre y cuando se indique en la boleta
            //de venta en consignación que se debe aplicar el descuento en cascada.
            pedido.DescuentoCascada = descuentoCascada;

            //Asignamos el porcentaje de descuento 1 definido en la venta en consignación.
            //Si no está definido en la venta en consignación entonces asignamos el descuento general del cliente.
            if (porcDesc1 > decimal.Zero)
                pedido.PorcentajeDescuento1  = porcDesc1;

            //Asignamos el porcentaje de descuento 2 definido en la venta en consignación.
            pedido.PorcentajeDescuento2 = porcDesc2;
   
            //Se asigna el código de la bodega de venta en consignación del cliente
            pedido.Bodega = configuracion.ClienteCia.Bodega;

            //Se indica que la factura está siendo generada por el desglose de una boleta de venta en consignación.
            pedido.EsConsignacion = true;
            pedido.NumRef = consignacion;

            //Se asigna la nota a la factura que se está gestionando.
            pedido.Notas = nota;

            return pedido;
        }

        /// <summary>
        /// Obtiene los descuentos de lineas de lo historicos para las devoluciones.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="compania"></param>
        /// <param name="numPed"></param>
        /// <returns></returns>
        public List<decimal> ObtieneDescuentosLineasHistFactura(string cliente, string compania, string numPed)
        {
            List<decimal> lista = new List<decimal>();

            //ABC Caso 37189
            //Se deja de condicionar por ruta, ya que el cliente puede pertenecer a varias rutas distintas
            string sentencia =
                "SELECT NUM_LN,CANT_FAC,PRE_UNI,MONT_LN FROM " + Table.ERPADMIN_alFAC_DET_HIST_FAC +
                //" WHERE COD_CLT = @CLIENTE" +

                //" AND   COD_CIA = @COMPANIA " +
                " WHERE   NUM_FAC = @NUM_PED " +
                " AND   EST_BON = @ESTADO " +
                " ORDER BY NUM_LN ASC";

            SQLiteDataReader  reader = null;
            SQLiteParameterList parametros =new SQLiteParameterList(new SQLiteParameter[]{                                           
                      new SQLiteParameter("@NUM_PED", numPed),
                      new SQLiteParameter("@ESTADO", "N")});

            //new SqlCeParameter("@ZONA", zona),
            //" AND   COD_ZON = @ZONA " +

            try
            {
                decimal canFact = 0;
                decimal preUnit = 0;
                decimal montLin = 0;
                decimal descLin = 0;

                reader = GestorDatos.EjecutarConsulta(sentencia, parametros);

                while (reader.Read())
                {
                    if (!reader.IsDBNull(1))
                        canFact = reader.GetDecimal(1);
                    if (!reader.IsDBNull(2))
                        preUnit = reader.GetDecimal(2);
                    if (!reader.IsDBNull(3))
                        montLin = reader.GetDecimal(3);

                    if (preUnit == (montLin / canFact))
                    {
                        descLin = 0;
                    }
                    else
                    {
                        descLin = (preUnit - (montLin / canFact)) / preUnit * 100;
                    }

                    lista.Add(descLin);

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al cargar los historicos de los pedidos. " + ex.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            return lista;
        }

        //Caso 28087 LDS 04/05/2007
        /// <summary>
        /// Metodo encargado de agregar una linea de detalle al pedido.
        /// </summary>
        /// <param name="articulo"></param>
        /// <param name="precioMax">
        /// Es el precio de almacén del artículo, se puede cambiar durante la toma del pedido o factura.
        /// </param>
        /// <param name="precioMin">
        /// Es el precio de detalle del artículo, se puede cambiar durante la toma del pedido o factura.
        /// </param>
        /// <param name="cantidadDetalle"></param>
        /// <param name="cantidadAlmacen"></param>
        /// <param name="validarCantidadLineas"></param>
        public void AgregarLineaDetalle(Articulo articulo,Precio precio,
            decimal unidadesDetalle, decimal unidadesAlmacen,
            bool validarCantidadLineas, decimal unidadAlmacenAdicional, decimal unidadDetalleAdicional,string tope)
        {
            DetallePedido detalle = Detalles.Buscar(articulo.Codigo);

            if (detalle == null)
            {
                //Se crea un nuevo detalle si alguna cantidad es mayor a cero.
                if (unidadesAlmacen == 0 && unidadesDetalle == 0)
                    return;

                if (validarCantidadLineas && this.Detalles.Lista.Count == PedidosCollection.MaximoLineasDetalle)
                        throw new Exception("Se alcanzó el máximo de líneas permitidas (" + PedidosCollection.MaximoLineasDetalle + ")");

                detalle = new DetallePedido();
                detalle.Articulo = articulo;
                //Cesar Iglesias
                detalle.Tope = tope;
                detalle.Articulo.Bodega.Codigo = this.Bodega;
                detalle.RecalcularDetalle(unidadesAlmacen,unidadesDetalle,
                           precio, configuracion.Nivel, configuracion.ClienteCia, PorcDescGeneral, true,configuracion.Compania.Activar_IVA_CR);
                
                //Validamos el margen de utilidad, se levanta frmExcepcion si no cumple.
                if (PedidosCollection.CambiarPrecio)
                    detalle.ValidarMargenUtilidad(configuracion.Nivel, this.PorcDescGeneral);

                if (unidadAlmacenAdicional > 0 || unidadDetalleAdicional > 0)
                    detalle.CargarBonificacionAdicional(unidadAlmacenAdicional, unidadDetalleAdicional);
                
                this.Detalles.Lista.Add(detalle);
                this.AgregarMontosLinea(detalle);
                //Actualizamos los montos de descuentos 1 y 2.
                this.ReCalcularDescuentosGenerales();
            }
            else
            {
                detalle.Tope = tope;
                //El detalle del pedido ya se encuentra en el pedido por lo que se procede a modificarlo.
                if (detalle.UnidadesAlmacen != unidadesAlmacen ||detalle.UnidadesDetalle != unidadesDetalle ||
                    detalle.Precio.Maximo != precio.Maximo || detalle.Precio.Minimo != precio.Minimo)                    
                {
                    //Restamos los montos actuales de la línea.
                    this.RestarMontosLinea(detalle);

                    //Se desea cambiar la cantidad o los precios de la línea del pedido por lo que 
                    //guardamos los valores del detalle para este caso.
                    Precio precioAnt = detalle.Precio;
                    decimal undsAlmAnt = detalle.UnidadesAlmacen;
                    decimal undsDetAnt = detalle.UnidadesDetalle;

                    detalle.RecalcularDetalle(unidadesAlmacen, unidadesDetalle,
                               precio, configuracion.Nivel, configuracion.ClienteCia, PorcDescGeneral, true, configuracion.Compania.Activar_IVA_CR);
                    try
                    {
                        //Validamos el margen de utilidad, se levanta frmExcepcion si no cumple.
                        if (PedidosCollection.CambiarPrecio)
                            detalle.ValidarMargenUtilidad(configuracion.Nivel, this.PorcDescGeneral);
                    }
                    catch (Exception ex)
                    {
                        detalle.RecalcularDetalle(undsAlmAnt,undsDetAnt,precioAnt,
                            configuracion.Nivel,configuracion.ClienteCia, PorcDescGeneral,true, configuracion.Compania.Activar_IVA_CR);
                        throw ex;
                    }
                    finally
                    {
                        this.AgregarMontosLinea(detalle);
                        //Actualizamos los montos de descuentos 1 y 2.
                        this.ReCalcularDescuentosGenerales();
                    }
                }

                if (detalle.LineaBonificadaAdicional != null &&
                    (detalle.LineaBonificadaAdicional.UnidadesAlmacen != unidadAlmacenAdicional ||
                     detalle.LineaBonificadaAdicional.UnidadesDetalle != unidadDetalleAdicional))
                {
                    detalle.LineaBonificadaAdicional.UnidadesAlmacen = unidadAlmacenAdicional;
                    detalle.LineaBonificadaAdicional.UnidadesDetalle = unidadDetalleAdicional;
                }
            }
        }
     
      

        //Caso 28087 LDS 04/05/2007
        /// <summary>
        /// Metodo encargado de agregar una linea de detalle al pedido.
        /// </summary>
        /// <param name="articulo"></param>
        /// <param name="precioMax">
        /// Es el precio de almacén del artículo, se puede cambiar durante la toma del pedido o factura.
        /// </param>
        /// <param name="precioMin">
        /// Es el precio de detalle del artículo, se puede cambiar durante la toma del pedido o factura.
        /// </param>
        /// <param name="cantidadDetalle"></param>
        /// <param name="cantidadAlmacen"></param>
        /// <param name="validarCantidadLineas"></param>
        public void AgregarLineaDetalleTomaFisica(Articulo articulo, Precio precio,
            decimal unidadesDetalle, decimal unidadesAlmacen,
            bool validarCantidadLineas, decimal unidadAlmacenAdicional, decimal unidadDetalleAdicional, string tope)
        {
            DetallePedido detalle = Detalles.Buscar(articulo.Codigo);

            if (detalle == null)
            {
                //Se crea un nuevo detalle si alguna cantidad es mayor a cero.
                if (unidadesAlmacen == 0 && unidadesDetalle == 0)
                    return;

                if (validarCantidadLineas && this.Detalles.Lista.Count == PedidosCollection.MaximoLineasDetalle)
                    throw new Exception("Se alcanzó el máximo de líneas permitidas (" + PedidosCollection.MaximoLineasDetalle + ")");

                detalle = new DetallePedido();
                detalle.Articulo = articulo;
                //Cesar Iglesias
                detalle.Tope = tope;
                detalle.Articulo.Bodega.Codigo = this.Bodega;
                detalle.RecalcularDetalleTomaFisica(unidadesAlmacen, unidadesDetalle,
                           precio, configuracion.Nivel, configuracion.ClienteCia, PorcDescGeneral, true, configuracion.Compania.Activar_IVA_CR);

                //Validamos el margen de utilidad, se levanta frmExcepcion si no cumple.
                if (PedidosCollection.CambiarPrecio)
                    detalle.ValidarMargenUtilidad(configuracion.Nivel, this.PorcDescGeneral);                

                this.Detalles.Lista.Add(detalle);
                this.AgregarMontosLinea(detalle);
                //Actualizamos los montos de descuentos 1 y 2.
                this.ReCalcularDescuentosGenerales();
            }
            else
            {
                detalle.Tope = tope;
                //El detalle del pedido ya se encuentra en el pedido por lo que se procede a modificarlo.
                if (detalle.UnidadesAlmacen != unidadesAlmacen || detalle.UnidadesDetalle != unidadesDetalle ||
                    detalle.Precio.Maximo != precio.Maximo || detalle.Precio.Minimo != precio.Minimo)
                {
                    //Restamos los montos actuales de la línea.
                    this.RestarMontosLinea(detalle);

                    //Se desea cambiar la cantidad o los precios de la línea del pedido por lo que 
                    //guardamos los valores del detalle para este caso.
                    Precio precioAnt = detalle.Precio;
                    decimal undsAlmAnt = detalle.UnidadesAlmacen;
                    decimal undsDetAnt = detalle.UnidadesDetalle;

                    detalle.RecalcularDetalleTomaFisica(unidadesAlmacen, unidadesDetalle,
                               precio, configuracion.Nivel, configuracion.ClienteCia, PorcDescGeneral, true, configuracion.Compania.Activar_IVA_CR);
                    try
                    {
                        //Validamos el margen de utilidad, se levanta frmExcepcion si no cumple.
                        if (PedidosCollection.CambiarPrecio)
                            detalle.ValidarMargenUtilidad(configuracion.Nivel, this.PorcDescGeneral);
                    }
                    catch (Exception ex)
                    {
                        detalle.RecalcularDetalleTomaFisica(undsAlmAnt, undsDetAnt, precioAnt,
                            configuracion.Nivel, configuracion.ClienteCia, PorcDescGeneral, true, configuracion.Compania.Activar_IVA_CR);
                        throw ex;
                    }
                    finally
                    {
                        this.AgregarMontosLinea(detalle);
                        //Actualizamos los montos de descuentos 1 y 2.
                        this.ReCalcularDescuentosGenerales();
                    }
                }
            }
        }

        public void AgregarLineaDetalle(Articulo articulo, Precio precio,
            decimal unidadesDetalle, decimal unidadesAlmacen,
            bool validarCantidadLineas, string tope)
        {
           AgregarLineaDetalle(articulo, precio, unidadesDetalle, unidadesAlmacen, validarCantidadLineas, 0, 0, tope);        
        }

        public void AgregarLineaDetalleTomaFisica(Articulo articulo, Precio precio,
            decimal unidadesDetalle, decimal unidadesAlmacen,
            bool validarCantidadLineas, string tope)
        {
            AgregarLineaDetalleTomaFisica(articulo, precio, unidadesDetalle, unidadesAlmacen, validarCantidadLineas, 0, 0, tope);
        }

        /// <summary>
        /// Elimina un detalle del pedido.
        /// </summary>
        /// <param name="codArt">Código del articulo que se debe eliminar.</param>
        /// <returns>Retorna la cantidad de lineas eliminadas</returns>
        public int EliminarDetalle(string articulo)
        {
            DetallePedido detalleEncontrado = detalles.Eliminar(articulo);

            //Hay que restar los montos de la línea.
            this.RestarMontosLinea(detalleEncontrado);
            //Actualizamos los montos de descuentos 1 y 2.
            this.ReCalcularDescuentosGenerales();

            //Debemos revisar si la línea tiene artículos bonificados.
            if (detalleEncontrado.LineaBonificada != null)
                return 2;
            else return 1;
        }
        /// <summary>
        /// Elimina un detalle bonificado de una factura gestionada por el desglose de una boleta de venta en consignación.
        /// </summary>
        /// <param name="codigoArticulo">Código del artículo que posee el artículo bonificado que se desea quitar de la factura generada por el desglose de una boleta de venta en consignación.</param>
        /// <returns>Inidca la cantidad de líneas bonificadas que se quitaron de la factura gestionada.</returns>
        public int EliminarDetalleBonificado(string articulo)
        {
            int posicionDetalle = detalles.BuscarPos(articulo);
            DetallePedido detalleEncontrado = detalles.Lista[posicionDetalle];
            //Debemos revisar si la línea tiene artículos bonificados.
            if (detalleEncontrado.LineaBonificada != null)
            {
                if (this.GestionarRetiroDetalleBonificado(ref detalleEncontrado))
                {
                    detalles.Lista[posicionDetalle] = detalleEncontrado;
                    return 1;
                }
            }
            return 0;
        }
        /// <summary>
        /// Gestiona el retiro de un detalle bonificado de la factura generada por el desglose de una boleta de venta
        /// en consignación.
        /// </summary>
        /// <param name="detalle">Detalle que contiene el detalle bonificado que se desea gestionar el retiro de la factura generada por el desglose de una boleta de venta en consignación.</param>
        /// <returns>Indica que se realizó el retiro del detalle bonificado.</returns>
        private bool GestionarRetiroDetalleBonificado(ref DetallePedido detalle)
        {
            bool detalleRetirado = false;
            try
            {
                //Restamos los montos actuales de la línea.
                this.RestarMontosLinea(detalle);
                //Se recalcula el detalle y no se carga ni se toma en cuenta las bonificaciones del detalle.
                detalle.RecalcularDetalle(detalle.UnidadesAlmacen,detalle.UnidadesDetalle,
                    detalle.Precio,
                    configuracion.Nivel, configuracion.ClienteCia, this.PorcDescGeneral, false, configuracion.Compania.Activar_IVA_CR);
                //Sumamos los montos actuales de la línea.
                this.AgregarMontosLinea(detalle);
                //Actualizamos los montos de descuentos 1 y 2.
                this.ReCalcularDescuentosGenerales();

                detalleRetirado = true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al gestionar el retiro del detalle bonificado. " + ex.Message);
            }
            return detalleRetirado;
        }

        public bool UsuarioCambioDescuentos()
        {
            if (this.PorcentajeDescuento2 != 0)
                return true;

            if (Configuracion.ClienteCia.Descuento != this.PorcentajeDescuento1)
                return true;

            return false;
        }
        public void RecalcularImpuestos(bool percepcion)
        {
            Impuesto.MontoImpuesto1 = 0;
            Impuesto.MontoImpuesto2 = 0;
            bool redondearFactura = this.Configuracion.Compania.UsaRedondeo && this.Configuracion.Compania.FactorRedondeo > 0 && this.Configuracion.Compania.RedondeoLinea;
            //Recorremos los detalles del pedido
            foreach (DetallePedido detalle in this.Detalles.Lista)
            {   
                // PA2-01482-MKP6 - KFC
                //detalle.CalcularImpuestos(this.Configuracion.ClienteCia.ExoneracionImp1, this.Configuracion.ClienteCia.ExoneracionImp2, this.PorcDescGeneral, percepcion);
                detalle.CalcularImpuestos(this.PorcDescGeneral, percepcion, this.Configuracion.ClienteCia, this.Configuracion.Compania.Activar_IVA_CR);
                if (redondearFactura)
                {

                    this.Impuesto.MontoImpuesto1 += Decimal.Round(detalle.Impuesto.MontoImpuesto1,2);
                    this.Impuesto.MontoImpuesto2 += Decimal.Round(detalle.Impuesto.MontoImpuesto2,2);
                }
                else
                {

                    this.Impuesto.MontoImpuesto1 += detalle.Impuesto.MontoImpuesto1;
                    this.Impuesto.MontoImpuesto2 += detalle.Impuesto.MontoImpuesto2;
                }

            }
        }


        // PA2-01482-MKP6 - KFC
        public static decimal RedondeoAlejandoseDeCero(decimal value, int digits)
        {
            return System.Math.Round(value +
                Convert.ToDecimal(System.Math.Sign(value) / System.Math.Pow(10, digits + 1)), digits);
        }

        public void RecalcularImpuestos()
        {
            this.RecalcularImpuestos(false);
        }
        /// <summary>
        /// Obtiene el costo total del pedido.
        /// </summary>
        /// <returns></returns>
        public decimal ObtenerCostoTotal()
        {
            decimal costoTotal = 0;
            foreach (DetallePedido detalle in this.Detalles.Lista)
                costoTotal += detalle.ObtenerCosto(Configuracion.Nivel.Moneda);
            return costoTotal;
        }
        /// <summary>
        /// Recalcula los montos de descuentos 1 y 2 basados en los porcentajes de los mismos,
        /// el monto bruto del pedido y el total de descuentos por línea.
        /// </summary>
        public void ReCalcularDescuentosGenerales()
        {
            try
            {
                //Ajustamos el monto del descuento 1
                this.MontoDescuento1 = Decimal.Round((MontoBruto - MontoDescuentoLineas) * PorcentajeDescuento1 / 100, 2);

                // En caso que el parámetro "Aplicación de Descuentos generales 1 y 2 en cascada" de la compañía 
                // esté asignado el descuento se deba aplicar en cascada, de lo contrario se aplica normalmente.
                if (this.DescuentoCascada)
                    this.MontoDescuento2 = Decimal.Round(((MontoBruto - MontoDescuentoLineas) - this.MontoDescuento1) * PorcentajeDescuento2 / 100, 2);
                else
                    this.MontoDescuento2 = Decimal.Round((MontoBruto - MontoDescuentoLineas) * PorcentajeDescuento2 / 100, 2);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al calcular los descuentos generales. " + ex.Message);
            }
        }
        /// <summary>
        /// Realiza los ajustes al pedido cuando se cambia las cantidades o los precios a una linea de detalle:
        /// - Suma los nuevos montos de la linea al pedido.Se asume que los montos anteriores fueron restados.
        /// - Carga o modifica la bonificacion de la linea, si es que hay.
        /// </summary>
        /// <param name="detalle"></param>
        public void AgregarMontosLinea(DetallePedido detalle)
        {
            bool redondearFactura = this.Configuracion.Compania.UsaRedondeo && this.Configuracion.Compania.FactorRedondeo > 0 && this.Configuracion.Compania.RedondeoLinea;
            //Actualizamos los montos.
            if (redondearFactura)
            {
                this.MontoDescuentoLineas += Decimal.Round(detalle.MontoDescuento,2);
                this.MontoBruto += Decimal.Round(detalle.MontoTotal,2);
                this.Impuesto.MontoImpuesto1 += Decimal.Round(detalle.Impuesto.MontoImpuesto1,2);
                this.Impuesto.MontoImpuesto2 += Decimal.Round(detalle.Impuesto.MontoImpuesto2,2);
            }
            else
            {
                this.MontoDescuentoLineas += detalle.MontoDescuento;
                this.MontoBruto += detalle.MontoTotal;
                this.Impuesto.MontoImpuesto1 += detalle.Impuesto.MontoImpuesto1;
                this.Impuesto.MontoImpuesto2 += detalle.Impuesto.MontoImpuesto2;
            }
            
        }
        /// <summary>
        /// Resta a los montos del pedido los montos de una línea de detalle.
        /// </summary>
        /// <param name="detalle"></param>
        public void RestarMontosLinea(DetallePedido detalle)
        {
            bool redondearFactura = this.Configuracion.Compania.UsaRedondeo && this.Configuracion.Compania.FactorRedondeo > 0 && this.Configuracion.Compania.RedondeoLinea;
            if (redondearFactura)
            {
                this.MontoDescuentoLineas -= Decimal.Round(detalle.MontoDescuento,2);
                this.MontoBruto -= Decimal.Round(detalle.MontoTotal,2);
                this.Impuesto.MontoImpuesto1 -= Decimal.Round(detalle.Impuesto.MontoImpuesto1,2);
                this.Impuesto.MontoImpuesto2 -= Decimal.Round(detalle.Impuesto.MontoImpuesto2,2);
            }
            else
            {
                this.MontoDescuentoLineas -= detalle.MontoDescuento;
                this.MontoBruto -= detalle.MontoTotal;
                this.Impuesto.MontoImpuesto1 -= detalle.Impuesto.MontoImpuesto1;
                this.Impuesto.MontoImpuesto2 -= detalle.Impuesto.MontoImpuesto2;
            }
            
        }

        #endregion

        #region Metodos de Instancia 

        public void Actualizar(bool commit)
        {
            if (detalles.Lista.Count == 0)
                return;
            try
            {
                GestorDatos.BeginTransaction();
                DBEliminar();
                DBGuardar();
                if (commit)
                    GestorDatos.CommitTransaction();
            }
            catch (Exception ex)
            {
                if (commit)
                    GestorDatos.RollbackTransaction();
                throw new Exception("Error al Actualizar Pedido " + ex.Message);
            }
        }

        public void Guardar(bool commit)
        {
            try
            {
                GestorDatos.BeginTransaction();

                DBGuardar();

                if (this.Tipo == TipoPedido.Factura)
                {
                    ParametroSistema.IncrementarFactura(Compania, Zona);                    
                }
                else
                {
                    ParametroSistema.IncrementarPedido(Compania, Zona,CodigoConsecutivo);
                }
                if (commit)
                    GestorDatos.CommitTransaction();
            }
            catch (Exception ex)
            {
                if (commit)
                    GestorDatos.RollbackTransaction();
                throw new Exception("Error al guardar " + (this.Tipo == TipoPedido.Factura ? "Factura. " : "Pedido. ") + ex.Message);
            }
        }
        
        /// <summary>
        /// Modificaciones en funcionalidad de recibos de contado - KFC
        /// Metodo para anular Facturas
        /// </summary>
        /// <param name="commit"></param>
        /// <param name="ruta"></param>
        public void Anular(bool commit, string ruta)
        {
            try
            {
                GestorDatos.BeginTransaction();
                DBAnular();
                if (commit)
                    GestorDatos.CommitTransaction();
            }
            catch (Exception ex)
            {
                if (commit)
                    GestorDatos.RollbackTransaction();
                throw new Exception("Error al Anular " + (this.Tipo == TipoPedido.Factura ? "Factura. " : "Pedido. ") + ex.Message);
            }
        }

        #region Manipulacion Detalles

        public void ObtenerDetalles()
        {
            detalles.Encabezado.Compania = this.Compania;
            detalles.Encabezado.Numero = this.Numero;
            detalles.Encabezado.Zona = this.Zona;
            detalles.NivelPrecio = NivelPrecio;
            detalles.Bodega = this.Bodega;
            detalles.Obtener(configuracion.ClienteCia,PorcDescGeneral,configuracion.Compania.Activar_IVA_CR);
        }

        public void ObtenerDetalles(SQLiteConnection cnx)
        {
            detalles.Encabezado.Compania = this.Compania;
            detalles.Encabezado.Numero = this.Numero;
            detalles.Encabezado.Zona = this.Zona;
            detalles.NivelPrecio = NivelPrecio;
            detalles.Bodega = this.Bodega;
            detalles.Obtener(configuracion.ClienteCia, PorcDescGeneral,cnx,configuracion.Compania.Activar_IVA_CR);
        }

        #endregion

        #endregion

        #region serialize        

        /// <summary>
        /// Serializa el pedido y lo guarda en un archivo
        /// </summary>
        public static void serializarPedido(Pedido pedido, string cliente)
        {
            string folder = "";
#if DEBUG
            {
                if (!FRmConfig.GuardarenSD)
                {
                    folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                }
                else
                {
                    //folder = "/sdcard";
                    folder = App.ObtenerRutaSD();
                }
            }
#else
                        {
                            if (!FRmConfig.GuardarenSD)
                            {
                                folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                            }
                            else
                            {
                                //folder = "/sdcard";
                                //folder = Softland.ERP.FR.Mobile.App.ObtenerRutaSD();
                            }
                        }
#endif
            BorrarXMLPedido(cliente);
            //StreamWriter writer = new StreamWriter(Path.Combine(folder, "PedTemporal" + cliente + ".xml"));
            StreamWriter writer = new StreamWriter(Path.Combine(folder, "PedTemporal" + cliente + ".txt"));
            try
            {
                string json = Newtonsoft.Json.JsonConvert.SerializeObject(pedido);
                //XmlSerializer serializer = new XmlSerializer(pedido.GetType());                
                //serializer.Serialize(writer.BaseStream, pedido);
                writer.Write(json);
                writer.Close();
            }
            catch (Exception ex)
            {
                writer.Close();
            }
        }

        /// <summary>
        /// Deserializa un archivo y devuelve un objeto pedido
        /// </summary>
        /// <returns></returns>
        public static Pedido deserializarPedido(string cliente)
        {
            Pedido pedido = new Pedido();
            string folder = "";
#if DEBUG
            {
                if (!FRmConfig.GuardarenSD)
                {
                    folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                }
                else
                {
                    //folder = "/sdcard";
                    folder = App.ObtenerRutaSD();
                }
            }
#else
                        {
                            if (!FRmConfig.GuardarenSD)
                            {
                                folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                            }
                            else
                            {
                                //folder = "/sdcard";
                                //folder = Softland.ERP.FR.Mobile.App.ObtenerRutaSD();
                            }
                        }
#endif
            try
            {
                //XmlSerializer serializer = new XmlSerializer(pedido.GetType());                                
                //StreamReader reader = new StreamReader(Path.Combine(folder, "PedTemporal" + cliente + ".xml"));
                StreamReader reader = new StreamReader(Path.Combine(folder, "PedTemporal" + cliente + ".txt"));
                pedido = Newtonsoft.Json.JsonConvert.DeserializeObject<Pedido>(reader.ReadToEnd());
                //object deserialized = serializer.Deserialize(reader.BaseStream);
                //pedido = (Pedido)deserialized;                
                reader.Close();
            }
            catch (Exception ex)
            {

            }
            return pedido;
        }

        /// <summary>
        /// Verifica si existe un pedido temporal
        /// </summary>
        public static bool VerificaXMLPedido(string cliente)
        {
            string folder = "";
            string rutaArchivo;
#if DEBUG
            {
                if (!FRmConfig.GuardarenSD)
                {
                    folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                }
                else
                {
                    //folder = "/sdcard";
                    folder = App.ObtenerRutaSD();
                }
            }
#else
                        {
                            if (!FRmConfig.GuardarenSD)
                            {
                                folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                            }
                            else
                            {
                                //folder = "/sdcard";
                                //folder = Softland.ERP.FR.Mobile.App.ObtenerRutaSD();
                            }
                        }
#endif
            //Java.IO.File file = new Java.IO.File(folder, "PedTemporal" + cliente + ".xml");
            rutaArchivo = Path.Combine(folder, "PedTemporal" + cliente + ".txt");
            try 
            {
                if (File.Exists(rutaArchivo))
                {
                    using (StreamReader sr = new StreamReader(rutaArchivo))
                    {
                        if (sr.Peek() > -1)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }


        /// <summary>
        /// Borra el xml de pedido temporal
        /// </summary>
        public static void BorrarXMLPedido(string cliente)
        {
            string folder = "";
            string rutaArchivo;
#if DEBUG
            {
                if (!FRmConfig.GuardarenSD)
                {
                    folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                }
                else
                {
                    //folder = "/sdcard";
                    folder = App.ObtenerRutaSD();
                }
            }
#else
                        {
                            if (!FRmConfig.GuardarenSD)
                            {
                                folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                            }
                            else
                            {
                                //folder = "/sdcard";
                                //folder = Softland.ERP.FR.Mobile.App.ObtenerRutaSD();
                            }
                        }
#endif
            //Java.IO.File file = new Java.IO.File(folder, "PedTemporal" + cliente + ".xml");
            rutaArchivo = Path.Combine(folder, "PedTemporal" + cliente + ".txt");
            try
            {
                if (File.Exists(rutaArchivo))
                {
                    using (StreamReader sr = new StreamReader(rutaArchivo))
                    {
                        if (sr.Peek() > -1)
                        {
                            File.Delete(rutaArchivo);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }

        }

        #endregion


        #region Acceso Datos

        public static List<Pedido> ObtenerVentas(Cliente cliente, string zona, EstadoPedido estado, TipoPedido tipo)
        {
            List<Pedido> ventas = new List<Pedido>();
            ventas.Clear();

            string sentencia =
                " SELECT COD_CIA,NUM_PED,MON_SIV,MON_DSC,MON_IMP_VT,MON_IMP_CS,LST_PRE,"+
                "        FEC_PED,FEC_DES,HOR_INI,HOR_FIN,DESC1,DESC2,MONT_DESC1,MONT_DESC2,DIR_ENT,"+
                "        COD_PAIS,CLASE,COD_CND,COD_BOD,OBS_PED,DESCUENTO_CASCADA,IMPRESO,CONSIGNACION, NCF_PREFIJO, NCF, NIVEL_PRECIO, MONEDA ,CONSEC_RESOLUCION" +
                " FROM " + Table.ERPADMIN_alFAC_ENC_PED  + 
                " WHERE COD_CLT = @CLIENTE" +
                " AND   COD_ZON = @ZONA" +
                " AND   TIP_DOC = @TIPO" +
                " AND   ESTADO = @ESTADO" +
                " AND   DOC_PRO IS NULL ";

            SQLiteDataReader reader = null;
            SQLiteParameterList parametros = new SQLiteParameterList( new SQLiteParameter[] { 
                      new SQLiteParameter("@CLIENTE", cliente.Codigo),
                      new SQLiteParameter("@ZONA", zona),
                      new SQLiteParameter("@TIPO", ((char)tipo).ToString()),
                      new SQLiteParameter("@ESTADO", ((char)estado).ToString())}); 
            try
            {
                reader = GestorDatos.EjecutarConsulta(sentencia,parametros);

                while (reader.Read())
                {
                    Pedido pedido = new Pedido();
                    pedido.Cliente = cliente.Codigo;
                    pedido.Estado = estado;
                    pedido.Tipo = tipo;
                    pedido.Zona = zona;
                    pedido.Compania = reader.GetString(0);
                    pedido.Numero = reader.GetString(1);
                    pedido.MontoBruto = reader.GetDecimal(2);
                    pedido.MontoDescuentoLineas = reader.GetDecimal(3);
                    pedido.Impuesto = new Impuesto(reader.GetDecimal(4), reader.GetDecimal(5));
                    //RRDG INICIO BUG 18305
                    pedido.NivelPrecio = reader.GetString(26).ToString();
                    //RRDG FIN BUG 18305
                    pedido.FechaRealizacion = reader.GetDateTime(7);
                    pedido.FechaEntrega = reader.GetDateTime(8);
                    pedido.HoraInicio = reader.GetDateTime(9);
                    pedido.HoraFin = reader.GetDateTime(10);
                    pedido.PorcentajeDescuento1 = reader.GetDecimal(11);
                    pedido.PorcentajeDescuento2 = reader.GetDecimal(12);
                    pedido.MontoDescuento1 = reader.GetDecimal(13);
                    pedido.MontoDescuento2 = reader.GetDecimal(14);
                    pedido.DireccionEntrega = reader.GetString(15);
                    pedido.Configuracion = new ConfigDocCia();
                    pedido.Configuracion.Compania.Codigo = pedido.Compania;
                    pedido.Configuracion.Pais.Codigo = reader.GetString(16);
                    pedido.Configuracion.Clase = (ClaseDoc)Convert.ToChar(reader.GetString(17));
                    pedido.Configuracion.CondicionPago.Codigo = reader.GetString(18);
                    pedido.Configuracion.Nivel.Codigo = pedido.NivelPrecio.ToString();
                    pedido.Configuracion.ClienteCia = cliente.ObtenerClienteCia(pedido.Compania);

                    pedido.Bodega = reader.GetString(19);
                    if (!reader.IsDBNull(20))
                        pedido.Notas = reader.GetString(20);
                    pedido.DescuentoCascada = reader.GetString(21).Equals("S");				
                    pedido.Impreso = reader.GetString(22).Equals("S");
                    pedido.EsConsignacion = reader.GetString(23).Equals("S");
                    //if (!reader.IsDBNull(21))
                    //    pedido.NumRef = reader.GetString(20);                    
                    pedido.Configuracion.Cargar();
                    ventas.Add(pedido);
                }
                return ventas;
            }
            catch (SQLiteException ex)
            {
                throw new Exception("Error cargando " + ((tipo == TipoPedido.Factura) ? "facturas" : "pedidos") +"  del cliente. " + ex.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        /// <summary>
        /// Método para obtener todos los pedidos almacenados en la BD para ser sincronizados con el servidor.
        /// </summary>
        /// <param name="estado"></param>
        /// <param name="tipo"></param>
        /// <returns></returns>
        public static List<Pedido> ObtenerPedidos()
        {
            List<Pedido> ventas = new List<Pedido>();
            ventas.Clear();

            string sentencia =
                " SELECT COD_CIA,NUM_PED,MON_SIV,MON_DSC,MON_IMP_VT,MON_IMP_CS,LST_PRE," +
                "        FEC_PED,FEC_DES,HOR_INI,HOR_FIN,DESC1,DESC2,MONT_DESC1,MONT_DESC2,DIR_ENT," +
                "        COD_PAIS,CLASE,COD_CND,COD_BOD,OBS_PED,DESCUENTO_CASCADA,IMPRESO,CONSIGNACION, NCF_PREFIJO, NCF, NIVEL_PRECIO, MONEDA ,CONSEC_RESOLUCION,COD_CLT,COD_ZON,TIP_DOC,ESTADO" +
                " FROM " + Table.ERPADMIN_alFAC_ENC_PED +
                " WHERE DOC_PRO IS NULL ";
            string sentenciaB =
                " SELECT DISTINCT COD_CIA,COD_CLT " +                
                " FROM " + Table.ERPADMIN_alFAC_ENC_PED +
                " WHERE DOC_PRO IS NULL ";

            SQLiteDataReader reader = null;
            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {});

            try
            {                          
                
                reader = GestorDatos.EjecutarConsulta(sentencia, parametros);

                while (reader.Read())
                {
                    Pedido pedido = new Pedido();
                    pedido.Cliente = reader.GetString(29);                       
                    pedido.Estado = EstadoPedido.Aprobado;
                    pedido.Tipo = TipoPedido.Prefactura;
                    pedido.Zona = reader.GetString(30);
                    //pedido.usuario = reader.GetString(31);
                    pedido.usuario = "OSAKA";
                    pedido.Compania = reader.GetString(0);                    
                    pedido.Numero = reader.GetString(1);
                    pedido.MontoBruto = reader.GetDecimal(2);
                    pedido.MontoDescuentoLineas = reader.GetDecimal(3);
                    pedido.Impuesto = new Impuesto(reader.GetDecimal(4), reader.GetDecimal(5));
                    //RRDG INICIO BUG 18305
                    //pedido.NivelPrecio = reader.GetString(6).ToString();
                    pedido.NivelPrecio = reader.GetString(26).ToString();
                    //RRDG INICIO BUG 18305
                    pedido.FechaRealizacion = reader.GetDateTime(7);
                    pedido.FechaEntrega = reader.GetDateTime(8);
                    pedido.HoraInicio = reader.GetDateTime(9);
                    pedido.HoraFin = reader.GetDateTime(10);
                    pedido.PorcentajeDescuento1 = reader.GetDecimal(11);
                    pedido.PorcentajeDescuento2 = reader.GetDecimal(12);
                    pedido.MontoDescuento1 = reader.GetDecimal(13);
                    pedido.MontoDescuento2 = reader.GetDecimal(14);
                    pedido.DireccionEntrega = reader.GetString(15);
                    pedido.Configuracion = new ConfigDocCia();
                    pedido.Configuracion.Compania.Codigo = pedido.Compania;
                    pedido.Configuracion.Pais.Codigo = reader.GetString(16);
                    pedido.Configuracion.Clase = (ClaseDoc)Convert.ToChar(reader.GetString(17));
                    pedido.Configuracion.CondicionPago.Codigo = reader.GetString(18);
                    pedido.Configuracion.Nivel.Codigo = pedido.NivelPrecio.ToString();  
                    pedido.Bodega = reader.GetString(19);
                    if (!reader.IsDBNull(20))
                        pedido.Notas = reader.GetString(20);
                    pedido.DescuentoCascada = reader.GetString(21).Equals("S");
                    pedido.Impreso = reader.GetString(22).Equals("S");
                    pedido.EsConsignacion = reader.GetString(23).Equals("S");
                    //if (!reader.IsDBNull(21))
                    //    pedido.NumRef = reader.GetString(20);                                        
                    ventas.Add(pedido);
       
                }
                return ventas;
            }
            catch (SQLiteException ex)
            {
                throw new Exception("Error cargando " + "pedidos" + " . " + ex.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        /// <summary>
        /// Método para obtener todos los pedidos almacenados en la BD para ser sincronizados con el servidor.
        /// </summary>
        /// <param name="estado"></param>
        /// <param name="tipo"></param>
        /// <returns></returns>
        public static List<Pedido> ObtenerPedidos(SQLiteConnection cnx)
        {
            List<Pedido> ventas = new List<Pedido>();
            ventas.Clear();

            string sentencia =
                " SELECT COD_CIA,NUM_PED,MON_SIV,MON_DSC,MON_IMP_VT,MON_IMP_CS,LST_PRE," +
                "        FEC_PED,FEC_DES,HOR_INI,HOR_FIN,DESC1,DESC2,MONT_DESC1,MONT_DESC2,DIR_ENT," +
                "        COD_PAIS,CLASE,COD_CND,COD_BOD,OBS_PED,DESCUENTO_CASCADA,IMPRESO,CONSIGNACION, NCF_PREFIJO, NCF, NIVEL_PRECIO, MONEDA ,CONSEC_RESOLUCION,COD_CLT,COD_ZON,TIP_DOC,ESTADO,COD_PED,USUARIO,ERROR_SINCRONIZACION,BACKORDER " +
                " FROM " + Table.ERPADMIN_alFAC_ENC_PED +
                " WHERE DOC_PRO IS NULL ";
            string sentenciaB =
                " SELECT DISTINCT COD_CIA,COD_CLT " +
                " FROM " + Table.ERPADMIN_alFAC_ENC_PED +
                " WHERE DOC_PRO IS NULL ";

            SQLiteDataReader reader = null;
            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] { });

            try
            {

                reader = GestorDatos.EjecutarConsulta(sentencia, parametros,cnx);

                while (reader.Read())
                {
                    Pedido pedido = new Pedido();
                    pedido.Cliente = reader.GetString(29);
                    pedido.Estado = EstadoPedido.Aprobado;
                    pedido.Tipo = TipoPedido.Prefactura;
                    pedido.Zona = reader.GetString(30);
                    pedido.CodigoConsecutivo = reader.GetString(33);
                    pedido.usuario = reader.GetString(34);
                    pedido.Compania = reader.GetString(0);
                    pedido.Numero = reader.GetString(1);
                    pedido.MontoBruto = reader.GetDecimal(2);
                    pedido.MontoDescuentoLineas = reader.GetDecimal(3);
                    pedido.Impuesto = new Impuesto(reader.GetDecimal(4), reader.GetDecimal(5));
                    //RRDG INICIO BUG 18305
                    pedido.NivelPrecio = reader.GetString(26).ToString();
                    //RRDG FIN BUG 18305
                    pedido.FechaRealizacion = reader.GetDateTime(7);
                    pedido.FechaEntrega = reader.GetDateTime(8);
                    pedido.HoraInicio = reader.GetDateTime(9);
                    pedido.HoraFin = reader.GetDateTime(10);
                    pedido.PorcentajeDescuento1 = reader.GetDecimal(11);
                    pedido.PorcentajeDescuento2 = reader.GetDecimal(12);
                    pedido.MontoDescuento1 = reader.GetDecimal(13);
                    pedido.MontoDescuento2 = reader.GetDecimal(14);
                    pedido.DireccionEntrega = reader.GetString(15);
                    pedido.Configuracion = new ConfigDocCia();
                    pedido.Configuracion.Compania.Codigo = pedido.Compania;
                    pedido.Configuracion.Pais.Codigo = reader.GetString(16);
                    pedido.Configuracion.Clase = (ClaseDoc)Convert.ToChar(reader.GetString(17));
                    pedido.Configuracion.CondicionPago.Codigo = reader.GetString(18);
                    pedido.Configuracion.Nivel.Codigo = pedido.NivelPrecio.ToString();
                    pedido.Bodega = reader.GetString(19);
                    if (!reader.IsDBNull(20))
                        pedido.Notas = reader.GetString(20);
                    pedido.DescuentoCascada = reader.GetString(21).Equals("S");
                    pedido.Impreso = reader.GetString(22).Equals("S");
                    pedido.EsConsignacion = reader.GetString(23).Equals("S");
                    if (!reader.IsDBNull(35))
                        pedido.Motivo = reader.GetString(35);
                    if (!reader.IsDBNull(36))
                        pedido.Backorder = reader.GetString(36);
                    else
                        pedido.Backorder = "N";
                    ventas.Add(pedido);

                }
                return ventas;
            }
            catch (SQLiteException ex)
            {
                throw new Exception("Error cargando " + "pedidos" + " . " + ex.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        public static void ActualizarSincronizados(List<PedidosSincronizados> pedidosSincronizados, SQLiteConnection cnx)
        {
            string error = "";

            foreach(PedidosSincronizados pedido in pedidosSincronizados.Where(x => x.sincronizado))
            {
                string sentencia =
                    " UPDATE " + Table.ERPADMIN_alFAC_ENC_PED + " SET DOC_PRO = 'S',NUM_PED_ERP=@NUMPED,ERROR_SINCRONIZACION=@ERROR" +
                    " WHERE NUM_PED = @PEDIDO" +
                    " AND   COD_CIA = @COMPANIA";

                SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] { 
                          new SQLiteParameter("@PEDIDO", pedido.pedido),
                          new SQLiteParameter("@COMPANIA", pedido.compania),
                          new SQLiteParameter("@NUMPED", pedido.consecutivoERP),
                          new SQLiteParameter("@ERROR", string.Empty)
                });

                int encabezado = GestorDatos.EjecutarComando(sentencia, parametros, cnx);

                if (encabezado != 1)
                    //throw new Exception("No se pudo actualizar el encabezado del pedido '" + pedido.pedido + "'.");
                    error = "'" + pedido.pedido + "'";
            }

            foreach (PedidosSincronizados pedido in pedidosSincronizados.Where(x => !x.sincronizado))
            {
                string sentencia =
                    " UPDATE " + Table.ERPADMIN_alFAC_ENC_PED + " SET ERROR_SINCRONIZACION=@ERROR" +
                    " WHERE NUM_PED = @PEDIDO" +
                    " AND   COD_CIA = @COMPANIA";

                SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] { 
                          new SQLiteParameter("@PEDIDO", pedido.pedido),
                          new SQLiteParameter("@COMPANIA", pedido.compania),
                          new SQLiteParameter("@ERROR", pedido.error)
                });

                int encabezado = GestorDatos.EjecutarComando(sentencia, parametros, cnx);

                if (encabezado != 1)
                    //throw new Exception("No se pudo actualizar el encabezado del pedido '" + pedido.pedido + "'.");
                    error = "'" + pedido.pedido + "'";
            }
        }

        private void DBGuardar()
        {
            this.HoraFin = DateTime.Now;
            string serie = string.Empty;
            int? consecutivo = null;

            //Los montos de impuestos y descuentos se redondean a 2
            this.Impuesto.MontoImpuesto1 = Decimal.Round(this.Impuesto.MontoImpuesto1,2);
            this.Impuesto.MontoImpuesto2 = Decimal.Round(this.Impuesto.MontoImpuesto2,2);
            this.MontoDescuentoLineas = Decimal.Round(MontoDescuentoLineas, 2);
            this.MontoDescuento1 = Decimal.Round(MontoDescuento1, 2);
            this.MontoDescuento2 = Decimal.Round(MontoDescuento2, 2);
            
            decimal montoCIV = Decimal.Round(this.MontoBruto + this.Impuesto.MontoImpuesto1, 2);          

            //Se procede a guardar el encabezado
            string sentencia =
                " INSERT INTO " + Table.ERPADMIN_alFAC_ENC_PED +
                "        ( COD_CIA,  COD_ZON,  COD_CLT,  NUM_PED,  HOR_INI,  HOR_FIN,  FEC_PED,  FEC_DES,  NUM_ITM,  ESTADO, COD_BOD,  TIP_DOC,  LST_PRE,  MON_SIV,  MON_CIV,  MON_DSC,  MON_IMP_VT,  MON_IMP_CS,  IMPRESO,  CONSIGNACION,   OBS_PED,  DIR_ENT,   COD_PAIS,  CLASE, COD_CND,  DESC1,  DESC2,  MONT_DESC1,  MONT_DESC2,  DESCUENTO_CASCADA, NCF_PREFIJO, NCF, NIVEL_PRECIO, MONEDA,SERIE_RESOLUCION,COD_GEO1,COD_GEO2,CONSEC_RESOLUCION,USUARIO,COD_PED,BACKORDER) " +
                " VALUES (@COD_CIA, @COD_ZON, @COD_CLT, @NUM_PED, @HOR_INI, @HOR_FIN, @FEC_PED, @FEC_DES, @NUM_ITM, @ESTADO, @COD_BOD, @TIP_DOC, @NVL_PRE, @MON_SIV, @MON_CIV, @MON_DSC, @MON_IMP_VT, @MON_IMP_CS, @IMPRESO, @CONSIGNACION,  @OBS_PED, @DIR_ENT, @COD_PAIS, @CLASE, @COD_CND, @DESC1, @DESC2, @MONT_DESC1, @MONT_DESC2, @DESCUENTO_CASCADA, @NCF_PREFIJO, @NCF, @NVL_PRE_COD, @MONEDA,@SERIE_RESOLUCION,@GEO1,@GEO2,@CONSEC_RESOLUCION,@USUARIO,@COD_PED,@BACKORDER) ";  //DOC_PRO 

            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                GestorDatos.SQLiteParameter("@COD_CIA",SqlDbType.NVarChar, Compania),
                GestorDatos.SQLiteParameter("@COD_ZON",SqlDbType.NVarChar, Zona),
                GestorDatos.SQLiteParameter("@COD_CLT",SqlDbType.NVarChar, Cliente),
                GestorDatos.SQLiteParameter("@NUM_PED",SqlDbType.NVarChar, Numero),
                GestorDatos.SQLiteParameter("@HOR_INI",SqlDbType.DateTime, HoraInicio),
                GestorDatos.SQLiteParameter("@HOR_FIN",SqlDbType.DateTime, HoraFin), //LJR Caso 36309
                GestorDatos.SQLiteParameter("@FEC_PED",SqlDbType.DateTime, FechaRealizacion.ToShortDateString()),
                GestorDatos.SQLiteParameter("@FEC_DES",SqlDbType.DateTime, FechaEntrega.ToShortDateString()),
                GestorDatos.SQLiteParameter("@NUM_ITM",SqlDbType.Int, detalles.TotalLineas),
                GestorDatos.SQLiteParameter("@ESTADO", SqlDbType.NVarChar,((char)Estado).ToString()),
                GestorDatos.SQLiteParameter("@COD_BOD",SqlDbType.NVarChar, (EsConsignacion ? Configuracion.ClienteCia.Bodega :this.Bodega)),
                GestorDatos.SQLiteParameter("@TIP_DOC",SqlDbType.NVarChar, ((char)Tipo).ToString()),
                GestorDatos.SQLiteParameter("@NVL_PRE",SqlDbType.Int, Configuracion.Nivel.Codigo),
                GestorDatos.SQLiteParameter("@MON_SIV",SqlDbType.Decimal, MontoBruto),
                GestorDatos.SQLiteParameter("@MON_CIV",SqlDbType.Decimal, montoCIV),
                GestorDatos.SQLiteParameter("@MON_DSC",SqlDbType.Decimal, MontoDescuentoLineas),
                GestorDatos.SQLiteParameter("@MON_IMP_VT",SqlDbType.Decimal,Impuesto.MontoImpuesto1),
                GestorDatos.SQLiteParameter("@MON_IMP_CS", SqlDbType.Decimal,Impuesto.MontoImpuesto2),
                GestorDatos.SQLiteParameter("@IMPRESO",SqlDbType.NVarChar, (Impreso? "S" : "N")),
                GestorDatos.SQLiteParameter("@CONSIGNACION",SqlDbType.NVarChar, (EsConsignacion ? "S" : "N")),
                GestorDatos.SQLiteParameter("@OBS_DET",SqlDbType.NVarChar, Notas), 
                GestorDatos.SQLiteParameter("@DIR_ENT", SqlDbType.NVarChar, DireccionEntrega),
                GestorDatos.SQLiteParameter("@COD_PAIS", SqlDbType.NVarChar, Configuracion.Pais.Codigo),
                GestorDatos.SQLiteParameter("@CLASE", SqlDbType.NVarChar, ((char)Configuracion.Clase).ToString()),
                GestorDatos.SQLiteParameter("@COD_CND", SqlDbType.NVarChar, Configuracion.CondicionPago.Codigo),
                GestorDatos.SQLiteParameter("@DESC1",SqlDbType.Decimal, PorcentajeDescuento1), 
                GestorDatos.SQLiteParameter("@DESC2",SqlDbType.Decimal, PorcentajeDescuento2), 
                GestorDatos.SQLiteParameter("@MONT_DESC1",SqlDbType.Decimal,MontoDescuento1), 
                GestorDatos.SQLiteParameter("@MONT_DESC2",SqlDbType.Decimal,MontoDescuento2),
                GestorDatos.SQLiteParameter("@OBS_PED", SqlDbType.NVarChar, Notas),
                GestorDatos.SQLiteParameter("@DESCUENTO_CASCADA", SqlDbType.NVarChar,(DescuentoCascada? "S" : "N" )),
                //ABC Manejo NCF
                GestorDatos.SQLiteParameter("@NCF_PREFIJO", SqlDbType.NVarChar,string.Empty),
                GestorDatos.SQLiteParameter("@NCF", SqlDbType.NVarChar,string.Empty),

                //KFC Sincronización Listas de precio
                GestorDatos.SQLiteParameter("@NVL_PRE_COD",SqlDbType.NVarChar, NivelPrecioCod),
                GestorDatos.SQLiteParameter("@MONEDA",SqlDbType.NVarChar, Moneda),
                //Resoluciones
                GestorDatos.SQLiteParameter("@SERIE_RESOLUCION",SqlDbType.NVarChar, serie),
                //Retenciones
                GestorDatos.SQLiteParameter("@GEO1",SqlDbType.NVarChar, this.Configuracion.Pais.DivisionGeografica1),
                GestorDatos.SQLiteParameter("@GEO2",SqlDbType.NVarChar, this.Configuracion.Pais.DivisionGeografica2),
                GestorDatos.SQLiteParameter("@CONSEC_RESOLUCION",SqlDbType.Int, consecutivo),
                GestorDatos.SQLiteParameter("@USUARIO",SqlDbType.NVarChar, this.Usuario),
                GestorDatos.SQLiteParameter("@COD_PED",SqlDbType.NVarChar, this.CodigoConsecutivo),
                GestorDatos.SQLiteParameter("@BACKORDER",SqlDbType.NVarChar, Configuracion.ClienteCia.AceptaBackorder)

            });


            //GestorDatos.SQLiteParameter("@NUM_REF",SqlDbType.NVarChar, numRef),
            int encabezado = GestorDatos.EjecutarComando(sentencia, parametros);

            if (encabezado != 1)
                throw new Exception("No se puede guardar el encabezado " + (Tipo.Equals(TipoPedido.Factura) ? "de la factura '" : "del pedido'") + this.Numero + "'.");

            //se guardan los detalles de Pedido
            detalles.Bodega = Bodega;
            detalles.NivelPrecio = Configuracion.Nivel.Codigo;
            detalles.Tipo = Tipo;
            detalles.Encabezado = this;
            detalles.Guardar();
        }        

        private void DBEliminar()
        {
            string sentencia =
                " DELETE FROM " + Table.ERPADMIN_alFAC_ENC_PED +
                " WHERE COD_CIA = @COD_CIA" +
                " AND COD_ZON = @COD_ZON " +
                " AND NUM_PED = @NUM_PED";

            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                GestorDatos.SQLiteParameter("@COD_CIA", SqlDbType.NVarChar, Compania),
                GestorDatos.SQLiteParameter("@COD_ZON", SqlDbType.NVarChar, Zona),
                GestorDatos.SQLiteParameter("@NUM_PED", SqlDbType.NVarChar, Numero)});

            int rows = GestorDatos.EjecutarComando(sentencia, parametros);

            if (rows != 1)
                throw new Exception((rows < 1) ? "No se eliminó ningún registro de Pedido." : "Se eliminó más de un registro de Pedido.");

            detalles.Eliminar();
        }

        public void DBEliminar(SQLiteConnection cnx)
        {
            string sentencia =
                " DELETE FROM " + Table.ERPADMIN_alFAC_ENC_PED +
                " WHERE COD_CIA = @COD_CIA" +
                " AND COD_ZON = @COD_ZON " +
                " AND NUM_PED = @NUM_PED";

            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                GestorDatos.SQLiteParameter("@COD_CIA", SqlDbType.NVarChar, Compania),
                GestorDatos.SQLiteParameter("@COD_ZON", SqlDbType.NVarChar, Zona),
                GestorDatos.SQLiteParameter("@NUM_PED", SqlDbType.NVarChar, Numero)});

            int rows = GestorDatos.EjecutarComando(sentencia, parametros,cnx);

            if (rows != 1)
                throw new Exception((rows < 1) ? "No se eliminó ningún registro de Pedido." : "Se eliminó más de un registro de Pedido.");
            detalles.Encabezado.Numero = Numero;
            detalles.Encabezado.Compania = Compania;
            detalles.Eliminar(cnx);
        }

        private void DBAnular()
        {
            this.Estado = EstadoPedido.Cancelado;

            string sentencia =
                " UPDATE " + Table.ERPADMIN_alFAC_ENC_PED +
                " SET ESTADO = @ESTADO " +
                " WHERE COD_CIA = @COD_CIA " +
                " AND COD_ZON = @COD_ZON " +
                " AND NUM_PED = @NUM_PED " +
                " AND DOC_PRO IS NULL";

            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                GestorDatos.SQLiteParameter("@COD_CIA", SqlDbType.NVarChar, Compania),
                GestorDatos.SQLiteParameter("@COD_ZON", SqlDbType.NVarChar, Zona),
                GestorDatos.SQLiteParameter("@NUM_PED", SqlDbType.NVarChar, Numero),
                GestorDatos.SQLiteParameter("@ESTADO", SqlDbType.NVarChar, ((char)Estado).ToString())});

            int rows = GestorDatos.EjecutarComando(sentencia, parametros);

            if (rows != 1)
                throw new Exception((rows < 1) ? "No se anuló ningún registro de Pedido." : "Se anuló más de un registro de Pedido.");
        }

        public void DBActualizarImpresion()
        {
            string sentencia =
                " UPDATE " + Table.ERPADMIN_alFAC_ENC_PED  +
                " SET IMPRESO = @IMPRESO " +
                " WHERE COD_ZON = @COD_ZON " +
                " AND NUM_PED = @NUM_PED ";
                //" AND DOC_PRO IS NULL";

            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                GestorDatos.SQLiteParameter("@COD_CIA", SqlDbType.NVarChar, Compania),
                GestorDatos.SQLiteParameter("@COD_ZON", SqlDbType.NVarChar, Zona),
                GestorDatos.SQLiteParameter("@NUM_PED", SqlDbType.NVarChar, Numero),
                GestorDatos.SQLiteParameter("@IMPRESO", SqlDbType.NVarChar, (Impreso? "S" : "N"))});

            try
            {
                GestorDatos.EjecutarComando(sentencia, parametros);
            }
            catch (Exception ex)
            {
                throw new Exception("Error actualizando el indicador de impresión. " + ex.Message);
            }
        }
        
        /// <summary>
        /// Valida si existe el consecutivo en un pedido ya realizado.
        /// Si esto pasa se aumenta el consecutivo hasta que ya no se repita.
        /// </summary>
        /// <param name="consecutivo"></param>
        /// <param name="compania"></param>
        /// <returns>Consecutivo valido.</returns>
        /// ABC 09/10/2008 Caso:33376A
        /// Se ingresa la compania como parametro para la validación del cosecutivo siguiente
        public string ValidarExistenciaConsecutivo(string consecutivo)
        {
            int existe = 0;

            do
            {
                //ABC 09/10/2008 Caso:33376A
                //Se incluye la compañia como nueva condicion en la validación del consecutivo.
                string sentencia =
                    " SELECT COUNT(NUM_PED) FROM " + Table.ERPADMIN_alFAC_ENC_PED +
                    " WHERE NUM_PED = @CONSECUTIVO " +
                    " AND UPPER(COD_CIA) = @COMPANIA";

                SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {
                GestorDatos.SQLiteParameter("@CONSECUTIVO",SqlDbType.NVarChar,consecutivo),
                GestorDatos.SQLiteParameter("@COMPANIA",SqlDbType.NVarChar,Compania.ToUpper())});
                existe = (int)GestorDatos.EjecutarScalar(sentencia, parametros);

                if (existe != 0)
                {
                    //Bitacora.Escribir("El consecutivo '" + consecutivo + "' ya existe.");
                    consecutivo = GestorUtilitario.proximoCodigo(consecutivo, 20);
                }

            } while (existe != 0);

            return consecutivo;
        }

        public static bool HayPendientesCarga()
        {
            string sentencia = "SELECT COUNT(*) FROM " + Table.ERPADMIN_alFAC_ENC_PED + " WHERE DOC_PRO IS NULL";
            return (GestorDatos.NumeroRegistros(sentencia) != 0);     
        }

        private static bool FaltanDevolver(string pedido,string compania) 
        {
            try
            {
                decimal cant_fac=0;
                decimal cant_dev=0;
                cant_fac=Convert.ToDecimal(GestorDatos.EjecutarScalar(string.Format("SELECT SUM(CANT_FAC) FROM " + Table.ERPADMIN_alFAC_DET_HIST_FAC + " WHERE NUM_FAC='{0}' AND COD_CIA='{1}' AND EST_BON!='B'",pedido,compania)));
                cant_dev = Convert.ToDecimal(GestorDatos.EjecutarScalar(string.Format("SELECT SUM(CANT_DEV) FROM " + Table.ERPADMIN_alFAC_DET_HIST_FAC + " WHERE NUM_FAC='{0}' AND COD_CIA='{1}'", pedido, compania)));
                return ((cant_fac-cant_dev)>0);
            }
            catch
            {
                throw new Exception("Error verificando cantidad a devolver.");
            }
        }

        //ABC Caso 35136
        /// <summary>
        /// Carga las facturas historicas
        /// </summary>
        /// <param name="compania"></param>
        /// <returns></returns>
        public static List<Pedido> ObtenerHistoricoFactura(string cliente, string zona, string compania)
        {
            List<Pedido> lista = new List<Pedido>();

            //ABC Caso 37189
            //Se deja de condicionar por ruta, ya que el cliente puede pertenecer a varias rutas distintas
            string sentencia =
                "SELECT NUM_FAC,FEC_FAC,MON_FAC, NCF, COD_CIA,COD_CND FROM " + Table.ERPADMIN_alFAC_ENC_HIST_FAC +
                " WHERE UPPER(COD_CLT) = @CLIENTE" +

                " AND   UPPER(COD_CIA) = @COMPANIA " +
                " ORDER BY FEC_FAC ASC";

            SQLiteDataReader reader = null;
            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] { 
                      new SQLiteParameter("@CLIENTE", cliente.ToUpper()),

                      new SQLiteParameter("@COMPANIA", compania.ToUpper())});

            //new SQLiteParameter("@ZONA", zona),
            //" AND   COD_ZON = @ZONA " +

            try
            {
                reader = GestorDatos.EjecutarConsulta(sentencia, parametros);

                while (reader.Read())
                {
                    Pedido ped = new Pedido();

                    if (!reader.IsDBNull(4))
                        ped.Compania = reader.GetString(4);

                    if (!reader.IsDBNull(0))
                        ped.Numero = reader.GetString(0);

                    if (!reader.IsDBNull(2))
                        ped.MontoBruto = reader.GetDecimal(2);

                    if (!reader.IsDBNull(1))
                        ped.FechaRealizacion = reader.GetDateTime(1);

                    #region Modificaciones en funcionalidad de generacion de recibos de contado - KFC

                    ped.CondicionPago = new CondicionPago();
                    ped.CondicionPago.Codigo = reader.GetString(5);
                    ped.CondicionPago.DiasNeto = CondicionPago.ObtenerDiasNeto(ped.CondicionPago.Codigo, ped.Compania);
                    #endregion

                    ped.descLineasDevolucionDocumento = ped.ObtieneDescuentosLineasHistFactura(cliente, compania, ped.Numero);

                    if (FaltanDevolver(ped.Numero,ped.Compania))
                        lista.Add(ped);

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al cargar los historicos de los pedidos. " + ex.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            return lista;
        }

        public int ObtenerSaldoFacturaHistorica()
        {
            int valor = int.MinValue;
            decimal saldo_dolar = decimal.Zero;
            decimal saldo_local = decimal.Zero;
            decimal monto_dolar = decimal.Zero;
            decimal monto_local = decimal.Zero;
            
            string sentencia =
                "SELECT SALDO_DOLAR, SALDO_LOCAL, MONTO_DOLAR, MONTO_LOCAL" +
                " FROM " + Table.ERPADMIN_alCXC_PEN_COB +
                " WHERE COD_TIP_DC IN (@TIPO1, @TIPO2, @TIPO3, @TIPO4, @TIPO5, @TIPO6, @TIPO7) AND NUM_DOC = @FACTURA AND COD_CLT = @CLIENTE";


            SQLiteDataReader reader = null;
            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] { 
                      new SQLiteParameter("@FACTURA", this.Numero),
                      new SQLiteParameter("@CLIENTE", this.Cliente),
                      new SQLiteParameter("@TIPO1", (int)TipoDocumento.Factura),
                      new SQLiteParameter("@TIPO2", (int)TipoDocumento.NotaDebito),
                      new SQLiteParameter("@TIPO3", (int)TipoDocumento.LetraCambio),
                      new SQLiteParameter("@TIPO4", (int)TipoDocumento.OtroDebito),
                      new SQLiteParameter("@TIPO5", (int)TipoDocumento.Intereses),
                      new SQLiteParameter("@TIPO6", (int)TipoDocumento.BoletaVenta),
                      new SQLiteParameter("@TIPO7", (int)TipoDocumento.InteresCorriente)});
            try
            {
                reader = GestorDatos.EjecutarConsulta(sentencia, parametros);

                if (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        saldo_dolar = reader.GetDecimal(0);
                    if (!reader.IsDBNull(2))
                        saldo_local = reader.GetDecimal(1);
                    if (!reader.IsDBNull(1))
                        monto_dolar = reader.GetDecimal(2);
                    if (!reader.IsDBNull(1))
                        monto_local = reader.GetDecimal(3);

                    if (saldo_local == monto_local && saldo_dolar == monto_dolar)
                        valor = 1;
                    else if (saldo_local > 0 && saldo_dolar > 0)
                        valor = 2;
                    else
                        valor = 0;

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al cargar saldos de historico de facturas. " + ex.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            return valor;
        }

        private string ObtenerCondicionPagoPedido()
        {
            if ((this.CondicionPago!=null && this.CondicionPago.DiasNeto==0) ||this.configuracion.CondicionPago.Codigo == Pedidos.Core.Compania.Obtener(this.Compania).CondicionPagoContado
                )
            {
                return "Contado";
            }
            else
            {
                try
                {
                    //Corporativo.CondicionPago.ObtenerDiasNeto(condicionPago,compania)
                    //return "Crédito a " + this.configuracion.CondicionPago.DiasNeto + " día(s)";
                    if (this.CondicionPago != null)
                    {
                        return "Crédito a " + this.CondicionPago.DiasNeto + " día(s)";
                    }
                    else
                    {
                        return "Crédito a " + this.Configuracion.CondicionPago.DiasNeto + " día(s)";
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error obteniendo los días de crédito. " + ex.Message);
                }
            }
        }

        private string ObtenerFechaVenceFactura()
        {
            string resultado = string.Empty;
             
            try
            {
                if (tipo == TipoPedido.Factura)
                {
                    DateTime fechaVence = FechaRealizacion;
                    double dias=0;
                    if (this.CondicionPago != null)
                    {
                        dias = (double)(this.CondicionPago.DiasNeto);
                    }
                    else
                    {
                        dias = (double)(this.configuracion.CondicionPago.DiasNeto);
                    }
                    fechaVence = fechaVence.AddDays(dias);

                    resultado = fechaVence.ToString("dd/MM/yyyy");
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Error fecha vencimiento de factura. " + ex.Message);
            }

            return resultado;
        }

        #endregion 
        
        #region IPrintable Members

        public override string GetObjectName()
        {
            return "PEDIDO";
        }

        public override object GetField(string name)
        {
            switch (name)
            {
                //Caso:32682 ABC 23/05/2008 Mostrar Total de Articulo y Total de Lineas
                case "DETALLES": return detalles.ExploteLineas();
                case "TOTAL_ARTICULOS": return detalles.TotalArticulos;
                case "TOTAL_LINEAS": return detalles.TotalArticulos;
                case "COMPANIA": return this.Configuracion.Compania != null ? this.Configuracion.Compania.Nombre : string.Empty;
                case "COMPANIA_NIT": return this.Configuracion.Compania != null ? this.Configuracion.Compania.Nit : string.Empty;
                case "COMPANIA_DIRECCION": return this.Configuracion.Compania != null ? this.Configuracion.Compania.Direccion : string.Empty;
                //Caso CR1-04845-CM4Q LDA 05/05/2011
                case "CONDICIONPAGOPEDIDO": return ObtenerCondicionPagoPedido();
                case "FECHAVENCEFACTURA": return ObtenerFechaVenceFactura();
                default:
                    object field = this.Configuracion.ClienteCia.GetField(name);
                    if (!field.Equals(string.Empty))
                        return field;
                    else
                        return base.GetField(name);

            }
        }

        #region Modificaciones en funcionalidad de recibos de contado - KFC

        /// <summary>
        /// Metodo que busca y devuelve el recibo asociado a la factura de contado
        /// </summary>
        /// <param name="pedido"></param>
        /// <returns></returns>
        public string ReciboAsociado(string pedido)
        {
            string numRecibo = string.Empty;

            string sentencia =
                "SELECT NUM_REC" +
                " FROM " + Table.ERPADMIN_alCXC_MOV_DIR +
                " WHERE NUM_DOC_AF = (@FACTURA)";


            SQLiteDataReader reader = null;
            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] { 
                      new SQLiteParameter("@FACTURA", this.Numero)});
            try
            {
                reader = GestorDatos.EjecutarConsulta(sentencia, parametros);

                if (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        numRecibo = reader.GetString(0);
                }

                if (numRecibo.Length == 0)
                    numRecibo = string.Empty;
            }
            catch (Exception ex)
            {
                throw new Exception("Error verificando si existen documentos asociados a la Factura. " + ex.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return numRecibo;
        }

        
        /// <summary>
        /// Metodo que devuelve una lista con los codigos de las notas de credito aplicadas
        /// a la factura de contado
        /// </summary>
        /// <param name="numRecibo"></param>
        /// <returns></returns>
        public List<string> NotasCreditoAsociadas(string numRecibo)
        {
            List<string> notasAsociadas = new List<string>();

            string sentencia =
                "SELECT NUM_DOC FROM " + Table.ERPADMIN_alCXC_MOV_DIR +
                " WHERE COD_TIP_DC in ('7','15') AND NUM_REC = @RECIBO ";

            SQLiteDataReader reader = null;
            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] { 
                      new SQLiteParameter("@RECIBO", numRecibo)});    

            try
            {
                reader = GestorDatos.EjecutarConsulta(sentencia, parametros);

                while (reader.Read())
                {
                    string nota = string.Empty;

                    if (!reader.IsDBNull(0))
                        nota = reader.GetString(0);

                    notasAsociadas.Add(nota);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al cargar las notas de crédito asociadas. " + ex.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            return notasAsociadas;            
        }


        /// <summary>
        /// Le cambia el tipo al recibo en caso que este no se anule junto con la factura
        /// para que pueda subir como un recibo de credito a CC
        /// </summary>
        /// <param name="numRecibo"></param>
        public void CambiarTipoRecibo(string numRecibo)
        {
            string sentencia =
                " UPDATE " + Table.ERPADMIN_alCXC_DOC_APL +
                " SET IND_ANL = @IND_ANL " +
                " WHERE COD_ZON = @COD_ZON " +
                " AND NUM_REC = @NUM_REC ";

            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {               
                GestorDatos.SQLiteParameter("@COD_ZON", SqlDbType.NVarChar, Zona),
                GestorDatos.SQLiteParameter("@IND_ANL", SqlDbType.NVarChar, ((char)EstadoDocumento.Activo).ToString()),
                GestorDatos.SQLiteParameter("@NUM_REC", SqlDbType.NVarChar, numRecibo)});

            try
            {
                GestorDatos.EjecutarComando(sentencia, parametros);

                EliminarAplicacionesRecibo(numRecibo);
            }
            catch (Exception ex)
            {
                throw new Exception("Error cambiando el tipo de recibo. " + ex.Message);
            }

        }


        /// <summary>
        /// Elimina de la tabla ALCXC_MOV_DIR las aplicaciones de una factura anulada
        /// a la que no se le anulo el recibo asociado 
        /// </summary>
        /// <param name="numRecibo"></param>
        public void EliminarAplicacionesRecibo(string numRecibo)
        {
            string sentencia =
                " DELETE FROM " + Table.ERPADMIN_alCXC_MOV_DIR +
                " WHERE COD_ZON = @COD_ZON " +
                " AND NUM_REC = @NUM_REC ";

            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {               
                GestorDatos.SQLiteParameter("@COD_ZON", SqlDbType.NVarChar, Zona),
                GestorDatos.SQLiteParameter("@NUM_REC", SqlDbType.NVarChar, numRecibo)});

            try
            {
                GestorDatos.EjecutarComando(sentencia, parametros);
            }
            catch (Exception ex)
            {
                throw new Exception("Error cambiando el tipo de recibo. " + ex.Message);
            }
        }


        /// <summary>
        /// Reversa las notas de credito que fueron aplicadas a la factura que se esta anulando
        /// </summary>
        /// <param name="numRecibo"></param>
        public void ReversarNotasCredito(string numRecibo)
        {
            List<string> notasAsociadas = NotasCreditoAsociadas(numRecibo);

            foreach (string notaCredito in notasAsociadas)
            {
                ReversarAplicacionNota(numRecibo, notaCredito);
            }
        }

        
        /// <summary>
        /// funcion que reversa la palicacion de una N/C al anular una factura
        /// para que la nota pueda seguir viva
        /// </summary>
        /// <param name="numRecibo"></param>
        /// <param name="numNota"></param>
        private void ReversarAplicacionNota(string numRecibo, string numNota)
        {
            // Se obtienen los montos aplicados de la nota de credito
            decimal montoLocal = decimal.Zero;
            decimal montoDolar = decimal.Zero;

            string sentencia =
                "SELECT MON_MOV_LOCAL,MON_MOV_DOL" +
                " FROM " + Table.ERPADMIN_alCXC_MOV_DIR +
                " WHERE NUM_REC = (@RECIBO)" +
                " AND NUM_DOC = (@DOCUMENTO)" +
                " AND IND_ANL in (@IND_ANL,@IND_CONT)";


            SQLiteDataReader reader = null;
            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] { 
                      new SQLiteParameter("@RECIBO", numRecibo),
                      new SQLiteParameter("@DOCUMENTO", numNota),
                      new SQLiteParameter("@IND_ANL", ((char)EstadoDocumento.Anulado).ToString()),
                      new SQLiteParameter("@IND_CONT", ((char)EstadoDocumento.Contado).ToString())});
            try
            {
                reader = GestorDatos.EjecutarConsulta(sentencia, parametros);

                if (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                        montoLocal = reader.GetDecimal(0);
                    if (!reader.IsDBNull(1))
                        montoDolar = reader.GetDecimal(1);
                }

                //llamado a la funcion de actualizacion de saldos
                ActualizarSaldosNota(numNota, montoLocal, montoDolar);
                
            }
            catch (Exception ex)
            {
                throw new Exception("Error verificando si existen documentos asociados a la Factura. " + ex.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }         

        }


        /// <summary>
        /// Se actualizan los saldos de las notas de credito
        /// </summary>
        /// <param name="numNota"></param>
        /// <param name="saldoLocal"></param>
        /// <param name="saldoDolar"></param>
        private void ActualizarSaldosNota(string numNota,decimal saldoLocal, decimal saldoDolar)
        {

            string sentencia =
                " UPDATE " + Table.ERPADMIN_alCXC_PEN_COB +
                " SET SALDO_LOCAL = @SALDO_LOCAL ," +
                " SALDO_DOLAR = @SALDO_DOLAR " +
                " WHERE COD_ZON = @COD_ZON " +
                " AND NUM_DOC = @NUM_DOC ";            

            SQLiteParameterList parametros = new SQLiteParameterList(new SQLiteParameter[] {               
                GestorDatos.SQLiteParameter("@COD_ZON", SqlDbType.NVarChar, Zona),
                GestorDatos.SQLiteParameter("@SALDO_LOCAL", SqlDbType.NVarChar, saldoLocal),
                GestorDatos.SQLiteParameter("@SALDO_DOLAR", SqlDbType.NVarChar, saldoDolar),
                GestorDatos.SQLiteParameter("@NUM_DOC", SqlDbType.NVarChar, numNota)});

            try
            {
                GestorDatos.EjecutarComando(sentencia, parametros);
            }
            catch (Exception ex)
            {
                throw new Exception("Error actualizando el indicador de impresión. " + ex.Message);
            }

        }


        #endregion

        #endregion

    }
}
