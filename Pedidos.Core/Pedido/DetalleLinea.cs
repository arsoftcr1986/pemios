﻿using System;
using System.Text;
using System.Data.SQLiteBase;
using BI.Shared;

namespace Pedidos.Core
{
    /// <summary>
    /// Detalle de una linea base de documento con precios, impuestos y descuentos
    /// </summary>
    public abstract class DetalleLinea : DetalleDocumento, IPrintable
    {        

        #region Precios

        private decimal montoTotal = 0;
        /// <summary>
        /// Monto total Bruto de la línea (sin impuestos ni descuentos).
        /// </summary>
        public decimal MontoTotal
        {
            get { return montoTotal; }
            set
            { //cambios para Guatemala.
                montoTotal = GestorUtilitario.Round(value, FRmConfig.CantidadDecimales, true);
            }
        }

        /// <summary>
        /// Monto neto de la linea
        /// </summary>
        public decimal MontoNeto
        {
            get { return montoTotal + impuesto.MontoTotal - montoDescuento;  }
        }

        private Precio precio;
        /// <summary>
        /// Contiene precio almacen y detalle de la línea
        /// </summary>
        public Precio Precio
        {
            get { return precio; }
            set { precio = value; }
        }

        #endregion

        #region Impuestos

        private Impuesto impuesto = new Impuesto();
        /// <summary>
        /// Contiene impuestos de la línea
        /// </summary>
        public Impuesto Impuesto
        {
            get { return impuesto; }
            set { impuesto = value; }
        }
        #endregion

        #region Descuento

        private decimal montoDescuento = 0;
        /// <summary>
        /// Monto de descuento de la linea.
        /// </summary>
        public decimal MontoDescuento
        {
            get { return montoDescuento; }
            set { montoDescuento = value; }
        }

        private decimal porcentajeDescuento = 0;
        /// <summary>
        /// Porcentaje de descuento de la linea.
        /// </summary>
        public decimal PorcentajeDescuento
        {
            get { return porcentajeDescuento; }
            set { porcentajeDescuento = value; }
        }

        private Descuento descuento;
        /// <summary>
        /// Contiene detalle de Descuento de la línea.
        /// </summary>
        public Descuento Descuento
        {
            get { return descuento; }
            set { descuento = value; }
        }

        private Descuento descuentoAnt;
        /// <summary>
        /// Contiene detalle de Descuento de la línea.
        /// </summary>
        public Descuento DescuentoAnt
        {
            get { return descuentoAnt; }
            set { descuentoAnt = value; }
        }  

        #endregion

        #region Constructor

        public DetalleLinea()
        { }
        #endregion

		#region Metodos de instancia

        /// <summary>
        /// Recalcula el monto de la línea y el monto del descuento.
        /// </summary>
        /// <param name="cliente">Datos del cliente sobre el que se realiza el calculo</param>
        /// <param name="descuento">Indica si carga el descuento de linea</param>
        public void CalcularMontos(ClienteCia cliente, bool descuento)
        {
            //Calculamos el monto de la linea
            decimal montoAlmacen = this.UnidadesAlmacen * this.Articulo.Precio.Maximo;
            decimal montoDetalle = this.UnidadesDetalle * this.Articulo.Precio.Minimo;
            this.MontoTotal = montoAlmacen + montoDetalle;

            if(descuento)
                this.Descuento = Descuento.ObtenerDescuento(cliente, Articulo, UnidadesAlmacen);

            //Recalculamos el monto de descuento de la línea.
            CalcularMontoDescuento();

            if (descuento)
            {
                this.Articulo.CargarImpuesto();
                //Calculamos el impuesto de venta.
                this.Impuesto.ImpuestoVentaLinea(this.MontoTotal, this.MontoDescuento, cliente.Descuento / 100, cliente.ExoneracionImp1);

                //Calculamos el impuesto de consumo
                this.Impuesto.ImpuestoConsumoLinea(this.MontoTotal, this.MontoDescuento, cliente.Descuento / 100, cliente.ExoneracionImp2);
            }
        }

        /// <summary>
        /// Recalcula el monto de la línea y el monto del descuento.
        /// </summary>
        /// <param name="cliente">Datos del cliente sobre el que se realiza el calculo</param>
        /// <param name="descuento">Indica si carga el descuento de linea</param>
        /// <param name="porcDescuento">Indica el porcentaje del descuento de linea</param>
        public void CalcularMontos(ClienteCia cliente, bool descuento, decimal porcDescuento)
        {
            //Calculamos el monto de la linea
            decimal montoAlmacen = this.UnidadesAlmacen * this.Articulo.Precio.Maximo;
            decimal montoDetalle = this.UnidadesDetalle * this.Articulo.Precio.Minimo;
            this.MontoTotal = montoAlmacen + montoDetalle;
            
            if (this.Descuento == null)
            {
                this.Descuento = new Descuento();
            }
            if (porcDescuento > 0)
                this.Descuento.Tipo = TipoDescuento.Porcentual;
            this.Descuento.Monto = porcDescuento;
            
            //Recalculamos el monto de descuento de la línea.
            CalcularMontoDescuento();

            if (descuento)
            {
                this.Articulo.CargarImpuesto();
                //Calculamos el impuesto de venta.
                this.Impuesto.ImpuestoVentaLinea(this.MontoTotal, this.MontoDescuento, cliente.Descuento / 100, cliente.ExoneracionImp1);

                //Calculamos el impuesto de consumo
                this.Impuesto.ImpuestoConsumoLinea(this.MontoTotal, this.MontoDescuento, cliente.Descuento / 100, cliente.ExoneracionImp2);
            }
        }

        /// <summary>
        /// Recalcula el monto de la línea y el monto del descuento.
        /// </summary>
        /// <param name="cliente">Datos del cliente sobre el que se realiza el calculo</param>
        public void CalcularMontos(ClienteCia cliente)
        {            
            this.CalcularMontos(cliente, true);
        }

        /// <summary>
        /// Recalcula el monto de la línea y el monto del descuento.
        /// </summary>
        public void CalcularMontos()
        {
            this.CalcularMontos(null, false);
        }

        /// <summary>
        /// Recalcula el monto de la línea y el monto del descuento.
        /// </summary>
        /// <param name="cliente">Datos del cliente sobre el que se realiza el calculo</param>
        /// <param name="DescLinea">Datos del cliente sobre el que se realiza el calculo</param>
        public void CalcularMontos(ClienteCia cliente, decimal DescLinea)
        {
            this.CalcularMontos(cliente, true, DescLinea);
        }

        /// <summary>
        /// Calcula el porcentaje de descuento aplicado a la linea.
        /// </summary>
        /// <returns></returns>
        public decimal CalcularPorcentajeDescuento()
        {
            //decimal porcentajeDescuento = 0;

            if (this.Descuento != null)
            {
                if (this.Descuento.Tipo == TipoDescuento.Porcentual)
                    porcentajeDescuento = this.Descuento.Monto;
                else
                {
                    try
                    {
                        porcentajeDescuento = (this.Descuento.Monto * 100) / this.MontoTotal;
                    }
                    catch (Exception) { porcentajeDescuento = 0; }
                }
            }
            return porcentajeDescuento;
        }

        /// <summary>
        /// Calcula el monto de descuento aplicado a la linea.
        /// </summary>
        /// <returns></returns>
        public decimal CalcularMontoDescuento()
        {
            if (this.Descuento != null)
            {
                if (this.Descuento.Tipo == TipoDescuento.Porcentual)
                    //Recalculamos el monto de descuento de la línea.
                    montoDescuento = montoTotal * Descuento.Monto / 100;
                else
                    montoDescuento = Descuento.Monto;
            }
            return montoDescuento;
        }

        /// <summary>
        /// Metodo que calcula el monto de la linea
        /// Es importante recalcar que por linea no se incluye ningun tipo de
        /// impuestos ni descuentos.
        /// </summary>
        public void CalcularMontoLinea()
        {
            //Caso 28086 LDS 04/05/2007
            //Se cambia porque se puede cambiar el precio durante la toma del pedido o factura.	
		    //TODO: Redondear decimal en el cálculo de los montos segun el parámetro de FA
            decimal montoAlmacen = UnidadesAlmacen * Precio.Maximo;
            decimal montoDetalle = UnidadesDetalle * Precio.Minimo;
            this.MontoTotal = montoAlmacen + montoDetalle;
        }
        /// <summary>
        /// Aumentar la existencia del articulo asociado a la linea
        /// </summary>
        public void AumentarExistencia()
        {
            Articulo.ActualizarBodega(TotalAlmacen);
        }

        /// <summary>
        /// Aumentar la existencia del articulo envase asociado a la linea
        /// </summary>
        public void AumentarExistenciaEnvase(string pBodega)
        {
            Articulo.CargarArticuloEnvase();
            if (Articulo.ArticuloEnvase != null)
            {
                Articulo.ArticuloEnvase.Bodega = new Bodega(Articulo.ArticuloEnvase, pBodega);
                Articulo.ArticuloEnvase.ActualizarBodega(TotalAlmacen);
            }
        }

        /// <summary>
        /// Disminuir la existencia del articulo asociado a la linea
        /// </summary>
        public void DisminuirExistencia()
        {
            Articulo.ActualizarBodega(TotalAlmacen * -1);
        }

        /// <summary>
        /// Disminuir la existencia del articulo asociado a la linea
        /// </summary>
        public void DisminuirExistenciaEnvase(string pBodega)
        {
            Articulo.CargarArticuloEnvase();
            if (Articulo.ArticuloEnvase != null)
            {
                Articulo.ArticuloEnvase.Bodega = new Bodega(Articulo.ArticuloEnvase, pBodega);
                Articulo.ArticuloEnvase.ActualizarBodega(TotalAlmacen * -1);
            }
        }


        #endregion

        #region IPrintable Members

        public override string GetObjectName()
        {
            return "DETALLE_LINEA";
        }

        public override object GetField(string name)
        {
            switch (name)
            {
                //Caso: 32679 ABC 20/05/2008 Carga constante del Reporte de Inventario
                case "PRECIO_UNITARIO": return this.Precio.Minimo;
                case "PRECIO_ALMACEN": return this.Precio.Maximo;
                case "MONTO": return this.montoTotal;

                //Caso 28803 LDS 14/09/2007
                case "DESCUENTO_MONTO": return this.MontoDescuento;
                case "DESCUENTO_PORCENTAJE": return this.CalcularPorcentajeDescuento();
                default:
                    return base.GetField(name);
            }
        }
        #endregion
    }
}
