﻿using System;

namespace BI.Android
{
	public enum enumTypesRelationship
	{
		NO_ACTION = 0,
		SET_NULL = 1,
		SET_DEFAULT = 2,
		CASCADE = 3,
		RESTRICT = 4,
		NULL = 5
	}
}

