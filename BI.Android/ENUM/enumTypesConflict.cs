﻿using System;

namespace BI.Android
{
	public enum enumTypesConflict
	{
		ROLLBACK = 0,
		ABORT = 1,
		FAIL = 2,
		IGNORE = 3,
		REPLACE = 4,
		NULL = 5
	}
}

