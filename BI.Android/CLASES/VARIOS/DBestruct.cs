﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLiteBase;
using BI.Shared;
using System.IO;
using Newtonsoft.Json;

namespace BI.Android
{
    public class DBestruct
    {
        DB baseDatos;
        string dispositivo;
        List<BI.Android.Sentencias.Sentencia> sentenciasCreate;
        List<BI.Android.Sentencias.Sentencia> sentenciasInserts;

        public DBestruct()
        {
            //Inicializa la instancia de la clase y crea o abre la base de datos
            baseDatos = new DB("miBase");
            CrearEstructura();
        }

        public DBestruct(string path, SQLiteConnection cnx)
        {
            //Inicializa la instancia de la clase y crea o abre la base de datos
            baseDatos = new DB(cnx);
        }

        public DBestruct(string path, List<BI.Android.Sentencias.Sentencia> psentenciasCreate)
        {
            //Inicializa la instancia de la clase y crea o abre la base de datos
            baseDatos = new DB("miBase", path);
            sentenciasCreate = psentenciasCreate;
            CrearEstructuraLista();
        }

        public DBestruct(string path, List<BI.Android.Sentencias.Sentencia> psentenciasCreate,bool insertarDatos, SQLiteConnection cnx,string dispositivo)
        {
            this.dispositivo = dispositivo;
            //Inicializa la instancia de la clase y crea o abre la base de datos    
            baseDatos = new DB(cnx);
            sentenciasCreate = psentenciasCreate;
            if (sentenciasCreate.Count > 0)
                CrearEstructuraLista();
            if (insertarDatos)
                EjecutarInsertsLista(path);            
        }

        public void CrearEstructura()
        {
            //TABLA alFAC_PAIS
            Table alFAC_PAIS = new Table("alFAC_PAIS");
            alFAC_PAIS.Columns.Add(new column("PAIS", enumTypesColumn.VARCHAR, false, false, "", 4));
            alFAC_PAIS.Columns.Add(new column("COD_CIA", enumTypesColumn.VARCHAR, false, false, "", 10));
            alFAC_PAIS.Columns.Add(new column("NOMBRE", enumTypesColumn.VARCHAR, false, false, "", 40));
            alFAC_PAIS.Columns.Add(new column("ETIQUETA_DIV_GEO1", enumTypesColumn.VARCHAR, false, true, "", 20));
            alFAC_PAIS.Columns.Add(new column("ETIQUETA_DIV_GEO2", enumTypesColumn.VARCHAR, false, true, "", 20));
            //Crea las tablas;
            baseDatos.CreateTable(alFAC_PAIS);
            baseDatos.DeleteRecords(alFAC_PAIS);

        }

        public void CrearEstructuraLista()
        {
            foreach (BI.Android.Sentencias.Sentencia sen in sentenciasCreate)
            {
                if (!sen.Tabla.Equals(BI.Shared.Table.ERPADMIN_alFAC_DET_PED) && !sen.Tabla.Equals(BI.Shared.Table.ERPADMIN_alFAC_ENC_PED))
                    baseDatos.DropTableAction(sen.Tabla);
                baseDatos.CreateTableAction(sen.Sql, sen.Tabla, sen.Nombre);
                //baseDatos.DeleteRecordsAction(sen.Tabla);
            }
        }

        public void EjecutarInsertsLista(string path)
        {
            baseDatos.cnx.BeginTransaction();
            try
            {
                string ruta = Path.Combine(path, dispositivo + "_sincro");
                if (Directory.Exists(ruta))
                {
                    int count = Directory.GetFiles(ruta).Length + 1;
                    for (int i = 0; i <= count; i++)
                    {
                        string archivo = Path.Combine(ruta, "Archive_" + i + ".json");
                        if (!File.Exists(archivo))
                        {
                            break;
                        }
                        Sentencias.Sentencia sen = JsonConvert.DeserializeObject<Sentencias.Sentencia>(FileTools.ReadFileString(archivo));
                        baseDatos.ExecuteScriptActionIndividual(sen.Sqls[0], sen.Tabla, sen.Nombre, sen.SqlListparams[0]);
                        sen = null;
                       
                    }
                }
                GC.Collect(GC.MaxGeneration);                
                if (Directory.Exists(ruta))
                {
                    Directory.Delete(ruta, true);
                }
            }
            catch (Exception ex)
            {
                baseDatos.cnx.Rollback();
                throw ex;
            }
            finally
            {
                baseDatos.cnx.Commit();
            }
            
        }

        public List<string> ObtenerRegistros(string tabla)
        {
            return baseDatos.ObtenerRegistros(tabla);
        }
    }
}