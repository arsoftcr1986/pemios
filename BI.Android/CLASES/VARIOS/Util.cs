﻿using System;
using Android.Content;
using Android.App;

namespace BI.Android
{
	public class Util
	{
        public Util ()
		{
		}
		/// <summary>
		/// Muestra un mensaje y realiza la acción indicada en el botón de aceptar
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="title">Title.</param>
		/// <param name="Message">Message.</param>
		/// <param name="Icon">Icon.</param>
		/// <param name="OKButtonName">OK button name.</param>
		/// <param name="Event">Button Event.</param>
		public static void showMessage(Context context, string title, string Message, int Icon,string OKButtonName, EventHandler<DialogClickEventArgs> Event)
		{
			AlertDialog.Builder alert = new AlertDialog.Builder(context);
			alert.SetTitle(title);
			alert.SetMessage(Message);
			alert.SetIcon(Icon);
			if (!string.IsNullOrEmpty(OKButtonName) && Event != null)
				alert.SetNeutralButton(OKButtonName, Event);
			alert.Show();
		}
		/// <summary>
		/// Muestra un mensaje con un tema en específico y realiza la acción indicada en el botón de aceptar
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="Theme">Theme.</param>
		/// <param name="title">Title.</param>
		/// <param name="Message">Message.</param>
		/// <param name="Icon">Icon.</param>
		/// <param name="OKButtonName">OK button name.</param>
		/// <param name="Event">Event.</param>
		public static void showMessage(Context context, int Theme, string title, string Message, int Icon,string OKButtonName, EventHandler<DialogClickEventArgs> Event)
		{
			AlertDialog.Builder alert = new AlertDialog.Builder(context, Theme);
			alert.SetTitle(title);
			alert.SetMessage(Message);
			alert.SetIcon(Icon);
			if (!string.IsNullOrEmpty(OKButtonName) && Event != null)
				alert.SetNeutralButton(OKButtonName, Event);
			alert.Show();
		}
		/// <summary>
		/// Shows a message confirm.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="title">Title.</param>
		/// <param name="Message">Message.</param>
		/// <param name="Icon">Icon.</param>
		/// <param name="acceptName">Accept name.</param>
		/// <param name="AcceptEvent">Accept event.</param>
		/// <param name="cancelName">Cancel name.</param>
		/// <param name="cancelEvent">Cancel event.</param>
		public static void showConfirm(Context context, string title, string Message, int Icon, string acceptName, EventHandler<DialogClickEventArgs> AcceptEvent, string cancelName, EventHandler<DialogClickEventArgs> cancelEvent)
		{
			AlertDialog.Builder alert = new AlertDialog.Builder(context);
			alert.SetTitle(title);
			alert.SetMessage(Message);
			alert.SetIcon(Icon);
			if (!string.IsNullOrEmpty(acceptName) && AcceptEvent != null)
				alert.SetPositiveButton (acceptName, AcceptEvent);
			if (!string.IsNullOrEmpty(cancelName) && cancelEvent != null)
				alert.SetNegativeButton (cancelName, cancelEvent);
			alert.Show();
		}
		/// <summary>
		/// Shows a message confirm.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="Theme">Theme.</param>
		/// <param name="title">Title.</param>
		/// <param name="Message">Message.</param>
		/// <param name="Icon">Icon.</param>
		/// <param name="acceptName">Accept name.</param>
		/// <param name="AcceptEvent">Accept event.</param>
		/// <param name="cancelName">Cancel name.</param>
		/// <param name="cancelEvent">Cancel event.</param>
		public static void showConfirm(Context context, int Theme, string title, string Message, int Icon, string acceptName, EventHandler<DialogClickEventArgs> AcceptEvent, string cancelName, EventHandler<DialogClickEventArgs> cancelEvent)
		{
			AlertDialog.Builder alert = new AlertDialog.Builder(context, Theme);
			alert.SetTitle(title);
			alert.SetMessage(Message);
			alert.SetIcon(Icon);
			if (!string.IsNullOrEmpty(acceptName) && AcceptEvent != null)
				alert.SetPositiveButton (acceptName, AcceptEvent);
			if (!string.IsNullOrEmpty(cancelName) && cancelEvent != null)
				alert.SetNegativeButton (cancelName, cancelEvent);
			alert.Show();
		}
	}
}

