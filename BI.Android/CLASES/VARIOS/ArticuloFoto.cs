using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BI.Android
{
    public class ArticuloFoto
    {
        string name;


        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        string base64str;

        public string Base64str
        {
            get { return base64str; }
            set { base64str = value; }
        }

        public ArticuloFoto(string name, string base64str, string compania)
        {
            Name = name;
            Base64str = base64str;
            Compania = compania;
        }

        string compania;

        public string Compania
        {
            get { return compania; }
            set { compania = value; }
        }

        public void GuardarImagen(string currentDirectory)
        {
            //System.IO.Stream s = new System.IO.MemoryStream(Convert.FromBase64String(Base64str));
            byte[] arr = Convert.FromBase64String(Base64str);
            //Image img = ImageUtilities.Base64ToImage(Base64str);
            string dir = System.IO.Path.Combine(currentDirectory, Compania);
            //Name = Name.Replace('/', '-');
            string codImage = Name.Replace('/', '-');
            string file = System.IO.Path.Combine(dir, codImage + ".jpg");
            if (!System.IO.Directory.Exists(dir))
            {
                System.IO.Directory.CreateDirectory(dir);
            }
            if (System.IO.File.Exists(file))
            {
                System.IO.File.Delete(file);
            }
            using (System.IO.FileStream fs = new System.IO.FileStream(file, System.IO.FileMode.CreateNew))
            {
                //fs.Name;
                //if (Name.Contains("/")) fs.Name.Replace('-', '/');
                fs.Write(arr, 0, arr.Length);
            }

            //NSFileManager.SetSkipBackupAttribute(file, true); // Evitar que este archivo sea respaldado por iOS.
            System.IO.File.WriteAllBytes(file, arr);

        }

        public void limpiarObjeto()
        {
            Base64str = null;
        }

    }
}