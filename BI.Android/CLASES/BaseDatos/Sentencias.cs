using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLiteBase;
using Newtonsoft.Json;
using Android.Runtime;

namespace BI.Android
{
    [Preserve(AllMembers = true)]
    public class Sentencias
    {
        string path;
        public string webSoporte;
        public bool CambiarDescuento;
        public List<Sentencia> listaSentencias { get; set; }

        public Sentencias(string pPath)
        {
            this.path = pPath;
            this.listaSentencias = new List<Sentencia>();
        }

        public Sentencias()
        {
            this.path = string.Empty;
            this.listaSentencias = new List<Sentencia>();
        }

        private void LeerJSON()
        {
            string ruta = System.IO.Path.Combine(path, "sentencias.json");
            string json = System.IO.File.ReadAllText(ruta);
            listaSentencias = JsonConvert.DeserializeObject<List<Sentencia>>(json);
        }

        private void LeerJSON(string pJson)
        {
            listaSentencias = JsonConvert.DeserializeObject<List<Sentencia>>(pJson);
        }

        public bool verificarExist()
        {
            string ruta = System.IO.Path.Combine(path, "sentencias.json");
            return System.IO.File.Exists(ruta);
        }

        public string devolverSentencia(string name)
        {
            return listaSentencias.Find(x => x.Nombre.ToUpper() == name.ToUpper()).Sql;
        }

        public Sentencia devolverSentenciaObject(string name)
        {
            return listaSentencias.Find(x => x.Nombre.ToUpper() == name.ToUpper());
        }

        [Preserve(AllMembers = true)]
        public class Sentencia
        {
            public string Nombre { get; set; }
            public string Tabla { get; set; }
            public string Sql { get; set; }
            public List<string> Sqls { get; set; }
            public List<Dictionary<string, object>> Sqlparams { get; set; }
            public List<SQLiteParameterList> SqlListparams { get; set; }
            public TipoSentencia Tipo { get; set; }
            public bool TablaGlobal { get; set; }
        }
    }

    public enum TipoSentencia
    {
        Login = 0,
        LlenarTabla = 1,
        CrearTabla = 2,
        LlenarTodo = 3,
        CrearTodo = 4,
        InsertarPedido = 5,
        InsertarPedidos = 6,
        Imagenes = 7,
        CrearParcial = 8,
        LlenarParcial = 9
    }
}