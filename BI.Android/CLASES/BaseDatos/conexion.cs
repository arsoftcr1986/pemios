using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Data;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Data.SQLiteBase;
using BI.Shared;
using Newtonsoft.Json.Serialization;
using System.Reflection;
using Java.Util.Zip;
//using System.Runtime.Serialization.Json;

namespace BI.Android
{
    public class conexion
    {
        #region propiedades

        //const string servidor = "http://10.0.1.17:9000/";
        static string servidor = string.Empty;

        public bool continuarInsertar
        {
            get;
            set;
        }
        public string atrServer
        {
            get;
            set;
        }
        public string atrTipoBD
        {
            get;
            set;
        }
        public string atrDatabase
        {
            get;
            set;
        }
        public string atrCompania
        {
            get;
            set;
        }
        public string atrUser
        {
            get;
            set;
        }
        public string atrPass
        {
            get;
            set;
        }
        public string atrJson
        {
            get;
            set;
        }
        public int atrRegistros
        {
            get;
            set;
        }
        public string atrDispositivo
        {
            get;
            set;
        }
        public string atrUsuario
        {
            get;
            set;
        }

        public string atrWebSoporte
        {
            get;
            set;
        }

        public bool atrCambiarDescuento
        {
            get;
            set;
        }

        List<BI.Android.Sentencias.Sentencia> sentenciasCreate = new List<BI.Android.Sentencias.Sentencia>();
        List<BI.Android.Sentencias.Sentencia> sentenciasInserts = new List<BI.Android.Sentencias.Sentencia>();

        public DataTable dtDatos = new DataTable();

        byte[] fotosZip;

        byte[] SincroZip;

        #endregion

        #region constructor

        public conexion(string server, string database, string compania, string user, string pass, int registros, string tipobd)
        {
            atrServer = server;
            atrDatabase = database;
            atrCompania = compania;
            atrUser = user;
            atrPass = pass;
            atrRegistros = registros;
            atrTipoBD = tipobd;
        }

        public conexion(string server, string database, string compania, string user, string pass, string tipobd)
        {
            atrServer = server;
            atrDatabase = database;
            atrCompania = compania;
            atrUser = user;
            atrPass = pass;
            atrRegistros = -1;
            atrTipoBD = tipobd;
        }

        public conexion(string dispositivo,string usuario)
        {
            atrDispositivo = dispositivo;
            atrUsuario = usuario;
        }

        public conexion(string dispositivo, string pServidor,string usuario)
        {
            atrDispositivo = dispositivo;
            atrUsuario = usuario;

            servidor = pServidor;
            continuarInsertar = false;
        }

        #endregion

        //public class NullableValueProvider : IValueProvider
        //{
        //    private readonly object _defaultValue;
        //    private readonly IValueProvider _underlyingValueProvider;


        //    public NullableValueProvider(MemberInfo memberInfo, Type underlyingType)
        //    {
        //        _underlyingValueProvider = new Newtonsoft.Json.Serialization. DynamicValueProvider(memberInfo);
        //        _defaultValue = Activator.CreateInstance(underlyingType);
        //    }

        //    public void SetValue(object target, object value)
        //    {
        //        _underlyingValueProvider.SetValue(target, value);
        //    }

        //    public object GetValue(object target)
        //    {
        //        return _underlyingValueProvider.GetValue(target) ?? _defaultValue;
        //    }
        //}

        //public class SpecialContractResolver : DefaultContractResolver
        //{
        //    protected override IValueProvider CreateMemberValueProvider(MemberInfo member)
        //    {
        //        if (member.MemberType == MemberTypes.Property)
        //        {
        //            var pi = (PropertyInfo)member;
        //            if (pi.PropertyType.IsGenericType && pi.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
        //            {
        //                return new NullableValueProvider(member, pi.PropertyType.GetGenericArguments().First());
        //            }
        //        }
        //        else if (member.MemberType == MemberTypes.Field)
        //        {
        //            var fi = (FieldInfo)member;
        //            if (fi.FieldType.IsGenericType && fi.FieldType.GetGenericTypeDefinition() == typeof(Nullable<>))
        //                return new NullableValueProvider(member, fi.FieldType.GetGenericArguments().First());
        //        }

        //        return base.CreateMemberValueProvider(member);
        //    }
        //}

        #region M�todos

        public DataRow[] getRegistro(string id)
        {
            var cod = id.Split('-');
            DataRow[] registro = dtDatos.Select(string.Format("ASIENTO={0} AND CONSECUTIVO={1}", cod[0].Trim(), cod[1].Trim()));
            return registro;
        }

        public async Task<string[]> getDatos()
        {
            try
            {
                dtDatos = JsonConvert.DeserializeObject<DataTable>(construyePOST().Result);
                string[] datos = new string[atrRegistros];
                int cont = 0;
                foreach (DataRow item in dtDatos.Rows)
                {
                    datos[cont] = item[0].ToString() + " - " + item[1].ToString();
                    cont++;
                }
                return datos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable getDatosJson()
        {
            try
            {
                dtDatos = JsonConvert.DeserializeObject<DataTable>(construyePOST().Result);
                return dtDatos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable getDatosJsonTable(string tabla)
        {
            try
            {
                dtDatos = JsonConvert.DeserializeObject<DataTable>(construyePOSTTable(tabla).Result);
                return dtDatos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable getDatosJsonConsulta(string consulta)
        {
            try
            {
                dtDatos = JsonConvert.DeserializeObject<DataTable>(construyePOSTConsulta(consulta).Result);
                return dtDatos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Sentencias getSentenciasAction(string action)
        {
            try
            {
                Sentencias sentencias = null;
                string res = construyePOSTActionAsync(action).Result;
                //sentencias = JsonConvert.DeserializeObject<Sentencias>(construyePOSTActionAsync(action).Result);
                sentencias = JsonConvert.DeserializeObject<Sentencias>(res);

                if (!string.IsNullOrEmpty(sentencias.webSoporte))
                {
                    this.atrWebSoporte = sentencias.webSoporte;
                }

                this.atrCambiarDescuento = sentencias.CambiarDescuento;

                return sentencias;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public Sentencias getSentenciasAction(string action,List<string> actions)
        {
            try
            {
                Sentencias sentencias = null;
                string res = construyePOSTActionAsync(action,actions).Result;
                //sentencias = JsonConvert.DeserializeObject<Sentencias>(construyePOSTActionAsync(action).Result);
                sentencias = JsonConvert.DeserializeObject<Sentencias>(res);

                if (!string.IsNullOrEmpty(sentencias.webSoporte))
                {
                    this.atrWebSoporte = sentencias.webSoporte;
                }

                this.atrCambiarDescuento = sentencias.CambiarDescuento;

                return sentencias;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string getSentenciasActionString(string action)
        {
            try
            {                
                string res = construyePOSTActionAsync(action).Result;
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string getSentenciasActionString(string action,List<string> actions)
        {
            try
            {
                string res = construyePOSTActionAsync(action,actions).Result;
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string createJson()
        {

            JObject oJson;
            if (string.IsNullOrEmpty(atrJson))
                oJson = new JObject(
                    new JProperty("ejecucion", "0"),
                    new JProperty("server", atrServer),
                    new JProperty("database", atrDatabase),
                    new JProperty("schema", atrCompania),
                    new JProperty("table", "Diario"),
                    new JProperty("user", atrUser),
                    new JProperty("pass", atrPass),
                    new JProperty("rows", atrRegistros),
                    new JProperty("tipobd", atrTipoBD));
            else
                oJson = new JObject(
                    new JProperty("json", atrJson),
                    new JProperty("ejecucion", "0"),
                    new JProperty("server", atrServer),
                    new JProperty("database", atrDatabase),
                    new JProperty("schema", atrCompania),
                    new JProperty("table", "New_Diario"),
                    new JProperty("user", atrUser),
                    new JProperty("pass", atrPass),
                    new JProperty("rows", atrRegistros),
                    new JProperty("tipobd", atrTipoBD));
            return oJson.ToString();
        }

        private string createJsonTable(string table)
        {
            JObject oJson;
            if (string.IsNullOrEmpty(atrJson))
                oJson = new JObject(
                    new JProperty("ejecucion", "0"),
                    new JProperty("server", atrServer),
                    new JProperty("database", atrDatabase),
                    new JProperty("schema", atrCompania),
                    new JProperty("table", table),
                    new JProperty("user", atrUser),
                    new JProperty("pass", atrPass),
                    new JProperty("rows", atrRegistros),
                    new JProperty("tipobd", atrTipoBD));
            else
                oJson = new JObject(
                    new JProperty("json", atrJson),
                    new JProperty("ejecucion", "0"),
                    new JProperty("server", atrServer),
                    new JProperty("database", atrDatabase),
                    new JProperty("schema", atrCompania),
                    new JProperty("table", "New_Diario"),
                    new JProperty("user", atrUser),
                    new JProperty("pass", atrPass),
                    new JProperty("rows", atrRegistros),
                    new JProperty("tipobd", atrTipoBD));
            return oJson.ToString();
        }

        private string createJsonAction(string action)
        {
            JObject oJson;
            if (string.IsNullOrEmpty(atrJson))
                oJson = new JObject(
                    new JProperty("dispositivo", atrDispositivo),
                    new JProperty("action", action),
                    new JProperty("usuario", atrUsuario));

            else
                oJson = new JObject(
                    new JProperty("json", atrJson),
                    new JProperty("dispositivo", atrDispositivo),
                    new JProperty("action", action),
                    new JProperty("usuario", atrUsuario));
            return oJson.ToString();
        }

        private string createJsonAction(string action,List<string> actions)
        {
            JObject oJson;
            string parcial = JsonConvert.SerializeObject(actions);
            if (string.IsNullOrEmpty(atrJson))
                oJson = new JObject(
                    new JProperty("dispositivo", atrDispositivo),
                    new JProperty("action", action),
                    new JProperty("usuario", atrUsuario),
                    new JProperty("parcial", parcial));

            else
                oJson = new JObject(
                    new JProperty("json", atrJson),
                    new JProperty("dispositivo", atrDispositivo),
                    new JProperty("action", action),
                    new JProperty("usuario", atrUsuario),
                    new JProperty("parcial", parcial));
            return oJson.ToString();
        }

        private string createJsonPedidos(string sentenciaPedido, string action)
        {
            JObject oJson;
            oJson = new JObject(
                new JProperty("json", sentenciaPedido),
                new JProperty("dispositivo", atrDispositivo),
                new JProperty("action", action),
                new JProperty("usuario", atrUsuario));
            return oJson.ToString();
        }

        private string createJsonConsulta(string consulta)
        {
            //TODO
            try
            {
                JObject oJson;
                if (string.IsNullOrEmpty(atrJson))
                    oJson = new JObject(
                        new JProperty("ejecucion", "1"),
                        new JProperty("server", atrServer),
                        new JProperty("database", atrDatabase),
                        new JProperty("schema", atrCompania),
                        new JProperty("user", atrUser),
                        new JProperty("pass", atrPass),
                        new JProperty("command", consulta),
                        new JProperty("tipobd", atrTipoBD));

                else
                    oJson = new JObject(
                        new JProperty("json", atrJson),
                        new JProperty("ejecucion", "1"),
                        new JProperty("server", atrServer),
                        new JProperty("database", atrDatabase),
                        new JProperty("schema", atrCompania),
                        new JProperty("user", atrUser),
                        new JProperty("pass", atrPass),
                        new JProperty("command", consulta),
                        new JProperty("tipobd", atrTipoBD));
                return oJson.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        async Task<string> construyePOST()
        {
            try
            {
                WebRequest request = WebRequest.Create(servidor);
                request.Method = "POST";
                string postData = createJson();
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                request.ContentType = "application/json";
                request.ContentLength = byteArray.Length;
                request.Timeout = 100000;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                WebResponse response = request.GetResponse();
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();
                return responseFromServer;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        async Task<string> construyePOSTTable(string table)
        {
            try
            {
                WebRequest request = WebRequest.Create(servidor);
                request.Method = "POST";
                string postData = createJsonTable(table);
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                request.ContentType = "application/json";
                request.ContentLength = byteArray.Length;
                request.Timeout = 100000;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                WebResponse response = request.GetResponse();
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();
                return responseFromServer;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        async Task<string> construyePOSTConsulta(string consulta)
        {
            try
            {
                WebRequest request = WebRequest.Create(servidor);
                request.Method = "POST";
                string postData = createJsonConsulta(consulta);
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                request.ContentType = "application/json";
                request.ContentLength = byteArray.Length;
                request.Timeout = 100000;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                WebResponse response = request.GetResponse();
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();
                return responseFromServer;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        async Task<string> construyePOSTActionAsync(string action)
        {
            try
            {
                WebRequest request = WebRequest.Create(servidor);
                request.Method = "POST";
                string postData = createJsonAction(action);
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                request.ContentType = "application/json";
                request.ContentLength = byteArray.Length;
                request.Timeout = 300000;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                WebResponse response = request.GetResponse();
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();
                if (responseFromServer.Length > 12 && responseFromServer.Substring(0, 12).Contains("ERROR_NoteJS"))
                {
                    throw new Exception(responseFromServer.Replace("ERROR_NoteJS", "Ocurri� un error en el servidor: "));
                }
                return responseFromServer;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        async Task<string> construyePOSTActionAsync(string action,List<string> actions)
        {
            try
            {
                WebRequest request = WebRequest.Create(servidor);
                request.Method = "POST";
                string postData = createJsonAction(action,actions);
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                request.ContentType = "application/json";
                request.ContentLength = byteArray.Length;
                request.Timeout = 500000;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                WebResponse response = request.GetResponse();
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();
                if (responseFromServer.Length > 12 && responseFromServer.Substring(0, 12).Contains("ERROR_NoteJS"))
                {
                    throw new Exception(responseFromServer.Replace("ERROR_NoteJS", "Ocurri� un error en el servidor: "));
                }
                return responseFromServer;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        async Task<string> construyePOSTPedidoAsync(string pedido)
        {
            try
            {
                WebRequest request = WebRequest.Create(servidor);
                request.Method = "POST";
                string postData = createJsonPedidos(pedido, "guardar_pedidos");
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                request.ContentType = "application/json";
                request.ContentLength = byteArray.Length;
                request.Timeout = 300000;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                WebResponse response = request.GetResponse();
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();
                if (responseFromServer.Length > 12 && responseFromServer.Substring(0, 12).Contains("ERROR_NoteJS"))
                {
                    throw new Exception(responseFromServer.Replace("ERROR_NoteJS", "Ocurri� un error en el servidor: "));
                }
                return responseFromServer;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ObtenerSentenciasServidor(ref string error)
        {
            try
            {
                sentenciasCreate = this.getSentenciasAction("create_allTables").listaSentencias;
                continuarInsertar = this.getSentenciasActionString("fill_allTables").Equals("EXITO");
                error = string.Empty;
                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    error = "Error: " + ex.Message;
                else
                    error = "Error: " + ex.InnerException.Message;
                return false;
            }
        }

        public bool ObtenerSentenciasServidor(ref string error,List<string> actionsCreate,List<string> actionsInsert)
        {
            try
            {
                sentenciasCreate = this.getSentenciasAction("create_parcial",actionsCreate).listaSentencias;
                continuarInsertar = this.getSentenciasActionString("fill_parcial",actionsInsert).Equals("EXITO");
                error = string.Empty;
                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    error = "Error: " + ex.Message;
                else
                    error = "Error: " + ex.InnerException.Message;
                return false;
            }
        }

        public bool EnviarPedidos(ref string error, Sentencias sentenciaPedido, ref List<PedidosSincronizados> pedidosSinc)
        {
            try
            {
                string res = construyePOSTPedidoAsync(JsonConvert.SerializeObject(sentenciaPedido)).Result;

                pedidosSinc = JsonConvert.DeserializeObject<List<PedidosSincronizados>>(res);

                error = string.Empty;
                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    error = "Error: " + ex.Message;
                else
                    error = "Error: " + ex.InnerException.Message;
                return false;
            }
        }

        public bool CrearEstructuraDinamica(string path, ref string error, SQLiteConnection cnx)
        {
            try
            {
                DBestruct mibase = new DBestruct(path, sentenciasCreate,false, cnx,string.Empty);
                error = string.Empty;
                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    error = "Error: " + ex.Message;
                else
                    error = "Error: " + ex.InnerException.Message;
                return false;
            }
        }

        public bool InsertDatosDinamico(string path, ref string error, SQLiteConnection cnx,string dispositivo)
        {
            try
            {
                DBestruct mibase = new DBestruct(path, new List<Sentencias.Sentencia>(), continuarInsertar, cnx,dispositivo);
                error = string.Empty;
                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    error = "Error: " + ex.Message;
                else
                    error = "Error: " + ex.InnerException.Message;
                return false;
            }
        }

        public List<string> ObtenerRegistros(string path, SQLiteConnection cnx, string tabla, ref string error)
        {
            List<string> registros = null;

            try
            {
                DBestruct mibase = new DBestruct(path, cnx);
                registros = mibase.ObtenerRegistros(tabla);
                error = string.Empty;
                return registros;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    error = "Error: " + ex.Message;
                else
                    error = "Error: " + ex.InnerException.Message;
                return registros;
            }
        }

        public bool DescargarArchivoFotos(string path, ref string error)
        {
            try
            {
                fotosZip = null;
                string ruta = Path.Combine(path, "Fotos_Temp.zip");
                if (File.Exists(ruta))
                {
                    File.Delete(ruta);
                }
                getFile(ruta, "Fotos_" + atrDispositivo + ".zip");
                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    error = "Error: " + ex.Message;
                else
                    error = "Error: " + ex.InnerException.Message;
                return false;
            }
        }

        public bool DescargarArchivoSincro(string path, ref string error)
        {
            try
            {
                SincroZip = null;
                string ruta = Path.Combine(path, "Sincro_Temp.zip");
                if (File.Exists(ruta))
                {
                    File.Delete(ruta);
                }
                getFile(ruta, "Sincro_" + atrDispositivo + ".zip");
                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    error = "Error: " + ex.Message;
                else
                    error = "Error: " + ex.InnerException.Message;
                return false;
            }
        }

        public bool DescomprimirArchivoFotos(string path, ref string error)
        {
            try
            {
                string rutaDir = Path.Combine(path, atrDispositivo + "_fotos");
                if (Directory.Exists(rutaDir))
                {
                    Directory.Delete(rutaDir, true);
                }
                Directory.CreateDirectory(rutaDir);
                //Zip 

                string ruta = Path.Combine(path, "Fotos_Temp.zip");
                using (ZipInputStream s = new ZipInputStream(File.OpenRead(ruta)))
                {
                    ZipEntry theEntry;
                    while ((theEntry = s.NextEntry) != null)
                    {
                        string directoryName = Path.GetDirectoryName(theEntry.Name);
                        string fileName = Path.GetFileName(theEntry.Name);
                        directoryName = Path.Combine(path, directoryName);
                        if (directoryName.Length > 0)
                        {
                            Directory.CreateDirectory(directoryName);
                        }
                        if (fileName != String.Empty)
                        {
                            using (FileStream streamWriter = File.Create(Path.Combine(rutaDir, theEntry.Name)))
                            {
                                int size = 2048;
                                byte[] data = new byte[2048];
                                while (true)
                                {
                                    size = s.Read(data, 0, data.Length);
                                    if (size > 0)
                                    {
                                        streamWriter.Write(data, 0, size);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                if (File.Exists(ruta))
                {
                    File.Delete(ruta);
                }
                GC.Collect(GC.MaxGeneration);
                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    error = "Error: " + ex.Message;
                else
                    error = "Error: " + ex.InnerException.Message;
                return false;
            }
        }

        public bool DescomprimirArchivoSincro(string path, ref string error)
        {
            try
            {
                string rutaDir = Path.Combine(path, atrDispositivo + "_sincro");
                if (Directory.Exists(rutaDir))
                {
                    Directory.Delete(rutaDir, true);
                }
                Directory.CreateDirectory(rutaDir);
                //Zip 

                string ruta = Path.Combine(path, "Sincro_Temp.zip");
                using (ZipInputStream s = new ZipInputStream(File.OpenRead(ruta)))
                {
                    ZipEntry theEntry;
                    while ((theEntry = s.NextEntry) != null)
                    {
                        string directoryName = Path.GetDirectoryName(theEntry.Name);
                        string fileName = Path.GetFileName(theEntry.Name);
                        directoryName = Path.Combine(path, directoryName);
                        if (directoryName.Length > 0)
                        {
                            Directory.CreateDirectory(directoryName);
                        }
                        if (fileName != String.Empty)
                        {
                            using (FileStream streamWriter = File.Create(Path.Combine(rutaDir, theEntry.Name)))
                            {
                                int size = 2048;
                                byte[] data = new byte[2048];
                                while (true)
                                {
                                    size = s.Read(data, 0, data.Length);
                                    if (size > 0)
                                    {
                                        streamWriter.Write(data, 0, size);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                if (File.Exists(ruta))
                {
                    File.Delete(ruta);
                }
                GC.Collect(GC.MaxGeneration);
                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    error = "Error: " + ex.Message;
                else
                    error = "Error: " + ex.InnerException.Message;
                return false;
            }
        }

        public string getFile(string path, string archivo)
        {
            try
            {
                string res = construyePOSTDownloadAsync(path, archivo).Result;
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        async Task<string> construyePOSTDownloadAsync(string path, string archivo)
        {
            try
            {
                string server = servidor[servidor.Length-1] == '/' ? servidor.Substring(0,servidor.Length-1) : servidor;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(server + "/download?file=" + archivo);
                request.Method = "GET";
                request.ContentType = "application/zip";
                // if the URI doesn't exist, an exception will be thrown here...
                using (HttpWebResponse httpResponse = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream responseStream = httpResponse.GetResponseStream())
                    {
                        using (FileStream localFileStream =
                            new FileStream(path, FileMode.Create))
                        {
                            var buffer = new byte[4096];
                            long totalBytesRead = 0;
                            int bytesRead;

                            while ((bytesRead = responseStream.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                totalBytesRead += bytesRead;
                                localFileStream.Write(buffer, 0, bytesRead);
                            }
                        }
                    }
                }

                return "Archivo descargado Correctamente";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CopiarArchivoFotos(string path, ref string error)
        {
            try
            {
                string ruta = Path.Combine(path, atrDispositivo + "_fotos");
                if (Directory.Exists(ruta))
                {
                    int count = Directory.GetFiles(ruta).Length + 1;
                    for (int i = 0; i <= count; i++)
                    {
                        string archivo = Path.Combine(ruta, "fotos_" + i + ".json");
                        if (!File.Exists(archivo))
                        {
                            break;
                        }
                        foreach (ArticuloFoto foto in JsonConvert.DeserializeObject<List<ArticuloFoto>>(FileTools.ReadFileString(archivo)))
                        {
                            foto.GuardarImagen(path);
                            System.GC.Collect(0);
                        }
                        GC.Collect(GC.MaxGeneration);
                    }
                }
                if (Directory.Exists(ruta))
                {
                    Directory.Delete(ruta, true);
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    error = "Error: " + ex.Message;
                else
                    error = "Error: " + ex.InnerException.Message;
                return false;
            }
        }

        #endregion
    }
}