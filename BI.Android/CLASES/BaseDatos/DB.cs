using System;
using System.Data;
using System.Data.SQLiteBase;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using BI.Shared;

namespace BI.Android
{
    public class DB
	{
        public SQLiteConnection cnx{get;set;}

		//String for Message handling
		private string sqldb_message;
		//Bool to check for database availability
		private bool sqldb_available;
		/// <summary>
		/// Constructor de la clase que solicita el nombre de la base de datos para inicializar
		/// </summary>
		/// <param name="sqldb_name">Sqldb name.</param>
		public DB(string sqldb_name)
		{
			try
			{
				sqldb_message = "";
				sqldb_available = false;
				CreateDatabase(sqldb_name);
			}
			catch (SQLiteException ex) 
			{
				sqldb_message = ex.Message;
			}
		}
		/// <summary>
		/// Constructor de la clase que solicita el nombre de la base de datos y el path para inicializar
		/// </summary>
		/// <param name="sqldb_name">Sqldb name.</param>
		/// <param name="path">Path.</param>
		public DB(string sqldb_name,string path)
		{
			try
			{
				sqldb_message = "";
				sqldb_available = false;
				CreateDatabase(sqldb_name,path);
			}
			catch (SQLiteException ex) 
			{
				sqldb_message = ex.Message;
			}
		}

        /// <summary>
        /// Constructor de la clase que solicita el nombre de la base de datos y el path para inicializar
        /// </summary>
        /// <param name="sqldb_name">Sqldb name.</param>
        /// <param name="path">Path.</param>
        public DB(SQLiteConnection cnx)
        {
            try
            {
                this.cnx = cnx;
                sqldb_message = "";
                sqldb_available = cnx.isOpen;
            }
            catch (SQLiteException ex)
            {
                sqldb_message = ex.Message;
            }
        }

		/// <summary>
		/// Indica si se encuentra o no disponible para uso la base de datos.
		/// </summary>
		/// <value><c>true</c> if database available; otherwise, <c>false</c>.</value>
		public bool DatabaseAvailable
		{
			get{ return sqldb_available; }
		}
		/// <summary>
		/// Conserva el �ltimo mensaje env�ado de los procesos de la base de datos
		/// </summary>
		/// <value>The message.</value>
		public string Message
		{
			get{ return sqldb_message; }
		}
		/// <summary>
		/// Crea o abre la base de datos.
		/// </summary>
		/// <param name="sqldb_name">Sqldb name.</param>
		private void CreateDatabase(string sqldb_name)
		{
            //try
            //{
            //    string sqldb_location = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            //    string sqldb_path = Path.Combine(sqldb_location, sqldb_name);
            //    if (File.Exists(sqldb_path))
            //        sqldb = SQLiteDatabase.OpenDatabase(sqldb_path, null, DatabaseOpenFlags.OpenReadwrite);
            //    else
            //        sqldb = SQLiteDatabase.OpenOrCreateDatabase(sqldb_path, null, errorHandler: null);
            //    sqldb_message = "Database: " + sqldb_name + " opened";
            //    sqldb_available = true;
            //}
            //catch(SQLiteException ex)
            //{
            //    sqldb_message = ex.Message;
            //}
		}

        /// <summary>
        /// Crea o abre la base de datos.
        /// </summary>
        /// <param name="sqldb_name"></param>
        /// <param name="path"></param>
		private void CreateDatabase(string sqldb_name,string path)
		{
            //try
            //{
            //    string sqldb_location = path;
            //    string sqldb_path = Path.Combine(sqldb_location, sqldb_name);
            //    if (File.Exists(sqldb_path))
            //        sqldb = SQLiteDatabase.OpenDatabase(sqldb_path, null, DatabaseOpenFlags.OpenReadwrite);
            //    else
            //        sqldb = SQLiteDatabase.OpenOrCreateDatabase(sqldb_path, null, errorHandler: null);
            //    sqldb_message = "Database: " + sqldb_name + " opened";
            //    sqldb_available = true;
            //}
            //catch(SQLiteException ex)
            //{
            //    sqldb_message = ex.Message;
            //}
		}

		/// <summary>
		/// Creates the table.
		/// </summary>
		/// <param name="sTable">Table Object</param>
		public void CreateTable(Table sTable)
		{
			try
			{
				if (sqldb_available)
				{
					sqldb_message = "Debe de ingresar el nombre correcto de la tabla y al menos una columna";
					if (string.IsNullOrEmpty(sTable.Name) || sTable.Columns.Count == 0) return;
					StringBuilder sQuery = new StringBuilder();
					sQuery.Append(string.Format("CREATE TABLE IF NOT EXISTS {0} ( ", sTable.Name));

					foreach (column item in sTable.Columns) {
						sQuery.Append(item.Name + " ");
						switch (item.type) {
						case enumTypesColumn.BLOB:
							sQuery.Append("BLOB ");
							break;
						case enumTypesColumn.INTEGER:
							sQuery.Append("INTEGER ");
							break;
						case enumTypesColumn.NULL:
							sQuery.Append("NULL ");
							break;
						case enumTypesColumn.REAL:
							sQuery.Append("REAL ");
							break;
						case enumTypesColumn.TEXT:
							sQuery.Append("TEXT ");
							break;
						case enumTypesColumn.BOOLEAN:
							sQuery.Append("BOOLEAN ");
							break;
						case enumTypesColumn.DATETIME:
							sQuery.Append("DATETIME ");
							break;
						case enumTypesColumn.INT:
							sQuery.Append("INT ");
							break;
						case enumTypesColumn.NUMERIC:
							sQuery.Append("NUMERIC ");
							break;
						case enumTypesColumn.VARCHAR:
							sQuery.Append("VARCHAR ");
							break;
						case enumTypesColumn.DATE:
							sQuery.Append("DATE ");
							break;
						default:
						break;
						}
						if (item.type == enumTypesColumn.NUMERIC)
							sQuery.Append(string.Format("({0},{1}) ", item.sizeColumn, item.numDecimals));
						else
							if (item.sizeColumn > 0)
								sQuery.Append(string.Format("({0}) ", item.sizeColumn));
						if (item.isPrimary)
							sQuery.Append("PRIMARY KEY ");
						if (item.onConflict != enumTypesConflict.NULL)
						{
							switch (item.onConflict) {
								case enumTypesConflict.ABORT:
									sQuery.Append("ON CONFLICT ABORT ");
									break;
								case enumTypesConflict.FAIL:
									sQuery.Append("ON CONFLICT FAIL ");
									break;
								case enumTypesConflict.IGNORE:
									sQuery.Append("ON CONFLICT IGNORE ");
									break;
								case enumTypesConflict.REPLACE:
									sQuery.Append("ON CONFLICT REPLACE ");
									break;
								case enumTypesConflict.ROLLBACK:
									sQuery.Append("ON CONFLICT ROLLBACK ");
									break;
								default:
									break;
							}
						}
						sQuery.Append(string.Format("{0}", item.Autoincrement ? "AUTOINCREMENT " : ""));
						sQuery.Append(string.Format("{0}", item.allowNulls ? "NULL " : "NOT NULL "));
						if (!string.IsNullOrEmpty(item.tableName))
						{
							sQuery.Append(string.Format("CONSTRAINT '{0}' REFERENCES {1}({2}) ", 
											item.relationName, item.tableName, item.columnRelation));
							if (item.onDelete != enumTypesRelationship.NULL)
							{
								switch (item.onDelete) {
								case enumTypesRelationship.CASCADE:
									sQuery.Append("ON DELETE CASCADE ");
									break;
								case enumTypesRelationship.NO_ACTION:
									sQuery.Append("ON DELETE NO ACTION ");
									break;
								case enumTypesRelationship.RESTRICT:
									sQuery.Append("ON DELETE RESTRICT ");
									break;
								case enumTypesRelationship.SET_DEFAULT:
									sQuery.Append("ON DELETE SET DEFAULT ");
									break;
								case enumTypesRelationship.SET_NULL:
									sQuery.Append("ON DELETE SET NULL ");
									break;
								default:
									break;
								}
							}
							if (item.onUpdate != enumTypesRelationship.NULL)
							{
								switch (item.onUpdate) {
								case enumTypesRelationship.CASCADE:
									sQuery.Append("ON UPDATE CASCADE ");
									break;
								case enumTypesRelationship.NO_ACTION:
									sQuery.Append("ON UPDATE NO ACTION ");
									break;
								case enumTypesRelationship.RESTRICT:
									sQuery.Append("ON UPDATE RESTRICT ");
									break;
								case enumTypesRelationship.SET_DEFAULT:
									sQuery.Append("ON UPDATE SET DEFAULT ");
									break;
								case enumTypesRelationship.SET_NULL:
									sQuery.Append("ON UPDATE SET NULL ");
									break;
								default:
								break;
								}
							}
						}
						if (item.onMatch != enumTypesMatch.NULL)
						{
							switch (item.onMatch) {
							case enumTypesMatch.NONE:
								sQuery.Append("MATCH NONE ");
								break;
							case enumTypesMatch.PARTIAL:
								sQuery.Append("MATCH PARTIAL ");
								break;
							case enumTypesMatch.FULL:
								sQuery.Append("MATCH FULL ");
								break;
							default:
								break;
							}
						}
						sQuery.Append(", ");


					}
					string cadena = sQuery.ToString().Remove(sQuery.ToString().LastIndexOf(','),1);
                    cnx.ExecuteNonQuery(cadena + "); ");
					//sqldb.ExecSQL(cadena + "); ");
					sqldb_message = "Table created";
				}
			}
			catch(SQLiteException ex) 
			{
				sqldb_message = ex.Message;
			}
		}

        /// <summary>
        /// Creates the table.
        /// </summary>
        /// <param name="sTable">Table Object</param>
        public void CreateTableAction(string script, string tablename, string action)
        {
            try
            {
                if (sqldb_available)
                {
                    sqldb_message = "Debe de ingresar el nombre correcto de la tabla y al menos una columna";
                    string cadena = script;
                    string[] sentencias = cadena.Split(';');
                    foreach (string str in sentencias)
                    {
                        if (!string.IsNullOrEmpty(str))
                        {
                            string temp = str + ";";
                            cnx.ExecuteNonQuery(temp.ToUpper());
                            //sqldb.ExecSQL(temp);
                        }
                    }
                    //sqldb.ExecSQL(cadena);
                    sqldb_message = "Table "+tablename+" created";
                }
            }
            catch (SQLiteException ex)
            {
                sqldb_message = ex.Message;
                throw new Exception("Error creando tabla " + tablename + " acci�n " + action + ".");
            }
        }

        /// <summary>
        /// Executes Script
        /// </summary>
        /// <param name="script"></param>
        /// <param name="tablename"></param>
        public void ExecuteScriptAction(List<string> scripts, string tablename, string action, List<SQLiteParameterList> parametros)
        {
            try
            {
                if (sqldb_available)
                {
                    sqldb_message = "Debe de ingresar el nombre correcto de la tabla y al menos una columna";
                    int cont = 0;
                    foreach (string str in scripts)
                    {
                        cnx.ExecuteNonQuery(str, parametros[cont]);
                        cont++;
                        //sqldb.ExecSQL(str,param);
                    }
                    sqldb_message = "Error al insertar datos en la tabla " + tablename;
                }
            }
            catch (SQLiteException ex)
            {
                sqldb_message = ex.Message;
                throw new Exception("Error ejecutando sentencia  en tabla " + tablename + " acci�n " + action + "."+ex.Message);
            }
        }

        /// <summary>
        /// Executes Script
        /// </summary>
        /// <param name="script"></param>
        /// <param name="tablename"></param>
        public void ExecuteScriptActionIndividual(string script, string tablename, string action, SQLiteParameterList parametros)
        {
            try
            {
                if (sqldb_available)
                {
                    sqldb_message = "Debe de ingresar el nombre correcto de la tabla y al menos una columna";
                    cnx.ExecuteNonQuery(script, parametros);
                    sqldb_message = "Error al insertar datos en la tabla " + tablename;
                }
            }
            catch (SQLiteException ex)
            {
                sqldb_message = ex.Message;
                throw new Exception("Error ejecutando sentencia  en tabla " + tablename + " acci�n " + action + "." + ex.Message);
            }
        }

        /// <summary>
        /// Drop a Table
        /// </summary>
        /// <param name="sTable"></param>
        public void DropTable(Table sTable)
        {
            try
            {
                if (sqldb_available)
                {
                    string command = string.Format("DROP TABLE IF EXIST {0};", sTable.Name);
                    cnx.ExecuteNonQuery(command);
                    //sqldb.ExecSQL(command);
                    sqldb_message = "Table deleted";
                }
            }
            catch (SQLiteException ex)
            {
                sqldb_message = ex.Message;
            }
        }

        /// <summary>
        /// Drop a Table
        /// </summary>
        /// <param name="sTable"></param>
        public void DeleteRecords(Table sTable)
        {
            try
            {
                if (sqldb_available)
                {
                    string command = string.Format("DELETE FROM {0};", sTable.Name);
                    cnx.ExecuteNonQuery(command);
                    //sqldb.ExecSQL(command);
                    sqldb_message = "records deleted";
                }
            }
            catch (SQLiteException ex)
            {
                sqldb_message = ex.Message;
            }
        }

        /// <summary>
        /// Delete the records of a table
        /// </summary>
        /// <param name="tablename"></param>
        public void DeleteRecordsAction(string tablename)
        {
            try
            {
                if (sqldb_available)
                {
                    string command = string.Format("DELETE FROM {0};", tablename);
                    cnx.ExecuteNonQuery(command);
                    //sqldb.ExecSQL(command);
                    sqldb_message = "records of "+tablename+" deleted";
                }
            }
            catch (SQLiteException ex)
            {
                sqldb_message = ex.Message;
                throw new Exception("Error borrando records de tabla " + tablename + "." + ex.Message);
            }
        }

        /// <summary>
        /// Delete the records of a table
        /// </summary>
        /// <param name="tablename"></param>
        public void DropTableAction(string tablename)
        {
            try
            {
                if (sqldb_available)
                {
                    string command = string.Format("DROP TABLE IF EXISTS {0};", tablename);
                    cnx.ExecuteNonQuery(command);
                    //sqldb.ExecSQL(command);
                    sqldb_message = tablename + " deleted";
                }
            }
            catch (SQLiteException ex)
            {
                sqldb_message = ex.Message;
                throw new Exception("Error borrando tabla " + tablename + "." + ex.Message);
            }
        }

		/// <summary>
		/// Crea un rango de tablas
		/// </summary>
		/// <param name="listTablas">Lista de tablas.</param>
		/*public void createTable(List<Table> listTablas)
		{
			foreach (Table item in listTablas) {
				createTable (item);
			}
		}*/

		/// <summary>
		/// Adds the record.
		/// </summary>
		/// <param name="sTable">S table.</param>
		/// <param name="sColumnas">S columnas.</param>
		/// <param name="sValores">S valores.</param>
		public void AddRecord(string sTable, List<string> sColumnas, List<object> sValores)
		{
			try
			{
				sqldb_message = "La cantidad de columnas no coincide con la cantidad de valores o no es superior a 0";
				if (sColumnas.Count != sValores.Count && sColumnas.Count > 0) return;
				StringBuilder squery =  new StringBuilder();
				squery.Append(string.Format("INSERT INTO {0} ( ", sTable));
				string str = "";
				foreach (var item in sColumnas) {
					if (str != "") str += ", ";
					str += item;
				}
				squery.Append(str + ") VALUES ( ");
				str = "";
				foreach (var dato in sValores) {
					bool res;
					int ires;
					double dures;
					decimal dres;
					if (str != "") str += ", ";
					if(bool.TryParse(dato.ToString(),out res))
					{
						str += Convert.ToBoolean(dato) ? 1 : 0;
					}
					else if (decimal.TryParse(dato.ToString(), out dres) 
						|| int.TryParse(dato.ToString(), out ires) 
						|| double.TryParse(dato.ToString(), out dures))
					{
						str += dato.ToString().Replace(',','.');
					}
					else
					{
						str += string.Format("'{0}'",dato);
					}
				}
				squery.Append(str + ");");
                cnx.ExecuteNonQuery(squery.ToString());
				//sqldb.ExecSQL(squery.ToString());
				sqldb_message = "Record saved";
			}
			catch(SQLiteException ex) 
			{
				sqldb_message = ex.Message;
			}
		}
		//Updates an existing record with the given parameters depending on id parameter
		public void UpdateRecord(string Tabla, Dictionary<string,object> listaDatos, Dictionary<string,object> listaCondicion)
		{
			try
			{
				sqldb_message = "Debe de ingresar al menos un valor a modificar para continuar";
				if (listaDatos.Count < 1) return;
				StringBuilder sQuery = new StringBuilder();
				sQuery.Append(string.Format("UPDATE {0} SET ", Tabla));
				string str = "";
				foreach (var item in listaDatos) {
					if (!string.IsNullOrEmpty(str)) str += ", ";
					str += item.Key + " = ";
					bool res;
					int ires;
					double dures;
					decimal dres;
					if(bool.TryParse(item.Value.ToString(),out res))
					{
						str += Convert.ToBoolean(item.Value) ? 1 : 0;
					}
					else if (decimal.TryParse(item.Value.ToString(), out dres) 
						|| int.TryParse(item.Value.ToString(), out ires) 
						|| double.TryParse(item.Value.ToString(), out dures))
					{
						str += item.Value.ToString();
					}
					else
					{
						str += string.Format("'{0}'",item.Value.ToString());
					}
				}
				if (listaCondicion.Count > 0)
				{
					sQuery.Append(string.Format("{0} WHERE ", str));
					str = "";
					foreach (var item in listaCondicion) {
						if (!string.IsNullOrEmpty(str)) str += "AND ";
						bool res;
						int ires;
						double dures;
						decimal dres;
						if(bool.TryParse(item.Value.ToString(),out res))
						{
							str += Convert.ToBoolean(item.Value) ? 1 : 0;
						}
						else if (decimal.TryParse(item.Value.ToString(), out dres) 
							|| int.TryParse(item.Value.ToString(), out ires) 
							|| double.TryParse(item.Value.ToString(), out dures))
						{
							str += item.Key + " = ";
							str += item.Value.ToString();
						}
						else
						{
							str += item.Key + " LIKE ";
							str += string.Format("'%{0}%'",item.Value.ToString());
						}
					}
				}
				sQuery.Append(string.Format("{0} ", str));
                cnx.ExecuteNonQuery(sQuery.ToString());
				//sqldb.ExecSQL(sQuery.ToString());
				sqldb_message = "Record updated";
			}
			catch(SQLiteException ex)
			{
				sqldb_message = ex.Message;
			}
		}
		//Deletes the record associated to id parameter
		public void DeleteRecord(string table, Dictionary<string, object> listaCondicion)
		{
			try
			{
				string str = "", cadena = "";
				if (listaCondicion.Count > 0)
					cadena = string.Format("DELETE FROM {0} WHERE ",table);
				else
					cadena = string.Format("DELETE FROM {0}",table);
				foreach (var item in listaCondicion) {
					if (!string.IsNullOrEmpty(str)) str += " AND ";
					bool res;
					int ires;
					double dures;
					decimal dres;
					if(bool.TryParse(item.Value.ToString(),out res))
					{
						str += Convert.ToBoolean(item.Value) ? 1 : 0;
					}
					else if (decimal.TryParse(item.Value.ToString(), out dres) 
						|| int.TryParse(item.Value.ToString(), out ires) 
						|| double.TryParse(item.Value.ToString(), out dures))
					{
						str += item.Key + " = ";
						str += item.Value.ToString();
					}
					else
					{
						str += item.Key + " LIKE ";
						str += string.Format("'%{0}%'",item.Value.ToString());
					}
				}
				cadena += str;
                cnx.ExecuteNonQuery(cadena);
				//sqldb.ExecSQL(cadena);
				sqldb_message = "Record deleted";
			}
			catch(SQLiteException ex) 
			{
				sqldb_message = ex.Message;
			}
		}
		//Searches a record and returns an Android.Database.ICursor cursor
		//Shows all the records from the table
        //public global::Android.Database.ICursor GetRecordCursor(string table, string columns)
        //{
        //    global::Android.Database.ICursor sqldb_cursor = null;
        //    //try
        //    //{
        //    //    string sqldb_query = string.Format("SELECT {1} FROM {0};", table, columns);
        //    //    sqldb_cursor = sqldb.RawQuery(sqldb_query, null);
        //    //    if(sqldb_cursor == null) sqldb_message = "Record not found";
        //    //}
        //    //catch(SQLiteException ex) 
        //    //{
        //    //    sqldb_message = ex.Message;
        //    //}
        //    return sqldb_cursor;
        //}
		//Searches a record and returns an Android.Database.ICursor cursor
		//Shows records according to search criteria
        //public global::Android.Database.ICursor GetRecordCursor(string table, string columns, Dictionary<string, object> listaCondicion)
        //{
        //    global::Android.Database.ICursor sqldb_cursor = null;
        //    //try
        //    //{
        //    //    string sQuery, str = "";
        //    //    if (listaCondicion != null && listaCondicion.Count > 0)
        //    //    {
        //    //        sQuery = string.Format("SELECT {1} FROM {0} WHERE ", table, columns);
        //    //        foreach (var item in listaCondicion) {
        //    //            if (!string.IsNullOrEmpty(str)) str += "AND ";
        //    //            bool res;
        //    //            int ires;
        //    //            double dures;
        //    //            decimal dres;
        //    //            if(bool.TryParse(item.Value.ToString(),out res))
        //    //            {
        //    //                str += Convert.ToBoolean(item.Value) ? 1 : 0;
        //    //            }
        //    //            else if (decimal.TryParse(item.Value.ToString(), out dres) 
        //    //                || int.TryParse(item.Value.ToString(), out ires) 
        //    //                || double.TryParse(item.Value.ToString(), out dures))
        //    //            {
        //    //                str += item.Key + " = ";
        //    //                str += item.Value.ToString();
        //    //            }
        //    //            else
        //    //            {
        //    //                str += item.Key + " LIKE ";
        //    //                str += string.Format("'%{0}%'",item.Value.ToString());
        //    //            }
        //    //        }
        //    //    }
        //    //    else
        //    //        sQuery = string.Format("SELECT {1} FROM {0}", table, columns);
        //    //    sQuery += str;
        //    //    sqldb_cursor = sqldb.RawQuery(sQuery, null);
        //    //    if(sqldb_cursor == null) sqldb_message = "Record not found";
        //    //}
        //    //catch(SQLiteException ex) 
        //    //{
        //    //    sqldb_message = ex.Message;
        //    //}
        //    return sqldb_cursor;
        //}

        /// <summary>
        /// Obtiene de la BD los registros ingresados en la Tabla.
        /// </summary>
        /// <returns></returns>
        public List<string> ObtenerRegistros(string tabla)
        {
            List<string> registros = new List<string>();

            //cnx.Open();

            using (var cmd = cnx.CreateCommand())
            {
                try
                {
                    cmd.CommandText = "SELECT * FROM ERPADMIN_" + tabla;// +" ORDER BY FECHA_VENCIMIENTO ASC";

                    SQLiteDataReader sqlReader = cmd.ExecuteDataReader();

                    if (sqlReader != null)
                    {
                        while (sqlReader.Read())
                        {
                            string registro = "";

                            for (int i = 0; i < sqlReader.FieldCount; i++)
                            {
                                registro += String.Concat(sqlReader.GetName(i).ToString(), ": ", sqlReader.GetValue(i).ToString(), "\n");
                            }

                            registros.Add(registro);
                        }
                    }
                    sqlReader.Close();
                }
                catch (Exception e)
                {
                    return registros;
                }
            }

            //cnx.Close();

            return registros;
        }
	}
}