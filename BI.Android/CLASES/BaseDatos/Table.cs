using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BI.Shared;


namespace BI.Android
{
    public class Table
    {
        public string Name
        {
            get;
            set;
        }
        public List<column> Columns
        {
            get;
            set;
        }
        public Table(string table)
        {
            Name = table;
            Columns = new List<column>();
        }
    }
}