using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BI.Shared;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using BI.Android;
using System.Data.SQLite;
using Pedidos.Core;
using Android.Util;

namespace Softland.Droid.PEL
{
    [Service]
    public class SyncService : Service
    {
        bool cancel=false;
        public static bool isRunnig=false;

        public override void OnCreate()
        {
            base.OnCreate();
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            Toast.MakeText(this, "Servicio de sincronizaci�n activado.", ToastLength.Short).Show();
            IniciarServicio();
            return base.OnStartCommand(intent, flags, startId);
        }

        private async void IniciarServicio() 
        {
            //Log.Info("rastreo", "Sync Iniciar Servicio");
            if (cancel)
            {
                isRunnig = false;
                this.StopSelf();
                return;
            }
            isRunnig = true;
            string result = await SincronizarPedidos();
            if (!string.IsNullOrEmpty(result) && result.ToUpper().Contains("SE HAN SINCRONIZADO"))
            {
                Toast.MakeText(Android.App.Application.Context, result, ToastLength.Short).Show();
                //Log.Info("rastreo", "Entro if iniciarServicio");
                //Notificacion
                // Instantiate the builder and set notification elements:
                try
                {
                    Notification.Builder builder = new Notification.Builder(Android.App.Application.Context)
                    .SetContentTitle("Softland.Android.PEM")
                    .SetContentText("Sincronizaci�n: " + result)
                    .SetSmallIcon(Resource.Drawable.Icon);

                    // Build the notification:
                    Notification notification = builder.Build();

                    // Get the notification manager:
                    NotificationManager notificationManager =
                        GetSystemService(Context.NotificationService) as NotificationManager;

                    // Publish the notification:
                    const int notificationId = 0;
                    notificationManager.Notify(notificationId, notification);
                    //var nMgr = (NotificationManager)GetSystemService(NotificationService);
                    //var notification = new Notification(Resource.Drawable.Icon, "Softland.Android.PEM: " + result);
                    ////var pendingIntent = PendingIntent.GetActivity(this, 0, new Intent(this, typeof(DemoActivity)), 0);
                    ////notification.SetLatestEventInfo(this, "Demo Service Notification", "Message from demo service", pendingIntent);
                    //nMgr.Notify(0, notification);
                }
                catch
                { 
                }                
                bool res = await Esperar();
                if (res)
                {
                    IniciarServicio();
                }                
            }
            else
            {
                bool res = await Esperar();
                if (res)
                {
                    IniciarServicio();
                }                
            }
            //Log.Info("rastreo", "Sync Salgo Iniciar Servicio");
        }

        public override IBinder OnBind(Intent intent)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// M�todo as�ncrono que se encarga de intentar sincronizar los pedidos pendientes con el servidor.
        /// </summary>
        /// <returns>Retorna un mensaje con el resultado de la operaci�n.</returns>
        public Task<string> SincronizarPedidos()
        {
            // Se retorna un Task para ejecutarlo de manera as�ncrona.
            return Task.Run(() =>
            {
                try
                {
                    if (string.IsNullOrEmpty(App.DeviceID) || string.IsNullOrEmpty(App.Servidor) || string.IsNullOrEmpty(App.Usuario))
                    {
                        return string.Empty;
                    }
                    // Se crea una referencia para conectar con el servidor.
                    conexion con = new conexion(App.DeviceID, App.Servidor, App.Usuario);

                    // se asigna una referencia a la conexi�n con la base de datos.
                    App.cnxSincro = new Connection(System.IO.Path.Combine(App.PathAplicacion,App.BDName));                    

                    string error = string.Empty;

                    if (App.cnxSincro == null)
                    {
                        return error;
                    }
                    else if (!App.cnxSincro.isOpen)
                    {
                        App.cnxSincro = null;
                        return error;
                    }

                    int cantidadPedidos = 0;
                    Sentencias sentencias = new Sentencias();

                    //Log.Info("rastreo", "Sincronizando Pedidos");
                    // Se obtienen los pedidos pendientes en forma de sentencias para enviar al servidor.
                    GestorPedido.CrearSentenciasPedidos(App.cnxSincro, ref sentencias, ref cantidadPedidos);

                    if (sentencias.listaSentencias.Count > 0)
                    {
                        // Lista de pedidos que ser�n devueltos por el servidor para evaluar su estado.
                        List<PedidosSincronizados> pedidosSinc = new List<PedidosSincronizados>();

                        // Se envian las sentencias y se evalua si el proceso termino correctamente.
                        if (con.EnviarPedidos(ref error, sentencias, ref pedidosSinc))
                        {
                            error = "";

                            if (pedidosSinc.Count > 0)
                            {
                                if (cantidadPedidos == pedidosSinc.Where(x => x.sincronizado).ToList().Count)
                                {
                                    error = "Se han sincronizado todos los pedidos pendientes.";
                                }
                                else
                                {
                                   // error = string.Concat("Se han sincronizado ", pedidosSinc.Where(x => x.sincronizado).ToList().Count, " de ", cantidadPedidos, " pedidos pendientes.");
                                }

                                // Se actualizan los pedidos sincronizados, exitosos o no.
                                Pedido.ActualizarSincronizados(pedidosSinc, App.cnxSincro);
                            }
                        }
                    }

                    // Se cierra la conexi�n extra para sincronizaci�n y se limpia la referencia.
                    if (App.cnxSincro != null)
                    {
                        if (App.cnxSincro.isOpen)
                            App.cnxSincro.Close();
                        App.cnxSincro = null;
                    }

                    return error;
                }
                catch (Exception ex)
                {
                    // Se cierra la conexi�n extra para sincronizaci�n y se limpia la referencia.
                    if (App.cnxSincro != null)
                    {
                        if (App.cnxSincro.isOpen)
                            App.cnxSincro.Close();
                        App.cnxSincro = null;
                    }

                    return "Ocurri� un error al enviar los pedidos pendientes, int�ntelo nuevamente.";
                }
            });
        }

        public Task<bool> Esperar()
        {
            return Task.Run(() =>
            {
                try
                {
                    System.Threading.Thread.Sleep(25000);
                    return true;
                }
                catch
                {
                    return false;
                }
            });
        }
    }
}