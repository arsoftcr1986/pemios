using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Text;
using Pedidos.Core;

namespace Softland.Droid.PEL
{
    public class EditTextFilterAlmDet : Java.Lang.Object, IInputFilter
    {
        Java.Util.Regex.Pattern mPattern;

        public EditTextFilterAlmDet()
        {
            mPattern = Java.Util.Regex.Pattern.Compile("[0-9]{0," + (16 - 1) + "}+((\\.[0-9]{0," + (FRmConfig.CantidadDecimales) + "})?)||(\\.)?");
        }

        public Java.Lang.ICharSequence FilterFormatted(Java.Lang.ICharSequence source, int start, int end, ISpanned dest, int dstart, int dend)
        {
            //Java.Util.Regex.Matcher matcher = mPattern.Matcher(dest);
            Java.Util.Regex.Matcher matcher = mPattern.Matcher((dest.SubSequenceFormatted(0, dstart).ToString() + source.SubSequenceFormatted(start, end).ToString() + dest.SubSequenceFormatted(dend, dest.Length()).ToString()));
            if (!matcher.Matches())
                return new Java.Lang.String(string.Empty);
            return null;
        }
    }
}