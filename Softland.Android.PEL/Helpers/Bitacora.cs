﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Softland.Droid.PEL
{
    public class Bitacora
    {
        string fileName;



        public Bitacora(string pFileName) 
        {
            fileName = pFileName;
            if (!Directory.Exists(Path.Combine(App.PathAplicacion, "Bitacoras")))
            {
                Directory.CreateDirectory(Path.Combine(App.PathAplicacion, "Bitacoras"));
            }
        }

        public void EscribirLinea(string linea)
        {
            string ruta = Path.Combine(App.PathAplicacion, "Bitacoras", fileName);
            if (!File.Exists(ruta))
            {
                using(var stw=File.CreateText(ruta))
                {
                    stw.WriteLine(linea);
                    stw.WriteLine();
                    stw.Close();
                }
            }
            else
            {
                using (var stw = File.AppendText(ruta))
                {
                    stw.WriteLine(linea);
                    stw.WriteLine();
                    stw.Close();
                }
            }            
        }
              
    }
}
