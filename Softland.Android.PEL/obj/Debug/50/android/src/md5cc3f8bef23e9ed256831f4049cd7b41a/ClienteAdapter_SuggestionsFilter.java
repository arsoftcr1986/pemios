package md5cc3f8bef23e9ed256831f4049cd7b41a;


public class ClienteAdapter_SuggestionsFilter
	extends android.widget.Filter
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_performFiltering:(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;:GetPerformFiltering_Ljava_lang_CharSequence_Handler\n" +
			"n_publishResults:(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V:GetPublishResults_Ljava_lang_CharSequence_Landroid_widget_Filter_FilterResults_Handler\n" +
			"";
		mono.android.Runtime.register ("Softland.Droid.PEL.ClienteAdapter+SuggestionsFilter, Softland.Droid.PEL", ClienteAdapter_SuggestionsFilter.class, __md_methods);
	}


	public ClienteAdapter_SuggestionsFilter ()
	{
		super ();
		if (getClass () == ClienteAdapter_SuggestionsFilter.class)
			mono.android.TypeManager.Activate ("Softland.Droid.PEL.ClienteAdapter+SuggestionsFilter, Softland.Droid.PEL", "", this, new java.lang.Object[] {  });
	}

	public ClienteAdapter_SuggestionsFilter (md5cc3f8bef23e9ed256831f4049cd7b41a.ClienteAdapter p0)
	{
		super ();
		if (getClass () == ClienteAdapter_SuggestionsFilter.class)
			mono.android.TypeManager.Activate ("Softland.Droid.PEL.ClienteAdapter+SuggestionsFilter, Softland.Droid.PEL", "Softland.Droid.PEL.ClienteAdapter, Softland.Droid.PEL", this, new java.lang.Object[] { p0 });
	}


	public android.widget.Filter.FilterResults performFiltering (java.lang.CharSequence p0)
	{
		return n_performFiltering (p0);
	}

	private native android.widget.Filter.FilterResults n_performFiltering (java.lang.CharSequence p0);


	public void publishResults (java.lang.CharSequence p0, android.widget.Filter.FilterResults p1)
	{
		n_publishResults (p0, p1);
	}

	private native void n_publishResults (java.lang.CharSequence p0, android.widget.Filter.FilterResults p1);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
