package md5cc3f8bef23e9ed256831f4049cd7b41a;


public abstract class BaseActivity
	extends android.support.v7.app.ActionBarActivity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("Softland.Droid.PEL.BaseActivity, Softland.Droid.PEL", BaseActivity.class, __md_methods);
	}


	public BaseActivity ()
	{
		super ();
		if (getClass () == BaseActivity.class)
			mono.android.TypeManager.Activate ("Softland.Droid.PEL.BaseActivity, Softland.Droid.PEL", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
