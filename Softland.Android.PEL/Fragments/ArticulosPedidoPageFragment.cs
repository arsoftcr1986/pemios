using System;
using System.Collections.Generic;
using Android.Runtime;
using Android.Content;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Pedidos.Core;
using Android.Support.V4.View;
using BI.Shared;
using System.Threading.Tasks;
using com.refractored;

namespace Softland.Droid.PEL
{
    public class ArticulosPedidoPageFragment : Fragment
    {
        private ViewPager m_ViewPager;
        private PagerSlidingTabStrip m_PageIndicator;
        private FragmentPagerAdapter m_Adapter;
        public static int currentPos = 0;


        public ArticulosPedidoPageFragment()
        {
            this.RetainInstance = true;
        }

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.ArticulosPageFragment, null);

            // Create your application here
            this.m_ViewPager = view.FindViewById<ViewPager>(Resource.Id.viewPager);
            this.m_ViewPager.OffscreenPageLimit = 4;
            this.m_PageIndicator = view.FindViewById<PagerSlidingTabStrip>(Resource.Id.tabs);

            //Since we are a fragment in a fragment you need to pass down the child fragment manager!
            this.m_Adapter = new ArticulosPedidoPageAdapter(this.ChildFragmentManager);


            this.m_ViewPager.Adapter = this.m_Adapter;

            this.m_PageIndicator.SetViewPager(this.m_ViewPager);
            this.m_ViewPager.PageSelected += (s, e) =>
            {
                currentPos = e.Position;
            };
            return view;
        }
    }
}