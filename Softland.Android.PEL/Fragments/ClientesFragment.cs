using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Content;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Android.Support.V4.View;
using Android.Support.V7.App;
using Android.Runtime;
using Pedidos.Core;
using BI.Shared;
using System.Threading.Tasks;
using Android.Util;

namespace Softland.Droid.PEL
{
    public class ClientesFragment : Fragment
    {
        Android.Support.V7.Widget.SearchView mSearchview;
        ListView list;
        ClienteAdapter adapter;

        public ClientesFragment()
        {
            this.RetainInstance = true;
        }
        //private List<FriendViewModel> _friends;

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            this.HasOptionsMenu = true;
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.ClientesFragment, null);
            list = view.FindViewById<ListView>(Resource.Id.list);
            var myActivity = (MainActivity)this.Activity;
            myActivity.SupportActionBar.Title = myActivity.Title = "Clientes";
            list.ItemClick += list_ItemClick;
            adapter = null;            
            InicializarLista();
            return view;
        }

        void list_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var myActivity = (MainActivity)this.Activity;
            Intent intent = new Intent(myActivity, typeof(ConsulClienteActivity));
            intent.PutExtra("codigo", adapter.GetCodigo(e.Position));
            StartActivity(intent);
        }

        private async void InicializarLista()
        {
            bool res = await ExecuteConsultar();
            //Log.Info("rastreo", "clientes Fragment exito");
        }

        private Task<bool> ExecuteConsultar()
        {
            //Log.Info("rastreo", "Clientes fragmento executeConsultar");
            return Task.Run(() =>
            {
                Android.App.ProgressDialog progressDialog = null;
                var myActivity = (MainActivity)this.Activity;
                try
                {
                    myActivity.RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(myActivity, "Cargando", "Por favor espere...", true); });           
                    List<Cliente> lista = new List<Cliente>();
                    lista = Cliente.CargarClientes(new CriterioBusquedaCliente(CriterioCliente.Ninguno, string.Empty, true, false), App.CompaniaDefault);
                    myActivity.RunOnUiThread(() => { adapter = new ClienteAdapter(myActivity, lista); list.Adapter = adapter; });
                    myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                    return true;
                }
                catch
                {
                    myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                    myActivity.RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error inicializando lista.", ToastLength.Short).Show(); });
                    return false;
                }

            });
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.search_main, menu);
            var item = menu.FindItem(Resource.Id.action_search);
            var searchView = MenuItemCompat.GetActionView(item);
            mSearchview = searchView.JavaCast<Android.Support.V7.Widget.SearchView>();
            mSearchview.QueryTextSubmit += (s, e) => {
                if (adapter == null)
                    return;
                adapter.Filter.InvokeFilter(e.Query);
            };
            mSearchview.QueryTextChange += (s, e) =>
            {
                if (adapter == null)
                    return;
                if (string.IsNullOrEmpty(e.NewText))
                {
                    adapter.ResetSearch();
                }
                else
                {
                    adapter.Filter.InvokeFilter(e.NewText);
                }
            };
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.menu_logoff:
                    {
                        if (GestorPedido.PedidosCollection.Gestionados != null)
                        {
                            GestorPedido.PedidosCollection.Gestionados.Clear();
                            GC.Collect(GC.MaxGeneration);
                        }
                        //if (SyncService.isRunnig)
                        //{
                        //    this.Activity.StopService(new Intent(this.Activity, typeof(SyncService)));
                        //    SyncService.isRunnig = false;
                        //} 
                        var intent = new Intent(this.Activity, typeof(LoginActiity));
                        intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
                        this.StartActivity(intent);
                       break;
                    }
                case Resource.Id.menu_filtro:
                    {
                        var myActivity = (MainActivity)this.Activity;
                        Android.App.Dialog builder = new Android.App.Dialog(myActivity);
                        builder.Window.RequestFeature(WindowFeatures.NoTitle);
                        builder.SetContentView(Resource.Layout.dialog_filtroclt);                        
                        RadioButton rbCodigo = builder.FindViewById<RadioButton>(Resource.Id.rbCodigo);
                        RadioButton rbDescripcion = builder.FindViewById<RadioButton>(Resource.Id.rbDescripcion);
                        RadioGroup rgFiltros = builder.FindViewById<RadioGroup>(Resource.Id.rgFiltros);
                        rbCodigo.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rbDescripcion.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rgFiltros.CheckedChange += (sender, e) =>
                        {
                            if (rbCodigo.Checked)
                            {
                                App.FiltroCliente = App.CodigoFilter;
                                Toast.MakeText(Android.App.Application.Context, "Filtrando clientes por c�digo.", ToastLength.Short).Show();
                                builder.Dismiss();
                            }
                            else
                            {
                                App.FiltroCliente = App.DescripcionFilter;
                                Toast.MakeText(Android.App.Application.Context, "Filtrando clientes por descripci�n.", ToastLength.Short).Show();
                                builder.Dismiss();
                            }
                                
                        };
                        if (string.IsNullOrEmpty(App.FiltroCliente))
                        {
                            App.FiltroCliente = App.CodigoFilter;
                            rbCodigo.Checked = true;
                        }
                        else
                        {
                            if (App.FiltroCliente.Equals(App.CodigoFilter))
                                rbCodigo.Checked = true;
                            else
                                rbDescripcion.Checked = true;                            
                        }
                        builder.Window.SetBackgroundDrawableResource(Resource.Color.semitransparent);
                        builder.Show(); break;
                    }
                default: break;
            }
            return base.OnOptionsItemSelected(item);
        }

        public override void OnDetach()
        {
            list.ItemClick -= list_ItemClick;
            base.OnDetach();
        }
    }
}