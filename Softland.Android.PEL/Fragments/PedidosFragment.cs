using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Content;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Android.Support.V4.View;
using Android.Support.V7.App;
using Android.Runtime;
using Pedidos.Core;
using BI.Shared;
using System.Threading.Tasks;
using Android.Util;

namespace Softland.Droid.PEL
{
    public class PedidosFragment : Fragment
    {

        Android.Support.V7.Widget.SearchView mSearchview;
        ListView list;
        ClienteAdapter adapter;
        bool cargando = false;

        public PedidosFragment()
        {
            this.RetainInstance = true;
        }

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            this.HasOptionsMenu = true;
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.PedidosFragment, null);
            list = view.FindViewById<ListView>(Resource.Id.list);
            var myActivity = (MainActivity)this.Activity;
            myActivity.SupportActionBar.Title = myActivity.Title = "Pedidos";
            list.ItemClick += list_ItemClick;
            adapter = null;            
            return view;
        }

        async void list_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            bool res = await ExecuteIniciarPedido(e.Position);
        }

        public override void OnResume()
        {                  
            base.OnResume();
            try
            {
                if (cargando)
                    return;
                if (GestorPedido.PedidosCollection.Gestionados != null && GestorPedido.PedidosCollection.Gestionados.Count > 0)
                {
                    var myActivity = (MainActivity)this.Activity;
                    myActivity.ListItemClicked(MainActivity.FragmentClientes);
                }
                else
                {
                    InicializarLista();
                }
            }
            catch (Exception ex)
            {
                string msj = ex.Message;
            }
            
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.search_main, menu);
            var item = menu.FindItem(Resource.Id.action_search);
            var searchView = MenuItemCompat.GetActionView(item);
            mSearchview = searchView.JavaCast<Android.Support.V7.Widget.SearchView>();
            mSearchview.QueryTextSubmit += (s, e) =>
            {
                if (adapter == null)
                    return;
                adapter.Filter.InvokeFilter(e.Query);
            };
            mSearchview.QueryTextChange += (s, e) =>
            {
                if (adapter == null)
                    return;
                if (string.IsNullOrEmpty(e.NewText))
                {
                    adapter.ResetSearch();
                }
                else
                {
                    adapter.Filter.InvokeFilter(e.NewText);
                }
            };
            
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.menu_logoff:
                    {
                        if (GestorPedido.PedidosCollection != null)
                        {
                            GestorPedido.PedidosCollection.Gestionados.Clear();
                            GC.Collect(GC.MaxGeneration);
                        }
                        //if (SyncService.isRunnig)
                        //{
                        //    this.Activity.StopService(new Intent(this.Activity, typeof(SyncService)));
                        //    SyncService.isRunnig = false;
                        //} 
                        var intent = new Intent(this.Activity, typeof(LoginActiity));
                        intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
                        this.StartActivity(intent);
                        break;
                    }
                case Resource.Id.menu_filtro:
                    {
                        var myActivity = (MainActivity)this.Activity;
                        Android.App.Dialog builder = new Android.App.Dialog(myActivity);
                        builder.Window.RequestFeature(WindowFeatures.NoTitle);
                        builder.SetContentView(Resource.Layout.dialog_filtroclt);
                        RadioButton rbCodigo = builder.FindViewById<RadioButton>(Resource.Id.rbCodigo);
                        RadioButton rbDescripcion = builder.FindViewById<RadioButton>(Resource.Id.rbDescripcion);
                        RadioGroup rgFiltros = builder.FindViewById<RadioGroup>(Resource.Id.rgFiltros);
                        rbCodigo.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rbDescripcion.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rgFiltros.CheckedChange += (sender, e) =>
                        {
                            if (rbCodigo.Checked)
                            {
                                App.FiltroCliente = App.CodigoFilter;
                                Toast.MakeText(Android.App.Application.Context, "Filtrando clientes por código.", ToastLength.Short).Show();
                                builder.Dismiss();
                            }
                            else
                            {
                                App.FiltroCliente = App.DescripcionFilter;
                                Toast.MakeText(Android.App.Application.Context, "Filtrando clientes por descripción.", ToastLength.Short).Show();
                                builder.Dismiss();
                            }

                        };
                        if (string.IsNullOrEmpty(App.FiltroCliente))
                        {
                            App.FiltroCliente = App.CodigoFilter;
                            rbCodigo.Checked = true;
                        }
                        else
                        {
                            if (App.FiltroCliente.Equals(App.CodigoFilter))
                                rbCodigo.Checked = true;
                            else
                                rbDescripcion.Checked = true;
                        }
                        builder.Window.SetBackgroundDrawableResource(Resource.Color.semitransparent);
                        builder.Show(); break;
                    }
                default: break;
            }
            return base.OnOptionsItemSelected(item);
        }

        public override void OnDetach()
        {
            list.ItemClick -= list_ItemClick;
            base.OnDetach();
        }

        #region metodos

        private async void InicializarLista()
        {
            if (cargando)
                return;
            bool res = await ExecuteConsultar();
        }

        private ConfigDocCia CargarConfiguracionCliente(string compania)
        {
            return GestorPedido.PedidosCollection.CargarConfiguracionCliente(GlobalUI.ClienteActual, compania);
        }

        private Task<bool> ExecuteConsultar()
        {
            //Log.Info("rastreo", "PedidoFragment executeConsultar");
            return Task.Run(() =>
            {
                cargando = true;
                Android.App.ProgressDialog progressDialog = null;
                var myActivity = (MainActivity)this.Activity;
                try
                {
                    myActivity.RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(myActivity, "Cargando", "Por favor espere...", true); });
                    if (adapter == null || (adapter != null && !adapter.listaInicializada()))
                    {
                        List<Cliente> lista = new List<Cliente>();
                        lista = Cliente.CargarClientes(new CriterioBusquedaCliente(CriterioCliente.Ninguno, string.Empty, true, false), App.CompaniaDefault);
                        myActivity.RunOnUiThread(() => { try { adapter = new ClienteAdapter(myActivity, lista); list.Adapter = adapter; }
                        catch (Exception ex) { string msj = ex.Message; } 
                        });
                    }                    
                    myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                    cargando = false;
                    return true;
                }
                catch
                {
                    myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                    myActivity.RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error inicializando lista.", ToastLength.Short).Show(); });
                    cargando = false;
                    return false;
                }

            });
        }

        private Task<bool> ExecuteIniciarPedido(int position)
        {
            return Task.Run(() =>
            {
                Android.App.ProgressDialog progressDialog = null;
                var myActivity = (MainActivity)this.Activity;
                try
                {
                    myActivity.RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(myActivity, "Cargando cliente", "Por favor espere...", true); });
                    GlobalUI.ClienteActual = adapter.GetItemCliente(position);
                    GlobalUI.RutaActual = Ruta.ObtenerRuta(GlobalUI.Rutas, GlobalUI.ClienteActual.Zona);
                    GlobalUI.RutasActuales = Ruta.ObtenerRutas(GlobalUI.Rutas, GlobalUI.ClienteActual.Zona);
                    GlobalUI.ClienteActual.Zona = GlobalUI.RutaActual.Codigo;
                    GlobalUI.ClienteActual.ObtenerClientesCia();
                    if (!GlobalUI.ClienteActual.NivelesPreciosDefinidos)
                    {
                        myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); Toast.MakeText(Android.App.Application.Context, "El cliente no tiene nivel de precios defidido o aprobado.", ToastLength.Short).Show(); });
                        GlobalUI.ClienteActual = null;
                        return false;
                    }
                    ConfigDocCia config = CargarConfiguracionCliente(GlobalUI.ClienteActual.Compania);
                    GlobalUI.ClienteActual.ClienteCompania[0].ObtenerDireccionesEntrega();
                    config.ClienteCia = GlobalUI.ClienteActual.ClienteCompania[0];
                    GestorPedido.PedidosCollection.CargarConfiguracionVenta(GlobalUI.ClienteActual.Compania, config);
                    myActivity.RunOnUiThread(() =>
                    {
                        progressDialog.Dismiss();
                        Regalias.LimpiarReglasXArticuloXPaquete();
                        Intent intent = new Intent(myActivity, typeof(PedidoMainActivity));
                        StartActivity(intent);
                    });
                    return true;
                }
                catch
                {
                    myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                    myActivity.RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error cargando configuración del cliente.", ToastLength.Short).Show(); });
                    return false;
                }

            });
        }


        #endregion

    }
}