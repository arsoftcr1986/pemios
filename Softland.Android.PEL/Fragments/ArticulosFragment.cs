using System;
using System.Collections.Generic;
using Android.Runtime;
using Android.Content;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Pedidos.Core;
using Android.Support.V4.View;
using BI.Shared;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace Softland.Droid.PEL
{
    public class ArticulosFragment : Fragment
    {
        Android.Support.V7.Widget.SearchView mSearchview;
        ListView list;
        ArticulosAdapter adapter;
        IMenuItem item;
        bool cargando = false;


        public ArticulosFragment()
        {
            this.RetainInstance = true;
        }

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            this.HasOptionsMenu = true;
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.ArticulosFragment, null);
            list = view.FindViewById<ListView>(Resource.Id.list);
            var myActivity = (MainActivity)this.Activity;
            myActivity.SupportActionBar.Title = myActivity.Title = "Art�culos";
            list.ItemClick += list_ItemClick;
            adapter = null;
            //InicializarLista();                        
            return view;
        }

        public override void OnResume()
        {
            InicializarLista();
            list.Enabled = true;
            base.OnResume();
        }

        void list_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            list.Enabled = false;
            var myActivity = (MainActivity)this.Activity;
            Intent intent = new Intent(myActivity, typeof(ConsulArticuloActivity));
            intent.PutExtra("codigo", adapter.GetCodigo(e.Position));
            StartActivity(intent);
        }

        public async void InicializarLista()
        {
            if (cargando)
                return;
            bool res = await ExecuteConsultar();
        }


        private Task<bool> ExecuteConsultar()
        {
            return Task.Run(() =>
            {
                cargando = true;
                Android.App.ProgressDialog progressDialog = null;
                var myActivity = (MainActivity)this.Activity;
                try
                {
                    myActivity.RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(myActivity, "Cargando Listado", "Por favor espere...", true); });
                    if (adapter == null || (adapter != null && !adapter.listaInicializada()))
                    {
                        Connection cnxArticulo = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                        if (cnxArticulo != null && cnxArticulo.isOpen)
                        {
                            List<Articulo> lista = new List<Articulo>();
                            lista = Articulo.ObtenerArticulos(new CriterioBusquedaArticulo(CriterioArticulo.Ninguno, "", false), App.CompaniaDefault, App.DeviceID, string.Empty, false, cnxArticulo);
                            myActivity.RunOnUiThread(() => { adapter = new ArticulosAdapter(myActivity, lista, true); list.Adapter = adapter; });
                        }
                        cnxArticulo.Close();
                        cnxArticulo = null;
                    }
                    myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                    cargando = false;
                    return true;
                }
                catch(Exception ex)
                {
                    myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                    myActivity.RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error inicializando lista."+ ex.Message, ToastLength.Short).Show(); });
                    cargando = false;
                    return false;
                }

            });
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.search_main, menu);
            item = menu.FindItem(Resource.Id.action_search);
            var searchView = MenuItemCompat.GetActionView(item);
            mSearchview = searchView.JavaCast<Android.Support.V7.Widget.SearchView>();
            try
            {
                mSearchview.QueryTextSubmit += (s, e) =>
                {
                    if (adapter == null || App.FiltroArticulo.Equals(App.ClasificacionFilter))
                        return;
                    adapter.Filter.InvokeFilter(e.Query);
                };
                mSearchview.QueryTextChange += (s, e) =>
                {
                    if (adapter == null || App.FiltroArticulo.Equals(App.ClasificacionFilter))
                        return;
                    if (string.IsNullOrEmpty(e.NewText))
                    {
                        adapter.ResetSearch();
                    }
                    else
                    {
                        adapter.Filter.InvokeFilter(e.NewText);
                    }
                };
            }
            catch
            {
            }

        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.menu_logoff:
                    {
                        if (GestorPedido.PedidosCollection != null)
                        {
                            GestorPedido.PedidosCollection.Gestionados.Clear();
                            GC.Collect(GC.MaxGeneration);
                        }
                        //if (SyncService.isRunnig)
                        //{
                        //    this.Activity.StopService(new Intent(this.Activity, typeof(SyncService)));
                        //    SyncService.isRunnig = false;
                        //} 
                        var intent = new Intent(this.Activity, typeof(LoginActiity));
                        intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
                        this.StartActivity(intent);
                        break;
                    }
                case Resource.Id.menu_filtro:
                    {
                        var myActivity = (MainActivity)this.Activity;
                        Android.App.Dialog builder = new Android.App.Dialog(myActivity);
                        builder.Window.RequestFeature(WindowFeatures.NoTitle);
                        builder.SetContentView(Resource.Layout.dialog_filtroart);
                        RadioButton rbCodigo = builder.FindViewById<RadioButton>(Resource.Id.rbCodigo);
                        RadioButton rbDescripcion = builder.FindViewById<RadioButton>(Resource.Id.rbDescripcion);
                        RadioButton rbBarras = builder.FindViewById<RadioButton>(Resource.Id.rbBarras);
                        RadioButton rbClasificacion = builder.FindViewById<RadioButton>(Resource.Id.rbClasificacion);
                        RadioGroup rgFiltros = builder.FindViewById<RadioGroup>(Resource.Id.rgFiltros);
                        rbCodigo.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rbDescripcion.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rbBarras.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rbClasificacion.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rgFiltros.CheckedChange += (sender, e) =>
                        {
                            if (rbCodigo.Checked)
                            {
                                this.item.SetEnabled(true);
                                App.FiltroArticulo = App.CodigoFilter;
                                Toast.MakeText(Android.App.Application.Context, "Filtrando art�culos por c�digo.", ToastLength.Short).Show();
                                builder.Dismiss();
                            }
                            else if (rbDescripcion.Checked)
                            {
                                this.item.SetEnabled(true);
                                App.FiltroArticulo = App.DescripcionFilter;
                                Toast.MakeText(Android.App.Application.Context, "Filtrando art�culos por descripci�n.", ToastLength.Short).Show();
                                builder.Dismiss();
                            }
                            else if (rbBarras.Checked)
                            {
                                this.item.SetEnabled(true);
                                App.FiltroArticulo = App.BarrasFilter;
                                Toast.MakeText(Android.App.Application.Context, "Filtrando art�culos por c�digo de barras.", ToastLength.Short).Show();
                                builder.Dismiss();
                            }
                            else
                            {
                                this.item.SetEnabled(false);
                                App.FiltroArticulo = App.ClasificacionFilter;
                                builder.Dismiss();
                                Connection cnxVerif = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                                try
                                {
                                    /*
                                     * Caso agregar clasificaciones CR6-24402-L14Y
                                     * jguzmanc
                                     * Inicio cambios
                                     */
                                    if (!Clasificacion.HayClasificaciones(App.CompaniaDefault, 1, cnxVerif) && !Clasificacion.HayClasificaciones(App.CompaniaDefault, 2, cnxVerif)
                                    && !Clasificacion.HayClasificaciones(App.CompaniaDefault, 3, cnxVerif) && !Clasificacion.HayClasificaciones(App.CompaniaDefault, 4, cnxVerif)
                                    && !Clasificacion.HayClasificaciones(App.CompaniaDefault, 5, cnxVerif) && !Clasificacion.HayClasificaciones(App.CompaniaDefault, 6, cnxVerif))
                                    {
                                        Toast.MakeText(Android.App.Application.Context, "No se encontraron clasificaciones disponibles", ToastLength.Short).Show();
                                        cnxVerif.Close();
                                        cnxVerif = null;
                                        rbCodigo.Checked = true;
                                        return;
                                    }
                                    /*
                                     * Caso agregar clasificaciones CR6-24402-L14Y
                                     * jguzmanc
                                     * Fin cambios
                                     */
                                    else
                                    {
                                        Toast.MakeText(Android.App.Application.Context, "Filtrando art�culos por clasificaci�n.", ToastLength.Short).Show();
                                    }
                                    cnxVerif.Close();
                                    cnxVerif = null;
                                }
                                catch
                                {
                                    Toast.MakeText(Android.App.Application.Context, "No se encontraron clasificaciones", ToastLength.Short).Show();
                                    cnxVerif.Close();
                                    cnxVerif = null;
                                    return;
                                }
                                Android.App.Dialog builderClas = new Android.App.Dialog(myActivity);
                                builderClas.Window.RequestFeature(WindowFeatures.NoTitle);
                                builderClas.SetContentView(Resource.Layout.dialog_clasificacion);
                                var label1 = builderClas.FindViewById<TextView>(Resource.Id.label1);
                                var label2 = builderClas.FindViewById<TextView>(Resource.Id.label2);
                                var txtFamilia = builderClas.FindViewById<TextView>(Resource.Id.txtFamilia);
                                var txtSubFamilia = builderClas.FindViewById<TextView>(Resource.Id.txtSubFamilia);

                                /**/

                                var label3 = builderClas.FindViewById<TextView>(Resource.Id.label3);
                                var label4 = builderClas.FindViewById<TextView>(Resource.Id.label4);
                                var label5 = builderClas.FindViewById<TextView>(Resource.Id.label5);
                                var label6 = builderClas.FindViewById<TextView>(Resource.Id.label6);
                                /*
                                 * Caso agregar clasificaciones CR6-24402-L14Y
                                 * jguzmanc
                                 * Inicio cambios
                                 */
                                var clasificacion3 = builderClas.FindViewById<TextView>(Resource.Id.txtClasificacion3);
                                var clasificacion4 = builderClas.FindViewById<TextView>(Resource.Id.txtClasificacion4);
                                var clasificacion5 = builderClas.FindViewById<TextView>(Resource.Id.txtClasificacion5);
                                var clasificacion6 = builderClas.FindViewById<TextView>(Resource.Id.txtClasificacion6);
                                /*
                                 * Caso agregar clasificaciones CR6-24402-L14Y
                                 * jguzmanc
                                 * Fin cambios
                                 */

                                label1.SetTextColor(Android.Graphics.Color.White);
                                label2.SetTextColor(Android.Graphics.Color.White);
                                /*
                                 * Caso agregar clasificaciones CR6-24402-L14Y
                                 * jguzmanc
                                 * Inicio cambios
                                 */
                                label3.SetTextColor(Android.Graphics.Color.White);
                                label4.SetTextColor(Android.Graphics.Color.White);
                                label5.SetTextColor(Android.Graphics.Color.White);
                                label6.SetTextColor(Android.Graphics.Color.White);
                                /*
                                 * Caso agregar clasificaciones CR6-24402-L14Y
                                 * jguzmanc
                                 * Fin cambios
                                 */
                                txtFamilia.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                                txtSubFamilia.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                                /*
                                 * Caso agregar clasificaciones CR6-24402-L14Y
                                 * jguzmanc
                                 * Inicio cambios
                                 */
                                clasificacion3.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                                clasificacion4.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                                clasificacion5.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                                clasificacion6.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                                /*
                                 * Caso agregar clasificaciones CR6-24402-L14Y
                                 * jguzmanc
                                 * Fin cambios
                                 */
                                txtFamilia.Text = App.FamiliaActual;
                                txtSubFamilia.Text = App.SubFamiliaActual;

                                /*
                                 * Caso agregar clasificaciones CR6-24402-L14Y
                                 * jguzmanc
                                 * Inicio cambios
                                 */
                                clasificacion3.Text = App.Clasificacion3;
                                clasificacion4.Text = App.Clasificacion4;
                                clasificacion5.Text = App.Clasificacion5;
                                clasificacion6.Text = App.Clasificacion6;
                                /*
                                 * Caso agregar clasificaciones CR6-24402-L14Y
                                 * jguzmanc
                                 * Fin cambios
                                 */
                                var imgEditarFamilia = builderClas.FindViewById<ImageButton>(Resource.Id.imgEditarFamilia);
                                var imgEliminarFamilia = builderClas.FindViewById<ImageButton>(Resource.Id.imgEliminarFamilia);
                                var imgEditarSubFamilia = builderClas.FindViewById<ImageButton>(Resource.Id.imgEditarSubFamilia);
                                var imgEliminarSubFamilia = builderClas.FindViewById<ImageButton>(Resource.Id.imgEliminarSubFamilia);

                                /*
                                 * Caso agregar clasificaciones CR6-24402-L14Y
                                 * jguzmanc
                                 * Inicio cambios
                                 */
                                /* clasificacion 3*/
                                var imgEditarClasificacion3 = builderClas.FindViewById<ImageButton>(Resource.Id.imgEditarClasificacion3);
                                var imgEliminarClasificacion3 = builderClas.FindViewById<ImageButton>(Resource.Id.imgEliminarClasificacion3);
                                /* clasificacion 4*/
                                var imgEditarClasificacion4 = builderClas.FindViewById<ImageButton>(Resource.Id.imgEditarClasificacion4);
                                var imgEliminarClasificacion4 = builderClas.FindViewById<ImageButton>(Resource.Id.imgEliminarClasificacion4);
                                /* clasificacion 5*/
                                var imgEditarClasificacion5 = builderClas.FindViewById<ImageButton>(Resource.Id.imgEditarClasificacion5);
                                var imgEliminarClasificacion5 = builderClas.FindViewById<ImageButton>(Resource.Id.imgEliminarClasificacion5);
                                /* clasificacion 6*/
                                var imgEditarClasificacion6 = builderClas.FindViewById<ImageButton>(Resource.Id.imgEditarClasificacion6);
                                var imgEliminarClasificacion6 = builderClas.FindViewById<ImageButton>(Resource.Id.imgEliminarClasificacion6);
                                /*
                                 * Caso agregar clasificaciones CR6-24402-L14Y
                                 * jguzmanc
                                 * Fin cambios
                                 */

                                var btnAceptar = builderClas.FindViewById<Button>(Resource.Id.btnAceptar);
                                imgEliminarFamilia.Click += (s, events) => { txtFamilia.Text = string.Empty; };
                                imgEliminarSubFamilia.Click += (s, events) => { txtSubFamilia.Text = string.Empty; };

                                /*
                                 * Caso agregar clasificaciones CR6-24402-L14Y, funcionalidad botones
                                 * jguzmanc
                                 * Inicio cambios
                                 */
                                /*Clasificaciones*/
                                imgEliminarClasificacion3.Click += (s, events) => { clasificacion3.Text = string.Empty; };
                                imgEliminarClasificacion4.Click += (s, events) => { clasificacion4.Text = string.Empty; };
                                imgEliminarClasificacion5.Click += (s, events) => { clasificacion5.Text = string.Empty; };
                                imgEliminarClasificacion6.Click += (s, events) => { clasificacion6.Text = string.Empty; };
                                /*
                                 * Caso agregar clasificaciones CR6-24402-L14Y
                                 * jguzmanc
                                 * Fin cambios
                                 */

                                btnAceptar.Click += (s, events) =>
                                {
                                    App.FamiliaActual = txtFamilia.Text;
                                    App.SubFamiliaActual = txtSubFamilia.Text;
                                    //clasificaciones
                                    /*
                                     * Caso agregar clasificaciones CR6-24402-L14Y
                                     * jguzmanc
                                     * Inicio cambios
                                     */
                                    App.Clasificacion3 = clasificacion3.Text;
                                    App.Clasificacion4 = clasificacion4.Text;
                                    App.Clasificacion5 = clasificacion5.Text;
                                    App.Clasificacion6 = clasificacion6.Text;

                                    if (string.IsNullOrEmpty(txtFamilia.Text) && string.IsNullOrEmpty(txtSubFamilia.Text)
                                    && string.IsNullOrEmpty(clasificacion3.Text) && string.IsNullOrEmpty(clasificacion4.Text)
                                    && string.IsNullOrEmpty(clasificacion5.Text) && string.IsNullOrEmpty(clasificacion6.Text))
                                    {
                                        if (adapter == null)
                                        {
                                            builderClas.Dismiss();
                                            return;
                                        }
                                        adapter.ResetSearch();
                                        builderClas.Dismiss();
                                    }
                                    else
                                    {
                                        string cadenaFiltro = "";
                                        if (!string.IsNullOrEmpty(txtFamilia.Text))
                                        {
                                            cadenaFiltro += "clasificacion1=" + txtFamilia.Text + ",";
                                        }
                                        if (!string.IsNullOrEmpty(txtSubFamilia.Text))
                                        {
                                            cadenaFiltro += "clasificacion2=" + txtSubFamilia.Text + ",";
                                        }
                                        if (!string.IsNullOrEmpty(clasificacion3.Text))
                                        {
                                            cadenaFiltro += "clasificacion3=" + clasificacion3.Text + ",";
                                        }
                                        if (!string.IsNullOrEmpty(clasificacion4.Text))
                                        {
                                            cadenaFiltro += "clasificacion4=" + clasificacion4.Text + ",";
                                        }
                                        if (!string.IsNullOrEmpty(clasificacion5.Text))
                                        {
                                            cadenaFiltro += "clasificacion5=" + clasificacion5.Text + ",";
                                        }
                                        if (!string.IsNullOrEmpty(clasificacion6.Text))
                                        {
                                            cadenaFiltro += "clasificacion6=" + clasificacion6.Text;
                                        }
                                        adapter.Filter.InvokeFilter(cadenaFiltro);
                                    }

                                    builderClas.Dismiss();

                                };
                                imgEditarFamilia.Click += (s, events) =>
                                {
                                    Android.App.Dialog buildertemp = new Android.App.Dialog(myActivity);
                                    buildertemp.Window.RequestFeature(WindowFeatures.NoTitle);
                                    buildertemp.SetContentView(Resource.Layout.dialog_clasificacion_list);
                                    ListView list = buildertemp.FindViewById<ListView>(Resource.Id.list);
                                    List<Clasificacion> lista = new List<Clasificacion>();
                                    Connection cnxArticulo = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                                    try
                                    {
                                        lista = Clasificacion.ObtenerClasificaciones(App.CompaniaDefault, 1, cnxArticulo);
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    catch
                                    {
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    ClasificacionAdapter adapterClas = new ClasificacionAdapter(myActivity, lista);
                                    list.Adapter = adapterClas;
                                    list.ItemClick += (ss, ee) =>
                                    {
                                        txtFamilia.Text = adapterClas.GetCodigo(ee.Position);
                                        buildertemp.Dismiss();
                                    };
                                    buildertemp.Window.SetBackgroundDrawableResource(Android.Resource.Drawable.DialogHoloLightFrame);
                                    buildertemp.Show();
                                };
                                imgEditarSubFamilia.Click += (s, events) =>
                                {
                                    Android.App.Dialog buildertemp = new Android.App.Dialog(myActivity);
                                    buildertemp.Window.RequestFeature(WindowFeatures.NoTitle);
                                    buildertemp.SetContentView(Resource.Layout.dialog_clasificacion_list);
                                    ListView list = buildertemp.FindViewById<ListView>(Resource.Id.list);
                                    List<Clasificacion> lista = new List<Clasificacion>();
                                    Connection cnxArticulo = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                                    try
                                    {
                                        lista = Clasificacion.ObtenerClasificaciones(App.CompaniaDefault, 2, cnxArticulo);
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    catch
                                    {
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    ClasificacionAdapter adapterClas = new ClasificacionAdapter(myActivity, lista);
                                    list.Adapter = adapterClas;
                                    list.ItemClick += (ss, ee) =>
                                    {
                                        txtSubFamilia.Text = adapterClas.GetCodigo(ee.Position);
                                        buildertemp.Dismiss();
                                    };
                                    buildertemp.Window.SetBackgroundDrawableResource(Android.Resource.Drawable.DialogHoloLightFrame);
                                    buildertemp.Show();
                                };
                                /*clasificacion 3 - 6*/
                                imgEditarClasificacion3.Click += (s, events) =>
                                {
                                    Android.App.Dialog buildertemp = new Android.App.Dialog(myActivity);
                                    buildertemp.Window.RequestFeature(WindowFeatures.NoTitle);
                                    buildertemp.SetContentView(Resource.Layout.dialog_clasificacion_list);
                                    ListView list = buildertemp.FindViewById<ListView>(Resource.Id.list);
                                    List<Clasificacion> lista = new List<Clasificacion>();
                                    Connection cnxArticulo = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                                    try
                                    {
                                        lista = Clasificacion.ObtenerClasificaciones(App.CompaniaDefault, 3, cnxArticulo);
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    catch
                                    {
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    ClasificacionAdapter adapterClas = new ClasificacionAdapter(myActivity, lista);
                                    list.Adapter = adapterClas;
                                    list.ItemClick += (ss, ee) =>
                                    {
                                        clasificacion3.Text = adapterClas.GetCodigo(ee.Position);
                                        buildertemp.Dismiss();
                                    };
                                    buildertemp.Window.SetBackgroundDrawableResource(Android.Resource.Drawable.DialogHoloLightFrame);
                                    buildertemp.Show();
                                };

                                //4
                                imgEditarClasificacion4.Click += (s, events) =>
                                {
                                    Android.App.Dialog buildertemp = new Android.App.Dialog(myActivity);
                                    buildertemp.Window.RequestFeature(WindowFeatures.NoTitle);
                                    buildertemp.SetContentView(Resource.Layout.dialog_clasificacion_list);
                                    ListView list = buildertemp.FindViewById<ListView>(Resource.Id.list);
                                    List<Clasificacion> lista = new List<Clasificacion>();
                                    Connection cnxArticulo = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                                    try
                                    {
                                        lista = Clasificacion.ObtenerClasificaciones(App.CompaniaDefault, 4, cnxArticulo);
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    catch
                                    {
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    ClasificacionAdapter adapterClas = new ClasificacionAdapter(myActivity, lista);
                                    list.Adapter = adapterClas;
                                    list.ItemClick += (ss, ee) =>
                                    {
                                        clasificacion4.Text = adapterClas.GetCodigo(ee.Position);
                                        buildertemp.Dismiss();
                                    };
                                    buildertemp.Window.SetBackgroundDrawableResource(Android.Resource.Drawable.DialogHoloLightFrame);
                                    buildertemp.Show();
                                };

                                //5
                                imgEditarClasificacion5.Click += (s, events) =>
                                {
                                    Android.App.Dialog buildertemp = new Android.App.Dialog(myActivity);
                                    buildertemp.Window.RequestFeature(WindowFeatures.NoTitle);
                                    buildertemp.SetContentView(Resource.Layout.dialog_clasificacion_list);
                                    ListView list = buildertemp.FindViewById<ListView>(Resource.Id.list);
                                    List<Clasificacion> lista = new List<Clasificacion>();
                                    Connection cnxArticulo = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                                    try
                                    {
                                        lista = Clasificacion.ObtenerClasificaciones(App.CompaniaDefault, 5, cnxArticulo);
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    catch
                                    {
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    ClasificacionAdapter adapterClas = new ClasificacionAdapter(myActivity, lista);
                                    list.Adapter = adapterClas;
                                    list.ItemClick += (ss, ee) =>
                                    {
                                        clasificacion5.Text = adapterClas.GetCodigo(ee.Position);
                                        buildertemp.Dismiss();
                                    };
                                    buildertemp.Window.SetBackgroundDrawableResource(Android.Resource.Drawable.DialogHoloLightFrame);
                                    buildertemp.Show();
                                };

                                //6
                                imgEditarClasificacion6.Click += (s, events) =>
                                {
                                    Android.App.Dialog buildertemp = new Android.App.Dialog(myActivity);
                                    buildertemp.Window.RequestFeature(WindowFeatures.NoTitle);
                                    buildertemp.SetContentView(Resource.Layout.dialog_clasificacion_list);
                                    ListView list = buildertemp.FindViewById<ListView>(Resource.Id.list);
                                    List<Clasificacion> lista = new List<Clasificacion>();
                                    Connection cnxArticulo = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                                    try
                                    {
                                        lista = Clasificacion.ObtenerClasificaciones(App.CompaniaDefault, 6, cnxArticulo);
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    catch
                                    {
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    ClasificacionAdapter adapterClas = new ClasificacionAdapter(myActivity, lista);
                                    list.Adapter = adapterClas;
                                    list.ItemClick += (ss, ee) =>
                                    {
                                        clasificacion6.Text = adapterClas.GetCodigo(ee.Position);
                                        buildertemp.Dismiss();
                                    };
                                    buildertemp.Window.SetBackgroundDrawableResource(Android.Resource.Drawable.DialogHoloLightFrame);
                                    buildertemp.Show();
                                };

                                /*
                                 * Caso agregar clasificaciones CR6-24402-L14Y, funcionalidad botones
                                 * jguzmanc
                                 * Fin cambios
                                 */
                                builderClas.Window.SetBackgroundDrawableResource(Resource.Color.semitransparent);
                                builderClas.Show();
                            }


                        };
                        if (string.IsNullOrEmpty(App.FiltroArticulo))
                        {
                            App.FiltroArticulo = App.CodigoFilter;
                            rbCodigo.Checked = true;
                        }
                        else
                        {
                            if (App.FiltroArticulo.Equals(App.CodigoFilter) || App.FiltroArticulo.Equals(App.SeleccionadosFilter) || App.FiltroArticulo.Equals(App.ClasificacionFilter))
                            {
                                rbCodigo.Checked = true;
                                App.FiltroArticulo = App.CodigoFilter;
                            }
                            else if (App.FiltroArticulo.Equals(App.DescripcionFilter))
                                rbDescripcion.Checked = true;
                            else
                                rbBarras.Checked = true;
                        }
                        builder.Window.SetBackgroundDrawableResource(Resource.Color.semitransparent);
                        builder.Show(); break;
                    }
                default: break;
            }
            return base.OnOptionsItemSelected(item);
        }


        public override void OnDetach()
        {
            list.ItemClick -= list_ItemClick;
            base.OnDetach();
        }
    }
}