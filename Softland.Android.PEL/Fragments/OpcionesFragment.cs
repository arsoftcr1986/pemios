using System;
using System.Collections.Generic;

using Android.Content;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Data.SQLite;

using BI.Android;
using BI.Shared;

using Pedidos.Core;
using Android.Bluetooth;

namespace Softland.Droid.PEL
{
    public class OpcionesFragment : Fragment
    {
        Spinner cmbCompania;
        Button btnDatos,btnDatosParcial, btnImagenes;
        public List<string> companias;

        public OpcionesFragment()
        {
            this.RetainInstance = true;
        }

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            this.HasOptionsMenu = true;
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.OpcionesFragment, null);

            cmbCompania = view.FindViewById<Spinner>(Resource.Id.cmbCompania);

            btnDatos = view.FindViewById<Button>(Resource.Id.btnDatos);
            btnDatosParcial = view.FindViewById<Button>(Resource.Id.btnDatosParcial);
            btnImagenes = view.FindViewById<Button>(Resource.Id.btnImagenes);

            cmbCompania.ItemSelected += cmbCompania_ItemSelected;
            btnDatos.Click+=btnDatos_Click;
            btnDatosParcial.Click += btnDatosParcial_Click;
            btnImagenes.Click+=btnImagenes_Click;

            var myActivity = (MainActivity)this.Activity;

            if (string.IsNullOrEmpty(App.CompaniaDefault))
            {
                if (App.VerificarExistenciaArchivo(System.IO.Path.Combine(App.PathAplicacion, App.BDName)))
                {
                    Toast.MakeText(Android.App.Application.Context, "Se seteo la compa��a predeterminada ya que no exist�a.", ToastLength.Long).Show();
                }                
            }
            

            var lista=Compania.Obtener().ConvertAll(x => x.Codigo);

            var adapter = new ArrayAdapter<string>(myActivity, Resource.Layout.SimpleSpinnerItem,lista );
            adapter.SetDropDownViewResource(Resource.Layout.SimpleSpinnerDropDownItem);
            cmbCompania.Adapter = adapter;
            if (!string.IsNullOrEmpty(App.CompaniaDefault))
            {
                try
                {
                    cmbCompania.SetSelection(((ArrayAdapter)cmbCompania.Adapter).GetPosition(App.CompaniaDefault));
                }
                catch
                {
                    cmbCompania.SetSelection(0);
                }
            }

            myActivity.SupportActionBar.Title = myActivity.Title = "Opciones";          

            return view;
        }

        void cmbConsecutivo_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var myActivity = (MainActivity)this.Activity;
            App.ConsecutivoPedido = ((Spinner)sender).SelectedItem.ToString();
            CrearPreferencia(1);
        }

        void cmbCompania_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var myActivity = (MainActivity)this.Activity;
            if(!string.IsNullOrEmpty(((Spinner)sender).SelectedItem.ToString()))
            {
                if (App.CompaniaDefault != null && !App.CompaniaDefault.Equals(((Spinner)sender).SelectedItem.ToString()))
                {
                    if (GestorPedido.PedidosCollection.Gestionados != null && GestorPedido.PedidosCollection.Gestionados.Count>0)
                    {
                        GestorPedido.PedidosCollection.Gestionados.Clear();
                        Toast.MakeText(Android.App.Application.Context, "Se elimin� el pedido en memoria debido al cambio de compa��a",ToastLength.Short).Show();
                        GC.Collect(GC.MaxGeneration);
                    }
                }
                App.CompaniaDefault = ((Spinner)sender).SelectedItem.ToString();
                CrearPreferencia(0);

                // Carga el simbolo monetario de la compa�ia guardada en la Configuraci�n.
                var compania = Compania.Obtener(App.CompaniaDefault);

                if (compania != null)
                {
                    GestorUtilitario.SimboloMonetario = compania.SimboloMonetarioFunc;
                }

                //LlenarListaConsecutivos(((Spinner)sender).SelectedItem.ToString());
            }            
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.logoff, menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.menu_logoff:
                    {
                        if (GestorPedido.PedidosCollection != null)
                        {
                            GestorPedido.PedidosCollection.Gestionados.Clear();
                            GC.Collect(GC.MaxGeneration);
                        }
                        //if (SyncService.isRunnig)
                        //{
                        //    this.Activity.StopService(new Intent(this.Activity, typeof(SyncService)));
                        //    SyncService.isRunnig = false;
                        //}    
                        var intent = new Intent(this.Activity, typeof(LoginActiity));
                        intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
                        this.StartActivity(intent);
                        break;
                    }
                case Resource.Id.menu_print:
                    {
                        var myActivity = (MainActivity)this.Activity;
                        bool encendiendo = false;
                        List<string> ImpresorasList = new List<string>();
                        List<double> papers = new List<double> { 2, 2.5, 3 };
                        //Validaci�n
                        if (encendiendo)
                        {
                            encendiendo = false;
                        }
                        else if (BluetoothAdapter.DefaultAdapter == null)
                        {                            
                            Toast.MakeText(Android.App.Application.Context, "El dispositivo no soporta bluetooth", ToastLength.Long).Show();
                            break;
                        }
                        else if (!BluetoothAdapter.DefaultAdapter.IsEnabled)
                        {
                            //.
                            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(myActivity);
                            alert.SetTitle("Bluetooth Desactivado");
                            alert.SetMessage("Activar el bluetooth para seleccionar la impresora?");
                            alert.SetPositiveButton("Si", delegate
                            {
                                BluetoothAdapter.DefaultAdapter.Enable();
                                encendiendo = true;
                                Toast.MakeText(Android.App.Application.Context, "Bluetooth encendido", ToastLength.Long).Show();
                            });
                            alert.SetNegativeButton("No", delegate
                            {
                                return;
                            });
                            myActivity.RunOnUiThread(() => { alert.Show(); });
                            //.

                        }
                        else
                        {
                            ImpresorasList = new List<string>();
                            if (BluetoothAdapter.DefaultAdapter.BondedDevices.Count > 0)
                            {
                                ICollection<BluetoothDevice> btd = BluetoothAdapter.DefaultAdapter.BondedDevices;
                                foreach (BluetoothDevice d in btd)
                                {
                                    ImpresorasList.Add(d.Name);
                                }
                            }
                            else
                            {
                                Toast.MakeText(Android.App.Application.Context,  "No hay ning�n dispositivo vinculado", ToastLength.Long).Show();  
                                break;
                            }
                            Android.App.Dialog builderClas = new Android.App.Dialog(myActivity);
                            builderClas.Window.RequestFeature(WindowFeatures.NoTitle);
                            builderClas.SetContentView(Resource.Layout.dialog_impresoras);
                            Spinner cmbPapeles, cmbImpresoras, cmbTipos;
                            var label1 = builderClas.FindViewById<TextView>(Resource.Id.label1);
                            var label2 = builderClas.FindViewById<TextView>(Resource.Id.label2);
                            var label3 = builderClas.FindViewById<TextView>(Resource.Id.label3);
                            var buttonAceptar = builderClas.FindViewById<Button>(Resource.Id.btnAceptar);
                            var chkSugerir = builderClas.FindViewById<CheckBox>(Resource.Id.chkSugerir);
                            label1.SetTextColor(Android.Graphics.Color.White);
                            label2.SetTextColor(Android.Graphics.Color.White);
                            label3.SetTextColor(Android.Graphics.Color.White);
                            chkSugerir.SetTextColor(Android.Graphics.Color.White);
                            cmbPapeles = builderClas.FindViewById<Spinner>(Resource.Id.cmbPapel);
                            cmbImpresoras = builderClas.FindViewById<Spinner>(Resource.Id.cmbImpresora);
                            cmbTipos = builderClas.FindViewById<Spinner>(Resource.Id.cmbTipo);
                            var adapter = new ArrayAdapter<string>(myActivity, Resource.Layout.SimpleSpinnerItemWhite, ImpresorasList);
                            adapter.SetDropDownViewResource(Resource.Layout.SimpleSpinnerDropDownItem);
                            cmbImpresoras.Adapter = adapter;
                            var adapterPapels = new ArrayAdapter<Double>(myActivity, Resource.Layout.SimpleSpinnerItemWhite, papers);
                            adapterPapels.SetDropDownViewResource(Resource.Layout.SimpleSpinnerDropDownItem);
                            cmbPapeles.Adapter = adapterPapels;
                            var adapterTipos = new ArrayAdapter<string>(myActivity, Resource.Layout.SimpleSpinnerItemWhite, new List<string> { "Generica", "Sewoo(LK-P30)","Zebra(MZ-iMZ)" });
                            adapterTipos.SetDropDownViewResource(Resource.Layout.SimpleSpinnerDropDownItem);
                            cmbTipos.Adapter = adapterTipos;
                            if (!string.IsNullOrEmpty(FRmConfig.Impresora) && (BluetoothAdapter.DefaultAdapter.BondedDevices.Count > 0))
                            {
                                if (BluetoothAdapter.DefaultAdapter != null && BluetoothAdapter.DefaultAdapter.IsEnabled)
                                {
                                    if (BluetoothAdapter.DefaultAdapter.BondedDevices.Count > 0)
                                    {
                                        foreach (string str in ImpresorasList)
                                        {
                                            if (str.Equals(FRmConfig.Impresora))
                                            {
                                                cmbImpresoras.SetSelection(((ArrayAdapter<string>)cmbImpresoras.Adapter).GetPosition(FRmConfig.Impresora));
                                            }
                                        }
                                    }
                                }
                            }
                            chkSugerir.Checked= Impresora.SugerirImprimir;
                            chkSugerir.CheckedChange += (s, e) => { Impresora.SugerirImprimir = chkSugerir.Checked; };
                            cmbTipos.SetSelection(((ArrayAdapter<string>)cmbTipos.Adapter).GetPosition(FRmConfig.TipoImpresora));
                            cmbPapeles.SetSelection(((ArrayAdapter<double>)cmbPapeles.Adapter).GetPosition(FRmConfig.Tama�oPapel));
                            cmbImpresoras.ItemSelected += (e, s) => { FRmConfig.Impresora = cmbImpresoras.SelectedItem.ToString(); };
                            cmbTipos.ItemSelected += (e, s) => { FRmConfig.TipoImpresora = cmbTipos.SelectedItem.ToString(); };
                            cmbPapeles.ItemSelected += (e, s) => { FRmConfig.Tama�oPapel = Convert.ToDouble(cmbPapeles.SelectedItem.ToString()); };
                            buttonAceptar.Click += (e, s) => {
                                try
                                {
                                    string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                                    string file = System.IO.Path.Combine(folder, "PrintPaperPreferences.txt");
                                    using (System.IO.StreamWriter st = new System.IO.StreamWriter(file, false))
                                    {
                                        st.Write(FRmConfig.Tama�oPapel.ToString());
                                    };
                                    folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                                    file = System.IO.Path.Combine(folder, "PrintNamePreferences.txt");
                                    using (System.IO.StreamWriter st = new System.IO.StreamWriter(file, false))
                                    {
                                        st.Write(FRmConfig.Impresora.ToString());
                                    };
                                    folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                                    file = System.IO.Path.Combine(folder, "PrintTypePreferences.txt");
                                    using (System.IO.StreamWriter st = new System.IO.StreamWriter(file, false))
                                    {
                                        st.Write(FRmConfig.TipoImpresora.ToString());
                                    };
                                    folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                                    file = System.IO.Path.Combine(folder, "PrintSugerirPreferences.txt");
                                    using (System.IO.StreamWriter st = new System.IO.StreamWriter(file, false))
                                    {
                                        if(Impresora.SugerirImprimir)
                                        {
                                            st.Write("SI");
                                        }
                                        else
                                        {
                                            st.Write("NO");
                                        }
                                        
                                    };
                                    Toast.MakeText(Android.App.Application.Context, "Preferencias de impresi�n guardadas.", ToastLength.Long).Show();
                                    builderClas.Dismiss();
                                }
                                catch
                                {
                                    Toast.MakeText(Android.App.Application.Context, "Error al guardar las preferencias de impresi�n.", ToastLength.Long).Show();
                                    builderClas.Dismiss();
                                }
                            };
                            builderClas.Window.SetBackgroundDrawableResource(Resource.Color.semitransparent);
                            builderClas.Show(); break;
                        }
                        break;
                    }
                case Resource.Id.menu_delete:
                    {
                        var myActivity = (MainActivity)this.Activity;
                        Android.App.AlertDialog.Builder alertb = new Android.App.AlertDialog.Builder(myActivity);
                        alertb.SetTitle(string.Empty);
                        alertb.SetMessage("�Desea eliminar todos los datos de aplicaci�n preferencias,base datos e im�genes?");
                        alertb.SetPositiveButton("Si",async delegate
                        {
                            bool result = await EliminarDatos();
                            if (result)
                            {
                                if (GestorPedido.PedidosCollection != null)
                                {
                                    GestorPedido.PedidosCollection.Gestionados.Clear();
                                    GC.Collect(GC.MaxGeneration);
                                }
                                if (SyncService.isRunnig)
                                {
                                    myActivity.StopService(new Intent(myActivity, typeof(SyncService)));
                                    SyncService.isRunnig = false;
                                }                                    
                                var intent = new Intent(this.Activity, typeof(LoginActiity));
                                intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
                                this.StartActivity(intent);
                            }
                        });
                        alertb.SetNegativeButton("No", delegate
                        {
                            return;
                        });
                        alertb.Show();
                        break;
                    }
                case Resource.Id.menu_websoporte:
                    {
                        
                        try
                        {
                            var uri = Android.Net.Uri.Parse(App.WebSoporte);
                            var intent = new Intent(Intent.ActionView, uri);
                            StartActivity(intent);
                        }
                        catch
                        {
                            Toast.MakeText(Android.App.Application.Context, 
                                "La direcci�n Web de la p�gina de soporte no est� configurada correctamente, consulte con su administrador.",
                                ToastLength.Long).Show();
                        }
                        break;
                    }
                case Resource.Id.menu_id:
                    {
                        var myActivity = (MainActivity)this.Activity;
                        Android.App.AlertDialog.Builder alertb = new Android.App.AlertDialog.Builder(myActivity);
                        alertb.SetTitle("Dispositivo");
                        alertb.SetMessage(App.DeviceID);
                        alertb.SetNeutralButton("OK", delegate
                        {
                            return;
                        });
                        alertb.Show();
                        break;
                    }
                case Resource.Id.menu_acercade:
                    {
                        var myActivity = (MainActivity)this.Activity;

                        Android.App.Dialog builder = new Android.App.Dialog(myActivity);
                        builder.Window.RequestFeature(WindowFeatures.NoTitle);
                        builder.SetContentView(Resource.Layout.dialog_acercade);
                        TextView txtMensaje = builder.FindViewById<TextView>(Resource.Id.mensaje);
                        Button btnAceptar = builder.FindViewById<Button>(Resource.Id.btnAceptar);
                        txtMensaje.Text = "Pedidos M�vil es una aplicaci�n que permite al usuario consultar en su dispositivo informaci�n del ERP relacionada con las ventas y generar pedidos que luego se ver�n reflejados en el ERP Desktop.\n\n\nPrincipales caracter�sticas:\n\n- Consulta de art�culos, existencias, clientes y documentos de los clientes.\n\n- Generaci�n de pedidos.\n\n- Modalidad Online para que los pedidos generados sean enviados a la base de datos del ERP si tiene conexi�n.\n\n- Modalidad Offline que permite al usuario generar pedidos aun sin conexi�n, luego al recuperar la conexi�n los pedidos pendientes se sincronizan.\n\n- C�lculo de los impuestos, descuentos y bonificaciones en el dispositivo m�vil.";
                        btnAceptar.Click += (sender, e) =>
                        {
                            builder.Hide();
                        };
                        builder.Show();
                        break;
                    }
                case Resource.Id.action_settings:
                    {
                        var myActivity = (MainActivity)this.Activity;
                        Android.App.Dialog builder = new Android.App.Dialog(myActivity);
                        builder.Window.RequestFeature(WindowFeatures.NoTitle);
                        builder.SetContentView(Resource.Layout.dialog_servidor);
                        EditText txtServidor = builder.FindViewById<EditText>(Resource.Id.txtServidor);
                        Button btnAceptar = builder.FindViewById<Button>(Resource.Id.btnAceptar);
                        btnAceptar.Click += (sender, e) =>
                        {
                            if (string.IsNullOrEmpty(txtServidor.Text))
                            {
                                Toast.MakeText(Android.App.Application.Context, "Debe digitar el servidor!", ToastLength.Short).Show();
                            }
                            else
                            {
                                App.Servidor = txtServidor.Text;
                                CrearPreferencia(2);
                                builder.Dismiss();
                            }
                        };
                        if (!string.IsNullOrEmpty(App.Servidor))
                        {
                            txtServidor.Text = App.Servidor;
                        }
                        txtServidor.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        txtServidor.SetHintTextColor(Android.Graphics.Color.LightSlateGray);
                        builder.Window.SetBackgroundDrawableResource(Resource.Color.semitransparent);
                        builder.Show(); break;
                    }
                default: break;
            }
            return base.OnOptionsItemSelected(item);
        }

        public override void OnDetach()
        {
            cmbCompania.ItemSelected -= cmbCompania_ItemSelected;
            btnDatos.Click -= btnDatos_Click;
            btnDatosParcial.Click -= btnDatosParcial_Click;
            btnImagenes.Click -= btnImagenes_Click;
            base.OnDetach();
        }

        #region metodos

        private Task<string> ejecutarBD()
        {

            return Task.Run(() =>
            {
                Android.App.ProgressDialog progressDialog = null;
                var myActivity = (MainActivity)this.Activity;
                Connection cnx = null;
                try
                {
                    bool result = true;
                    myActivity.RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(myActivity, "Sincronizando", "Por favor espere...", true); });
                    conexion con = new conexion(App.DeviceID, App.Servidor, App.Usuario);
                    cnx = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                    string error = string.Empty;
                    myActivity.RunOnUiThread(() => { progressDialog.SetMessage("Obteniendo sentencias del servidor"); });
                    if (result && con.ObtenerSentenciasServidor(ref error))
                    {
                        result = true;
                    }
                    else
                    {
                        cnx.Close();
                        myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return error;
                    }
                    myActivity.RunOnUiThread(() => { progressDialog.SetMessage("Creando Tablas en dispositivo"); });
                    if (result && con.CrearEstructuraDinamica(App.PathAplicacion, ref error, cnx))
                    {
                        result = true;
                        cnx.Close();
                    }
                    else
                    {
                        cnx.Close();
                        myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return error;
                    }
                    myActivity.RunOnUiThread(() => { progressDialog.SetMessage("Descargando sentencias insert"); });
                    if (result && con.continuarInsertar && con.DescargarArchivoSincro(App.PathAplicacion, ref error))
                    {
                        result = true;
                        cnx.Close();
                    }
                    else
                    {
                        cnx.Close();
                        myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return error;
                    }
                    myActivity.RunOnUiThread(() => { progressDialog.SetMessage("Descomprimiendo sentencias insert"); });
                    if (result && con.continuarInsertar && con.DescomprimirArchivoSincro(App.PathAplicacion, ref error))
                    {
                        result = true;
                        cnx.Close();
                    }
                    else
                    {
                        cnx.Close();
                        myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return error;
                    }
                    myActivity.RunOnUiThread(() => { progressDialog.SetMessage("Insertando Datos en dispositivo"); });
                    cnx.Open();
                    if (result && con.continuarInsertar &&  con.InsertDatosDinamico(App.PathAplicacion, ref error, cnx,App.DeviceID))
                    {
                        cnx.Close();
                        if (Pedidos.Core.GestorDatos.cnx != null && Pedidos.Core.GestorDatos.cnx.isOpen)
                        {
                            Pedidos.Core.GestorDatos.cnx.Close();
                            Pedidos.Core.GestorDatos.cnx = null;
                        }
                        Pedidos.Core.GestorDatos.cnx = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                        Pedidos.Core.GestorDatos.cnx.Open();
                        try
                        {
                            GlobalUI.Rutas = Ruta.ObtenerRutas();
                            GlobalUI.CargaParametrosX(false);
                            GlobalUI.ValidarConfiguracion();
                        }
                        catch
                        {
                            myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                            return "Base de datos creada correctamente pero los par�metros no se cargaron correctamente.";
                        }

                        if (!string.IsNullOrEmpty(con.atrWebSoporte))
                        {
                            App.WebSoporte = con.atrWebSoporte;

                            if (!App.WebSoporte.Contains("http://"))
                            {
                                App.WebSoporte = "http://" + App.WebSoporte;
                            }
                        }

                        App.CambiarDescuento = con.atrCambiarDescuento;

                        var prefs = this.Activity.GetSharedPreferences("Softland.Android.PEM.preferences", FileCreationMode.Private);
                        var editor = prefs.Edit();
                        editor.PutString("websoporte", App.WebSoporte);
                        editor.PutBoolean("cambiardescuento", App.CambiarDescuento);
                        editor.Commit();

                        myActivity.RunOnUiThread(() => { progressDialog.Dismiss();
                        var lista = Compania.Obtener().ConvertAll(x => x.Codigo);
                        var adapter = new ArrayAdapter<string>(myActivity, Resource.Layout.SimpleSpinnerItem, lista);
                        adapter.SetDropDownViewResource(Resource.Layout.SimpleSpinnerDropDownItem);
                        cmbCompania.Adapter = adapter;
                        if (!string.IsNullOrEmpty(App.CompaniaDefault))
                        {
                            try
                            {
                                cmbCompania.SetSelection(((ArrayAdapter)cmbCompania.Adapter).GetPosition(App.CompaniaDefault));
                            }
                            catch
                            {
                                cmbCompania.SetSelection(0);
                            }
                        }
                        });
                        return "Base de datos creada correctamente";
                    }
                    else
                    {
                        cnx.Close();
                        myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return error;
                    }
                }
                catch
                {
                    if (cnx != null && cnx.isOpen)
                        cnx.Close();
                    myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                    return "Ocurri� un error al crear la base de datos";
                }
            });
        }

        private Task<string> ejecutarBD(List<string> creates,List<string> inserts)
        {

            return Task.Run(() =>
            {
                Android.App.ProgressDialog progressDialog = null;
                var myActivity = (MainActivity)this.Activity;
                Connection cnx = null;
                try
                {
                    bool result = true;
                    myActivity.RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(myActivity, "Sincronizando", "Por favor espere...", true); });
                    conexion con = new conexion(App.DeviceID, App.Servidor, App.Usuario);
                    cnx = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                    string error = string.Empty;
                    myActivity.RunOnUiThread(() => { progressDialog.SetMessage("Obteniendo sentencias del servidor"); });
                    if (result && con.ObtenerSentenciasServidor(ref error,creates,inserts))
                    {
                        result = true;
                    }
                    else
                    {
                        cnx.Close();
                        myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return error;
                    }
                    myActivity.RunOnUiThread(() => { progressDialog.SetMessage("Creando Tablas en dispositivo"); });
                    if (result && con.CrearEstructuraDinamica(App.PathAplicacion, ref error, cnx))
                    {
                        result = true;
                        cnx.Close();
                    }
                    else
                    {
                        cnx.Close();
                        myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return error;
                    }
                    myActivity.RunOnUiThread(() => { progressDialog.SetMessage("Descargando sentencias insert"); });
                    if (result && con.continuarInsertar && con.DescargarArchivoSincro(App.PathAplicacion, ref error))
                    {
                        result = true;
                        cnx.Close();
                    }
                    else
                    {
                        cnx.Close();
                        myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return error;
                    }
                    myActivity.RunOnUiThread(() => { progressDialog.SetMessage("Descomprimiendo sentencias insert"); });
                    if (result && con.continuarInsertar && con.DescomprimirArchivoSincro(App.PathAplicacion, ref error))
                    {
                        result = true;
                        cnx.Close();
                    }
                    else
                    {
                        cnx.Close();
                        myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return error;
                    }
                    myActivity.RunOnUiThread(() => { progressDialog.SetMessage("Insertando Datos en dispositivo"); });
                    cnx.Open();
                    if (result && con.continuarInsertar && con.InsertDatosDinamico(App.PathAplicacion, ref error, cnx, App.DeviceID))
                    {
                        cnx.Close();
                        if (Pedidos.Core.GestorDatos.cnx != null && Pedidos.Core.GestorDatos.cnx.isOpen)
                        {
                            Pedidos.Core.GestorDatos.cnx.Close();
                            Pedidos.Core.GestorDatos.cnx = null;
                        }
                        Pedidos.Core.GestorDatos.cnx = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                        Pedidos.Core.GestorDatos.cnx.Open();
                        try
                        {
                            GlobalUI.Rutas = Ruta.ObtenerRutas();
                            GlobalUI.CargaParametrosX(false);
                            GlobalUI.ValidarConfiguracion();
                        }
                        catch
                        {
                            myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                            return "Base de datos creada correctamente pero los par�metros no se cargaron correctamente.";
                        }

                        if (!string.IsNullOrEmpty(con.atrWebSoporte))
                        {
                            App.WebSoporte = con.atrWebSoporte;

                            if (!App.WebSoporte.Contains("http://"))
                            {
                                App.WebSoporte = "http://" + App.WebSoporte;
                            }
                        }

                        App.CambiarDescuento = con.atrCambiarDescuento;

                        var prefs = this.Activity.GetSharedPreferences("Softland.Android.PEM.preferences", FileCreationMode.Private);
                        var editor = prefs.Edit();
                        editor.PutString("websoporte", App.WebSoporte);
                        editor.PutBoolean("cambiardescuento", App.CambiarDescuento);
                        editor.Commit();

                        myActivity.RunOnUiThread(() => { progressDialog.Dismiss();
                        var lista = Compania.Obtener().ConvertAll(x => x.Codigo);
                        var adapter = new ArrayAdapter<string>(myActivity, Resource.Layout.SimpleSpinnerItem, lista);
                        adapter.SetDropDownViewResource(Resource.Layout.SimpleSpinnerDropDownItem);
                        cmbCompania.Adapter = adapter;
                        if (!string.IsNullOrEmpty(App.CompaniaDefault))
                        {
                            try
                            {
                                cmbCompania.SetSelection(((ArrayAdapter)cmbCompania.Adapter).GetPosition(App.CompaniaDefault));
                            }
                            catch
                            {
                                cmbCompania.SetSelection(0);
                            }
                        }


                        });
                        return "Base de datos creada correctamente";
                    }
                    else
                    {
                        cnx.Close();
                        myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return error;
                    }
                }
                catch
                {
                    if (cnx != null && cnx.isOpen)
                        cnx.Close();
                    myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                    return "Ocurri� un error al crear la base de datos";
                }
            });
        }

        private Task<string> ejecutarImagenes()
        {
            return Task.Run(() =>
            {
                Android.App.ProgressDialog progressDialog = null;
                var myActivity = (MainActivity)this.Activity;
                Connection cnx = null;
                try
                {
                    bool result = true;
                    myActivity.RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(myActivity, "Sincronizando", "Por favor espere...", true); });
                    conexion con = new conexion(App.DeviceID, App.Servidor, App.Usuario);
                    //cnx = new Connection(System.IO.Path.Combine(ObtenerRutaSD(), "miBase.db"));
                    string error = string.Empty;
                    myActivity.RunOnUiThread(() => { progressDialog.SetMessage("Obteniendo archivo de fotos del servidor"); });
                    if (result && con.DescargarArchivoFotos(App.PathAplicacion, ref error))
                    {
                        result = true;
                    }
                    else
                    {
                        myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return error;
                    }
                    myActivity.RunOnUiThread(() => { progressDialog.SetMessage("Descomprimiendo archivo de fotos"); });
                    if (result && con.DescomprimirArchivoFotos(App.PathAplicacion, ref error))
                    {
                        result = true;
                    }
                    else
                    {
                        myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return error;
                    }
                    myActivity.RunOnUiThread(() => { progressDialog.SetMessage("Copiando im�genes en dispositivo"); });
                    if (result && con.CopiarArchivoFotos(App.PathAplicacion, ref error))
                    {
                        myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return "Im�genes descargadas correctamente";
                    }
                    else
                    {
                        myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return error;
                    }
                }
                catch
                {
                    myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                    return "Ocurri� un error al descargar im�genes";
                }
            });

        }

        async void btnDatos_Click(object sender, EventArgs e)
        {
          
                if (string.IsNullOrEmpty(App.Servidor))
                {
                    Toast.MakeText(Android.App.Application.Context, "Configure el servidor..", ToastLength.Short).Show();
                    return;
                }
                ((Button)sender).Enabled = false;
                string result = await ejecutarBD();
                ((Button)sender).Enabled = true;

                if (result.Equals("Base de datos creada correctamente"))
                {

                    bool usuarioAutenticado = true;

                    // Se verifica si el usuario actual sigue estando en los datos actualizados.
                    if (!GestorSeguridad.AutenticarUsuario(BI.Android.GestorDatos.NombreUsuario, BI.Android.GestorDatos.ContrasenaUsuario))
                    {
                        result += ". Parece que su usuario no es v�lido seg�n los datos actualizados, por favor inicie sesi�n nuevamente.";

                        usuarioAutenticado = false;
                    }

                    if (!usuarioAutenticado)
                    {
                        // Limpiar la lista de compa��as gestionadas.
                        Compania.Gestionadas = new List<Compania>();

                        if (GlobalUI.ClienteActual != null)
                        {
                            // Limpia y reinicia los datos del pedido global.
                            PedidosCollection.FacturarPedido = false;
                            GestorPedido.PedidosCollection = new PedidosCollection();
                            Pedido.BorrarXMLPedido(GlobalUI.ClienteActual.Codigo);

                            // Limpia el carrito de compras.
                            GestorPedido.carrito = null;

                            // Limpia al cliente del pedido.
                            GlobalUI.ClienteActual = null;
                            GlobalUI.RutaActual = null;
                            GlobalUI.RutasActuales = null;
                        }

                        var myActivity = (MainActivity)this.Activity;
                        Android.App.AlertDialog.Builder alertb = new Android.App.AlertDialog.Builder(myActivity);
                        alertb.SetTitle("Proceso finalizado");
                        alertb.SetMessage(result);
                        alertb.SetNeutralButton("OK", delegate
                        {
                            GC.Collect(GC.MaxGeneration);

                            var intent = new Intent(this.Activity, typeof(LoginActiity));
                            intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
                            this.StartActivity(intent);
                        });
                        alertb.Show();
                    }
                    else
                    {
                        Toast.MakeText(Android.App.Application.Context, result, ToastLength.Long).Show();
                    }
                }
                else
                {
                    Toast.MakeText(Android.App.Application.Context, result, ToastLength.Long).Show();
                }
        }

        void btnDatosParcial_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(App.Servidor))
            {
                Toast.MakeText(Android.App.Application.Context, "Configure el servidor..", ToastLength.Short).Show();
                return;
            }
            var myActivity = (MainActivity)this.Activity;
            Android.App.Dialog builder = new Android.App.Dialog(myActivity);
            builder.Window.RequestFeature(WindowFeatures.NoTitle);
            builder.SetContentView(Resource.Layout.dialog_sincros);
            ListView list = builder.FindViewById<ListView>(Resource.Id.list);
            SimpleListAdapter adapter;
            List<string> lista = new List<string>() {"Art�culos","Clientes","Consecutivos","Existencias","Par�metros Globales","Pendientes Cobro","Precios","Paquetes y Reglas","Usuarios y Privilegios" };
            adapter = new SimpleListAdapter(myActivity, lista);
            list.Adapter = adapter;
            list.ItemClick += async (s, ev) =>
            {
                ((Button)sender).Enabled = false;
                list.Enabled = false;
                List<string> creates = new List<string>();
                List<string> inserts = new List<string>();
                switch (ev.Position)
                {
                    case 0:{
                        creates = new List<string>() { "ERPADMIN_CLASIFICACION_crear", "ERPADMIN_ARTICULO_crear", "ERPADMIN_BONIFICACION_NIVEL_crear", 
                            "ERPADMIN_ARTICULO_EXISTENCIA_LOTE_crear","ERPADMIN_ARTICULO_EXISTENCIA_crear","ERPADMIN_IMPUESTO_crear","ERPADMIN_ARTICULO_PRECIO_crear",
                        "ERPADMIN_NIVEL_LISTA_crear","ERPADMIN_BONIFICACION_CLIART_crear","ERPADMIN_DESCUENTO_CLAS_crear","ERPADMIN_DESCUENTO_NIVEL_crear",
                        "ERPADMIN_DESCUENTO_CLIART_crear","ERPADMIN_DES_BON_ESCALA_BONIFICACION_crear","ERPADMIN_DES_BON_PAQUETE_crear","ERPADMIN_DES_BON_REGLA_crear",
                        "ERPADMIN_DES_BON_PAQUETE_REGLA_crear"};
                        inserts = new List<string>() { "ERPADMIN_CLASIFICACION_llenar", "ERPADMIN_ARTICULO_llenar", "ERPADMIN_BONIFICACION_NIVEL_llenar", 
                            "ERPADMIN_ARTICULO_EXISTENCIA_LOTE_llenar","ERPADMIN_ARTICULO_EXISTENCIA_llenar","ERPADMIN_IMPUESTO_llenar","ERPADMIN_ARTICULO_PRECIO_llenar",
                        "ERPADMIN_NIVEL_LISTA_llenar","ERPADMIN_BONIFICACION_CLIART_llenar","ERPADMIN_DESCUENTO_CLAS_llenar","ERPADMIN_DESCUENTO_NIVEL_llenar",
                        "ERPADMIN_DESCUENTO_CLIART_llenar","ERPADMIN_DES_BON_ESCALA_BONIFICACION_llenar","ERPADMIN_DES_BON_PAQUETE_llenar","ERPADMIN_DES_BON_REGLA_llenar",
                        "ERPADMIN_DES_BON_PAQUETE_REGLA_llenar"};
                        break;
                    }
                    case 1:
                        {
                            creates = new List<string>() {"ERPADMIN_ALFAC_PAIS_crear", "ERPADMIN_IMPUESTO_crear", "ERPADMIN_CLIENTE_CIA_crear", "ERPADMIN_CLIENTE_crear", "ERPADMIN_ALFAC_DIR_EMB_CLT_crear",
                            "ERPADMIN_ALFAC_CND_PAG_crear","ERPADMIN_DIVISION_GEOGRAFICA1_crear","ERPADMIN_DIVISION_GEOGRAFICA2_crear","ERPADMIN_BONIFICACION_CLIART_crear","ERPADMIN_DESCUENTO_CLAS_crear","ERPADMIN_DESCUENTO_NIVEL_crear",
                        "ERPADMIN_DESCUENTO_CLIART_crear","ERPADMIN_DES_BON_ESCALA_BONIFICACION_crear","ERPADMIN_DES_BON_PAQUETE_crear","ERPADMIN_DES_BON_REGLA_crear",
                        "ERPADMIN_DES_BON_PAQUETE_REGLA_crear"};
                            inserts = new List<string>() {"ERPADMIN_ALFAC_PAIS_llenar", "ERPADMIN_IMPUESTO_llenar", "ERPADMIN_CLIENTE_CIA_llenar", "ERPADMIN_CLIENTE_llenar", "ERPADMIN_ALFAC_DIR_EMB_CLT_llenar",
                            "ERPADMIN_ALFAC_CND_PAG_llenar","ERPADMIN_DIVISION_GEOGRAFICA1_llenar","ERPADMIN_DIVISION_GEOGRAFICA2_llenar","ERPADMIN_BONIFICACION_CLIART_llenar","ERPADMIN_DESCUENTO_CLAS_llenar","ERPADMIN_DESCUENTO_NIVEL_llenar",
                        "ERPADMIN_DESCUENTO_CLIART_llenar","ERPADMIN_DES_BON_ESCALA_BONIFICACION_llenar","ERPADMIN_DES_BON_PAQUETE_llenar","ERPADMIN_DES_BON_REGLA_llenar",
                        "ERPADMIN_DES_BON_PAQUETE_REGLA_llenar"};
                            break;
                        }
                    case 2:
                        {
                            creates = new List<string>() { "ERPADMIN_ALSYS_PRM_crear" };
                            inserts = new List<string>() { "ERPADMIN_ALSYS_PRM_llenar" };
                            break;
                        }
                    case 3:
                        {
                            creates = new List<string>() { "ERPADMIN_ARTICULO_EXISTENCIA_LOTE_crear", "ERPADMIN_ARTICULO_EXISTENCIA_crear" };
                            inserts = new List<string>() { "ERPADMIN_ARTICULO_EXISTENCIA_LOTE_llenars", "ERPADMIN_ARTICULO_EXISTENCIA_llenar" };
                            break;
                        }
                    case 4:
                        {
                            creates = new List<string>() { "ERPADMIN_COMPANIA_crear", "ERPADMIN_GLOBALES_MODULO_crear", "ERPADMIN_GLOBALES_FR_crear", "ERPADMIN_ALSYS_PRM_crear",
                            "ERPADMIN_RUTA_CFG_crear","ERPADMIN_CONFIG_HH_crear"};
                            inserts = new List<string>() { "ERPADMIN_COMPANIA_llenar", "ERPADMIN_GLOBALES_MODULO_llenar", "ERPADMIN_GLOBALES_FR_llenar", "ERPADMIN_ALSYS_PRM_llenar",
                            "ERPADMIN_RUTA_CFG_llenar","ERPADMIN_CONFIG_HH_llenar"};
                            break;
                        }
                    case 5:
                        {
                            creates = new List<string>() { "ERPADMIN_ALCXC_PEN_COB_crear" };
                            inserts = new List<string>() { "ERPADMIN_ALCXC_PEN_COB_llenar" };
                            break;
                        }
                    case 6:
                        {
                            creates = new List<string>() { "ERPADMIN_CLASIFICACION_crear", "ERPADMIN_BONIFICACION_NIVEL_crear", "ERPADMIN_IMPUESTO_crear",
                            "ERPADMIN_ARTICULO_PRECIO_crear","ERPADMIN_NIVEL_LISTA_crear","ERPADMIN_DESCUENTO_NIVEL_crear","ERPADMIN_BONIFICACION_CLIART_crear",
                            "ERPADMIN_DESCUENTO_CLAS_crear"};
                            inserts = new List<string>() { "ERPADMIN_CLASIFICACION_llenar", "ERPADMIN_BONIFICACION_NIVEL_llenar", "ERPADMIN_IMPUESTO_llenar",
                            "ERPADMIN_ARTICULO_PRECIO_llenar","ERPADMIN_NIVEL_LISTA_llenar","ERPADMIN_DESCUENTO_NIVEL_llenar","ERPADMIN_BONIFICACION_CLIART_llenar",
                            "ERPADMIN_DESCUENTO_CLAS_llenar"};
                            break;
                        }
                    case 7:
                        {
                            creates = new List<string>() { "ERPADMIN_CLASIFICACION_crear", "ERPADMIN_BONIFICACION_NIVEL_crear", 
                         "ERPADMIN_BONIFICACION_CLIART_crear","ERPADMIN_DESCUENTO_CLAS_crear","ERPADMIN_DESCUENTO_NIVEL_crear",
                        "ERPADMIN_DESCUENTO_CLIART_crear","ERPADMIN_DES_BON_ESCALA_BONIFICACION_crear","ERPADMIN_DES_BON_PAQUETE_crear","ERPADMIN_DES_BON_REGLA_crear",
                        "ERPADMIN_DES_BON_PAQUETE_REGLA_crear"};
                            inserts = new List<string>() { "ERPADMIN_CLASIFICACION_llenar", "ERPADMIN_BONIFICACION_NIVEL_llenar", 
                         "ERPADMIN_BONIFICACION_CLIART_llenar","ERPADMIN_DESCUENTO_CLAS_llenar","ERPADMIN_DESCUENTO_NIVEL_llenar",
                        "ERPADMIN_DESCUENTO_CLIART_llenar","ERPADMIN_DES_BON_ESCALA_BONIFICACION_llenar","ERPADMIN_DES_BON_PAQUETE_llenar","ERPADMIN_DES_BON_REGLA_llenar",
                        "ERPADMIN_DES_BON_PAQUETE_REGLA_llenar"};
                            break;
                        }
                    case 8:
                        {
                            creates = new List<string>() { "SYSTEM_USUARIO_crear", "SYSTEM_PRIVILEGIO_EX_crear" };
                            inserts = new List<string>() { "SYSTEM_USUARIO_llenar", "SYSTEM_PRIVILEGIO_EX_llenar" };
                            break;
                        }
                    default:break;
                }
                string result = await ejecutarBD(creates,inserts);
                ((Button)sender).Enabled = true;
                list.Enabled = true;
                builder.Dismiss();

                if (result.Equals("Base de datos creada correctamente"))
                {

                    bool usuarioAutenticado = true;

                    // Se verifica si el usuario actual sigue estando en los datos actualizados.
                    if (!GestorSeguridad.AutenticarUsuario(BI.Android.GestorDatos.NombreUsuario, BI.Android.GestorDatos.ContrasenaUsuario))
                    {
                        result += ". Parece que su usuario no es v�lido seg�n los datos actualizados, por favor inicie sesi�n nuevamente.";

                        usuarioAutenticado = false;
                    }

                    if (!usuarioAutenticado)
                    {
                        // Limpiar la lista de compa��as gestionadas.
                        Compania.Gestionadas = new List<Compania>();

                        if (GlobalUI.ClienteActual != null)
                        {
                            // Limpia y reinicia los datos del pedido global.
                            PedidosCollection.FacturarPedido = false;
                            GestorPedido.PedidosCollection = new PedidosCollection();
                            Pedido.BorrarXMLPedido(GlobalUI.ClienteActual.Codigo);

                            // Limpia el carrito de compras.
                            GestorPedido.carrito = null;

                            // Limpia al cliente del pedido.
                            GlobalUI.ClienteActual = null;
                            GlobalUI.RutaActual = null;
                            GlobalUI.RutasActuales = null;
                        }

                        Android.App.AlertDialog.Builder alertb = new Android.App.AlertDialog.Builder(myActivity);
                        alertb.SetTitle("Proceso finalizado");
                        alertb.SetMessage(result);
                        alertb.SetNeutralButton("OK", delegate
                        {
                            GC.Collect(GC.MaxGeneration);

                            var intent = new Intent(this.Activity, typeof(LoginActiity));
                            intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
                            this.StartActivity(intent);
                        });
                        alertb.Show();
                    }
                    else
                    {
                        Toast.MakeText(Android.App.Application.Context, result, ToastLength.Long).Show();
                    }
                }
                else
                {
                    Toast.MakeText(Android.App.Application.Context, result, ToastLength.Long).Show();
                }
            };
            builder.Window.SetBackgroundDrawableResource(Resource.Color.semitransparent);
            builder.Show();
           
        }


        async void btnImagenes_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(App.Servidor))
            {
                Toast.MakeText(Android.App.Application.Context, "Configure el servidor..", ToastLength.Short).Show();
                return;
            }
            ((Button)sender).Enabled = false;
            string result = await ejecutarImagenes();
            ((Button)sender).Enabled = true;
            Toast.MakeText(Android.App.Application.Context, result, ToastLength.Long).Show();
        }
     
        private void CrearPreferencia(int opcion)
        {
            switch (opcion)
            {
                case 0: {
                    string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                    string file = System.IO.Path.Combine(folder, "CiaPreferences.txt");
                    using (System.IO.StreamWriter st = new System.IO.StreamWriter(file, false))
                    {
                        st.Write(App.CompaniaDefault.ToString());
                    };
                    break;
                }
                case 1:
                    {
                        string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                        string file = System.IO.Path.Combine(folder, "ConsecutivoPreferences.txt");
                        using (System.IO.StreamWriter st = new System.IO.StreamWriter(file, false))
                        {
                            st.Write(App.ConsecutivoPedido.ToString());
                        };
                        break;
                    }
                case 2:
                    {
                        string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                        string file = System.IO.Path.Combine(folder, "ServidorPreferences.txt");
                        using (System.IO.StreamWriter st = new System.IO.StreamWriter(file, false))
                        {
                            st.Write(App.Servidor.ToString());
                        };
                        break;
                    }
                default: break;
            }
            
        }

        private Task<bool> EliminarDatos() 
        {
            return Task.Run(() =>
            {
                var myActivity = (MainActivity)this.Activity;
                try
                {
                    if (System.IO.File.Exists(System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CiaPreferences.txt")))
                    {
                        System.IO.File.Delete(System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CiaPreferences.txt"));
                    }
                    if (System.IO.File.Exists(System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "ConsecutivoPreferences.txt")))
                    {
                        System.IO.File.Delete(System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "ConsecutivoPreferences.txt"));
                    }
                    if (System.IO.File.Exists(System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "PrintPaperPreferences.txt")))
                    {
                        System.IO.File.Delete(System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "PrintPaperPreferences.txt"));
                    }
                    if (System.IO.File.Exists(System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "PrintNamePreferences.txt")))
                    {
                        System.IO.File.Delete(System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "PrintNamePreferences.txt"));
                    }
                    if (System.IO.File.Exists(System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "PrintTypePreferences.txt")))
                    {
                        System.IO.File.Delete(System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "PrintTypePreferences.txt"));
                    }
                    if (System.IO.Directory.Exists(App.PathAplicacion))
                    {
                        System.IO.Directory.Delete(App.PathAplicacion, true);
                    }
                    myActivity.RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Datos eliminados.", ToastLength.Short); });
                    return true;
                }
                catch
                {
                    myActivity.RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error al borrar los datos de la aplicaci�n.",ToastLength.Short); });
                    return false;
                }
            });
        }

        #endregion
    }
}