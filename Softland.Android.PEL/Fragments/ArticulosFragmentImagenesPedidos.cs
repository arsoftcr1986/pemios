using System;
using System.Collections.Generic;
using Android.Runtime;
using Android.Content;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Pedidos.Core;
using Android.Support.V4.View;
using BI.Shared;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace Softland.Droid.PEL
{
    public class ArticulosFragmentImagenesPedidos : Fragment
    {
        Android.Support.V7.Widget.SearchView mSearchview;
        IMenuItem item;
        GridView list;
        TextView txtTotal;


        public ArticulosFragmentImagenesPedidos()
        {
            this.RetainInstance = true;
        }

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            this.HasOptionsMenu = true;            
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.ArticulosGridFragment, null);
            list = view.FindViewById<GridView>(Resource.Id.list);
            var myActivity = (ArticulosPedidoActivity)this.Activity;
            myActivity.SupportActionBar.Title = myActivity.Title = "Art�culos";    
            // Create your application here   
            list.ItemClick += list_ItemClick;
            list.ItemLongClick += list_ItemLongClick;
            InicializarLista();
            //InicializarLista();                        
            return view;
        }

        void list_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            try
            {
                var myActivity = (ArticulosPedidoActivity)this.Activity;
                if (myActivity.adapterGrid.Seleccionado(e.Position))
                {
                    myActivity.removidos.Add(myActivity.adapterGrid.GetCodigo(e.Position));
                    myActivity.adapterGrid.Seleccionar(e.Position);
                    myActivity.adapter.Seleccionar(e.Position);                    
                }
                else
                {
                    //
                    Android.App.Dialog builder = new Android.App.Dialog(myActivity);
                    builder.Window.RequestFeature(WindowFeatures.NoTitle);
                    builder.SetContentView(Resource.Layout.dialog_cantidad);
                    NumberPicker picker = builder.FindViewById<NumberPicker>(Resource.Id.pickerCantidad);
                    Button btnAceptar = builder.FindViewById<Button>(Resource.Id.btnAceptar);
                    Button btnCancelar = builder.FindViewById<Button>(Resource.Id.btnCancelar);
                    btnAceptar.Text = "Confirmar";
                    picker.MinValue = 1;
                    picker.MaxValue = 9999;
                    picker.Value = Convert.ToInt32(myActivity.adapterGrid.getCantidadAlmacen(e.Position));
                    btnAceptar.Click += (s, events) =>
                    {
                        //
                        if (myActivity.removidos.Exists(x => x.Equals(myActivity.adapterGrid.GetCodigo(e.Position))))
                        {
                            myActivity.removidos.Remove(myActivity.adapterGrid.GetCodigo(e.Position));
                        }

                        // Permite que el valor se asigne adecuadamente en el picker si fue digitado sin presionar enter.
                        picker.ClearFocus();

                        myActivity.adapter.SetCantidadAlmacen(e.Position, picker.Value);
                        myActivity.adapterGrid.SetCantidadAlmacen(e.Position, picker.Value);
                        myActivity.adapterGrid.Seleccionar(e.Position);
                        myActivity.adapter.Seleccionar(e.Position);                        
                        builder.Dismiss();

                    };
                    btnCancelar.Click += (s, events) =>
                    {
                        builder.Dismiss();
                    };
                    if (Android.OS.Build.VERSION.SdkInt.Equals(Android.OS.BuildVersionCodes.Lollipop))
                        builder.Window.SetBackgroundDrawable(myActivity.GetDrawable(Android.Resource.Drawable.DialogHoloLightFrame));
                    else
                        builder.Window.SetBackgroundDrawableResource(Android.Resource.Drawable.DialogHoloLightFrame);
                    builder.Show();

                }
            }
            catch
            {
            }
            //
            //try
            //{
            //    var myActivity = (ArticulosPedidoActivity)this.Activity;
            //    if (myActivity.adapterGrid.Seleccionado(e.Position))
            //    {
            //        myActivity.removidos.Add(myActivity.adapterGrid.GetCodigo(e.Position));
            //    }
            //    else
            //    {
            //        if (myActivity.removidos.Exists(x => x.Equals(myActivity.adapterGrid.GetCodigo(e.Position))))
            //        {
            //            myActivity.removidos.Remove(myActivity.adapterGrid.GetCodigo(e.Position));
            //        }
            //    }
            //    myActivity.adapterGrid.Seleccionar(e.Position);
            //    myActivity.adapter.Seleccionar(e.Position);
            //}
            //catch
            //{ 
            //}            
        }

        void list_ItemLongClick(object sender, AdapterView.ItemLongClickEventArgs e)
        {
            try
            {
                var myActivity = (ArticulosPedidoActivity)this.Activity;
                //
                Android.App.Dialog builder = new Android.App.Dialog(myActivity);
                builder.Window.RequestFeature(WindowFeatures.NoTitle);
                builder.SetContentView(Resource.Layout.dialog_consul_art);
                Button btnConsultar = builder.FindViewById<Button>(Resource.Id.btnConsultar);
                Button btnEliminar = builder.FindViewById<Button>(Resource.Id.btnEliminar);
                btnConsultar.Click += (senderbtn, ebtn) =>
                {
                    builder.Dismiss();
                    list.Enabled = false;
                    Intent intent = new Intent(myActivity, typeof(ConsulArticuloActivity));
                    intent.PutExtra("codigo", myActivity.adapterGrid.GetCodigo(e.Position));
                    intent.PutExtra("enpedido", true);
                    StartActivity(intent);
                };
                builder.Window.SetBackgroundDrawableResource(Resource.Color.semitransparent);
                builder.Show();  
            }
            catch
            { 
            }            
        }

        public override void OnDetach()
        {
            list.ItemClick -= list_ItemClick;
            list.ItemLongClick -= list_ItemLongClick;
            base.OnDetach();
        }

        private async void CargaInicial()
        {
            bool result = await ExecuteConsultar();
        }

        public override void OnResume()
        {
            list.Enabled = true;
            base.OnResume();
            
        }


        #region metodos

        private async void InicializarLista()
        {
            bool res = await ExecuteConsultar();
        }

        private Task<bool> ExecuteConsultar()
        {

            return Task.Run(() =>
            {
                Android.App.ProgressDialog progressDialog = null;
                try
                {
                    var myActivity = (ArticulosPedidoActivity)this.Activity;
                    myActivity.RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(myActivity, "Cargando Im�genes", "Por favor espere...", true); });
                    List<Articulo> lista = new List<Articulo>();
                    Connection cnxArticulo = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                    if (GlobalUI.ClienteActual.NivelesPreciosDefinidos)
                    {
                        lista = Articulo.ObtenerArticulos(new CriterioBusquedaArticulo(CriterioArticulo.Ninguno, "", false), App.CompaniaDefault, App.DeviceID, string.Empty, true, cnxArticulo);
                    }
                    else
                    {
                        lista = Articulo.ObtenerArticulos(new CriterioBusquedaArticulo(CriterioArticulo.Ninguno, "", false), App.CompaniaDefault, App.DeviceID, string.Empty, false, cnxArticulo);
                    }
                    cnxArticulo.Close();
                    cnxArticulo = null;
                    myActivity.RunOnUiThread(() => { myActivity.adapterGrid = new ArticulosGridAdapter(myActivity, lista, false); list.Adapter = myActivity.adapterGrid; });
                    myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                    return true;
                }
                catch
                {
                    var myActivity = (ArticulosPedidoActivity)this.Activity;
                    myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                    myActivity.RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error inicializando lista.", ToastLength.Short).Show(); });
                    return false;
                }
            });
        }
        
        #endregion

        
    }
}