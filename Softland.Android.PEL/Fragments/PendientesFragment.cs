using System;
using System.Collections.Generic;
using Android.Runtime;
using Android.Content;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Pedidos.Core;
using Android.Support.V4.View;
using BI.Shared;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace Softland.Droid.PEL
{
    public class PendientesFragment : Fragment
    {
        Android.Support.V7.Widget.SearchView mSearchview;
        ListView list;
        PendientesAdapter adapter;
        bool limpiarMemoria=false;
        IMenuItem itemborrar;


        public PendientesFragment()
        {
            this.RetainInstance = true;
        }

        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Android.OS.Bundle savedInstanceState)
        {
            this.HasOptionsMenu = true;            
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.PendientesFragment, null);
            list = view.FindViewById<ListView>(Resource.Id.list);
            var myActivity = (MainActivity)this.Activity;
            myActivity.SupportActionBar.Title = myActivity.Title = "No Sincronizados";
            list.ItemClick += list_ItemClick;           
            InicializarLista();                        
            return view;
        }

        void list_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            list.Enabled = false;
            adapter.Seleccionar(e.Position);
            if (adapter.getSeleccionadosCount() <= 0)
            {
                itemborrar.SetVisible(false);
                itemborrar.SetEnabled(false);
            }
            else
            {
                itemborrar.SetVisible(true);
                itemborrar.SetEnabled(true);
            }
            list.Enabled = true;
        }

        public async void InicializarLista()
        {
            adapter = null;
            bool res = (await ExecuteConsultar());
        }


        private Task<bool> ExecuteConsultar()
        {
            return Task.Run(() =>
            {                
                Android.App.ProgressDialog progressDialog = null;
                var myActivity = (MainActivity)this.Activity;
                try
                {
                    myActivity.RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(myActivity, "Cargando", "Por favor espere...", true); });
                    Connection cnxPedidos = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                    List<Pedido> pedidos = Pedido.ObtenerPedidos(cnxPedidos);
                    foreach (Pedido ped in pedidos)
                    {
                        Cliente cliente = new Cliente();
                        cliente.Codigo = ped.Cliente;
                        cliente.Compania = ped.Compania;
                        cliente.Cargar(cnxPedidos);
                        cliente.ObtenerClientesCia(cnxPedidos);
                        //ped.Configuracion.ClienteCia = cliente.ObtenerClienteCia(ped.Compania);
                        //ped.Configuracion.Compania = new Compania(ped.Compania);
                        //ped.Configuracion.Cargar(cnxPedidos);
                        //ped.Configuracion.ClienteCia.ObtenerDireccionesEntrega(cnxPedidos);

                        ped.Configuracion.ClienteCia = cliente.ObtenerClienteCia(ped.Compania);
                        ped.Configuracion.Cargar(cnxPedidos);
                        ped.Configuracion.ClienteCia.ObtenerDireccionesEntrega(cnxPedidos);
                    }
                    myActivity.RunOnUiThread(() =>
                    {
                        adapter = new PendientesAdapter(myActivity, pedidos);
                        list.Adapter = adapter;
                    });
                    cnxPedidos.Close();
                    cnxPedidos = null;                                        
                    myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                    return true;
                }
                catch (Exception ex)
                {
                    myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                    myActivity.RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "No se han podido cargar los pedidos pendientes.", ToastLength.Short).Show(); });
                    return false;
                }
                
            });
        }

        private Task<bool> ExecuteEliminar()
        {
            return Task.Run(() =>
            {
                int cantidad = 0;
                Android.App.ProgressDialog progressDialog = null;
                var myActivity = (MainActivity)this.Activity;
                try
                {
                    myActivity.RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(myActivity, "Eliminando", "Por favor espere...", true); });
                    Connection cnxPedidos = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                    List<Pedido> pedidos = adapter.getSeleccionados();
                    cantidad = pedidos.Count;
                    foreach (Pedido ped in pedidos)
                    {
                        ped.DBEliminar(cnxPedidos);
                    }
                    cnxPedidos.Close();
                    cnxPedidos = null;
                    myActivity.RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context,
                        (cantidad > 1 ? "Pedidos eliminados correctamente." : "Pedido eliminado correctamente.")
                        , ToastLength.Short).Show(); progressDialog.Dismiss(); });
                    return true;
                }
                catch
                {
                    myActivity.RunOnUiThread(() => { progressDialog.Dismiss(); });
                    myActivity.RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "No se ha podido completar la eliminaci�n " + 
                        (cantidad > 1 ? "de los pedidos seleccionados." : "del pedido seleccionado."), ToastLength.Short).Show(); });
                    return false;
                }

            });
        }

        private async void delete()
        {
            if (await ExecuteEliminar())
            {
                InicializarLista();
            }
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.search_pendientes, menu);
            var item = menu.FindItem(Resource.Id.action_search);
            itemborrar = menu.FindItem(Resource.Id.menu_delete);
            itemborrar.SetVisible(false);
            itemborrar.SetEnabled(false);
            var searchView = MenuItemCompat.GetActionView(item);
            mSearchview = searchView.JavaCast<Android.Support.V7.Widget.SearchView>();
            try
            {
                mSearchview.QueryTextSubmit += (s, e) =>
                {
                    if (adapter == null)
                        return;
                    adapter.Filter.InvokeFilter(e.Query);
                };
                mSearchview.QueryTextChange += (s, e) =>
                {
                    if (adapter == null)
                        return;
                    if (string.IsNullOrEmpty(e.NewText))
                    {
                        adapter.ResetSearch();
                    }
                    else
                    {
                        adapter.Filter.InvokeFilter(e.NewText);
                    }
                };
            }
            catch
            { 
            }
            
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.menu_logoff:
                    {
                        if (GestorPedido.PedidosCollection != null)
                        {
                            GestorPedido.PedidosCollection.Gestionados.Clear();
                            GC.Collect(GC.MaxGeneration);
                        }
                        //if (SyncService.isRunnig)
                        //{
                        //    this.Activity.StopService(new Intent(this.Activity, typeof(SyncService)));
                        //    SyncService.isRunnig = false;
                        //} 
                        var intent = new Intent(this.Activity, typeof(LoginActiity));
                        intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
                        this.StartActivity(intent);
                        break;
                    }
                case Resource.Id.menu_delete:
                    {
                        delete();
                        break;
                    }
                case Resource.Id.menu_filtro:
                    {
                        var myActivity = (MainActivity)this.Activity;
                        Android.App.Dialog builder = new Android.App.Dialog(myActivity);
                        builder.Window.RequestFeature(WindowFeatures.NoTitle);
                        builder.SetContentView(Resource.Layout.dialog_filtroPed);
                        RadioButton rbCodigo = builder.FindViewById<RadioButton>(Resource.Id.rbConsecutivo);
                        RadioButton rbCliente = builder.FindViewById<RadioButton>(Resource.Id.rbCliente);
                        RadioButton rbCompania = builder.FindViewById<RadioButton>(Resource.Id.rbCompania);
                        RadioGroup rgFiltros = builder.FindViewById<RadioGroup>(Resource.Id.rgFiltros);
                        rbCodigo.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rbCliente.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rbCompania.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rgFiltros.CheckedChange += (sender, e) =>
                        {
                            if (rbCodigo.Checked)
                            {
                                App.FiltroCliente = App.CodigoFilter;
                                Toast.MakeText(Android.App.Application.Context, "Filtrando pedidos por c�digo.", ToastLength.Short).Show();
                                builder.Dismiss();
                            }
                            else if (rbCliente.Checked)
                            {
                                App.FiltroCliente = App.ClienteFilter;
                                Toast.MakeText(Android.App.Application.Context, "Filtrando pedidos por cliente.", ToastLength.Short).Show();
                                builder.Dismiss();
                            }
                            else
                            {
                                App.FiltroCliente = App.CompaniaFilter;
                                Toast.MakeText(Android.App.Application.Context, "Filtrando pedidos por compa��a.", ToastLength.Short).Show();
                                builder.Dismiss();
                            }

                        };
                        if (string.IsNullOrEmpty(App.FiltroCliente))
                        {
                            App.FiltroCliente = App.CodigoFilter;
                            rbCodigo.Checked = true;
                        }
                        else
                        {
                            if (App.FiltroCliente.Equals(App.CodigoFilter))
                            {
                                rbCodigo.Checked = true;
                                App.FiltroCliente = App.CodigoFilter;
                            }
                            else if (App.FiltroCliente.Equals(App.ClienteFilter))
                                rbCliente.Checked = true;
                            else
                                rbCompania.Checked = true;
                        }
                        builder.Window.SetBackgroundDrawableResource(Resource.Color.semitransparent);
                        builder.Show(); break;
                    }
                default: break;
            }
            return base.OnOptionsItemSelected(item);
        }


        public override void OnDetach()
        {
            list.ItemClick -= list_ItemClick;
            base.OnDetach();
        }
    }
}