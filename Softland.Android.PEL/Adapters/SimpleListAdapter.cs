using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pedidos.Core;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BI.Shared;

namespace Softland.Droid.PEL
{
    public class SimpleListAdapter : BaseAdapter
    {
        List<String> _List;
        Activity _activity;

        public SimpleListAdapter(Activity activity, List<String> p_List)
        {
            _activity = activity;
            this._List = p_List;
        }

        public override int Count
        {
            get { return _List.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return ObjectExtension.ToJavaObject<String>(this._List[position]);
        }        

        public override long GetItemId(int position)
        {
            return 0;
        }

        public bool listaInicializada ()
        {
            return _List != null && _List.Count > 0;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = null;
            try
            {   
                view = convertView ?? _activity.LayoutInflater.Inflate(
                Resource.Layout.simple_row, parent, false);
                var txtIText1 = view.FindViewById<TextView>(Resource.Id.text1);
                txtIText1.Text = _List[position].ToString();

                txtIText1.SetTextColor(Android.Graphics.Color.WhiteSmoke);

                return view;
            }
            catch (Exception ex)
            {
                //throw ex;
                view = convertView ?? _activity.LayoutInflater.Inflate(
                   Resource.Layout.simple_row, parent, false);
                return view;
            }
            
        }
        
    }
}