using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pedidos.Core;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BI.Shared;

namespace Softland.Droid.PEL
{
    public class DocumentosAdapter : BaseAdapter
    {
        List<DocumentoContable> _List;
        Activity _activity;
        bool dolar;

        public DocumentosAdapter(Activity activity, List<DocumentoContable> p_List, bool pdolar)
        {
            _activity = activity;
            dolar = pdolar;
            this._List = p_List;            
        }

        public override int Count
        {
            get { return _List.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return ObjectExtension.ToJavaObject<DocumentoContable>(this._List[position]);
        }        

        public override long GetItemId(int position)
        {
            return 0;
        }

        public bool listaInicializada ()
        {
            return _List != null && _List.Count > 0;
        }

        public DocumentoContable GetItemArticulo(int position)
        {
            return this._List[position];
        }

        public string GetItemIdCliente(int position)
        {
            return this._List[position].Numero;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = null;
            try
            {   
                view = convertView ?? _activity.LayoutInflater.Inflate(
                Resource.Layout.documentos_rows, parent, false);
                var txtICodigo = view.FindViewById<TextView>(Resource.Id.txtCodigo);
                var txtIFecha = view.FindViewById<TextView>(Resource.Id.txtFechaVence);
                var txtISaldo = view.FindViewById<TextView>(Resource.Id.txtSaldo);
                var txtIMonto = view.FindViewById<TextView>(Resource.Id.txtMonto);
                txtICodigo.Text = _List[position].Numero;
                txtIFecha.Text = GestorUtilitario.ObtenerFechaString(_List[position].FechaVencimiento);
                if (!dolar)
                {
                    txtISaldo.Text = GestorUtilitario.FormatNumero(_List[position].SaldoDocLocal);
                    txtIMonto.Text = GestorUtilitario.FormatNumero(_List[position].MontoDocLocal);
                }
                else
                {
                    txtISaldo.Text = GestorUtilitario.FormatNumeroDolar(_List[position].SaldoDocDolar);
                    txtIMonto.Text = GestorUtilitario.FormatNumeroDolar(_List[position].MontoDocDolar);
                }
                return view;
            }
            catch (Exception ex)
            {
                //throw ex;
                view = convertView ?? _activity.LayoutInflater.Inflate(
                   Resource.Layout.documentos_rows, parent, false);
                return view;
            }
            
        }
        
    }
}