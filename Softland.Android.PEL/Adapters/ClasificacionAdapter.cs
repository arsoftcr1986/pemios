using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pedidos.Core;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BI.Shared;

namespace Softland.Droid.PEL
{
    public class ClasificacionAdapter : BaseAdapter
    {
        List<Clasificacion> _List;
        Activity _activity;

        public ClasificacionAdapter(Activity activity, List<Clasificacion> p_List)
        {
            _activity = activity;
            this._List = p_List;            
        }

        public override int Count
        {
            get { return _List.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return ObjectExtension.ToJavaObject<Clasificacion>(this._List[position]);
        }

        public  string GetDescripcion(int position)
        {
            return _List[position].Descripcion;
        }

        public string GetCodigo(int position)
        {
            return _List[position].Codigo;
        }
       


        public override long GetItemId(int position)
        {
            return 0;
        }

        public bool listaInicializada ()
        {
            return _List != null && _List.Count > 0;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = null;
            try
            {   
                view = convertView ?? _activity.LayoutInflater.Inflate(
                Resource.Layout.simple_row, parent, false);
                var txtIDescripcion = view.FindViewById<TextView>(Resource.Id.text1);                
                txtIDescripcion.Text = _List[position].Descripcion;                
                txtIDescripcion.SetTextColor(Android.Graphics.Color.DarkGray);
                return view;
            }
            catch (Exception ex)
            {
                //throw ex;
                view = convertView ?? _activity.LayoutInflater.Inflate(
                   Resource.Layout.simple_row, parent, false);
                return view;
            }
            
        }
        
    }
}