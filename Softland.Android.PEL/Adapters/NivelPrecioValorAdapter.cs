using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pedidos.Core;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BI.Shared;

namespace Softland.Droid.PEL
{
    public class NivelPrecioValorAdapter : BaseAdapter
    {
        List<NivelPrecioValor> _List;
        Activity _activity;

        public NivelPrecioValorAdapter(Activity activity, List<NivelPrecioValor> p_List)
        {
            _activity = activity;
            this._List = p_List;
        }

        public override int Count
        {
            get { return _List.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return ObjectExtension.ToJavaObject<NivelPrecioValor>(this._List[position]);
        }        

        public override long GetItemId(int position)
        {
            return 0;
        }

        public bool listaInicializada ()
        {
            return _List != null && _List.Count > 0;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = null;
            try
            {   
                view = convertView ?? _activity.LayoutInflater.Inflate(
                Resource.Layout.precios_rows, parent, false);
                var txtICodigo = view.FindViewById<TextView>(Resource.Id.txtNivel);
                var txtIPrecio = view.FindViewById<TextView>(Resource.Id.txtPrecio);
                txtICodigo.Text = _List[position].Codigo;
                if (_List[position].Local)
                {
                    txtIPrecio.Text = "Alm:" + GestorUtilitario.FormatNumero(_List[position].Maximo) + " | Det:" + GestorUtilitario.FormatNumero(_List[position].Minimo);
                }
                else
                {
                    txtIPrecio.Text = "Alm:" + GestorUtilitario.FormatNumeroDolar(_List[position].Maximo) + " | Det:" + GestorUtilitario.FormatNumeroDolar(_List[position].Minimo);
                }
                txtICodigo.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                txtIPrecio.SetTextColor(Android.Graphics.Color.WhiteSmoke);

                return view;
            }
            catch (Exception ex)
            {
                //throw ex;
                view = convertView ?? _activity.LayoutInflater.Inflate(
                   Resource.Layout.precios_rows, parent, false);
                return view;
            }
            
        }
        
    }
}