using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pedidos.Core;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BI.Shared;

namespace Softland.Droid.PEL
{
    public class DetallePedidoAdapter : BaseAdapter, IFilterable
    {
        Filter filter;
        List<DetallePedido> _articulosList;
        List<DetallePedido> _articulosListMatch;
        bool dolar = false;
        Activity _activity;
        bool consulta;

        public DetallePedidoAdapter(Activity activity, List<DetallePedido> p_articulosList, bool pConsulta)
        {
            _activity = activity;
            filter = new SuggestionsFilterDetPed(this);
            this._articulosList = p_articulosList;
            this._articulosListMatch = p_articulosList;
            this.consulta = pConsulta;
            if (GlobalUI.ClienteActual != null)
            {
                GlobalUI.ClienteActual.ObtenerClientesCia();
                dolar = GlobalUI.ClienteActual.ObtenerClienteCia(App.CompaniaDefault).Moneda == TipoMoneda.DOLAR;
            }
        }

        public override int Count
        {
            get { return _articulosListMatch.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return ObjectExtension.ToJavaObject<DetallePedido>(this._articulosListMatch[position]);
        }        

        public override long GetItemId(int position)
        {
            return 0;
        }

        public DetallePedido GetItemArticulo(int position)
        {
            return this._articulosListMatch[position];
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView ?? _activity.LayoutInflater.Inflate(
                Resource.Layout.detallepedido_rows, parent, false);
            try
            {
                var txtICodigo = view.FindViewById<TextView>(Resource.Id.txtCodigo);
                var txtIDesc = view.FindViewById<TextView>(Resource.Id.txtDescripcion);
                var txtIBonificada = view.FindViewById<TextView>(Resource.Id.txtBonificada);
                var txtIDescuento = view.FindViewById<TextView>(Resource.Id.txtDescuento);
                var txtICantidad = view.FindViewById<TextView>(Resource.Id.txtCantidad);
                var txtISubTotal = view.FindViewById<TextView>(Resource.Id.txtSubTotal);
                var txtIImpuesto = view.FindViewById<TextView>(Resource.Id.txtImpuesto);
                var txtITotal = view.FindViewById<TextView>(Resource.Id.txtTotal);
                //Jarbis Ajustes IVA >>>>>
                var txtTipoImpuesto1 = view.FindViewById<TextView>(Resource.Id.txtTipoImpuesto1);
                var txtTipoTarifa1 = view.FindViewById<TextView>(Resource.Id.txtTipoTarifa1);
                var txtPorcImpuesto1 = view.FindViewById<TextView>(Resource.Id.txtPorcImpuesto1);
                var txtTipoImpuesto2 = view.FindViewById<TextView>(Resource.Id.txtTipoImpuesto2);
                var txtTipoTarifa2 = view.FindViewById<TextView>(Resource.Id.txtTipoTarifa2);
                var txtPorcImpuesto2 = view.FindViewById<TextView>(Resource.Id.txtPorcImpuesto2);
                //Jarbis Ajustes IVA <<<<<


                txtICodigo.Text = _articulosListMatch[position].Articulo.Codigo;
                txtIDesc.Text = _articulosListMatch[position].Articulo.Descripcion;
                txtICantidad.Text = GestorUtilitario.FormatDecimalAlmDet(_articulosListMatch[position].TotalAlmacen, 3);
                if (_articulosListMatch[position].LineaBonificada != null)
                {
                    txtIBonificada.Text = "Si tiene";
                }
                else
                {
                    txtIBonificada.Text = "No tiene";
                }
                if (dolar)
                {
                    if (_articulosListMatch[position].Descuento == null)
                    {
                        txtIDescuento.Text = "No tiene";
                    }
                    else
                    {
                        if (_articulosListMatch[position].Descuento.Tipo.Equals(TipoDescuento.Porcentual))
                            txtIDescuento.Text = GestorUtilitario.FormatPorcentaje(_articulosListMatch[position].Descuento.Monto);
                        else
                            txtIDescuento.Text = GestorUtilitario.FormatNumeroDolar(_articulosListMatch[position].Descuento.Monto);
                    }
                    txtIImpuesto.Text = GestorUtilitario.FormatNumeroDolar(_articulosListMatch[position].Impuesto.MontoTotal);
                    txtISubTotal.Text = GestorUtilitario.FormatNumeroDolar(_articulosListMatch[position].MontoTotal);
                    txtITotal.Text = GestorUtilitario.FormatNumeroDolar(_articulosListMatch[position].MontoNeto);

                }
                else
                {
                    if (_articulosListMatch[position].Descuento == null)
                    {
                        txtIDescuento.Text = "No tiene";
                    }
                    else
                    {
                        if (_articulosListMatch[position].Descuento.Tipo.Equals(TipoDescuento.Porcentual))
                            txtIDescuento.Text = GestorUtilitario.FormatPorcentaje(_articulosListMatch[position].Descuento.Monto);
                        else
                            txtIDescuento.Text = GestorUtilitario.FormatNumero(_articulosListMatch[position].Descuento.Monto);
                    }
                    //Jarbis Ajustes IVA >>>>>
                    txtTipoImpuesto1.Text = _articulosListMatch[position].Articulo.Impuesto.Desc_tipo_Impuesto1;
                    txtTipoTarifa1.Text = _articulosListMatch[position].Articulo.Impuesto.Desc_tipo_tarifa1;
                    txtPorcImpuesto1.Text = GestorUtilitario.FormatPorcentaje(_articulosListMatch[position].Articulo.Impuesto.Impuesto1);
                    txtTipoImpuesto2.Text = _articulosListMatch[position].Articulo.Impuesto.Desc_tipo_Impuesto2;
                    txtTipoTarifa2.Text = _articulosListMatch[position].Articulo.Impuesto.Desc_tipo_tarifa2;
                    txtPorcImpuesto2.Text = GestorUtilitario.FormatPorcentaje(_articulosListMatch[position].Articulo.Impuesto.Impuesto2);
                    //Jarbis Ajustes IVA <<<<<

                    txtIImpuesto.Text = GestorUtilitario.FormatNumero(_articulosListMatch[position].Impuesto.MontoTotal);
                    txtISubTotal.Text = GestorUtilitario.FormatNumero(_articulosListMatch[position].MontoTotal);
                    txtITotal.Text = GestorUtilitario.FormatNumero(_articulosListMatch[position].MontoNeto);
                }
            }
            catch
            { 
            }
            
            return view;            
        }

        public void CambiarCantidad(int position,int cantidad) 
        {
            Toast.MakeText(_activity, "Nueva cantidad art�culo " + _articulosListMatch[position].Articulo.Codigo + "   " + cantidad, ToastLength.Long).Show();
        }

        public decimal getCantidadAlmacen(int position) 
        {
            return _articulosListMatch[position].UnidadesAlmacen;
        }

        public decimal getCantidadDetalle(int position)
        {
            return _articulosListMatch[position].UnidadesDetalle;
        }

        public Articulo getArticuloLinea(int position)
        {
            return _articulosListMatch[position].Articulo;
        }

        public Filter Filter
        {
            get { return filter; }
        }

        public void ResetSearch()
        {
            _articulosListMatch = _articulosList;
            NotifyDataSetChanged();
        }

        public string GetCodigo(int position)
        {
            return _articulosListMatch[position].Articulo.Codigo;
        }

        public bool Bonificado(int position)
        {
            // Referencia al detalle del art�culo en el pedido.
            DetallePedido DetalleSeleccionado = GestorPedido.PedidosCollection.BuscarDetalle(_articulosListMatch[position].Articulo);

            // Si el detalle tiene una linea bonificada.
            if (DetalleSeleccionado.LineaBonificada != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void MostarDatosBonificacion(int position)
        {
            string mostrar;
            string cantidad;

            // Referencia al detalle del art�culo en el pedido.
            DetallePedido DetalleSeleccionado = GestorPedido.PedidosCollection.BuscarDetalle(_articulosListMatch[position].Articulo);

            // Si el detalle tiene una linea bonificada, se arma un mensaje con la informaci�n del articulo bonificado
            // para mostrarlo al usuario.
            if (DetalleSeleccionado.LineaBonificada != null)
            {
                // Se obtiene el art�culo bonificado.
                Articulo articuloBonificado = DetalleSeleccionado.LineaBonificada.Articulo;

                mostrar = articuloBonificado.Codigo + " - " + articuloBonificado.Descripcion;
                if (mostrar.Length > 34)
                    mostrar = mostrar.Substring(0, 34);
                mostrar = mostrar + " - ";
                cantidad = " Cant. Bonifica: " + articuloBonificado.TipoEmpaqueAlmacen + ": " + GestorUtilitario.Formato(DetalleSeleccionado.LineaBonificada.UnidadesAlmacen) + " | " +
                           articuloBonificado.TipoEmpaqueDetalle + ": " + GestorUtilitario.Formato(DetalleSeleccionado.LineaBonificada.UnidadesDetalle);
                mostrar = mostrar + cantidad;

                Toast.MakeText(Android.App.Application.Context, mostrar, ToastLength.Long).Show();
            }
            else
            {
                Toast.MakeText(Android.App.Application.Context, "No hay art�culo bonificado.", ToastLength.Long).Show();
            }
        }

        class SuggestionsFilterDetPed : Filter
        {
            readonly DetallePedidoAdapter _adapter;

            public SuggestionsFilterDetPed(DetallePedidoAdapter adapter)
                : base()
            {
                _adapter = adapter;
            }

            protected override Filter.FilterResults PerformFiltering(Java.Lang.ICharSequence constraint)
            {
                FilterResults results = new FilterResults();
                if (constraint != null && (!String.IsNullOrEmpty(constraint.ToString()) || App.FiltroArticulo.Equals(App.SeleccionadosFilter)))
                {
                    var searchFor = constraint.ToString().ToUpper();
                    var matchList = new List<DetallePedido>();
                    if (App.FiltroArticulo.Equals(App.CodigoFilter))
                    {
                        var matches =
                        from i in _adapter._articulosList
                        where i.Articulo.Codigo.IndexOf(searchFor, StringComparison.InvariantCultureIgnoreCase) >= 0 && !i.EsBonificada
                        select i;
                        matchList = matches.ToList();
                    }
                    else if (App.FiltroArticulo.Equals(App.DescripcionFilter))
                    {
                        var matches =
                        from i in _adapter._articulosList
                        where i.Articulo.Descripcion.IndexOf(searchFor, StringComparison.InvariantCultureIgnoreCase) >= 0 && !i.EsBonificada
                        select i;
                        matchList = matches.ToList();
                    }
                    else if (App.FiltroArticulo.Equals(App.BarrasFilter))
                    {
                        var matches =
                        from i in _adapter._articulosList
                        where i.Articulo.CodigoBarras.IndexOf(searchFor, StringComparison.InvariantCultureIgnoreCase) >= 0 && !i.EsBonificada
                        select i;
                        matchList = matches.ToList();
                    }
                    else
                    {
                        string[] arrayStr = searchFor.Split(Convert.ToChar(","));
                        switch (arrayStr.Length)
                        {
                            case 1:
                                {
                                    var matches =
                                    from i in _adapter._articulosList
                                    where i.Articulo.Familia.IndexOf(arrayStr[0], StringComparison.InvariantCultureIgnoreCase) >= 0
                                    select i;
                                    matchList = matches.ToList();
                                    break;
                                }
                            case 2:
                                {
                                    var matches =
                                    from i in _adapter._articulosList
                                    where i.Articulo.Familia.IndexOf(arrayStr[0], StringComparison.InvariantCultureIgnoreCase) >= 0
                                    && i.Articulo.Clase.IndexOf(arrayStr[1], StringComparison.InvariantCultureIgnoreCase) >= 0
                                    select i;
                                    matchList = matches.ToList();
                                    break;
                                }
                            default: break;
                        }  
                    }
                    _adapter._articulosListMatch = matchList.ToList();
                    Java.Lang.Object[] matchObjects;
                    matchObjects = new Java.Lang.Object[matchList.Count];
                    for (int i = 0; i < matchList.Count; i++)
                    {
                        matchObjects[i] = (matchList[i]).ToJavaObject();
                    }
                    results.Values = matchObjects;
                    results.Count = matchList.Count;
                }
                else
                {
                    _adapter.ResetSearch();
                }
                return results;
            }

            protected override void PublishResults(Java.Lang.ICharSequence constraint, Filter.FilterResults results)
            {
                _adapter.NotifyDataSetChanged();
            }
        }
    }
}