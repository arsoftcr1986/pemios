using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pedidos.Core;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BI.Shared;

namespace Softland.Droid.PEL
{
    public class ArticulosAdapter : BaseAdapter, IFilterable
    {
        SuggestionsFilterArt filter;
        List<Articulo> _articulosList;
        List<Articulo> _articulosListMatch;
        Activity _activity;
        bool consulta;
        bool dolar;

        public ArticulosAdapter(Activity activity, List<Articulo> p_articulosList, bool pConsulta)
        {
            _activity = activity;
            filter = new SuggestionsFilterArt(this);
            this.consulta = pConsulta;
            this._articulosList = p_articulosList;
            if (!consulta)
            {
                dolar = GlobalUI.ClienteActual.ObtenerClienteCia(App.CompaniaDefault).NivelPrecio.Moneda == TipoMoneda.DOLAR;
                foreach (Articulo art in this._articulosList.Where(x => x.ExisteDetalle()))
                {
                    art.Seleccionado = true;
                }
            }
            this._articulosListMatch = p_articulosList;

        }

        public override int Count
        {
            get { return _articulosListMatch.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return ObjectExtension.ToJavaObject<Articulo>(this._articulosListMatch[position]);
        }

        public override long GetItemId(int position)
        {
            return 0;
        }

        public bool listaInicializada()
        {
            return _articulosList != null && _articulosList.Count > 0;
        }

        public Articulo GetItemArticulo(int position)
        {
            return this._articulosListMatch[position];
        }

        public string GetItemIdCliente(int position)
        {
            return this._articulosListMatch[position].Codigo;
        }

        public decimal getCantidadAlmacen(int position)
        {
            return _articulosListMatch[position].CantidadAlmacen;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = null;
            try
            {
                if (consulta)
                {
                    view = convertView ?? _activity.LayoutInflater.Inflate(
                    Resource.Layout.articulos_rows, parent, false);
                    var txtICodigo = view.FindViewById<TextView>(Resource.Id.txtCodigo);
                    var txtIDesc = view.FindViewById<TextView>(Resource.Id.txtDescripcion);
                    var txtIBar = view.FindViewById<TextView>(Resource.Id.txtBarras);
                    //Jarbis Ajustes IVA >>>>>
                    //var chkCanastaBasica = view.FindViewById<CheckBox>(Resource.Id.chkCanastaBasica);
                    //var chkOtrosCargos = view.FindViewById<CheckBox>(Resource.Id.chkOtrosCargos);
                    //Jarbis Ajustes IVA <<<<<

                    txtICodigo.Text = _articulosListMatch[position].Codigo;
                    txtIDesc.Text = _articulosListMatch[position].Descripcion;
                    txtIBar.Text = _articulosListMatch[position].CodigoBarras;
                    //Jarbis Ajustes IVA >>>>>
                    //chkCanastaBasica.Checked = _articulosListMatch[position].Canasta_Basica;
                    //chkOtrosCargos.Checked = _articulosListMatch[position].Es_Otro_Cargo;
                    //Jarbis Ajustes IVA <<<<<
                }
                else
                {
                    view = convertView ?? _activity.LayoutInflater.Inflate(
                    Resource.Layout.articulos_pedido_rows, parent, false);
                    var txtICodigo = view.FindViewById<TextView>(Resource.Id.txtCodigo);
                    var txtIDesc = view.FindViewById<TextView>(Resource.Id.txtDescripcion);
                    var txtIBar = view.FindViewById<TextView>(Resource.Id.txtBarras);
                    var txtIPrecio = view.FindViewById<TextView>(Resource.Id.txtPrecio);
                    ////Jarbis Ajustes IVA >>>>>
                    //var chkCanastaBasica = view.FindViewById<CheckBox>(Resource.Id.chkCanastaBasica);
                    //var chkOtrosCargos = view.FindViewById<CheckBox>(Resource.Id.chkOtrosCargos);
                    ////Jarbis Ajustes IVA <<<<<
                    //_articulosListMatch[position].CargarPrecioArticulo();
                    //_articulosList.Find(x => x.Codigo == _articulosListMatch[position].Codigo).Precio = _articulosListMatch[position].Precio;
                    txtICodigo.Text = _articulosListMatch[position].Codigo;
                    txtIDesc.Text = _articulosListMatch[position].Descripcion;
                    txtIBar.Text = _articulosListMatch[position].CodigoBarras;
                    ////Jarbis Ajustes IVA >>>>>
                    //chkCanastaBasica.Checked = _articulosListMatch[position].Canasta_Basica;
                    //chkOtrosCargos.Checked = _articulosListMatch[position].Es_Otro_Cargo;
                    ////Jarbis Ajustes IVA <<<<<
                    if (dolar)
                        txtIPrecio.Text = "Alm:" + GestorUtilitario.FormatNumeroDolar(_articulosListMatch[position].PrecioMaximo) + "   Det:" + GestorUtilitario.FormatNumeroDolar(_articulosListMatch[position].PrecioMinimo);
                    else
                        txtIPrecio.Text = "Alm:" + GestorUtilitario.FormatNumero(_articulosListMatch[position].PrecioMaximo) + "   Det:" + GestorUtilitario.FormatNumero(_articulosListMatch[position].PrecioMinimo);
                    if (!consulta && _articulosListMatch[position].Seleccionado)
                    {
                        view.SetBackgroundColor(_activity.Resources.GetColor(Resource.Color.material_blue_grey_800));
                        txtICodigo.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        txtIDesc.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        txtIBar.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        txtIPrecio.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                    }
                    else
                    {
                        view.SetBackgroundColor(Android.Graphics.Color.Transparent);
                        txtICodigo.SetTextColor(_activity.Resources.GetColor(Resource.Color.material_gray_A200));
                        txtIDesc.SetTextColor(_activity.Resources.GetColor(Resource.Color.material_gray_A200));
                        txtIBar.SetTextColor(_activity.Resources.GetColor(Resource.Color.material_gray_A200));
                        txtIPrecio.SetTextColor(_activity.Resources.GetColor(Resource.Color.material_gray_A200));
                    }
                }
                return view;
            }
            catch (Exception ex)
            {
                //throw ex;
                view = convertView ?? _activity.LayoutInflater.Inflate(
                   Resource.Layout.articulos_grid_rows, parent, false);
                return view;
            }

        }

        public Filter Filter
        {
            get { return filter; }
        }

        public void ResetSearch()
        {
            _articulosListMatch = _articulosList;
            NotifyDataSetChanged();
        }

        public void Seleccionar(int position)
        {
            _articulosListMatch[position].Seleccionado = !_articulosListMatch[position].Seleccionado;
            _articulosList.Find(x => x.Codigo == _articulosListMatch[position].Codigo).Seleccionado = _articulosListMatch[position].Seleccionado;
            if (App.FiltroArticulo.Equals(App.SeleccionadosFilter))
            {
                _articulosListMatch = _articulosListMatch.Where(x => x.Seleccionado).ToList();
            }
            NotifyDataSetChanged();
        }

        public void SetCantidadAlmacen(int position, int almacen)
        {
            if (_articulosListMatch[position].Es_Otro_Cargo)
            {
                //Jarbis Ajustes IVA >>>>>

                //Jarbis Ajustes IVA <<<<<
            }
            else
            {
                _articulosListMatch[position].CantidadAlmacen = almacen;
                _articulosList.Find(x => x.Codigo == _articulosListMatch[position].Codigo).CantidadAlmacen = _articulosListMatch[position].CantidadAlmacen;
            }
        }

        public bool Seleccionado(int position)
        {
            return _articulosListMatch[position].Seleccionado;
        }

        public string GetCodigo(int position)
        {
            return _articulosListMatch[position].Codigo;
        }

        public List<Articulo> getSeleccionados()
        {
            return _articulosList.Where(x => x.Seleccionado).ToList();
        }

        public List<Articulo> getRemovidos(List<string> pCodigos)
        {
            return _articulosList.Where(x => pCodigos.Exists(xy => xy.Equals(x.Codigo))).ToList();
        }

        class SuggestionsFilterArt : Filter
        {
            readonly ArticulosAdapter _adapter;

            public SuggestionsFilterArt(ArticulosAdapter adapter)
                : base()
            {
                _adapter = adapter;
            }

            protected override Filter.FilterResults PerformFiltering(Java.Lang.ICharSequence constraint)
            {
                FilterResults results = new FilterResults();
                if (constraint != null && (!String.IsNullOrEmpty(constraint.ToString()) || App.FiltroArticulo.Equals(App.SeleccionadosFilter)))
                {
                    var searchFor = constraint.ToString().ToUpper();
                    var matchList = new List<Articulo>();
                    if (App.FiltroArticulo.Equals(App.CodigoFilter))
                    {
                        var matches =
                        from i in _adapter._articulosList
                        where i.Codigo.IndexOf(searchFor, StringComparison.InvariantCultureIgnoreCase) >= 0
                        select i;
                        matchList = matches.ToList();
                    }
                    else if (App.FiltroArticulo.Equals(App.DescripcionFilter))
                    {
                        var matches =
                        from i in _adapter._articulosList
                        where i.Descripcion.IndexOf(searchFor, StringComparison.InvariantCultureIgnoreCase) >= 0
                        select i;
                        matchList = matches.ToList();
                    }
                    else if (App.FiltroArticulo.Equals(App.BarrasFilter))
                    {
                        var matches =
                        from i in _adapter._articulosList
                        where i.CodigoBarras.IndexOf(searchFor, StringComparison.InvariantCultureIgnoreCase) >= 0
                        select i;
                        matchList = matches.ToList();
                    }
                    else if (App.FiltroArticulo.Equals(App.ClasificacionFilter))
                    {
                        string[] arrayStr = searchFor.Split(Convert.ToChar(","));
                        //.Substring(0, "clasificacionN=".Length)
                        /*
                         * Caso agregar clasificaciones CR6-24402-L14Y
                         * jguzmanc
                         * Inicio cambios
                         */
                        
                        foreach (var item in arrayStr)
                        {
                            if (item.Contains("CLASIFICACION1="))
                            {
                                var matches =
                                    from i in _adapter._articulosList    
                                    where i.Familia.IndexOf(item.Remove(0, "clasificacionN=".Length), StringComparison.InvariantCultureIgnoreCase) >= 0
                                    select i;
                                matchList.AddRange(matches.ToList());
                            }
                            if (item.Contains("CLASIFICACION2="))
                            {
                                var matches =
                                from i in _adapter._articulosList
                                where i.Clase.IndexOf(item.Remove(0, "clasificacionN=".Length), StringComparison.InvariantCultureIgnoreCase) >= 0
                                select i;
                                matchList.AddRange(matches.ToList());
                            }
                            if (item.Contains("CLASIFICACION3="))
                            {
                                var matches =
                                from i in _adapter._articulosList
                                where i.Clasificacion3.IndexOf(item.Remove(0, "clasificacionN=".Length), StringComparison.InvariantCultureIgnoreCase) >= 0
                                select i;
                                matchList.AddRange(matches.ToList());
                            }
                            if (item.Contains("CLASIFICACION4="))
                            {
                                var matches =
                                from i in _adapter._articulosList
                                where i.Clasificacion4.IndexOf(item.Remove(0, "clasificacionN=".Length), StringComparison.InvariantCultureIgnoreCase) >= 0
                                select i;
                                matchList.AddRange(matches.ToList());                  
                            }
                            if (item.Contains("CLASIFICACION5="))
                            {
                                var matches =
                                from i in _adapter._articulosList
                                where i.Clasificacion5.IndexOf(item.Remove(0, "clasificacionN=".Length), StringComparison.InvariantCultureIgnoreCase) >= 0
                                select i;
                                matchList.AddRange(matches.ToList());
                            }
                            if (item.Contains("CLASIFICACION6="))
                            {
                                var matches =
                                from i in _adapter._articulosList
                                where i.Clasificacion6.IndexOf(item.Remove(0, "clasificacionN=".Length), StringComparison.InvariantCultureIgnoreCase) >= 0
                                select i;
                                matchList.AddRange(matches.ToList());
                            }
                        }
                        matchList= matchList.Distinct().ToList();
                        /*
                         * Caso agregar clasificaciones CR6-24402-L14Y
                         * jguzmanc
                         * Fin cambios
                         */

                    }
                    else if (App.FiltroArticulo.Equals(App.SeleccionadosFilter))
                    {
                        var matches =
                        from i in _adapter._articulosList
                        where i.Seleccionado
                        select i;
                        matchList = matches.ToList();
                    }
                    _adapter._articulosListMatch = matchList.ToList();
                    Java.Lang.Object[] matchObjects;
                    matchObjects = new Java.Lang.Object[matchList.Count];
                    for (int i = 0; i < matchList.Count; i++)
                    {
                        matchObjects[i] = (matchList[i]).ToJavaObject();
                    }
                    results.Values = matchObjects;
                    results.Count = matchList.Count;
                }
                else
                {
                    _adapter.ResetSearch();
                }
                return results;
            }

            protected override void PublishResults(Java.Lang.ICharSequence constraint, Filter.FilterResults results)
            {
                _adapter.NotifyDataSetChanged();
            }
        }
    }
}