using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pedidos.Core;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Softland.Droid.PEL
{
    public class ClienteAdapter : BaseAdapter, IFilterable
    {
        Filter filter;
        List<Cliente> _clientesList;
        List<Cliente> _clientesListMatch;
        Activity _activity;

        public ClienteAdapter(Activity activity, List<Cliente> p_clientesList)
        {
            _activity = activity;
            filter = new SuggestionsFilter(this);
            this._clientesList = p_clientesList;
            this._clientesListMatch = p_clientesList;
        }

        public override int Count
        {
            get { return _clientesListMatch.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return ObjectExtension.ToJavaObject<Cliente>(this._clientesListMatch[position]);
        }        

        public override long GetItemId(int position)
        {
            return 0;
        }

        public bool listaInicializada()
        {
            return _clientesList != null && _clientesList.Count > 0;
        }


        public Cliente GetItemCliente(int position)
        {
            return this._clientesListMatch[position];
        }

        public string GetItemIdCliente(int position)
        {
            return this._clientesListMatch[position].Codigo;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView ?? _activity.LayoutInflater.Inflate(
                Resource.Layout.cliente_rows, parent, false);
            try
            {
                var txtICodigo = view.FindViewById<TextView>(Resource.Id.txtCodigo);
                var txtIDesc = view.FindViewById<TextView>(Resource.Id.txtDescripcion);
                txtICodigo.Text = _clientesListMatch[position].Codigo;
                txtIDesc.Text = _clientesListMatch[position].Nombre;
            }
            catch
            { 
            }
            return view;
        }

        public Filter Filter
        {
            get { return filter; }
        }

        public void ResetSearch()
        {
            _clientesListMatch = _clientesList;
            NotifyDataSetChanged();
        }

        public void Seleccionar(int  position)
        {
            _clientesListMatch[position].Seleccionado = !_clientesListMatch[position].Seleccionado;
            _clientesList.Find(x => x.Codigo == _clientesListMatch[position].Codigo).Seleccionado = _clientesListMatch[position].Seleccionado;
            NotifyDataSetChanged();
        }

        public string GetCodigo(int position)
        {
            return _clientesListMatch[position].Codigo;            
        }

        class SuggestionsFilter : Filter
        {
            readonly ClienteAdapter _adapter;

            public SuggestionsFilter(ClienteAdapter adapter)
                : base()
            {
                _adapter = adapter;
            }

            protected override Filter.FilterResults PerformFiltering(Java.Lang.ICharSequence constraint)
            {
                FilterResults results = new FilterResults();
                if (!String.IsNullOrEmpty(constraint.ToString()))
                {
                    var searchFor = constraint.ToString().ToUpper();
                    var matchList = new List<Cliente>();
                    if (App.FiltroCliente.Equals(App.CodigoFilter))
                    {
                        var matches =
                        from i in _adapter._clientesList
                        where i.Codigo.IndexOf(searchFor, StringComparison.InvariantCultureIgnoreCase) >= 0
                        select i;
                        matchList = matches.ToList();
                    }
                    else
                    {
                        var matches =
                        from i in _adapter._clientesList
                        where i.Nombre.IndexOf(searchFor, StringComparison.InvariantCultureIgnoreCase) >= 0
                        select i;
                        matchList = matches.ToList();
                    }
                    _adapter._clientesListMatch = matchList.ToList();
                    Java.Lang.Object[] matchObjects;
                    matchObjects = new Java.Lang.Object[matchList.Count];
                    for (int i = 0; i < matchList.Count; i++)
                    {
                        matchObjects[i] = (matchList[i]).ToJavaObject();
                    }
                    results.Values = matchObjects;
                    results.Count = matchList.Count;
                }
                else
                {
                    _adapter.ResetSearch();
                }
                return results;
            }

            protected override void PublishResults(Java.Lang.ICharSequence constraint, Filter.FilterResults results)
            {
                _adapter.NotifyDataSetChanged();
            }
        }
    }
}