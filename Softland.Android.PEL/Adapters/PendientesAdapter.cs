using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pedidos.Core;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BI.Shared;

namespace Softland.Droid.PEL
{
    public class PendientesAdapter : BaseAdapter, IFilterable
    {
        SuggestionsFilterPed filter;
        List<Pedido> _pedidosList;
        List<Pedido> _pedidosListMatch;
        Activity _activity;

        public PendientesAdapter(Activity activity, List<Pedido> p_pedidosList)
        {
            _activity = activity;
            filter = new SuggestionsFilterPed(this);
            this._pedidosList = p_pedidosList;
            this._pedidosListMatch = p_pedidosList;
            
        }

        public override int Count
        {
            get { return _pedidosListMatch.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return ObjectExtension.ToJavaObject<Pedido>(this._pedidosListMatch[position]);
        }        

        public override long GetItemId(int position)
        {
            return 0;
        }

        public bool listaInicializada ()
        {
            return _pedidosList != null && _pedidosList.Count > 0;
        }

        public Pedido GetItemArticulo(int position)
        {
            return this._pedidosListMatch[position];
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = null;
            try
            {
                string simboloMonetarioPedido = GestorUtilitario.SimboloMonetario;

                view = convertView ?? _activity.LayoutInflater.Inflate(
                Resource.Layout.pendientes_rows, parent, false);
                var txtICompania = view.FindViewById<TextView>(Resource.Id.txtCompania);
                var txtICodigo = view.FindViewById<TextView>(Resource.Id.txtConsecutivo);
                var txtICliente = view.FindViewById<TextView>(Resource.Id.txtCliente);
                var txtIFecha = view.FindViewById<TextView>(Resource.Id.txtFecha);
                var txtITotal = view.FindViewById<TextView>(Resource.Id.txtTotal);
                var txtIMotivo = view.FindViewById<TextView>(Resource.Id.txtMotivo);

                txtICompania.Text = _pedidosListMatch[position].Compania;
                txtICodigo.Text = _pedidosListMatch[position].CodigoConsecutivo;
                txtICliente.Text =  _pedidosListMatch[position].Cliente;
                txtIMotivo.Text = _pedidosListMatch[position].Motivo;
                txtIFecha.Text =  GestorUtilitario.ObtenerFechaString(_pedidosListMatch[position].FechaRealizacion);

                var compania = Compania.Obtener(_pedidosListMatch[position].Compania);

                if (compania != null)
                {
                    simboloMonetarioPedido = compania.SimboloMonetarioFunc;
                }

                if (_pedidosListMatch[position].Moneda.Equals("L") || _pedidosListMatch[position].Configuracion.Nivel.Moneda == TipoMoneda.LOCAL)
                {
                    txtITotal.Text = GestorUtilitario.FormatNumero(_pedidosListMatch[position].MontoNeto, simboloMonetarioPedido);
                }
                else
                {
                    txtITotal.Text = GestorUtilitario.FormatNumeroDolar(_pedidosListMatch[position].MontoNeto);
                }

                if (_pedidosListMatch[position].Seleccionado)
                {
                    view.SetBackgroundColor(_activity.Resources.GetColor(Resource.Color.material_blue_grey_800));
                    txtICodigo.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                    txtICliente.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                    txtIFecha.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                    txtITotal.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                    txtICompania.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                    txtIMotivo.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                    txtIMotivo.SetHintTextColor(Android.Graphics.Color.WhiteSmoke);
                }
                else
                {
                    view.SetBackgroundColor(Android.Graphics.Color.Transparent);
                    txtICodigo.SetTextColor(_activity.Resources.GetColor(Resource.Color.material_gray_A200));
                    txtICliente.SetTextColor(_activity.Resources.GetColor(Resource.Color.material_gray_A200));
                    txtIFecha.SetTextColor(_activity.Resources.GetColor(Resource.Color.material_gray_A200));
                    txtITotal.SetTextColor(_activity.Resources.GetColor(Resource.Color.material_gray_A200));
                    txtICompania.SetTextColor(_activity.Resources.GetColor(Resource.Color.material_gray_A200));
                    txtIMotivo.SetTextColor(_activity.Resources.GetColor(Resource.Color.material_gray_A200));
                    txtIMotivo.SetHintTextColor(_activity.Resources.GetColor(Resource.Color.material_gray_A200));
                }
                
                return view;
            }
            catch (Exception ex)
            {
                //throw ex;
                view = convertView ?? _activity.LayoutInflater.Inflate(
                   Resource.Layout.articulos_grid_rows, parent, false);
                return view;
            }
            
        }

        public Filter Filter
        {
            get { return filter; }
        }

        public void ResetSearch()
        {
            _pedidosListMatch = _pedidosList;
            NotifyDataSetChanged();
        }

        public void Seleccionar(int  position)
        {            
            _pedidosListMatch[position].Seleccionado = !_pedidosListMatch[position].Seleccionado;
            _pedidosList.Find(x => x.Numero == _pedidosListMatch[position].Numero && x.Compania == _pedidosListMatch[position].Compania).Seleccionado = _pedidosListMatch[position].Seleccionado;
            NotifyDataSetChanged();
        }

        public bool Seleccionado(int position)
        {
            return _pedidosListMatch[position].Seleccionado;
        }

        public string GetCodigo(int position)
        {
            return _pedidosListMatch[position].Numero;            
        }

        public List<Pedido> getSeleccionados()
        {
            return _pedidosList.Where(x => x.Seleccionado).ToList();
        }

        public int getSeleccionadosCount()
        {
            return _pedidosList.Where(x => x.Seleccionado).ToList().Count;
        }

        class SuggestionsFilterPed : Filter
        {
            readonly PendientesAdapter _adapter;

            public SuggestionsFilterPed(PendientesAdapter adapter)
                : base()
            {
                _adapter = adapter;
            }

            protected override Filter.FilterResults PerformFiltering(Java.Lang.ICharSequence constraint)
            {
                FilterResults results = new FilterResults();
                if (constraint!=null && !String.IsNullOrEmpty(constraint.ToString()))
                {
                    var searchFor = constraint.ToString().ToUpper();
                    var matchList = new List<Pedido>();
                    if (App.FiltroPedido.Equals(App.CodigoFilter))
                    {
                        var matches =
                        from i in _adapter._pedidosList
                        where i.Numero.IndexOf(searchFor, StringComparison.InvariantCultureIgnoreCase) >= 0
                        select i;
                        matchList = matches.ToList();
                    }
                    else if (App.FiltroPedido.Equals(App.ClienteFilter))
                    {
                        var matches =
                        from i in _adapter._pedidosList
                        where i.Cliente.IndexOf(searchFor, StringComparison.InvariantCultureIgnoreCase) >= 0
                        select i;
                        matchList = matches.ToList();
                    }
                    else if (App.FiltroPedido.Equals(App.CompaniaFilter))
                    {
                        var matches =
                        from i in _adapter._pedidosList
                        where i.Compania.IndexOf(searchFor, StringComparison.InvariantCultureIgnoreCase) >= 0
                        select i;
                        matchList = matches.ToList();
                    }
                    _adapter._pedidosListMatch = matchList.ToList();
                    Java.Lang.Object[] matchObjects;
                    matchObjects = new Java.Lang.Object[matchList.Count];
                    for (int i = 0; i < matchList.Count; i++)
                    {
                        matchObjects[i] = (matchList[i]).ToJavaObject();
                    }
                    results.Values = matchObjects;
                    results.Count = matchList.Count;
                }
                else
                {
                    _adapter.ResetSearch();
                }
                return results;
            }

            protected override void PublishResults(Java.Lang.ICharSequence constraint, Filter.FilterResults results)
            {
                _adapter.NotifyDataSetChanged();
            }
        }
    }
}