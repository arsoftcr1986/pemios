using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pedidos.Core;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BI.Shared;
using Android.Graphics.Drawables;

namespace Softland.Droid.PEL
{
    public class ArticulosGridAdapter : BaseAdapter, IFilterable
    {
        SuggestionsFilterArtGrid filter;
        List<Articulo> _articulosList;
        List<Articulo> _articulosListMatch;
        Activity _activity;
        bool consulta;
        bool dolar;

        public ArticulosGridAdapter(Activity activity, List<Articulo> p_articulosList,bool pConsulta)
        {
            _activity = activity;
            filter = new SuggestionsFilterArtGrid(this);
            this.consulta = pConsulta;
            this._articulosList = p_articulosList;
            if (!consulta)
            {
                dolar = GlobalUI.ClienteActual.ObtenerClienteCia(App.CompaniaDefault).NivelPrecio.Moneda  == TipoMoneda.DOLAR;
                foreach (Articulo art in this._articulosList.Where(x => x.ExisteDetalle()))
                {
                    art.Seleccionado = true;
                }
            }            
            this._articulosListMatch = p_articulosList;
            
        }

        public override int Count
        {
            get { return _articulosListMatch.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return ObjectExtension.ToJavaObject<Articulo>(this._articulosListMatch[position]);
        }        

        public override long GetItemId(int position)
        {
            return 0;
        }

        public bool listaInicializada ()
        {
            return _articulosList != null && _articulosList.Count > 0;
        }

        public Articulo GetItemArticulo(int position)
        {
            return this._articulosListMatch[position];
        }

        public string GetItemIdCliente(int position)
        {
            return this._articulosListMatch[position].Codigo;
        }

        public decimal getCantidadAlmacen(int position)
        {
            return _articulosListMatch[position].CantidadAlmacen;
        }

        public void SetCantidadAlmacen(int position, int almacen)
        {
            _articulosListMatch[position].CantidadAlmacen = almacen;
            _articulosList.Find(x => x.Codigo == _articulosListMatch[position].Codigo).CantidadAlmacen = _articulosListMatch[position].CantidadAlmacen;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = null;
            try
            {
                String codImage = "";          
                if (consulta)
                {
                    view = convertView ?? _activity.LayoutInflater.Inflate(
                    Resource.Layout.articulos_grid_rows, parent, false);

                    var txtICodigo = view.FindViewById<TextView>(Resource.Id.txtCodigo);
                    var txtIDesc = view.FindViewById<TextView>(Resource.Id.txtDescripcion);
                    var imgIIMG1 = view.FindViewById<ImageView>(Resource.Id.img1);
                    ////Jarbis Ajustes IVA >>>>>
                    //var chkCanastaBasica = view.FindViewById<CheckBox>(Resource.Id.chkCanastaBasica);
                    //var chkOtrosCargos = view.FindViewById<CheckBox>(Resource.Id.chkOtrosCargos);
                    ////Jarbis Ajustes IVA <<<<<

                    txtICodigo.Text = _articulosListMatch[position].Codigo;
                    txtIDesc.Text = _articulosListMatch[position].Descripcion;
                    ////Jarbis Ajustes IVA >>>>>
                    //chkCanastaBasica.Checked = _articulosListMatch[position].Canasta_Basica;
                    //chkOtrosCargos.Checked = _articulosListMatch[position].Es_Otro_Cargo;
                    ////Jarbis Ajustes IVA <<<<<

                    codImage = _articulosListMatch[position].Codigo.Replace('/', '-');
                    string pathImage = System.IO.Path.Combine(App.PathImagen, codImage + ".jpg");//System.IO.Path.Combine(App.PathImagen, _articulosListMatch[position].Codigo + ".jpg");

                    if (System.IO.File.Exists(pathImage))
                    {
                        Drawable image = Drawable.CreateFromPath(pathImage);
                        imgIIMG1.SetBackgroundDrawable(image);
                    }
                    else
                    {
                        imgIIMG1.SetBackgroundResource(Resource.Drawable.ic_jpg);
                    }

                    imgIIMG1.SetAdjustViewBounds(true);
                    
                }
                else
                {
                    view = convertView ?? _activity.LayoutInflater.Inflate(
                    Resource.Layout.articulos_pedido_grid_rows, parent, false);
                    var txtICodigo = view.FindViewById<TextView>(Resource.Id.txtCodigo);
                    var txtIDesc = view.FindViewById<TextView>(Resource.Id.txtDescripcion);
                    var imgIIMG1 = view.FindViewById<ImageView>(Resource.Id.img1);
                    var txtIPrecio = view.FindViewById<TextView>(Resource.Id.txtPrecio);
                    //_articulosListMatch[position].CargarPrecioArticulo();
                    //_articulosList.Find(x => x.Codigo == _articulosListMatch[position].Codigo).Precio = _articulosListMatch[position].Precio;
                    txtICodigo.Text = _articulosListMatch[position].Codigo;
                    txtIDesc.Text = _articulosListMatch[position].Descripcion;
                    codImage = _articulosListMatch[position].Codigo.Replace('/', '-');
                    string pathImage = System.IO.Path.Combine(App.PathImagen, codImage + ".jpg");
                    //string pathImage = System.IO.Path.Combine(App.PathImagen, _articulosListMatch[position].Codigo + ".jpg");
                    if (System.IO.File.Exists(pathImage))
                    {
                        Drawable image = Drawable.CreateFromPath(pathImage);
                        imgIIMG1.SetBackgroundDrawable(image);
                    }
                    else
                    {
                        imgIIMG1.SetBackgroundResource(Resource.Drawable.ic_jpg);
                    }
                    imgIIMG1.SetAdjustViewBounds(true);
                    if (dolar)
                        txtIPrecio.Text = "Alm:" + GestorUtilitario.FormatNumeroDolar(_articulosListMatch[position].PrecioMaximo) + "   Det:" + GestorUtilitario.FormatNumeroDolar(_articulosListMatch[position].PrecioMinimo);
                    else
                        txtIPrecio.Text = "Alm:" + GestorUtilitario.FormatNumero(_articulosListMatch[position].PrecioMaximo) + "   Det:" + GestorUtilitario.FormatNumero(_articulosListMatch[position].PrecioMinimo);
                    if (!consulta && _articulosListMatch[position].Seleccionado)
                    {
                        view.SetBackgroundColor(_activity.Resources.GetColor(Resource.Color.material_blue_grey_800));
                        txtICodigo.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        txtIDesc.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        txtIPrecio.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                    }
                    else
                    {
                        view.SetBackgroundColor(Android.Graphics.Color.Transparent);
                        txtICodigo.SetTextColor(_activity.Resources.GetColor(Resource.Color.material_gray_A200));
                        txtIDesc.SetTextColor(_activity.Resources.GetColor(Resource.Color.material_gray_A200));
                        txtIPrecio.SetTextColor(_activity.Resources.GetColor(Resource.Color.material_gray_A200));
                    }
                }
                return view;
            }
            catch (Exception ex)
            {
                //throw ex;
                view = convertView ?? _activity.LayoutInflater.Inflate(
                   Resource.Layout.articulos_grid_rows, parent, false);
                return view;
            }
            
        }

        public Filter Filter
        {
            get { return filter; }
        }

        public void ResetSearch()
        {
            _articulosListMatch = _articulosList;
            NotifyDataSetChanged();
        }

        public void Seleccionar(int  position)
        {            
            _articulosListMatch[position].Seleccionado = !_articulosListMatch[position].Seleccionado;
            _articulosList.Find(x => x.Codigo == _articulosListMatch[position].Codigo).Seleccionado = _articulosListMatch[position].Seleccionado;
            if (App.FiltroArticulo.Equals(App.SeleccionadosFilter))
            {
                _articulosListMatch = _articulosListMatch.Where(x => x.Seleccionado).ToList();
            }
            NotifyDataSetChanged();
        }

        public bool Seleccionado(int position)
        {
            return _articulosListMatch[position].Seleccionado;
        }

        public string GetCodigo(int position)
        {
            return _articulosListMatch[position].Codigo;            
        }

        public List<Articulo> getSeleccionados()
        {
            return _articulosList.Where(x => x.Seleccionado).ToList();
        }

        public List<Articulo> getRemovidos(List<string> pCodigos)
        {
            return _articulosList.Where(x => pCodigos.Exists(xy=>xy.Equals(x.Codigo))).ToList();
        }

        class SuggestionsFilterArtGrid : Filter
        {
            readonly ArticulosGridAdapter _adapter;

            public SuggestionsFilterArtGrid(ArticulosGridAdapter adapter)
                : base()
            {
                _adapter = adapter;
            }

            protected override Filter.FilterResults PerformFiltering(Java.Lang.ICharSequence constraint)
            {
                FilterResults results = new FilterResults();
                if(constraint!=null && (!String.IsNullOrEmpty(constraint.ToString()) || App.FiltroArticulo.Equals(App.SeleccionadosFilter)))
                {
                    var searchFor = constraint.ToString().ToUpper();
                    var matchList = new List<Articulo>();
                    if (App.FiltroArticulo.Equals(App.CodigoFilter))
                    {
                        var matches =
                        from i in _adapter._articulosList
                        where i.Codigo.IndexOf(searchFor, StringComparison.InvariantCultureIgnoreCase) >= 0
                        select i;
                        matchList = matches.ToList();
                    }
                    else if (App.FiltroArticulo.Equals(App.DescripcionFilter))
                    {
                        var matches =
                        from i in _adapter._articulosList
                        where i.Descripcion.IndexOf(searchFor, StringComparison.InvariantCultureIgnoreCase) >= 0
                        select i;
                        matchList = matches.ToList();
                    }
                    else if (App.FiltroArticulo.Equals(App.BarrasFilter))
                    {
                        var matches =
                        from i in _adapter._articulosList
                        where i.CodigoBarras.IndexOf(searchFor, StringComparison.InvariantCultureIgnoreCase) >= 0
                        select i;
                        matchList = matches.ToList();
                    }
                    else if (App.FiltroArticulo.Equals(App.ClasificacionFilter))
                    {
                        string[] arrayStr = searchFor.Split(Convert.ToChar(","));
                        switch (arrayStr.Length)
                        {
                            case 1:
                                {
                                    var matches =
                                    from i in _adapter._articulosList
                                    where i.Familia.IndexOf(arrayStr[0], StringComparison.InvariantCultureIgnoreCase) >= 0
                                    select i;
                                    matchList = matches.ToList();
                                    break;
                                }
                            case 2:
                                {
                                    var matches =
                                    from i in _adapter._articulosList
                                    where i.Familia.IndexOf(arrayStr[0], StringComparison.InvariantCultureIgnoreCase) >= 0
                                    && i.Clase.IndexOf(arrayStr[1], StringComparison.InvariantCultureIgnoreCase) >= 0
                                    select i;
                                    matchList = matches.ToList();
                                    break;
                                }
                            default: break;
                        }
                    }
                    else if (App.FiltroArticulo.Equals(App.SeleccionadosFilter))
                    {
                        var matches =
                        from i in _adapter._articulosList
                        where i.Seleccionado
                        select i;
                        matchList = matches.ToList();
                    }
                    _adapter._articulosListMatch = matchList.ToList();
                    Java.Lang.Object[] matchObjects;
                    matchObjects = new Java.Lang.Object[matchList.Count];
                    for (int i = 0; i < matchList.Count; i++)
                    {
                        matchObjects[i] = (matchList[i]).ToJavaObject();
                    }
                    results.Values = matchObjects;
                    results.Count = matchList.Count;
                }
                else
                {
                    _adapter.ResetSearch();
                }
                return results;
            }

            protected override void PublishResults(Java.Lang.ICharSequence constraint, Filter.FilterResults results)
            {
                _adapter.NotifyDataSetChanged();
            }
        }
    }
}