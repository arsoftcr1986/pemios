using Android.Support.V4.App;

namespace Softland.Droid.PEL
{
    class ArticulosPageAdapter: FragmentPagerAdapter
    {
        ArticulosFragment articulofragment;
        ArticulosFragmentImagenes articulofragmentimages;

        private static readonly string[] Content = new string[] { "Listado", "Im�genes" };

        public ArticulosPageAdapter(FragmentManager p0) : base(p0) 
        { 
        }

        public override int Count
        {
            get { return Content.Length; }
        }

        public override Fragment GetItem(int index)
        {
            switch (index)
            {
                case 0:
                    {
                        if (articulofragment == null)
                            articulofragment = new ArticulosFragment();
                        return articulofragment;
                    }
                case 1:
                    {
                        if (articulofragmentimages == null)
                            articulofragmentimages = new ArticulosFragmentImagenes();
                        return articulofragmentimages;
                    }
										
            }
            return null;
        }            

        public override Java.Lang.ICharSequence GetPageTitleFormatted(int p0) 
        { 
            return new Java.Lang.String(Content[p0 % Content.Length].ToUpper()); 
        }
    }
}