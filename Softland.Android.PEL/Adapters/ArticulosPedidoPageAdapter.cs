using Android.Support.V4.App;

namespace Softland.Droid.PEL
{
    class ArticulosPedidoPageAdapter: FragmentPagerAdapter
    {
        ArticulosFragmentPedidos articulofragment;
        ArticulosFragmentImagenesPedidos articulofragmentimages;

        private static readonly string[] Content = new[]
            {
                "Listado","Im�genes"
            };

        public ArticulosPedidoPageAdapter(FragmentManager p0) 
                : base(p0) 
            { }

            public override int Count
            {
                get { return Content.Length; }
            }

            public override Fragment GetItem(int index)
            {
                switch (index)
                {
                    case 0:
                        {
                            if (articulofragment == null)
                                articulofragment = new ArticulosFragmentPedidos();
                            return articulofragment;
                        }
                    case 1:
                        {
                            if (articulofragmentimages == null)
                                articulofragmentimages = new ArticulosFragmentImagenesPedidos();
                            return articulofragmentimages;
                        }
										
                }
                return null;
            }            

            public override Java.Lang.ICharSequence GetPageTitleFormatted(int p0) { return new Java.Lang.String(Content[p0 % Content.Length].ToUpper()); }
    }
}