using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BI.Shared;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Data.SQLite;
using BI.Android;
using Pedidos.Core;

namespace Softland.Droid.PEL
{
    [Activity(Label = "Editar l�nea", Icon = "@drawable/ic_launcher", ParentActivity = typeof(PedidoMainActivity), ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
    public class PedidoLineaActivity : BaseActivity
    {
        string codArticulos;
        TextView txtCodigo, txtDescripcion, txtExistencia,txtExistenciaBon, txtPrecio;
        EditText txtDescuento, txtBonificacion, txtCantidad, txtCantidadDetalle,txtSubtotal, txtTotal;
        bool dolar;
        bool cargaInicial = false;
        bool cambiaDescuento = true;
        bool cambiaBonificacion = true;
        bool permiteCambioDescuento = true;
        bool haybonificacion = false;
        bool usaLocalizacion = false;
        Spinner cmbBodega, cmbBodegaBon, cmdLocalizaciones, cmdLocalizacionesBon;
        ImageButton imgBonificacion;

        protected override int LayoutResource
        {
            get
            {
                return Resource.Layout.linea_articulo;
            }
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            cargaInicial = true;
            permiteCambioDescuento = App.CambiarDescuento;
            txtCodigo = FindViewById<TextView>(Resource.Id.txtCodigo);
            txtDescripcion = FindViewById<TextView>(Resource.Id.txtDescripcion);
            txtExistencia = FindViewById<TextView>(Resource.Id.txtExistencia);
            txtExistenciaBon = FindViewById<TextView>(Resource.Id.txtExistenciaBon);
            txtPrecio = FindViewById<TextView>(Resource.Id.txtPrecio);
            cmbBodega = FindViewById<Spinner>(Resource.Id.cmbBodegas);
            cmbBodegaBon = FindViewById<Spinner>(Resource.Id.cmbBodegasBon);
            txtDescuento = FindViewById<EditText>(Resource.Id.txtDescuento);
            txtBonificacion = FindViewById<EditText>(Resource.Id.txtBonificacion);
            txtCantidad = FindViewById<EditText>(Resource.Id.txtCantidad);
            txtCantidadDetalle = FindViewById<EditText>(Resource.Id.txtCantidadDetalle);
            txtSubtotal = FindViewById<EditText>(Resource.Id.txtSubTotal);
            txtTotal = FindViewById<EditText>(Resource.Id.txtTotal);
            dolar = GlobalUI.ClienteActual.ObtenerClienteCia(App.CompaniaDefault).NivelPrecio.Moneda == TipoMoneda.DOLAR;
            cmdLocalizaciones = FindViewById<Spinner>(Resource.Id.cmbLocalizacion);
            cmdLocalizacionesBon = FindViewById<Spinner>(Resource.Id.cmbLocalizacionBon);
            imgBonificacion = FindViewById<ImageButton>(Resource.Id.imgBonificacion);
            // Create your application here   
            usaLocalizacion = GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.UsaLocalizacion;
            cmbBodega.ItemSelected += async (s, e) => {                
                bool result = await ExecuteConsultarExistencia(cmbBodega.SelectedItem.ToString());
            };
            cmbBodegaBon.ItemSelected += async (s, e) =>
            {
                bool result = await ExecuteConsultarExistenciaBonificacion(cmbBodegaBon.SelectedItem.ToString());
            };
            cmdLocalizaciones.ItemSelected += async (s, e) =>
            {
                bool result = await ExecuteConsultarLocalizacion(cmdLocalizaciones.SelectedItem.ToString());
            };
            cmdLocalizacionesBon.ItemSelected += async (s, e) =>
            {
                bool result = await ExecuteConsultarLocalizacionBon(cmdLocalizacionesBon.SelectedItem.ToString());
            };
            txtCantidad.AfterTextChanged += async (s, e) =>
                {
                    try
                    {
                        if (cargaInicial)
                            return;
                        DetallePedido art = GestorPedido.PedidosCollection.BuscarDetalleCodigo(codArticulos, App.CompaniaDefault);
                        decimal cantidad = 0;
                        decimal cantidadDetalle = 0;
                        if (string.IsNullOrEmpty(((EditText)s).Text) || !Decimal.TryParse(((EditText)s).Text, out cantidad))
                        {
                            Toast.MakeText(this, "Cantidad almac�n inv�lida revise la entrada de datos", ToastLength.Short).Show();
                            return;
                        }
                        if (string.IsNullOrEmpty(txtCantidadDetalle.Text) || !Decimal.TryParse(txtCantidadDetalle.Text, out cantidadDetalle))
                        {
                            Toast.MakeText(this, "Cantidad detalle inv�lida revise la entrada de datos", ToastLength.Short).Show();
                            return;
                        }
                        if (!Decimal.TryParse(((EditText)s).Text, out cantidad))
                        {
                            cantidad = art.UnidadesAlmacen;
                        }
                        if (!Decimal.TryParse(txtCantidadDetalle.Text, out cantidadDetalle))
                        {
                            cantidadDetalle = art.UnidadesDetalle;
                        }
                        if (cantidadDetalle == 0 && cantidad == 0)
                        {
                            Toast.MakeText(this, "Ambas cantidades no pueden ser 0", ToastLength.Short).Show();
                            return;
                        }
                        if (await (ExecuteCambiarCantidad(cantidad,art.UnidadesDetalle)))
                        {
                            if (await ExecuteConsultar())
                            {
                                
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        string excep = ex.Message;
                    }
                    
                    
                };
            txtCantidadDetalle.AfterTextChanged += async (s, e) =>
            {
                try
                {
                    if (cargaInicial)
                        return;
                    DetallePedido art = GestorPedido.PedidosCollection.BuscarDetalleCodigo(codArticulos, App.CompaniaDefault);
                    decimal cantidad = 0;
                    decimal cantidadAmacen = 0;
                    if (string.IsNullOrEmpty(((EditText)s).Text) || !Decimal.TryParse(((EditText)s).Text, out cantidad))
                    {
                        Toast.MakeText(this, "Cantidad detalle inv�lida revise la entrada de datos", ToastLength.Short).Show();
                        return;
                    }
                    if (string.IsNullOrEmpty(txtCantidad.Text) || !Decimal.TryParse(txtCantidad.Text, out cantidadAmacen))
                    {
                        Toast.MakeText(this, "Cantidad almac�n inv�lida revise la entrada de datos", ToastLength.Short).Show();
                        return;
                    }
                    if (!Decimal.TryParse(((EditText)s).Text, out cantidad))
                    {
                        cantidad = art.UnidadesDetalle;
                    }
                    if (!Decimal.TryParse(txtCantidad.Text, out cantidadAmacen))
                    {
                        cantidadAmacen = art.UnidadesAlmacen;
                    }
                    if (cantidad == 0 && cantidadAmacen == 0)
                    {
                        Toast.MakeText(this, "Ambas cantidades no pueden ser 0", ToastLength.Short).Show();
                        return;
                    }
                    if (await (ExecuteCambiarCantidad(art.UnidadesAlmacen,cantidad)))
                    {
                        if (await ExecuteConsultar())
                        {
                        }
                    }
                }
                catch (Exception ex)
                {
                    string excep = ex.Message;
                }


            };
            txtDescuento.AfterTextChanged += async (s, e) =>
            {
                try
                {
                    if (cargaInicial || !cambiaDescuento)
                        return;                    
                    decimal cantidad = 0;
                    if (string.IsNullOrEmpty(((EditText)s).Text) || !Decimal.TryParse(((EditText)s).Text.Replace("%", ""), out cantidad))
                    {
                        Toast.MakeText(this, "Cantidad descuento inv�lida revise la entrada de datos", ToastLength.Short).Show();
                        return;
                    }
                    if (await (ExecuteCambiarDescuento(cantidad)))
                    {
                        if (await ExecuteConsultar())
                        {
                        }
                    }
                    else
                    {
                        if (await ExecuteConsultar())
                        {
                        }
                    }
                }
                catch (Exception ex)
                {
                    string excep = ex.Message;
                }                                
                
            };
            imgBonificacion.Click += (s, e) =>
            {
                MostarDatosBonificacion();
            };
            //txtCantidad.SetFilters(new Android.Text.IInputFilter[] { new EditTextFilterAlmDet() });
            CargaInicial();            
        }


        public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);

            if (newConfig.Orientation == Android.Content.Res.Orientation.Landscape)
            {
                base.setContentViewReload(Resource.Layout.linea_articulo);
            }
            else
            {
                base.setContentViewReload(Resource.Layout.linea_articulo);
            }

            permiteCambioDescuento = App.CambiarDescuento;
            txtCodigo = FindViewById<TextView>(Resource.Id.txtCodigo);
            txtDescripcion = FindViewById<TextView>(Resource.Id.txtDescripcion);
            txtExistencia = FindViewById<TextView>(Resource.Id.txtExistencia);
            txtExistenciaBon = FindViewById<TextView>(Resource.Id.txtExistenciaBon);
            txtPrecio = FindViewById<TextView>(Resource.Id.txtPrecio);
            cmbBodega = FindViewById<Spinner>(Resource.Id.cmbBodegas);
            cmbBodegaBon = FindViewById<Spinner>(Resource.Id.cmbBodegasBon);
            txtDescuento = FindViewById<EditText>(Resource.Id.txtDescuento);
            txtBonificacion = FindViewById<EditText>(Resource.Id.txtBonificacion);
            txtCantidad = FindViewById<EditText>(Resource.Id.txtCantidad);
            txtCantidadDetalle = FindViewById<EditText>(Resource.Id.txtCantidadDetalle);
            txtSubtotal = FindViewById<EditText>(Resource.Id.txtSubTotal);
            txtTotal = FindViewById<EditText>(Resource.Id.txtTotal);
            dolar = GlobalUI.ClienteActual.ObtenerClienteCia(App.CompaniaDefault).NivelPrecio.Moneda == TipoMoneda.DOLAR;
            cmdLocalizaciones = FindViewById<Spinner>(Resource.Id.cmbLocalizacion);
            cmdLocalizacionesBon = FindViewById<Spinner>(Resource.Id.cmbLocalizacionBon);
            imgBonificacion = FindViewById<ImageButton>(Resource.Id.imgBonificacion);
            // Create your application here   
            usaLocalizacion = GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.UsaLocalizacion;
            cmbBodega.ItemSelected += async (s, e) =>
            {
                bool result = await ExecuteConsultarExistencia(cmbBodega.SelectedItem.ToString());
            };
            cmbBodegaBon.ItemSelected += async (s, e) =>
            {
                bool result = await ExecuteConsultarExistenciaBonificacion(cmbBodegaBon.SelectedItem.ToString());
            };
            cmdLocalizaciones.ItemSelected += async (s, e) =>
            {
                bool result = await ExecuteConsultarLocalizacion(cmdLocalizaciones.SelectedItem.ToString());
            };
            cmdLocalizacionesBon.ItemSelected += async (s, e) =>
            {
                bool result = await ExecuteConsultarLocalizacionBon(cmdLocalizacionesBon.SelectedItem.ToString());
            };
            txtCantidad.AfterTextChanged += async (s, e) =>
            {
                try
                {
                    if (cargaInicial)
                        return;
                    DetallePedido art = GestorPedido.PedidosCollection.BuscarDetalleCodigo(codArticulos, App.CompaniaDefault);
                    decimal cantidad = 0;
                    decimal cantidadDetalle = 0;
                    if (string.IsNullOrEmpty(((EditText)s).Text) || !Decimal.TryParse(((EditText)s).Text, out cantidad))
                    {
                        Toast.MakeText(this, "Cantidad almac�n inv�lida revise la entrada de datos", ToastLength.Short).Show();
                        return;
                    }
                    if (string.IsNullOrEmpty(txtCantidadDetalle.Text) || !Decimal.TryParse(txtCantidadDetalle.Text, out cantidadDetalle))
                    {
                        Toast.MakeText(this, "Cantidad detalle inv�lida revise la entrada de datos", ToastLength.Short).Show();
                        return;
                    }
                    if (!Decimal.TryParse(((EditText)s).Text, out cantidad))
                    {
                        cantidad = art.UnidadesAlmacen;
                    }
                    if (!Decimal.TryParse(txtCantidadDetalle.Text, out cantidadDetalle))
                    {
                        cantidadDetalle = art.UnidadesDetalle;
                    }
                    if (cantidadDetalle == 0 && cantidad == 0)
                    {
                        Toast.MakeText(this, "Ambas cantidades no pueden ser 0", ToastLength.Short).Show();
                        return;
                    }
                    if (await (ExecuteCambiarCantidad(cantidad, art.UnidadesDetalle)))
                    {
                        if (await ExecuteConsultar())
                        {

                        }
                    }
                }
                catch (Exception ex)
                {
                    string excep = ex.Message;
                }


            };
            txtCantidadDetalle.AfterTextChanged += async (s, e) =>
            {
                try
                {
                    if (cargaInicial)
                        return;
                    DetallePedido art = GestorPedido.PedidosCollection.BuscarDetalleCodigo(codArticulos, App.CompaniaDefault);
                    decimal cantidad = 0;
                    decimal cantidadAmacen = 0;
                    if (string.IsNullOrEmpty(((EditText)s).Text) || !Decimal.TryParse(((EditText)s).Text, out cantidad))
                    {
                        Toast.MakeText(this, "Cantidad detalle inv�lida revise la entrada de datos", ToastLength.Short).Show();
                        return;
                    }
                    if (string.IsNullOrEmpty(txtCantidad.Text) || !Decimal.TryParse(txtCantidad.Text, out cantidadAmacen))
                    {
                        Toast.MakeText(this, "Cantidad almac�n inv�lida revise la entrada de datos", ToastLength.Short).Show();
                        return;
                    }
                    if (!Decimal.TryParse(((EditText)s).Text, out cantidad))
                    {
                        cantidad = art.UnidadesDetalle;
                    }
                    if (!Decimal.TryParse(txtCantidad.Text, out cantidadAmacen))
                    {
                        cantidadAmacen = art.UnidadesAlmacen;
                    }
                    if (cantidad == 0 && cantidadAmacen == 0)
                    {
                        Toast.MakeText(this, "Ambas cantidades no pueden ser 0", ToastLength.Short).Show();
                        return;
                    }
                    if (await (ExecuteCambiarCantidad(art.UnidadesAlmacen, cantidad)))
                    {
                        if (await ExecuteConsultar())
                        {
                        }
                    }
                }
                catch (Exception ex)
                {
                    string excep = ex.Message;
                }


            };
            txtDescuento.AfterTextChanged += async (s, e) =>
            {
                try
                {
                    if (cargaInicial || !cambiaDescuento)
                        return;
                    decimal cantidad = 0;
                    if (string.IsNullOrEmpty(((EditText)s).Text) || !Decimal.TryParse(((EditText)s).Text.Replace("%", ""), out cantidad))
                    {
                        Toast.MakeText(this, "Cantidad descuento inv�lida revise la entrada de datos", ToastLength.Short).Show();
                        return;
                    }
                    if (await (ExecuteCambiarDescuento(cantidad)))
                    {
                        if (await ExecuteConsultar())
                        {
                        }
                    }
                    else
                    {
                        if (await ExecuteConsultar())
                        {
                        }
                    }
                }
                catch (Exception ex)
                {
                    string excep = ex.Message;
                }

            };
            imgBonificacion.Click += (s, e) =>
            {
                MostarDatosBonificacion();
            };
            //txtCantidad.SetFilters(new Android.Text.IInputFilter[] { new EditTextFilterAlmDet() });
            CargaInicial();
        }


        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.info, menu);
            return true;
        }

        private async void CargaInicial()
        {
            codArticulos = Intent.GetStringExtra("codigo");
            txtDescuento.Enabled = permiteCambioDescuento;
            bool result = await ExecuteConsultar();
            cargaInicial = false;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    {
                        OnBackPressed();
                        break;
                    }
                case Resource.Id.menu_info:
                    {
                        Toast.MakeText(this, GlobalUI.NameCliente, ToastLength.Long).Show();
                        break;
                    }
            }
            return true;
        }

        public override void OnBackPressed()
        {
            var intent = new Intent(this, typeof(PedidoMainActivity));
            intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
            SetResult(Result.Ok, intent);
            Finish();
        }
        

        #region metodos        

        private void MostarDatosBonificacion()
        {
            string mostrar;
            string cantidad;

            // Referencia al detalle del art�culo en el pedido.
            DetallePedido DetalleSeleccionado = GestorPedido.PedidosCollection.BuscarDetalleCodigo(codArticulos, App.CompaniaDefault);

            // Si el detalle tiene una linea bonificada, se arma un mensaje con la informaci�n del articulo bonificado
            // para mostrarlo al usuario.
            if (DetalleSeleccionado.LineaBonificada != null)
            {
                // Se obtiene el art�culo bonificado.
                Articulo articuloBonificado = DetalleSeleccionado.LineaBonificada.Articulo;

                mostrar = articuloBonificado.Codigo + " - " + articuloBonificado.Descripcion;
                if (mostrar.Length > 34)
                    mostrar = mostrar.Substring(0, 34);
                mostrar = mostrar + " - ";
                cantidad = " Cant. Bonifica: " + articuloBonificado.TipoEmpaqueAlmacen + ": " + GestorUtilitario.Formato(DetalleSeleccionado.LineaBonificada.UnidadesAlmacen) + " | " +
                           articuloBonificado.TipoEmpaqueDetalle + ": " + GestorUtilitario.Formato(DetalleSeleccionado.LineaBonificada.UnidadesDetalle);
                mostrar = mostrar + cantidad;

                Toast.MakeText(Android.App.Application.Context, mostrar, ToastLength.Long).Show();
            }
            else
            {
                Toast.MakeText(Android.App.Application.Context, "No hay art�culo bonificado.", ToastLength.Long).Show();
            }
        }

        private Task<bool> ExecuteConsultar()
        {
            try
            {
                return Task.Run(() =>
                {
                    try
                    {
                        Connection cnxArticulo = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                        DetallePedido art = GestorPedido.PedidosCollection.BuscarDetalleCodigo(codArticulos, App.CompaniaDefault);
                        var listaBodegas = art.Articulo.ObtenerBodegasAsociadas(App.CompaniaDefault, cnxArticulo);
                        var listaBodegasBon=new List<string>(){" "};
                        if (art.LineaBonificada != null)
                        {
                            haybonificacion = true;
                            RunOnUiThread(()=>{
                                cmbBodegaBon.Enabled = true;
                                cmdLocalizacionesBon.Enabled = true;
                            });                            
                            listaBodegasBon=art.LineaBonificada.Articulo.ObtenerBodegasAsociadas(App.CompaniaDefault, cnxArticulo);
                        }
                        else
                        {
                            haybonificacion = false;
                            RunOnUiThread(() =>
                            {
                                cmbBodegaBon.Enabled = false;
                                cmdLocalizacionesBon.Enabled = false;
                            });
                            
                        }
                        cnxArticulo.Close();
                        cnxArticulo = null;
                        cnxArticulo = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                        art.Articulo.CargarPrecioArticulo(cnxArticulo);
                        cnxArticulo.Close();
                        cnxArticulo = null;
                        RunOnUiThread(() =>
                        {
                            txtCodigo.Text = art.Articulo.Codigo;
                            txtDescripcion.Text = art.Articulo.Descripcion;
                            if (art.RegaliaDescuentoLinea != null && art.RegaliaDescuentoLinea.Tipo == TipoRegalia.DescuentoLinea)
                            {
                                txtDescuento.Enabled = false;
                                cambiaDescuento = false;
                            }
                            else
                            {
                                txtDescuento.Enabled = permiteCambioDescuento;
                                cambiaDescuento = permiteCambioDescuento;
                            }
                            if (art.RegaliaBonificacion != null && art.RegaliaBonificacion.Tipo == TipoRegalia.Bonificacion)
                            {
                                txtBonificacion.Enabled = false;
                                cambiaBonificacion = false;
                            }
                            else
                            {
                                //De momento no se permite cambiar la bonificaci�n
                                txtBonificacion.Enabled = false;
                                cambiaBonificacion = false;
                            }
                            decimal cantidad = 0;
                            if (Decimal.TryParse(txtCantidad.Text, out cantidad))
                            {
                                if (cantidad != art.UnidadesAlmacen)
                                {
                                    txtCantidad.Text = GestorUtilitario.FormatDecimalAlmDet(art.UnidadesAlmacen, 3);
                                }
                            }
                            else
                            {
                                txtCantidad.Text = GestorUtilitario.FormatDecimalAlmDet(art.UnidadesAlmacen, 3);
                            }
                            decimal cantidadDet = 0;
                            if (Decimal.TryParse(txtCantidadDetalle.Text, out cantidadDet))
                            {
                                if (cantidadDet != art.UnidadesDetalle)
                                {
                                    txtCantidadDetalle.Text = GestorUtilitario.FormatDecimalAlmDet(art.UnidadesDetalle, 3);
                                }
                            }
                            else
                            {
                                txtCantidadDetalle.Text = GestorUtilitario.FormatDecimalAlmDet(art.UnidadesDetalle, 3);
                            }
                            if (!dolar)
                            {
                                txtPrecio.Text = "Alm:" + GestorUtilitario.FormatNumero(art.Articulo.PrecioMaximo) + "   Det:" + GestorUtilitario.FormatNumero(art.Articulo.PrecioMinimo);
                                txtSubtotal.Text = GestorUtilitario.FormatNumero(art.MontoTotal);
                                txtTotal.Text = GestorUtilitario.FormatNumero(art.MontoNeto);
                                if (art.Descuento != null)
                                {
                                    if (art.Descuento.Tipo == TipoDescuento.Porcentual)
                                    {
                                        decimal cantidadDesc = 0;
                                        if (Decimal.TryParse(txtDescuento.Text.Replace("%", ""), out cantidadDesc))
                                        {
                                            if (cantidadDesc != art.Descuento.Monto)
                                            {
                                                txtDescuento.Text = GestorUtilitario.FormatPorcentaje(art.Descuento.Monto);
                                            }
                                        }
                                        else
                                        {
                                            txtDescuento.Text = GestorUtilitario.FormatPorcentaje(art.Descuento.Monto);
                                        }

                                    }
                                    else
                                        txtDescuento.Text = GestorUtilitario.FormatNumero(art.Descuento.Monto);
                                }
                                else
                                {
                                    decimal cantidadDesc = 0;
                                    if (Decimal.TryParse(txtDescuento.Text.Replace("%", ""), out cantidadDesc))
                                    {
                                        if (cantidadDesc != 0)
                                        {
                                            txtDescuento.Text = txtDescuento.Text = GestorUtilitario.FormatPorcentaje(0);
                                        }
                                    }
                                    else
                                    {
                                        txtDescuento.Text = GestorUtilitario.FormatPorcentaje(0);
                                    }

                                }
                                if (art.LineaBonificada != null)
                                {
                                    txtBonificacion.Text = GestorUtilitario.FormatDecimalAlmDet(art.LineaBonificada.TotalAlmacen,3);
                                }
                                else
                                {
                                    txtBonificacion.Text = string.Empty;
                                }
                            }
                            else
                            {
                                txtPrecio.Text = "Alm:" + GestorUtilitario.FormatNumeroDolar(art.Articulo.PrecioMaximo) + "   Det:" + GestorUtilitario.FormatNumeroDolar(art.Articulo.PrecioMinimo);
                                txtSubtotal.Text = GestorUtilitario.FormatNumeroDolar(art.MontoTotal);
                                txtTotal.Text = GestorUtilitario.FormatNumeroDolar(art.MontoNeto);
                                if (art.Descuento != null)
                                {
                                    if (art.Descuento.Tipo == TipoDescuento.Porcentual)
                                    {
                                        decimal cantidadDesc = 0;
                                        if (Decimal.TryParse(txtDescuento.Text.Replace("%", ""), out cantidadDesc))
                                        {
                                            if (cantidadDesc != art.Descuento.Monto)
                                            {
                                                txtDescuento.Text = GestorUtilitario.FormatPorcentaje(art.Descuento.Monto);
                                            }
                                        }
                                        else
                                        {
                                            txtDescuento.Text = GestorUtilitario.FormatPorcentaje(art.Descuento.Monto);
                                        }
                                    }
                                    else
                                        txtDescuento.Text = GestorUtilitario.FormatNumeroDolar(art.Descuento.Monto);
                                }
                                else
                                {
                                    decimal cantidadDesc = 0;
                                    if (Decimal.TryParse(txtDescuento.Text.Replace("%", ""), out cantidadDesc))
                                    {
                                        if (cantidadDesc != 0)
                                        {
                                            txtDescuento.Text = txtDescuento.Text = GestorUtilitario.FormatPorcentaje(0);
                                        }
                                    }
                                    else
                                    {
                                        txtDescuento.Text = GestorUtilitario.FormatPorcentaje(0);
                                    }
                                }
                                if (art.LineaBonificada != null)
                                {
                                    txtBonificacion.Text = GestorUtilitario.FormatDecimalAlmDet(art.LineaBonificada.TotalAlmacen,3);
                                }
                                else
                                {
                                    txtBonificacion.Text = string.Empty;
                                }
                            }
                            var adapterBodegas = new ArrayAdapter<string>(this, Resource.Layout.SimpleSpinnerItem, listaBodegas);
                            adapterBodegas.SetDropDownViewResource(Resource.Layout.SimpleSpinnerDropDownItem);
                            cmbBodega.Adapter = adapterBodegas;
                            var adapterBodegasB = new ArrayAdapter<string>(this, Resource.Layout.SimpleSpinnerItem, listaBodegasBon);
                            adapterBodegasB.SetDropDownViewResource(Resource.Layout.SimpleSpinnerDropDownItem);
                            cmbBodegaBon.Adapter = adapterBodegasB;
                            if (art.Articulo.Bodega != null && !string.IsNullOrEmpty(art.Articulo.Bodega.Codigo))
                            {
                                cmbBodega.SetSelection(((ArrayAdapter)cmbBodega.Adapter).GetPosition(art.Articulo.Bodega.Codigo));
                            }
                            if (haybonificacion && art.LineaBonificada.Articulo.Bodega != null && !string.IsNullOrEmpty(art.LineaBonificada.Articulo.Bodega.Codigo))
                            {
                                cmbBodegaBon.SetSelection(((ArrayAdapter)cmbBodegaBon.Adapter).GetPosition(art.LineaBonificada.Articulo.Bodega.Codigo));
                            }

                        }
                        );
                        return true;
                    }
                    catch
                    {
                        RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error consultando el art�culo", ToastLength.Long).Show(); });
                        return false;
                    }
                });
            }
            catch (Exception ex)
            {
                string excep = ex.Message;
                 return Task.Run(() =>
                {
                    return false;
                });
            }
            
        }

        private Task<bool> ExecuteCambiarCantidad(decimal cantidad,decimal detalle)
        {

            return Task.Run(() =>
            {
                Android.App.ProgressDialog progressDialog = null;
                try
                {
                    RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(this, "Cambiando cantidad", "Por favor espere...", true); });
                    GestorPedido.AgregarModificarDetalle(GestorPedido.PedidosCollection.BuscarDetalleCodigo(codArticulos,App.CompaniaDefault).Articulo , cantidad,detalle);
                    //adapter.CambiarCantidad(position, cantidad);
                    RunOnUiThread(() => { progressDialog.Dismiss(); });

                    return true;
                }
                catch
                {
                    RunOnUiThread(() => { progressDialog.Dismiss(); });
                    RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error al actualizar cantidad.", ToastLength.Short).Show(); });
                    return false;
                }
            });
        }

        private Task<bool> ExecuteCambiarDescuento(decimal porcentaje)
        {

            return Task.Run(() =>
            {
                Android.App.ProgressDialog progressDialog = null;
                try
                {
                    if (!PedidosCollection.CambiarDescuento)
                    {
                        return false;
                    }
                    if (porcentaje < 0 || porcentaje > 100)
                    {
                        RunOnUiThread(() => { Toast.MakeText(this, "El porcentaje de descuento debe de estar entre 0 y 100", ToastLength.Short).Show(); });                        
                        return false;
                    }
                    RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(this, "Cambiando Descuento", "Por favor espere...", true); });
                    if(GestorPedido.PedidosCollection.BuscarDetalleCodigo(codArticulos,App.CompaniaDefault).LineaBonificada!=null)
                    {
                        GestorPedido.CambiosBonificacionDescuento(GestorPedido.PedidosCollection.BuscarDetalleCodigo(codArticulos, App.CompaniaDefault).Articulo, porcentaje, GestorPedido.PedidosCollection.BuscarDetalleCodigo(codArticulos, App.CompaniaDefault).LineaBonificada.TotalAlmacen, 0);
                    }
                    else
                    {
                        GestorPedido.CambiosBonificacionDescuento(GestorPedido.PedidosCollection.BuscarDetalleCodigo(codArticulos, App.CompaniaDefault).Articulo, porcentaje, 0, 0);
                    }
                    RunOnUiThread(() => { progressDialog.Dismiss(); });

                    return true;
                }
                catch
                {
                    RunOnUiThread(() => { progressDialog.Dismiss(); });
                    RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error al actualizar cantidad.", ToastLength.Short).Show(); });
                    return false;
                }
            });
        }      

        private Task<bool> ExecuteConsultarExistencia(string codbodega)
        {

            return Task.Run(() =>
            {
                try
                {
                    Connection cnxArticulo = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                    int index = GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista.FindIndex(x => x.Articulo.Codigo == codArticulos);
                    GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista[index].Articulo.CargarExistenciaPedido(codbodega, cnxArticulo);                   
                    cnxArticulo.Close();
                    cnxArticulo = null;
                    var lista = new List<string>() { "ND" };
                    if (usaLocalizacion)
                    {
                    
                        cnxArticulo = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                        lista = GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista[index].Articulo.Bodega.Obtenerlocalizaciones(App.CompaniaDefault, codArticulos, cnxArticulo);                        
                        cnxArticulo.Close();
                        cnxArticulo = null;
                        RunOnUiThread(() =>
                        {
                            cmdLocalizaciones.Enabled = true;
                            var adapter = new ArrayAdapter<string>(this, Resource.Layout.SimpleSpinnerItem, lista);
                            adapter.SetDropDownViewResource(Resource.Layout.SimpleSpinnerDropDownItem);
                            cmdLocalizaciones.Adapter = adapter;
                            if (GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista[index].Articulo.Bodega != null && !string.IsNullOrEmpty(GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista[index].Articulo.Bodega.Localizacion))
                            {
                                cmdLocalizaciones.SetSelection(((ArrayAdapter)cmdLocalizaciones.Adapter).GetPosition(GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista[index].Articulo.Bodega.Localizacion));
                            }
                        });
                    }
                    else
                    {
                         RunOnUiThread(() =>
                         { cmdLocalizaciones.Enabled = false;
                         var adapter = new ArrayAdapter<string>(this, Resource.Layout.SimpleSpinnerItem, lista);
                         adapter.SetDropDownViewResource(Resource.Layout.SimpleSpinnerDropDownItem);
                         cmdLocalizaciones.Adapter = adapter;
                         });
                        
                       
                    }
                    RunOnUiThread(() =>
                    {
                        txtExistencia.Text = " " + GestorUtilitario.FormatDecimalAlmDet(GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista[index].Articulo.Bodega.Existencia, 3);
                    }
                    );
                    return true;
                }
                catch
                {
                    RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error consultando existencias para la bodega "+codbodega+".", ToastLength.Long).Show(); });
                    return false;
                }
            });
        }

        private Task<bool> ExecuteConsultarLocalizacion(string codLocalizacion)
        {

            return Task.Run(() =>
            {
                try
                {
                    
                    int index = GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista.FindIndex(x => x.Articulo.Codigo == codArticulos);
                    GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista[index].Articulo.Bodega.Localizacion=codLocalizacion;
                   
                    return true;
                }
                catch
                {
                    RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error asignando localizaci�n.", ToastLength.Long).Show(); });
                    return false;
                }
            });
        }

        private Task<bool> ExecuteConsultarLocalizacionBon(string codLocalizacion)
        {

            return Task.Run(() =>
            {
                try
                {
                    if (!haybonificacion)
                        return true;
                    int index = GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista.FindIndex(x => x.Articulo.Codigo == codArticulos);
                    GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista[index].LineaBonificada.Articulo.Bodega.Localizacion = codLocalizacion;

                    return true;
                }
                catch
                {
                    RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error asignando localizaci�n.", ToastLength.Long).Show(); });
                    return false;
                }
            });
        }

        private Task<bool> ExecuteConsultarExistenciaBonificacion(string codbodega)
        {

            return Task.Run(() =>
            {
                try
                {
                    if (!haybonificacion)
                        return true;
                    Connection cnxArticulo = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));                    
                    int index = GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista.FindIndex(x => x.Articulo.Codigo == codArticulos);
                    GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista[index].LineaBonificada.Articulo.CargarExistenciaPedido(codbodega, cnxArticulo);
                    cnxArticulo.Close();
                    cnxArticulo = null;
                    var lista = new List<string>() { "ND" };
                    if (usaLocalizacion)
                    {
                      
                        cnxArticulo = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName)); 
                        lista = GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista[index].Articulo.Bodega.Obtenerlocalizaciones(App.CompaniaDefault, GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista[index].Articulo.Codigo, cnxArticulo);
                        cnxArticulo.Close();
                        cnxArticulo = null;
                        RunOnUiThread(() =>
                        {
                            cmdLocalizacionesBon.Enabled = true;
                            var adapter = new ArrayAdapter<string>(this, Resource.Layout.SimpleSpinnerItem, lista);
                            adapter.SetDropDownViewResource(Resource.Layout.SimpleSpinnerDropDownItem);
                            cmdLocalizacionesBon.Adapter = adapter;
                            if (GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista[index].LineaBonificada.Articulo.Bodega != null && !string.IsNullOrEmpty(GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista[index].LineaBonificada.Articulo.Bodega.Localizacion))
                            {
                                cmdLocalizacionesBon.SetSelection(((ArrayAdapter)cmdLocalizacionesBon.Adapter).GetPosition(GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista[index].LineaBonificada.Articulo.Bodega.Localizacion));
                            }
                        });
                    }
                    else
                    {
                        RunOnUiThread(() =>
                        { cmdLocalizacionesBon.Enabled = false;
                        var adapter = new ArrayAdapter<string>(this, Resource.Layout.SimpleSpinnerItem, lista);
                        adapter.SetDropDownViewResource(Resource.Layout.SimpleSpinnerDropDownItem);
                        cmdLocalizacionesBon.Adapter = adapter;
                        });
                       
                    } 
                    RunOnUiThread(() =>
                    {
                        txtExistenciaBon.Text = " " + GestorUtilitario.FormatDecimalAlmDet(GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista[index].LineaBonificada.Articulo.Bodega.Existencia, 3);
                    }
                    );
                    return true;
                }
                catch
                {
                    RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error consultando existencias para la bodega " + codbodega + ".", ToastLength.Long).Show(); });
                    return false;
                }
            });
        }



        #endregion


    }
}