using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Pedidos.Core;
using Android.Bluetooth;

namespace Softland.Droid.PEL
{
    [Activity(Label = "Opciones impresi�n", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, ConfigurationChanges = Android.Content.PM.ConfigChanges.KeyboardHidden | Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
    public class ImpresionActivity : BaseActivity
    {
        CheckBox chkOriginal, chkCopias;
        Button btnImprimir;
        EditText txtCopias;
        TextView txtTitulo;
        string error;

        #region propiedades

        public static Action<bool, int, DetalleSort.Ordenador,Activity> OnImprimir { private get; set; }

        #endregion

        #region lifecycle

        protected override int LayoutResource
        {
            get
            {
                return Resource.Layout.impresion;
            }
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            chkOriginal = FindViewById<CheckBox>(Resource.Id.chkOriginal);
            chkCopias = FindViewById<CheckBox>(Resource.Id.chkCopias);
            btnImprimir = FindViewById<Button>(Resource.Id.btnImprimir);
            txtCopias = FindViewById<EditText>(Resource.Id.txtCopias);
            txtTitulo = FindViewById<TextView>(Resource.Id.txtTitulo);

            try
            {
                this.CargaInicial();
                this.AsignarEventos();
                error = string.Empty;
            }
            catch (Exception ex)
            {
                error = "Ocurri� un error al cargar la pantalla." + ex.Message;
            }
        }

        protected override void OnStart()
        {
            base.OnStart();
        }

        protected override void OnResume()
        {
            base.OnResume();
        }

        protected override void OnPause()
        {
            base.OnPause();
        }

        #endregion

        #region eventos

        private void AsignarEventos() 
        {
            btnImprimir.Click += btnImprimir_Click;
        }

        void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                this.ImprimirDocumento();
            }
            catch (Exception ex)
            {
                Toast.MakeText(Android.App.Application.Context,"Ocurri� un error al imprimir el documento." + ex.Message,ToastLength.Short).Show();

            }
        }

        public override void OnBackPressed()
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            //alert.SetTitle("Configuraci�n Impresi�n");
            alert.SetMessage("�Desea cancelar la impresi�n?");
            alert.SetPositiveButton("Si", delegate { this.ImprimirDocumentoCancelar(); return; });
            alert.SetNegativeButton("No", delegate { return; });
            RunOnUiThread(() => { alert.Show(); });
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.print, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    {
                        OnBackPressed();
                        break;
                    }
                case Resource.Id.menu_print:
                    {
                        var myActivity = this;
                        bool encendiendo = false;
                        List<string> ImpresorasList = new List<string>();
                        List<double> papers = new List<double> { 2, 2.5, 3 };
                        //Validaci�n
                        if (encendiendo)
                        {
                            encendiendo = false;
                        }
                        else if (BluetoothAdapter.DefaultAdapter == null)
                        {
                            Toast.MakeText(Android.App.Application.Context, "El dispositivo no soporta bluetooth", ToastLength.Long).Show();
                            break;
                        }
                        else if (!BluetoothAdapter.DefaultAdapter.IsEnabled)
                        {
                            //.
                            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(myActivity);
                            alert.SetTitle("Bluetooth Desactivado");
                            alert.SetMessage("Activar el bluetooth para seleccionar la impresora?");
                            alert.SetPositiveButton("Si", delegate
                            {
                                BluetoothAdapter.DefaultAdapter.Enable();
                                encendiendo = true;
                                Toast.MakeText(Android.App.Application.Context, "Bluetooth encendido", ToastLength.Long).Show();
                            });
                            alert.SetNegativeButton("No", delegate
                            {
                                return;
                            });
                            myActivity.RunOnUiThread(() => { alert.Show(); });
                            //.

                        }
                        else
                        {
                            ImpresorasList = new List<string>();
                            if (BluetoothAdapter.DefaultAdapter.BondedDevices.Count > 0)
                            {
                                ICollection<BluetoothDevice> btd = BluetoothAdapter.DefaultAdapter.BondedDevices;
                                foreach (BluetoothDevice d in btd)
                                {
                                    ImpresorasList.Add(d.Name);
                                }
                            }
                            else
                            {
                                Toast.MakeText(Android.App.Application.Context, "No hay ning�n dispositivo vinculado", ToastLength.Long).Show();
                                break;
                            }
                            Android.App.Dialog builderClas = new Android.App.Dialog(myActivity);
                            builderClas.Window.RequestFeature(WindowFeatures.NoTitle);
                            builderClas.SetContentView(Resource.Layout.dialog_impresoras);
                            Spinner cmbPapeles, cmbImpresoras, cmbTipos;
                            var label1 = builderClas.FindViewById<TextView>(Resource.Id.label1);
                            var label2 = builderClas.FindViewById<TextView>(Resource.Id.label2);
                            var label3 = builderClas.FindViewById<TextView>(Resource.Id.label3);
                            var buttonAceptar = builderClas.FindViewById<Button>(Resource.Id.btnAceptar);
                            var chkSugerir = builderClas.FindViewById<CheckBox>(Resource.Id.chkSugerir);
                            label1.SetTextColor(Android.Graphics.Color.White);
                            label2.SetTextColor(Android.Graphics.Color.White);
                            label3.SetTextColor(Android.Graphics.Color.White);
                            chkSugerir.SetTextColor(Android.Graphics.Color.White);
                            cmbPapeles = builderClas.FindViewById<Spinner>(Resource.Id.cmbPapel);
                            cmbImpresoras = builderClas.FindViewById<Spinner>(Resource.Id.cmbImpresora);
                            cmbTipos = builderClas.FindViewById<Spinner>(Resource.Id.cmbTipo);
                            var adapter = new ArrayAdapter<string>(myActivity, Resource.Layout.SimpleSpinnerItemWhite, ImpresorasList);
                            adapter.SetDropDownViewResource(Resource.Layout.SimpleSpinnerDropDownItem);
                            cmbImpresoras.Adapter = adapter;
                            var adapterPapels = new ArrayAdapter<Double>(myActivity, Resource.Layout.SimpleSpinnerItemWhite, papers);
                            adapterPapels.SetDropDownViewResource(Resource.Layout.SimpleSpinnerDropDownItem);
                            cmbPapeles.Adapter = adapterPapels;
                            var adapterTipos = new ArrayAdapter<string>(myActivity, Resource.Layout.SimpleSpinnerItemWhite, new List<string> { "Generica", "Sewoo(LK-P30)", "Zebra(MZ-iMZ)" });
                            adapterTipos.SetDropDownViewResource(Resource.Layout.SimpleSpinnerDropDownItem);
                            cmbTipos.Adapter = adapterTipos;
                            if (!string.IsNullOrEmpty(FRmConfig.Impresora) && (BluetoothAdapter.DefaultAdapter.BondedDevices.Count > 0))
                            {
                                if (BluetoothAdapter.DefaultAdapter != null && BluetoothAdapter.DefaultAdapter.IsEnabled)
                                {
                                    if (BluetoothAdapter.DefaultAdapter.BondedDevices.Count > 0)
                                    {
                                        foreach (string str in ImpresorasList)
                                        {
                                            if (str.Equals(FRmConfig.Impresora))
                                            {
                                                cmbImpresoras.SetSelection(((ArrayAdapter<string>)cmbImpresoras.Adapter).GetPosition(FRmConfig.Impresora));
                                            }
                                        }
                                    }
                                }
                            }
                            chkSugerir.Checked = Impresora.SugerirImprimir;
                            chkSugerir.CheckedChange += (s, e) => { Impresora.SugerirImprimir = chkSugerir.Checked; };
                            cmbTipos.SetSelection(((ArrayAdapter<string>)cmbTipos.Adapter).GetPosition(FRmConfig.TipoImpresora));
                            cmbPapeles.SetSelection(((ArrayAdapter<double>)cmbPapeles.Adapter).GetPosition(FRmConfig.Tama�oPapel));
                            cmbImpresoras.ItemSelected += (e, s) => { FRmConfig.Impresora = cmbImpresoras.SelectedItem.ToString(); };
                            cmbTipos.ItemSelected += (e, s) => { FRmConfig.TipoImpresora = cmbTipos.SelectedItem.ToString(); };
                            cmbPapeles.ItemSelected += (e, s) => { FRmConfig.Tama�oPapel = Convert.ToDouble(cmbPapeles.SelectedItem.ToString()); };
                            buttonAceptar.Click += (e, s) =>
                            {
                                try
                                {
                                    string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                                    string file = System.IO.Path.Combine(folder, "PrintPaperPreferences.txt");
                                    using (System.IO.StreamWriter st = new System.IO.StreamWriter(file, false))
                                    {
                                        st.Write(FRmConfig.Tama�oPapel.ToString());
                                    };
                                    folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                                    file = System.IO.Path.Combine(folder, "PrintNamePreferences.txt");
                                    using (System.IO.StreamWriter st = new System.IO.StreamWriter(file, false))
                                    {
                                        st.Write(FRmConfig.Impresora.ToString());
                                    };
                                    folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                                    file = System.IO.Path.Combine(folder, "PrintTypePreferences.txt");
                                    using (System.IO.StreamWriter st = new System.IO.StreamWriter(file, false))
                                    {
                                        st.Write(FRmConfig.TipoImpresora.ToString());
                                    };
                                    folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                                    file = System.IO.Path.Combine(folder, "PrintSugerirPreferences.txt");
                                    using (System.IO.StreamWriter st = new System.IO.StreamWriter(file, false))
                                    {
                                        if (Impresora.SugerirImprimir)
                                        {
                                            st.Write("SI");
                                        }
                                        else
                                        {
                                            st.Write("NO");
                                        }

                                    };
                                    Toast.MakeText(Android.App.Application.Context, "Preferencias de impresi�n guardadas.", ToastLength.Long).Show();
                                    builderClas.Dismiss();
                                }
                                catch
                                {
                                    Toast.MakeText(Android.App.Application.Context, "Error al guardar las preferencias de impresi�n.", ToastLength.Long).Show();
                                    builderClas.Dismiss();
                                }
                            };
                            builderClas.Window.SetBackgroundDrawableResource(Resource.Color.semitransparent);
                            builderClas.Show(); break;
                        }
                        break;
                    }
                default: break;
            }
            return base.OnOptionsItemSelected(item);
        }



        #endregion

        #region m�todos

        /// <summary>
        /// Carga la pantalla inicial
        /// </summary>
        private void CargaInicial() 
        {
            txtTitulo.Text = Intent.GetStringExtra("titulo");
            if (string.IsNullOrEmpty(txtTitulo.Text))
                txtTitulo.Text = "Impresi�n";
            if (Intent.GetBooleanExtra("original", false))
            {
                chkOriginal.Checked = true;
                chkCopias.Checked = false;
                chkOriginal.Visibility = ViewStates.Visible;
                txtCopias.Text = "0";
            }
            else
            {
                chkOriginal.Checked = false;
                chkCopias.Checked = true;
                chkOriginal.Visibility = ViewStates.Gone;
                txtCopias.Text = "1";
            }           

        }

        /// <summary>
        /// Invoca la impresion correspondiente 
        /// </summary>
        private void ImprimirDocumento()
        {
            OnImprimir(chkOriginal.Checked, Convert.ToInt16(txtCopias.Text), DetalleSort.Ordenador.Codigo,this);
        }

        private void ImprimirDocumentoCancelar()
        {          
            OnImprimir(false,0, DetalleSort.Ordenador.Codigo, this);
        }

        #endregion
    }
}