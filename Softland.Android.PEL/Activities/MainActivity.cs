﻿using Android.App;
using Android.Content.PM;
using Android.Content.Res;
using Android.OS;
using Android.Views;
using Android.Widget;
//using Softland.Droid.PEL.Helpers;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Content;
using Pedidos.Core;

namespace Softland.Droid.PEL
{
    [Activity(Label = "Pedidos Móvil", MainLauncher = false, Icon = "@drawable/ic_launcher", ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
    public class MainActivity : BaseActivity
    {

        private MyActionBarDrawerToggle drawerToggle;
        private string drawerTitle;
        private string title;
        private static int currentFragment = 0;

        //Constantes
        public const int FragmentClientes=0;
        const int FragmentArticulos=1;
        const int FragmentPedidos = 2;
        const int FragmentPendientes = 3;
        const int FragmentOpciones = 4;
        

        private DrawerLayout drawerLayout;
        private ListView drawerListView;
        private static readonly string[] Sections = new[] {
			"Clientes", "Artículos", "Pedidos","No sincronizados","Opciones"
		};

        protected override int LayoutResource
        {
            get
            {
                return Resource.Layout.Main;
            }
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            this.title = this.drawerTitle = this.Title;

            this.drawerLayout = this.FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            this.drawerListView = this.FindViewById<ListView>(Resource.Id.left_drawer);


            //Create Adapter for drawer List
            this.drawerListView.Adapter = new ArrayAdapter<string>(this, Resource.Layout.item_menu, Sections);

            //Set click handler when item is selected
            this.drawerListView.ItemClick += (sender, args) => ListItemClicked(args.Position);

            //Set Drawer Shadow
            this.drawerLayout.SetDrawerShadow(Resource.Drawable.drawer_shadow_dark, (int)GravityFlags.Start);

            //DrawerToggle is the animation that happens with the indicator next to the actionbar
            this.drawerToggle = new MyActionBarDrawerToggle(this, this.drawerLayout,
                this.Toolbar,
                Resource.String.drawer_open,
                Resource.String.drawer_close);

            //Display the current fragments title and update the options menu
            this.drawerToggle.DrawerClosed += (o, args) =>
            {
                this.SupportActionBar.Title = this.title;
                this.InvalidateOptionsMenu();
            };

            //Display the drawer title and update the options menu
            this.drawerToggle.DrawerOpened += (o, args) =>
            {
                this.SupportActionBar.Title = this.drawerTitle;
                this.InvalidateOptionsMenu();
            };

            //Set the drawer lister to be the toggle.
            this.drawerLayout.SetDrawerListener(this.drawerToggle);



            //if first time you will want to go ahead and click first item.
            if (savedInstanceState == null)
            {
                ListItemClicked(0);
            }

            //Inicia el servicio de sincronizacion
            if (!SyncService.isRunnig)
            {
                StartService(new Intent(this, typeof(SyncService)));
            }                
        }

        public void ListItemClicked(int position)
        {
            Android.Support.V4.App.Fragment fragment = null;
            if (string.IsNullOrEmpty(App.CompaniaDefault))
            {
                fragment = new OpcionesFragment();

                position = FragmentOpciones;

                SupportActionBar.Title = this.title = Sections[FragmentOpciones];
            }
            else
            {
                switch (position)
                {
                    case FragmentClientes:
                        fragment = new ClientesFragment();
                        break;
                    case FragmentArticulos:
                        fragment = new ArticulosPageFragment();
                        break;
                    case FragmentPedidos:
                        //if (string.IsNullOrEmpty(App.ConsecutivoPedido))
                        //{
                        //    Toast.MakeText(Android.App.Application.Context, "No puede hacer pedidos hasta asignar el consecutivo.", ToastLength.Long).Show();
                        //    ListItemClicked(MainActivity.FragmentOpciones);
                        //    return;
                        //}
                        fragment = new PedidosFragment();

                        //if (GestorPedido.PedidosCollection != null && GestorPedido.PedidosCollection.Gestionados.Count>0)
                        //{                            
                        //    //this.drawerListView.SetItemChecked(position, true);
                        //    //SupportActionBar.Title = this.title = Sections[position];

                        //    SupportActionBar.Title = this.title = Sections[position];

                        //    SupportFragmentManager.BeginTransaction()
                        //        .Replace(Resource.Id.content_frame, fragment).Commit();

                        //    this.drawerListView.SetItemChecked(position, true);

                        //    this.drawerLayout.CloseDrawers();
                        //    Toast.MakeText(Android.App.Application.Context, "Continuando pedido guardado.", ToastLength.Short).Show();                            
                        //    Intent intent = new Intent(this, typeof(PedidoMainActivity));
                        //    StartActivity(intent);                            
                        //    return;
                        //}
                        break;
                    case FragmentPendientes:
                        fragment = new PendientesFragment();
                        break;
                    case FragmentOpciones:
                        fragment = new OpcionesFragment();

                        break;
                }

                SupportActionBar.Title = this.title = Sections[position];
            }                       

            SupportFragmentManager.BeginTransaction()
                .Replace(Resource.Id.content_frame, fragment).Commit();

            this.drawerListView.SetItemChecked(position, true);

            //if (!(GestorPedido.PedidosCollection != null && GestorPedido.PedidosCollection.Gestionados.Count > 0))
            //{
            //    SupportActionBar.Title = this.title = Sections[position];
            //} 

            this.drawerLayout.CloseDrawers();

            if (position == FragmentPedidos && GestorPedido.PedidosCollection != null && GestorPedido.PedidosCollection.Gestionados.Count > 0)
            {
                this.drawerLayout.CloseDrawers();
                Toast.MakeText(Android.App.Application.Context, "Continuando pedido guardado.", ToastLength.Short).Show();
                Intent intent = new Intent(this, typeof(PedidoMainActivity));
                StartActivity(intent);
                return;
            }
        }

        public override bool OnPrepareOptionsMenu(IMenu menu)
        {

            var drawerOpen = this.drawerLayout.IsDrawerOpen((int)GravityFlags.Left);
            //when open don't show anything
            for (int i = 0; i < menu.Size(); i++)
                menu.GetItem(i).SetVisible(!drawerOpen);
         
            return base.OnPrepareOptionsMenu(menu);
        }

        protected override void OnPostCreate(Bundle savedInstanceState)
        {
            base.OnPostCreate(savedInstanceState);
            this.drawerToggle.SyncState();
        }

        public override void OnConfigurationChanged(Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);
            this.drawerToggle.OnConfigurationChanged(newConfig);
        }

        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (this.drawerToggle.OnOptionsItemSelected(item))
                return true;

            return base.OnOptionsItemSelected(item);
        }

        public override void OnBackPressed()
        {
            switch (currentFragment)
            {
                case FragmentClientes: this.MoveTaskToBack(true); break;
                case FragmentArticulos: this.MoveTaskToBack(true); break;
                case FragmentPedidos: this.MoveTaskToBack(true); break;
                case FragmentOpciones: this.MoveTaskToBack(true); break;
                case FragmentPendientes: this.MoveTaskToBack(true); break;
                default: base.OnBackPressed(); break;
            }
        }        

    }
}

