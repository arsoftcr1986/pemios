using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Data.SQLite;
using BI.Android;
using Pedidos.Core;
using Android.Util;

namespace Softland.Droid.PEL
{
    [Activity(Label = "Consulta Cliente", Icon = "@drawable/ic_launcher", ParentActivity = typeof(MainActivity), ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
    public class ConsulClienteActivity : BaseActivity
    {
        string codCliente;
        ImageButton btnMapa;
        TextView txtCodigo, txtNombre, txtCedulaJuridica, txtTelefono, txtDirecci�n;

        protected override int LayoutResource
        {
            get
            {
                return Resource.Layout.consulta_cliente;
            }
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            btnMapa = FindViewById<ImageButton>(Resource.Id.btnMapa);
            txtCodigo = FindViewById<TextView>(Resource.Id.txtCodigo);
            txtNombre = FindViewById<TextView>(Resource.Id.txtNombre);
            txtTelefono = FindViewById<TextView>(Resource.Id.txtTelefono);
            txtDirecci�n = FindViewById<TextView>(Resource.Id.txtDirecci�n);
            txtCedulaJuridica = FindViewById<TextView>(Resource.Id.txtCedulaJuridica);
            // Create your application here   
            CargaInicial();
        }

        public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);

            if (newConfig.Orientation == Android.Content.Res.Orientation.Landscape)
            {
                base.setContentViewReload(Resource.Layout.consulta_cliente);
            }
            else
            {
                base.setContentViewReload(Resource.Layout.consulta_cliente);
            }

            btnMapa = FindViewById<ImageButton>(Resource.Id.btnMapa);
            txtCodigo = FindViewById<TextView>(Resource.Id.txtCodigo);
            txtNombre = FindViewById<TextView>(Resource.Id.txtNombre);
            txtTelefono = FindViewById<TextView>(Resource.Id.txtTelefono);
            txtDirecci�n = FindViewById<TextView>(Resource.Id.txtDirecci�n);
            txtCedulaJuridica = FindViewById<TextView>(Resource.Id.txtCedulaJuridica);

            CargaInicial();
        }

        private async void CargaInicial()
        {
            codCliente = Intent.GetStringExtra("codigo");
            bool result = await ExecuteConsultar();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.consulta_cliente, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    {
                        OnBackPressed();
                        break;
                    }
                case Resource.Id.action_settings:
                    {
                        Intent intent = new Intent(this, typeof(DocumentosActivity));
                        intent.PutExtra("codigo", codCliente);
                        StartActivity(intent);
                        break;
                    }
            }
            return true;
        }

        public override void OnBackPressed()
        {
            var intent = new Intent(this, typeof(MainActivity));
            intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
            this.StartActivity(intent);
            Finish();
        }
        

        #region metodos

        private Task<bool> ExecuteMap()
        {

            return Task.Run(() =>
            {
                return true;
            });
        }

        private Task<bool> ExecuteConsultar()
        {
            Log.Info("rastreo", "ConsulClienteActivity ExecuteConsultar");
            return Task.Run(() =>
            {
                try
                {
                    Cliente cli=Cliente.CargarClientes(new BI.Shared.CriterioBusquedaCliente(BI.Shared.CriterioCliente.Codigo, codCliente, true, false), App.CompaniaDefault).Find(x=>x.Codigo.Equals(codCliente));                    
                    cli.ObtenerClientesCia();
                    string telefono = !string.IsNullOrWhiteSpace(cli.ClienteCompania[0].Telefono) ? cli.ClienteCompania[0].Telefono : "No disponible.";
                    RunOnUiThread(() =>
                        {
                            txtCodigo.Text = cli.Codigo;
                            txtNombre.Text = cli.Nombre;
                            txtDirecci�n.Text = cli.Direccion;

                            txtCedulaJuridica.Text = !cli.ClienteCompania[0].Contribuyente.Contains("ND") ? cli.ClienteCompania[0].Contribuyente : "No disponible.";

                            txtTelefono.Text = telefono;
                        }
                    );
                    return true;
                }
                catch (Exception ex)
                {
                    RunOnUiThread(()=>{Toast.MakeText(Android.App.Application.Context, "Error consultando el cliente",ToastLength.Long).Show();});
                    return false;
                }
            });
        }



        #endregion


    }
}