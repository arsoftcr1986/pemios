using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Data.SQLite;

using BI.Android;
using BI.Shared;

using Pedidos.Core;
using Android.Views.InputMethods;

namespace Softland.Droid.PEL
{
    [Activity(Label = "Pedidos M�vil", MainLauncher = false, Icon = "@drawable/ic_launcher", ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
    public class LoginActiity : BaseActivity
    {
        EditText txtUsuario, txtPassword;
        Button btnAceptar;
        string usuario, password;
        bool configchanges = false;

        protected override int LayoutResource
        {
            get
            {
                return Resource.Layout.inicio_sesion;
            }
        }

        public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);
            usuario = txtUsuario.Text.ToString();
            password = txtPassword.Text.ToString();
            if (newConfig.Orientation == Android.Content.Res.Orientation.Landscape)
            {
                base.setContentViewReload(Resource.Layout.inicio_sesion);
            }
            else
            {
                base.setContentViewReload(Resource.Layout.inicio_sesion);
            }
            txtUsuario = FindViewById<EditText>(Resource.Id.txtUser);
            txtPassword = FindViewById<EditText>(Resource.Id.txtPass);
            btnAceptar = FindViewById<Button>(Resource.Id.btnAceptar);

            btnAceptar.Click += btnAceptar_Click;

            txtUsuario.Text = usuario;
            txtPassword.Text = password;
        }

        protected override void OnResume()
        {
            base.OnResume();

        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here
            txtUsuario = FindViewById<EditText>(Resource.Id.txtUser);
            txtPassword = FindViewById<EditText>(Resource.Id.txtPass);
            btnAceptar = FindViewById<Button>(Resource.Id.btnAceptar);

            txtPassword.EditorAction += txtPassword_EditorAction;

            btnAceptar.Click += btnAceptar_Click;

            LoadPreferences();
        }

        /// <summary>
        /// Prop�sito:
        /// Este Evento se cre� para controlar cuando el usuario selecciona enviar al ingresar la contrase�a.
        /// 
        /// Creado por: Sergio N��ez Gonz�lez 23/10/2015.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPassword_EditorAction(object sender, TextView.EditorActionEventArgs e)
        {
            e.Handled = false;

            if (e.ActionId == ImeAction.Send)
            {
                btnAceptar_Click(sender, null);
                e.Handled = true;
            }
        }

        private bool AutenticarUsuario(string usuario, string password)
        {
            int intentosPermitidos;
            try
            {
                // Como el usuario cambi�, se vuelve a obtener los intentos permitidos.
                intentosPermitidos = GestorSeguridad.VerificarUsuario(usuario);
                //intentosPermitidos = 3;
            }
            catch
            {
                Toast.MakeText(this, "Error verificando el Usuario.", ToastLength.Short).Show();
                return false;
            }


            // Si el usuario no tiene definido el valor de intentos permitidos.
            if (intentosPermitidos == -1)
            {
                Toast.MakeText(this, "Usuario inv�lido", ToastLength.Short).Show();
                txtUsuario.RequestFocus();
                txtUsuario.SelectAll();
                return false;
            }

            // Si el usuario ya no puede seguir intentando ingresar a la aplicaci�n.
            if (intentosPermitidos == 0)
            {
                Toast.MakeText(this, "Este usuario no puede ingresar al sistema", ToastLength.Short).Show();
                return false;
            }
            bool autenticado;
            try
            {
                // Se autentifica los datos del usuario y se obtiene si es un son v�lidos.
                autenticado = GestorSeguridad.AutenticarUsuario(usuario, password);
                autenticado = true;
                if (!autenticado)
                {
                    Toast.MakeText(this, "Contrase�a inv�lida", ToastLength.Short).Show();
                    txtPassword.RequestFocus();
                    txtPassword.SelectAll();
                    return false;
                }
            }
            catch
            {
                Toast.MakeText(this, "Error autenticando Usuario", ToastLength.Short).Show();
                return false;
            }
            if (autenticado)
            {
                App.Usuario = txtUsuario.Text.ToUpper();

                // Se asignan los valores globales de nombre de usuario y contrase�a.
                BI.Android.GestorDatos.NombreUsuario = txtUsuario.Text.ToUpper();
                BI.Android.GestorDatos.ContrasenaUsuario = txtPassword.Text.ToUpper();
            }
            return autenticado;
        }

        async void btnAceptar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(App.Servidor))
            {
                Toast.MakeText(this, "Configure la url del servidor", ToastLength.Short).Show();
                return;
            }
            if (string.IsNullOrEmpty(txtUsuario.Text))
            {
                Toast.MakeText(this, "Digite el usuario", ToastLength.Short).Show();
                return;
            }
            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                Toast.MakeText(this, "Digite la contrase�a", ToastLength.Short).Show();
                return;
            }
            if (App.VerificarExistenciaArchivo(System.IO.Path.Combine(App.PathAplicacion, App.BDName)))
            {
                try
                {
                    if (Pedidos.Core.GestorDatos.cnx != null && Pedidos.Core.GestorDatos.cnx.isOpen)
                    {
                        Pedidos.Core.GestorDatos.cnx.Close();
                        Pedidos.Core.GestorDatos.cnx = null;
                    }
                    Pedidos.Core.GestorDatos.cnx = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                    Pedidos.Core.GestorDatos.cnx.Open();
                    if (!AutenticarUsuario(txtUsuario.Text.ToUpper(), txtPassword.Text.ToUpper()))
                    {
                        return;
                    }
                    GlobalUI.Rutas = Ruta.ObtenerRutas();
                    GlobalUI.CargaParametrosX(false);
                    GlobalUI.ValidarConfiguracion();
                    SavePreferences();
                    LoadPreferencesGlobal();
                    CopiarReportes();
                    Intent intent = new Intent(this, typeof(MainActivity));
                    StartActivity(intent);
                }
                catch
                {
                    Toast.MakeText(this, "Los par�metros no se pudieron cargar.", ToastLength.Short).Show();
                }

            }
            else
            {
                if (string.IsNullOrEmpty(App.Servidor))
                {
                    Toast.MakeText(this, "Configure el servidor..", ToastLength.Short).Show();
                    return;
                }
                btnAceptar.Enabled = false;
                App.Usuario = txtUsuario.Text.ToUpper();
                string result = await ejecutarBD();
                btnAceptar.Enabled = true;
                Toast.MakeText(Application.Context, result, ToastLength.Long).Show();
            }

        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.login, menu);

            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.action_settings:
                    {
                        Dialog builder = new Dialog(this);
                        builder.Window.RequestFeature(WindowFeatures.NoTitle);
                        builder.SetContentView(Resource.Layout.dialog_servidor_login);
                        EditText txtServidor = builder.FindViewById<EditText>(Resource.Id.txtServidor);
                        Button btnAceptar = builder.FindViewById<Button>(Resource.Id.btnAceptar);
                        Button btnSincronizar = builder.FindViewById<Button>(Resource.Id.btnSincronizar);

                        if (string.IsNullOrEmpty(App.Servidor))
                        {
                            btnSincronizar.Enabled = false;
                        }
                        else
                        {
                            btnSincronizar.Enabled = true;
                        }

                        btnAceptar.Click += (sender, e) =>
                        {
                            if (string.IsNullOrEmpty(txtServidor.Text))
                            {
                                Toast.MakeText(Application.Context, "Debe digitar el servidor!", ToastLength.Short).Show();
                            }
                            else
                            {
                                App.Servidor = txtServidor.Text;

                                if (!App.Servidor.Contains("http://"))
                                {
                                    App.Servidor = "http://" + App.Servidor;
                                }
                                try
                                {
                                    string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                                    string file = System.IO.Path.Combine(folder, "ServidorPreferences.txt");
                                    using (System.IO.StreamWriter st = new System.IO.StreamWriter(file, false))
                                    {
                                        st.Write(App.Servidor.ToString());
                                    };
                                }
                                catch (Exception ex)
                                {

                                }

                                builder.Dismiss();
                            }
                        };
                        btnSincronizar.Click += async (sender, e) =>
                        {
                            btnSincronizar.Enabled = false;
                            App.Usuario = txtUsuario.Text.ToUpper();
                            string result = await ejecutarBD();
                            btnSincronizar.Enabled = true;
                            Toast.MakeText(Application.Context, result, ToastLength.Long).Show();
                        };

                        if (!string.IsNullOrEmpty(App.Servidor))
                        {
                            txtServidor.Text = App.Servidor;
                        }

                        txtServidor.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        txtServidor.SetHintTextColor(Android.Graphics.Color.LightSlateGray);
                        builder.Window.SetBackgroundDrawableResource(Resource.Color.semitransparent);
                        builder.Show(); break;
                    }
                default: break;
            }
            return base.OnOptionsItemSelected(item);
        }


        #region metodos

        private Task<string> ejecutarBD()
        {

            return Task.Run(() =>
            {
                Android.App.ProgressDialog progressDialog = null;
                try
                {
                    Bitacora log = new Bitacora("Sincro_" + DateTime.Now.ToString().Replace(":", ".").Replace("\\", ".").Replace("/", ".").Replace(" ", ".") + ".txt");
                }
                catch (Exception ex)
                {

                }
                Connection cnx = null;
                try
                {
                    bool result = true;
                    RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(this, "Sincronizando", "Por favor espere...", true); });
                    conexion con = new conexion(App.DeviceID, App.Servidor, App.Usuario);
                    cnx = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                    string error = string.Empty;
                    RunOnUiThread(() => { progressDialog.SetMessage("Obteniendo sentencias del servidor"); });
                    if (result && con.ObtenerSentenciasServidor(ref error))
                    {
                        result = true;
                    }
                    else
                    {
                        cnx.Close();
                        RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return error;
                    }
                    RunOnUiThread(() => { progressDialog.SetMessage("Creando Tablas en dispositivo"); });
                    if (result && con.CrearEstructuraDinamica(App.PathAplicacion, ref error, cnx))
                    {
                        result = true;
                        cnx.Close();
                    }
                    else
                    {
                        cnx.Close();
                        RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return error;
                    }
                    RunOnUiThread(() => { progressDialog.SetMessage("Descargando sentencias insert"); });
                    if (result && con.continuarInsertar && con.DescargarArchivoSincro(App.PathAplicacion, ref error))
                    {
                        result = true;
                        cnx.Close();
                    }
                    else
                    {
                        cnx.Close();
                        RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return error;
                    }
                    RunOnUiThread(() => { progressDialog.SetMessage("Descomprimiendo sentencias insert"); });
                    if (result && con.continuarInsertar && con.DescomprimirArchivoSincro(App.PathAplicacion, ref error))
                    {
                        result = true;
                        cnx.Close();
                    }
                    else
                    {
                        cnx.Close();
                        RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return error;
                    }
                    RunOnUiThread(() => { progressDialog.SetMessage("Insertando Datos en dispositivo"); });
                    cnx.Open();
                    if (result && con.continuarInsertar && con.InsertDatosDinamico(App.PathAplicacion, ref error, cnx, App.DeviceID))
                    {
                        cnx.Close();
                        if (Pedidos.Core.GestorDatos.cnx != null && Pedidos.Core.GestorDatos.cnx.isOpen)
                        {
                            Pedidos.Core.GestorDatos.cnx.Close();
                            Pedidos.Core.GestorDatos.cnx = null;
                        }
                        Pedidos.Core.GestorDatos.cnx = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                        Pedidos.Core.GestorDatos.cnx.Open();
                        try
                        {
                            GlobalUI.Rutas = Ruta.ObtenerRutas();
                            GlobalUI.CargaParametrosX(false);
                            GlobalUI.ValidarConfiguracion();
                        }
                        catch
                        {
                            RunOnUiThread(() => { progressDialog.Dismiss(); });
                            return "Base de datos creada correctamente pero los par�metros no se cargaron correctamente.";
                        }

                        if (!string.IsNullOrEmpty(con.atrWebSoporte))
                        {
                            App.WebSoporte = con.atrWebSoporte;

                            if (!App.WebSoporte.Contains("http://"))
                            {
                                App.WebSoporte = "http://" + App.WebSoporte;
                            }
                        }

                        App.CambiarDescuento = con.atrCambiarDescuento;

                        RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return "Base de datos creada correctamente";
                    }
                    else
                    {
                        cnx.Close();
                        RunOnUiThread(() => { progressDialog.Dismiss(); });
                        return error;
                    }
                }
                catch
                {
                    if (cnx != null && cnx.isOpen)
                        cnx.Close();
                    RunOnUiThread(() => { progressDialog.Dismiss(); });
                    return "Ocurri� un error al crear la base de datos";
                }
            });
        }

        private void SavePreferences()
        {
            var prefs = this.GetSharedPreferences("Softland.Android.PEM.preferences", FileCreationMode.Private);
            var editor = prefs.Edit();
            editor.PutString("User", txtUsuario.Text);
            editor.PutString("Pass", txtPassword.Text);
            editor.PutString("Server", App.Servidor);
            editor.PutString("websoporte", App.WebSoporte);
            editor.PutBoolean("cambiardescuento", App.CambiarDescuento);
            editor.Commit();
        }

        /// <summary>
        /// Carga las preferencias de la pantalla
        /// </summary>
        private void LoadPreferences()
        {
            var prefs = this.GetSharedPreferences("Softland.Android.PEM.preferences", FileCreationMode.Private);

            if (prefs.Contains("User"))
            {
                txtUsuario.Text = prefs.GetString("User", "");
            }
            if (prefs.Contains("Pass"))
            {
                txtPassword.Text = prefs.GetString("Pass", "");
            }
            if (prefs.Contains("websoporte"))
            {
                App.WebSoporte = prefs.GetString("websoporte", "");
            }
            if (prefs.Contains("cambiardescuento"))
            {
                App.CambiarDescuento = prefs.GetBoolean("cambiardescuento", false);
            }
            try
            {
                string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                string file = System.IO.Path.Combine(folder, "ServidorPreferences.txt");
                App.Servidor = System.IO.File.ReadAllText(file);

            }
            catch
            {
                App.Servidor = string.Empty;
            }
        }

        private void LoadPreferencesGlobal()
        {
            try
            {
                string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                string file = System.IO.Path.Combine(folder, "CiaPreferences.txt");
                App.CompaniaDefault = System.IO.File.ReadAllText(file);

                // Carga el simbolo monetario de la compa�ia guardada en la Configuraci�n.
                var compania = Compania.Obtener(App.CompaniaDefault);

                if (compania != null)
                {
                    GestorUtilitario.SimboloMonetario = compania.SimboloMonetarioFunc;
                }
            }
            catch
            {
                App.CompaniaDefault = string.Empty;
            }

            try
            {
                string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                string file = System.IO.Path.Combine(folder, "ConsecutivoPreferences.txt");
                App.ConsecutivoPedido = System.IO.File.ReadAllText(file);
            }
            catch
            {
                App.ConsecutivoPedido = string.Empty;
            }

            try
            {
                string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                string file = System.IO.Path.Combine(folder, "PrintPaperPreferences.txt");
                double tama�oPapel = Convert.ToDouble(System.IO.File.ReadAllText(file));
                FRmConfig.Tama�oPapel = tama�oPapel;
            }
            catch
            {
                FRmConfig.Tama�oPapel = 2.5;
            }

            try
            {
                string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                string file = System.IO.Path.Combine(folder, "PrintTypePreferences.txt");
                FRmConfig.TipoImpresora = System.IO.File.ReadAllText(file);
            }
            catch
            {
                FRmConfig.TipoImpresora = "Generica";
            }

            try
            {
                string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                string file = System.IO.Path.Combine(folder, "PrintNamePreferences.txt");
                FRmConfig.Impresora = System.IO.File.ReadAllText(file);
            }
            catch
            {
                //Do nothing
            }

            try
            {
                string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                string file = System.IO.Path.Combine(folder, "PrintSugerirPreferences.txt");
                Impresora.SugerirImprimir = System.IO.File.ReadAllText(file).Equals("SI");
            }
            catch
            {
                //Do nothing
            }
        }

        /// <summary>
        /// M�todo encargado de copiar los reportes al folder de la aplicacion.
        /// </summary>
        public void CopiarReportes()
        {
            string folder = System.IO.Path.Combine(App.PathAplicacion, "PEM.DROID.REPORTS"); ;

            folder += Java.IO.File.Separator + "Reportes";
            //Crea el directorio si no existe
            Java.IO.File Dir = new Java.IO.File(folder);
            Java.IO.File Dir2 = new Java.IO.File(folder + "2");
            Java.IO.File Dir3 = new Java.IO.File(folder + "3");
            if (!Dir.Exists())
                Dir.Mkdirs();
            if (!Dir2.Exists())
                Dir2.Mkdirs();
            if (!Dir3.Exists())
                Dir3.Mkdirs();

            for (int it = 1; it <= 3; it++)
            {
                for (int i = 1; i <= 1; i++)
                {
                    string name = string.Empty;
                    string nameAs = string.Empty;
                    string path = string.Empty;
                    switch (i)
                    {

                        case 1:
                            {
                                //Detalle Devolucion                        
                                if (it == 1)
                                {
                                    name = "Pedido.rdl";
                                    path = System.IO.Path.Combine(folder, name);
                                    Java.IO.File file = new Java.IO.File(folder, name);
                                    if (file.Length() == 0)
                                    {
                                        Java.IO.FileWriter writer = new Java.IO.FileWriter(file, false);
                                        using (System.IO.Stream stream = this.Assets.Open(name))
                                        {
                                            System.IO.StreamReader reader = new System.IO.StreamReader(stream);
                                            string text = reader.ReadToEnd();
                                            writer.Write(text);
                                            writer.Close();
                                        }
                                    }
                                }
                                if (it == 2)
                                {
                                    name = "Pedido.rdl";
                                    nameAs = "Pedido_2.rdl";
                                    path = System.IO.Path.Combine(folder + "2", name);
                                    Java.IO.File file = new Java.IO.File(folder + "2", name);
                                    if (file.Length() == 0)
                                    {
                                        Java.IO.FileWriter writer = new Java.IO.FileWriter(file, false);
                                        using (System.IO.Stream stream = this.Assets.Open(nameAs))
                                        {
                                            System.IO.StreamReader reader = new System.IO.StreamReader(stream);
                                            string text = reader.ReadToEnd();
                                            writer.Write(text);
                                            writer.Close();
                                        }
                                    }
                                }
                                if (it == 3)
                                {
                                    name = "Pedido.rdl";
                                    nameAs = "Pedido_3.rdl";
                                    path = System.IO.Path.Combine(folder + "3", name);
                                    Java.IO.File file = new Java.IO.File(folder + "3", name);
                                    if (file.Length() == 0)
                                    {
                                        Java.IO.FileWriter writer = new Java.IO.FileWriter(file, false);
                                        using (System.IO.Stream stream = this.Assets.Open(nameAs))
                                        {
                                            System.IO.StreamReader reader = new System.IO.StreamReader(stream);
                                            string text = reader.ReadToEnd();
                                            writer.Write(text);
                                            writer.Close();
                                        }
                                    }
                                }
                                break;
                            }
                    }

                }
            }

        }



        #endregion


    }
}