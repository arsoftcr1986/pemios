using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BI.Shared;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Data.SQLite;
using BI.Android;
using Pedidos.Core;
using Android.Views.InputMethods;

namespace Softland.Droid.PEL
{
    [Activity(Label = "Revisar Pedido", Icon = "@drawable/ic_launcher", ParentActivity = typeof(PedidoMainActivity))]
    public class PedidoReviewActivity : BaseActivity
    {
        const int DATE_DIALOG_ID = 0;
        DateTime fechaEntrega;
        Spinner cmbConsecutivo;
        TextView txtFechaRealizacion, txtHoraInicio, txtFechaEntrega,txtTotalBruto,
            txtDescuento,txtDescuentoVol, txtDescuento1tx, txtDescuento2tx, txtLabelImpuesto1,
            txtLabelImpuesto2, txtImpuestoVentas, txtConsumo,txtTotalNeto,txtActividadComercial, lblActividadComercial;
        EditText txtPorcDescuento1, txtPorcDescuento2;
        decimal descuentoPorCIA;
        bool dolar;
        bool cargaInicial;
        bool cambiaDescuento=true;
        bool permiteCambioDescuento=true;

        protected override int LayoutResource
        {
            get
            {
                return Resource.Layout.pedido_review;
            }
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            cargaInicial = true;
            permiteCambioDescuento = App.CambiarDescuento;
            txtFechaRealizacion = FindViewById<TextView>(Resource.Id.txtFechaRealizacion);
            txtHoraInicio = FindViewById<TextView>(Resource.Id.txtHoraInicio);
            txtFechaEntrega = FindViewById<TextView>(Resource.Id.txtFechaEntrega);
            txtTotalBruto = FindViewById<TextView>(Resource.Id.txtTotalBruto);
            txtTotalNeto = FindViewById<TextView>(Resource.Id.txtTotalNeto);
            txtDescuento = FindViewById<TextView>(Resource.Id.txtDescuento);
            txtDescuentoVol = FindViewById<TextView>(Resource.Id.txtDescuentoVol);
            txtDescuento1tx = FindViewById<TextView>(Resource.Id.txtDescuento1tx);
            txtDescuento2tx = FindViewById<TextView>(Resource.Id.txtDescuento2tx);
            txtLabelImpuesto1 = FindViewById<TextView>(Resource.Id.txtLabelImpuesto1);
            txtLabelImpuesto2 = FindViewById<TextView>(Resource.Id.txtLabelImpuesto2);
            txtImpuestoVentas = FindViewById<TextView>(Resource.Id.txtImpuestoVentas);
            txtConsumo = FindViewById<TextView>(Resource.Id.txtConsumo);
            txtPorcDescuento1 = FindViewById<EditText>(Resource.Id.txtPorcDescuento1);
            txtPorcDescuento2 = FindViewById<EditText>(Resource.Id.txtPorcDescuento2);
            //Jarbis Ajustes IVA >>>>>
            lblActividadComercial = FindViewById<TextView>(Resource.Id.lblActividadComercial);
            txtActividadComercial = FindViewById<TextView>(Resource.Id.txtActividadComercial);

            if (GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.Activar_IVA_CR)
            {
                txtActividadComercial.Visibility = ViewStates.Visible;
                lblActividadComercial.Visibility = ViewStates.Visible;
            }
            else
            {
                txtActividadComercial.Visibility = ViewStates.Invisible;
                lblActividadComercial.Visibility = ViewStates.Invisible;
            }
            
            //Jarbis Ajustes IVA <<<<<
            cmbConsecutivo = FindViewById<Spinner>(Resource.Id.cmbConsecutivo);
            
            dolar = GlobalUI.ClienteActual.ObtenerClienteCia(App.CompaniaDefault).NivelPrecio.Moneda == TipoMoneda.DOLAR;
            // Create your application here 
            txtFechaEntrega.Click += (s, e) => { ShowDialog(DATE_DIALOG_ID); };
            txtPorcDescuento2.AfterTextChanged += async (s, e) =>
            {
                if (cargaInicial || !cambiaDescuento)
                    return;     
                decimal cantidad = 0;
                if (!Decimal.TryParse(((EditText)s).Text.Replace("%", ""), out cantidad))
                {
                    Toast.MakeText(this, "Cantidad descuento inv�lida revise la entrada de datos", ToastLength.Short).Show();
                    return;
                    //cantidad = art.TotalAlmacen;
                }
                bool result = ValidarDescuentos(cantidad);
                if (!result)
                {
                    GestorPedido.PedidosCollection.Gestionados[0].PorcentajeDescuento2 = cantidad;
                    result = await ExecuteConsultar();
                }
                else
                {
                    ((EditText)s).Text = GestorUtilitario.FormatPorcentaje(GestorPedido.PedidosCollection.Gestionados[0].PorcentajeDescuento2);
                    result = await ExecuteConsultar();
                }
            };

            //txtPorcDescuento2.EditorAction += HandleEditorAction;  

            cmbConsecutivo.ItemSelected += cmbConsecutivo_ItemSelected;

            CargaInicial();            
        }

        private async void CargaInicial()
        {
            txtPorcDescuento2.Enabled = permiteCambioDescuento;
            bool result = await ExecuteConsultar();
            cargaInicial = false;
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.review, menu);
            var view = menu.FindItem(Resource.Id.menu_cantidad).ActionView;
            var txt = view.FindViewById<TextView>(Resource.Id.txtCantidadArticulos);
            txt.Text = GestorPedido.PedidosCollection.Gestionados[0].Detalles.TotalArticulos.ToString();
            txt.SetTextColor(Android.Graphics.Color.WhiteSmoke);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    {
                        OnBackPressed();
                        break;
                    }
                case Resource.Id.menu_info:
                    {
                        Toast.MakeText(this, GlobalUI.NameCliente, ToastLength.Long).Show();
                        break;
                    }
                case Resource.Id.action_save:
                    {
                        item.SetEnabled(false);
                        GuardarPedido();
                        item.SetEnabled(true);
                        break;
                    }
            }
            return true;
        }

        protected override Dialog OnCreateDialog(int id)
        {

            switch (id)
            {
                case DATE_DIALOG_ID:
                    var dialog = new DatePickerDialog(this, DatePickerDataSet, fechaEntrega.Year, fechaEntrega.Month, fechaEntrega.Day);
                    return dialog;
            }
            return base.OnCreateDialog(id);
        }

        public override void OnBackPressed()
        {
            var intent = new Intent(this, typeof(PedidoMainActivity));
            intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
            SetResult(Result.Ok, intent);
            Finish();
        }        
        

        #region metodos      
  
        void cmbConsecutivo_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            App.ConsecutivoPedido = ((Spinner)sender).SelectedItem.ToString();
            CrearPreferencia(1);
        }

        private void LlenarListaConsecutivos(string cia)
        {
            try
            {
                Connection cnxConsecutivos = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                var lista = Compania.Obtener(cia).DevolverConsecutivos(cnxConsecutivos);
                var adapter = new ArrayAdapter<string>(this, Resource.Layout.SimpleSpinnerItem, lista);
                adapter.SetDropDownViewResource(Resource.Layout.SimpleSpinnerDropDownItem);
                cmbConsecutivo.Adapter = adapter;
                cnxConsecutivos.Close();
                cnxConsecutivos = null;
                if (!string.IsNullOrEmpty(App.ConsecutivoPedido))
                {
                    try
                    {
                        cmbConsecutivo.SetSelection(((ArrayAdapter)cmbConsecutivo.Adapter).GetPosition(App.ConsecutivoPedido));
                    }
                    catch
                    {
                        cmbConsecutivo.SetSelection(0);
                    }
                }
            }
            catch
            {
                Toast.MakeText(Android.App.Application.Context, "No se han podido obtener los consecutivos.", ToastLength.Short).Show();
            }
        }

        private void DatePickerDataSet(object sender, DatePickerDialog.DateSetEventArgs e)
        {
            fechaEntrega = e.Date;
            CambioFechaEntrega();
            txtFechaEntrega.Text = GestorUtilitario.ObtenerFechaString(fechaEntrega);
        }

        private Task<bool> ExecuteConsultar()
        {

            return Task.Run(() =>
            {
                try
                {
                    CalcularImpVentas();        
                    RunOnUiThread(() =>
                        {
                            LlenarListaConsecutivos(App.CompaniaDefault);

                            txtLabelImpuesto1.Text=GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.Impuesto1Descripcion;
                            txtLabelImpuesto2.Text=GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.Impuesto2Descripcion;
                            foreach (Pedido ped in GestorPedido.PedidosCollection.Gestionados)
                            {
                                if (ped.FechaEntrega < DateTime.Today.AddDays(1))
                                {
                                    fechaEntrega = DateTime.Today.AddDays(1);
                                    CambioFechaEntrega();
                                }
                                else
                                {
                                    fechaEntrega = ped.FechaEntrega;
                                }
                                txtFechaRealizacion.Text = GestorUtilitario.ObtenerFechaString(ped.FechaRealizacion);
                                txtHoraInicio.Text = GestorUtilitario.ObtenerHoraString(ped.HoraInicio);
                                txtFechaEntrega.Text = GestorUtilitario.ObtenerFechaString(fechaEntrega);
                                txtPorcDescuento1.Text = GestorUtilitario.FormatPorcentaje(GestorPedido.PedidosCollection.PorcDesc1);

                                //Jarbis Ajustes IVA >>>>>
                                if (GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.Activar_IVA_CR)
                                {
                                    txtActividadComercial.Text = GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.ActividadComercial;
                                }
                                //Jarbis Ajustes IVA <<<<<
                                
                                decimal cantidadDesc = 0;
                                if (Decimal.TryParse(txtPorcDescuento2.Text.Replace("%", ""), out cantidadDesc))
                                {
                                    if (cantidadDesc != GestorPedido.PedidosCollection.PorcDesc2)
                                    {
                                        txtPorcDescuento2.Text = GestorUtilitario.FormatPorcentaje(GestorPedido.PedidosCollection.PorcDesc2);
                                    }
                                }
                                else
                                {
                                    txtPorcDescuento2.Text = GestorUtilitario.FormatPorcentaje(GestorPedido.PedidosCollection.PorcDesc2);
                                }
                                //
                                //decimal cantidad = 0;
                                //if (!Decimal.TryParse(txtPorcDescuento2.Text.Replace("%", ""), out cantidad))
                                //{
                                //    //cantidad = art.TotalAlmacen;
                                //}
                                //if (cantidad != GestorPedido.PedidosCollection.PorcDesc2 || string.IsNullOrEmpty(txtPorcDescuento2.Text))
                                //    txtPorcDescuento2.Text = GestorUtilitario.FormatPorcentaje(GestorPedido.PedidosCollection.PorcDesc2);
                                decimal montoBrutoBon = GestorPedido.PedidosCollection.Gestionados[0].MontoBrutoBonificaciones;
                                decimal montoNetoRedondeo = 0;
                                decimal diferenciaDescuento1 = 0;
                                bool redondearFactura = GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.UsaRedondeo && GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.FactorRedondeo > 0;
                                if (redondearFactura)
                                {
                                    montoNetoRedondeo = GestorUtilitario.TrunqueAFactor(GestorPedido.PedidosCollection.Gestionados[0].MontoNeto, GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.FactorRedondeo);
                                    diferenciaDescuento1 = GestorPedido.PedidosCollection.Gestionados[0].MontoNeto - montoNetoRedondeo;
                                    diferenciaDescuento1 = Math.Abs(diferenciaDescuento1);
                                }

                                if (dolar)
                                {
                                    if(redondearFactura)
                                        txtDescuento1tx.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalDescuento1+diferenciaDescuento1);
                                    else
                                        txtDescuento1tx.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalDescuento1);
                                    txtDescuento2tx.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalDescuento2);
                                    txtDescuento.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalDescuentoLineas);
                                    txtDescuentoVol.Text = GestorUtilitario.FormatNumeroDolar(montoBrutoBon);
                                    txtImpuestoVentas.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalImpuesto1);
                                    txtConsumo.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalImpuesto2);
                                    txtTotalBruto.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalBruto + montoBrutoBon);
                                    if(redondearFactura)
                                        txtTotalNeto.Text = GestorUtilitario.FormatNumeroDolar(montoNetoRedondeo);                                    
                                    else
                                        txtTotalNeto.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalNeto);                                    
                                }
                                else
                                {
                                    if(redondearFactura)
                                        txtDescuento1tx.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalDescuento1+diferenciaDescuento1);
                                    else
                                        txtDescuento1tx.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalDescuento1);
                                    txtDescuento2tx.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalDescuento2);
                                    txtDescuento.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalDescuentoLineas);
                                    txtDescuentoVol.Text = GestorUtilitario.FormatNumero(montoBrutoBon);
                                    txtImpuestoVentas.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalImpuesto1);
                                    txtConsumo.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalImpuesto2);
                                    txtTotalBruto.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalBruto + montoBrutoBon);
                                    if(redondearFactura)
                                        txtTotalNeto.Text = GestorUtilitario.FormatNumero(montoNetoRedondeo);
                                    else
                                        txtTotalNeto.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalNeto);
                                    
                                }
                            }                                                          

                        }
                    );                    
                    return true;
                }
                catch
                {
                    RunOnUiThread(()=>{Toast.MakeText(Android.App.Application.Context, "Error consultando el art�culo",ToastLength.Long).Show();});
                    return false;
                }
            });
        }

        private Task<bool> ExecuteGuardar(Activity activity)
        {

            return Task.Run(() =>
            {
                Android.App.ProgressDialog progressDialog = null;
                try
                {
                    RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(activity, "Guardando Pedido", "Por favor espere...", true); });
                    bool result = true;
                    if (result)
                    {
                        if (string.IsNullOrEmpty(GestorPedido.PedidosCollection.Gestionados[0].DireccionEntrega))
                        {
                            RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Debe definir la direcci�n de entrega para la compa��a " + GestorPedido.PedidosCollection.Gestionados[0].Compania + ".", ToastLength.Short).Show();;});
                            return false;
                        }
                        decimal costoTotal = 0;
                        decimal montoPedido = 0;
                        costoTotal = GestorPedido.PedidosCollection.Gestionados[0].ObtenerCostoTotal();
                        montoPedido = GestorPedido.PedidosCollection.Gestionados[0].MontoBruto - GestorPedido.PedidosCollection.Gestionados[0].MontoTotalDescuento;
                        if (montoPedido < costoTotal)
                        {
                            RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "No se puede vender por debajo del costo total.", ToastLength.Short).Show(); });
                            return false;
                        }
                        GestorPedido.PedidosCollection.Gestionados[0].Usuario = App.Usuario;
                        GestorPedido.PedidosCollection.Gestionados[0].CodigoConsecutivo = App.ConsecutivoPedido;
                        GestorPedido.guardarPedido(App.ConsecutivoPedido);
                        GestorPedido.PedidosCollection = new PedidosCollection();
                        GC.Collect(GC.MaxGeneration);
                        RunOnUiThread(() => { 
                            progressDialog.Dismiss();
                            Toast.MakeText(Android.App.Application.Context, "Pedido guardado correctamente", ToastLength.Short).Show();
                            var intent = new Intent(activity, typeof(MainActivity));
                            intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
                            activity.StartActivity(intent);
                            Finish();
                        });
                    }
                    else
                    {
                        RunOnUiThread(() => { progressDialog.Dismiss(); });
                    }
                    return true;
                }
                catch
                {
                    RunOnUiThread(() => { progressDialog.Dismiss(); });
                    RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error al guardar el pedido.", ToastLength.Short).Show(); });
                    return false;
                }
            });
        }

        /// <summary>
        /// Imprime el Documento
        /// </summary>
        /// <param name="esOriginal"></param>
        /// <param name="cantidadCopias"></param>
        /// <param name="ordernarPor"></param>
        /// <param name="viewModel"></param>
        private async void ImprimirDocumento(bool esOriginal, int cantidadCopias, DetalleSort.Ordenador ordernarPor, Activity activity)
        {
            bool error = false;
            try
            {
                int cantidad = 0;
                cantidad = cantidadCopias;
                if (cantidad > 0 || esOriginal)
                {
                    PedidosCollection pedidosImprimir = new PedidosCollection(GestorPedido.PedidosCollection.Gestionados, GlobalUI.ClienteActual);
                    bool result = await pedidosImprimir.ImprimeDetallePedido(cantidad, (DetalleSort.Ordenador)ordernarPor, activity,App.PathAplicacion);
                    if (result)
                    {
                        foreach (Pedido pedido in GestorPedido.PedidosCollection.Gestionados)
                            pedido.Impreso = true;
                        result = await ExecuteGuardar(activity);
                    }
                    else
                    {
                        Toast.MakeText(Android.App.Application.Context, "No se pudo imprimir el documento", ToastLength.Short).Show();
                        result = await ExecuteGuardar(activity);                        
                    }
                }
                else
                {
                    AlertDialog.Builder alertB = new AlertDialog.Builder(activity);
                    alertB.SetCancelable(false);
                    alertB.SetIcon(Resource.Drawable.ic_info);
                    alertB.SetMessage("Solo se guardara el documento ya que no especifico ni copia ni original.");
                    alertB.SetPositiveButton("Aceptar",async delegate {bool result = await ExecuteGuardar(activity); });
                    alertB.SetNegativeButton("Cancelar", delegate { return; });
                    activity.RunOnUiThread(() => { alertB.Show(); });
                }

            }
            catch (FormatException)
            {
                throw new Exception("Error obteniendo la cantidad de copias. Formato inv�lido.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CrearPreferencia(int opcion)
        {
            switch (opcion)
            {
                case 0:
                    {
                        string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                        string file = System.IO.Path.Combine(folder, "CiaPreferences.txt");
                        using (System.IO.StreamWriter st = new System.IO.StreamWriter(file, false))
                        {
                            st.Write(App.CompaniaDefault.ToString());
                        };
                        break;
                    }
                case 1:
                    {
                        string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                        string file = System.IO.Path.Combine(folder, "ConsecutivoPreferences.txt");
                        using (System.IO.StreamWriter st = new System.IO.StreamWriter(file, false))
                        {
                            st.Write(App.ConsecutivoPedido.ToString());
                        };
                        break;
                    }
                case 2:
                    {
                        string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                        string file = System.IO.Path.Combine(folder, "ServidorPreferences.txt");
                        using (System.IO.StreamWriter st = new System.IO.StreamWriter(file, false))
                        {
                            st.Write(App.Servidor.ToString());
                        };
                        break;
                    }
                default: break;
            }
        }

        private async void GuardarPedido() 
        {
            if (!Impresora.SugerirImprimir)
            {
                bool result = await ExecuteGuardar(this);
            }
            else
            {
                string msj = "�Desea imprimir el detalle de los pedidos realizados?";
                string titulo = "Impresi�n de Detalle de Pedidos";
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.SetMessage(msj);
                alert.SetPositiveButton("Si", delegate
                {
                    Intent intent = new Intent(this, typeof(ImpresionActivity));
                    intent.PutExtra(titulo, "titulo");
                    intent.PutExtra("original", true);
                    ImpresionActivity.OnImprimir = ImprimirDocumento;
                    this.StartActivity(intent);
                });
                alert.SetNegativeButton("No",async delegate
                {
                    bool result = await ExecuteGuardar(this);
                });
                RunOnUiThread(() => { alert.Show(); });
            }            
        }
        
        private void CambioFechaEntrega()
        {
            foreach (Pedido ped in GestorPedido.PedidosCollection.Gestionados)
            {
                ped.FechaEntrega = fechaEntrega;
            }
        }

        private void CalcularImpVentas()
        {
            try
            {
                foreach (Pedido pedido in GestorPedido.PedidosCollection.Gestionados)
                {
                    pedido.RecalcularImpuestos(pedido.MontoSubTotal < pedido.Configuracion.Compania.MontoMinimoExcento);
                }
                GestorPedido.PedidosCollection.SacarMontosTotales();
                descuentoPorCIA = 100 * GestorPedido.PedidosCollection.Gestionados[GestorPedido.PedidosCollection.Gestionados.Count - 1].PorcDescGeneral;
            }
            catch
            {
                Toast.MakeText(this, "Error al recalcular montos. ", ToastLength.Short).Show();
            }
        }

        private bool ValidarDescuentos(decimal PorcDescuento2)
        {
            Boolean limpiarDescuentos = false;
            if (limpiarDescuentos == false && PorcDescuento2 < 0)
            {
                Toast.MakeText(this,"El Descuento 2 debe ser mayor a cero",ToastLength.Short).Show();
                limpiarDescuentos = true;
                PorcDescuento2 = 0;
            }else 
            if (!limpiarDescuentos)
            {
                if ((GestorPedido.PedidosCollection.Gestionados[0].PorcentajeDescuento1  + PorcDescuento2) > 100)
                {
                    Toast.MakeText(this, "La suma del porcentaje de descuento 1 y 2 no puede exceder el 100%", ToastLength.Short).Show();
                    limpiarDescuentos = true;
                    PorcDescuento2 = 100 - GestorPedido.PedidosCollection.Gestionados[0].PorcentajeDescuento1;
                }
            }
            return limpiarDescuentos;
        }


        #endregion


    }
}