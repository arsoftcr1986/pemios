using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BI.Shared;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Data.SQLite;
using BI.Android;
using Pedidos.Core;
using Android.Support.V4.View;
using com.refractored;
using Android.Support.V4.App;

namespace Softland.Droid.PEL
{
    [Activity(Label = "Seleccionar Art�culos", Icon = "@drawable/ic_launcher", ParentActivity = typeof(PedidoMainActivity), ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
    public class ArticulosPedidoActivity : BaseActivity
    {
        public List<string> removidos;
        public ArticulosAdapter adapter;
        public ArticulosGridAdapter adapterGrid;
        Android.Support.V7.Widget.SearchView mSearchview;
        IMenuItem item;

        protected override int LayoutResource
        {
            get
            {
                return Resource.Layout.pedido_articulos;
            }
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Create your application here   
            adapter = null;
            adapterGrid = null;
            removidos = new List<string>();
            //Pages
            //if first time you will want to go ahead and click first item.
            if (savedInstanceState == null)
            {
                ListItemClicked();
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.search_articulos_pedidos, menu);
            item = menu.FindItem(Resource.Id.action_search);
            var searchView = MenuItemCompat.GetActionView(item);
            mSearchview = searchView.JavaCast<Android.Support.V7.Widget.SearchView>();
            try
            {
                mSearchview.QueryTextSubmit += (s, e) =>
                {
                    if (ArticulosPedidoPageFragment.currentPos == 0)
                    {                        
                        if (this.adapter == null || App.FiltroArticulo.Equals(App.ClasificacionFilter))
                            return;
                        this.adapter.Filter.InvokeFilter(e.Query);
                    }
                    else
                    {
                        if (this.adapterGrid == null || App.FiltroArticulo.Equals(App.ClasificacionFilter))
                            return;
                        this.adapterGrid.Filter.InvokeFilter(e.Query);
                    }                    
                };
                mSearchview.QueryTextChange += (s, e) =>
                {
                    if (ArticulosPedidoPageFragment.currentPos == 0)
                    {
                        if (this.adapter == null || App.FiltroArticulo.Equals(App.ClasificacionFilter))
                            return;
                        if (string.IsNullOrEmpty(e.NewText))
                        {
                            this.adapter.ResetSearch();
                        }
                        else
                        {
                            this.adapter.Filter.InvokeFilter(e.NewText);
                        }
                    }
                    else
                    {
                        if (this.adapterGrid == null || App.FiltroArticulo.Equals(App.ClasificacionFilter))
                            return;
                        if (string.IsNullOrEmpty(e.NewText))
                        {
                            this.adapterGrid.ResetSearch();
                        }
                        else
                        {
                            this.adapterGrid.Filter.InvokeFilter(e.NewText);
                        }
                    }                    
                    
                };
            }
            catch
            {
            }
            return true;
        }

        public override void OnBackPressed()
        {            
            GestionarPedido();
        }

        private async void GestionarPedido() 
        {
            Android.App.ProgressDialog progressDialog = null;
            progressDialog = Android.App.ProgressDialog.Show(this, "Editando Pedido", "Por favor espere...", true);
            bool res= await ExecuteAgregarEliminar();
            if (res)
            {
                var intent = new Intent(this, typeof(PedidoMainActivity));
                intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
                SetResult(Result.Ok, intent);
                Finish();
                progressDialog.Dismiss();
            }
            else
            {
                var intent = new Intent(this, typeof(PedidoMainActivity));
                intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
                SetResult(Result.Canceled, intent);
                Finish();
                progressDialog.Dismiss();
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    {
                        GestionarPedido();
                        break;
                    }
                case Resource.Id.menu_info:
                    {
                        Toast.MakeText(this, GlobalUI.NameCliente, ToastLength.Long).Show();
                        break;
                    }
                case Resource.Id.menu_filtro:
                    {
                        var myActivity = this;
                        Android.App.Dialog builder = new Android.App.Dialog(myActivity);
                        builder.Window.RequestFeature(WindowFeatures.NoTitle);
                        builder.SetContentView(Resource.Layout.dialog_filtroartPed);
                        RadioButton rbCodigo = builder.FindViewById<RadioButton>(Resource.Id.rbCodigo);
                        RadioButton rbDescripcion = builder.FindViewById<RadioButton>(Resource.Id.rbDescripcion);
                        RadioButton rbBarras = builder.FindViewById<RadioButton>(Resource.Id.rbBarras);
                        RadioButton rbSeleccionados = builder.FindViewById<RadioButton>(Resource.Id.rbSeleccionados);
                        RadioButton rbClasificacion = builder.FindViewById<RadioButton>(Resource.Id.rbClasificacion);
                        RadioGroup rgFiltros = builder.FindViewById<RadioGroup>(Resource.Id.rgFiltros);
                        rbCodigo.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rbDescripcion.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rbBarras.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rbSeleccionados.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rbClasificacion.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rgFiltros.CheckedChange += (sender, e) =>
                        {
                            bool reset = App.FiltroArticulo.Equals(App.SeleccionadosFilter);
                            if (rbCodigo.Checked)
                            {
                                App.FiltroArticulo = App.CodigoFilter;
                                Toast.MakeText(Android.App.Application.Context, "Filtrando art�culos por c�digo.", ToastLength.Short).Show();
                                if (reset && myActivity.adapterGrid != null)
                                {
                                    myActivity.adapterGrid.ResetSearch();
                                    myActivity.adapter.ResetSearch();
                                }
                                builder.Dismiss();
                            }
                            else if (rbDescripcion.Checked)
                            {
                                App.FiltroArticulo = App.DescripcionFilter;
                                Toast.MakeText(Android.App.Application.Context, "Filtrando art�culos por descripci�n.", ToastLength.Short).Show();
                                if (reset && myActivity.adapterGrid != null)
                                {
                                    myActivity.adapterGrid.ResetSearch();
                                    myActivity.adapter.ResetSearch();
                                }
                                builder.Dismiss();
                            }
                            else if (rbBarras.Checked)
                            {
                                App.FiltroArticulo = App.BarrasFilter;
                                Toast.MakeText(Android.App.Application.Context, "Filtrando art�culos por c�digo de barras.", ToastLength.Short).Show();
                                if (reset && myActivity.adapterGrid != null)
                                {
                                    myActivity.adapterGrid.ResetSearch();
                                    myActivity.adapter.ResetSearch();
                                }
                                builder.Dismiss();
                            }
                            else if (rbClasificacion.Checked)
                            {
                                App.FiltroArticulo = App.ClasificacionFilter;
                                builder.Dismiss();
                                Connection cnxVerif = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                                try
                                {
                                    if (!Clasificacion.HayClasificaciones(App.CompaniaDefault, 1, cnxVerif) && !Clasificacion.HayClasificaciones(App.CompaniaDefault, 2, cnxVerif))
                                    {
                                        Toast.MakeText(Android.App.Application.Context, "No se encontraron clasificaciones disponibles", ToastLength.Short).Show();
                                        cnxVerif.Close();
                                        cnxVerif = null;
                                        rbCodigo.Checked = true;
                                        return;
                                    }
                                    else
                                    {
                                        Toast.MakeText(Android.App.Application.Context, "Filtrando art�culos por clasificaci�n.", ToastLength.Short).Show();
                                    }
                                    cnxVerif.Close();
                                    cnxVerif = null;
                                }
                                catch
                                {
                                    Toast.MakeText(Android.App.Application.Context, "No se encontraron clasificaciones", ToastLength.Short).Show();
                                    cnxVerif.Close();
                                    cnxVerif = null;
                                    return;
                                }
                                Android.App.Dialog builderClas = new Android.App.Dialog(myActivity);
                                builderClas.Window.RequestFeature(WindowFeatures.NoTitle);
                                builderClas.SetContentView(Resource.Layout.dialog_clasificacion);
                                var label1 = builderClas.FindViewById<TextView>(Resource.Id.label1);
                                var label2 = builderClas.FindViewById<TextView>(Resource.Id.label2);
                                var txtFamilia = builderClas.FindViewById<TextView>(Resource.Id.txtFamilia);
                                var txtSubFamilia = builderClas.FindViewById<TextView>(Resource.Id.txtSubFamilia);
                                label1.SetTextColor(Android.Graphics.Color.White);
                                label2.SetTextColor(Android.Graphics.Color.White);
                                txtFamilia.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                                txtSubFamilia.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                                txtFamilia.Text = App.FamiliaActual;
                                txtSubFamilia.Text = App.SubFamiliaActual;
                                var imgEditarFamilia = builderClas.FindViewById<ImageButton>(Resource.Id.imgEditarFamilia);
                                var imgEliminarFamilia = builderClas.FindViewById<ImageButton>(Resource.Id.imgEliminarFamilia);
                                var imgEditarSubFamilia = builderClas.FindViewById<ImageButton>(Resource.Id.imgEditarSubFamilia);
                                var imgEliminarSubFamilia = builderClas.FindViewById<ImageButton>(Resource.Id.imgEliminarSubFamilia);
                                var btnAceptar = builderClas.FindViewById<Button>(Resource.Id.btnAceptar);
                                imgEliminarFamilia.Click += (s, events) => { txtFamilia.Text = string.Empty; };
                                imgEliminarSubFamilia.Click += (s, events) => { txtSubFamilia.Text = string.Empty; };
                                btnAceptar.Click += (s, events) =>
                                {
                                    App.FamiliaActual = txtFamilia.Text;
                                    App.SubFamiliaActual = txtSubFamilia.Text;
                                    if (string.IsNullOrEmpty(txtFamilia.Text) && string.IsNullOrEmpty(txtSubFamilia.Text))
                                    {
                                        if (adapter == null)
                                        {
                                            builderClas.Dismiss();
                                            return;
                                        }
                                        if (ArticulosPedidoPageFragment.currentPos == 0)
                                            adapter.ResetSearch();
                                        else
                                            adapterGrid.ResetSearch();
                                        builderClas.Dismiss();
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(txtFamilia.Text) && !string.IsNullOrEmpty(txtSubFamilia.Text))
                                        {
                                            if (ArticulosPedidoPageFragment.currentPos == 0)
                                            {
                                                adapter.Filter.InvokeFilter(txtFamilia.Text + "," + txtSubFamilia.Text);
                                            }
                                            else
                                            {
                                                adapterGrid.Filter.InvokeFilter(txtFamilia.Text + "," + txtSubFamilia.Text);
                                            }                                            
                                            
                                        }
                                        else if (!string.IsNullOrEmpty(txtFamilia.Text))
                                        {
                                            if (ArticulosPedidoPageFragment.currentPos == 0)
                                                adapter.Filter.InvokeFilter(txtFamilia.Text);
                                            else
                                                adapterGrid.Filter.InvokeFilter(txtFamilia.Text);
                                        }
                                        else
                                        {
                                            if (ArticulosPedidoPageFragment.currentPos == 0)
                                                adapter.Filter.InvokeFilter(txtSubFamilia.Text);
                                            else
                                                adapterGrid.Filter.InvokeFilter(txtSubFamilia.Text);
                                        }
                                        builderClas.Dismiss();
                                    }
                                };
                                imgEditarFamilia.Click += (s, events) =>
                                {
                                    Android.App.Dialog buildertemp = new Android.App.Dialog(myActivity);
                                    buildertemp.Window.RequestFeature(WindowFeatures.NoTitle);
                                    buildertemp.SetContentView(Resource.Layout.dialog_clasificacion_list);
                                    ListView list = buildertemp.FindViewById<ListView>(Resource.Id.list);
                                    List<Clasificacion> lista = new List<Clasificacion>();
                                    Connection cnxArticulo = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                                    try
                                    {
                                        lista = Clasificacion.ObtenerClasificaciones(App.CompaniaDefault, 1, cnxArticulo);
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    catch
                                    {
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    ClasificacionAdapter adapterClas = new ClasificacionAdapter(myActivity, lista);
                                    list.Adapter = adapterClas;
                                    list.ItemClick += (ss, ee) =>
                                    {
                                        txtFamilia.Text = adapterClas.GetCodigo(ee.Position);
                                        buildertemp.Dismiss();
                                    };
                                    buildertemp.Window.SetBackgroundDrawableResource(Android.Resource.Drawable.DialogHoloLightFrame);
                                    buildertemp.Show();
                                };
                                imgEditarSubFamilia.Click += (s, events) =>
                                {
                                    Android.App.Dialog buildertemp = new Android.App.Dialog(myActivity);
                                    buildertemp.Window.RequestFeature(WindowFeatures.NoTitle);
                                    buildertemp.SetContentView(Resource.Layout.dialog_clasificacion_list);
                                    ListView list = buildertemp.FindViewById<ListView>(Resource.Id.list);
                                    List<Clasificacion> lista = new List<Clasificacion>();
                                    Connection cnxArticulo = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                                    try
                                    {
                                        lista = Clasificacion.ObtenerClasificaciones(App.CompaniaDefault, 2, cnxArticulo);
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    catch
                                    {
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    ClasificacionAdapter adapterClas = new ClasificacionAdapter(myActivity, lista);
                                    list.Adapter = adapterClas;
                                    list.ItemClick += (ss, ee) =>
                                    {
                                        txtSubFamilia.Text = adapterClas.GetCodigo(ee.Position);
                                        buildertemp.Dismiss();
                                    };
                                    buildertemp.Window.SetBackgroundDrawableResource(Android.Resource.Drawable.DialogHoloLightFrame);
                                    buildertemp.Show();
                                };
                                builderClas.Window.SetBackgroundDrawableResource(Resource.Color.semitransparent);
                                builderClas.Show();
                            }
                            else
                            {

                                if (myActivity.adapterGrid != null)
                                {
                                    App.FiltroArticulo = App.SeleccionadosFilter;
                                    myActivity.adapterGrid.Filter.InvokeFilter(string.Empty);
                                    myActivity.adapter.Filter.InvokeFilter(string.Empty);
                                }
                                Toast.MakeText(Android.App.Application.Context, "Filtrando art�culos por seleccionados.", ToastLength.Short).Show();
                                builder.Dismiss();
                            }

                        };
                        if (string.IsNullOrEmpty(App.FiltroArticulo))
                        {
                            App.FiltroArticulo = App.CodigoFilter;
                            rbCodigo.Checked = true;
                        }
                        else
                        {
                            if (App.FiltroArticulo.Equals(App.CodigoFilter) || App.FiltroArticulo.Equals(App.ClasificacionFilter))
                                rbCodigo.Checked = true;
                            else if (App.FiltroArticulo.Equals(App.DescripcionFilter))
                                rbDescripcion.Checked = true;
                            else if (App.FiltroArticulo.Equals(App.SeleccionadosFilter))
                                rbSeleccionados.Checked = true;
                            else
                                rbBarras.Checked = true;
                        }
                        builder.Window.SetBackgroundDrawableResource(Resource.Color.semitransparent);
                        builder.Show(); break;
                    }
            }
            return true;
        }

        private void ListItemClicked()
        {
            Android.Support.V4.App.Fragment fragment = null;
            fragment = new ArticulosPedidoPageFragment();

            SupportFragmentManager.BeginTransaction()
                .Replace(Resource.Id.content_frame, fragment).Commit();
        }
        

        #region metodos      

        private Task<bool> ExecuteAgregarEliminar()
        {

            return Task.Run(() =>
            {
                try
                {
                    if (removidos.Count > 0)
                    {
                        GestorPedido.RetirarDetalles(adapter.getRemovidos(removidos));
                    }
                    
                    if (GestorPedido.PedidosCollection != null && GestorPedido.PedidosCollection.Gestionados.Count > 0 
                        && GestorPedido.PedidosCollection.Gestionados[0].Detalles!=null && GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista.Count>0)
                    {
                        var coleccion = GestorPedido.PedidosCollection.Gestionados[0];

                        var seleccionados = adapter.getSeleccionados();

                        GestorPedido.AgregarModificarDetalles((adapter.getSeleccionados().Where(
                                x => !GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista.Exists(
                                    xx => xx.Articulo.Codigo == x.Codigo && xx.Articulo.CantidadAlmacen == x.CantidadAlmacen))
                        ).ToList());
                    }
                    else
                    {
                        GestorPedido.AgregarModificarDetalles(adapter.getSeleccionados());
                    }                    
                    return true;
                }
                catch
                {
                    RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error al actualizar cantidad.", ToastLength.Short).Show(); });
                    return false;
                }
            });
        }



        #endregion


    }
}