using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BI.Shared;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Data.SQLite;
using BI.Android;
using Pedidos.Core;
using Android.Support.V4.View;

namespace Softland.Droid.PEL
{
    [Activity(Label = "Crear Pedido", Icon = "@drawable/ic_launcher", ParentActivity = typeof(MainActivity), ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
    public class PedidoMainActivity : BaseActivity
    {
        Android.Support.V7.Widget.SearchView mSearchview;
        ListView list;
        DetallePedidoAdapter adapter;
        TextView txtTotal;
        IMenuItem item;
        bool esperar;
        int posActual = 0;

        protected override int LayoutResource
        {
            get
            {
                return Resource.Layout.pedido_main;
            }
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            txtTotal = FindViewById<TextView>(Resource.Id.txtTotal);
            list = FindViewById<ListView>(Resource.Id.list);
            // Create your application here   
            list.ItemClick += list_ItemClick;
            list.ItemLongClick += list_ItemLongClick;
            adapter = null;
            InicializarLista();
        }

        void list_ItemLongClick(object sender, AdapterView.ItemLongClickEventArgs e)
        {
            Dialog builder = new Dialog(this);
            builder.Window.RequestFeature(WindowFeatures.NoTitle);
            builder.SetContentView(Resource.Layout.dialog_edit);
            Button btnBonificado = builder.FindViewById<Button>(Resource.Id.btnBonificado);
            Button btnEditar = builder.FindViewById<Button>(Resource.Id.btnEditar);
            Button btnEliminar = builder.FindViewById<Button>(Resource.Id.btnEliminar);
            Button btnConsultar = builder.FindViewById<Button>(Resource.Id.btnConsultar);

            if (adapter.Bonificado(e.Position))
            {
                btnBonificado.Click += (senderbtn, ebtn) =>
                {
                    builder.Dismiss();
                    adapter.MostarDatosBonificacion(e.Position);
                };
            }
            else
            {
                btnBonificado.Enabled = false;
            }

            btnEditar.Click += (senderbtn, ebtn) =>
            {
                builder.Dismiss();
                list.Enabled = false;
                posActual = e.Position;
                Intent intent = new Intent(this, typeof(PedidoLineaActivity));
                intent.PutExtra("codigo", adapter.GetCodigo(e.Position));
                StartActivityForResult(intent, 1);
            };
            btnEliminar.Click += async (senderbtn, ebtn) =>
            {
                bool result=await ExecuteEliminarDetalle(e.Position);
                if (result)
                {
                    InicializarLista(e.Position);
                    builder.Dismiss();
                }
                else
                {
                    builder.Dismiss();
                } 
            };
            btnConsultar.Click += (senderbtn, ebtn) =>
            {
                builder.Dismiss();
                list.Enabled = false;
                Intent intent = new Intent(this, typeof(ConsulArticuloActivity));
                intent.PutExtra("codigo", this.adapter.GetCodigo(e.Position));
                intent.PutExtra("enpedido", true);
                intent.PutExtra("mainpedido", true);
                StartActivity(intent);
            };
            builder.Window.SetBackgroundDrawableResource(Resource.Color.semitransparent);
            builder.Show();           
            
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.search_main_pedidos, menu);
            item = menu.FindItem(Resource.Id.action_search);
            var searchView = MenuItemCompat.GetActionView(item);
            mSearchview = searchView.JavaCast<Android.Support.V7.Widget.SearchView>();
            mSearchview.QueryTextSubmit += (s, e) =>
            {
                adapter.Filter.InvokeFilter(e.Query);
            };
            mSearchview.QueryTextChange += (s, e) =>
            {
                if (string.IsNullOrEmpty(e.NewText))
                {
                    adapter.ResetSearch();
                }
                else
                {
                    adapter.Filter.InvokeFilter(e.NewText);
                }
            };
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    {
                        OnBackPressed();
                        break;
                    }
                case Resource.Id.menu_edit:
                    {
                        posActual = 0;
                        esperar = true;
                        Intent intent = new Intent(this, typeof(ArticulosPedidoActivity));
                        StartActivityForResult(intent,0);
                        break;
                    }
                case Resource.Id.menu_delete:
                    {
                        if (GestorPedido.PedidosCollection != null && GestorPedido.PedidosCollection.Gestionados.Count > 0)
                        {
                            EliminarLista();
                            break;
                            
                        }
                        else
                        {
                            Toast.MakeText(this, "No hay pedidos gestionados", ToastLength.Short).Show();
                        }
                        break;
                    }
                case Resource.Id.menu_info:
                    {
                        Toast.MakeText(this, GlobalUI.NameCliente, ToastLength.Long).Show();
                        break;
                    }
                case Resource.Id.menu_next:
                    {
                        if (GestorPedido.PedidosCollection != null && GestorPedido.PedidosCollection.Gestionados.Count > 0)
                        {
                            Intent intent = new Intent(this, typeof(PedidoReviewActivity));
                            StartActivityForResult(intent, 2);
                            break;
                        }
                        else
                        {
                            Toast.MakeText(this, "No hay pedidos gestionados", ToastLength.Short).Show();
                        }
                        break;
                    }
                case Resource.Id.menu_filtro:
                    {                        
                        Android.App.Dialog builder = new Android.App.Dialog(this);
                        builder.Window.RequestFeature(WindowFeatures.NoTitle);
                        builder.SetContentView(Resource.Layout.dialog_filtroart);
                        RadioButton rbCodigo = builder.FindViewById<RadioButton>(Resource.Id.rbCodigo);
                        RadioButton rbDescripcion = builder.FindViewById<RadioButton>(Resource.Id.rbDescripcion);
                        RadioButton rbBarras = builder.FindViewById<RadioButton>(Resource.Id.rbBarras);
                        RadioButton rbClasificacion = builder.FindViewById<RadioButton>(Resource.Id.rbClasificacion);
                        RadioGroup rgFiltros = builder.FindViewById<RadioGroup>(Resource.Id.rgFiltros);
                        rbCodigo.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rbDescripcion.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rbBarras.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rbClasificacion.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rgFiltros.CheckedChange += (sender, e) =>
                        {
                            if (rbCodigo.Checked)
                            {
                                this.item.SetEnabled(true);
                                App.FiltroArticulo = App.CodigoFilter;
                                Toast.MakeText(Android.App.Application.Context, "Filtrando art�culos por c�digo.", ToastLength.Short).Show();
                                builder.Dismiss();
                            }
                            else if (rbDescripcion.Checked)
                            {
                                this.item.SetEnabled(true);
                                App.FiltroArticulo = App.DescripcionFilter;
                                Toast.MakeText(Android.App.Application.Context, "Filtrando art�culos por descripci�n.", ToastLength.Short).Show();
                                builder.Dismiss();
                            }
                            else if(rbClasificacion.Checked)
                            {
                                this.item.SetEnabled(false);
                                App.FiltroArticulo = App.ClasificacionFilter;
                                builder.Dismiss();
                                Connection cnxVerif = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                                try 
                                {
                                    if (!Clasificacion.HayClasificaciones(App.CompaniaDefault, 1, cnxVerif) && !Clasificacion.HayClasificaciones(App.CompaniaDefault, 2, cnxVerif))
                                    {
                                        Toast.MakeText(Android.App.Application.Context, "No se encontraron clasificaciones disponibles", ToastLength.Short).Show();
                                        cnxVerif.Close();
                                        cnxVerif = null;
                                        rbCodigo.Checked = true;
                                        return;
                                    }
                                    else
                                    {
                                        Toast.MakeText(Android.App.Application.Context, "Filtrando art�culos por clasificaci�n.", ToastLength.Short).Show();
                                    }
                                    cnxVerif.Close();
                                    cnxVerif = null;
                                }
                                catch
                                {
                                    Toast.MakeText(Android.App.Application.Context, "No se encontraron clasificaciones", ToastLength.Short).Show();
                                    cnxVerif.Close();
                                    cnxVerif = null;
                                    return;
                                }
                                Android.App.Dialog builderClas = new Android.App.Dialog(this);
                                builderClas.Window.RequestFeature(WindowFeatures.NoTitle);
                                builderClas.SetContentView(Resource.Layout.dialog_clasificacion);
                                var label1 = builderClas.FindViewById<TextView>(Resource.Id.label1);
                                var label2 = builderClas.FindViewById<TextView>(Resource.Id.label2);
                                var txtFamilia = builderClas.FindViewById<TextView>(Resource.Id.txtFamilia);
                                var txtSubFamilia = builderClas.FindViewById<TextView>(Resource.Id.txtSubFamilia);
                                label1.SetTextColor(Android.Graphics.Color.White);
                                label2.SetTextColor(Android.Graphics.Color.White);
                                txtFamilia.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                                txtSubFamilia.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                                txtFamilia.Text = App.FamiliaActual;
                                txtSubFamilia.Text = App.SubFamiliaActual;
                                var imgEditarFamilia = builderClas.FindViewById<ImageButton>(Resource.Id.imgEditarFamilia);
                                var imgEliminarFamilia = builderClas.FindViewById<ImageButton>(Resource.Id.imgEliminarFamilia);
                                var imgEditarSubFamilia = builderClas.FindViewById<ImageButton>(Resource.Id.imgEditarSubFamilia);
                                var imgEliminarSubFamilia = builderClas.FindViewById<ImageButton>(Resource.Id.imgEliminarSubFamilia);
                                var btnAceptar = builderClas.FindViewById<Button>(Resource.Id.btnAceptar);
                                imgEliminarFamilia.Click += (s, events) => { txtFamilia.Text = string.Empty; };
                                imgEliminarSubFamilia.Click += (s, events) => { txtSubFamilia.Text = string.Empty; };
                                btnAceptar.Click += (s, events) =>
                                {
                                    App.FamiliaActual = txtFamilia.Text;
                                    App.SubFamiliaActual = txtSubFamilia.Text;
                                    if (string.IsNullOrEmpty(txtFamilia.Text) && string.IsNullOrEmpty(txtSubFamilia.Text))
                                    {
                                        if (adapter == null)
                                        {
                                            builderClas.Dismiss();
                                            return;
                                        }
                                        adapter.ResetSearch();
                                        builderClas.Dismiss();
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(txtFamilia.Text) && !string.IsNullOrEmpty(txtSubFamilia.Text))
                                        {
                                            adapter.Filter.InvokeFilter(txtFamilia.Text + "," + txtSubFamilia.Text);
                                        }
                                        else if (!string.IsNullOrEmpty(txtFamilia.Text))
                                        {
                                            adapter.Filter.InvokeFilter(txtFamilia.Text);
                                        }
                                        else
                                        {
                                            adapter.Filter.InvokeFilter(txtSubFamilia.Text);
                                        }
                                        builderClas.Dismiss();
                                    }
                                };
                                imgEditarFamilia.Click += (s, events) =>
                                {
                                    Android.App.Dialog buildertemp = new Android.App.Dialog(this);
                                    buildertemp.Window.RequestFeature(WindowFeatures.NoTitle);
                                    buildertemp.SetContentView(Resource.Layout.dialog_clasificacion_list);
                                    ListView list = buildertemp.FindViewById<ListView>(Resource.Id.list);
                                    List<Clasificacion> lista = new List<Clasificacion>();
                                    Connection cnxArticulo = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                                    try
                                    {
                                        lista = Clasificacion.ObtenerClasificaciones(App.CompaniaDefault, 1, cnxArticulo);
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    catch
                                    {
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    ClasificacionAdapter adapterClas = new ClasificacionAdapter(this, lista);
                                    list.Adapter = adapterClas;
                                    list.ItemClick += (ss, ee) =>
                                    {
                                        txtFamilia.Text = adapterClas.GetCodigo(ee.Position);
                                        buildertemp.Dismiss();
                                    };
                                    buildertemp.Window.SetBackgroundDrawableResource(Android.Resource.Drawable.DialogHoloLightFrame);
                                    buildertemp.Show();
                                };
                                imgEditarSubFamilia.Click += (s, events) =>
                                {
                                    Android.App.Dialog buildertemp = new Android.App.Dialog(this);
                                    buildertemp.Window.RequestFeature(WindowFeatures.NoTitle);
                                    buildertemp.SetContentView(Resource.Layout.dialog_clasificacion_list);
                                    ListView list = buildertemp.FindViewById<ListView>(Resource.Id.list);
                                    List<Clasificacion> lista = new List<Clasificacion>();
                                    Connection cnxArticulo = new Connection(System.IO.Path.Combine(App.PathAplicacion, App.BDName));
                                    try
                                    {
                                        lista = Clasificacion.ObtenerClasificaciones(App.CompaniaDefault, 2, cnxArticulo);
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    catch
                                    {
                                        cnxArticulo.Close();
                                        cnxArticulo = null;
                                    }
                                    ClasificacionAdapter adapterClas = new ClasificacionAdapter(this, lista);
                                    list.Adapter = adapterClas;
                                    list.ItemClick += (ss, ee) =>
                                    {
                                        txtSubFamilia.Text = adapterClas.GetCodigo(ee.Position);
                                        buildertemp.Dismiss();
                                    };
                                    buildertemp.Window.SetBackgroundDrawableResource(Android.Resource.Drawable.DialogHoloLightFrame);
                                    buildertemp.Show();
                                };
                                builderClas.Window.SetBackgroundDrawableResource(Resource.Color.semitransparent);
                                builderClas.Show();
                            }
                            else
                            {
                                this.item.SetEnabled(true);
                                App.FiltroArticulo = App.BarrasFilter;
                                Toast.MakeText(Android.App.Application.Context, "Filtrando art�culos por c�digo de barras.", ToastLength.Short).Show();
                                builder.Dismiss();
                            }

                        };
                        if (string.IsNullOrEmpty(App.FiltroArticulo))
                        {
                            App.FiltroArticulo = App.CodigoFilter;
                            rbCodigo.Checked = true;
                        }                         
                        else
                        {
                            if (App.FiltroArticulo.Equals(App.CodigoFilter) || App.FiltroArticulo.Equals(App.ClasificacionFilter))
                                rbCodigo.Checked = true;
                            else if (App.FiltroArticulo.Equals(App.DescripcionFilter))
                                rbDescripcion.Checked = true;
                            else
                                rbBarras.Checked = true;
                        }
                        builder.Window.SetBackgroundDrawableResource(Resource.Color.semitransparent);
                        builder.Show(); break;
                    }
            }
            return true;
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (resultCode == Result.Ok)
            {
                if (esperar)
                {                    
                    esperar = false;
                }
                adapter = null;
                InicializarLista(posActual);
            }
        }

        protected override void OnResume()
        {
            list.Enabled = true;
            base.OnResume();
        }
        

        void list_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            //list.Enabled = false;
            Android.App.Dialog builder = new Android.App.Dialog(this);
            builder.Window.RequestFeature(WindowFeatures.NoTitle);
            builder.SetContentView(Resource.Layout.dialog_cantidad);
            NumberPicker picker = builder.FindViewById<NumberPicker>(Resource.Id.pickerCantidad);
            Button btnAceptar = builder.FindViewById<Button>(Resource.Id.btnAceptar);
            Button btnCancelar = builder.FindViewById<Button>(Resource.Id.btnCancelar);
            picker.MinValue = 1;
            picker.MaxValue = 9999;
            picker.Value = Convert.ToInt32(adapter.getCantidadAlmacen(e.Position));
            btnAceptar.Click += async (s, events) =>
            {

                picker.ClearFocus();

                if (await ExecuteCambiarCantidad(e.Position, picker.Value))
                {
                    InicializarLista(e.Position);
                    builder.Dismiss();
                }
                else
                {
                    builder.Dismiss();
                }            
               
            };
            btnCancelar.Click += (s, events) =>
            {
                builder.Dismiss();
            };
            if (Android.OS.Build.VERSION.SdkInt.Equals(Android.OS.BuildVersionCodes.Lollipop))
                builder.Window.SetBackgroundDrawable(this.GetDrawable(Android.Resource.Drawable.DialogHoloLightFrame));
            else
                builder.Window.SetBackgroundDrawableResource(Android.Resource.Drawable.DialogHoloLightFrame);
            builder.Show();
        }

        private async void CargaInicial()
        {            
            bool result = await ExecuteConsultar();
            if (result && GestorPedido.PedidosCollection != null && GestorPedido.PedidosCollection.Gestionados.Count > 0)
            {
                CalcularImpVentas();  
                decimal montoBrutoBon = GestorPedido.PedidosCollection.Gestionados[0].MontoBrutoBonificaciones;
                decimal montoNetoRedondeo = 0;
                decimal diferenciaDescuento1 = 0;
                bool redondearFactura = GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.UsaRedondeo && GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.FactorRedondeo > 0;
                if (redondearFactura)
                {
                    montoNetoRedondeo = GestorUtilitario.TrunqueAFactor(GestorPedido.PedidosCollection.Gestionados[0].MontoNeto, GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.FactorRedondeo);
                    diferenciaDescuento1 = GestorPedido.PedidosCollection.Gestionados[0].MontoNeto - montoNetoRedondeo;
                    diferenciaDescuento1 = Math.Abs(diferenciaDescuento1);
                }
                if (GlobalUI.ClienteActual.ObtenerClienteCia(App.CompaniaDefault).NivelPrecio.Moneda == TipoMoneda.LOCAL)
                {
                    if (redondearFactura)
                        txtTotal.Text = GestorUtilitario.FormatNumero(montoNetoRedondeo);
                    else
                        txtTotal.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalNeto);
                }
                else
                {
                    if (redondearFactura)
                        txtTotal.Text = GestorUtilitario.FormatNumeroDolar(montoNetoRedondeo);
                    else
                        txtTotal.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalNeto);
                }
            }
            else
            {
                if (GlobalUI.ClienteActual.ObtenerClienteCia(App.CompaniaDefault).NivelPrecio.Moneda == TipoMoneda.LOCAL)
                    txtTotal.Text = GestorUtilitario.FormatNumero(0);
                else
                    txtTotal.Text = GestorUtilitario.FormatNumeroDolar(0);
            }
        }        

        public override void OnBackPressed()
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.SetTitle(string.Empty);
            alert.SetMessage("�Desea salir del pedido?");
            alert.SetPositiveButton("Si", delegate {
                if (GestorPedido.PedidosCollection != null && GestorPedido.PedidosCollection.Gestionados.Count > 0)
                {
                    AlertDialog.Builder alertb = new AlertDialog.Builder(this);
                    alertb.SetTitle(string.Empty);
                    alertb.SetMessage("�Desea guardar el pedido en memoria para continuar despu�s?");
                    alertb.SetPositiveButton("Si", delegate
                    {
                        var intent = new Intent(this, typeof(MainActivity));
                        intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
                        this.StartActivity(intent);
                        Finish();
                    });
                    alertb.SetNegativeButton("No", delegate
                    {
                        GestorPedido.PedidosCollection.Gestionados.Clear();
                        GC.Collect(GC.MaxGeneration);
                        var intent = new Intent(this, typeof(MainActivity));
                        intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
                        this.StartActivity(intent);
                        Finish();
                    });
                    alertb.Show();
                }
                else
                {
                    GestorPedido.PedidosCollection.Gestionados.Clear();
                    GC.Collect(GC.MaxGeneration);
                    var intent = new Intent(this, typeof(MainActivity));
                    intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
                    this.StartActivity(intent);
                    Finish();
                }                
            });
            alert.SetNegativeButton("No", delegate { return; });
            alert.Show();
        }
        

        #region metodos      
  
        private async void InicializarLista()
        {
            bool res = await ExecuteConsultar();
            if (res && GestorPedido.PedidosCollection != null && GestorPedido.PedidosCollection.Gestionados.Count > 0)
            {
                CalcularImpVentas();
                decimal montoBrutoBon = GestorPedido.PedidosCollection.Gestionados[0].MontoBrutoBonificaciones;
                decimal montoNetoRedondeo = 0;
                decimal diferenciaDescuento1 = 0;
                bool redondearFactura = GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.UsaRedondeo && GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.FactorRedondeo > 0;
                if (redondearFactura)
                {
                    montoNetoRedondeo = GestorUtilitario.TrunqueAFactor(GestorPedido.PedidosCollection.Gestionados[0].MontoNeto, GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.FactorRedondeo);
                    diferenciaDescuento1 = GestorPedido.PedidosCollection.Gestionados[0].MontoNeto - montoNetoRedondeo;
                    diferenciaDescuento1 = Math.Abs(diferenciaDescuento1);
                }
                if (GlobalUI.ClienteActual.ObtenerClienteCia(App.CompaniaDefault).NivelPrecio.Moneda == TipoMoneda.LOCAL)
                {
                    if (redondearFactura)
                        txtTotal.Text = GestorUtilitario.FormatNumero(montoNetoRedondeo);
                    else
                        txtTotal.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalNeto);
                }
                else
                {
                    if (redondearFactura)
                        txtTotal.Text = GestorUtilitario.FormatNumeroDolar(montoNetoRedondeo);
                    else
                        txtTotal.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalNeto);
                }
            }
            else
            {
                if (GlobalUI.ClienteActual.ObtenerClienteCia(App.CompaniaDefault).NivelPrecio.Moneda == TipoMoneda.LOCAL)
                    txtTotal.Text = GestorUtilitario.FormatNumero(0);
                else
                    txtTotal.Text = GestorUtilitario.FormatNumeroDolar(0);
            }
        }

        private async void EliminarLista()
        {
            bool res = await ExecuteEliminarDetalles();
            if (res)
            {
                res = await ExecuteConsultar();
                if (res && GestorPedido.PedidosCollection != null && GestorPedido.PedidosCollection.Gestionados.Count > 0)
                {
                    CalcularImpVentas();
                    decimal montoBrutoBon = GestorPedido.PedidosCollection.Gestionados[0].MontoBrutoBonificaciones;
                    decimal montoNetoRedondeo = 0;
                    decimal diferenciaDescuento1 = 0;
                    bool redondearFactura = GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.UsaRedondeo && GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.FactorRedondeo > 0;
                    if (redondearFactura)
                    {
                        montoNetoRedondeo = GestorUtilitario.TrunqueAFactor(GestorPedido.PedidosCollection.Gestionados[0].MontoNeto, GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.FactorRedondeo);
                        diferenciaDescuento1 = GestorPedido.PedidosCollection.Gestionados[0].MontoNeto - montoNetoRedondeo;
                        diferenciaDescuento1 = Math.Abs(diferenciaDescuento1);
                    }
                    if (GlobalUI.ClienteActual.ObtenerClienteCia(App.CompaniaDefault).NivelPrecio.Moneda == TipoMoneda.LOCAL)
                    {
                        if (redondearFactura)
                            txtTotal.Text = GestorUtilitario.FormatNumero(montoNetoRedondeo);
                        else
                            txtTotal.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalNeto);
                    }
                    else
                    {
                        if (redondearFactura)
                            txtTotal.Text = GestorUtilitario.FormatNumeroDolar(montoNetoRedondeo);
                        else
                            txtTotal.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalNeto);
                    }
                }
                else
                {
                    if (GlobalUI.ClienteActual.ObtenerClienteCia(App.CompaniaDefault).NivelPrecio.Moneda == TipoMoneda.LOCAL)
                        txtTotal.Text = GestorUtilitario.FormatNumero(0);
                    else
                        txtTotal.Text = GestorUtilitario.FormatNumeroDolar(0);
                }
            }
            
        }

        private async void InicializarLista(int position)
        {
            var res = await ExecuteConsultar(position);

            if (res && GestorPedido.PedidosCollection != null && GestorPedido.PedidosCollection.Gestionados.Count > 0)
            {
                Pedido pedido = GestorPedido.PedidosCollection.Gestionados[0];

                // -------------------------------------------------------------------------------------------------------
                // Estas dos variables se crearon para probar si es posible obtener la cantidad de articulos y la cantidad
                // de l�neas en el pedido para luego hacer controles visuales donde mostrar estos datos.
                var totalArticulos = pedido.Detalles.TotalArticulos;
                    var totalLineas = pedido.Detalles.TotalLineas;
                // -------------------------------------------------------------------------------------------------------

                CalcularImpVentas();

                decimal montoBrutoBon = GestorPedido.PedidosCollection.Gestionados[0].MontoBrutoBonificaciones;
                decimal montoNetoRedondeo = 0;
                decimal diferenciaDescuento1 = 0;
                bool redondearFactura = GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.UsaRedondeo && GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.FactorRedondeo > 0;

                if (redondearFactura)
                {
                    montoNetoRedondeo = GestorUtilitario.TrunqueAFactor(GestorPedido.PedidosCollection.Gestionados[0].MontoNeto, GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.FactorRedondeo);
                    diferenciaDescuento1 = GestorPedido.PedidosCollection.Gestionados[0].MontoNeto - montoNetoRedondeo;
                    diferenciaDescuento1 = Math.Abs(diferenciaDescuento1);
                }

                if (GlobalUI.ClienteActual.ObtenerClienteCia(App.CompaniaDefault).NivelPrecio.Moneda == TipoMoneda.LOCAL)
                {
                    if (redondearFactura)
                        txtTotal.Text = GestorUtilitario.FormatNumero(montoNetoRedondeo);
                    else
                        txtTotal.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalNeto);
                }
                else
                {
                    if (redondearFactura)
                        txtTotal.Text = GestorUtilitario.FormatNumeroDolar(montoNetoRedondeo);
                    else
                        txtTotal.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalNeto);
                }
            }
            else
            {
                if (GlobalUI.ClienteActual.ObtenerClienteCia(App.CompaniaDefault).NivelPrecio.Moneda == TipoMoneda.LOCAL)
                    txtTotal.Text = GestorUtilitario.FormatNumero(0);
                else
                    txtTotal.Text = GestorUtilitario.FormatNumeroDolar(0);
            }
          
        }     

        private Task<bool> ExecuteConsultar()
        {
            GC.Collect(GC.MaxGeneration);
            return Task.Run(() =>
            {
                Android.App.ProgressDialog progressDialog = null;
                try
                {
                    RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(this, "Cargando", "Por favor espere...", true); });                    
                    if (GestorPedido.PedidosCollection != null && GestorPedido.PedidosCollection.Gestionados.Count > 0)
                    {
                        RunOnUiThread(() => { adapter = new DetallePedidoAdapter(this, GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista, true); list.Adapter = adapter; });
                        RunOnUiThread(() => { progressDialog.Dismiss(); });
                    }
                    else
                    {
                        var lista = new List<DetallePedido>();
                        RunOnUiThread(() => { adapter = new DetallePedidoAdapter(this, lista, true); list.Adapter = adapter; });
                        RunOnUiThread(() => { progressDialog.Dismiss(); });
                    }
                    return true;
                }
                catch
                {
                    RunOnUiThread(() => { progressDialog.Dismiss(); });
                    RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error inicializando lista.", ToastLength.Short).Show(); });
                    return false;
                }
            });
        }

        private Task<bool> ExecuteConsultar(int position)
        {

            return Task.Run(() =>
            {
                Android.App.ProgressDialog progressDialog = null;
                try
                {
                    RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(this, "Cargando", "Por favor espere...", true); });
                    if (GestorPedido.PedidosCollection != null && GestorPedido.PedidosCollection.Gestionados.Count > 0)
                    {
                        RunOnUiThread(() => { adapter = new DetallePedidoAdapter(this, GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista, true); list.Adapter = adapter; });
                        RunOnUiThread(() => { progressDialog.Dismiss(); list.SetSelection(position); });
                    }
                    else
                    {
                        var lista = new List<DetallePedido>();
                        RunOnUiThread(() => { adapter = new DetallePedidoAdapter(this, lista, true); list.Adapter = adapter; });
                        RunOnUiThread(() => { progressDialog.Dismiss(); });
                    }
                    return true;
                }
                catch
                {
                    RunOnUiThread(() => { progressDialog.Dismiss(); });
                    RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error inicializando lista.", ToastLength.Short).Show(); });
                    return false;
                }
            });
        }

        private Task<bool> ExecuteCambiarCantidad(int position,int cantidad)
        {

            return Task.Run(() =>
            {
                Android.App.ProgressDialog progressDialog = null;
                try
                {
                    RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(this, "Cambiando cantidad", "Por favor espere...", true); });
                    RunOnUiThread(() => { Toast.MakeText(this, "Nueva cantidad art�culo " + adapter.GetCodigo(position) + "   " + cantidad, ToastLength.Long).Show(); });
                    GestorPedido.AgregarModificarDetalle(adapter.getArticuloLinea(position), Convert.ToDecimal(cantidad),adapter.getCantidadDetalle(position));
                    //adapter.CambiarCantidad(position, cantidad);
                    RunOnUiThread(() => { progressDialog.Dismiss(); });
                    RunOnUiThread(() => { Toast.MakeText(this, "Bonificaciones y Descuentos recalculados.",ToastLength.Short).Show(); });
                    return true;
                }
                catch
                {
                    RunOnUiThread(() => { progressDialog.Dismiss(); });
                    RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error al actualizar cantidad.", ToastLength.Short).Show(); });
                    return false;
                }
            });
        }

        private void CalcularImpVentas()
        {
            try
            {
                foreach (Pedido pedido in GestorPedido.PedidosCollection.Gestionados)
                {
                    pedido.RecalcularImpuestos(pedido.MontoSubTotal < pedido.Configuracion.Compania.MontoMinimoExcento);
                }
                GestorPedido.PedidosCollection.SacarMontosTotales();
            }
            catch
            {
                Toast.MakeText(this, "Error al recalcular montos. ", ToastLength.Short).Show();
            }
        }

        private Task<bool> ExecuteEliminarDetalle(int position)
        {

            return Task.Run(() =>
            {
                Android.App.ProgressDialog progressDialog = null;
                try
                {
                    RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(this, "Eliminando detalle", "Por favor espere...", true); });
                    GestorPedido.RetirarDetalle(adapter.getArticuloLinea(position));
                    CalcularImpVentas();
                    //adapter.CambiarCantidad(position, cantidad);
                    RunOnUiThread(() => { progressDialog.Dismiss(); });
                    //RunOnUiThread(() => { Toast.MakeText(this, "Bonificaciones y Descuentos recalculados.", ToastLength.Short).Show(); });
                    return true;
                }
                catch
                {
                    RunOnUiThread(() => { progressDialog.Dismiss(); });
                    RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error eliminando Detalle.", ToastLength.Short).Show(); });
                    return false;
                }
            });
        }

        private Task<bool> ExecuteEliminarDetalles()
        {

            return Task.Run(() =>
            {
                Android.App.ProgressDialog progressDialog = null;
                try
                {
                    RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(this, "Eliminando detalles", "Por favor espere...", true); });
                    if (GestorPedido.PedidosCollection != null && GestorPedido.PedidosCollection.Gestionados != null && GestorPedido.PedidosCollection.Gestionados.Count > 0)
                    {
                        List<Articulo> listadetalles = new List<Articulo>();
                        foreach (DetallePedido det in GestorPedido.PedidosCollection.Gestionados[0].Detalles.Lista)
                        {
                            listadetalles.Add(det.Articulo);
                        }
                        foreach(Articulo art in listadetalles)
                        {
                            GestorPedido.RetirarDetalle(art);
                        }
                    }
                    CalcularImpVentas();
                    //adapter.CambiarCantidad(position, cantidad);
                    RunOnUiThread(() => { progressDialog.Dismiss(); });
                    //RunOnUiThread(() => { Toast.MakeText(this, "Bonificaciones y Descuentos recalculados.", ToastLength.Short).Show(); });
                    return true;
                }
                catch
                {
                    RunOnUiThread(() => { progressDialog.Dismiss(); });
                    RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error eliminando Detalle.", ToastLength.Short).Show(); });
                    return false;
                }
            });
        }

        



        #endregion


    }
}