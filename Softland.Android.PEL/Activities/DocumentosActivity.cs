using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BI.Shared;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Data.SQLite;
using BI.Android;
using Pedidos.Core;
using Android.Support.V4.View;

namespace Softland.Droid.PEL
{
    [Activity(Label = "Documentos Pendientes", Icon = "@drawable/ic_launcher", ParentActivity = typeof(ConsulClienteActivity), ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
    public class DocumentosActivity : BaseActivity
    {        
        ListView list;
        DocumentosAdapter adapter;
        TextView txtTotal;
        TextView txtTotalSaldo;
        TextView txtTotalDolar;
        TextView txtTotalSaldoDolar;
        string codCliente;
        bool dolar;

        protected override int LayoutResource
        {
            get
            {
                return Resource.Layout.documentos_cliente;
            }
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            txtTotal = FindViewById<TextView>(Resource.Id.txtTotal);
            txtTotalSaldo = FindViewById<TextView>(Resource.Id.txtTotalSaldo);
            txtTotalDolar = FindViewById<TextView>(Resource.Id.txtTotalDolar);
            txtTotalSaldoDolar = FindViewById<TextView>(Resource.Id.txtTotalSaldoDolar);
            list = FindViewById<ListView>(Resource.Id.list);
            // Create your application here               
            adapter = null;
            codCliente = Intent.GetStringExtra("codigo");
            dolar = false;
            InicializarLista();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.documentos_cliente, menu);            
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    {
                        OnBackPressed();
                        break;
                    }
                case Resource.Id.menu_filtro:
                    {                        
                        Android.App.Dialog builder = new Android.App.Dialog(this);
                        builder.Window.RequestFeature(WindowFeatures.NoTitle);
                        builder.SetContentView(Resource.Layout.dialog_documentos);
                        RadioButton rbLocal = builder.FindViewById<RadioButton>(Resource.Id.rbLocal);
                        RadioButton rbDolar = builder.FindViewById<RadioButton>(Resource.Id.rbDolar);
                        RadioGroup rgFiltros = builder.FindViewById<RadioGroup>(Resource.Id.rgFiltros);
                        rbLocal.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rbDolar.SetTextColor(Android.Graphics.Color.WhiteSmoke);
                        rgFiltros.CheckedChange += (sender, e) =>
                        {
                            if (rbLocal.Checked)
                            {
                                dolar = false;
                                InicializarLista();
                                Toast.MakeText(Android.App.Application.Context, "Filtrando documentos por moneda local.", ToastLength.Short).Show();
                                builder.Dismiss();
                            }
                            else if (rbDolar.Checked)
                            {
                                dolar = true;
                                InicializarLista();
                                Toast.MakeText(Android.App.Application.Context, "Filtrando documentos por moneda dolar.", ToastLength.Short).Show();
                                builder.Dismiss();
                            }                            

                        };                        
                        builder.Window.SetBackgroundDrawableResource(Resource.Color.semitransparent);
                        builder.Show(); break;
                    }
            }
            return true;
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (resultCode == Result.Ok)
            {
                    adapter = null;
                    InicializarLista();
            }
        }        

        public override void OnBackPressed()
        {            
            var intent = new Intent(this, typeof(ConsulClienteActivity));
            intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
            this.StartActivity(intent);
            Finish();
        }
        

        #region metodos      
  
        private async void InicializarLista()
        {
            bool res = await ExecuteConsultar();        
        }    

        private Task<bool> ExecuteConsultar()
        {

            return Task.Run(() =>
            {
                Android.App.ProgressDialog progressDialog = null;
                try
                {
                    RunOnUiThread(() => { progressDialog = Android.App.ProgressDialog.Show(this, "Cargando", "Por favor espere...", true); });
                    var facs = Factura.ObtenerFacturasPendientesCobro(App.CompaniaDefault, codCliente, App.DeviceID).ConvertAll<DocumentoContable>(x => (DocumentoContable)x);
                    RunOnUiThread(() => { adapter = new DocumentosAdapter(this, facs, dolar); list.Adapter = adapter; });
                    RunOnUiThread(() => {
                        txtTotal.Text = GestorUtilitario.FormatNumero(facs.Sum(x => x.MontoDocLocal));
                        txtTotalSaldo.Text = GestorUtilitario.FormatNumero(facs.Sum(x => x.SaldoDocLocal));
                        txtTotalDolar.Text = GestorUtilitario.FormatNumeroDolar(facs.Sum(x => x.MontoDocDolar));
                        txtTotalSaldoDolar.Text = GestorUtilitario.FormatNumeroDolar(facs.Sum(x => x.SaldoDocDolar));
                        progressDialog.Dismiss(); 
                    });                    
                    return true;
                }
                catch
                {
                    RunOnUiThread(() =>
                    {
                        txtTotal.Text = GestorUtilitario.FormatNumero(0);
                        txtTotalSaldo.Text = GestorUtilitario.FormatNumero(0);
                        txtTotalDolar.Text = GestorUtilitario.FormatNumeroDolar(0);
                        txtTotalSaldoDolar.Text = GestorUtilitario.FormatNumeroDolar(0);
                        progressDialog.Dismiss();
                    }); 
                    RunOnUiThread(() => { Toast.MakeText(Android.App.Application.Context, "Error inicializando lista.", ToastLength.Short).Show(); });
                    return false;
                }
            });
        }


        #endregion


    }
}