using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

namespace Softland.iOS.Pem.Modelo
{
    public class Cliente
    {
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public double latitud { get; set; }
        public double longitud { get; set; }
    }
}