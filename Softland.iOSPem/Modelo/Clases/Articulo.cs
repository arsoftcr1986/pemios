using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

namespace Softland.iOS.Pem.Modelo
{
    public class Articulo
    {
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public double Cantidad { get; set; }
        public string Imagen { get; set; }
    }
}