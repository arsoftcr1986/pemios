﻿using System;
using System.Drawing;
using System.Data.SQLite;
using System.Globalization;
using System.Threading.Tasks;

using BI.iOS;
using BI.Shared;
using Pedidos.Core;

using UIKit;
using Foundation;
using System.Data.SQLiteBase;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Softland.iOS.Pem
{
    public partial class RootViewController : UIViewController
    {

        #region Propiedades y Contructor de instancia

        /// <summary>
        /// Variable que almacena si debe hacerse una sincronización inicial de datos antes de iniciar sesión.
        /// </summary>
        private bool Sincronizar = true;

        /// <summary>
        /// Controlador que sirve para mostrar una pantalla de espera o de carga.
        /// </summary>
        static LoadingOverlay loadingOverlay;

        /// <summary>
        /// Variable que almacena la cantidad de intentos permitidos que tiene el usuario para iniciar sesión.
        /// </summary>
        private int intentosPermitidos;

        /// <summary>
        /// Variable que almacena la cantidad de intentos que el usuario lleva iniciando sesión.
        /// </summary>
        private int numeroIntentos;

        /// <summary>
        /// Variable que almacena si debe permitirse que se efectue o no el cambio de pantalla (ingresar).
        /// </summary>
        private bool doSegue = false;

        /// <summary>
        /// Variable que almacena los valores para generar el color verde en los mensajes de pantalla de exito.
        /// </summary>
        public static UIColor verdeOscuro = UIColor.FromRGB(0, 200, 0);

        /// <summary>
        /// Costructor por defecto.
        /// </summary>
        /// <param name="handle"></param>
        public RootViewController(IntPtr handle) : base(handle)
        {
        }

        #endregion

        #region Eventos

        /// <summary>
        /// Evento que se ejecuta al iniciar la navegación de una pantalla a otra usando un Segue (antes que PrepareForSegue).
        /// Se usa para validar si se debe o no completar el cambio de pantalla, validando si la información del usuario 
        /// es valida y si se debe permitir ingresar al resto de la aplicación o notificar que los datos son incorrectos o
        /// si debe primero hacerse una sincronización de la base de datos.
        /// </summary>
        /// <param name="segue"></param>
        /// <param name="sender"></param>
        public override bool ShouldPerformSegue(string segueIdentifier, NSObject sender)
        {
            doSegue = false;

            txt_Usuario.ResignFirstResponder();
            txt_Password.ResignFirstResponder();

            // Si debe sincronizar primero la base de datos con el servidor.
            if (Sincronizar)
            {
                // Requerir que ingrese el nombre de usuario para proveerlo al servidor.
                if (!string.IsNullOrEmpty(txt_Usuario.Text))
                {
                    // Requerir que ingrese la dirección del servidor con el que sincronizar.
                    if (!string.IsNullOrEmpty(AppDelegate.Servidor))
                    {
                        // Iniciar el proceso de sincronización.
                        IniciarSincronizacion();
                    }
                    else
                    {
                        Toast.MakeToast(View, "Ingrese la dirección del servidor con el que desea conectar, utilice el botón en la esquina superior derecha.", null, UIColor.Black, false, enumDuracionToast.Corto);
                    }
                }
                else
                {
                    Toast.MakeToast(View, "Ingrese su usuario para iniciar la primera sincronizacion.", null, UIColor.Black, false, enumDuracionToast.Corto);
                }
            }
            else
            {
                // Si la base de datos está sincronizada, validar el ingreso del usuario.

                try
                {
                    // Iniciar el proceso de inicio de sesión.
                    this.LoginProcess();
                }
                catch (Exception ex)
                {
                    Toast.MakeToast(View, "Ocurrió un error al ingresar." + ex.Message, null, UIColor.Black, false, enumDuracionToast.Corto);
                }
            }

            // Si doSegue es True, se mostrará la nueva pantalla del TabView, de lo contrario evitará que se cambie de pantalla.
            return doSegue;
        }

        /// <summary>
        /// Evento que se ejecuta al iniciar la navegación de una pantalla a otra usando un Segue.
        /// Se usa para limpiar la información en los campos de texto de usuario y contraseña.
        /// </summary>
        /// <param name="segue"></param>
        /// <param name="sender"></param>
        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);

            txt_Password.Text = "";
            txt_Usuario.Text = "";
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Método que inicia el proceso de sincronización, asigna las variables del hilo nuevo que se creará para procesar la
        /// sincronización y prepara el loadingOverlay para ser mostrado en pantalla.
        /// </summary>
        private void IniciarSincronizacion()
        {
            InvokeOnMainThread(async () =>
            {
                // Determine the correct size to start the overlay (depending on device orientation)
                var bounds = UIScreen.MainScreen.Bounds; // portrait bounds
                if (UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft || UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight)
                {
                    bounds.Size = new SizeF((float)bounds.Size.Width, (float)bounds.Size.Height);
                }
                // show the loading overlay on the UI thread using the correct orientation sizing
                loadingOverlay = new LoadingOverlay((RectangleF)bounds);

                ParentViewController.Add(loadingOverlay);

                // Se muestra el indicador de actividad con la red.
                UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;

                // Se crea un ID para la tarea que se ejecutará, esto permite que el OS de oportunidad de tener una
                // tarea en el background por un tiempo de máximo 10 minutos antes de terminar la aplicacion por si mismo. 
                nint taskID = UIApplication.SharedApplication.BeginBackgroundTask(() =>
                {
                    // Evento en caso de que el timepo restante de la tarea esté a punto de terminar, se debe hacer 
                    // una limpieza de las variables usadas o terminar la tarea para evitar una terminación
                    // total de la aplicación por el OS.

                    double timeRemaining = UIApplication.SharedApplication.BackgroundTimeRemaining;

                    if (timeRemaining < 20)
                    {
                        Toast.MakeToast(View, "La tarea de sincronizacion va a expirar", null, UIColor.Black, true, enumDuracionToast.Mediano);
                    }
                });

                // Se llama al método que se encarga de realizar la sincronización.
                string result = await SincronizacionDatos(txt_Usuario.Text);

                // Se oculta el indicador de actividad con la red.
                UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
                
                // Si la sincronización terminó exitosamente.
                if (result.Equals("Sincronización realizada correctamente."))
                {
                    try
                    {
                        // Se vuelven a asignar los valores de las variables globales que trabajan con la base de datos.

                        // Se crea una nueva conexión con la base de datos recien sincronizada.
                        AppDelegate.cnx = new Connection(System.IO.Path.Combine(AppDelegate.RutaSD, AppDelegate.BaseDatos));
                        
                        // Se asigna la conexión con la referencia en el Gestor de datos.
                        GestorDatos.cnx = AppDelegate.cnx;

                        // Se vuelve a cargar todos los datos requeridos por la aplicación.
                        GlobalUI.Rutas = Ruta.ObtenerRutas();
                        GlobalUI.CargaParametrosX(false);
                        GlobalUI.ValidarConfiguracion();

                        // Se asgina que ya no es requerido sincronizar la aplicación.
                        Sincronizar = false;

                        btn_Iniciar.SetTitle("Ingresar", UIControlState.Normal);

                        UIAlertView alert = new UIAlertView("Proceso finalizado", result, null, "OK", null);

                        // Evento click del botón de Aceptar.
                        alert.Clicked += (s2, b2) =>
                        {
                            // Si la contraseña no está vacia se intentará loguear al usuario automáticamente.
                            if (!string.IsNullOrEmpty(txt_Password.Text))
                            {
                                doSegue = false;

                                // Iniciar el proceso de inicio de sesión.
                                this.LoginProcess();

                                // Si se validó correctamente la información del usuario.
                                if (doSegue)
                                {
                                    // Se referencia al Storyboard del app.
                                    var myStoryboard = AppDelegate.Storyboard;

                                    // Se instancia un ViewController con el ID del Storyboard y su clase respectiva.
                                    var TabView = myStoryboard.InstantiateViewController("VistaTabs") as VistaTabs;

                                    // Se navega a la pantalla instanciada mostrandola de forma Modal (Sobre de la pantalla actual).
                                    NavigationController.PresentViewController(TabView, true, () => { });
                                }
                            }
                        };

                        alert.Show();
                    }
                    catch
                    {
                        // Indicar al usuario que necesita sincronizar nuevamente.
                        Toast.MakeToast(View, "No se pudo establecer conexión con la BD, sincronice nuevamente la aplicación.", null, UIColor.Black, true, enumDuracionToast.Mediano);
                    }
                }
                else
                { 
                    // Se notifica de algún error que ocurrió con la sincronización.
                    new UIAlertView("Sincronización detenida", result, null, "OK", null).Show();
                }

                // Se termina el proceso que se inició con el ID establecido, para evitar que el OS temine la aplicación.
                UIApplication.SharedApplication.EndBackgroundTask(taskID);
            });
        }

        /// <summary>
        /// Método asíncrono que se encarga de realizar todo el proceso de sincronizar los datos con el servidor
        /// para obtener toda la información de la base de datos (No carga imagenes ni sincroniza pedidos creados),
        /// para tener la versión más actualizada de la base de datos del ERP.
        /// </summary>
        /// <returns>Retorna un mensaje como resultado del proceso.</returns>
        private Task<string> SincronizacionDatos(string usuario)
        {
            return Task.Run(() =>
            {
                try
                {
                    // Se obtiene el UDID para ser enviado al servidor para validar si el dispositivo
                    // se encuentra ya configurado en el administrador.
                    NSUuid identifier = UIDevice.CurrentDevice.IdentifierForVendor;
                    string uuid = identifier.AsString();

                    // Se crea la referencia a la conexión con el servidor para manejar el proceso, enviando
                    // el nombre de usuario como referencia para el administrador.
                    conexion con = new conexion(uuid, AppDelegate.Servidor, usuario);

                    // Iniciamos una instancia de conexión con la base de datos para actualizar su información.
                    AppDelegate.cnx = new Connection(System.IO.Path.Combine(AppDelegate.RutaSD, AppDelegate.BaseDatos));

                    // Variable que tendrá el mensaje de retorno del proceso.
                    string error = string.Empty;

                    InvokeOnMainThread(() => { loadingOverlay.UpdateMessage("Generando Datos..."); });

                    // Se obtiene las sentencias de creación y llenado de la base de datos.
                    if (!con.ObtenerSentenciasServidor(ref error))
                    {
                        AppDelegate.cnx.Close();
                        InvokeOnMainThread(() => { loadingOverlay.Hide(); });

                        return error;
                    }

                    InvokeOnMainThread(() => { loadingOverlay.UpdateMessage("Preparando Sincronización..."); });

                    // Se crea o actualiza la estructura de toda la base de datos.
                    if (con.CrearEstructuraDinamica(AppDelegate.RutaSD, ref error, AppDelegate.cnx))
                    {
                        AppDelegate.cnx.Close();
                    }
                    else
                    {
                        AppDelegate.cnx.Close();
                        InvokeOnMainThread(() => { loadingOverlay.Hide(); });
                        return error;
                    }

                    InvokeOnMainThread(() => { loadingOverlay.UpdateMessage("Obteniendo Datos..."); });

                    // Se descarga el archivo comprimido de las sentencias.
                    if (con.continuarInsertar && !con.DescargarArchivoSincro(AppDelegate.RutaSD, ref error))
                    {
                        InvokeOnMainThread(() => { loadingOverlay.Hide(); });
                        return error;
                    }

                    InvokeOnMainThread(() => { loadingOverlay.UpdateMessage("Procesando Datos..."); });

                    // Se descomprime el archivo descargado en una carpeta temporal.
                    if (con.continuarInsertar && !con.DescomprimirArchivoSincro(AppDelegate.RutaSD, ref error))
                    {
                        InvokeOnMainThread(() => { loadingOverlay.Hide(); });
                        return error;
                    }

                    InvokeOnMainThread(() => { loadingOverlay.UpdateMessage("Actualizando Información..."); });

                    AppDelegate.cnx.Open();

                    // Se insertan todos los datos obtenidos desde el servidor en la base de datos.
                    if (con.continuarInsertar && con.InsertDatosDinamico(AppDelegate.RutaSD, ref error, AppDelegate.cnx))
                    {
                        AppDelegate.cnx.Close();
                        InvokeOnMainThread(() => { loadingOverlay.Hide(); });

                        if (!string.IsNullOrEmpty(con.atrWebSoporte))
                        {
                            AppDelegate.webSoporte = con.atrWebSoporte;

                            if (!AppDelegate.webSoporte.Contains("http://"))
                            {
                                AppDelegate.webSoporte = "http://" + AppDelegate.webSoporte;
                            }

                            AppDelegate.SetConfigVar(AppDelegate.webSoporte, "websoporte");
                        }

                        AppDelegate.CambiarDescuento = con.atrCambiarDescuento;

                        AppDelegate.SetConfigVar(AppDelegate.CambiarDescuento, "cambiardescuento");

                        return "Sincronización realizada correctamente.";
                    }
                    else
                    {

                        AppDelegate.cnx.Close();
                        InvokeOnMainThread(() => { loadingOverlay.Hide(); });

                        return error;
                    }
                }
                catch(Exception ex)
                {
                    InvokeOnMainThread(() => { loadingOverlay.Hide(); });

                    return "Ocurrió un error al crear la base de datos, inténtelo nuevamente.";
                }
            });
        }

        /// <summary>
        /// Método que llama al proceso de validar al usuario.
        /// </summary>
        public void LoginProcess()
        {
            this.ValidarIngreso();
        }


        /// <summary>
        /// Método que valida los datos ingresados de usuario y contraseña, además de los intentos de ingresar.
        /// </summary>
        public void ValidarIngreso()
        {
            // Validar que se ingresó el nombre de usuario.
            if (txt_Usuario.Text == string.Empty)
            {
                Toast.MakeToast(View, "Digite el Usuario", null, UIColor.Black, false, enumDuracionToast.Corto);
            }
            // Validar que se ingresó la contraseña.
            else if (txt_Password.Text == string.Empty)
            {
                Toast.MakeToast(View, "Digite la contraseña", null, UIColor.Black, false, enumDuracionToast.Corto);
            }
            else
            {
                // Si los datos están ingresados.

                txt_Usuario.Text = txt_Usuario.Text.ToUpper();

                // Se cargan las variables Globales de la aplicación.
                FRdConfig.CargarGlobales();

                // Valida si se sigue intentando ingresar con otro nombre de usuario.
                if (txt_Usuario.Text != GestorDatos.NombreUsuario)
                {
                    // Se asignan los valores globales de nombre de usuario y contraseña.
                    GestorDatos.NombreUsuario = txt_Usuario.Text;
                    GestorDatos.ContrasenaUsuario = txt_Password.Text.ToUpper();

                    try
                    {
                        // Como el usuario cambió, se vuelve a obtener los intentos permitidos.
                        intentosPermitidos = GestorSeguridad.VerificarUsuario(txt_Usuario.Text);
                        //intentosPermitidos = 3;
                    }
                    catch (Exception ex)
                    {
                        Toast.MakeToast(View, "Verificando el Usuario." + ex.Message, null, UIColor.Black, false, enumDuracionToast.Corto);
                        return;
                    }

                    // Se asigna el número de intentos de usuario a 0.
                    numeroIntentos = 0;

                    // Si el usuario no tiene definido el valor de intentos permitidos.
                    if (intentosPermitidos == -1)
                    {
                        Toast.MakeToast(View, "Usuario inválido", null, UIColor.Black, false, enumDuracionToast.Corto);
                        txt_Password.Text = string.Empty;
                        txt_Usuario.Text = string.Empty;
                        return;
                    }

                    // Si el usuario ya no puede seguir intentando ingresar a la aplicación.
                    if (intentosPermitidos == 0)
                    {
                        Toast.MakeToast(View, "Este usuario no puede ingresar al sistema", null, UIColor.Black, false, enumDuracionToast.Corto);
                        txt_Password.Text = string.Empty;
                        txt_Usuario.Text = string.Empty;
                        return;
                    }
                }

                bool autenticado;

                try
                {
                    // Se autentifica los datos del usuario y se obtiene si es un son válidos.
                    autenticado = GestorSeguridad.AutenticarUsuario(txt_Usuario.Text, txt_Password.Text);
                    //autenticado = true;
                }
                catch (Exception ex)
                {
                    Toast.MakeToast(View, "Autenticando el Usuario." + ex.Message, null, UIColor.Black, false, enumDuracionToast.Corto);
                    return;
                }

                // Si el usuario ingreso la información correcta.
                if (autenticado)
                {
                    // Se valida que existan las configuraciones del dispositivo.
                    if (ValidarConfiguracion())
                    {
                        // Se asignan privilegios al usuario que está ingresando.
                        bool tienePrivilegio = false;
                        GestorSeguridad.VerificarPermiso(txt_Usuario.Text.ToUpper(), Acciones.FR_BORRAR_BD, out tienePrivilegio);
                        FRmConfig.EraseBD = tienePrivilegio;

                        // Se procede a permitir el ingreso del usuario.
                        IngresoPermitidoSistema();
                    }
                }
                else
                {
                    // Si los datos no son válidos, se incrementa la cuenta de intentos realizados.
                    numeroIntentos++;

                    Toast.MakeToast(View, "Contraseña inválida.", null, UIColor.Black, false, enumDuracionToast.Corto);
                    txt_Password.Text = string.Empty;

                    // Si el usuario ya alcanzó la cantidad de intentos permitidos se le notifica.
                    if (numeroIntentos == intentosPermitidos)
                    {
                        Toast.MakeToast(View, "Se sobrepasó el máximo número de intentos permitido para este usuario.", null, UIColor.Black, false, enumDuracionToast.Corto);
                    }
                }
            }
        }

        /// <summary>
        /// Método para validar si se puede cargar la configuración del dispositivo.
        /// </summary>
        /// <returns>Retorna si la configuración se cargo o no.</returns>
        private bool ValidarConfiguracion()
        {
            bool exito = false;
            string mensaje = string.Empty;
            HandHeldConfig config = new HandHeldConfig();

            exito = config.cargarConfiguracionHandHeld(ref mensaje);

            if (exito)
                exito = config.cargarConfiguracionGlobalHandHeld(ref mensaje);
            if (!exito)
            {
                Toast.MakeToast(View, mensaje, null, UIColor.Black, false, enumDuracionToast.Corto);
            }

            return exito;
        }


        /// <summary>
        /// Método que permite el ingreso al sistema, y guarda las variables por defecto de usuario y contraseña si
        /// la opción está habilitada.
        /// </summary>
        private void IngresoPermitidoSistema()
        {
            // Si la carga de parámetros de configuración es exitosa.
            if (this.CargaParametros(true))
            {
                // Permitir que el Segue continue y se pueda ingresar al TabView.
                doSegue = true;

                AppDelegate.SetConfigVar(txt_Usuario.Text, "usuario");

                if (AppDelegate.AlmacenarContrasena)
                {
                    AppDelegate.SetConfigVar(txt_Password.Text, "contrasena");
                }
                else
                {
                    AppDelegate.SetConfigVar("", "contrasena");
                }
            }
        }

        /// <summary>
        /// Método que cargar los parámetros globales y configuración del dispositivo.
        /// </summary>
        /// <param name="logBitacora"></param>
        /// <param name="viewModel"></param>
        /// <returns>Retorna si se pudo cargar la configuración del dispositivo o no.</returns>
        public bool CargaParametros(bool logBitacora)
        {
            string mensaje = string.Empty;

            // Si se pudo cargar los parámetros al dispositivo.
            if (GlobalUI.CargaParametrosX(logBitacora))
            {
                try
                {
                    HandHeldConfig config = new HandHeldConfig();

                    //Carga nuevamente los parametros del dispositivo.
                    config.cargarConfiguracionHandHeld(ref mensaje);
                    config.cargarConfiguracionGlobalHandHeld(ref mensaje);

                    GlobalUI.Rutas = Ruta.ObtenerRutas();
                    
                    // LAS. Mejora Paquetes y Reglas. Liberar los paquetes de las regalias.
                    Regalias.Liberar();
                }
                catch (Exception ex)
                {
                    Toast.MakeToast(View, "No se pudo cargar apropiadamente los datos de configuración, intente ingresar nuevamente.", null, UIColor.Black, false, enumDuracionToast.Mediano);
                    return false;
                }
                return true;
            }
            else
                return false;
        }
        
        #endregion

        #region View lifecycle

        /// <summary>
        /// Método para liberación de memoria de la pantalla cuando se cierra.
        /// </summary>
        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        /// <summary>
        /// Método que se ejecuta al crear la pantalla, se ejecuta una única vez a menos que la pantalla se remueva del stack.
        /// </summary>
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Inicializar las variables o asignar los datos que se utilizan en la pantalla.

            // Asigna el ícono de la aplicación a la imagen de Iniciar sesión.
            img_Logo.Image = UIImage.FromFile("Pem-Pedidos-Movil512x512.png");
            
            // Evento de retorno del campo de texto txt_Usuario, ocultando el teclado.
            txt_Usuario.ShouldReturn += (textField) =>
            {
                textField.ResignFirstResponder();

                txt_Password.BecomeFirstResponder();

                return true;
            };

            // Evento de retorno del campo de texto txt_Password, ocultando el teclado.
            txt_Password.ShouldReturn += (textField) =>
            {
                textField.ResignFirstResponder();

                // Si debe sincronizar primero la base de datos con el servidor.
                if (Sincronizar)
                {
                    // Requerir que ingrese el nombre de usuario para proveerlo al servidor.
                    if (!string.IsNullOrEmpty(txt_Usuario.Text))
                    {
                        // Requerir que ingrese la dirección del servidor con el que sincronizar.
                        if (!string.IsNullOrEmpty(AppDelegate.Servidor))
                        {
                            // Iniciar el proceso de sincronización.
                            IniciarSincronizacion();
                        }
                        else
                        {
                            Toast.MakeToast(View, "Ingrese la dirección del servidor con el que desea conectar, utilice el botón en la esquina superior derecha.", null, UIColor.Black, false, enumDuracionToast.Corto);
                        }
                    }
                    else
                    {
                        Toast.MakeToast(View, "Ingrese su usuario para iniciar la primera sincronizacion.", null, UIColor.Black, false, enumDuracionToast.Corto);
                    }
                }
                else
                {
                    doSegue = false;

                    // Iniciar el proceso de inicio de sesión.
                    this.LoginProcess();

                    // Si se validó correctamente la información del usuario.
                    if (doSegue)
                    {
                        // Se referencia al Storyboard del app.
                        var myStoryboard = AppDelegate.Storyboard;

                        // Se instancia un ViewController con el ID del Storyboard y su clase respectiva.
                        var TabView = myStoryboard.InstantiateViewController("VistaTabs") as VistaTabs;

                        // Se navega a la pantalla instanciada mostrandola de forma Modal (Sobre de la pantalla actual).
                        NavigationController.PresentViewController(TabView, true, () => { });
                    }
                }

                return true;
            };
                        
            // A continuación se cargan o se crean los valores default de la aplicación, variables Globales de compañía,
            // servidor, almacenarcontrasena, usuario y contraseña.

            // Se crea una referencia a los valores por defecto del usuario.
            var defaults = NSUserDefaults.StandardUserDefaults;

            if (defaults.ValueForKey(new NSString("compania")) == null)
            {
                defaults.SetString("", "compania");
                defaults.Synchronize();
            }

            if (defaults.ValueForKey(new NSString("servidor")) == null)
            {
                defaults.SetString("", "servidor");
                defaults.Synchronize();
            }

            if (defaults.ValueForKey(new NSString("consecutivo")) == null)
            {
                defaults.SetString("", "consecutivo");
                defaults.Synchronize();
            }

            if (defaults.ValueForKey(new NSString("websoporte")) == null)
            {
                defaults.SetString("", "websoporte");
                defaults.Synchronize();
            }

            if (defaults.ValueForKey(new NSString("almacenarcontrasena")) == null)
            {
                defaults.SetBool(true, "almacenarcontrasena");
                defaults.Synchronize();
            }

            if (defaults.ValueForKey(new NSString("usuario")) == null)
            {
                defaults.SetString("", "usuario");
                defaults.Synchronize();
            }

            if (defaults.ValueForKey(new NSString("contrasena")) == null)
            {
                defaults.SetString("", "contrasena");
                defaults.Synchronize();
            }

            if (defaults.ValueForKey(new NSString("cambiardescuento")) == null)
            {
                defaults.SetBool(false, "cambiardescuento");
                defaults.Synchronize();
            }
        }

        /// <summary>
        /// Método que se ejecuta cuando la pantalla va a aparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animación.</param>
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            // Sacar datos default de las Globales servidor, usuario y contraseña.

            AppDelegate.Compania = NSUserDefaults.StandardUserDefaults.StringForKey("compania");

            AppDelegate.Servidor = NSUserDefaults.StandardUserDefaults.StringForKey("servidor");

            AppDelegate.Consecutivo = NSUserDefaults.StandardUserDefaults.StringForKey("consecutivo");

            AppDelegate.webSoporte = NSUserDefaults.StandardUserDefaults.StringForKey("websoporte");

            AppDelegate.AlmacenarContrasena = NSUserDefaults.StandardUserDefaults.BoolForKey("almacenarcontrasena");

            AppDelegate.CambiarDescuento = NSUserDefaults.StandardUserDefaults.BoolForKey("cambiardescuento");

            txt_Usuario.Text = NSUserDefaults.StandardUserDefaults.StringForKey("usuario");

            if (AppDelegate.AlmacenarContrasena)
            {
                txt_Password.Text = NSUserDefaults.StandardUserDefaults.StringForKey("contrasena");
            }

            // Se intenta obtener la dirección del directorio de la aplicación.
            AppDelegate.RutaSD = AppDelegate.ObtenerRutaSD();

            // Se intenta crea una conexión a la base de datos.
            AppDelegate.cnx = AppDelegate.crearConexion(System.IO.Path.Combine(AppDelegate.RutaSD, AppDelegate.BaseDatos));

            // Valida si se pudo iniciar la conexión a la base de datos.
            if (AppDelegate.cnx != null || NSUserDefaults.StandardUserDefaults.BoolForKey("cerrado"))
            {
                try
                {
                    GestorDatos.cnx = AppDelegate.cnx;

                    GlobalUI.Rutas = Ruta.ObtenerRutas();

                    GlobalUI.CargaParametrosX(false);
                    GlobalUI.ValidarConfiguracion();

                    // Carga el simbolo monetario de la compañia guardada en la Configuración.
                    var compania = Compania.Obtener(AppDelegate.Compania);

                    if(compania != null)
                    {
                        GestorUtilitario.SimboloMonetario = compania.SimboloMonetarioFunc;
                    }

                    Sincronizar = false;

                    // Si la opción de almacenar contraseña está habilitada se intentará ingresar al usuario a la aplicación.
                    if (AppDelegate.AlmacenarContrasena)
                    {
                        // Si la contraseña no está vacia se intentará loguear al usuario automáticamente.
                        if (!string.IsNullOrEmpty(txt_Password.Text))
                        {
                            doSegue = false;

                            // Iniciar el proceso de inicio de sesión.
                            this.LoginProcess();

                            // Si se validó correctamente la información del usuario.
                            if (doSegue)
                            {
                                // Se referencia al Storyboard del app.
                                var myStoryboard = AppDelegate.Storyboard;

                                // Se instancia un ViewController con el ID del Storyboard y su clase respectiva.
                                var TabView = myStoryboard.InstantiateViewController("VistaTabs") as VistaTabs;

                                // Se navega a la pantalla instanciada mostrandola de forma Modal (Sobre de la pantalla actual).
                                NavigationController.PresentViewController(TabView, true, () => { });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Si no se pudo cargar la información de la base de datos algo está mal y es requerido sincronizar con el servidor.
                    Sincronizar = true;
                }
            }
            else
            {
                // Si no se pudo conectar con la base de datos algo está mal y es requerido sincronizar con el servidor.
                Sincronizar = true;
            }


            // Si ya se sincronizó la aplicación anteriormente, brindar una opción para volver a hacerlo por si hubo algún cambio
            // en los datos desde el lado del servidor, sin la necesidad de ingresar a la aplicación para ejecutar la acción.
            if (!Sincronizar)
            {
                UIBarButtonItem[] items = new UIBarButtonItem[] {
                    new UIBarButtonItem(UIImage.FromFile("btn_Servidor.png")
                    , UIBarButtonItemStyle.Plain
                    , (sender, args) =>
                    {
                        // Evento de click del botón de configurar servidor.

                        txt_Usuario.ResignFirstResponder();
                        txt_Password.ResignFirstResponder();

                        UIAlertView alert = new UIAlertView();
                        alert.Title = "Servidor";
                        alert.AddButton("Cancelar");
                        alert.AddButton("Guardar");
                        alert.Message = "Ingrese la dirección del servidor:";
                        alert.AlertViewStyle = UIAlertViewStyle.PlainTextInput;
                        alert.GetTextField(0).Text = AppDelegate.Servidor;

                        // Evento click del botón de Guardar.
                        alert.Clicked += (object s, UIButtonEventArgs ev) =>
                        {
                            if (ev.ButtonIndex == 1)
                            {
                                // Se guarda la nueva dirección del servidor ingresada por el usuario.

                                AppDelegate.Servidor = alert.GetTextField(0).Text;

                                if (!AppDelegate.Servidor.Contains("http://"))
                                {
                                    AppDelegate.Servidor = "http://" + AppDelegate.Servidor;
                                }

                                AppDelegate.SetConfigVar(AppDelegate.Servidor, "servidor");

                                Toast.MakeToast(View, "Dirección del servidor guardada (" + alert.GetTextField(0).Text+ ").", null, UIColor.Black, false, enumDuracionToast.Corto);
                            }
                        };

                        alert.Show();
                    }),
                    new UIBarButtonItem(UIImage.FromFile("tab_Sincronizar.png")
                    , UIBarButtonItemStyle.Plain
                    , (sender, args) =>
                    {
                        // Evento de click del botón de sincronizar.
                        
                        txt_Usuario.ResignFirstResponder();
                        txt_Password.ResignFirstResponder();

                        // Requerir que ingrese el nombre de usuario para proveerlo al servidor.
                        if (!string.IsNullOrEmpty(txt_Usuario.Text))
                        {
                            // Requerir que ingrese la dirección del servidor con el que sincronizar.
                            if (!string.IsNullOrEmpty(AppDelegate.Servidor))
                            {
                                // Iniciar el proceso de sincronización.
                                IniciarSincronizacion();
                            }
                            else
                            {
                                Toast.MakeToast(View, "Ingrese la dirección del servidor con el que desea conectar, utilice el botón en la esquina superior derecha.", null, UIColor.Black, false, enumDuracionToast.Corto);
                            }
                        }
                        else
                        {
                            Toast.MakeToast(View, "Ingrese su usuario para iniciar la primera sincronizacion.", null, UIColor.Black, false, enumDuracionToast.Corto);
                        }
                    })
                };

                // Agregar en la barra de Navegación los botones para sincronizar la aplicación y
                // para ingresar la dirección del servidor.
                this.NavigationItem.SetRightBarButtonItems(items, true);
            }
            else
            {
                // Agregar en la barra de Navegación un botón para ingresar la dirección del servidor.
                this.NavigationItem.SetRightBarButtonItem(
                    new UIBarButtonItem(UIImage.FromFile("btn_Servidor.png")
                    , UIBarButtonItemStyle.Plain
                    , (sender, args) =>
                    {
                        // Evento de click del botón de configurar servidor.

                        txt_Usuario.ResignFirstResponder();
                        txt_Password.ResignFirstResponder();

                        UIAlertView alert = new UIAlertView();
                        alert.Title = "Servidor";
                        alert.AddButton("Cancelar");
                        alert.AddButton("Guardar");
                        alert.Message = "Ingrese la dirección del servidor:";
                        alert.AlertViewStyle = UIAlertViewStyle.PlainTextInput;
                        alert.GetTextField(0).Text = AppDelegate.Servidor;

                        // Evento click del botón de Guardar.
                        alert.Clicked += (object s, UIButtonEventArgs ev) =>
                        {
                            if (ev.ButtonIndex == 1)
                            {
                                // Se guarda la nueva dirección del servidor ingresada por el usuario.

                                AppDelegate.Servidor = alert.GetTextField(0).Text;

                                if (!AppDelegate.Servidor.Contains("http://"))
                                {
                                    AppDelegate.Servidor = "http://" + AppDelegate.Servidor;
                                }

                                AppDelegate.SetConfigVar(AppDelegate.Servidor, "servidor");

                                Toast.MakeToast(View, "Dirección del servidor guardada (" + alert.GetTextField(0).Text + ").", null, UIColor.Black, false, enumDuracionToast.Corto);
                            }
                        };

                        alert.Show();
                    })
                , true);

                // Notificar al usuario que debe sincronizar la aplicación.
                new UIAlertView("Advertencia",
                    "Parece que es la primera vez que utiliza la aplicación o la base de datos no está configurada, " +
                    "ingrese su nombre de usuario y sincronice la aplicación primero.", null, "OK", null).Show();

                btn_Iniciar.SetTitle("Sincronizar", UIControlState.Normal);
            }
        }
        
        /// <summary>
        /// Método que se ejecuta cuando la pantalla apareció.
        /// </summary>
        /// <param name="animated">Si debe tener animación.</param>
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
        }

        /// <summary>
        /// Método que se ejecuta cuando la pantalla va a desaparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animación.</param>
        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        /// <summary>
        /// Método que se ejecuta cuando la pantalla desapareció.
        /// </summary>
        /// <param name="animated">Si debe tener animación.</param>
        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
    }
}