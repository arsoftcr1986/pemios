// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Softland.iOS.Pem
{
	[Register ("RootViewController")]
	partial class RootViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_Iniciar { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIImageView img_Logo { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField txt_Password { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField txt_Usuario { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (btn_Iniciar != null) {
				btn_Iniciar.Dispose ();
				btn_Iniciar = null;
			}
			if (img_Logo != null) {
				img_Logo.Dispose ();
				img_Logo = null;
			}
			if (txt_Password != null) {
				txt_Password.Dispose ();
				txt_Password = null;
			}
			if (txt_Usuario != null) {
				txt_Usuario.Dispose ();
				txt_Usuario = null;
			}
		}
	}
}
