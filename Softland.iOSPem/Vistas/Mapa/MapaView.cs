using System;
using System.CodeDom.Compiler;
using System.Drawing;
using System.Collections.Generic;
using MapKit;
using CoreLocation;

using Pedidos.Core;
using BI.Shared;

using Foundation;
using UIKit;

namespace Softland.iOS.Pem
{
	partial class MapaView : UIViewController
    {

        #region Propiedades y Contructor de instancia
        
        /// <summary>
        /// Referencia a la ventana de PopUp de cliente mostrada al buscar clientes.
        /// </summary>
        private PopUpView ventanaResultadoClientes;

        /// <summary>
        /// Lista de clientes a cargar en el controlador de Mapa, se llena con la b�squeda de clientes.
        /// </summary>
        public List<Cliente> clientes = new List<Cliente>();

        /// <summary>
        /// Referencia al delegado del mapa para acceder a su informaci�n.
        /// </summary>
        private MyMapDelegate mapDel;

        /// <summary>
        /// Referencia al administrador de localizaci�n del usuario.
        /// </summary>
        public static CLLocationManager locationManager;

        /// <summary>
        /// Variable que almacena las coordenadas actuales del usuario.
        /// </summary>
        public static CLLocationCoordinate2D CoordenadasUsuario;
        
        /// <summary>
        /// Variable que almacena si ya se centro la vista del mapa en alg�n punto en espec�fico.
        /// </summary>
        public static bool centroVista = false;

        /// <summary>
        /// Controlador para generar la barra de b�squeda r�pida.
        /// </summary>
        private UISearchBar searchBar;

        /// <summary>
        /// Constructor por defecto.
        /// </summary>
        /// <param name="handle"></param>
		public MapaView (IntPtr handle) : base (handle)
		{
		}

        #endregion

        #region Clases

        /// <summary>
        /// Clase que se utilizar� para manejar la informaci�n que se muestra en el mapa.
        /// </summary>
        class MyMapDelegate : MKMapViewDelegate
        {

            #region Propiedades y Contructor de instancia
            
            /// <summary>
            /// Variable para almacenar el ID de la anotaci�n normal.
            /// </summary>
            static string pinId = "PinAnnotation";

            /// <summary>
            /// Variable para almacenar el ID de la anotaci�n de ejemplo de centro de conferencias.
            /// </summary>
            //static string annotationId = "ConferenceAnnotation";

            /// <summary>
            /// Controlador de la imagen a mostrar en el ejemplo de centro de conferencias.
            /// </summary>
            //UIImageView venueView;

            /// <summary>
            /// Referencia a la imagen a mostrar en el ejemplo de centro de conferencias.
            /// </summary>
            //UIImage venueImage;

            #endregion

            #region Eventos

            /// <summary>
            /// Evento requerido por la clase, se ejecuta cuando se muestra en el mapa una anotaci�n.
            /// </summary>
            /// <param name="mapView"></param>
            /// <param name="annotation"></param>
            /// <returns>Retorna la anotaci�n a mostrar.</returns>
            public override MKAnnotationView GetViewForAnnotation(MKMapView mapView, IMKAnnotation annotation)
            {
                // Referencia a la anotaci�n que se mostrar�.
                MKAnnotationView anView;

                // Si la anotaci�n es la localizaci�n del usuario.
                if (annotation.Coordinate.Latitude == CoordenadasUsuario.Latitude && 
                    annotation.Coordinate.Longitude == CoordenadasUsuario.Longitude)
                {
                    return null;
                }

                //else if (annotation is ConferenceAnnotation)
                //{
                //    //Muestra una anotaci�n del ejemplo de centro de conferencias.

                //    anView = mapView.DequeueReusableAnnotation(annotationId);

                //    if (anView == null)
                //        anView = new MKAnnotationView(annotation, annotationId);

                //    anView.Image = UIImage.FromFile("tab_Mapa.png");
                //    anView.CanShowCallout = false;
                //}

                else
                {
                    // Muesta una anotaci�n normal usada para mostrar los clientes en el mapa.

                    anView = (MKPinAnnotationView)mapView.DequeueReusableAnnotation(pinId);

                    if (anView == null)
                        anView = new MKPinAnnotationView(annotation, pinId);

                    ((MKPinAnnotationView)anView).PinColor = MKPinAnnotationColor.Red;
                    anView.CanShowCallout = true;
                }

                return anView;
            }

            // Evento requerido por la clase, se ejecuta cuando se seleccion� una anotaci�n.
            public override void DidSelectAnnotationView(MKMapView mapView, MKAnnotationView view)
            {
                // Muestra la imagen del centro de conferencias cuando se selecciona.
                //if (view.Annotation is ConferenceAnnotation)
                //{

                //    venueView = new UIImageView();
                //    venueView.ContentMode = UIViewContentMode.ScaleAspectFit;
                //    venueImage = UIImage.FromFile("imagen_no_disponible.jpg");
                //    venueView.Image = venueImage;
                //    view.AddSubview(venueView);

                //    UIView.Animate(0.4, () =>
                //    {
                //        venueView.Frame = new RectangleF(-75, -75, 200, 200);
                //    });
                //}
            }
            
            // Evento requerido por la clase, se ejecuta cuando se quit� la selecci�n de una anotaci�n.
            public override void DidDeselectAnnotationView(MKMapView mapView, MKAnnotationView view)
            {
                // Remueve la imagen del centro de conferencias cuando se deselecciona.
                //if (view.Annotation is ConferenceAnnotation)
                //{

                //    venueView.RemoveFromSuperview();
                //    venueView.Dispose();
                //    venueView = null;
                //}
            }
            
            // Evento requerido por la clase, se ejecuta cuando se muestra una anotaci�n seleccionada.
            public override void CalloutAccessoryControlTapped(MKMapView mapView, MKAnnotationView view, UIControl control)
            {
                // Se puede mostrar una alerta de la anotaci�n que se recibe.
            }
            
            /// <summary>
            /// Evento requerido por la clase, se ejecuta cuando se muestra en el mapa un Overlay.
            /// </summary>
            /// <param name="mapView"></param>
            /// <param name="overlay"></param>
            /// <returns>Retorna el Overlay a mostrar.</returns>
            public override MKOverlayView GetViewForOverlay(MKMapView mapView, IMKOverlay overlay)
            {
                // Ejemplo de Overlay circular.
                //var circleOverlay = overlay as MKCircle;
                //var circleView = new MKCircleView(circleOverlay);
                //circleView.FillColor = UIColor.Red;
                //circleView.Alpha = 0.4f;

                // Ejemplo de Overlay de centro de conferencias.
                MKPolygon polygon = overlay as MKPolygon;
                MKPolygonView polygonView = new MKPolygonView(polygon);
                polygonView.FillColor = UIColor.Blue;
                polygonView.StrokeColor = UIColor.Red;

                return polygonView;
            }

            public override void DidUpdateUserLocation(MKMapView mapView, MKUserLocation userLocation)
            {
                if (userLocation != null)
                {
                    // Se guarda la posici�n actual del usuario.
                    CoordenadasUsuario = userLocation.Coordinate;

                    // Si se va a mostrar por primera vez la ubicaci�n del usuario, se centra donde se encuentra.
                    if(!centroVista)
                    {
                        MKCoordinateRegion region = MKCoordinateRegion.FromDistance(CoordenadasUsuario, 100000, 100000);
                        mapView.Region = region;

                        centroVista = true;
                    }
                }
            }

            public override void DidFailToLocateUser(MKMapView mapView, NSError error)
            {
                if (!centroVista)
                {
                    double lat = 9.927128;
                    double lon = -84.082011;

                    // user denied permission, or device doesn't have GPS/location ability
                    CLLocationCoordinate2D coords = new CLLocationCoordinate2D(lat, lon); // Costa Rica
                    MKCoordinateRegion region = MKCoordinateRegion.FromDistance(coords, 100000, 100000);
                    mapView.Region = region;

                    centroVista = true;
                }
            }

            #endregion
        }

        /// <summary>
        /// Clase que se utilizar� para manejar la informaci�n de la anotacion de centro de conferencias
        /// que se muestra en el ejemplo de centro de conferencias.
        /// </summary>
        public class ConferenceAnnotation : MKAnnotation
        {

            #region Propiedades y Contructor de instancia
            
            /// <summary>
            /// Variable que almacena el nombre de la anotaci�n.
            /// </summary>
            string title;

            /// <summary>
            /// Variable que almacena la coordenada de la anotaci�n.
            /// </summary>
            CLLocationCoordinate2D coord;

            /// <summary>
            /// Constructor por defecto.
            /// </summary>
            /// <param name="title"></param>
            /// <param name="coord"></param>
            public ConferenceAnnotation(string title, CLLocationCoordinate2D coord)
            {
                this.title = title;
                this.coord = coord;
            }
            
            #endregion

            #region M�todos
            
            /// <summary>
            /// Get de la variable title.
            /// </summary>
            public override string Title
            {
                get
                {
                    return title;
                }
            }

            /// <summary>
            /// Get y Set de la variable coord.
            /// </summary>
            public override CLLocationCoordinate2D Coordinate
            {
                get
                {
                    return coord;
                }
                //set
                //{

                //    // call WillChangeValue and DidChangeValue to use KVO with an MKAnnotation so that
                //    // setting the coordinate on the annotation instance causes the associated annotation
                //    // view to move to the new location.

                //    WillChangeValue("coordinate");
                //    coord = value;
                //    DidChangeValue("coordinate");
                //}
            }

            #endregion
        }

        #endregion

        #region Tabla Resultado Clientes

        /// <summary>
        /// Clase que se utilizar� para manejar la informaci�n de la tabla de selecci�n de cliente.
        /// </summary>
        public class TableSource : UITableViewSource
        {

            #region Propiedades y Contructor de instancia

            /// <summary>
            /// Variable para identificar las celdas que utiliza este TableSource.
            /// </summary>
            string cellIdentifier = "";

            /// <summary>
            /// Referencia a la pantalla para poder acceder a sus propiedades desde otra clase. 
            /// </summary>
            MapaView VistaPadre;

            /// <summary>
            /// Constructor por defecto. 
            /// </summary>
            /// <param name="padre"></param>
            public TableSource(MapaView padre, string CellID)
            {
                VistaPadre = padre;
                cellIdentifier = CellID;
            }

            #endregion

            #region Eventos

            // Evento requerido por la clase, retorna la cantidad de registros en la fuente de datos.
            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return VistaPadre.clientes.Count;
            }

            /// <summary>
            /// Evento que se ejecuta al cargar las celdas de la tabla seg�n vayan siendo mostradas.
            /// </summary>
            /// <param name="tableView"></param>
            /// <param name="indexPath">�ndice de la celda por cargar.</param>
            /// <returns>Retorna la celda cargada.</returns>
            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                // Busca la celda creada para ahorrar memoria.
                UITableViewCell cell = tableView.DequeueReusableCell(cellIdentifier);

                // Si no hay celdas por reutilizar, se crea una nueva, en este caso se usa una celda predefinida Subtitle.
                if (cell == null)
                {
                    cell = new UITableViewCell(UITableViewCellStyle.Subtitle, cellIdentifier);

                    cell.TextLabel.Font = UIFont.FromName("Helvetica-Bold", 17f);
                    cell.TextLabel.AdjustsFontSizeToFitWidth = true;
                    cell.TextLabel.MinimumFontSize = 8;

                    cell.DetailTextLabel.Font = UIFont.FromName("Helvetica", 15f);
                    cell.DetailTextLabel.AdjustsFontSizeToFitWidth = true;
                    cell.DetailTextLabel.MinimumFontSize = 8;
                }

                // Se actualiza la informaci�n de la celda.
                cell.TextLabel.Text = VistaPadre.clientes[indexPath.Row].Nombre;
                cell.DetailTextLabel.Text = "C�digo: " + VistaPadre.clientes[indexPath.Row].Codigo;

                return cell;
            }

            /// <summary>
            /// Evento que se ejecuta al seleccionar una celda de la tabla.
            /// </summary>
            /// <param name="tableView"></param>
            /// <param name="indexPath">�ndice de la celda seleccionada.</param>
            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                string cliente = VistaPadre.clientes[indexPath.Row].Codigo;

                InvokeOnMainThread(() => { VistaPadre.ventanaResultadoClientes.Hide(); });

                VistaPadre.clientes.Clear();
                VistaPadre.clientes.Add(ClientesView.clientesCargados.Find(x => x.Codigo.Equals(cliente)));

                VistaPadre.mostrarClientes();
            }

            #endregion
        }

        #endregion

        #region Metodos

        /// <summary>
        /// M�todo que se utiliza para mostrar a todos los clientes cargados en el mapa utilizando su informaci�n
        /// de geolocalizaci�n.
        /// </summary>
        public void mostrarClientes()
        {
            // Se usa Random pues no se tiene la informaci�n de localizaci�n de los clientes.
            //Random rand = new Random();

            // Se limpian todas las anotaciones en el mapa.
            map_Mapa.RemoveAnnotations(map_Mapa.Annotations);

            // Agregar la anotaci�n del cada cliente en la lista.
            foreach (Cliente cliente in clientes)
            {
                // Se carga la informaci�n de geolocalizaci�n del cliente.
                cliente.CargarGeolocalizacion();

                // Se crea una anotaci�n usando la informaci�n del cliente y se agrega al mapa.
                MKPointAnnotation annotation = new MKPointAnnotation();
                annotation.Title = cliente.Nombre;

                // Se agrega una coordenada aleatoria en relaci�n al punto central de Costa Rica (Es ejemplo).
                //annotation.SetCoordinate(new CLLocationCoordinate2D((9.927128 + (rand.Next(-999999, 999999) * 0.000001)),
                //        (-84.082011 + (rand.Next(-999999, 999999) * 0.000001))));

                annotation.SetCoordinate(new CLLocationCoordinate2D(cliente.Latitud, cliente.Longitud));

                if (cliente.Latitud != 0 && cliente.Longitud != 0)
                {
                    map_Mapa.AddAnnotation(annotation);
                }
            }

            // Si solo hay un cliente en la lista, se centra la vista del mapa sobre la anotaci�n del cliente.
            if (clientes.Count == 1)
            {
                btn_GuardarGeoloc.Hidden = false;

                if (clientes[0].Latitud != 0 && clientes[0].Longitud != 0)
                {
                    CLLocationCoordinate2D coords = new CLLocationCoordinate2D(clientes[0].Latitud, clientes[0].Longitud);
                    MKCoordinateRegion region = MKCoordinateRegion.FromDistance(coords, 100000, 100000);
                    map_Mapa.Region = region;

                    centroVista = true;
                }
            }
            else
            {
                btn_GuardarGeoloc.Hidden = true;

                centroVista = false;
            }
        }

        /// <summary>
        /// M�todo que se ejecuta al iniciar una b�squeda r�pida por alg�n cliente.
        /// Busca por clientes seg�n la descripci�n de estos y el dato ingresado en el campo de texto.
        /// </summary>
        void QuickSearch()
        {
            var clientesCompania = "";

            clientes.Clear();

            // Se obtienen los clientes encontrados, y se agregan a la lista.
            clientes.AddRange(ClientesView.clientesCargados.FindAll(
                x =>

                (
                    x.Nombre.ToUpper().Contains(searchBar.Text.ToUpper()) ||
                    x.Codigo.ToUpper().Contains(searchBar.Text.ToUpper())
                )
                    &&
                (
                    x.Compania.ToUpper().Equals(clientesCompania) ||
                    string.IsNullOrEmpty(clientesCompania)
                )
            ));

            // Crear PopUp con resultados y mostrarlos al usuario.
            CrearPopUpClientes();

            // Cargar los clientes obtenidos por la b�squeda.
            mostrarClientes();

            // Se quita el Foco de atenci�n del searchBar.
            searchBar.ResignFirstResponder();
        }

        /// <summary>
        /// M�todo que sirve para mostrar en pantalla una ventana PopUp con la lista de clientes encontrados.
        /// </summary>
        private void CrearPopUpClientes()
        {
            // Determine the correct size to start the overlay (depending on device orientation)
            var bounds = map_Mapa.Bounds;

            var Orientacion = UIApplication.SharedApplication.StatusBarOrientation;
            if (Orientacion == UIInterfaceOrientation.LandscapeLeft || Orientacion == UIInterfaceOrientation.LandscapeRight)
            {
                bounds.Size = new SizeF((float)bounds.Size.Width, (float)bounds.Size.Height);
            }

            // Posici�n Y justo debajo de la barra de navegaci�n.
            bounds.Y = 25f;

            // Si la ventana PopUp ya est� abierta, cerrarla.
            if (ventanaResultadoClientes != null)
            {
                ventanaResultadoClientes.Hide();
            }

            // Crear vista que se va a mostrar en la vista PopUp, adem�s crear y configurar los controles que se le agregar�n.

            // Dimensiones de la ventana del PopUp.
            float VPX = 0, VPY = 0, VPAncho = 0, VPAlto = 0;
            // Se manda a obtener las dimensiones que tendr�a la ventana principal del PopUp.
            PopUpView.ObtenerDimensiones((RectangleF)bounds, 0.9f, ref VPX, ref VPY, ref VPAncho, ref VPAlto, Orientacion);

            // Vista de pantalla Acerca De con los controles a mostrar.
            UIView VistaMostrar = new UIView(new RectangleF(VPX, VPY, VPAncho, VPAlto));

            // Controles a mostrar en pantalla Resultados Clientes.
            UITableView tbv_TablaClientes;

            tbv_TablaClientes = new UITableView()
            {
                Source = new TableSource(this,"ClienteCell"),
                Editing = false,
                Bounces = false,
                AutoresizingMask = UIViewAutoresizing.FlexibleDimensions
            };
            tbv_TablaClientes.Frame = new RectangleF(0, 0, VPAncho, VPAlto);
            tbv_TablaClientes.Layer.CornerRadius = 10f;
            tbv_TablaClientes.Layer.MasksToBounds = true;
            
            // Se agregan los controles en el orden que deben ser mostrados.
            VistaMostrar.Add(tbv_TablaClientes);

            // Se manda a crear la vista PopUp con los parametros que se desean para esta vista.
            ventanaResultadoClientes = new PopUpView((RectangleF)bounds, Orientacion, VistaMostrar, UIColor.FromRGB(225, 106, 12), UIColor.White, UIColor.Black);

            // Se agrega al controlador de la ventana principal para mostrarlo.
            this.Add(ventanaResultadoClientes);
        }

        #endregion

        #region Eventos
                
        /// <summary>
        /// Evento de click del bot�n btn_Tipo.
        /// Se usa para cambiar el tipo de mapa a mostrar.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_Tipo_TouchUpInside(UIButton sender)
        {
            if (map_Mapa.MapType == MKMapType.Standard)
            {
                map_Mapa.MapType = MKMapType.Satellite;
            }
            else if (map_Mapa.MapType == MKMapType.Satellite)
            {
                map_Mapa.MapType = MKMapType.Hybrid;
            }
            else
            {
                map_Mapa.MapType = MKMapType.Standard;
            }
        }

        /// <summary>
        /// Evento de click del bot�n btn_GuardarGeoloc.
        /// Se usa para guardar la nueva geolocalizaci�n del cliente seg�n la localizaci�n del dispositivo.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_GuardarGeoloc_TouchUpInside(UIButton sender)
        {
            // Si se pudo cargar la locaclizaci�n del dispositivo se intenta guardar como la localizaci�n del cliente.
            if (map_Mapa.UserLocation.Coordinate.Latitude != 0 && map_Mapa.UserLocation.Coordinate.Longitude != 0)
            {
                try
                {
                    // Se intenta guardar la geolocalizaci�n del dispositivo como la localizaci�n del cliente.
                    clientes[0].GuardarGeolocalizacion(map_Mapa.UserLocation.Coordinate.Latitude, map_Mapa.UserLocation.Coordinate.Longitude);

                    mostrarClientes();
                }
                catch (Exception ex)
                {
                    Toast.MakeToast(View, "No se pudo guardar la nueva geolocalizaci�n del cliente, intentelo nuevamente.",
                                   null, UIColor.Black, true, enumDuracionToast.Mediano);
                }
            }
            else
            {
                Toast.MakeToast(View, "No se puede precisar la geolocalizaci�n del dispositivo, intentelo nuevamente.",
                                   null, UIColor.Black, true, enumDuracionToast.Mediano);
            }
        }

        /// <summary>
        /// Evento que se encarga de actualizar la informaci�n de localizaci�n del usuario.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void map_Mapa_DidUpdateUserLocation(object sender, MKUserLocationEventArgs e)
        {
            if (map_Mapa.UserLocation != null && !centroVista)
            {
                CLLocationCoordinate2D coords = map_Mapa.UserLocation.Coordinate;
                MKCoordinateRegion region = MKCoordinateRegion.FromDistance(coords, 100000, 100000);
                map_Mapa.Region = region;

                centroVista = true;
            }
        }
        
        #endregion

        #region View lifecycle
        
        /// <summary>
        /// M�todo para liberaci�n de memoria de la pantalla cuando se cierra.
        /// </summary>
        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        /// <summary>
        /// M�todo que se ejecuta al crear la pantalla, se ejecuta una �nica vez a menos que la pantalla se remueva del stack.
        /// </summary>
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            
            // Inicializar las variables o asignar los datos que se utilizan en la pantalla.

            // Se inicializan propiedades del mapa.
            map_Mapa.MapType = MKMapType.Standard;
            map_Mapa.ShowsUserLocation = true;
            map_Mapa.ZoomEnabled = true;
            map_Mapa.ScrollEnabled = true;
            
            // Coordenadas default para el centrar el mapa.
            //double lat = 9.927128;
            //double lon = -84.082011;
            
            // Se asigna el delegado del mapa.
            mapDel = new MyMapDelegate();
            map_Mapa.Delegate = mapDel;

            // Ejemplo de como agregar al mapa un Overlay circular en base a una posicion central y el radio.
            //var circleOverlay = MKCircle.Circle(map_Mapa.CenterCoordinate, 30000);
            //map_Mapa.AddOverlay(circleOverlay);

            // Ejemplo de como trazar un per�metro o ruta para una anotaci�n.
            // Se crea una anotaci�n para mostrar como centro del per�metro y se agrega al mapa.
            //double latEC = 30.2652233534254;
            //double lonEC = -97.73815460962083;
            //CLLocationCoordinate2D eventCenter = new CLLocationCoordinate2D(latEC, lonEC);
            //map_Mapa.AddAnnotation(new ConferenceAnnotation("Evolve Conference", eventCenter));

            // Se crea y especifican los puntos del per�metro para poder trazarlo y se agrega al mapa.
            //MKPolygon hotelOverlay = MKPolygon.FromCoordinates(
            //    new CLLocationCoordinate2D[] {
            //    new CLLocationCoordinate2D(30.2649977168594, -97.73863627705),
            //    new CLLocationCoordinate2D(30.2648461170005, -97.7381627734755),
            //    new CLLocationCoordinate2D(30.2648355402574, -97.7381750192576),
            //    new CLLocationCoordinate2D(30.2647791309417, -97.7379872505988),
            //    new CLLocationCoordinate2D(30.2654525150319, -97.7377341711021),
            //    new CLLocationCoordinate2D(30.2654807195004, -97.7377994819399),
            //    new CLLocationCoordinate2D(30.2655089239607, -97.7377994819399),
            //    new CLLocationCoordinate2D(30.2656428950368, -97.738346460207),
            //    new CLLocationCoordinate2D(30.2650364981811, -97.7385709662122),
            //    new CLLocationCoordinate2D(30.2650470749025, -97.7386199493406)
            //});
            //map_Mapa.AddOverlay(hotelOverlay);

            // Se crea y asigna un nuevo evento de reconocimiento de gestos al mapa, para captar si el usuario
            // toca un punto en el mapa.
            //UITapGestureRecognizer tap = new UITapGestureRecognizer(g =>
            //{
            //    //var pt = g.LocationInView(map_Mapa);
            //    //CLLocationCoordinate2D tapCoord = map_Mapa.ConvertPoint(pt, map_Mapa);

            //    //Console.WriteLine("new CLLocationCoordinate2D({0}, {1}),", tapCoord.Latitude, tapCoord.Longitude);

            //    if (g.LocationInView(map_Mapa).X > (map_Mapa.Bounds.Width - 50) && g.LocationInView(map_Mapa).Y > (map_Mapa.Bounds.Height - 50))
            //    {
            //        if (map_Mapa.MapType == MKMapType.Standard)
            //        {
            //            map_Mapa.MapType = MKMapType.Satellite;

            //            lbl_Tipo.AttributedText = new NSAttributedString("Sat�lite", underlineStyle: NSUnderlineStyle.Single,
            //                foregroundColor: UIColor.LightGray, font: UIFont.FromName("Helvetica-Bold", 9f));

            //            btn_Buscar.SetAttributedTitle(new NSAttributedString("Clientes", underlineStyle: NSUnderlineStyle.Single,
            //                foregroundColor: UIColor.LightGray, font: UIFont.FromName("Helvetica-Bold", 9f)), UIControlState.Normal);
            //        }
            //        else if (map_Mapa.MapType == MKMapType.Satellite)
            //        {
            //            map_Mapa.MapType = MKMapType.Hybrid;

            //            lbl_Tipo.AttributedText = new NSAttributedString("H�brido", underlineStyle: NSUnderlineStyle.Single,
            //                foregroundColor: UIColor.LightGray, font: UIFont.FromName("Helvetica-Bold", 9f));

            //            btn_Buscar.SetAttributedTitle(new NSAttributedString("Clientes", underlineStyle: NSUnderlineStyle.Single,
            //                foregroundColor: UIColor.LightGray, font: UIFont.FromName("Helvetica-Bold", 9f)), UIControlState.Normal);
            //        }
            //        else
            //        {
            //            map_Mapa.MapType = MKMapType.Standard;

            //            lbl_Tipo.AttributedText = new NSAttributedString("Est�ndar", underlineStyle: NSUnderlineStyle.Single,
            //                foregroundColor: UIColor.DarkGray, font: UIFont.FromName("Helvetica-Bold", 9f));

            //            btn_Buscar.SetAttributedTitle(new NSAttributedString("Clientes", underlineStyle: NSUnderlineStyle.Single,
            //                foregroundColor: UIColor.DarkGray, font: UIFont.FromName("Helvetica-Bold", 9f)), UIControlState.Normal);
            //        }
            //    }
            //});
            //map_Mapa.AddGestureRecognizer(tap);
            
            //// Evento del mapa de si logr� mostrar la informaci�n del usuario.
            //map_Mapa.DidUpdateUserLocation += (sender, e) =>
            //{
            //    if (map_Mapa.UserLocation != null && !centroVista)
            //    {
            //        CLLocationCoordinate2D coords = map_Mapa.UserLocation.Coordinate;
            //        MKCoordinateRegion region = MKCoordinateRegion.FromDistance(coords, 100000, 100000);
            //        map_Mapa.Region = region;

            //        centroVista = true;
            //    }
            //};

            //// Evento del mapa de si no logr� mostrar la informaci�n del usuario.
            //map_Mapa.DidFailToLocateUser += (sender, e) =>
            //{
            //    if (!centroVista)
            //    {
            //        // user denied permission, or device doesn't have GPS/location ability
            //        CLLocationCoordinate2D coords = new CLLocationCoordinate2D(lat, lon); // Costa Rica
            //        MKCoordinateRegion region = MKCoordinateRegion.FromDistance(coords, 100000, 100000);
            //        map_Mapa.Region = region;

            //        centroVista = true;
            //    }
            //};

            // Se crea un UISearchBar para la b�squeda r�pida de art�culos.
            searchBar = new UISearchBar(new RectangleF(0, 0, (float)View.Frame.Width, 50));
            searchBar.SizeToFit();
            searchBar.AutocorrectionType = UITextAutocorrectionType.No;
            searchBar.AutocapitalizationType = UITextAutocapitalizationType.None;
            searchBar.EnablesReturnKeyAutomatically = false;

            // Se asigna el Evento de click en la barra.
            searchBar.SearchButtonClicked += (sender, e) =>
            {
                QuickSearch();
            };

            // Se crea una barra para colocar el bot�n de cancelar en el teclado que abre el control searchBar.
            UIToolbar toolbar = new UIToolbar(new RectangleF(0.0f, 0.0f, (float)this.View.Frame.Size.Width, 44.0f));
            toolbar.TintColor = UIColor.White;
            toolbar.BarStyle = UIBarStyle.Black;
            toolbar.Translucent = true;

            // Se crean los botones de la barra y se asignan los Eventos de los mismos.
            toolbar.Items = new UIBarButtonItem[]{
                new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                new UIBarButtonItem(UIBarButtonSystemItem.Cancel, delegate {
                    searchBar.ResignFirstResponder();
                })
            };

            // Se asgrega la barra para cancelar al teclado, y se asigna el searchBar como el componente del t�tulo.
            searchBar.InputAccessoryView = toolbar;
            this.NavigationItem.TitleView = searchBar;
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a aparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            // Si no se pudo cargar clientes, se bloquea la b�squeda r�pida de lo contrario se habilita.
            if (ClientesView.clientesCargados.Count > 0)
            {
                searchBar.Placeholder = "B�squeda r�pida de Clientes";

                searchBar.UserInteractionEnabled = true;
            }
            else
            {
                searchBar.Placeholder = "No hay clientes disponibles";

                searchBar.UserInteractionEnabled = false;
            }
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla apareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            
            // Se verifica si el servicio de localizaci�n est� disponible.
            if (CLLocationManager.LocationServicesEnabled)
            {
                // Se inicializa el administrador de localizaci�n.
                locationManager = new CLLocationManager();

                // Si est� usando la versi�n de iOS 8.0 o mayor, preguntar al usuario por permiso.
                if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
                {
                    locationManager.RequestWhenInUseAuthorization();
                }

                // Evento que se ejecuta cuando el administrador actualiza la localizaci�n.
                locationManager.LocationsUpdated += delegate(object sender, CLLocationsUpdatedEventArgs e)
                {
                    foreach (CLLocation loc in e.Locations)
                    {
                        //Toast.MakeToast(View, "Lat: " + loc.Coordinate.Latitude + " | long: " + loc.Coordinate.Longitude,
                        //    null, UIColor.Black, true, enumDuracionToast.Corto);

                        //Toast.MakeToast(View, "Lat: " + map_Mapa.UserLocation.Coordinate.Latitude + " | long: " + map_Mapa.UserLocation.Coordinate.Longitude,
                        //    null, UIColor.Black, true, enumDuracionToast.Corto);
                    }
                };

                // Se inicia la b�squeda de localizaci�n del usuario.
                locationManager.StartUpdatingLocation();

                locationManager.StartUpdatingHeading();
                //locationManager.StartMonitoringSignificantLocationChanges();
            }

            mostrarClientes();

            // Se inicia el proceso de intentar sincronizar pedidos pendientes.
            AppDelegate.IniciarSincronizacionPedidos(this.ParentViewController.ParentViewController);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a desaparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillDisappear(bool animated)
        {
            // Se verifica si el servicio de localizaci�n est� disponible.
            if (CLLocationManager.LocationServicesEnabled)
            {
                if (locationManager != null)
                {
                    locationManager.StopUpdatingLocation();

                    locationManager.StopUpdatingHeading();
                }
            }

            base.ViewWillDisappear(animated);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla desapareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
    }
}

