// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Softland.iOS.Pem
{
	[Register ("MapaView")]
	partial class MapaView
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_GuardarGeoloc { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_Tipo { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		MapKit.MKMapView map_Mapa { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITabBarItem tabMapa { get; set; }

		[Action ("btn_GuardarGeoloc_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_GuardarGeoloc_TouchUpInside (UIButton sender);

		[Action ("btn_Tipo_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_Tipo_TouchUpInside (UIButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (btn_GuardarGeoloc != null) {
				btn_GuardarGeoloc.Dispose ();
				btn_GuardarGeoloc = null;
			}
			if (btn_Tipo != null) {
				btn_Tipo.Dispose ();
				btn_Tipo = null;
			}
			if (map_Mapa != null) {
				map_Mapa.Dispose ();
				map_Mapa = null;
			}
			if (tabMapa != null) {
				tabMapa.Dispose ();
				tabMapa = null;
			}
		}
	}
}
