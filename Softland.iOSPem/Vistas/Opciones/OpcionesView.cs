using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Data.SQLite;

using Pedidos.Core;
using BI.iOS;
using BI.Shared;

namespace Softland.iOS.Pem
{
	partial class OpcionesView : UITableViewController
	{

        #region Propiedades y Contructor de instancia

        /// <summary>
        /// Referencia a la vista de carga que es mostrada al realizar la sincronizaci�n de datos.
        /// </summary>
        static LoadingOverlay loadingOverlay;

        /// <summary>
        /// Referencia a la ventana PopUp con la lista de opciones de cargas parciales.
        /// </summary>
        public PopUpView ventanaCargasParciales;

        /// <summary>
        /// Referencia a la ventana PopUp de Acerda de.
        /// </summary>
        public PopUpView ventanaAcercaDe;

        /// <summary>
        /// Referencia a la im�gen de fondo de la ventana PopUp de Acerda de.
        /// </summary>
        public UIImageView ImagenFondo;

        /// <summary>
        /// Lista de compa��as disponibles para seleccionar como compa��a general.
        /// </summary>
        public List<string> companias;

        /// <summary>
        /// Variable que almacena el valor de la compa�ia general seleccionada.
        /// </summary>
        public string CIA = string.Empty;

        /// <summary>
        /// Lista de opciones de cargas parciales disponibles para el usuario.
        /// </summary>
        public List<string> cargasParciales;
                
        /// <summary>
        /// Costructor por defecto.
        /// </summary>
        /// <param name="handle"></param>
		public OpcionesView (IntPtr handle) : base (handle)
		{
		}

        #endregion

        #region Tabla Cargas Parciales

        /// <summary>
        /// Clase que se utilizar� para manejar la informaci�n de la tabla de selecci�n de cliente.
        /// </summary>
        public class TableSource : UITableViewSource
        {

            #region Propiedades y Contructor de instancia

            /// <summary>
            /// Variable para identificar las celdas que utiliza este TableSource.
            /// </summary>
            string cellIdentifier = "";

            /// <summary>
            /// Referencia a la pantalla para poder acceder a sus propiedades desde otra clase. 
            /// </summary>
            OpcionesView VistaPadre;

            /// <summary>
            /// Constructor por defecto. 
            /// </summary>
            /// <param name="padre"></param>
            public TableSource(OpcionesView padre, string CellID)
            {
                VistaPadre = padre;
                cellIdentifier = CellID;
            }

            #endregion

            #region Eventos

            // Evento requerido por la clase, retorna la cantidad de registros en la fuente de datos.
            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return VistaPadre.cargasParciales.Count;
            }

            // Evento requerido por la clase, retorna la altura de las celdas de la lista.
            public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
            {
                float height = 44;

                if ((float)tableView.Bounds.Height / VistaPadre.cargasParciales.Count > 44)
                {
                    height = (float)tableView.Bounds.Width / VistaPadre.cargasParciales.Count;
                }

                return height;
            }

            /// <summary>
            /// Evento que se ejecuta al cargar las celdas de la tabla seg�n vayan siendo mostradas.
            /// </summary>
            /// <param name="tableView"></param>
            /// <param name="indexPath">�ndice de la celda por cargar.</param>
            /// <returns>Retorna la celda cargada.</returns>
            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                // Busca la celda creada para ahorrar memoria.
                CustomParcialCell cell = tableView.DequeueReusableCell(cellIdentifier) as CustomParcialCell;

                // Si no hay celdas por reutilizar, se crea una nueva, en este caso se usa una celda predefinida Subtitle.
                if (cell == null)
                {
                    cell = new CustomParcialCell((NSString)cellIdentifier);
                }

                // Se actualiza la informaci�n de la celda.
                cell.UpdateCell(VistaPadre.cargasParciales[indexPath.Row]);

                return cell;
            }

            /// <summary>
            /// Evento que se ejecuta al seleccionar una celda de la tabla.
            /// </summary>
            /// <param name="tableView"></param>
            /// <param name="indexPath">�ndice de la celda seleccionada.</param>
            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                List<string> creates = new List<string>();
                List<string> inserts = new List<string>();

                switch (indexPath.Row)
                {
                    case 0:
                        {
                            creates = new List<string>() { "ERPADMIN_CLASIFICACION_crear", "ERPADMIN_ARTICULO_crear", "ERPADMIN_BONIFICACION_NIVEL_crear", 
                            "ERPADMIN_ARTICULO_EXISTENCIA_LOTE_crear","ERPADMIN_ARTICULO_EXISTENCIA_crear","ERPADMIN_IMPUESTO_crear","ERPADMIN_ARTICULO_PRECIO_crear",
                        "ERPADMIN_NIVEL_LISTA_crear","ERPADMIN_BONIFICACION_CLIART_crear","ERPADMIN_DESCUENTO_CLAS_crear","ERPADMIN_DESCUENTO_NIVEL_crear",
                        "ERPADMIN_DESCUENTO_CLIART_crear","ERPADMIN_DES_BON_ESCALA_BONIFICACION_crear","ERPADMIN_DES_BON_PAQUETE_crear","ERPADMIN_DES_BON_REGLA_crear",
                        "ERPADMIN_DES_BON_PAQUETE_REGLA_crear"};
                            inserts = new List<string>() { "ERPADMIN_CLASIFICACION_llenar", "ERPADMIN_ARTICULO_llenar", "ERPADMIN_BONIFICACION_NIVEL_llenar", 
                            "ERPADMIN_ARTICULO_EXISTENCIA_LOTE_llenar","ERPADMIN_ARTICULO_EXISTENCIA_llenar","ERPADMIN_IMPUESTO_llenar","ERPADMIN_ARTICULO_PRECIO_llenar",
                        "ERPADMIN_NIVEL_LISTA_llenar","ERPADMIN_BONIFICACION_CLIART_llenar","ERPADMIN_DESCUENTO_CLAS_llenar","ERPADMIN_DESCUENTO_NIVEL_llenar",
                        "ERPADMIN_DESCUENTO_CLIART_llenar","ERPADMIN_DES_BON_ESCALA_BONIFICACION_llenar","ERPADMIN_DES_BON_PAQUETE_llenar","ERPADMIN_DES_BON_REGLA_llenar",
                        "ERPADMIN_DES_BON_PAQUETE_REGLA_llenar"};
                            break;
                        }
                    case 1:
                        {
                            creates = new List<string>() {"ERPADMIN_ALFAC_PAIS_crear", "ERPADMIN_IMPUESTO_crear", "ERPADMIN_CLIENTE_CIA_crear", "ERPADMIN_CLIENTE_crear", "ERPADMIN_ALFAC_DIR_EMB_CLT_crear",
                            "ERPADMIN_ALFAC_CND_PAG_crear","ERPADMIN_DIVISION_GEOGRAFICA1_crear","ERPADMIN_DIVISION_GEOGRAFICA2_crear","ERPADMIN_BONIFICACION_CLIART_crear","ERPADMIN_DESCUENTO_CLAS_crear","ERPADMIN_DESCUENTO_NIVEL_crear",
                        "ERPADMIN_DESCUENTO_CLIART_crear","ERPADMIN_DES_BON_ESCALA_BONIFICACION_crear","ERPADMIN_DES_BON_PAQUETE_crear","ERPADMIN_DES_BON_REGLA_crear", "ERPADMIN_CLIENTE_GEOLOCALIZACION_crear",
                        "ERPADMIN_DES_BON_PAQUETE_REGLA_crear"};
                            inserts = new List<string>() {"ERPADMIN_ALFAC_PAIS_llenar", "ERPADMIN_IMPUESTO_llenar", "ERPADMIN_CLIENTE_CIA_llenar", "ERPADMIN_CLIENTE_llenar", "ERPADMIN_ALFAC_DIR_EMB_CLT_llenar",
                            "ERPADMIN_ALFAC_CND_PAG_llenar","ERPADMIN_DIVISION_GEOGRAFICA1_llenar","ERPADMIN_DIVISION_GEOGRAFICA2_llenar","ERPADMIN_BONIFICACION_CLIART_llenar","ERPADMIN_DESCUENTO_CLAS_llenar","ERPADMIN_DESCUENTO_NIVEL_llenar",
                        "ERPADMIN_DESCUENTO_CLIART_llenar","ERPADMIN_DES_BON_ESCALA_BONIFICACION_llenar","ERPADMIN_DES_BON_PAQUETE_llenar","ERPADMIN_DES_BON_REGLA_llenar", "ERPADMIN_CLIENTE_GEOLOCALIZACION_llenar",
                        "ERPADMIN_DES_BON_PAQUETE_REGLA_llenar"};
                            break;
                        }
                    case 2:
                        {
                            creates = new List<string>() { "ERPADMIN_ALSYS_PRM_crear" };
                            inserts = new List<string>() { "ERPADMIN_ALSYS_PRM_llenar" };
                            break;
                        }
                    case 3:
                        {
                            creates = new List<string>() { "ERPADMIN_ARTICULO_EXISTENCIA_LOTE_crear", "ERPADMIN_ARTICULO_EXISTENCIA_crear" };
                            inserts = new List<string>() { "ERPADMIN_ARTICULO_EXISTENCIA_LOTE_llenars", "ERPADMIN_ARTICULO_EXISTENCIA_llenar" };
                            break;
                        }
                    case 4:
                        {
                            creates = new List<string>() { "ERPADMIN_COMPANIA_crear", "ERPADMIN_GLOBALES_MODULO_crear", "ERPADMIN_GLOBALES_FR_crear", "ERPADMIN_ALSYS_PRM_crear",
                            "ERPADMIN_RUTA_CFG_crear","ERPADMIN_CONFIG_HH_crear"};
                            inserts = new List<string>() { "ERPADMIN_COMPANIA_llenar", "ERPADMIN_GLOBALES_MODULO_llenar", "ERPADMIN_GLOBALES_FR_llenar", "ERPADMIN_ALSYS_PRM_llenar",
                            "ERPADMIN_RUTA_CFG_llenar","ERPADMIN_CONFIG_HH_llenar"};
                            break;
                        }
                    case 5:
                        {
                            creates = new List<string>() { "ERPADMIN_ALCXC_PEN_COB_crear" };
                            inserts = new List<string>() { "ERPADMIN_ALCXC_PEN_COB_llenar" };
                            break;
                        }
                    case 6:
                        {
                            creates = new List<string>() { "ERPADMIN_CLASIFICACION_crear", "ERPADMIN_BONIFICACION_NIVEL_crear", "ERPADMIN_IMPUESTO_crear",
                            "ERPADMIN_ARTICULO_PRECIO_crear","ERPADMIN_NIVEL_LISTA_crear","ERPADMIN_DESCUENTO_NIVEL_crear","ERPADMIN_BONIFICACION_CLIART_crear",
                            "ERPADMIN_DESCUENTO_CLAS_crear"};
                            inserts = new List<string>() { "ERPADMIN_CLASIFICACION_llenar", "ERPADMIN_BONIFICACION_NIVEL_llenar", "ERPADMIN_IMPUESTO_llenar",
                            "ERPADMIN_ARTICULO_PRECIO_llenar","ERPADMIN_NIVEL_LISTA_llenar","ERPADMIN_DESCUENTO_NIVEL_llenar","ERPADMIN_BONIFICACION_CLIART_llenar",
                            "ERPADMIN_DESCUENTO_CLAS_llenar"};
                            break;
                        }
                    case 7:
                        {
                            creates = new List<string>() { "ERPADMIN_CLASIFICACION_crear", "ERPADMIN_BONIFICACION_NIVEL_crear", 
                         "ERPADMIN_BONIFICACION_CLIART_crear","ERPADMIN_DESCUENTO_CLAS_crear","ERPADMIN_DESCUENTO_NIVEL_crear",
                        "ERPADMIN_DESCUENTO_CLIART_crear","ERPADMIN_DES_BON_ESCALA_BONIFICACION_crear","ERPADMIN_DES_BON_PAQUETE_crear","ERPADMIN_DES_BON_REGLA_crear",
                        "ERPADMIN_DES_BON_PAQUETE_REGLA_crear"};
                            inserts = new List<string>() { "ERPADMIN_CLASIFICACION_llenar", "ERPADMIN_BONIFICACION_NIVEL_llenar", 
                         "ERPADMIN_BONIFICACION_CLIART_llenar","ERPADMIN_DESCUENTO_CLAS_llenar","ERPADMIN_DESCUENTO_NIVEL_llenar",
                        "ERPADMIN_DESCUENTO_CLIART_llenar","ERPADMIN_DES_BON_ESCALA_BONIFICACION_llenar","ERPADMIN_DES_BON_PAQUETE_llenar","ERPADMIN_DES_BON_REGLA_llenar",
                        "ERPADMIN_DES_BON_PAQUETE_REGLA_llenar"};
                            break;
                        }
                    case 8:
                        {
                            creates = new List<string>() { "SYSTEM_USUARIO_crear", "SYSTEM_PRIVILEGIO_EX_crear" };
                            inserts = new List<string>() { "SYSTEM_USUARIO_llenar", "SYSTEM_PRIVILEGIO_EX_llenar" };
                            break;
                        }
                    default: break;
                }

                VistaPadre.ventanaCargasParciales.Hide();

                VistaPadre.InicializarSincronizacion(creates, inserts);
            }

            #endregion
        }


        /// <summary>
        /// Clase que se utilizar� para manejar la informaci�n de la celda de la tabla de art�culos en el pedido.
        /// </summary>
        public class CustomParcialCell : UITableViewCell
        {

            #region Propiedades y Contructor de instancia

            /// <summary>
            /// Controladores para mostrar el nombre y la cantidad en el carrito del art�culo.
            /// </summary>
            UILabel lbl_Parcial;

            /// <summary>
            /// �ndice del art�culo en el carrito.
            /// </summary>
            public int parcial;

            /// <summary>
            /// Constructor por defecto.
            /// </summary>
            /// <param name="cellId"></param>
            public CustomParcialCell(NSString cellId)
                : base(UITableViewCellStyle.Default, cellId)
            {
                SelectionStyle = UITableViewCellSelectionStyle.Blue;
                ContentView.BackgroundColor = UIColor.Clear;

                lbl_Parcial = new UILabel()
                {
                    TextAlignment = UITextAlignment.Center,
                    BackgroundColor = UIColor.Clear,
                    Font = UIFont.FromName("Helvetica-Bold", 22f),
                    AdjustsFontSizeToFitWidth = true,
                    MinimumFontSize = 8
                };

                // Se agregan los controladores a la celda.
                ContentView.AddSubviews(new UIView[] { lbl_Parcial });
            }

            #endregion

            #region Eventos

            /// <summary>
            /// Evento que permite acomodar las dimensiones y posici�n de las Subvistas en la celda.
            /// </summary>
            public override void LayoutSubviews()
            {
                base.LayoutSubviews();

                lbl_Parcial.Frame = new RectangleF(10, 0, (float)ContentView.Bounds.Width - 10, (float)ContentView.Bounds.Height);
            }

            #endregion

            #region M�todos

            /// <summary>
            /// M�todo que sirve para actualizar los datos de la celda, llamado al cargar cada celda. 
            /// </summary>
            /// <param name="nombre">Nombre del art�culo.</param>
            /// <param name="cantidad">Cantidad seleccionada del art�culo.</param>
            public void UpdateCell(string nombre)
            {
                lbl_Parcial.Text = nombre;
            }

            #endregion
        }

        #endregion

        #region Eventos

        /// <summary>
        /// Evento de click del bot�n btn_Sincronizar.
        /// Se usa para consultar al usuario el tipo de sincronizaci�n que desea realizar con el servidor e
        /// inicializar el proceso de sincronizaci�n seleccionado.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_Sincronizar_TouchUpInside(UIButton sender)
        {
            var actionSheet = new UIActionSheet("Sincronizar");
            actionSheet.TintColor = UIColor.FromRGB(225, 106, 12);
            actionSheet.AddButton("Completo");
            actionSheet.AddButton("Parcial");
            actionSheet.AddButton("Im�genes");
            actionSheet.AddButton("Cancelar");
            // Bot�n rojo.
            //actionSheet.DestructiveButtonIndex = 1; 
            // Bot�n negro de Cancelar
            actionSheet.CancelButtonIndex = 3;

            // Evento click del bot�n Datos o Im�genes.
            actionSheet.Clicked += delegate(object a, UIButtonEventArgs b)
            {
                switch (b.ButtonIndex)
                {
                    case 0: // Completa.

                        // Mandar a hacer una carga completa de los datos.
                        InicializarSincronizacion(null, null);

                        break;
                    case 1: // Parcial.

                        // Mostrar la lista de opciones de cargas parciales.
                        CrearPopUpParciales();

                        break;
                    case 2: // Im�genes.

                        InvokeOnMainThread(async () =>
                        {
                            // Determine the correct size to start the overlay (depending on device orientation)
                            var bounds = UIScreen.MainScreen.Bounds; // portrait bounds
                            if (UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft || UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight)
                            {
                                bounds.Size = new SizeF((float)bounds.Size.Width, (float)bounds.Size.Height);
                            }
                            // show the loading overlay on the UI thread using the correct orientation sizing
                            loadingOverlay = new LoadingOverlay((RectangleF)bounds);

                            ParentViewController.ParentViewController.Add(loadingOverlay);

                            // Se muestra el indicador de actividad con la red.
                            UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;

                            // Se crea un ID para la tarea que se ejecutar�, esto permite que el OS de oportunidad de tener una
                            // tarea en el background por un tiempo de m�ximo 10 minutos antes de terminar la aplicacion por si mismo. 
                            nint taskID = UIApplication.SharedApplication.BeginBackgroundTask(() =>
                            {
                                // Evento en caso de que el timepo restante de la tarea est� a punto de terminar, se debe hacer 
                                // una limpieza de las variables usadas o terminar la tarea para evitar una terminaci�n
                                // total de la aplicaci�n por el OS.

                                double timeRemaining = UIApplication.SharedApplication.BackgroundTimeRemaining;

                                if (timeRemaining < 20)
                                {
                                    Toast.MakeToast(ParentViewController.View, "La tarea de sincronizacion va a expirar", null, UIColor.Black, true, enumDuracionToast.Mediano);
                                }
                            });

                            // Se inicia el proceso de intentar sincronizar pedidos pendientes.
                            AppDelegate.IniciarSincronizacionPedidos(this.ParentViewController.ParentViewController);

                            // Se llama al m�todo que se encarga de realizar la sincronizaci�n.
                            string result = await SincronizarImagenes(GestorDatos.NombreUsuario);

                            // Se oculta el indicador de actividad con la red.
                            UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
                            
                            // Si la sincronizaci�n termin� exitosamente.
                            if (result.Equals("Sincronizaci�n realizada correctamente."))
                            {
                                new UIAlertView("Proceso finalizado", result, null, "OK", null).Show();
                            }
                            else
                            {
                                // Se notifica de alg�n error que ocurri� con la sincronizaci�n.
                                new UIAlertView("Sincronizaci�n detenida", result, null, "OK", null).Show();
                            }

                            // Se termina el proceso que se inici� con el ID establecido, para evitar que el OS temine la aplicaci�n.
                            UIApplication.SharedApplication.EndBackgroundTask(taskID);
                        });

                        break;
                }
            };

            actionSheet.ShowInView(View);
        }

        /// <summary>
        /// Evento de click del bot�n btn_CerrarSesion.
        /// Se usa para devolver al usuario a la pantalla de iniciar sesi�n, si se usa esta opci�n se reinicia el 
        /// NSUserDefaults de contrasena para forzar a que la ingrese nuevamente.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_CerrarSesion_TouchUpInside(UIButton sender)
        {
            UIAlertView alert = new UIAlertView("Cerrar sesi�n", "�Seguro que desea cerrar la sesi�n?", null, "Salir", "Cancelar");

            // Evento click del bot�n de Aceptar.
            alert.Clicked += (s, b) =>
            {
                if (b.ButtonIndex == 0)
                {
                    // Se reinicia la variable de contrase�a y se devuelve al usuario a la pantalla de Iniciar sesi�n.

                    AppDelegate.SetConfigVar("", "contrasena");

                    // Limpia los clientes cargados.
                    ClientesView.clientesCargados.Clear();
                    ClientesView.clientesCompania = string.Empty;

                    // Limpia los art�culos cargados.
                    ArticulosVista.articulosCargados.Clear();
                    ArticulosVista.articulosCompania = string.Empty;

                    // Limpiar la lista de compa��as gestionadas.
                    Compania.Gestionadas = new List<Compania>();

                    if (GlobalUI.ClienteActual != null)
                    {
                        // Limpia y reinicia los datos del pedido global.
                        PedidosCollection.FacturarPedido = false;
                        GestorPedido.PedidosCollection = new PedidosCollection();
                        Pedido.BorrarXMLPedido(GlobalUI.ClienteActual.Codigo);

                        // Limpia el carrito de compras.
                        GestorPedido.carrito = null;

                        // Limpia al cliente del pedido.
                        GlobalUI.ClienteActual = null;
                        GlobalUI.RutaActual = null;
                        GlobalUI.RutasActuales = null;

                        // Asigna que no se est� creando una orden.
                        PedidosView.crearOrden = false;
                    }

                    ParentViewController.DismissViewController(true, null);
                }

            };
            alert.Show();
        }

        /// <summary>
        /// Evento de click del bot�n btn_IdDispositivo.
        /// Se usa para mostrar al usuario un mensaje con el ID del dispositivo.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_IdDispositivo_TouchUpInside(UIButton sender)
        {
            new UIAlertView(AppDelegate.UUID, null, null, "Ok", null).Show();
        }

        /// <summary>
        /// Evento de click del bot�n btn_Servidor.
        /// Se usa para permitir que el usuario asigne una nueva direcci�n para el servidor.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_Servidor_TouchUpInside(UIButton sender)
        {
            UIAlertView alert = new UIAlertView();
            alert.TintColor = UIColor.FromRGB(225, 106, 12);
            alert.Title = "Servidor";
            alert.AddButton("Cancelar");
            alert.AddButton("Guardar");
            alert.Message = "Ingrese la direcci�n del servidor.";
            alert.AlertViewStyle = UIAlertViewStyle.PlainTextInput;
            alert.GetTextField(0).Text = AppDelegate.Servidor;

            // Evento click del bot�n de Guardar.
            alert.Clicked += (object s, UIButtonEventArgs ev) =>
            {
                if (ev.ButtonIndex == 1)
                {
                    // Se guarda la nueva direcci�n del servidor ingresada por el usuario.

                    AppDelegate.Servidor = alert.GetTextField(0).Text;

                    if (!AppDelegate.Servidor.Contains("http://"))
                    {
                        AppDelegate.Servidor = "http://" + AppDelegate.Servidor;
                    }

                    lbl_DatoServidor.Text = AppDelegate.Servidor;

                    AppDelegate.SetConfigVar(AppDelegate.Servidor, "servidor");

                    this.ReloadInputViews();

                    Toast.MakeToast(ParentViewController.View, "Direcci�n del servidor guardada (" + alert.GetTextField(0).Text + ").", null, UIColor.Black, true, enumDuracionToast.Corto);
                }
            };

            alert.Show();
        }

        /// <summary>
        /// Evento de click del bot�n btn_Soporte.
        /// Se usa para abrir en el buscador la p�gina Web de soporte de la aplicaci�n.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_Soporte_TouchUpInside(UIButton sender)
        {
            // Se ejecuta un m�todo as�ncrono para intentar abrir la p�gina de soporte.
            Task.Run(async () =>
            {
                try
                {
                    // Se intenta abrir el link por medio de Rivets por si hay una o varias aplicaci�nes que soporten el link.
                    var result = await Rivets.AppLinks.Navigator.Navigate(AppDelegate.webSoporte);

                    // Si no se pudo abrir con Rivets, se abre el link por medio del buscador Web.
                    if (result == Rivets.NavigationResult.Failed || result == Rivets.NavigationResult.Web)
                    {
                        InvokeOnMainThread(() => { UIApplication.SharedApplication.OpenUrl(new NSUrl(AppDelegate.webSoporte)); });
                    }
                }
                catch
                {
                    InvokeOnMainThread(() => {
                        Toast.MakeToast(ParentViewController.View, "La direcci�n Web de la p�gina de soporte no est� configurada correctamente, consulte con su administrador.",
                            null, UIColor.Black, true, enumDuracionToast.Mediano);
                    });
                }
            });
        }

        /// <summary>
        /// Evento de click del bot�n btn_Acercade.
        /// Se usa para abrir una ventana de informaci�n sobre la versi�n actual de la aplicaci�n.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_Acercade_TouchUpInside(UIButton sender)
        {
            string Imagen = "Acerca-De-Vertical.png";

            // Determine the correct size to start the overlay (depending on device orientation)
            var bounds = UIScreen.MainScreen.Bounds;
            var Orientacion = UIApplication.SharedApplication.StatusBarOrientation;
            if (Orientacion == UIInterfaceOrientation.LandscapeLeft || Orientacion == UIInterfaceOrientation.LandscapeRight)
            {
                bounds.Size = new SizeF((float)bounds.Size.Width, (float)bounds.Size.Height);

                Imagen = "Acerca-De-Horizontal.png";
            }

            // Crear vista que se va a mostrar en la vista PopUp, adem�s crear y configurar los controles que se le agregar�n.

            // Dimensiones de la ventana del PopUp.
            float VPX = 0, VPY = 0, VPAncho = 0, VPAlto = 0;
            // Se manda a obtener las dimensiones que tendr�a la ventana principal del PopUp.
            PopUpView.ObtenerDimensiones((RectangleF)bounds, 0.9f, ref VPX, ref VPY, ref VPAncho, ref VPAlto, Orientacion);

            // Vista de pantalla Acerca De con los controles a mostrar.
            UIView VistaMostrar = new UIView(new RectangleF(VPX, VPY, VPAncho, VPAlto));

            // Controles a mostrar en pantalla Acerca De.
            UILabel lbl_Header, lbl_Footer;
            UITextView txv_Body;

            lbl_Header = new UILabel()
            {
                AttributedText = new NSAttributedString("Pedidos M�vil", 
                    new UIStringAttributes { UnderlineStyle = NSUnderlineStyle.Single }),
                TextAlignment = UITextAlignment.Left,
                BackgroundColor = UIColor.Clear,
                Font = UIFont.FromName("Helvetica", 22f),
                AutoresizingMask = UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleRightMargin,
                AdjustsFontSizeToFitWidth = true,
                MinimumFontSize = 8
            };
            lbl_Header.Frame = new RectangleF(20, 20, VPAncho * 0.6f, 33);
            
            txv_Body = new UITextView()
            {
                Text = "Pedidos M�vil es una aplicaci�n que permite al usuario consultar en su dispositivo informaci�n del ERP relacionada con las ventas y generar pedidos que luego se ver�n reflejados en el ERP Desktop.\n\n\nPrincipales caracter�sticas:\n\n- Consulta de art�culos, existencias, clientes y documentos de los clientes.\n\n- Generaci�n de pedidos.\n\n- Modalidad Online para que los pedidos generados sean enviados a la base de datos del ERP si tiene conexi�n.\n\n- Modalidad Offline que permite al usuario generar pedidos aun sin conexi�n, luego al recuperar la conexi�n los pedidos pendientes se sincronizan.\n\n- C�lculo de los impuestos, descuentos y bonificaciones en el dispositivo m�vil.",
                Editable = false,
                TranslatesAutoresizingMaskIntoConstraints = true,
                TextAlignment = UITextAlignment.Justified,
                BackgroundColor = UIColor.Clear,
                AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin | 
                                    UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth,
                Font = UIFont.FromName("Helvetica", 17f),
            };
            //txv_Body.Layer.BorderColor = UIColor.Black.CGColor;
            //txv_Body.Layer.BorderWidth = 1.5f;
            txv_Body.Frame = new RectangleF(20, (20 + 33 + 15), VPAncho * 0.6f, VPAlto - (120 + 33 + 20));

            lbl_Footer = new UILabel()
            {
                AttributedText = new NSAttributedString("Versi�n: " + AppDelegate.Version,
                    new UIStringAttributes { UnderlineStyle = NSUnderlineStyle.Single }),
                TextAlignment = UITextAlignment.Center,
                BackgroundColor = UIColor.Clear,
                Font = UIFont.FromName("Helvetica", 17f),
                AutoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleBottomMargin
                                        | UIViewAutoresizing.FlexibleWidth,
                AdjustsFontSizeToFitWidth = true,
                MinimumFontSize = 4
            };
            lbl_Footer.Frame = new RectangleF(0, VPAlto - ((VPAlto * 0.12f) + 33), VPAncho, 33);

            // Imagen de fondo del PopUp.
            ImagenFondo = new UIImageView(new RectangleF(0, 0, VPAncho, VPAlto));
            ImagenFondo.Layer.CornerRadius = 10f;
            ImagenFondo.Layer.MasksToBounds = true;
            ImagenFondo.ContentMode = UIViewContentMode.ScaleToFill;
            ImagenFondo.AutoresizingMask = UIViewAutoresizing.FlexibleDimensions;
            ImagenFondo.Image = UIImage.FromFile(Imagen);

            // Se agregan los controles en el orden que deben ser mostrados.
            VistaMostrar.Add(ImagenFondo);
            VistaMostrar.Add(lbl_Header);
            VistaMostrar.Add(txv_Body);
            VistaMostrar.Add(lbl_Footer);
            
            // Se manda a crear la vista PopUp con los parametros que se desean para esta vista.
            ventanaAcercaDe = new PopUpView((RectangleF)bounds, Orientacion, VistaMostrar, UIColor.FromRGB(225, 106, 12), UIColor.White, UIColor.Black);

            // Se agrega al controlador de la ventana principal para mostrarlo.
            ParentViewController.ParentViewController.Add(ventanaAcercaDe);
        }
        
        /// <summary>
        /// Evento de click del bot�n btn_borrarBD.
        /// Se usa para borrar del dispositivo toda la informaci�n guardada por la aplicaci�n (Base de datos,
        /// Im�genes y Configuraci�n).
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_borrarBD_TouchUpInside(UIButton sender)
        {
            UIAlertView alert = new UIAlertView("Limpiar datos", "�Seguro que desea borrar toda la informaci�n que utiliza la " +
                "aplicaci�n? (Configuraci�n, Datos e Imagenes de la aplicaci�n)", null, "Limpiar", "Cancelar");

            // Evento click del bot�n de Aceptar.
            alert.Clicked += (s, b) =>
            {
                if (b.ButtonIndex == 0)
                {
                    UIAlertView alert2 = new UIAlertView("Limpiar datos",
                        "Si contin�a la informaci�n eliminada no podr� ser restaurada nuevamente.", null, "Continuar", "Cancelar");
                           
                    // Evento click del bot�n de Aceptar.
                    alert2.Clicked += (s2, b2) =>
                    {
                        if (b2.ButtonIndex == 0)
                        {
                            // Se intenta borrar toda la informaci�n de la aplicaci�n y se notifica al usuario del resultado.
                            if (AppDelegate.borrarBaseDatos())
                            {
                                new UIAlertView("Proceso finalizado", "Todos los datos han sido limpiados, a continuaci�n se cerrar� la sesi�n.", null, "OK", null);

                                // Limpia los clientes cargados.
                                ClientesView.clientesCargados.Clear();
                                ClientesView.clientesCompania = string.Empty;

                                // Limpia los art�culos cargados.
                                ArticulosVista.articulosCargados.Clear();
                                ArticulosVista.articulosCompania = string.Empty;

                                // Limpiar la lista de compa��as gestionadas.
                                Compania.Gestionadas = new List<Compania>();

                                if (GlobalUI.ClienteActual != null)
                                {
                                    // Limpia y reinicia los datos del pedido global.
                                    PedidosCollection.FacturarPedido = false;
                                    GestorPedido.PedidosCollection = new PedidosCollection();
                                    Pedido.BorrarXMLPedido(GlobalUI.ClienteActual.Codigo);

                                    // Limpia el carrito de compras.
                                    GestorPedido.carrito = null;

                                    // Limpia al cliente del pedido.
                                    GlobalUI.ClienteActual = null;
                                    GlobalUI.RutaActual = null;
                                    GlobalUI.RutasActuales = null;

                                    // Asigna que no se est� creando una orden.
                                    PedidosView.crearOrden = false;
                                }

                                // Se cierra la sesi�n del usuario dado a que se borr� toda la informaci�n.
                                ParentViewController.DismissViewController(true, null);
                            }
                            else
                            {
                                new UIAlertView("Advertencia", "Ocurri� un problema con el proceso, int�ntelo nuevamente.", null, "OK", null).Show();
                            }
                        }

                    };
                    alert2.Show();
                }

            };
            alert.Show();
        }

        /// <summary>
        /// Evento de click del bot�n btn_SincronizarPedidos.
        /// Se usa para iniciar la sincronizaci�n de los pedidos pendientes.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_SincronizarPedidos_TouchUpInside(UIButton sender)
        {
            InvokeOnMainThread(async () =>
            {
                // Determine the correct size to start the overlay (depending on device orientation)
                var bounds = UIScreen.MainScreen.Bounds; // portrait bounds
                if (UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft || UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight)
                {
                    bounds.Size = new SizeF((float)bounds.Size.Width, (float)bounds.Size.Height);
                }
                // show the loading overlay on the UI thread using the correct orientation sizing
                loadingOverlay = new LoadingOverlay((RectangleF)bounds);

                ParentViewController.ParentViewController.Add(loadingOverlay);

                // Se muestra el indicador de actividad con la red.
                UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;

                // Se crea un ID para la tarea que se ejecutar�, esto permite que el OS de oportunidad de tener una
                // tarea en el background por un tiempo de m�ximo 10 minutos antes de terminar la aplicacion por si mismo. 
                nint taskID = UIApplication.SharedApplication.BeginBackgroundTask(() =>
                {
                    // Evento en caso de que el timepo restante de la tarea est� a punto de terminar, se debe hacer 
                    // una limpieza de las variables usadas o terminar la tarea para evitar una terminaci�n
                    // total de la aplicaci�n por el OS.

                    double timeRemaining = UIApplication.SharedApplication.BackgroundTimeRemaining;

                    if (timeRemaining < 20)
                    {
                        Toast.MakeToast(ParentViewController.View, "La tarea de sincronizacion va a expirar", null, UIColor.Black, true, enumDuracionToast.Mediano);
                    }
                });

                // Se llama al m�todo que se encarga de realizar la sincronizaci�n de los pedidos.
                string result = await AppDelegate.SincronizarPedidos();

                loadingOverlay.Hide();

                // Se oculta el indicador de actividad con la red.
                UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;

                // Si el resultado no es nulo o vacio, quiere decir que se sincronizaron pedidos exitosamente.
                if (!string.IsNullOrEmpty(result))
                {
                    if (!result.ToUpper().Contains("ERROR"))
                    {
                        obtenerDatosGenerales();

                        Toast.MakeToast(ParentViewController.View, result, null, UIColor.FromRGB(0, 200, 0), true, enumDuracionToast.Mediano);
                    }
                    else
                    {
                        Toast.MakeToast(ParentViewController.View, result, null, UIColor.Black, true, enumDuracionToast.Mediano);
                    }
                }

                // Se termina el proceso que se inici� con el ID establecido, para evitar que el OS temine la aplicaci�n.
                UIApplication.SharedApplication.EndBackgroundTask(taskID);
            });
        }

        /// <summary>
        /// Evento que se ejecuta al cambiar la orientaci�n del dispositivo.
        /// Se usa para cambiar la im�gen de fondo de la ventana PopUp de Acerda de.
        /// </summary>
        /// <param name="toInterfaceOrientation"></param>
        /// <param name="duration"></param>
        public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
        {
            if (ventanaAcercaDe != null && ventanaAcercaDe.Hidden == false)
            {
                if (toInterfaceOrientation == UIInterfaceOrientation.LandscapeLeft ||
                    toInterfaceOrientation == UIInterfaceOrientation.LandscapeRight)
                {
                    ImagenFondo.Image = UIImage.FromFile("Acerca-De-Horizontal.png");
                }
                else
                {
                    ImagenFondo.Image = UIImage.FromFile("Acerca-De-Vertical.png");
                }
            }

            base.WillRotate(toInterfaceOrientation, duration);
        }

        #endregion

        #region M�todos

        /// <summary>
        /// M�todo as�ncrono que se encarga de realizar todo el proceso de sincronizar los datos con el servidor
        /// para obtener toda la informaci�n de la base de datos (No carga imagenes ni sincroniza pedidos creados),
        /// para tener la versi�n m�s actualizada de la base de datos del ERP.
        /// </summary>
        /// <returns>Retorna un mensaje como resultado del proceso.</returns>
        private Task<string> SincronizacionDatos(string usuario, List<string> creates, List<string> inserts)
        {
            return Task.Run(() =>
            {
                try
                {
                    // Se obtiene el UDID para ser enviado al servidor para validar si el dispositivo
                    // se encuentra ya configurado en el administrador.
                    NSUuid identifier = UIDevice.CurrentDevice.IdentifierForVendor;
                    string uuid = identifier.AsString();

                    // Se crea la referencia a la conexi�n con el servidor para manejar el proceso, enviando
                    // el nombre de usuario como referencia para el administrador.
                    conexion con = new conexion(uuid, AppDelegate.Servidor, usuario);

                    // Iniciamos una instancia de conexi�n con la base de datos para actualizar su informaci�n.
                    AppDelegate.cnx = new Connection(System.IO.Path.Combine(AppDelegate.RutaSD, AppDelegate.BaseDatos));

                    // Variable que tendr� el mensaje de retorno del proceso.
                    string error = string.Empty;

                    InvokeOnMainThread(() => { loadingOverlay.UpdateMessage("Generando Datos..."); });

                    // Se obtiene las sentencias de creaci�n y llenado de la base de datos.
                    if (creates != null && inserts != null)
                    {
                        if (!con.ObtenerSentenciasServidor(ref error, creates, inserts))
                        {
                            AppDelegate.cnx.Close();
                            InvokeOnMainThread(() => { loadingOverlay.Hide(); });

                            return error;
                        }
                    }
                    else
                    {
                        if (!con.ObtenerSentenciasServidor(ref error))
                        {
                            AppDelegate.cnx.Close();
                            InvokeOnMainThread(() => { loadingOverlay.Hide(); });

                            return error;
                        }
                    }

                    InvokeOnMainThread(() => { loadingOverlay.UpdateMessage("Preparando Sincronizaci�n..."); });

                    // Se crea o actualiza la estructura de toda la base de datos.
                    if (con.CrearEstructuraDinamica(AppDelegate.RutaSD, ref error, AppDelegate.cnx))
                    {
                        AppDelegate.cnx.Close();
                    }
                    else
                    {
                        AppDelegate.cnx.Close();
                        InvokeOnMainThread(() => { loadingOverlay.Hide(); });
                        return error;
                    }

                    InvokeOnMainThread(() => { loadingOverlay.UpdateMessage("Obteniendo Datos..."); });

                    // Se descarga el archivo comprimido de las sentencias.
                    if (con.continuarInsertar && !con.DescargarArchivoSincro(AppDelegate.RutaSD, ref error))
                    {
                        InvokeOnMainThread(() => { loadingOverlay.Hide(); });
                        return error;
                    }

                    InvokeOnMainThread(() => { loadingOverlay.UpdateMessage("Procesando Datos..."); });

                    // Se descomprime el archivo descargado en una carpeta temporal.
                    if (con.continuarInsertar && !con.DescomprimirArchivoSincro(AppDelegate.RutaSD, ref error))
                    {
                        InvokeOnMainThread(() => { loadingOverlay.Hide(); });
                        return error;
                    }

                    InvokeOnMainThread(() => { loadingOverlay.UpdateMessage("Actualizando Informaci�n..."); });

                    AppDelegate.cnx.Open();

                    // Se insertan todos los datos obtenidos desde el servidor en la base de datos.
                    if (con.continuarInsertar && con.InsertDatosDinamico(AppDelegate.RutaSD, ref error, AppDelegate.cnx))
                    {
                        AppDelegate.cnx.Close();
                        InvokeOnMainThread(() => { loadingOverlay.Hide(); });

                        if (!string.IsNullOrEmpty(con.atrWebSoporte))
                        {
                            AppDelegate.webSoporte = con.atrWebSoporte;

                            if (!AppDelegate.webSoporte.Contains("http://"))
                            {
                                AppDelegate.webSoporte = "http://" + AppDelegate.webSoporte;
                            }

                            AppDelegate.SetConfigVar(AppDelegate.webSoporte, "websoporte");
                        }

                        AppDelegate.CambiarDescuento = con.atrCambiarDescuento;

                        AppDelegate.SetConfigVar(AppDelegate.CambiarDescuento, "cambiardescuento");

                        return "Sincronizaci�n realizada correctamente.";
                    }
                    else
                    {

                        AppDelegate.cnx.Close();
                        InvokeOnMainThread(() => { loadingOverlay.Hide(); });

                        return error;
                    }
                }
                catch (Exception ex)
                {
                    InvokeOnMainThread(() => { loadingOverlay.Hide(); });

                    return "Ocurri� un error al crear la base de datos, int�ntelo nuevamente.";
                }
            });
        }

        /// <summary>
        /// M�todo as�ncrono que se encarga de realizar todo el proceso de sincronizar las im�genes con el servidor
        /// para obtener las im�genes de los art�culos (No carga datos ni sincroniza pedidos creados).
        /// </summary>
        /// <returns>Retorna un mensaje como resultado del proceso.</returns>
        private Task<string> SincronizarImagenes(string usuario)
        {
            return Task.Run(() =>
            {
                try
                {
                    // Se obtiene el UDID para ser enviado al servidor para validar si el dispositivo
                    // se encuentra ya configurado en el administrador.
                    NSUuid identifier = UIDevice.CurrentDevice.IdentifierForVendor;
                    string uuid = identifier.AsString();

                    // Se crea la referencia a la conexi�n con el servidor para manejar el proceso, enviando
                    // el nombre de usuario como referencia para el administrador.
                    conexion con = new conexion(uuid, AppDelegate.Servidor, usuario);

                    // Variable que tendr� el mensaje de retorno del proceso.
                    string error = string.Empty;

                    InvokeOnMainThread(() => { loadingOverlay.UpdateMessage("Obteniendo Datos..."); });

                    // Se descarga el archivo comprimido de las imagenes.
                    if (!con.DescargarArchivoFotos(AppDelegate.RutaSD, ref error))
                    {
                        InvokeOnMainThread(() => { loadingOverlay.Hide(); });
                        return error;
                    }

                    InvokeOnMainThread(() => { loadingOverlay.UpdateMessage("Procesando Datos..."); });

                    // Se descomprime el archivo descargado en una carpeta temporal.
                    if (!con.DescomprimirArchivoFotos(AppDelegate.RutaSD, ref error))
                    {
                        InvokeOnMainThread(() => { loadingOverlay.Hide(); });
                        return error;
                    }

                    InvokeOnMainThread(() => { loadingOverlay.UpdateMessage("Guardando Im�genes..."); });

                    // Se almacenan apropiadamente las imagenes para ser utilizadas por la aplicaci�n.
                    if (con.CopiarArchivoFotos(AppDelegate.RutaSD, ref error))
                    {
                        InvokeOnMainThread(() => { loadingOverlay.Hide(); });
                        return "Sincronizaci�n realizada correctamente.";
                    }
                    else
                    {
                        InvokeOnMainThread(() => { loadingOverlay.Hide(); });
                        return error;
                    }
                }
                catch
                {
                    InvokeOnMainThread(() => { loadingOverlay.Hide(); });
                    return "Ocurri� un error al descargar las im�genes, int�ntelo nuevamente.";
                }
            });

        }

        /// <summary>
        /// M�todo que configura el campo de texto txt_DatoCompaniaDefault para ser usado como un controlador de selecci�n m�ltiple.
        /// Se crea un controlador de selecci�n m�ltiple que se mostrar� en pantalla una vez que el campo de texto 
        /// sea seleccionado y cuando la seleccion termine se le asignar� el valor elegido al campo de texto.
        /// </summary>
        private void configurarPicker()
        {
            // Se inicializa el modelo a utilizar por el campo de texto.
            PickerModel model = new PickerModel(companias);

            // Evento de cambio de valor del selector.
            model.PickerChanged += (sender, e) =>
            {
                PickerChangedEventArgs obj = (PickerChangedEventArgs)e;

                // Se asigna la compa�ia seleccionada.
                CIA = obj.SelectedValue;
            };

            // Se inicializa el selector m�ltiple a utilizar por el campo de texto.
            UIPickerView picker = new UIPickerView();
            picker.BackgroundColor = UIColor.White;
            picker.TintColor = UIColor.White;
            picker.ShowSelectionIndicator = true;
            picker.Model = model;

            // Se crea una barra para colocar el bot�n de listo en el teclado que abre el control txt_DatoCompania.
            UIToolbar toolbar = new UIToolbar(new RectangleF(0.0f, 0.0f, (float)this.View.Frame.Size.Width, 44.0f));
            toolbar.TintColor = UIColor.White;
            toolbar.BarStyle = UIBarStyle.Black;
            toolbar.Translucent = true;

            // Se crea un bot�n para usarse como T�tulo de la barra y se deshabilita para que no tenga funcionalidad.
            UIBarButtonItem TitleButton;
            TitleButton = new UIBarButtonItem("Seleccione una compa��a:", UIBarButtonItemStyle.Plain, (s, e) => { });
            TitleButton.Enabled = false;

            // Se crean los botones de la barra y se asignan los Eventos de los mismos.
            toolbar.Items = new UIBarButtonItem[]{
                TitleButton,
                new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                new UIBarButtonItem(UIBarButtonSystemItem.Done, delegate {
                    // Se asigna al campo de texto la compa��a seleccionada por el usuario y se quita del Foco de atenci�n.
                    txt_DatoCompaniaDefault.Text = CIA;
                    txt_DatoCompaniaDefault.ResignFirstResponder();
                    
                    // Se asigna y guarda la nueva compa��a general seleccionada por el usuario.
                    AppDelegate.Compania = CIA;

                    AppDelegate.SetConfigVar(AppDelegate.Compania, "compania");

                    // Carga el simbolo monetario de la compa�ia guardada en la Configuraci�n.
                    var compania = Compania.Obtener(AppDelegate.Compania);

                    if (compania != null)
                    {
                        GestorUtilitario.SimboloMonetario = compania.SimboloMonetarioFunc;
                    }

                    // Dado que se cambi� la compa��a seleccionada se reinician los dados cargados en otras pantallas.
                    ClientesView.clientesCargados.Clear();
                    ArticulosVista.articulosCargados.Clear();
                    ArticulosVista.articulosCompania = "";

                    // Revisar que compa��a general y consecutivo este en la informaci�n actualizada.
                    obtenerDatosGenerales();
                })
            };

            // Se le asigna al campo de texto el control de selecci�n m�ltiple en lugar de usar un teclado.
            txt_DatoCompaniaDefault.InputView = picker;

            // Se le asigna al campo de texto la barra a mostrar sobre el teclado.
            txt_DatoCompaniaDefault.InputAccessoryView = toolbar;
        }
        
        /// <summary>
        /// M�todo que sirve para refrescar en pantalla la informaci�n de datos generales a mostrar.
        /// Se usa tambien para validar que la compa��a general seleccionada est� entre la lista de compa��as disponibles.
        /// </summary>
        private void obtenerDatosGenerales()
        {
            companias = new List<string>();
            companias.Add("");

            //consecutivos = new List<string>();
            //consecutivos.Add("");

            // Se agrega el nombre de las compa��as disponibles.
            companias.AddRange(Compania.Obtener().ConvertAll(x => x.Codigo));

            // Se verifica que la compa��a general est� en la lista.
            if (companias.Contains(AppDelegate.Compania))
            {
                // Si est� se asigna como la opci�n seleccionada en el control.

                txt_DatoCompaniaDefault.Text = AppDelegate.Compania;
                CIA = AppDelegate.Compania;
            }
            else
            {
                // Si no se encuentra en la lista se reinicia la variable compa��a general y consecutivo y las variables en NSUserDefaults.

                AppDelegate.Compania = "";

                AppDelegate.SetConfigVar(AppDelegate.Compania, "compania");

                AppDelegate.Consecutivo = "";

                AppDelegate.SetConfigVar(AppDelegate.Compania, "consecutivo");
            }

            swt_Guardar.On = AppDelegate.AlmacenarContrasena;

            lbl_DatoServidor.Text = AppDelegate.Servidor;

            // Se llama a configurar el campo de texto txt_DatoCompaniaDefault para usarlo como selecci�n m�ltiple.
            configurarPicker();

            int pedidos = Pedido.ObtenerCantidadPendientes();

            if (pedidos == 0)
            {
                lbl_DatoEstadoPedidos.Hidden = false;

                btn_SincronizarPedidos.Hidden = true;
            }
            else
            {
                lbl_DatoEstadoPedidos.Hidden = true;

                btn_SincronizarPedidos.Hidden = false;
                btn_SincronizarPedidos.SetTitle(pedidos + " pendientes.",UIControlState.Normal);
            }

            this.ReloadInputViews();
        }

        public void InicializarSincronizacion(List<string> creates, List<string> inserts)
        {
            InvokeOnMainThread(async () =>
            {
                // Determine the correct size to start the overlay (depending on device orientation)
                var bounds = UIScreen.MainScreen.Bounds; // portrait bounds
                if (UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft || UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight)
                {
                    bounds.Size = new SizeF((float)bounds.Size.Width, (float)bounds.Size.Height);
                }
                // show the loading overlay on the UI thread using the correct orientation sizing
                loadingOverlay = new LoadingOverlay((RectangleF)bounds);

                ParentViewController.ParentViewController.Add(loadingOverlay);

                // Se muestra el indicador de actividad con la red.
                UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;

                // Se crea un ID para la tarea que se ejecutar�, esto permite que el OS de oportunidad de tener una
                // tarea en el background por un tiempo de m�ximo 10 minutos antes de terminar la aplicacion por si mismo. 
                nint taskID = UIApplication.SharedApplication.BeginBackgroundTask(() =>
                {
                    // Evento en caso de que el timepo restante de la tarea est� a punto de terminar, se debe hacer 
                    // una limpieza de las variables usadas o terminar la tarea para evitar una terminaci�n
                    // total de la aplicaci�n por el OS.

                    double timeRemaining = UIApplication.SharedApplication.BackgroundTimeRemaining;

                    if (timeRemaining < 20)
                    {
                        Toast.MakeToast(ParentViewController.View, "La tarea de sincronizacion va a expirar", null, UIColor.Black, true, enumDuracionToast.Mediano);
                    }
                });

                // Se llama al m�todo que se encarga de realizar la sincronizaci�n.
                string result = await AppDelegate.SincronizarPedidos();

                // Se llama al m�todo que se encarga de realizar la sincronizaci�n.
                result = await SincronizacionDatos(GestorDatos.NombreUsuario, creates, inserts);

                // Se oculta el indicador de actividad con la red.
                UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;

                // Si la sincronizaci�n termin� exitosamente.
                if (result.Equals("Sincronizaci�n realizada correctamente."))
                {
                    bool usuarioAutenticado = true;

                    try
                    {
                        // Se vuelven a asignar los valores de las variables globales que trabajan con la base de datos.

                        // Se crea una nueva conexi�n con la base de datos recien sincronizada.
                        AppDelegate.cnx = new Connection(System.IO.Path.Combine(AppDelegate.RutaSD, AppDelegate.BaseDatos));

                        // Se asigna la conexi�n con la referencia en el Gestor de datos.
                        GestorDatos.cnx = AppDelegate.cnx;

                        // Se vuelve a cargar todos los datos requeridos por la aplicaci�n.
                        GlobalUI.Rutas = Ruta.ObtenerRutas();
                        GlobalUI.CargaParametrosX(false);
                        GlobalUI.ValidarConfiguracion();

                        // Revisar que compa��a general y consecutivo este en la informaci�n actualizada.
                        obtenerDatosGenerales();
                    }
                    catch
                    {
                        // Indicar al usuario que necesita sincronizar nuevamente.
                        Toast.MakeToast(ParentViewController.View, "No se pudo establecer conexi�n con la BD, sincronice nuevamente la aplicaci�n.", null, UIColor.Black, true, enumDuracionToast.Mediano);
                    }

                    // Se verifica si el usuario actual sigue estando en los datos actualizados.
                    if (!GestorSeguridad.AutenticarUsuario(GestorDatos.NombreUsuario, GestorDatos.ContrasenaUsuario))
                    {
                        result += " Parece que su usuario no es v�lido seg�n los datos actualizados, por favor inicie sesi�n nuevamente.";

                        usuarioAutenticado = false;
                    }

                    UIAlertView alert = new UIAlertView("Proceso finalizado", result, null, "OK", null);

                    // Evento click del bot�n de Aceptar.
                    alert.Clicked += (s2, b2) =>
                    {
                        ClientesView.clientesCargados = new List<Cliente>();
                        ArticulosVista.articulosCargados = new List<Articulo>();

                        if (!usuarioAutenticado)
                        {
                            // Se reinicia la variable de contrase�a y se devuelve al usuario a la pantalla de Iniciar sesi�n.

                            AppDelegate.SetConfigVar("", "contrasena");

                            // Limpia los clientes cargados.
                            ClientesView.clientesCargados.Clear();
                            ClientesView.clientesCompania = string.Empty;

                            // Limpia los art�culos cargados.
                            ArticulosVista.articulosCargados.Clear();
                            ArticulosVista.articulosCompania = string.Empty;

                            // Limpiar la lista de compa��as gestionadas.
                            Compania.Gestionadas = new List<Compania>();

                            if (GlobalUI.ClienteActual != null)
                            {
                                // Limpia y reinicia los datos del pedido global.
                                PedidosCollection.FacturarPedido = false;
                                GestorPedido.PedidosCollection = new PedidosCollection();
                                Pedido.BorrarXMLPedido(GlobalUI.ClienteActual.Codigo);

                                // Limpia el carrito de compras.
                                GestorPedido.carrito = null;

                                // Limpia al cliente del pedido.
                                GlobalUI.ClienteActual = null;
                                GlobalUI.RutaActual = null;
                                GlobalUI.RutasActuales = null;

                                // Asigna que no se est� creando una orden.
                                PedidosView.crearOrden = false;
                            }

                            ParentViewController.DismissViewController(true, null);
                        }

                    };
                    alert.Show();
                }
                else
                {
                    // Se notifica de alg�n error que ocurri� con la sincronizaci�n.
                    new UIAlertView("Sincronizaci�n detenida", result, null, "OK", null).Show();
                }

                // Se termina el proceso que se inici� con el ID establecido, para evitar que el OS temine la aplicaci�n.
                UIApplication.SharedApplication.EndBackgroundTask(taskID);
            });
        }

        /// <summary>
        /// M�todo que sirve para mostrar en pantalla una ventana PopUp con la lista de clientes encontrados.
        /// </summary>
        public void CrearPopUpParciales()
        {
            // Determine the correct size to start the overlay (depending on device orientation)
            var bounds = UIScreen.MainScreen.Bounds;
            var Orientacion = UIApplication.SharedApplication.StatusBarOrientation;
            if (Orientacion == UIInterfaceOrientation.LandscapeLeft || Orientacion == UIInterfaceOrientation.LandscapeRight)
            {
                bounds.Size = new SizeF((float)bounds.Size.Width, (float)bounds.Size.Height);
            }

            // Crear vista que se va a mostrar en la vista PopUp, adem�s crear y configurar los controles que se le agregar�n.

            // Dimensiones de la ventana del PopUp.
            float VPX = 0, VPY = 0, VPAncho = 0, VPAlto = 0;
            // Se manda a obtener las dimensiones que tendr�a la ventana principal del PopUp.
            PopUpView.ObtenerDimensiones((RectangleF)bounds, 0.9f, ref VPX, ref VPY, ref VPAncho, ref VPAlto, Orientacion);

            // Vista de pantalla Acerca De con los controles a mostrar.
            UIView VistaMostrar = new UIView(new RectangleF(VPX, VPY, VPAncho, VPAlto));

            // Controles a mostrar en pantalla Acerca De.

            // Controles a mostrar en pantalla Resultados Clientes.
            UITableView tbv_TablaParciales;

            tbv_TablaParciales = new UITableView()
            {
                Source = new TableSource(this, "ParcialCell"),
                Editing = false,
                Bounces = false,
                AutoresizingMask = UIViewAutoresizing.FlexibleDimensions
            };
            tbv_TablaParciales.Frame = new RectangleF(0, 0, VPAncho, VPAlto);
            tbv_TablaParciales.Layer.CornerRadius = 10f;
            tbv_TablaParciales.Layer.MasksToBounds = true;
            
            // Se agregan los controles en el orden que deben ser mostrados.
            VistaMostrar.Add(tbv_TablaParciales);

            // Se manda a crear la vista PopUp con los parametros que se desean para esta vista.
            ventanaCargasParciales = new PopUpView((RectangleF)bounds, Orientacion, VistaMostrar, UIColor.FromRGB(225, 106, 12), UIColor.White, UIColor.Black);

            // Se agrega al controlador de la ventana principal para mostrarlo.
            ParentViewController.ParentViewController.Add(ventanaCargasParciales);
        }

        #endregion

        #region View lifecycle

        /// <summary>
        /// M�todo para liberaci�n de memoria de la pantalla cuando se cierra.
        /// </summary>
        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        /// <summary>
        /// M�todo que se ejecuta al crear la pantalla, se ejecuta una �nica vez a menos que la pantalla se remueva del stack.
        /// </summary>
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Inicializar las variables o asignar los datos que se utilizan en la pantalla.

            cargasParciales = new List<string>() {
                "Art�culos",
                "Clientes",
                "Consecutivos",
                "Existencias",
                "Par�metros Globales",
                "Pendientes Cobro",
                "Precios",
                "Paquetes y Reglas",
                "Usuarios y Privilegios"
            };

            // Se le asigna el Evento de cambi� valor al controlador swt_Guardar.
            swt_Guardar.ValueChanged += delegate
            {
                // Se asigna y guarda el nuevo valor de Almacenar contrase�a seleccionado por el usuario.

                AppDelegate.AlmacenarContrasena = swt_Guardar.On;

                AppDelegate.SetConfigVar(AppDelegate.AlmacenarContrasena, "almacenarcontrasena");

                // Guardar o limpiar la variable de Configuraci�n de contrase�a.
                if (AppDelegate.AlmacenarContrasena)
                {
                    AppDelegate.SetConfigVar(GestorDatos.ContrasenaUsuario, "contrasena");
                }
                else
                {
                    AppDelegate.SetConfigVar("", "contrasena");
                }
            };
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a aparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            // Cargar toda la informaci�n en pantalla.
            obtenerDatosGenerales();
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla apareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            // Se inicia el proceso de intentar sincronizar pedidos pendientes.
            AppDelegate.IniciarSincronizacionPedidos(this.ParentViewController.ParentViewController);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a desaparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla desapareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
	}
}
