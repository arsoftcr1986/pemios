// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Softland.iOS.Pem
{
	[Register ("OpcionesView")]
	partial class OpcionesView
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_Acercade { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_borrarBD { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_CerrarSesion { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_IdDispositivo { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_Servidor { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_Sincronizar { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_SincronizarPedidos { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_Soporte { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_CompaniaDefault { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoEstadoPedidos { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoServidor { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_EstadoPedidos { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_IdDispositivo { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Recordar { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Separador { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Servidor { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UISwitch swt_Guardar { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITabBarItem tabOpciones { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField txt_DatoCompaniaDefault { get; set; }

		[Action ("btn_Acercade_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_Acercade_TouchUpInside (UIButton sender);

		[Action ("btn_borrarBD_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_borrarBD_TouchUpInside (UIButton sender);

		[Action ("btn_CerrarSesion_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_CerrarSesion_TouchUpInside (UIButton sender);

		[Action ("btn_IdDispositivo_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_IdDispositivo_TouchUpInside (UIButton sender);

		[Action ("btn_Servidor_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_Servidor_TouchUpInside (UIButton sender);

		[Action ("btn_Sincronizar_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_Sincronizar_TouchUpInside (UIButton sender);

		[Action ("btn_SincronizarPedidos_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_SincronizarPedidos_TouchUpInside (UIButton sender);

		[Action ("btn_Soporte_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_Soporte_TouchUpInside (UIButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (btn_Acercade != null) {
				btn_Acercade.Dispose ();
				btn_Acercade = null;
			}
			if (btn_borrarBD != null) {
				btn_borrarBD.Dispose ();
				btn_borrarBD = null;
			}
			if (btn_CerrarSesion != null) {
				btn_CerrarSesion.Dispose ();
				btn_CerrarSesion = null;
			}
			if (btn_IdDispositivo != null) {
				btn_IdDispositivo.Dispose ();
				btn_IdDispositivo = null;
			}
			if (btn_Servidor != null) {
				btn_Servidor.Dispose ();
				btn_Servidor = null;
			}
			if (btn_Sincronizar != null) {
				btn_Sincronizar.Dispose ();
				btn_Sincronizar = null;
			}
			if (btn_SincronizarPedidos != null) {
				btn_SincronizarPedidos.Dispose ();
				btn_SincronizarPedidos = null;
			}
			if (btn_Soporte != null) {
				btn_Soporte.Dispose ();
				btn_Soporte = null;
			}
			if (lbl_CompaniaDefault != null) {
				lbl_CompaniaDefault.Dispose ();
				lbl_CompaniaDefault = null;
			}
			if (lbl_DatoEstadoPedidos != null) {
				lbl_DatoEstadoPedidos.Dispose ();
				lbl_DatoEstadoPedidos = null;
			}
			if (lbl_DatoServidor != null) {
				lbl_DatoServidor.Dispose ();
				lbl_DatoServidor = null;
			}
			if (lbl_EstadoPedidos != null) {
				lbl_EstadoPedidos.Dispose ();
				lbl_EstadoPedidos = null;
			}
			if (lbl_IdDispositivo != null) {
				lbl_IdDispositivo.Dispose ();
				lbl_IdDispositivo = null;
			}
			if (lbl_Recordar != null) {
				lbl_Recordar.Dispose ();
				lbl_Recordar = null;
			}
			if (lbl_Separador != null) {
				lbl_Separador.Dispose ();
				lbl_Separador = null;
			}
			if (lbl_Servidor != null) {
				lbl_Servidor.Dispose ();
				lbl_Servidor = null;
			}
			if (swt_Guardar != null) {
				swt_Guardar.Dispose ();
				swt_Guardar = null;
			}
			if (tabOpciones != null) {
				tabOpciones.Dispose ();
				tabOpciones = null;
			}
			if (txt_DatoCompaniaDefault != null) {
				txt_DatoCompaniaDefault.Dispose ();
				txt_DatoCompaniaDefault = null;
			}
		}
	}
}
