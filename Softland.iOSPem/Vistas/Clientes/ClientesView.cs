using System;
using Foundation;
using UIKit;
using System.Collections.Generic;
using System.CodeDom.Compiler;

using Pedidos.Core;
using System.Drawing;
using BI.Shared;
using System.Threading.Tasks;
using BI.iOS;
using System.Data.SQLite;

namespace Softland.iOS.Pem
{
	partial class ClientesView : UIViewController
    {

        #region Propiedades y Contructor de instancia

        /// <summary>
        /// Referencia a la vista de carga que es mostrada al realizar la carga de clientes.
        /// </summary>
        public static LoadingOverlay loadingOverlay;

        /// <summary>
        /// Referencia a DetalleClienteTableView para tener acceso a sus variables antes de moverse a esa nueva pantalla.
        /// </summary>
        public DetalleClienteTableView detalleClienteView;

        /// <summary>
        /// Lista de clientes que almancenar� todos los clientes disponibles en la compa��a cargada seg�n clientesCompania.
        /// </summary>
        public static List<Cliente> clientesCargados;

        /// <summary>
        /// Lista de clientes a cargar en el controlador de tabla, se llena con las b�squedas r�pida y avanzada.
        /// </summary>
        public List<Cliente> clientes;

        /// <summary>
        /// Controlador para generar la barra de b�squeda r�pida.
        /// </summary>
        private UISearchBar searchBar;

        /// <summary>
        /// Lista de compa��as disponibles para realizar las b�squedas.
        /// </summary>
        public List<string> companias;

        /// <summary>
        /// Variable que almacena el valor de la compa�ia seleccionada para la b�squeda.
        /// </summary>
        public string CIA = "";

        /// <summary>
        /// Variable que almacena la compan�a de los clientes actualmente cargados.
        /// </summary>
        public static string clientesCompania = string.Empty;

        /// <summary>
        /// Costructor por defecto.
        /// </summary>
        /// <param name="handle"></param>
		public ClientesView (IntPtr handle) : base (handle)
		{
		}

        #endregion

        #region Tabla

        /// <summary>
        /// Clase que se utilizar� para manejar la informaci�n de la tabla de selecci�n de cliente.
        /// </summary>
        public class TableSource : UITableViewSource
        {

            #region Propiedades y Contructor de instancia

            /// <summary>
            /// Variable para identificar las celdas que utiliza este TableSource.
            /// </summary>
            string cellIdentifier = "TableCell";

            /// <summary>
            /// Referencia a la pantalla para poder acceder a sus propiedades desde otra clase. 
            /// </summary>
            ClientesView VistaOpciones;

            /// <summary>
            /// Constructor por defecto. 
            /// </summary>
            /// <param name="padre"></param>
            public TableSource(ClientesView padre)
            {
                VistaOpciones = padre;
            }

            #endregion

            #region Eventos

            // Evento requerido por la clase, retorna la cantidad de registros en la fuente de datos.
            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return VistaOpciones.clientes.Count;
            }

            /// <summary>
            /// Evento que se ejecuta al cargar las celdas de la tabla seg�n vayan siendo mostradas.
            /// </summary>
            /// <param name="tableView"></param>
            /// <param name="indexPath">�ndice de la celda por cargar.</param>
            /// <returns>Retorna la celda cargada.</returns>
            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                // Busca la celda creada para ahorrar memoria.
                UITableViewCell cell = tableView.DequeueReusableCell(cellIdentifier);

                // Si no hay celdas por reutilizar, se crea una nueva, en este caso se usa una celda predefinida Subtitle.
                if (cell == null)
                {
                    cell = new UITableViewCell(UITableViewCellStyle.Subtitle, cellIdentifier);

                    cell.TextLabel.Font = UIFont.FromName("Helvetica-Bold", 14f);
                    cell.TextLabel.AdjustsFontSizeToFitWidth = false;

                    cell.DetailTextLabel.Font = UIFont.FromName("Helvetica", 12f);
                    cell.DetailTextLabel.AdjustsFontSizeToFitWidth = true;
                    cell.DetailTextLabel.MinimumFontSize = 8;
                }

                // Se actualiza la informaci�n de la celda.
                cell.TextLabel.Text = VistaOpciones.clientes[indexPath.Row].Nombre;
                cell.DetailTextLabel.Text = "C�digo: " + VistaOpciones.clientes[indexPath.Row].Codigo;

                return cell;
            }

            /// <summary>
            /// Evento que se ejecuta al seleccionar una celda de la tabla.
            /// </summary>
            /// <param name="tableView"></param>
            /// <param name="indexPath">�ndice de la celda seleccionada.</param>
            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                // Se referencia al Storyboard del app.
                var myStoryboard = AppDelegate.Storyboard;

                // Se instancia un ViewController con el ID del Storyboard y su clase respectiva.
                VistaOpciones.detalleClienteView = myStoryboard.InstantiateViewController("DetalleClienteTableView") as DetalleClienteTableView;

                // Se asignan los valores que se desean pasar a la nueva pantalla.
                VistaOpciones.detalleClienteView.cliente = VistaOpciones.clientes[indexPath.Row];

                // Se navega a la pantalla instanciada agregandola al stack.
                VistaOpciones.NavigationController.PushViewController(VistaOpciones.detalleClienteView, true);

                // Se quita el resaltado que se le da a una celda al ser seleccionada (simula comportamiento de iOS).
                tableView.DeselectRow(indexPath, true);
            }

            #endregion
        }

        #endregion
        
        #region Eventos
        
        /// <summary>
        /// Evento de click del bot�n btn_Buscar.
        /// Se usa para que el usuario pueda elegir la compa��a por la cual filtrar clientes, despliega una pantalla para elegir 
        /// una compa��a.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void btn_Buscar_TouchUpInside(object sender, EventArgs e)
        {
            // Controlador que mostrar� el selector de fecha en pantalla.
            var modalPicker = new ModalPickerViewController(ModalPickerType.Custom, "Seleccione una compa��a:", this)
            {
                HeaderBackgroundColor = UIColor.DarkGray,
                InternalBackgroundColor = UIColor.White,
                HeaderTextColor = UIColor.White,
                TransitioningDelegate = new ModalPickerTransitionDelegate(),
                ModalPresentationStyle = UIModalPresentationStyle.Custom
            };

            // Se crea el Model para el selector.
            modalPicker.PickerView.Model = new PickerModel(companias);

            // Evento en que la pantalla del controlador desaparece.
            modalPicker.OnModalPickerDismissed += (s, ea) =>
            {
                var index = modalPicker.PickerView.SelectedRowInComponent(0);
                CIA = companias[(int)index];
                
                this.QuickSearch();
            };

            // Mostrar la pantalla.
            await PresentViewControllerAsync(modalPicker, true);
        }

        #endregion

        #region M�todos

        /// <summary>
        /// M�todo que se ejecuta al iniciar una b�squeda r�pida por alg�n cliente.
        /// Busca por clientes seg�n la descripci�n de estos y el dato ingresado en el campo de texto.
        /// </summary>
        void QuickSearch()
        {
            if (CIA.Equals("Todos"))
            {
                clientesCompania = "";

                btn_Buscar.SetTitle(" Todas las compa��as", UIControlState.Normal);
            }
            else
            {
                clientesCompania = CIA;

                btn_Buscar.SetTitle(" Compa��a " + CIA, UIControlState.Normal);
            }

            clientes.Clear();

            // Se obtienen los clientes encontrados, y se agregan a la lista.
            clientes.AddRange(clientesCargados.FindAll(
                x =>
                        
                (
                    x.Nombre.ToUpper().Contains(searchBar.Text.ToUpper()) ||
                    x.Codigo.ToUpper().Contains(searchBar.Text.ToUpper())
                )
                    &&
                (
                    x.Compania.ToUpper().Equals(clientesCompania) ||
                    string.IsNullOrEmpty(clientesCompania)
                )
            ));

            TableView.ReloadData();

            // Se quita el Foco de atenci�n del searchBar.
            searchBar.ResignFirstResponder();
        }

        /// <summary>
        /// M�todo que se ejecuta al abrir la secci�n clientes.
        /// Carga todos los clientes disponibles en la base de datos.
        /// </summary>
        public async void CargaInicialClientes()
        {
            // Se referencia a la conexi�n con la base de datos para asegurar que est� activa.
            GestorDatos.cnx = AppDelegate.cnx;

            // Determine the correct size to start the overlay (depending on device orientation)
            var bounds = UIScreen.MainScreen.Bounds; // portrait bounds
            if (UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft || UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight)
            {
                bounds.Size = new SizeF((float)bounds.Size.Width, (float)bounds.Size.Height);
            }

            // show the loading overlay on the UI thread using the correct orientation sizing
            loadingOverlay = new LoadingOverlay((RectangleF)bounds);

            ParentViewController.ParentViewController.Add(loadingOverlay);

            loadingOverlay.UpdateMessage("Cargando todos los clientes...");

            await Task.Run(() =>
            {
                clientesCargados.Clear();

                // Se obtienen los clientes encontrados, y se agregan a la lista.
                clientesCargados.AddRange(Cliente.CargarClientes(new CriterioBusquedaCliente(CriterioCliente.Ninguno, "", false, false), ""));

                // Si no hay una compa��a general seleccionada se selecciona autom�ticamente por el usuario.
                if (string.IsNullOrEmpty(AppDelegate.Compania))
                {
                    AppDelegate.Compania = Compania.Obtener().ConvertAll(x => x.Codigo)[0];

                    AppDelegate.SetConfigVar(AppDelegate.Compania, "compania");
                }

                // Se establece la compa��a que se seleccion� para cargar clientes.
                CIA = AppDelegate.Compania;

                InvokeOnMainThread(() =>
                {
                    // Si no se pudo cargar clientes, se bloquea la b�squeda r�pida de lo contrario se habilita.
                    if (clientesCargados.Count > 0)
                    {
                        searchBar.Placeholder = "B�squeda r�pida de Clientes";

                        searchBar.UserInteractionEnabled = true;
                    }
                    else
                    {
                        searchBar.Placeholder = "No hay clientes disponibles";

                        searchBar.UserInteractionEnabled = false;
                    }

                    QuickSearch();

                    loadingOverlay.Hide();
                });
            });
        }

        #endregion

        #region View lifecycle

        /// <summary>
        /// M�todo para liberaci�n de memoria de la pantalla cuando se cierra.
        /// </summary>
        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        /// <summary>
        /// M�todo que se ejecuta al crear la pantalla, se ejecuta una �nica vez a menos que la pantalla se remueva del stack.
        /// </summary>
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Inicializar las variables o asignar los datos que se utilizan en la pantalla.

            clientesCargados = new List<Cliente>();

            clientes = new List<Cliente>();

            TableView.Source = new TableSource(this);
            TableView.Editing = false;

            // Se crea un UISearchBar para la b�squeda r�pida de art�culos.
            searchBar = new UISearchBar(new RectangleF(0, 0, (float)View.Frame.Width, 50));
            searchBar.SizeToFit();
            searchBar.AutocorrectionType = UITextAutocorrectionType.No;
            searchBar.AutocapitalizationType = UITextAutocapitalizationType.None;
            searchBar.EnablesReturnKeyAutomatically = false;

            // Se asigna el Evento de click en la barra.
            searchBar.SearchButtonClicked += (sender, e) =>
            {
                QuickSearch();
            };

            // Se crea una barra para colocar el bot�n de cancelar en el teclado que abre el control searchBar.
            UIToolbar toolbar = new UIToolbar(new RectangleF(0.0f, 0.0f, (float)this.View.Frame.Size.Width, 44.0f));
            toolbar.TintColor = UIColor.White;
            toolbar.BarStyle = UIBarStyle.Black;
            toolbar.Translucent = true;

            // Se crean los botones de la barra y se asignan los Eventos de los mismos.
            toolbar.Items = new UIBarButtonItem[]{
                new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                new UIBarButtonItem(UIBarButtonSystemItem.Cancel, delegate {
                    searchBar.ResignFirstResponder();
                })
            };

            // Se asgrega la barra para cancelar al teclado, y se asigna el searchBar como el componente del t�tulo.
            searchBar.InputAccessoryView = toolbar;
            this.NavigationItem.TitleView = searchBar;

            btn_Buscar.TouchUpInside += btn_Buscar_TouchUpInside;

            // Se selecciona como anaranjado el color de los items de la barra de pesta�as.
            this.TabBarController.TabBar.TintColor = UIColor.FromRGB(225, 106, 12);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a aparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            TableView.ReloadData();
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla apareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            
            // Se inicializa la lista de compa��as.
            companias = new List<string>();
            companias.Add("Todos");

            // Se obtiene la lista de compa��as de la base de datos.
            companias.AddRange(Compania.Obtener().ConvertAll(x => x.Codigo));

            if (companias.Count == 2)
            {
                btn_Buscar.Enabled = false;
            }

            // Cargar todos los clientes disponibles si a�n no han sido cargados.
            if (clientesCargados.Count == 0)
            {
                CargaInicialClientes();
            }

            //this.TabBarController.TabBar.TintColor = UIColor.FromRGB(225, 106, 12);

            // Se inicia el proceso de intentar sincronizar pedidos pendientes.
            AppDelegate.IniciarSincronizacionPedidos(this.ParentViewController.ParentViewController);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a desaparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla desapareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
	}
}
