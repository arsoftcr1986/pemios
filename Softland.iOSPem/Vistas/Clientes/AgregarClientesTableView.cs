using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;

namespace Softland.iOS.Pem
{
    /// <summary>
    /// Esta Clase no tiene ninguna verdadera implementaci�n por el momento.
    /// </summary>
	partial class AgregarClientesTableView : UITableViewController
    {

        #region Propiedades y Contructor de instancia

        /// <summary>
        /// Constructor por defecto.
        /// </summary>
        /// <param name="handle"></param>
		public AgregarClientesTableView (IntPtr handle) : base (handle)
		{
		}

        #endregion

        #region M�todos

        /// <summary>
        /// M�todo que sirve para guardar la informaci�n del nuevo cliente.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void GuardarCliente(UIButton sender, EventArgs args)
        {
            this.NavigationController.PopViewController(true);
        }

        #endregion

        #region View lifecycle

        /// <summary>
        /// M�todo para liberaci�n de memoria de la pantalla cuando se cierra.
        /// </summary>
        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        /// <summary>
        /// M�todo que se ejecuta al crear la pantalla, se ejecuta una �nica vez a menos que la pantalla se remueva del stack.
        /// </summary>
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Inicializar las variables o asignar los datos que se utilizan en la pantalla.

            btn_Guardar.TouchUpInside += (sender, e) =>
            {
                GuardarCliente((UIButton)sender, e);
            };

            txt_Codigo.ShouldReturn += (textField) =>
            {
                textField.ResignFirstResponder();

                return true;
            };

            txt_Nombre.ShouldReturn += (textField) =>
            {
                textField.ResignFirstResponder();

                return true;
            };
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a aparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla apareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a desaparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla desapareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
	}
}
