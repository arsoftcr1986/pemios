// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Softland.iOS.Pem
{
	[Register ("DetalleClienteTableView")]
	partial class DetalleClienteTableView
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_crearPedido { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_Direccion { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_Mapa { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_VerDocumentos { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_CedulaJuridica { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Codigo { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Compania { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoCedulaJuridica { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoCodigo { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoCompania { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoMontoTotal { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoNombre { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoSaldoTotal { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoTelefono { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Direccion { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Monto { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Nombre { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Saldo { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Separador { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Telefono { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UISegmentedControl seg_TipoMoneda { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView TableView { get; set; }

		[Action ("btn_crearPedido_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_crearPedido_TouchUpInside (UIButton sender);

		[Action ("btn_Direccion_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_Direccion_TouchUpInside (UIButton sender);

		[Action ("btn_Mapa_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_Mapa_TouchUpInside (UIButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (btn_crearPedido != null) {
				btn_crearPedido.Dispose ();
				btn_crearPedido = null;
			}
			if (btn_Direccion != null) {
				btn_Direccion.Dispose ();
				btn_Direccion = null;
			}
			if (btn_Mapa != null) {
				btn_Mapa.Dispose ();
				btn_Mapa = null;
			}
			if (btn_VerDocumentos != null) {
				btn_VerDocumentos.Dispose ();
				btn_VerDocumentos = null;
			}
			if (lbl_CedulaJuridica != null) {
				lbl_CedulaJuridica.Dispose ();
				lbl_CedulaJuridica = null;
			}
			if (lbl_Codigo != null) {
				lbl_Codigo.Dispose ();
				lbl_Codigo = null;
			}
			if (lbl_Compania != null) {
				lbl_Compania.Dispose ();
				lbl_Compania = null;
			}
			if (lbl_DatoCedulaJuridica != null) {
				lbl_DatoCedulaJuridica.Dispose ();
				lbl_DatoCedulaJuridica = null;
			}
			if (lbl_DatoCodigo != null) {
				lbl_DatoCodigo.Dispose ();
				lbl_DatoCodigo = null;
			}
			if (lbl_DatoCompania != null) {
				lbl_DatoCompania.Dispose ();
				lbl_DatoCompania = null;
			}
			if (lbl_DatoMontoTotal != null) {
				lbl_DatoMontoTotal.Dispose ();
				lbl_DatoMontoTotal = null;
			}
			if (lbl_DatoNombre != null) {
				lbl_DatoNombre.Dispose ();
				lbl_DatoNombre = null;
			}
			if (lbl_DatoSaldoTotal != null) {
				lbl_DatoSaldoTotal.Dispose ();
				lbl_DatoSaldoTotal = null;
			}
			if (lbl_DatoTelefono != null) {
				lbl_DatoTelefono.Dispose ();
				lbl_DatoTelefono = null;
			}
			if (lbl_Direccion != null) {
				lbl_Direccion.Dispose ();
				lbl_Direccion = null;
			}
			if (lbl_Monto != null) {
				lbl_Monto.Dispose ();
				lbl_Monto = null;
			}
			if (lbl_Nombre != null) {
				lbl_Nombre.Dispose ();
				lbl_Nombre = null;
			}
			if (lbl_Saldo != null) {
				lbl_Saldo.Dispose ();
				lbl_Saldo = null;
			}
			if (lbl_Separador != null) {
				lbl_Separador.Dispose ();
				lbl_Separador = null;
			}
			if (lbl_Telefono != null) {
				lbl_Telefono.Dispose ();
				lbl_Telefono = null;
			}
			if (seg_TipoMoneda != null) {
				seg_TipoMoneda.Dispose ();
				seg_TipoMoneda = null;
			}
			if (TableView != null) {
				TableView.Dispose ();
				TableView = null;
			}
		}
	}
}
