using BI.Shared;
using Foundation;
using Pedidos.Core;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Drawing;
using UIKit;

namespace Softland.iOS.Pem
{
	partial class DocumentosClienteTableView : UITableViewController
    {

        #region Propiedades y Contructor de instancia

        /// <summary>
        /// Referencia al cliente del que se est� viendo su detalle.
        /// </summary>
        public Cliente cliente;

        /// <summary>
        /// Variable que almacena el tipo de moneda a mostrar en el detalle del estado del cliente.
        /// </summary>
        public TipoMoneda moneda = TipoMoneda.LOCAL;

        /// <summary>
        /// Lista de Documentos contables a cargar en el controlador de tabla.
        /// </summary>
        public List<DocumentoContable> Documentos;

        /// <summary>
        /// Variable que almacena el S�mbolo Monetario de la compa��a del cliente actual.
        /// </summary>
        public string simboloMonetarioCompania = string.Empty;

        /// <summary>
        /// Costructor por defecto.
        /// </summary>
		public DocumentosClienteTableView (IntPtr handle) : base (handle)
		{
		}

        #endregion

        #region Tabla y Celdas

        /// <summary>
        /// Clase que se utilizar� para manejar la informaci�n de la tabla de documentos del cliente.
        /// </summary>
        public class TableSource : UITableViewSource
        {
           
            #region Propiedades y Contructor de instancia

            /// <summary>
            /// Variable para identificar las celdas que utiliza este TableSource.
            /// </summary>
            string cellIdentifier = "Documento";

            /// <summary>
            /// Referencia a la pantalla para poder acceder a sus propiedades desde otra clase. 
            /// </summary>
            DocumentosClienteTableView VistaPadre;

            /// <summary>
            /// Constructor por defecto. 
            /// </summary>
            /// <param name="padre"></param>
            public TableSource(DocumentosClienteTableView padre)
            {
                VistaPadre = padre;
            }

            #endregion

            #region Eventos

            // Evento requerido por la clase, retorna la cantidad de registros en la fuente de datos.
            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return VistaPadre.Documentos.Count;
            }

            /// <summary>
            /// Evento que se ejecuta al cargar las celdas de la tabla seg�n vayan siendo mostradas.
            /// </summary>
            /// <param name="tableView"></param>
            /// <param name="indexPath">�ndice de la celda por cargar.</param>
            /// <returns>Retorna la celda cargada.</returns>
            public override UITableViewCell GetCell(UITableView tableView, Foundation.NSIndexPath indexPath)
            {
                // Busca la celda creada para ahorrar memoria.
                var cell = tableView.DequeueReusableCell(cellIdentifier) as CustomDocumentoCell;

                // Si no hay celdas por reutilizar, se crea una nueva, en este caso se usa una celda CustomDocumentoCell.
                if (cell == null)
                {
                    cell = new CustomDocumentoCell((NSString)cellIdentifier);
                    cell.VistaPadre = VistaPadre;
                }

                // Se actualiza la informaci�n de la celda seg�n el tipo de moneda.
                cell.documento = indexPath.Row;
                if (VistaPadre.moneda == TipoMoneda.LOCAL)
                {
                    cell.UpdateCell(VistaPadre.Documentos[indexPath.Row].Numero,
                            VistaPadre.Documentos[indexPath.Row].FechaVencimiento.ToString("MM/dd/yyyy"),
                            VistaPadre.Documentos[indexPath.Row].SaldoDocLocal,
                            VistaPadre.Documentos[indexPath.Row].MontoDocLocal);
                }
                else
                {
                    cell.UpdateCell(VistaPadre.Documentos[indexPath.Row].Numero,
                            VistaPadre.Documentos[indexPath.Row].FechaVencimiento.ToString("MM/dd/yyyy"),
                            VistaPadre.Documentos[indexPath.Row].SaldoDocDolar,
                            VistaPadre.Documentos[indexPath.Row].MontoDocDolar);
                }

                return cell;
            }

            /// <summary>
            /// Evento que se ejecuta al seleccionar una celda de la tabla.
            /// </summary>
            /// <param name="tableView"></param>
            /// <param name="indexPath">�ndice de la celda seleccionada.</param>
            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                // Se quita el resaltado que se le da a una celda al ser seleccionada (simula comportamiento de iOS).
                tableView.DeselectRow(indexPath, true);
            }

            #endregion
        }

        /// <summary>
        /// Clase que se utilizar� para manejar la informaci�n de la celda de la tabla de documentos del cliente.
        /// </summary>
        public class CustomDocumentoCell : UITableViewCell
        {

            #region Propiedades y Contructor de instancia

            /// <summary>
            /// Controladores para mostrar el nombre y la cantidad en el carrito del art�culo.
            /// </summary>
            UILabel lbl_Codigo, lbl_Fecha, lbl_Saldo, lbl_Monto;

            /// <summary>
            /// �ndice del documento en la lista de documentos.
            /// </summary>
            public int documento;

            /// <summary>
            /// Referencia a la pantalla para poder acceder a sus propiedades desde otra clase.
            /// </summary>
            public DocumentosClienteTableView VistaPadre;

            /// <summary>
            /// Constructor por defecto.
            /// </summary>
            /// <param name="cellId"></param>
            public CustomDocumentoCell(NSString cellId)
                : base(UITableViewCellStyle.Default, cellId)
            {
                SelectionStyle = UITableViewCellSelectionStyle.Blue;
                ContentView.BackgroundColor = UIColor.Clear;

                lbl_Codigo = new UILabel()
                {
                    BackgroundColor = UIColor.Clear,
                    TextAlignment = UITextAlignment.Left,
                    AdjustsFontSizeToFitWidth = true,
                    MinimumFontSize = 8
                };

                lbl_Fecha = new UILabel()
                {
                    BackgroundColor = UIColor.Clear,
                    TextAlignment = UITextAlignment.Right,
                    AdjustsFontSizeToFitWidth = true,
                    MinimumFontSize = 8
                };

                lbl_Saldo = new UILabel()
                {
                    TextAlignment = UITextAlignment.Left,
                    BackgroundColor = UIColor.Clear,
                    AdjustsFontSizeToFitWidth = true,
                    MinimumFontSize = 8
                };

                lbl_Monto = new UILabel()
                {
                    TextAlignment = UITextAlignment.Right,
                    BackgroundColor = UIColor.Clear,
                    AdjustsFontSizeToFitWidth = true,
                    MinimumFontSize = 8
                };

                // Se agregan los controladores a la celda.
                ContentView.AddSubviews(new UIView[] { lbl_Codigo, lbl_Fecha, lbl_Saldo, lbl_Monto });
            }

            #endregion

            #region Eventos

            /// <summary>
            /// Evento que permite acomodar las dimensiones y posici�n de las Subvistas en la celda.
            /// </summary>
            public override void LayoutSubviews()
            {
                base.LayoutSubviews();

                lbl_Codigo.Frame = new RectangleF(10, 10, ((float)ContentView.Bounds.Width / 2) - 5, 21);
                lbl_Fecha.Frame = new RectangleF(((float)ContentView.Bounds.Width / 2) + 15, 10, ((float)ContentView.Bounds.Width / 2) - 25, 21);
                lbl_Saldo.Frame = new RectangleF(10, 40, ((float)ContentView.Bounds.Width / 2) - 10, 21);
                lbl_Monto.Frame = new RectangleF(((float)ContentView.Bounds.Width / 2) + 15, 40, ((float)ContentView.Bounds.Width / 2) - 25, 21);
            }

            #endregion

            #region M�todos

            /// <summary>
            /// M�todo que sirve para actualizar los datos de la celda, llamado al cargar cada celda.
            /// </summary>
            /// <param name="codigo">C�digo del documento.</param>
            /// <param name="fecha">Fecha de vencimiento del documento.</param>
            /// <param name="saldo">Saldo del documento.</param>
            /// <param name="monto">Monto del documento.</param>
            public void UpdateCell(string codigo, string fecha, decimal saldo, decimal monto)
            {
                lbl_Codigo.Text = "C�digo: " + codigo;
                lbl_Fecha.Text = "Vence: " + fecha;

                if (VistaPadre.moneda == TipoMoneda.LOCAL)
                {
                    lbl_Saldo.Text = "Saldo: " + GestorUtilitario.FormatNumero(saldo, VistaPadre.simboloMonetarioCompania);
                    lbl_Monto.Text = "Monto: " + GestorUtilitario.FormatNumero(monto, VistaPadre.simboloMonetarioCompania);
                }
                else
                {
                    lbl_Saldo.Text = "Saldo: " + GestorUtilitario.FormatNumeroDolar(saldo);
                    lbl_Monto.Text = "Monto: " + GestorUtilitario.FormatNumeroDolar(monto);
                }
            }

            #endregion
        }

        #endregion

        #region View lifecycle

        /// <summary>
        /// M�todo para liberaci�n de memoria de la pantalla cuando se cierra.
        /// </summary>
        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        /// <summary>
        /// M�todo que se ejecuta al crear la pantalla, se ejecuta una �nica vez a menos que la pantalla se remueva del stack.
        /// </summary>
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Inicializar las variables o asignar los datos que se utilizan en la pantalla.

            TableView.Source = new TableSource(this);
            TableView.Editing = false;
            TableView.ReloadData();

            // Agregar en la barra de Navegaci�n un bot�n para cambiar la moneda de los documentos.
            this.NavigationItem.SetRightBarButtonItem(
                new UIBarButtonItem(UIImage.FromFile("btn_calcularmoneda.png")
                , UIBarButtonItemStyle.Plain
                , (sender, args) =>
                {
                    // Evento de click del bot�n de cambiar moneda.

                    if (moneda == TipoMoneda.LOCAL)
                        moneda = TipoMoneda.DOLAR;
                    else
                        moneda = TipoMoneda.LOCAL;

                    TableView.ReloadData();
                })
            , true);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla apareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            // Se inicia el proceso de intentar sincronizar pedidos pendientes.
            AppDelegate.IniciarSincronizacionPedidos(this.ParentViewController.ParentViewController);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a desaparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla desapareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
	}
}
