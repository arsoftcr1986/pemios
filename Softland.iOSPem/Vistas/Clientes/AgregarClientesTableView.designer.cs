// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Softland.iOS.Pem
{
	[Register ("AgregarClientesTableView")]
	partial class AgregarClientesTableView
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_Guardar { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Codigo { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Nombre { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField txt_Codigo { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField txt_Nombre { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (btn_Guardar != null) {
				btn_Guardar.Dispose ();
				btn_Guardar = null;
			}
			if (lbl_Codigo != null) {
				lbl_Codigo.Dispose ();
				lbl_Codigo = null;
			}
			if (lbl_Nombre != null) {
				lbl_Nombre.Dispose ();
				lbl_Nombre = null;
			}
			if (txt_Codigo != null) {
				txt_Codigo.Dispose ();
				txt_Codigo = null;
			}
			if (txt_Nombre != null) {
				txt_Nombre.Dispose ();
				txt_Nombre = null;
			}
		}
	}
}
