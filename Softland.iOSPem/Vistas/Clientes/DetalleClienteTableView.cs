using Foundation;
using System;
using System.Linq;
using System.CodeDom.Compiler;
using UIKit;

using Pedidos.Core;
using BI.Shared;
using System.Collections.Generic;

namespace Softland.iOS.Pem
{
	partial class DetalleClienteTableView : UITableViewController
    {

        #region Propiedades y Contructor de instancia

        /// <summary>
        /// Referencia al cliente del que se est� viendo su detalle.
        /// </summary>
        public Cliente cliente;

        /// <summary>
        /// Variable que almacena el tipo de moneda a mostrar en el detalle del estado del cliente.
        /// </summary>
        public TipoMoneda moneda = TipoMoneda.LOCAL;

        /// <summary>
        /// Lista de Documentos contables a cargar en el controlador de tabla de la pantalla DocumentosClienteTableView.
        /// </summary>
        public List<DocumentoContable> Documentos;

        /// <summary>
        /// Variable que almacena el S�mbolo Monetario de la compa��a del cliente actual.
        /// </summary>
        public string simboloMonetarioCompania = string.Empty;

        /// <summary>
        /// Costructor por defecto.
        /// </summary>
		public DetalleClienteTableView (IntPtr handle) : base (handle)
		{
		}

        #endregion

        #region Eventos

        /// <summary>
        /// Evento que se ejecuta al iniciar la navegaci�n de una pantalla a otra usando un Segue.
        /// Se usa para inicializar y pasar variables a la clase destino, solo se usa cuando se configur�
        /// una navegaci�n usando el dise�ador por medio de un Segue.
        /// </summary>
        /// <param name="segue"></param>
        /// <param name="sender"></param>
        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);

            // Se obtiene una referencia de la clase destino especificando como castearla.
            var DocumentosCliente = segue.DestinationViewController as DocumentosClienteTableView;

            // Si la referencia no es nula, se asignan las variables necesarias.
            if (DocumentosCliente != null)
            {
                DocumentosCliente.cliente = cliente;
                DocumentosCliente.moneda = moneda;
                DocumentosCliente.Documentos = Documentos;
                DocumentosCliente.simboloMonetarioCompania = simboloMonetarioCompania;
            }
        }

        /// <summary>
        /// Evento de click del bot�n btn_Mapa.
        /// Se usa para mostrar al usuario en la secci�n de Mapa la geolocalizaci�n del cliente.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_Mapa_TouchUpInside(UIButton sender)
        {
            // Se obtiene la referencia al TabView.
            var tabController = (UITabBarController)ParentViewController.ParentViewController;

            try
            {
                // Se selecciona que se muestre el tab de Mapa.
                tabController.SelectedIndex = 3;

                // Se instancia el Mapa usando la vista seleccionada desde el controlador de la vista hijo del TabView.
                var Mapa = (MapaView)tabController.SelectedViewController.ChildViewControllers[0];

                // Si la referencia no es nula, se asignan las variables necesarias.
                if (Mapa != null)
                {
                    Mapa.clientes.Clear();
                    Mapa.clientes.Add(cliente);
                }
            }
            catch
            {
                tabController.SelectedIndex = 0;

                Toast.MakeToast(ParentViewController.View, "Parece haber un problema con la informaci�n de geolocalizaci�n del cliente.", null, UIColor.Black, true, enumDuracionToast.Mediano);
            }
        }

        /// <summary>
        /// Evento de click del bot�n btn_Direccion.
        /// Se usa para mostrar al usuario la direccion del cliente.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_Direccion_TouchUpInside(UIButton sender)
        {
            Toast.MakeToast(ParentViewController.View, cliente.Direccion, null, UIColor.Black, true, enumDuracionToast.MuyLargo);
        }

        /// <summary>
        /// Evento que se ejecuta al cambiar el valor de la secci�n seleccionada del control seg_Moneda.
        /// Se usa para actualizar los datos de monto total y saldo total del cliente seg�n la moneda seleccionada.
        /// </summary>
        /// <param name="sender">Quien llamo al evento.</param>
        /// <param name="e">Parametos del evento.</param>
        private void seg_Moneda_ValueChanged(object sender, EventArgs e)
        {
            // Se obtiene la moneda seleccionada.
            moneda = (int)seg_TipoMoneda.SelectedSegment == 0 ? TipoMoneda.LOCAL : TipoMoneda.DOLAR;
        
            // Se actualiza la informaci�n en pantalla realizando una suma en los documentos pendientes del cliente.
            if (moneda == TipoMoneda.LOCAL)
            {
                lbl_DatoMontoTotal.Text = GestorUtilitario.FormatNumero(Documentos.Sum(x => x.MontoDocLocal), simboloMonetarioCompania);

                lbl_DatoSaldoTotal.Text = GestorUtilitario.FormatNumero(Documentos.Sum(x => x.SaldoDocLocal), simboloMonetarioCompania);
            }
            else
            {
                lbl_DatoMontoTotal.Text = GestorUtilitario.FormatNumeroDolar(Documentos.Sum(x => x.MontoDocDolar));

                lbl_DatoSaldoTotal.Text = GestorUtilitario.FormatNumeroDolar(Documentos.Sum(x => x.SaldoDocDolar));
            }
        }

        /// <summary>
        /// Evento de click del bot�n btn_crearPedido.
        /// Se usa para iniciar la gesti�n de un pedido para el cliente mostrado en la vista, se validar� si se est�
        /// gestionando un pedido actualmente para confirmar con el usuario si desea terminarlo y se validar�
        /// luego si se puede crear un pedido para el cliente seleccionado permitiendo navegar a la secci�n de pedidos
        /// para que el usuario continue la gesti�n.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_crearPedido_TouchUpInside(UIButton sender)
        {
            // Si no se est� gestionando ning�n pedido, validar que se pueda crear el pedido para el cliente seleccionado.
            if (GlobalUI.ClienteActual == null && GestorPedido.PedidosCollection.Gestionados.Count == 0)
            {
                ValidacionIniciarPedido();
            }
            else
            {
                // Si se est� gestionando un pedido consultar con el usuario si desea terminar la gesti�n del pedido actual
                // y crear uno nuevo para el nuevo cliente seleccionado, de ser as�, cancelar el pedido actual y validar 
                // que se pueda crear el pedido para el cliente seleccionado.

                UIAlertView alert = new UIAlertView("Advertencia", "�Ya se est� gestionando un pedido al cliente " + 
                    GlobalUI.ClienteActual.Nombre+ 
                    ", desea terminar la gesti�n de ese pedido y crear un nuevo pedido para el cliente seleccionado?", null,
                    "Cancelar", "Aceptar");

                // Evento click del bot�n de Aceptar.
                alert.Clicked += (s, b) =>
                {
                    if (b.ButtonIndex == 1)
                    {
                        InvokeOnMainThread(() =>
                        {
                            // Limpia y reinicia los datos del pedido global.
                            PedidosCollection.FacturarPedido = false;
                            GestorPedido.PedidosCollection = new PedidosCollection();
                            Pedido.BorrarXMLPedido(GlobalUI.ClienteActual.Codigo);

                            // Limpia el carrito de compras.
                            GestorPedido.carrito = null;

                            // Limpia al cliente del pedido.
                            GlobalUI.ClienteActual = null;
                            GlobalUI.RutaActual = null;
                            GlobalUI.RutasActuales = null;

                            ValidacionIniciarPedido();
                        });
                    }

                };
                alert.Show();
            }
        }

        #endregion

        #region M�todos

        /// <summary>
        /// M�todo que sirve para validar si se puede crear un pedido al cliente seleccionado e iniciar el proceso.
        /// </summary>
        /// <returns>Retorna si el m�todo complet� o no su ejecuci�n.</returns>
        public void ValidacionIniciarPedido()
        {
            // Mensaje de error a mostrar al usuario.
            string error = "";

            // Variable para validar si ocurri� un error al cargar la informaci�n.
            bool errorValidacion = false;

            // Se cargan la bodega actual y bodegas del cliente (Rutas = Bodegas).
            GlobalUI.RutaActual = Ruta.ObtenerRuta(GlobalUI.Rutas, cliente.Zona);
            GlobalUI.RutasActuales = Ruta.ObtenerRutas(GlobalUI.Rutas, cliente.Zona);

            try
            {
                // Se asigna el cliente seleccionado a la referencia global de GlobalUI.ClienteActual y su c�digo de bodega.
                GlobalUI.ClienteActual = cliente;
                GlobalUI.ClienteActual.Zona = GlobalUI.RutaActual.Codigo;
            }
            catch (Exception ex)
            {
                error = "Error en la selecci�n del cliente. " + ex.Message;

                errorValidacion = true;
            }

            try
            {
                // Se carga la informaci�n de las compa��as del cliente.
                GlobalUI.ClienteActual.ObtenerClientesCia();

                // Se llama a cargar los datos requeridos para iniciar el pedido.
                if (!InicializarPedido())
                {
                    error = "Error al inicializar el pedido con el cliente " + GlobalUI.NameCliente;

                    errorValidacion = true;
                }

            }
            catch (Exception ex)
            {
                error = "Error cargando datos del cliente en compa��as. " + ex.Message;

                errorValidacion = true;
            }

            // Si ocurri� alg�n problema al cargar toda la informaci�n del cliente seleccionado se notifica al usuario.
            if (errorValidacion)
            {
                // Se desactiva el bot�n dado que al cliente no se le pueden realizar pedidos.
                btn_crearPedido.Enabled = false;

                // Dado que el cliente seleccionado no es valido para crear un pedido se reinician las variables utilizadas.
                GlobalUI.RutaActual = null;
                GlobalUI.RutasActuales = null;
                GlobalUI.ClienteActual = null;
                GestorPedido.PedidosCollection = new PedidosCollection();

                Toast.MakeToast(ParentViewController.View, "Parece haber un problema con la informaci�n del cliente, " + error, null, UIColor.Black, true, enumDuracionToast.Mediano);
            }
            else
            {
                // Se obtiene la referencia al TabView.
                var tabController = (UITabBarController)ParentViewController.ParentViewController;

                try
                {
                    // Se selecciona que se muestre el tab de Pedidos.
                    tabController.SelectedIndex = 2;

                    // Se instancia el Mapa usando la vista seleccionada desde el controlador de la vista hijo del TabView.
                    var Pedidos = (PedidosView)tabController.SelectedViewController.ChildViewControllers[0];

                    // Si la referencia no es nula, se asignan las variables necesarias.
                    if (Pedidos != null)
                    {
                        Pedidos.pedidoDesdeClientes = true;
                    }
                }
                catch(Exception ex)
                {
                    tabController.SelectedIndex = 0;

                    Toast.MakeToast(ParentViewController.View, "Parece haber un problema al cargar la pantalla de Pedidos, int�ntelo nuevamente.", null, UIColor.Black, true, enumDuracionToast.Mediano);
                }
            }
        }

        /// <summary>
        /// M�todo que llama a cargar los datos de configuraci�n para el cliente y el pedido que se va a realizar.
        /// </summary>
        /// <returns>Retorna si el m�todo complet� o no su ejecuci�n.</returns>
        public bool InicializarPedido()
        {
            try
            {
                ConfigDocCia config = CargarConfiguracionCliente(GlobalUI.ClienteActual.Compania);
                GlobalUI.ClienteActual.ClienteCompania[0].ObtenerDireccionesEntrega();
                config.ClienteCia = GlobalUI.ClienteActual.ClienteCompania[0];
                GestorPedido.PedidosCollection.CargarConfiguracionVenta(GlobalUI.ClienteActual.Compania, config);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// M�todo que carga la configuraci�n del cliente seleccionado para el pedido seg�n la compa��a deseada.
        /// </summary>
        /// <param name="compania">Compa��a por la que buscar la configuraci�n.</param>
        /// <returns>Retorna la configuraci�n del cliente.</returns>
        private ConfigDocCia CargarConfiguracionCliente(string compania)
        {
            return GestorPedido.PedidosCollection.CargarConfiguracionCliente(GlobalUI.ClienteActual, compania);
        }

        #endregion

        #region View lifecycle

        /// <summary>
        /// M�todo para liberaci�n de memoria de la pantalla cuando se cierra.
        /// </summary>
        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        /// <summary>
        /// M�todo que se ejecuta al crear la pantalla, se ejecuta una �nica vez a menos que la pantalla se remueva del stack.
        /// </summary>
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Inicializar las variables o asignar los datos que se utilizan en la pantalla.

            // Se obtiene una lista de las facturas pendientes del cliente.
            var facs = Factura.ObtenerFacturasPendientesCobro(cliente.Compania, cliente.Codigo, cliente.Zona);
            // Se convierten las facturas en Documentos contables para sacar sus datos.
            Documentos = facs.ConvertAll<DocumentoContable>(x => (DocumentoContable)x);

            // Se muestra en pantalla la informaci�n del cliente.

            lbl_DatoNombre.Text = cliente.Nombre;

            lbl_DatoCodigo.Text = cliente.Codigo;

            lbl_DatoCompania.Text = cliente.Compania;

            try
            {
                cliente.ObtenerClientesCia();

                lbl_DatoTelefono.Text = !string.IsNullOrWhiteSpace(cliente.ClienteCompania[0].Telefono) ? cliente.ClienteCompania[0].Telefono : "No disponible.";

                lbl_DatoCedulaJuridica.Text = !cliente.ClienteCompania[0].Contribuyente.Contains("ND") ? cliente.ClienteCompania[0].Contribuyente : "No disponible.";
            }
            catch
            {
                lbl_DatoTelefono.Text = "No disponible.";

                lbl_DatoCedulaJuridica.Text = "No disponible.";
            }

            var compania = Compania.Obtener(cliente.Compania);

            if (compania != null)
            {
                simboloMonetarioCompania = compania.SimboloMonetarioFunc;
            }

            moneda = (int)seg_TipoMoneda.SelectedSegment == 0 ? TipoMoneda.LOCAL : TipoMoneda.DOLAR;

            if (moneda == TipoMoneda.LOCAL)
            {
                lbl_DatoMontoTotal.Text = GestorUtilitario.FormatNumero(Documentos.Sum(x => x.MontoDocLocal), simboloMonetarioCompania);

                lbl_DatoSaldoTotal.Text = GestorUtilitario.FormatNumero(Documentos.Sum(x => x.SaldoDocLocal), simboloMonetarioCompania);
            }
            else
            {
                lbl_DatoMontoTotal.Text = GestorUtilitario.FormatNumeroDolar(Documentos.Sum(x => x.MontoDocDolar));

                lbl_DatoSaldoTotal.Text = GestorUtilitario.FormatNumeroDolar(Documentos.Sum(x => x.SaldoDocDolar));
            }

            // Se asigna el Evento de cambio de valor en el selector de moneda.
            seg_TipoMoneda.ValueChanged += seg_Moneda_ValueChanged;
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla apareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            // Se inicia el proceso de intentar sincronizar pedidos pendientes.
            AppDelegate.IniciarSincronizacionPedidos(this.ParentViewController.ParentViewController);
        }
        
        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a desaparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla desapareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
	}
}
