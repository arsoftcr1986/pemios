using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using Pedidos.Core;
using BI.Shared;

using Foundation;
using UIKit;
using System.Drawing;
using System.IO;

namespace Softland.iOS.Pem
{
	partial class DetalleArticuloPedidoTableView : UITableViewController
    {

        #region Propiedades y Contructor de instancia

        /// <summary>
        /// Variable que almacena la posici�n en la lista de carrito de compras del art�culo.
        /// </summary>
        public int Indice;

        /// <summary>
        /// Referencia al art�culo del que se est� viendo su detalle.
        /// </summary>
        public Articulo artTemp;

        /// <summary>
        /// Referencia a PedidosView para tener acceso a sus variables antes de devolverse a esa pantalla.
        /// </summary>
        public PedidosView pedidosView;

        /// <summary>
        /// Variable que almacena si hay bodegas disponibles para el art�culo.
        /// </summary>
        public bool BodegaDisponible = false;

        /// <summary>
        /// Variable que almacena si hay bodegas disponibles para el art�culo.
        /// </summary>
        public bool BonBodegaDisponible = false;

        /// <summary>
        /// Variable que almacena la bodega seleccionada para el art�culo.
        /// </summary>
        public string CodBodega = "";

        /// <summary>
        /// Lista de las bodegas en las que se encuentra el art�culo.
        /// </summary>
        public List<string> Bodegas;

        /// <summary>
        /// Variable que almacena la bodega seleccionada para el art�culo bonificado.
        /// </summary>
        public string BonBodega = "";

        /// <summary>
        /// Lista de las bodegas en las que se encuentra el art�culo bonificado.
        /// </summary>
        public List<string> BonBodegas;

        /// <summary>
        /// Variable que almacena si la moneda del cliente es el dolar.
        /// </summary>
        public bool dolar;

        /// <summary>
        /// Variable que almacena si la compa��a del pedido usa o no localizaciones.
        /// </summary>
        public bool UsaLocalizacion;

        /// <summary>
        /// Variable que almacena la localizaci�n seleccionada para el art�culo.
        /// </summary>
        public string Localizacion = "";

        /// <summary>
        /// Lista de las localizaciones que posee el art�culo.
        /// </summary>
        public List<string> Localizaciones;

        /// <summary>
        /// Variable que almacena la localizaci�n seleccionada para el art�culo bonificado.
        /// </summary>
        public string BonLocalizacion = "";

        /// <summary>
        /// Lista de las localizaciones que posee el art�culo bonificado.
        /// </summary>
        public List<string> BonLocalizaciones;

        /// <summary>
        /// Variable que almacena la cantidad de almacen del art�culo.
        /// </summary>
        public decimal cantidadArticuloAlm = 0;

        /// <summary>
        /// Variable que almacena la cantidad de detalle del art�culo.
        /// </summary>
        public decimal cantidadArticuloDet = 0;

        /// <summary>
        /// Variable que almacena el valor del descuento del art�culo.
        /// </summary>
        public decimal descuentoArticulo = 0;

        /// <summary>
        /// Variable que almacena el S�mbolo Monetario de la compa��a del cliente actual del pedido.
        /// </summary>
        public string simboloMonetarioPedido = string.Empty;

        /// <summary>
        /// Costructor por defecto.
        /// </summary>
        public DetalleArticuloPedidoTableView (IntPtr handle) : base(handle)
		{
		}

        #endregion

        #region Eventos

        /// <summary>
        /// Evento que se ejecuta al cambiar la orientaci�n del dispositivo.
        /// Se usa para reacomodar visualmente los controladores, en este caso por medio del m�todo de seg_Vistas.
        /// </summary>
        /// <param name="fromInterfaceOrientation"></param>
        public override void DidRotate(UIInterfaceOrientation fromInterfaceOrientation)
        {
            base.DidRotate(fromInterfaceOrientation);

            TableView.ReloadData();
        }

        /// <summary>
        /// Evento de click del bot�n btn_Bonificaciones.
        /// Se usa para mostrar al usuario la infomaci�n del art�culo bonificado si existe una linea bonificada.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_Bonificaciones_TouchUpInside(UIButton sender)
        {
            string mostrar;
            string cantidad;
            
            // Referencia al detalle del art�culo en el pedido.
            DetallePedido DetalleSeleccionado = GestorPedido.PedidosCollection.BuscarDetalle(artTemp);

            // Si el detalle tiene una linea bonificada, se arma un mensaje con la informaci�n del articulo bonificado
            // para mostrarlo al usuario.
            if (DetalleSeleccionado.LineaBonificada != null)
            {
                // Se obtiene el art�culo bonificado.
                Articulo articuloBonificado = DetalleSeleccionado.LineaBonificada.Articulo;

                mostrar = articuloBonificado.Codigo + " - " + articuloBonificado.Descripcion;
                if (mostrar.Length > 34)
                    mostrar = mostrar.Substring(0, 34);
                mostrar = mostrar + " - ";
                cantidad = " Cant. Bonifica: " + articuloBonificado.TipoEmpaqueAlmacen + ": " + GestorUtilitario.Formato(DetalleSeleccionado.LineaBonificada.UnidadesAlmacen) + " | " +
                           articuloBonificado.TipoEmpaqueDetalle + ": " + GestorUtilitario.Formato(DetalleSeleccionado.LineaBonificada.UnidadesDetalle);
                mostrar = mostrar + cantidad;

                Toast.MakeToast(ParentViewController.View, mostrar, null, UIColor.Black, true, enumDuracionToast.Mediano);
            }
            else
            {
                Toast.MakeToast(ParentViewController.View, "No hay art�culo bonificado.", null, UIColor.Black, true, enumDuracionToast.Mediano);
            }
        }

        /// <summary>
        /// Evento de cambio valor del campo de texto txt_Descuento.
        /// Se usa para validar que no se pueda ingresar una cifra mayor a 100 ni menor a 0, adem�s de que sean solo n�meros.
        /// </summary>
        private void txt_Descuento_ValueChanged()
        {
            try
            {
                if (Convert.ToDecimal(txt_Descuento.Text) > 100)
                {
                    txt_Descuento.Text = "100";
                }
                else
                {
                    if (Convert.ToDecimal(txt_Descuento.Text) < 0)
                    {
                        txt_Descuento.Text = "0";
                    }
                }
            }
            catch
            {
                // Si la conversi�n a int fall�, indica que se ingres� alg�n caracter no num�rico.
                if (txt_Descuento.Text.Length - 1 >= 0)
                {
                    txt_Descuento.Text = txt_Descuento.Text.Substring(0, txt_Descuento.Text.Length - 1);
                }
            }
        }

        /// <summary>
        /// Evento de cambio valor del campo de texto txt_CantidadAlmacen.
        /// Se usa para validar que se ingresen solo n�meros.
        /// </summary>
        private void txt_CantidadAlmacen_ValueChanged()
        {
            try
            {
                if (Convert.ToInt64(txt_CantidadAlmacen.Text) > 999999999999999999)
                {
                    txt_CantidadAlmacen.Text = "999999999999999999";
                }
                else
                {
                    if (Convert.ToInt64(txt_CantidadAlmacen.Text) < 0)
                    {
                        txt_CantidadAlmacen.Text = "0";
                    }
                }
            }
            catch
            {
                // Si la conversi�n a int fall�, indica que se ingres� alg�n caracter no num�rico.
                if (txt_CantidadAlmacen.Text.Length - 1 >= 0)
                {
                    txt_CantidadAlmacen.Text = txt_CantidadAlmacen.Text.Substring(0, txt_CantidadAlmacen.Text.Length - 1);
                }
            }
        }

        /// <summary>
        /// Evento de cambio valor del campo de texto txt_CantidadDetalle.
        /// Se usa para validar que se ingresen solo n�meros.
        /// </summary>
        private void txt_CantidadDetalle_ValueChanged()
        {
            try
            {
                if (Convert.ToInt64(txt_CantidadDetalle.Text) > 999999999999999999)
                {
                    txt_CantidadDetalle.Text = "999999999999999999";
                }
                else
                {
                    if (Convert.ToInt64(txt_CantidadDetalle.Text) < 0)
                    {
                        txt_CantidadDetalle.Text = "0";
                    }
                }
            }
            catch
            {
                // Si la conversi�n a int fall�, indica que se ingres� alg�n caracter no num�rico.
                if (txt_CantidadDetalle.Text.Length - 1 >= 0)
                {
                    txt_CantidadDetalle.Text = txt_CantidadDetalle.Text.Substring(0, txt_CantidadDetalle.Text.Length - 1);
                }
            }
        }

        #endregion
        
        #region Metodos

        /// <summary>
        /// M�todo que se encarga de obtener una imagen desde un directorio en el dispositivo y devolverla como objeto UIImage.
        /// </summary>
        /// <param name="compania">Compa��a del art�culo</param>
        /// <param name="codigo">C�digo del art�culo</param>
        /// <returns>Imagen en formato UIImage</returns>
        static UIImage FromFile(string compania, string codigo)
        {
            UIImage imagen;

            // Directorio de la app en el dispotivo, agregando el nombre de la compa��a para obtener la imagen espec�fica.
            string directorio = Path.Combine(AppDelegate.RutaSD, compania);

            // Si la imagen existe retornarla de lo contrario devolver la imagen b�sica de no disponible.
            if (File.Exists(Path.Combine(directorio, codigo + ".jpg")))
            {
                imagen = UIImage.FromFile(Path.Combine(directorio, codigo + ".jpg"));
            }
            else
            {
                imagen = UIImage.FromFile("imagen_no_disponible.jpg");
            }

            return imagen;
        }

        /// <summary>
        /// M�todo que configura el campo de texto txt_DatoBodega para ser usado como un controlador de selecci�n m�ltiple.
        /// Se crea un controlador de selecci�n m�ltiple que se mostrar� en pantalla una vez que el campo de texto 
        /// sea seleccionado y cuando la seleccion termine se le asignar� el valor elegido al campo de texto.
        /// </summary>
        private void configurarPickerBodega()
        {
            // Se inicializa el modelo a utilizar por el campo de texto.
            PickerModel model = new PickerModel(Bodegas);

            // Evento de cambio de valor del selector.
            model.PickerChanged += (sender, e) =>
            {
                PickerChangedEventArgs obj = (PickerChangedEventArgs)e;

                // Se asigna la bodega seleccionada.
                CodBodega = obj.SelectedValue;
            };

            // Se inicializa el selector m�ltiple a utilizar por el campo de texto.
            UIPickerView picker = new UIPickerView();
            picker.BackgroundColor = UIColor.White;
            picker.TintColor = UIColor.White;
            picker.ShowSelectionIndicator = true;
            picker.Model = model;

            // Se crea una barra para colocar el bot�n de listo en el teclado que abre el control txt_DatoCompania.
            UIToolbar toolbar = new UIToolbar(new RectangleF(0.0f, 0.0f, (float)this.View.Frame.Size.Width, 44.0f));
            toolbar.TintColor = UIColor.White;
            toolbar.BarStyle = UIBarStyle.Black;
            toolbar.Translucent = true;

            // Se crea un bot�n para usarse como T�tulo de la barra y se deshabilita para que no tenga funcionalidad.
            UIBarButtonItem TitleButton;
            TitleButton = new UIBarButtonItem("Seleccione una bodega:", UIBarButtonItemStyle.Plain, (s, e) => { });
            TitleButton.Enabled = false;

            // Se crean los botones de la barra y se asignan los Eventos de los mismos.
            toolbar.Items = new UIBarButtonItem[]{
                TitleButton,
                new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                new UIBarButtonItem(UIBarButtonSystemItem.Done, delegate {
                    // Se quita del Foco de atenci�n.
                    txt_DatoBodega.ResignFirstResponder();
                    
                    // Si la bodega seleccionada es vacio y hay bodegas disponibles para seleccionar, se le notifica 
                    // al usuario que seleccione una bodega para el art�culo, se marca el campo de texto en rojo
                    // y se oculta la opci�n de regresar.
                    //if (string.IsNullOrEmpty(Bodega) && BodegaDisponible == true)
                    //{
                    //    Toast.MakeToast(ParentViewController.View, "Hay bodegas disponibles, seleccione una bodega.", null, UIColor.Black, true, enumDuracionToast.Largo);

                    //    // Se marca el campo de texto de color rojo para indicar donde est� el error.
                    //    txt_DatoBodega.BackgroundColor = UIColor.Red;

                    //    // Se esconde el bot�n de Atr�s.
                    //    this.NavigationItem.SetHidesBackButton(true, false);
                    //}
                    //else
                    if (!string.IsNullOrEmpty(CodBodega))
                    {
                        // Si la bodega seleccionada es una de la lista.
                        // Se asigna al campo de texto la bodega seleccionada por el usuario.
                        txt_DatoBodega.Text = CodBodega;

                        // Se cargan los valores de existencia del art�culo.
                        artTemp.CargarExistenciaPedido(CodBodega);

                        // Se limpia la marca de color en el campo de texto si la tuviera.
                        txt_DatoBodega.BackgroundColor = UIColor.Clear;

                        if (UsaLocalizacion)
                        {
                            Localizaciones.Clear();
                            Localizaciones.Add("");
                            Localizaciones.AddRange(artTemp.Bodega.Obtenerlocalizaciones(artTemp));
                            
                            if (Localizaciones.Count > 1)
                            {
                                configurarPickerLocalizacion();

                                if (!string.IsNullOrEmpty(Localizacion) && Localizaciones.Contains(Localizacion))
                                {
                                    txt_DatoLocalizacion.Text = Localizacion;
                                }
                                else
                                {
                                    txt_DatoLocalizacion.Text = Localizaciones[1];
                                    Localizacion = Localizaciones[1];

                                    artTemp.Bodega.Localizacion = Localizacion;
                                }

                                txt_DatoLocalizacion.Enabled = true;
                            }
                            else
                            {
                                txt_DatoLocalizacion.Text = "No disponible.";
                                txt_DatoLocalizacion.Enabled = false;
                                Localizacion = "";
                            }
                        }

                        // Se muestra el bot�n de Atr�s.
                        this.NavigationItem.SetHidesBackButton(false, false);
                    }

                    RefrescarDatosDetalle();
                })
            };

            // Se le asigna al campo de texto el control de selecci�n m�ltiple en lugar de usar un teclado.
            txt_DatoBodega.InputView = picker;

            // Se le asigna al campo de texto la barra a mostrar sobre el teclado.
            txt_DatoBodega.InputAccessoryView = toolbar;
        }

        /// <summary>
        /// M�todo que configura el campo de texto txt_DatoBonBodega para ser usado como un controlador de selecci�n m�ltiple.
        /// Se crea un controlador de selecci�n m�ltiple que se mostrar� en pantalla una vez que el campo de texto 
        /// sea seleccionado y cuando la seleccion termine se le asignar� el valor elegido al campo de texto.
        /// </summary>
        private void configurarPickerBonBodega()
        {
            // Se inicializa el modelo a utilizar por el campo de texto.
            PickerModel model = new PickerModel(BonBodegas);

            // Evento de cambio de valor del selector.
            model.PickerChanged += (sender, e) =>
            {
                PickerChangedEventArgs obj = (PickerChangedEventArgs)e;

                // Se asigna la bodega seleccionada.
                BonBodega = obj.SelectedValue;
            };

            // Se inicializa el selector m�ltiple a utilizar por el campo de texto.
            UIPickerView picker = new UIPickerView();
            picker.BackgroundColor = UIColor.White;
            picker.TintColor = UIColor.White;
            picker.ShowSelectionIndicator = true;
            picker.Model = model;

            // Se crea una barra para colocar el bot�n de listo en el teclado que abre el control txt_DatoCompania.
            UIToolbar toolbar = new UIToolbar(new RectangleF(0.0f, 0.0f, (float)this.View.Frame.Size.Width, 44.0f));
            toolbar.TintColor = UIColor.White;
            toolbar.BarStyle = UIBarStyle.Black;
            toolbar.Translucent = true;

            // Se crea un bot�n para usarse como T�tulo de la barra y se deshabilita para que no tenga funcionalidad.
            UIBarButtonItem TitleButton;
            TitleButton = new UIBarButtonItem("Seleccione una bodega:", UIBarButtonItemStyle.Plain, (s, e) => { });
            TitleButton.Enabled = false;

            // Se crean los botones de la barra y se asignan los Eventos de los mismos.
            toolbar.Items = new UIBarButtonItem[]{
                TitleButton,
                new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                new UIBarButtonItem(UIBarButtonSystemItem.Done, delegate {
                    // Se quita del Foco de atenci�n.
                    txt_DatoBonBodega.ResignFirstResponder();
                    
                    // Si la bodega seleccionada es vacio y hay bodegas disponibles para seleccionar, se le notifica 
                    // al usuario que seleccione una bodega para el art�culo, se marca el campo de texto en rojo
                    // y se oculta la opci�n de regresar.
                    //if (string.IsNullOrEmpty(BonBodega) && BonBodegaDisponible == true)
                    //{
                    //    Toast.MakeToast(ParentViewController.View, "Hay bodegas disponibles, seleccione una bodega para el art�culo bonificado.", null, UIColor.Black, true, enumDuracionToast.Largo);

                    //    // Se marca el campo de texto de color rojo para indicar donde est� el error.
                    //    txt_DatoBonBodega.BackgroundColor = UIColor.Red;

                    //    // Se esconde el bot�n de Atr�s.
                    //    this.NavigationItem.SetHidesBackButton(true, false);
                    //}
                    //else
                    if (!string.IsNullOrEmpty(BonBodega))
                    {
                        // Si la bodega seleccionada es una de la lista.
                        // Se asigna al campo de texto la bodega seleccionada por el usuario.
                        txt_DatoBonBodega.Text = BonBodega;

                        DetallePedido DetalleSeleccionado = GestorPedido.PedidosCollection.BuscarDetalle(artTemp);
                        
                        // Se cargan los valores de existencia del art�culo.
                        DetalleSeleccionado.LineaBonificada.Articulo.CargarExistenciaPedido(BonBodega);

                        // Se limpia la marca de color en el campo de texto si la tuviera.
                        txt_DatoBonBodega.BackgroundColor = UIColor.Clear;

                        if (UsaLocalizacion)
                        {
                            BonLocalizaciones.Clear();
                            BonLocalizaciones.Add("");
                            BonLocalizaciones.AddRange(DetalleSeleccionado.LineaBonificada.Articulo.Bodega.Obtenerlocalizaciones(
                                DetalleSeleccionado.LineaBonificada.Articulo));

                            if (BonLocalizaciones.Count > 1)
                            {
                                configurarPickerBonLocalizacion();

                                if (!string.IsNullOrEmpty(BonLocalizacion) && BonLocalizaciones.Contains(BonLocalizacion))
                                {
                                    txt_DatoBonLocalizacion.Text = BonLocalizacion;
                                }
                                else
                                {
                                    txt_DatoBonLocalizacion.Text = BonLocalizaciones[1];
                                    BonLocalizacion = BonLocalizaciones[1];

                                    DetalleSeleccionado.LineaBonificada.Articulo.Bodega.Localizacion = BonLocalizacion;
                                }

                                txt_DatoBonLocalizacion.Enabled = true;
                            }
                            else
                            {
                                txt_DatoBonLocalizacion.Text = "No disponible.";
                                txt_DatoBonLocalizacion.Enabled = false;
                                BonLocalizacion = "";
                            }
                        }

                        // Se muestra el bot�n de Atr�s.
                        this.NavigationItem.SetHidesBackButton(false, false);
                    }

                    RefrescarDatosDetalle();
                })
            };

            // Se le asigna al campo de texto el control de selecci�n m�ltiple en lugar de usar un teclado.
            txt_DatoBonBodega.InputView = picker;

            // Se le asigna al campo de texto la barra a mostrar sobre el teclado.
            txt_DatoBonBodega.InputAccessoryView = toolbar;
        }

        /// <summary>
        /// M�todo que configura el campo de texto txt_DatoLocalizacion para ser usado como un controlador de selecci�n m�ltiple.
        /// Se crea un controlador de selecci�n m�ltiple que se mostrar� en pantalla una vez que el campo de texto 
        /// sea seleccionado y cuando la seleccion termine se le asignar� el valor elegido al campo de texto.
        /// </summary>
        private void configurarPickerLocalizacion()
        {
            // Se inicializa el modelo a utilizar por el campo de texto.
            PickerModel model = new PickerModel(Localizaciones);

            // Evento de cambio de valor del selector.
            model.PickerChanged += (sender, e) =>
            {
                PickerChangedEventArgs obj = (PickerChangedEventArgs)e;

                // Se asigna la bodega seleccionada.
                Localizacion = obj.SelectedValue;
            };

            // Se inicializa el selector m�ltiple a utilizar por el campo de texto.
            UIPickerView picker = new UIPickerView();
            picker.BackgroundColor = UIColor.White;
            picker.TintColor = UIColor.White;
            picker.ShowSelectionIndicator = true;
            picker.Model = model;

            // Se crea una barra para colocar el bot�n de listo en el teclado que abre el control txt_DatoCompania.
            UIToolbar toolbar = new UIToolbar(new RectangleF(0.0f, 0.0f, (float)this.View.Frame.Size.Width, 44.0f));
            toolbar.TintColor = UIColor.White;
            toolbar.BarStyle = UIBarStyle.Black;
            toolbar.Translucent = true;

            // Se crea un bot�n para usarse como T�tulo de la barra y se deshabilita para que no tenga funcionalidad.
            UIBarButtonItem TitleButton;
            TitleButton = new UIBarButtonItem("Seleccione una bodega:", UIBarButtonItemStyle.Plain, (s, e) => { });
            TitleButton.Enabled = false;

            // Se crean los botones de la barra y se asignan los Eventos de los mismos.
            toolbar.Items = new UIBarButtonItem[]{
                TitleButton,
                new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                new UIBarButtonItem(UIBarButtonSystemItem.Done, delegate {
                    // Se quita del Foco de atenci�n.
                    txt_DatoLocalizacion.ResignFirstResponder();
                    
                    // Si la localizaci�n seleccionada es vacio y hay localizaciones disponibles para seleccionar, se le notifica 
                    // al usuario que seleccione una localizaci�n para la bodega del art�culo, se marca el campo de texto en rojo
                    // y se oculta la opci�n de regresar.
                    //if (string.IsNullOrEmpty(Localizacion))
                    //{
                    //    Toast.MakeToast(ParentViewController.View, "Hay localizaciones disponibles, seleccione una localizaci�n.", null, UIColor.Black, true, enumDuracionToast.Largo);

                    //    // Se marca el campo de texto de color rojo para indicar donde est� el error.
                    //    txt_DatoLocalizacion.BackgroundColor = UIColor.Red;

                    //    // Se esconde el bot�n de Atr�s.
                    //    this.NavigationItem.SetHidesBackButton(true, false);
                    //}
                    //else
                    if (!string.IsNullOrEmpty(Localizacion))
                    {
                        // Si la localizaci�n seleccionada es una de la lista.
                        // Se asigna al campo de texto la bodega seleccionada por el usuario.
                        txt_DatoLocalizacion.Text = Localizacion;

                        // Se le asigna la localizaci�n a la localizaci�n de la bodega.
                        artTemp.Bodega.Localizacion = Localizacion;

                        // Se limpia la marca de color en el campo de texto si la tuviera.
                        txt_DatoLocalizacion.BackgroundColor = UIColor.Clear;

                        // Se muestra el bot�n de Atr�s.
                        this.NavigationItem.SetHidesBackButton(false, false);
                    }

                    RefrescarDatosDetalle();
                })
            };

            // Se le asigna al campo de texto el control de selecci�n m�ltiple en lugar de usar un teclado.
            txt_DatoLocalizacion.InputView = picker;

            // Se le asigna al campo de texto la barra a mostrar sobre el teclado.
            txt_DatoLocalizacion.InputAccessoryView = toolbar;

            txt_DatoLocalizacion.Enabled = true;
        }

        /// <summary>
        /// M�todo que configura el campo de texto txt_DatoBonLocalizacion para ser usado como un controlador de selecci�n m�ltiple.
        /// Se crea un controlador de selecci�n m�ltiple que se mostrar� en pantalla una vez que el campo de texto 
        /// sea seleccionado y cuando la seleccion termine se le asignar� el valor elegido al campo de texto.
        /// </summary>
        private void configurarPickerBonLocalizacion()
        {
            // Se inicializa el modelo a utilizar por el campo de texto.
            PickerModel model = new PickerModel(BonLocalizaciones);

            // Evento de cambio de valor del selector.
            model.PickerChanged += (sender, e) =>
            {
                PickerChangedEventArgs obj = (PickerChangedEventArgs)e;

                // Se asigna la bodega seleccionada.
                BonLocalizacion = obj.SelectedValue;
            };

            // Se inicializa el selector m�ltiple a utilizar por el campo de texto.
            UIPickerView picker = new UIPickerView();
            picker.BackgroundColor = UIColor.White;
            picker.TintColor = UIColor.White;
            picker.ShowSelectionIndicator = true;
            picker.Model = model;

            // Se crea una barra para colocar el bot�n de listo en el teclado que abre el control txt_DatoCompania.
            UIToolbar toolbar = new UIToolbar(new RectangleF(0.0f, 0.0f, (float)this.View.Frame.Size.Width, 44.0f));
            toolbar.TintColor = UIColor.White;
            toolbar.BarStyle = UIBarStyle.Black;
            toolbar.Translucent = true;

            // Se crea un bot�n para usarse como T�tulo de la barra y se deshabilita para que no tenga funcionalidad.
            UIBarButtonItem TitleButton;
            TitleButton = new UIBarButtonItem("Seleccione una bodega:", UIBarButtonItemStyle.Plain, (s, e) => { });
            TitleButton.Enabled = false;

            // Se crean los botones de la barra y se asignan los Eventos de los mismos.
            toolbar.Items = new UIBarButtonItem[]{
                TitleButton,
                new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                new UIBarButtonItem(UIBarButtonSystemItem.Done, delegate {
                    // Se quita del Foco de atenci�n.
                    txt_DatoBonLocalizacion.ResignFirstResponder();
                    
                    // Si la localizaci�n seleccionada es vacio y hay localizaciones disponibles para seleccionar, se le notifica 
                    // al usuario que seleccione una localizaci�n para la bodega del art�culo, se marca el campo de texto en rojo
                    // y se oculta la opci�n de regresar.
                    //if (string.IsNullOrEmpty(BonLocalizacion))
                    //{
                    //    Toast.MakeToast(ParentViewController.View, "Hay localizaciones disponibles para el art�culo bonificado, seleccione una localizaci�n.", null, UIColor.Black, true, enumDuracionToast.Largo);

                    //    // Se marca el campo de texto de color rojo para indicar donde est� el error.
                    //    txt_DatoBonLocalizacion.BackgroundColor = UIColor.Red;

                    //    // Se esconde el bot�n de Atr�s.
                    //    this.NavigationItem.SetHidesBackButton(true, false);
                    //}
                    //else
                    if (!string.IsNullOrEmpty(BonLocalizacion))
                    {
                        // Si la localizaci�n seleccionada es una de la lista.
                        // Se asigna al campo de texto la bodega seleccionada por el usuario.
                        txt_DatoBonLocalizacion.Text = BonLocalizacion;

                        DetallePedido DetalleSeleccionado = GestorPedido.PedidosCollection.BuscarDetalle(artTemp);
                        
                        // Se le asigna la localizaci�n a la localizaci�n de la bodega del art�culo bonificado.
                        DetalleSeleccionado.LineaBonificada.Articulo.Bodega.Localizacion = BonLocalizacion;

                        // Se limpia la marca de color en el campo de texto si la tuviera.
                        txt_DatoBonLocalizacion.BackgroundColor = UIColor.Clear;

                        // Se muestra el bot�n de Atr�s.
                        this.NavigationItem.SetHidesBackButton(false, false);
                    }

                    RefrescarDatosDetalle();
                })
            };

            // Se le asigna al campo de texto el control de selecci�n m�ltiple en lugar de usar un teclado.
            txt_DatoBonLocalizacion.InputView = picker;

            // Se le asigna al campo de texto la barra a mostrar sobre el teclado.
            txt_DatoBonLocalizacion.InputAccessoryView = toolbar;

            txt_DatoBonLocalizacion.Enabled = true;
        }

        /// <summary>
        /// M�todo que sirve para mostrar la informaci�n del detalle del art�culo en pantalla.
        /// Llamado cada vez que se hace alg�n cambio a los datos del pedido para refrescar la informaci�n.
        /// </summary>
        public void RefrescarDatosDetalle()
        {
            Pedido pedido = GestorPedido.PedidosCollection.Buscar(GlobalUI.ClienteActual.Compania);

            DetallePedido DetalleSeleccionado = GestorPedido.PedidosCollection.BuscarDetalle(artTemp);

            // Si se pudo obtener la referencia al pedido y al detalle del art�culo.
            if (pedido != null && DetalleSeleccionado != null)
            {
                if (DetalleSeleccionado.RegaliaDescuentoLinea != null && 
                    DetalleSeleccionado.RegaliaDescuentoLinea.Tipo == TipoRegalia.DescuentoLinea)
                {
                    txt_Descuento.Enabled = false;
                }
                else
                {
                    txt_Descuento.Enabled = AppDelegate.CambiarDescuento;
                }

                // Se muestran los precios seg�n la moneda del cliente.
                if (dolar)
                {
                    lbl_PrecioAlmacen.Text = "(" + GestorUtilitario.SimboloDolar + ") Almac�n:";

                    lbl_PrecioDetalle.Text = "(" + GestorUtilitario.SimboloDolar + ") Detalle:";

                    lbl_MontoDescuento.Text = "(" + GestorUtilitario.SimboloDolar + ") Descuento:";

                    lbl_Impuesto.Text = "(" + GestorUtilitario.SimboloDolar + ") Impuesto:";

                    lbl_Subtotal.Text = "(" + GestorUtilitario.SimboloDolar + ") Subtotal:";

                    lbl_DatoTotal.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalNeto);

                    lbl_DatoImpuestos.Text = GestorUtilitario.FormatNumeroDolar(DetalleSeleccionado.Impuesto.MontoTotal);

                    lbl_DatoPrecioAlmacen.Text = GestorUtilitario.FormatNumeroDolar(DetalleSeleccionado.Precio.Maximo);
                    lbl_DatoPrecioDetalle.Text = GestorUtilitario.FormatNumeroDolar(DetalleSeleccionado.Precio.Minimo);
                    lbl_DatoSubtotal.Text = GestorUtilitario.FormatNumeroDolar(DetalleSeleccionado.MontoNeto);
                    lbl_DatoMontoDescuento.Text = GestorUtilitario.FormatNumeroDolar(DetalleSeleccionado.MontoDescuento);
                }
                else
                {
                    lbl_PrecioAlmacen.Text = "(" + simboloMonetarioPedido + ") Almac�n:";

                    lbl_PrecioDetalle.Text = "(" + simboloMonetarioPedido + ") Detalle:";

                    lbl_MontoDescuento.Text = "(" + simboloMonetarioPedido + ") Descuento:";

                    lbl_Impuesto.Text = "(" + simboloMonetarioPedido + ") Impuesto:";

                    lbl_Subtotal.Text = "(" + simboloMonetarioPedido + ") Subtotal:";

                    lbl_DatoTotal.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalNeto, simboloMonetarioPedido);

                    lbl_DatoImpuestos.Text = GestorUtilitario.FormatNumero(DetalleSeleccionado.Impuesto.MontoTotal, simboloMonetarioPedido);

                    lbl_DatoPrecioAlmacen.Text = GestorUtilitario.FormatNumero(DetalleSeleccionado.Precio.Maximo, simboloMonetarioPedido);
                    lbl_DatoPrecioDetalle.Text = GestorUtilitario.FormatNumero(DetalleSeleccionado.Precio.Minimo, simboloMonetarioPedido);
                    lbl_DatoSubtotal.Text = GestorUtilitario.FormatNumero(DetalleSeleccionado.MontoNeto, simboloMonetarioPedido);
                    lbl_DatoMontoDescuento.Text = GestorUtilitario.FormatNumero(DetalleSeleccionado.MontoDescuento, simboloMonetarioPedido);
                }

                // Si el detalle no tiene una linea bonificada se muestran valores por defecto.
                if (DetalleSeleccionado.LineaBonificada == null)
                {
                    txt_Bonificacion1.Text = GestorUtilitario.FormatDecimalAlmDet(0, 2);
                    txt_Bonificacion2.Text = GestorUtilitario.FormatDecimalAlmDet(0, 2);

                    btn_Bonificaciones.Enabled = false;

                    txt_DatoBonLocalizacion.Text = "";
                    txt_DatoBonLocalizacion.Placeholder = "No disponible";
                    txt_DatoBonLocalizacion.Enabled = false;

                    txt_DatoBonBodega.Text = "";
                    txt_DatoBonBodega.Placeholder = "No disponible";
                    txt_DatoBonBodega.Enabled = false;

                    BonBodegaDisponible = false;

                    lbl_DatoBonExistencia.Text = "";
                }
                else
                {
                    // Configurar los controladores solo si no se ha hecho a�n.
                    if (!BonBodegaDisponible)
                    {
                        // Cargar las bodegas disponibles del art�culo bonificado.
                        CargarBonBodegasDisponibles(DetalleSeleccionado.LineaBonificada.Articulo);

                        // Si encontr� al menos una bodega disponible para el art�culo bonificado.
                        if (BonBodegas.Count > 1)
                        {
                            // Se llama a configurar el campo de texto txt_DatoBonBodega para usarlo como selecci�n m�ltiple.
                            configurarPickerBonBodega();

                            btn_Bonificaciones.Enabled = true;
                            txt_DatoBonBodega.Enabled = true;
                        }
                        else
                        {
                            // Si no hay bodegas con el art�culo bonificado resgistrado se deshabilita la opci�n de seleccionar bodega.

                            txt_DatoBonBodega.Enabled = false;
                            txt_DatoBonBodega.Placeholder = "No disponible";
                        }
                    }
                    else
                    {
                        // Se cargan los valores de existencia del art�culo.
                        DetalleSeleccionado.LineaBonificada.Articulo.CargarExistenciaPedido(BonBodega);

                        // Se limpia la marca de color en el campo de texto si la tuviera.
                        txt_DatoBonBodega.BackgroundColor = UIColor.Clear;

                        if (UsaLocalizacion)
                        {
                            if (BonLocalizaciones.Count > 1)
                            {
                                if (!string.IsNullOrEmpty(BonLocalizacion) && BonLocalizaciones.Contains(BonLocalizacion))
                                {
                                    txt_DatoBonLocalizacion.Text = BonLocalizacion;
                                }
                                else
                                {
                                    txt_DatoBonLocalizacion.Text = BonLocalizaciones[1];
                                    BonLocalizacion = BonLocalizaciones[1];

                                    DetalleSeleccionado.LineaBonificada.Articulo.Bodega.Localizacion = BonLocalizacion;
                                }

                                txt_DatoBonLocalizacion.Enabled = true;
                            }
                            else
                            {
                                txt_DatoBonLocalizacion.Text = "No disponible.";
                                txt_DatoBonLocalizacion.Enabled = false;
                                BonLocalizacion = "";
                            }
                        }
                    }

                    txt_Bonificacion1.Text = GestorUtilitario.FormatDecimalAlmDet(DetalleSeleccionado.LineaBonificada.UnidadesAlmacen, 2);
                    txt_Bonificacion2.Text = GestorUtilitario.FormatDecimalAlmDet(DetalleSeleccionado.LineaBonificada.UnidadesDetalle, 2);

                    // Si la bodega seleccionada es vacia o no tiene existencias, se le muestra al usuario.
                    if (DetalleSeleccionado.LineaBonificada.Articulo.Bodega.Existencia < 0)
                    {
                        lbl_DatoBonExistencia.Text = "Sin existencias.";
                    }
                    else
                    {
                        lbl_DatoBonExistencia.Text = GestorUtilitario.FormatDecimalAlmDet(
                            DetalleSeleccionado.LineaBonificada.Articulo.Bodega.Existencia, 4);
                    }
                }

                // Si el detalle tiene un descuento que se aplica se muestra dicho descuento, de lo contrario ser� 0.
                if (DetalleSeleccionado.Descuento != null)
                {
                    //txt_Descuento.Text = GestorUtilitario.FormatPorcentaje(DetalleSeleccionado.Descuento.Monto);
                    txt_Descuento.Text = GestorUtilitario.FormatPorcentaje(DetalleSeleccionado.CalcularPorcentajeDescuento());
                }
                else
                {
                    txt_Descuento.Text = GestorUtilitario.FormatPorcentaje(0);
                }

                // Si la bodega seleccionada es vacia o no tiene existencias, se le muestra al usuario.
                if (artTemp.Bodega.Existencia < 0)
                {
                    lbl_DatoExistencia.Text = "Sin existencias.";
                }
                else
                {
                    lbl_DatoExistencia.Text = GestorUtilitario.FormatDecimalAlmDet(artTemp.Bodega.Existencia, 4);
                }

                // Si no se puede vender por detalle deshabilitar la opci�n para ingresar cantidades de detalle.
                if(artTemp.UnidadEmpaque == 1)
                {
                    txt_CantidadDetalle.Enabled = false;
                }
                else
                {
                    txt_CantidadDetalle.Enabled = true;
                }
            }
            else
            {
                // En caso de que no se pudiera obtener el pedido o el detalle se muestran valores por defecto.

                if (dolar)
                {
                    lbl_PrecioAlmacen.Text = "(" + GestorUtilitario.SimboloDolar + ") Almac�n:";

                    lbl_PrecioDetalle.Text = "(" + GestorUtilitario.SimboloDolar + ") Detalle:";

                    lbl_MontoDescuento.Text = "(" + GestorUtilitario.SimboloDolar + ") Descuento:";

                    lbl_Impuesto.Text = "(" + GestorUtilitario.SimboloDolar + ") Impuesto:";

                    lbl_Subtotal.Text = "(" + GestorUtilitario.SimboloDolar + ") Subtotal:";

                    lbl_DatoTotal.Text = GestorUtilitario.FormatNumeroDolar(0);
                    lbl_DatoPrecioAlmacen.Text = GestorUtilitario.FormatNumeroDolar(0);
                    lbl_DatoPrecioDetalle.Text = GestorUtilitario.FormatNumeroDolar(0);
                    lbl_DatoSubtotal.Text = GestorUtilitario.FormatNumeroDolar(0);
                    lbl_DatoMontoDescuento.Text = GestorUtilitario.FormatNumeroDolar(0);
                }
                else
                {
                    lbl_PrecioAlmacen.Text = "(" + GestorUtilitario.SimboloMonetario + ") Almac�n:";

                    lbl_PrecioDetalle.Text = "(" + GestorUtilitario.SimboloMonetario + ") Detalle:";

                    lbl_MontoDescuento.Text = "(" + GestorUtilitario.SimboloMonetario + ") Descuento:";

                    lbl_Impuesto.Text = "(" + GestorUtilitario.SimboloMonetario + ") Impuesto:";

                    lbl_Subtotal.Text = "(" + GestorUtilitario.SimboloMonetario + ") Subtotal:";

                    lbl_DatoTotal.Text = GestorUtilitario.FormatNumero(0, simboloMonetarioPedido);
                    lbl_DatoPrecioAlmacen.Text = GestorUtilitario.FormatNumero(0, simboloMonetarioPedido);
                    lbl_DatoPrecioDetalle.Text = GestorUtilitario.FormatNumero(0, simboloMonetarioPedido);
                    lbl_DatoSubtotal.Text = GestorUtilitario.FormatNumero(0, simboloMonetarioPedido);
                    lbl_DatoMontoDescuento.Text = GestorUtilitario.FormatNumero(0, simboloMonetarioPedido);
                }

                txt_Bonificacion1.Text = GestorUtilitario.FormatDecimalAlmDet(0, 2);
                txt_Bonificacion2.Text = GestorUtilitario.FormatDecimalAlmDet(0, 2);

                txt_Descuento.Text = GestorUtilitario.FormatPorcentaje(0);
                lbl_DatoExistencia.Text = "Sin existencias.";

                lbl_DatoBonExistencia.Text = "Sin existencias.";

                txt_CantidadAlmacen.Enabled = false;
                txt_CantidadDetalle.Enabled = false;

                btn_Bonificaciones.Enabled = false;

                txt_DatoLocalizacion.Enabled = false;
                txt_DatoBonLocalizacion.Enabled = false;

                txt_DatoBodega.Enabled = false;
                txt_DatoBonBodega.Enabled = false;
            }
            
            TableView.ReloadData();
        }

        /// <summary>
        /// M�todo que se usa para cargar las bodegas del art�culo y agregar a la lista de bodegas �nicamente
        /// aquellas que al menos tengan al art�culo registrado, selecciona automaticamente la bodega que el art�culo
        /// trae previamente definida.
        /// </summary>
        private void CargarBodegasDisponibles()
        {
            try
            {
                // Variable para almacenar la bodega que trae registado ya el art�culo.
                string bodegaActual = artTemp.Bodega.Codigo;

                // Variable para almacenar la primera bodega que se encuentre disponible en caso de que no
                // haya una bodega actual para el art�culo.
                string bodegaFirst = string.Empty;

                // Variable para almacenar las existencias del art�culo.
                decimal existencias = -1;      
          
                // Se revisa en cada bodega si el art�culo existe, de ser as� se agrega a la lista.
                foreach (Ruta ruta in GlobalUI.RutasActuales)
                {
                    // Se obtienen las existencias del art�culo en la bodega.
                    artTemp.CargarExistenciaPedido(ruta.Bodega);                    
                    existencias = artTemp.Bodega.Existencia;

                    // Si almenos hay 0 o m�s en existencia indica que el art�culo est� registrado en la bodega.
                    if (existencias >= 0)
                    {
                        // Si es la primera bodega que tenga el art�culo se guarda.
                        if(string.IsNullOrEmpty(bodegaFirst))
                            bodegaFirst = ruta.Bodega;

                        BodegaDisponible = true;
                        Bodegas.Add(ruta.Bodega);
                    }
                }

                // Si no habia una bodega actual seleccionada para el art�culo, se asigna la primera que se encontr�.
                if (string.IsNullOrEmpty(bodegaActual))
                    bodegaActual = bodegaFirst;
                
                // Si no hay ninguna bodega disponible para el art�culo se le notifica al usuario.
                if (Bodegas.Count <= 1)
                {
                    Toast.MakeToast(ParentViewController.View, "No se encontraron existencias o registros del art�culo en ninguna bodega.", null, UIColor.Black, true, enumDuracionToast.Mediano);
                }
                else if (bodegaActual != null)
                {
                    // Si hay una bodega actual se carga sus existencias y se selecciona como bodega del art�culo.
                    artTemp.CargarExistenciaPedido(bodegaActual);
                    txt_DatoBodega.Text = bodegaActual;

                    CodBodega = bodegaActual;
                    BonLocalizacion = artTemp.Bodega.Localizacion;

                    if (UsaLocalizacion)
                    {
                        Localizaciones.Clear();
                        Localizaciones.Add("");
                        Localizaciones.AddRange(artTemp.Bodega.Obtenerlocalizaciones(artTemp));

                        if (Localizaciones.Count > 1)
                        {
                            configurarPickerLocalizacion();

                            if (!string.IsNullOrEmpty(Localizacion) && Localizaciones.Contains(Localizacion))
                            {
                                txt_DatoLocalizacion.Text = Localizacion;
                            }
                            else
                            {
                                txt_DatoLocalizacion.Text = Localizaciones[1];
                                Localizacion = Localizaciones[1];

                                artTemp.Bodega.Localizacion = Localizacion;
                            }

                            txt_DatoLocalizacion.Enabled = true;
                        }
                        else
                        {
                            txt_DatoLocalizacion.Text = "No disponible.";
                            txt_DatoLocalizacion.Enabled = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error cargando las existencias en planta. " + ex.Message);
            }
        }

        /// <summary>
        /// M�todo que se usa para cargar las bodegas del art�culo bonificado y agregar a la lista de bodegas �nicamente
        /// aquellas que al menos tengan al art�culo registrado, selecciona automaticamente la bodega que el art�culo bonificado
        /// trae previamente definida.
        /// </summary>
        private void CargarBonBodegasDisponibles(Articulo artBonificado)
        {
            try
            {
                // Variable para almacenar la bodega que trae registado ya el art�culo.
                string bodegaActual = artBonificado.Bodega.Codigo;

                // Variable para almacenar la primera bodega que se encuentre disponible en caso de que no
                // haya una bodega actual para el art�culo.
                string bodegaFirst = string.Empty;

                // Variable para almacenar las existencias del art�culo.
                decimal existencias = -1;

                // Se revisa en cada bodega si el art�culo existe, de ser as� se agrega a la lista.
                foreach (Ruta ruta in GlobalUI.RutasActuales)
                {
                    // Se obtienen las existencias del art�culo en la bodega.
                    artBonificado.CargarExistenciaPedido(ruta.Bodega);
                    existencias = artBonificado.Bodega.Existencia;

                    // Si almenos hay 0 o m�s en existencia indica que el art�culo est� registrado en la bodega.
                    if (existencias >= 0)
                    {
                        // Si es la primera bodega que tenga el art�culo se guarda.
                        if (string.IsNullOrEmpty(bodegaFirst))
                            bodegaFirst = ruta.Bodega;

                        BonBodegaDisponible = true;
                        BonBodegas.Add(ruta.Bodega);
                    }
                }

                // Si no habia una bodega actual seleccionada para el art�culo, se asigna la primera que se encontr�.
                if (string.IsNullOrEmpty(bodegaActual))
                    bodegaActual = bodegaFirst;

                // Si no hay ninguna bodega disponible para el art�culo se le notifica al usuario.
                if (BonBodegas.Count <= 1)
                {
                    Toast.MakeToast(ParentViewController.View, "No se encontraron existencias o registros del art�culo bonificado en ninguna bodega.", null, UIColor.Black, true, enumDuracionToast.Mediano);
                }
                else if (bodegaActual != null)
                {
                    // Si hay una bodega actual se carga sus existencias y se selecciona como bodega del art�culo.
                    artBonificado.CargarExistenciaPedido(bodegaActual);
                    txt_DatoBonBodega.Text = bodegaActual;

                    BonBodega = bodegaActual;
                    BonLocalizacion = artBonificado.Bodega.Localizacion;
                    
                    if (UsaLocalizacion)
                    {
                        BonLocalizaciones.Clear();
                        BonLocalizaciones.Add("");
                        BonLocalizaciones.AddRange(artBonificado.Bodega.Obtenerlocalizaciones(artBonificado));

                        if (BonLocalizaciones.Count > 1)
                        {
                            configurarPickerBonLocalizacion();

                            if (!string.IsNullOrEmpty(BonLocalizacion) && BonLocalizaciones.Contains(BonLocalizacion))
                            {
                                txt_DatoBonLocalizacion.Text = BonLocalizacion;
                            }
                            else
                            {
                                txt_DatoBonLocalizacion.Text = BonLocalizaciones[1];
                                BonLocalizacion = BonLocalizaciones[1];

                                artBonificado.Bodega.Localizacion = BonLocalizacion;
                            }

                            txt_DatoBonLocalizacion.Enabled = true;
                        }
                        else
                        {
                            txt_DatoBonLocalizacion.Text = "No disponible.";
                            txt_DatoBonLocalizacion.Enabled = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error cargando las existencias en planta. " + ex.Message);
            }
        }

        /// <summary>
        /// M�todo de la clase UITableViewController para definir la altura de las celdas.
        /// Se usa para cambiar la altura de la celda que contiene la imagen al tama�o de la altura de la imagen.
        /// </summary>
        /// <param name="tableView"></param>
        /// <param name="indexPath"></param>
        /// <returns></returns>
        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            // Si es la cuarta secci�n y es la primera fila, acomodar el tama�o, de lo contrario retornar el tama�o estandar.
            if (indexPath.Section == 3 && indexPath.Row == 0)
            {
                return img_Imagen.Bounds.Height + 30;
            }
            else
            {
                return 44;
            }
        }
        
        #endregion

        #region View lifecycle

        /// <summary>
        /// M�todo para liberaci�n de memoria de la pantalla cuando se cierra.
        /// </summary>
        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        /// <summary>
        /// M�todo que se ejecuta al crear la pantalla, se ejecuta una �nica vez a menos que la pantalla se remueva del stack.
        /// </summary>
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Inicializar las variables o asignar los datos que se utilizan en la pantalla.

            dolar = GlobalUI.ClienteActual.ClienteCompania[0].Moneda == TipoMoneda.DOLAR;

            artTemp = GestorPedido.carrito[Indice];

            // Se configura la imagen del art�culo.
            //img_Imagen.Image = FromUrl(string.Format("{0}images/xamarin/{1}/{2}.jpg",
            //      AppDelegate.Servidor, articulo.Compania, articulo.Codigo));
            img_Imagen.Layer.BorderColor = UIColor.DarkGray.CGColor;
            img_Imagen.Layer.BorderWidth = 2f;
            img_Imagen.Layer.CornerRadius = 3f;
            img_Imagen.Layer.MasksToBounds = true;
            img_Imagen.ContentMode = UIViewContentMode.ScaleToFill;
            img_Imagen.Image = FromFile(artTemp.Compania, artTemp.Codigo);

            // Se muestra la cantidad seleccionada para el art�culo.
            txt_CantidadAlmacen.Text = String.Format("{0:0}", artTemp.CantidadAlmacen);
            txt_CantidadDetalle.Text = String.Format("{0:0}", artTemp.CantidadDetalle);

            Bodegas = new List<string>();
            Bodegas.Add("");

            BonBodegas = new List<string>();
            BonBodegas.Add("");

            Localizaciones = new List<string>();
            Localizaciones.Add("");

            BonLocalizaciones = new List<string>();
            BonLocalizaciones.Add("");

            UsaLocalizacion = GestorPedido.PedidosCollection.Gestionados[0].Configuracion.Compania.UsaLocalizacion;

            if (!UsaLocalizacion)
            {
                txt_DatoLocalizacion.Placeholder = "No disponible";
                txt_DatoLocalizacion.Enabled = false;

                txt_DatoBonLocalizacion.Placeholder = "No disponible";
                txt_DatoBonLocalizacion.Enabled = false;
            }

            CargarBodegasDisponibles();

            // Se crea una barra para colocar el bot�n de listo en el teclado que abre el control txt_Cantidad.
            UIToolbar toolbar = new UIToolbar(new RectangleF(0.0f, 0.0f, (float)this.View.Frame.Size.Width, 44.0f));
            toolbar.TintColor = UIColor.White;
            toolbar.BarStyle = UIBarStyle.Black;
            toolbar.Translucent = true;

            // Se crean los botones de la barra y se asignan los Eventos de los mismos.
            toolbar.Items = new UIBarButtonItem[]{
                new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                new UIBarButtonItem(UIBarButtonSystemItem.Done, delegate {
                    // Si el usuario dejo el campo de cantidad en blanco se pone por defecto la cantidad de 1.
                    if (string.IsNullOrEmpty(txt_CantidadAlmacen.Text))
                    {
                        txt_CantidadAlmacen.Text = "1";
                    }

                    try
                    {
                        // Se obtiene la cantidad ingresada por el usuario.
                        cantidadArticuloAlm = Convert.ToDecimal(txt_CantidadAlmacen.Text);
                    }
                    catch
                    {
                        // Si la conversi�n a decimal fall�, indica que se ingres� alg�n caracter no num�rico.
                        cantidadArticuloAlm = GestorPedido.carrito[Indice].CantidadAlmacen;
                    }

                    // Si la nueva cantidad es diferente de la cantidad actual, se procede a modificar el detalle.
                    if (GestorPedido.carrito[Indice].CantidadAlmacen != cantidadArticuloAlm)
                    {
                        // Se asigna la nueva cantidad al art�culo.
                        GestorPedido.carrito[Indice].CantidadAlmacen = cantidadArticuloAlm;

                        // Se vuelve a calcular todos los montos del detalle.
                        GestorPedido.AgregarModificarDetalle(artTemp);

                        RefrescarDatosDetalle();
                    }

                    // Se actualiza la cantidad en el campo de texto.
                    txt_CantidadAlmacen.Text = GestorUtilitario.FormatMiles(cantidadArticuloAlm);

                    txt_CantidadAlmacen.ResignFirstResponder();
                })
            };

            // Se le asigna al campo de texto la barra a mostrar sobre el teclado.
            txt_CantidadAlmacen.InputAccessoryView = toolbar;

            // Se crea una barra para colocar el bot�n de listo en el teclado que abre el control txt_Cantidad.
            UIToolbar toolbar3 = new UIToolbar(new RectangleF(0.0f, 0.0f, (float)this.View.Frame.Size.Width, 44.0f));
            toolbar3.TintColor = UIColor.White;
            toolbar3.BarStyle = UIBarStyle.Black;
            toolbar3.Translucent = true;

            // Se crean los botones de la barra y se asignan los Eventos de los mismos.
            toolbar3.Items = new UIBarButtonItem[]{
                new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                new UIBarButtonItem(UIBarButtonSystemItem.Done, delegate {
                    // Si el usuario dejo el campo de cantidad en blanco se pone por defecto la cantidad de 1.
                    if (string.IsNullOrEmpty(txt_CantidadDetalle.Text))
                    {
                        txt_CantidadDetalle.Text = "0";
                    }

                    try
                    {
                        // Se obtiene la cantidad ingresada por el usuario.
                        cantidadArticuloDet = Convert.ToDecimal(txt_CantidadDetalle.Text);
                    }
                    catch
                    {
                        // Si la conversi�n a decimal fall�, indica que se ingres� alg�n caracter no num�rico.
                        cantidadArticuloDet = GestorPedido.carrito[Indice].CantidadDetalle;
                    }

                    // Si la nueva cantidad es diferente de la cantidad actual, se procede a modificar el detalle.
                    if (GestorPedido.carrito[Indice].CantidadDetalle != cantidadArticuloDet)
                    {
                        // Se asigna la nueva cantidad al art�culo.
                        GestorPedido.carrito[Indice].CantidadDetalle = cantidadArticuloDet;

                        // Se vuelve a calcular todos los montos del detalle.
                        GestorPedido.AgregarModificarDetalle(artTemp);

                        RefrescarDatosDetalle();
                    }

                    // Se actualiza la cantidad en el campo de texto.
                    txt_CantidadDetalle.Text = GestorUtilitario.FormatMiles(cantidadArticuloDet);

                    txt_CantidadDetalle.ResignFirstResponder();
                })
            };

            // Se le asigna al campo de texto la barra a mostrar sobre el teclado.
            txt_CantidadDetalle.InputAccessoryView = toolbar3;

            // Se crea una barra para colocar el bot�n de listo en el teclado que abre el control txt_Cantidad.
            UIToolbar toolbar2 = new UIToolbar(new RectangleF(0.0f, 0.0f, (float)this.View.Frame.Size.Width, 44.0f));
            toolbar2.TintColor = UIColor.White;
            toolbar2.BarStyle = UIBarStyle.Black;
            toolbar2.Translucent = true;

            // Se crean los botones de la barra y se asignan los Eventos de los mismos.
            toolbar2.Items = new UIBarButtonItem[]{
                new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                new UIBarButtonItem(UIBarButtonSystemItem.Done, delegate {
                    // Si el usuario dejo el campo de descuento en blanco se pone por defecto 0 de descuento.
                    if (string.IsNullOrEmpty(txt_Descuento.Text))
                    {
                        txt_Descuento.Text = "0";
                    }
                    
                    // Se obtiene el descuento ingresado por el usuario.
                    descuentoArticulo = Convert.ToDecimal(txt_Descuento.Text);

                    DetallePedido detalle = GestorPedido.PedidosCollection.BuscarDetalle(artTemp);

                    if (PedidosCollection.CambiarDescuento)
                    {
                        if (detalle.LineaBonificada != null)
                        {
                            GestorPedido.CambiosBonificacionDescuento(detalle.Articulo, descuentoArticulo, detalle.LineaBonificada.TotalAlmacen, 0);
                        }
                        else
                        {
                            GestorPedido.CambiosBonificacionDescuento(detalle.Articulo, descuentoArticulo, 0, 0);
                        }
                    }

                    RefrescarDatosDetalle();

                    this.txt_Descuento.ResignFirstResponder();
                })
            };

            // Se le asigna al campo de texto la barra a mostrar sobre el teclado.
            txt_Descuento.InputAccessoryView = toolbar2;

            // Se agrega un Observer para escuchar el evento de ValueChanged al cambiar el descuento.
            NSNotificationCenter.DefaultCenter.AddObserver
            (UITextField.TextFieldTextDidChangeNotification, (notification) =>
            {
                if (notification.Object == txt_Descuento)
                {
                    txt_Descuento_ValueChanged();
                }
                else if (notification.Object == txt_CantidadAlmacen)
                {
                    txt_CantidadAlmacen_ValueChanged();
                }
                else if (notification.Object == txt_CantidadDetalle)
                {
                    txt_CantidadDetalle_ValueChanged();
                }
            });

            // Si encontr� al menos una bodega disponible para el art�culo.
            if (Bodegas.Count > 1)
            {
                // Se llama a configurar el campo de texto txt_DatoBodega para usarlo como selecci�n m�ltiple.
                configurarPickerBodega();
            }
            else
            {
                // Si no hay bodegas con el art�culo resgistrado se deshabilita la opci�n de seleccionar bodega.

                txt_DatoBodega.Enabled = false;
                txt_DatoBodega.Placeholder = "No disponible";
            }
            
            // Agregar en la barra de Navegaci�n un bot�n para cancelar el pedido.
            this.NavigationItem.SetRightBarButtonItem(
                new UIBarButtonItem(UIImage.FromFile("btn_Cancelar.png")
                , UIBarButtonItemStyle.Plain
                , (sender, args) =>
                {
                    // Evento de click del bot�n de cancelar pedido.

                    UIAlertView alert = new UIAlertView("Cancelar pedido", "Va a salir de la gesti�n del pedido.", null, "Salir", "Continuar");
                    alert.Clicked += (s, b) =>
                    {
                        if (b.ButtonIndex == 0)
                        {
                            // Limpia y reinicia los datos del pedido global.
                            PedidosCollection.FacturarPedido = false;
                            GestorPedido.PedidosCollection = new PedidosCollection();
                            Pedido.BorrarXMLPedido(GlobalUI.ClienteActual.Codigo);

                            // Limpia el carrito de compras.
                            GestorPedido.carrito = null;

                            // Limpia al cliente del pedido.
                            GlobalUI.ClienteActual = null;
                            GlobalUI.RutaActual = null;
                            GlobalUI.RutasActuales = null;

                            // Asigna que no se est� creando una orden.
                            PedidosView.crearOrden = false;

                            // Devuelve al usuario a la pantalla inicial de la secci�n de pedidos.
                            InvokeOnMainThread(() => { NavigationController.PopToViewController(pedidosView, true); });
                        }

                    };
                    alert.Show();
                })
            , true);

            this.Title = artTemp.Descripcion;
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a aparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            var compania = Compania.Obtener(GlobalUI.ClienteActual.Compania);

            if (compania != null)
            {
                simboloMonetarioPedido = compania.SimboloMonetarioFunc;
            }

            RefrescarDatosDetalle();
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla apareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            // Refrescar la lista para que reacomode la celda de la imagen.
            TableView.ReloadData();

            // Se inicia el proceso de intentar sincronizar pedidos pendientes.
            AppDelegate.IniciarSincronizacionPedidos(this.ParentViewController.ParentViewController);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a desaparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla desapareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
	}
}
