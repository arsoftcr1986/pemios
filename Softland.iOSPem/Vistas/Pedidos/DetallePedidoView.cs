using System;
using System.Linq;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Drawing;

using BI.Shared;
using Pedidos.Core;

namespace Softland.iOS.Pem
{
	partial class DetallePedidoView : UIViewController
    {

        #region Propiedades y Contructor de instancia

        /// <summary>
        /// Lista de art�culos a cargar en el controlador de tabla del carrito, se llena con los art�culos agregados al carrito.
        /// </summary>
        public List<Articulo> articulos;

        /// <summary>
        /// Referencia a PedidosView para tener acceso a sus variables antes de devolverse a esa pantalla.
        /// </summary>
        public PedidosView pedidosView;

        /// <summary>
        /// Referencia a si mismo para tener acceso a sus variables desde otras clases. 
        /// </summary>
        public static DetallePedidoView VistaPadre;

        /// <summary>
        /// Variable que almacena los valores para generar el color verde en los mensajes de pantalla de exito.
        /// </summary>
        public static UIColor verdeOscuro = UIColor.FromRGB(0, 200, 0);

        /// <summary>
        /// Variable que almacena el S�mbolo Monetario de la compa��a del cliente actual del pedido.
        /// </summary>
        public string simboloMonetarioPedido = string.Empty;

        /// <summary>
        /// Variable que almacena si la moneda del cliente es el dolar.
        /// </summary>
        public bool dolar;

        /// <summary>
        /// Costructor por defecto.
        /// </summary>
        /// <param name="handle"></param>
		public DetallePedidoView (IntPtr handle) : base (handle)
		{
		}

        #endregion

        #region Tabla y Celdas

        /// <summary>
        /// Clase que se utilizar� para manejar la informaci�n de la tabla de art�culos en el pedido.
        /// </summary>
        public class TableSource : UITableViewSource
        {

            #region Propiedades y Contructor de instancia

            /// <summary>
            /// Variable para identificar las celdas que utiliza este TableSource.
            /// </summary>
            string cellIdentifier = "Articulo";

            /// <summary>
            /// Constructor por defecto. 
            /// </summary>
            /// <param name="padre"></param>
            public TableSource()
            {

            }

            #endregion

            #region Eventos

            // Evento requerido por la clase, retorna la cantidad de registros en la fuente de datos.
            public override nint RowsInSection(UITableView tableview, nint section)
            {
                if (VistaPadre.articulos != null)
                {
                    return VistaPadre.articulos.Count;
                }
                else
                {
                    return 0;
                }
            }

            /// <summary>
            /// Evento que se ejecuta al cargar las celdas de la tabla seg�n vayan siendo mostradas.
            /// </summary>
            /// <param name="tableView"></param>
            /// <param name="indexPath">�ndice de la celda por cargar.</param>
            /// <returns>Retorna la celda cargada.</returns>
            public override UITableViewCell GetCell(UITableView tableView, Foundation.NSIndexPath indexPath)
            {
                // Busca la celda creada para ahorrar memoria.
                var cell = tableView.DequeueReusableCell(cellIdentifier) as CustomArticuloCell;

                // Si no hay celdas por reutilizar, se crea una nueva, en este caso se usa una celda CustomArticuloCell.
                if (cell == null)
                {
                    cell = new CustomArticuloCell((NSString)cellIdentifier);
                }

                // Referencia al art�culo de la celda.
                var articulo = VistaPadre.articulos[indexPath.Row];

                // Referencia al detalle del art�culo de la celda.
                var detalle = GestorPedido.PedidosCollection.BuscarDetalle(articulo);
                
                // Se actualiza la informaci�n de la celda.
                cell.articulo = indexPath.Row;
                cell.UpdateCell(articulo.Descripcion, detalle);

                return cell;
            }

            /// <summary>
            /// Evento que se ejecuta al seleccionar una celda de la tabla.
            /// </summary>
            /// <param name="tableView"></param>
            /// <param name="indexPath">�ndice de la celda seleccionada.</param>
            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                // Se referencia al Storyboard del app.
                var myStoryboard = AppDelegate.Storyboard;

                // Se instancia un ViewController con el ID del Storyboard y su clase respectiva.
                var vistaDetalle = myStoryboard.InstantiateViewController("DetalleArticuloPedidoTableView") as DetalleArticuloPedidoTableView;

                // Se asignan los valores que se desean pasar a la nueva pantalla.
                vistaDetalle.Indice = indexPath.Row;
                vistaDetalle.pedidosView = VistaPadre.pedidosView;

                // Se navega a la pantalla instanciada agregandola al stack.
                VistaPadre.NavigationController.PushViewController(vistaDetalle, true);

                // Se quita el resaltado que se le da a una celda al ser seleccionada (simula comportamiento de iOS).
                tableView.DeselectRow(indexPath, true);
            }

            /// <summary>
            /// Evento que se ejecuta una vez finalizada la edici�n de una celda de la lista.
            /// En este caso se utiliza para borrar la celda de la lista y quitar el art�culo del pedido.
            /// </summary>
            /// <param name="tableView"></param>
            /// <param name="editingStyle"></param>
            /// <param name="indexPath">�ndice de la celda seleccionada.</param>
            public override void CommitEditingStyle(UITableView tableView, UITableViewCellEditingStyle editingStyle, NSIndexPath indexPath)
            {
                // Acciones a tomar seg�n el tipo de edici�n que se di� sobre la celda.
                switch (editingStyle)
                {
                    case UITableViewCellEditingStyle.Delete: //Borrar celda.

                        // Se elimina el art�culo del pedido y del carrito realizando todos los c�lculos correspondientes.
                        GestorPedido.RetirarDetalle(VistaPadre.articulos[indexPath.Row]);
                        VistaPadre.articulos.RemoveAt(indexPath.Row);

                        // Se borra el art�culo de la lista (se elimina la celda).
                        tableView.DeleteRows(new NSIndexPath[] { indexPath }, UITableViewRowAnimation.Left);

                        // Se refresca la informaci�n del pedido seg�n el nuevo c�lculo.
                        VistaPadre.RefrescarPrecioTotal();

                        if (VistaPadre.articulos.Count == 0)
                        {
                            VistaPadre.NavigationItem.SetHidesBackButton(false, false);
                        }

                        Toast.MakeToast(VistaPadre.View, "Articulo removido del carrito", null, verdeOscuro, true, enumDuracionToast.Corto);
                        break;
                    case UITableViewCellEditingStyle.None: //Ninguna edici�n.
                        Console.WriteLine("CommitEditingStyle:None called");
                        break;
                }
            }

            // Evento que se usa para seleccionar si se puede editar las celdas o no.
            public override bool CanEditRow(UITableView tableView, NSIndexPath indexPath)
            {
                // Se puede retornar false para bloquear una (seg�n el �ndice) o todas las celdas de la tabla.
 
                // retorna true para habilitar editar en todas las celdas.
                return true;
            }

            // Evento que especifica el texto a mostrar en el bot�n de eliminar celda, el texto default es 'Delete'.
            public override string TitleForDeleteConfirmation(UITableView tableView, NSIndexPath indexPath)
            {   
                string mensaje = "";

                try
                {
                    mensaje = "Borrar " + VistaPadre.articulos[indexPath.Row].Codigo;
                }
                catch
                {
                    mensaje = "Borrar";
                }

                return mensaje;
            }

            #endregion
        }

        /// <summary>
        /// Clase que se utilizar� para manejar la informaci�n de la celda de la tabla de art�culos en el pedido.
        /// </summary>
        public class CustomArticuloCell : UITableViewCell
        {

            #region Propiedades y Contructor de instancia

            /// <summary>
            /// Controladores para mostrar el nombre y la cantidad en el carrito del art�culo.
            /// </summary>
            UILabel lbl_Nombre, lbl_Cantidad, lbl_Bonificaciones, lbl_Descuento, lbl_DatoDescuento, lbl_Impuesto, lbl_DatoImpuesto,
                lbl_DatoBonificaciones, lbl_Total, lbl_SubTotal, lbl_TotalSinDescuento, lbl_DatoCantidad;
            
            /// <summary>
            /// Controlador para modificar la cantidad de cada art�culo.
            /// </summary>
            UIStepper stp_Cantidad;

            /// <summary>
            /// Controlador para mostrar la informaci�n del art�culo bonificado.
            /// </summary>
            UIButton btn_bonifInfo;

            /// <summary>
            /// Referencia al detalle del art�culo de la celda.
            /// </summary>
            DetallePedido detalleArticulo;
            
            /// <summary>
            /// �ndice del art�culo en el carrito.
            /// </summary>
            public int articulo;
            
            /// <summary>
            /// Constructor por defecto.
            /// </summary>
            /// <param name="cellId"></param>
            public CustomArticuloCell(NSString cellId)
                : base(UITableViewCellStyle.Default, cellId)
            {
                SelectionStyle = UITableViewCellSelectionStyle.Blue;
                ContentView.BackgroundColor = UIColor.Clear;

                stp_Cantidad = new UIStepper()
                {
                    MinimumValue = 1,
                    MaximumValue = 999999999999999999, // 18 D�gitos m�ximo para el Int64.
                    Value = 1,
                    StepValue = 1
                };
                stp_Cantidad.ValueChanged += stp_Cantidad_ValueChanged;

                lbl_Nombre = new UILabel()
                {
                    BackgroundColor = UIColor.Clear,
                    TextAlignment = UITextAlignment.Left,
                    Font = UIFont.FromName("Helvetica-Bold", 14f),
                    AdjustsFontSizeToFitWidth = false
                };

                lbl_DatoCantidad = new UILabel()
                {
                    TextAlignment = UITextAlignment.Right,
                    BackgroundColor = UIColor.Clear,
                    Font = UIFont.FromName("Helvetica", 14f),
                    AdjustsFontSizeToFitWidth = true,
                    MinimumFontSize = 8
                };

                lbl_DatoBonificaciones = new UILabel()
                {
                    TextAlignment = UITextAlignment.Right,
                    BackgroundColor = UIColor.Clear,
                    Font = UIFont.FromName("Helvetica", 14f),
                    AdjustsFontSizeToFitWidth = true,
                    MinimumFontSize = 8
                };

                lbl_DatoDescuento = new UILabel()
                {
                    BackgroundColor = UIColor.Clear,
                    TextAlignment = UITextAlignment.Right,
                    Font = UIFont.FromName("Helvetica", 14f),
                    AdjustsFontSizeToFitWidth = true,
                    MinimumFontSize = 8
                };

                lbl_DatoImpuesto = new UILabel()
                {
                    BackgroundColor = UIColor.Clear,
                    TextAlignment = UITextAlignment.Right,
                    Font = UIFont.FromName("Helvetica", 14f),
                    AdjustsFontSizeToFitWidth = true,
                    MinimumFontSize = 8
                };

                lbl_Impuesto = new UILabel()
                {
                    Text = "Impuesto:",
                    TextAlignment = UITextAlignment.Left,
                    BackgroundColor = UIColor.Clear,
                    Font = UIFont.FromName("Helvetica", 14f),
                    AdjustsFontSizeToFitWidth = true,
                    MinimumFontSize = 8
                };
                
                lbl_Cantidad = new UILabel()
                {
                    Text = "Cantidad:",
                    TextAlignment = UITextAlignment.Left,
                    BackgroundColor = UIColor.Clear,
                    Font = UIFont.FromName("Helvetica", 14f),
                    AdjustsFontSizeToFitWidth = true,
                    MinimumFontSize = 8
                };

                lbl_Descuento = new UILabel()
                {
                    Text = "Descuento:",
                    TextAlignment = UITextAlignment.Left,
                    BackgroundColor = UIColor.Clear,
                    Font = UIFont.FromName("Helvetica", 14f),
                    AdjustsFontSizeToFitWidth = true,
                    MinimumFontSize = 8
                };

                lbl_Bonificaciones = new UILabel()
                {
                    Text = "Bonificaciones:",
                    TextAlignment = UITextAlignment.Left,
                    BackgroundColor = UIColor.Clear,
                    Font = UIFont.FromName("Helvetica", 14f),
                    AdjustsFontSizeToFitWidth = true,
                    MinimumFontSize = 8
                };

                lbl_Total = new UILabel()
                {
                    Text = "Total:",
                    TextAlignment = UITextAlignment.Left,
                    BackgroundColor = UIColor.Clear,
                    Font = UIFont.FromName("Helvetica", 14f),
                    AdjustsFontSizeToFitWidth = true,
                    MinimumFontSize = 8
                };

                lbl_SubTotal = new UILabel()
                {
                    TextAlignment = UITextAlignment.Right,
                    BackgroundColor = UIColor.Clear,
                    Font = UIFont.FromName("Helvetica", 14f),
                    AdjustsFontSizeToFitWidth = true,
                    MinimumFontSize = 8
                };

                lbl_TotalSinDescuento = new UILabel()
                {
                    TextAlignment = UITextAlignment.Right,
                    TextColor = UIColor.LightGray,
                    BackgroundColor = UIColor.Clear,
                    Font = UIFont.FromName("Helvetica", 14f),
                    AdjustsFontSizeToFitWidth = true,
                    MinimumFontSize = 8
                };

                btn_bonifInfo = new UIButton();
                btn_bonifInfo.TintColor = UIColor.FromRGB(0, 122, 255);
                btn_bonifInfo.SetTitleColor(UIColor.FromRGB(0, 122, 255), UIControlState.Normal);
                btn_bonifInfo.SetImage(UIImage.FromFile("btn_Informacion.png"), UIControlState.Normal);
                btn_bonifInfo.TouchUpInside += btn_bonifInfo_TouchUpInside;

                // Se agregan los controladores a la celda.
                ContentView.AddSubviews(new UIView[] { lbl_Nombre, lbl_Cantidad, stp_Cantidad, lbl_Bonificaciones, lbl_Total, 
                            lbl_Descuento, lbl_Impuesto, lbl_SubTotal, lbl_TotalSinDescuento, btn_bonifInfo, lbl_DatoBonificaciones,
                            lbl_DatoDescuento, lbl_DatoImpuesto, lbl_DatoCantidad });
            }

            #endregion

            #region Eventos

            /// <summary>
            /// Evento de cambio de valor del controlador stp_Cantidad, que lleva cuenta de la cantidad del art�culo.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            void stp_Cantidad_ValueChanged(object sender, EventArgs e)
            {
                // Verifica si el cambio en cantidad a pedir del art�culo no ha sido hecho a�n antes de modificarlo,
                // esto ya que se puede modificar la cantidad desde la pantalla DetalleArticuloPedidoTableView.
                if (GestorPedido.carrito[articulo].CantidadAlmacen != Convert.ToDecimal(stp_Cantidad.Value))
                {
                    GestorPedido.carrito[articulo].CantidadAlmacen = Convert.ToDecimal(stp_Cantidad.Value);

                    GestorPedido.AgregarModificarDetalle(GestorPedido.carrito[articulo]);

                    VistaPadre.RefrescarPrecioTotal();
                }
            }

            /// <summary>
            /// Evento de click del bot�n btn_bonifInfo de la celda.
            /// Se usa para mostrar al usuario la infomaci�n del art�culo bonificado si existe una linea bonificada.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            void btn_bonifInfo_TouchUpInside(object sender, EventArgs e)
            {
                string mostrar;
                string cantidad;

                // Se arma un mensaje con la informaci�n del articulo bonificado
                // para mostrarlo al usuario.

                // Se obtiene el art�culo bonificado.
                Articulo articuloBonificado = detalleArticulo.LineaBonificada.Articulo;

                mostrar = articuloBonificado.Codigo + " - " + articuloBonificado.Descripcion;
                if (mostrar.Length > 34)
                    mostrar = mostrar.Substring(0, 34);
                mostrar = mostrar + " - ";
                cantidad = " Cant. Bonifica: " + articuloBonificado.TipoEmpaqueAlmacen + ": " + GestorUtilitario.Formato(detalleArticulo.LineaBonificada.UnidadesAlmacen) + " | " +
                           articuloBonificado.TipoEmpaqueDetalle + ": " + GestorUtilitario.Formato(detalleArticulo.LineaBonificada.UnidadesDetalle);
                mostrar = mostrar + cantidad;

                Toast.MakeToast(VistaPadre.View, mostrar, null, UIColor.Black, true, enumDuracionToast.Mediano);
            }

            /// <summary>
            /// Evento que permite acomodar las dimensiones y posici�n de las Subvistas en la celda.
            /// </summary>
            public override void LayoutSubviews()
            {
                base.LayoutSubviews();
                float posicionY = 0;
                float totalLength = 0;
                float bonifLength = 0;

                // Variable que ayuda para posicionar los controles lo m�s compactos y centrados seg�n el
                // tama�o de la celda y los controladores visibles.
                posicionY = 5 + (string.IsNullOrEmpty(lbl_DatoBonificaciones.Text) ? 15 : 0) +
                    (string.IsNullOrEmpty(lbl_DatoDescuento.Text) ? 15 : 0);

                // Se calcula el tama�o del label de lbl_SubTotal y de sobrepasar el tama�o m�ximo, se le asigna el m�ximo.
                totalLength = (float)lbl_SubTotal.Font.CapHeight * lbl_SubTotal.Text.Length;
                totalLength = totalLength > 125 ? 125 : totalLength;

                if (!string.IsNullOrEmpty(lbl_DatoBonificaciones.Text))
                {
                    // Se calcula el tama�o del label de lbl_DatoBonificaciones y de sobrepasar el tama�o m�ximo, se le asigna el m�ximo.
                    bonifLength = (float)lbl_DatoBonificaciones.Font.CapHeight * lbl_DatoBonificaciones.Text.Length;
                    bonifLength = bonifLength > 150 ? 150 : bonifLength;
                }

                // Se asignan las posiciones y dimensiones de cada controlador.

                lbl_Nombre.Frame = new RectangleF(10, posicionY, (float)ContentView.Bounds.Width - 10, 33);

                posicionY += 33;

                lbl_Cantidad.Frame = new RectangleF(10, posicionY, 100, 33);
                lbl_DatoCantidad.Frame = new RectangleF((float)ContentView.Bounds.Width - (120 + 5 + 110), posicionY, 120, 33);
                stp_Cantidad.Frame = new RectangleF((float)ContentView.Bounds.Width - (100 + 10), posicionY+3, 100, 15);

                posicionY += 33;

                if (!string.IsNullOrEmpty(lbl_DatoBonificaciones.Text))
                {
                    lbl_Bonificaciones.Frame = new RectangleF(10, posicionY, 100, 33);
                    btn_bonifInfo.Frame = new RectangleF(110, posicionY+1, 40, 30);
                    lbl_DatoBonificaciones.Frame = new RectangleF((float)ContentView.Bounds.Width - (bonifLength + 10), posicionY, bonifLength, 33);

                    posicionY += 33;
                }
                else 
                { 
                    lbl_Bonificaciones.Frame = new RectangleF();
                    btn_bonifInfo.Frame = new RectangleF();
                    lbl_DatoBonificaciones.Frame = new RectangleF();
                }

                if (!string.IsNullOrEmpty(lbl_DatoDescuento.Text))
                {
                    lbl_Descuento.Frame = new RectangleF(10, posicionY, 100, 33);
                    lbl_DatoDescuento.Frame = new RectangleF(120, posicionY, (float)ContentView.Bounds.Width - (120 + 10), 33);

                    posicionY += 33;
                }
                else
                {
                    lbl_Descuento.Frame = new RectangleF();
                    lbl_DatoDescuento.Frame = new RectangleF();
                }

                lbl_Impuesto.Frame = new RectangleF(10, posicionY, 100, 33);
                lbl_DatoImpuesto.Frame = new RectangleF((float)ContentView.Bounds.Width - (70 + 10), posicionY, 70, 33);

                posicionY += 33;

                lbl_Total.Frame = new RectangleF(10, posicionY, 50, 33);
                lbl_SubTotal.Frame = new RectangleF((float)ContentView.Bounds.Width - (totalLength + 10), posicionY, totalLength, 33);

                if (!string.IsNullOrEmpty(lbl_DatoDescuento.Text))
                {
                    lbl_TotalSinDescuento.Frame = new RectangleF((float)ContentView.Bounds.Width - ((totalLength * 2) + 10), posicionY, totalLength, 33);
                }
                else
                {
                    lbl_TotalSinDescuento.Frame = new RectangleF();
                }
            }

            #endregion

            #region M�todos

            /// <summary>
            /// M�todo que sirve para actualizar los datos de la celda, llamado al cargar cada celda. 
            /// </summary>
            /// <param name="nombre">Nombre del art�culo.</param>
            /// <param name="cantidad">Cantidad seleccionada del art�culo.</param>
            public void UpdateCell(string nombre, DetallePedido detalle)
            {
                var attributedText = new NSAttributedString();
                string totalLinea = "";
                string impuesto = "";
                decimal descuento = 0;
                decimal impuestoValor = 0;
                string bonificaciones = "";

                detalleArticulo = detalle;

                // Si el detalle tiene una linea bonificada, se arma un mensaje con la informaci�n del articulo bonificado
                // para mostrarlo al usuario.
                if (detalleArticulo.LineaBonificada != null)
                {
                    bonificaciones = "Alm: " +
                        GestorUtilitario.FormatDecimalAlmDet(detalleArticulo.LineaBonificada.UnidadesAlmacen,
                        2) + " | Det: " +
                        GestorUtilitario.FormatDecimalAlmDet(detalleArticulo.LineaBonificada.UnidadesDetalle, 2);

                    lbl_DatoBonificaciones.Text = bonificaciones;
                }
                else
                {
                    lbl_DatoBonificaciones.Text = "";
                }

                // Se obtiene el descuento del detalle.
                if (detalleArticulo.Descuento != null)
                {
                    descuento = detalleArticulo.CalcularPorcentajeDescuento();
                }

                // Se asigna el valor de cantidad al Controlador para que lleve la cuenta correcta,
                // tener en cuenta la conversion por perdida de decimales.
                stp_Cantidad.Value = Convert.ToDouble(detalle.UnidadesAlmacen);

                lbl_Nombre.Text = nombre;

                lbl_DatoCantidad.Text = GestorUtilitario.FormatDecimalAlmDet(detalle.TotalAlmacen, 4);

                if (descuento != 0)
                {
                    if (VistaPadre.dolar)
                    {
                        lbl_DatoDescuento.Text = GestorUtilitario.FormatPorcentaje(descuento) + " (" +
                            GestorUtilitario.FormatNumeroDolar(detalleArticulo.MontoDescuento) + ")";
                    }
                    else
                    {
                        lbl_DatoDescuento.Text = GestorUtilitario.FormatPorcentaje(descuento) + " (" +
                            GestorUtilitario.FormatNumero(detalleArticulo.MontoDescuento, VistaPadre.simboloMonetarioPedido) + ")";
                    }
                }
                else
                {
                    lbl_DatoDescuento.Text = "";
                }

                impuestoValor = detalleArticulo.Impuesto.MontoTotal;

                if (VistaPadre.dolar)
                {
                    totalLinea = GestorUtilitario.FormatNumeroDolar(detalleArticulo.MontoNeto);

                    attributedText = new NSAttributedString(GestorUtilitario.FormatNumeroDolar(detalleArticulo.MontoTotal +
                        impuestoValor), strikethroughStyle: NSUnderlineStyle.Single);

                    impuesto = GestorUtilitario.FormatNumeroDolar(impuestoValor);
                }
                else
                {
                    totalLinea = GestorUtilitario.FormatNumero(detalleArticulo.MontoNeto, VistaPadre.simboloMonetarioPedido);

                    attributedText = new NSAttributedString(GestorUtilitario.FormatNumero(detalleArticulo.MontoTotal, VistaPadre.simboloMonetarioPedido +
                        impuestoValor), strikethroughStyle: NSUnderlineStyle.Single);

                    impuesto = GestorUtilitario.FormatNumero(impuestoValor, VistaPadre.simboloMonetarioPedido);
                }

                lbl_DatoImpuesto.Text = impuesto;

                lbl_SubTotal.Text = totalLinea;

                if (detalleArticulo.MontoNeto != (detalleArticulo.MontoTotal + detalleArticulo.Impuesto.MontoTotal))
                {
                    lbl_TotalSinDescuento.AttributedText = attributedText;
                }
                else
                {
                    lbl_TotalSinDescuento.Text = "";
                }
            }

            #endregion
        }


        #endregion

        #region Eventos

        /// <summary>
        /// Evento que se ejecuta al iniciar la navegaci�n de una pantalla a otra usando un Segue.
        /// Se usa para inicializar y pasar variables a la clase destino, solo se usa cuando se configur�
        /// una navegaci�n usando el dise�ador por medio de un Segue.
        /// </summary>
        /// <param name="segue"></param>
        /// <param name="sender"></param>
        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);

            // Se obtiene una referencia de la clase destino especificando como castearla.
            var articulosView = segue.DestinationViewController as ArticulosVista;

            // Si la referencia no es nula, se asignan las variables necesarias.
            if (articulosView != null)
            {
                articulosView.desdeCarrito = true;
            }
        }

        /// <summary>
        /// Evento de click del bot�n btn_Agregar.
        /// Se usa para asignar que se est� llamando a la pantalla de ArticulosVista desde la creaci�n de un pedido
        /// antes de moverse a dicha pantalla por medio del Segue.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_Agregar_TouchUpInside(UIButton sender)
        {
            GestorPedido.agregandoArticulos = true;
        }

        /// <summary>
        /// Evento de click del bot�n btn_Cliente.
        /// Se usa para mostrar un mensaje con la informaci�n del cliente al que se le realiza el pedido.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_Cliente_TouchUpInside(UIButton sender)
        {
            Toast.MakeToast(View, "Cliente: " + GlobalUI.ClienteActual.Nombre + " - C�digo: " + GlobalUI.ClienteActual.Codigo + 
                " - Compa��a: " + GlobalUI.ClienteActual.Compania + ".", null, UIColor.Black, true, enumDuracionToast.Mediano);
        }

        /// <summary>
        /// Evento de click del bot�n btn_Vaciar.
        /// Se usa para remover del carrito todos los art�culos en el pedido.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_Vaciar_TouchUpInside(UIButton sender)
        {
            if (GestorPedido.carrito.Count > 0)
            {
                UIAlertView alert = new UIAlertView("Limpiar pedido", "Va a eliminar todos los art�culos del pedido.", null, "Limpiar", "Cancelar");
                alert.Clicked += (s, b) =>
                {
                    if (b.ButtonIndex == 0)
                    {
                        // Uno a uno saca del pedido y del carrito cada art�culo realizando los c�lculos requeridos.
                        while (articulos.Count > 0)
                        {
                            GestorPedido.RetirarDetalle(articulos[0]);
                            articulos.RemoveAt(0);
                        }

                        RefrescarPrecioTotal();

                        if (articulos.Count == 0)
                        {
                            NavigationItem.SetHidesBackButton(false, false);
                        }

                        Toast.MakeToast(View, "Art�culos removidos del carrito.", null, verdeOscuro, true, enumDuracionToast.Corto);
                    }

                };
                alert.Show();
            }
            else
            {
                Toast.MakeToast(View, "No hay art�culos en el carrito.", null, UIColor.Black, true, enumDuracionToast.Corto);
            }
        }

        /// <summary>
        /// Evento de click del bot�n btn_Ordenar.
        /// Se usa para moverse a la pantalla de DetallePedidoTableView que es la pantalla final del pedido.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_Ordenar_TouchUpInside(UIButton sender)
        {
            // Valida que el carrito no est� vacio antes de continuar, de lo contrario retorna un mensaje de advertencia al usuario.
            if (articulos.Count > 0)
            {
                // Se referencia al Storyboard del app.
                var myStoryboard = AppDelegate.Storyboard;

                // Se instancia un ViewController con el ID del Storyboard y su clase respectiva.
                var detallePedidoTableView = myStoryboard.InstantiateViewController("DetallePedidoTableView") as DetallePedidoTableView;

                // Se asignan los valores que se desean pasar a la nueva pantalla.
                detallePedidoTableView.pedidosView = pedidosView;

                // Se navega a la pantalla instanciada agregandola al stack.
                NavigationController.PushViewController(detallePedidoTableView, true);
            }
            else
            {
                new UIAlertView("No hay art�culos", "Primero ingrese art�culos al pedido para poder continuar.", null, "OK", null).Show();
            }
        }

        #endregion

        #region Metodos

        /// <summary>
        /// M�todo que se utiliza para mostrar el monto total del pedido actual en pantalla.
        /// </summary>
        public void RefrescarPrecioTotal()
        {
            Pedido pedido = GestorPedido.PedidosCollection.Buscar(GlobalUI.ClienteActual.Compania);

            if (pedido != null)
            {
                // -------------------------------------------------------------------------------------------------------
                // Estas dos variables se crearon para probar si es posible obtener la cantidad de articulos y la cantidad
                // de l�neas en el pedido para luego hacer controles visuales donde mostrar estos datos.
                var totalArticulos = pedido.Detalles.TotalArticulos;
                    var totalLineas = pedido.Detalles.TotalLineas;
                // -------------------------------------------------------------------------------------------------------

                #region Calcular impuestos de Ventas

                foreach (Pedido Pedido in GestorPedido.PedidosCollection.Gestionados)
                {
                    Pedido.RecalcularImpuestos(Pedido.MontoSubTotal < Pedido.Configuracion.Compania.MontoMinimoExcento);
                }

                GestorPedido.PedidosCollection.SacarMontosTotales();

                #endregion Calcular impuestos de Ventas

                decimal montoBrutoBon = pedido.MontoBrutoBonificaciones;
                decimal montoNetoRedondeo = 0;
                decimal diferenciaDescuento1 = 0;
                bool redondearFactura = pedido.Configuracion.Compania.UsaRedondeo && pedido.Configuracion.Compania.FactorRedondeo > 0;

                if (redondearFactura)
                {
                    montoNetoRedondeo = GestorUtilitario.TrunqueAFactor(pedido.MontoNeto, pedido.Configuracion.Compania.FactorRedondeo);
                    diferenciaDescuento1 = pedido.MontoNeto - montoNetoRedondeo;
                    diferenciaDescuento1 = Math.Abs(diferenciaDescuento1);
                }

                if (dolar)
                {
                    if (redondearFactura)
                        lbl_DatoTotal.Text = GestorUtilitario.FormatNumeroDolar(montoNetoRedondeo);
                    else
                        lbl_DatoTotal.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalNeto);
                }
                else
                {
                    if (redondearFactura)
                        lbl_DatoTotal.Text = GestorUtilitario.FormatNumero(montoNetoRedondeo, simboloMonetarioPedido);
                    else
                        lbl_DatoTotal.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalNeto, simboloMonetarioPedido);
                }
            }
            else
            {
                if (dolar)
                {
                    lbl_DatoTotal.Text = GestorUtilitario.FormatNumeroDolar(0);
                }
                else
                {
                    lbl_DatoTotal.Text = GestorUtilitario.FormatNumero(0, simboloMonetarioPedido);
                }
            }

            TableView.ReloadData();
        }

        #endregion

        #region View lifecycle

        /// <summary>
        /// M�todo para liberaci�n de memoria de la pantalla cuando se cierra.
        /// </summary>
        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        /// <summary>
        /// M�todo que se ejecuta al crear la pantalla, se ejecuta una �nica vez a menos que la pantalla se remueva del stack.
        /// </summary>
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Inicializar las variables o asignar los datos que se utilizan en la pantalla.

            dolar = GlobalUI.ClienteActual.ClienteCompania[0].Moneda == TipoMoneda.DOLAR;

            // Se hace referencia al controlador de la pantalla.
            VistaPadre = this;

            if (GestorPedido.carrito == null)
            {
                GestorPedido.carrito = new List<Articulo>();
            }

            articulos = GestorPedido.carrito;

            TableView.Source = new TableSource();
            TableView.Editing = false;

            this.NavigationController.NavigationBar.TintColor = UIColor.FromRGB(225, 106, 12);

            // Agregar en la barra de Navegaci�n un bot�n para cancelar el pedido.
            this.NavigationItem.SetRightBarButtonItem(
                new UIBarButtonItem(UIImage.FromFile("btn_Cancelar.png")
                , UIBarButtonItemStyle.Plain
                , (sender, args) =>
                {
                    // Evento de click del bot�n de cancelar pedido.

                    UIAlertView alert = new UIAlertView("Cancelar pedido", "Va a salir de la gesti�n del pedido.", null, "Salir", "Continuar");
                    alert.Clicked += (s, b) =>
                    {
                        if (b.ButtonIndex == 0)
                        {
                            // Limpia y reinicia los datos del pedido global.
                            PedidosCollection.FacturarPedido = false;
                            GestorPedido.PedidosCollection = new PedidosCollection();
                            Pedido.BorrarXMLPedido(GlobalUI.ClienteActual.Codigo);

                            // Limpia el carrito de compras.
                            GestorPedido.carrito = null;

                            // Limpia al cliente del pedido.
                            GlobalUI.ClienteActual = null;
                            GlobalUI.RutaActual = null;
                            GlobalUI.RutasActuales = null;

                            // Asigna que no se est� creando una orden.
                            PedidosView.crearOrden = false;

                            // Devuelve al usuario a la pantalla inicial de la secci�n de pedidos.
                            InvokeOnMainThread(() => { NavigationController.PopToViewController(pedidosView, true); });
                        }

                    };
                    alert.Show();
                })
            , true);

            Toast.MakeToast(View, "Agregue art�culos al carrito para crear el pedido.", null, UIColor.Black, true, enumDuracionToast.Corto);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a aparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            // Se refresca la informaci�n del controlador de la pantalla.
            VistaPadre = this;

            // Inicializar el carrito si se limpi� o a�n no se ha creado.
            if (GestorPedido.carrito == null)
            {
                GestorPedido.carrito = new List<Articulo>();
            }

            // Obtener los art�culos en el carrito para refrescar la tabla de carrito.
            articulos = GestorPedido.carrito;

            var compania = Compania.Obtener(GlobalUI.ClienteActual.Compania);

            if (compania != null)
            {
                simboloMonetarioPedido = compania.SimboloMonetarioFunc;
            }

            RefrescarPrecioTotal();
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla apareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            if (articulos.Count == 0)
            {
                NavigationItem.SetHidesBackButton(false, false);

                Toast.MakeToast(View, "Agregue art�culos al carrito para crear el pedido.", null, UIColor.Black, true, enumDuracionToast.Corto);
            }
            else
            {
                NavigationItem.SetHidesBackButton(true, false);
            }

            // Se inicia el proceso de intentar sincronizar pedidos pendientes.
            AppDelegate.IniciarSincronizacionPedidos(this.ParentViewController.ParentViewController);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a desaparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillDisappear(bool animated)
        {
            // Valida si la pantalla actual no existe en el stack de navegaci�n (Se presion� el boton de Atr�s)
            // y si a�n no ha sido cancelado el pedido.
            if (!NavigationController.ViewControllers.Contains(this) && PedidosView.crearOrden)
            {
                // Se limpia toda la informaci�n del pedido dado a que se devolvio a la seleccion de clientes.

                // Limpia y reinicia los datos del pedido global.
                PedidosCollection.FacturarPedido = false;
                GestorPedido.PedidosCollection = new PedidosCollection();
                Pedido.BorrarXMLPedido(GlobalUI.ClienteActual.Codigo);

                // Limpia el carrito de compras.
                GestorPedido.carrito = null;

                // Limpia al cliente del pedido.
                GlobalUI.ClienteActual = null;
                GlobalUI.RutaActual = null;
                GlobalUI.RutasActuales = null;

                // Se valida como se inicio el pedido.
                if (pedidosView.pedidoDesdeClientes)
                {
                    // Asigna que no se est� creando una orden para poder regresar a detalle del cliente.
                    PedidosView.crearOrden = false;
                }
                else
                {
                    // Asigna que se est� creando una orden para poder seleccionar otro cliente.
                    PedidosView.crearOrden = true;
                }
            }

            base.ViewWillDisappear(animated);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla desapareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
	}
}
