// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Softland.iOS.Pem
{
	[Register ("DetallePedidoTableView")]
	partial class DetallePedidoTableView
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_FechaEntrega { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_Ordenar { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Consecutivo { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Consumo { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoConsumo { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoDescuento1 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoDescuento2 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoDescuentoDetalle { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoDescuentoVolumen { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoFecha { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoHora { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoTotalBruto { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoTotalNeto { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoVenta { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Descuento1 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Descuento2 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DescuentoDetalles { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DescuentoVolumen { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Entrega { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Fecha { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Hora { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_TotalBruto { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_TotalNeto { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Ventas { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField txt_DatoConsecutivo { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField txt_DatoDescuento1 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField txt_DatoDescuento2 { get; set; }

		[Action ("btn_Ordenar_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_Ordenar_TouchUpInside (UIButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (btn_FechaEntrega != null) {
				btn_FechaEntrega.Dispose ();
				btn_FechaEntrega = null;
			}
			if (btn_Ordenar != null) {
				btn_Ordenar.Dispose ();
				btn_Ordenar = null;
			}
			if (lbl_Consecutivo != null) {
				lbl_Consecutivo.Dispose ();
				lbl_Consecutivo = null;
			}
			if (lbl_Consumo != null) {
				lbl_Consumo.Dispose ();
				lbl_Consumo = null;
			}
			if (lbl_DatoConsumo != null) {
				lbl_DatoConsumo.Dispose ();
				lbl_DatoConsumo = null;
			}
			if (lbl_DatoDescuento1 != null) {
				lbl_DatoDescuento1.Dispose ();
				lbl_DatoDescuento1 = null;
			}
			if (lbl_DatoDescuento2 != null) {
				lbl_DatoDescuento2.Dispose ();
				lbl_DatoDescuento2 = null;
			}
			if (lbl_DatoDescuentoDetalle != null) {
				lbl_DatoDescuentoDetalle.Dispose ();
				lbl_DatoDescuentoDetalle = null;
			}
			if (lbl_DatoDescuentoVolumen != null) {
				lbl_DatoDescuentoVolumen.Dispose ();
				lbl_DatoDescuentoVolumen = null;
			}
			if (lbl_DatoFecha != null) {
				lbl_DatoFecha.Dispose ();
				lbl_DatoFecha = null;
			}
			if (lbl_DatoHora != null) {
				lbl_DatoHora.Dispose ();
				lbl_DatoHora = null;
			}
			if (lbl_DatoTotalBruto != null) {
				lbl_DatoTotalBruto.Dispose ();
				lbl_DatoTotalBruto = null;
			}
			if (lbl_DatoTotalNeto != null) {
				lbl_DatoTotalNeto.Dispose ();
				lbl_DatoTotalNeto = null;
			}
			if (lbl_DatoVenta != null) {
				lbl_DatoVenta.Dispose ();
				lbl_DatoVenta = null;
			}
			if (lbl_Descuento1 != null) {
				lbl_Descuento1.Dispose ();
				lbl_Descuento1 = null;
			}
			if (lbl_Descuento2 != null) {
				lbl_Descuento2.Dispose ();
				lbl_Descuento2 = null;
			}
			if (lbl_DescuentoDetalles != null) {
				lbl_DescuentoDetalles.Dispose ();
				lbl_DescuentoDetalles = null;
			}
			if (lbl_DescuentoVolumen != null) {
				lbl_DescuentoVolumen.Dispose ();
				lbl_DescuentoVolumen = null;
			}
			if (lbl_Entrega != null) {
				lbl_Entrega.Dispose ();
				lbl_Entrega = null;
			}
			if (lbl_Fecha != null) {
				lbl_Fecha.Dispose ();
				lbl_Fecha = null;
			}
			if (lbl_Hora != null) {
				lbl_Hora.Dispose ();
				lbl_Hora = null;
			}
			if (lbl_TotalBruto != null) {
				lbl_TotalBruto.Dispose ();
				lbl_TotalBruto = null;
			}
			if (lbl_TotalNeto != null) {
				lbl_TotalNeto.Dispose ();
				lbl_TotalNeto = null;
			}
			if (lbl_Ventas != null) {
				lbl_Ventas.Dispose ();
				lbl_Ventas = null;
			}
			if (txt_DatoConsecutivo != null) {
				txt_DatoConsecutivo.Dispose ();
				txt_DatoConsecutivo = null;
			}
			if (txt_DatoDescuento1 != null) {
				txt_DatoDescuento1.Dispose ();
				txt_DatoDescuento1 = null;
			}
			if (txt_DatoDescuento2 != null) {
				txt_DatoDescuento2.Dispose ();
				txt_DatoDescuento2 = null;
			}
		}
	}
}
