using Foundation;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Drawing;

using Pedidos.Core;
using BI.Shared;

using UIKit;
using BI.iOS;
using System.Data.SQLite;
using System.Text;
using System.Data.SQLiteBase;
using System.Data;
using System.Globalization;

namespace Softland.iOS.Pem
{
	partial class DetallePedidoTableView : UITableViewController
    {

        #region Propiedades y Contructor de instancia

        /// <summary>
        /// Controlador que sirve para mostrar una pantalla de espera o de carga.
        /// </summary>
        private static LoadingOverlay loadingOverlay;

        /// <summary>
        /// Referencia a PedidosView para tener acceso a sus variables antes de devolverse a esa pantalla.
        /// </summary>
        public PedidosView pedidosView;

        /// <summary>
        /// Lista de consecutivos disponibles para seleccionar como consecutivo de los pedido.
        /// </summary>
        public List<string> consecutivos;

        /// <summary>
        /// Variable que almacena el valor del consecutivo seleccionada.
        /// </summary>
        public string Consec = "";

        /// <summary>
        /// Variable que almacena la fecha seleccionada por el usuario para la Entrega del pedido.
        /// </summary>
        public string fechaSeleccionada = "";

        /// <summary>
        /// Variable que almacena el mensaje de error que retornan los m�todos o eventos.
        /// </summary>
        static string Error = string.Empty;

        /// <summary>
        /// Variable que almacena si se debe mostrar la opci�n para imprimir el pedido o no.
        /// </summary>
        public bool Imprimir;

        /// <summary>
        /// Variable que almacena si la moneda del cliente es el dolar.
        /// </summary>
        public bool dolar;

        /// <summary>
        /// Variable que almacena el S�mbolo Monetario de la compa��a del cliente actual del pedido.
        /// </summary>
        public string simboloMonetarioPedido = string.Empty;

        /// <summary>
        /// Costructor por defecto.
        /// </summary>
        /// <param name="handle"></param>
		public DetallePedidoTableView (IntPtr handle) : base (handle)
		{
		}

        #endregion

        #region Eventos

        /// <summary>
        /// Evento de click del bot�n btn_FechaEntrega.
        /// Se usa para que el usuario pueda elegir la fecha de entrega del pedido, despliega una pantalla para elegir 
        /// una fecha.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void btn_FechaEntrega_TouchUpInside(object sender, EventArgs e)
        {
            // Controlador que mostrar� el selector de fecha en pantalla.
            var modalPicker = new ModalPickerViewController(ModalPickerType.Date, "Seleccione una fecha:", this)
            {
                HeaderBackgroundColor = UIColor.DarkGray,
                InternalBackgroundColor = UIColor.White,
                HeaderTextColor = UIColor.White,
                TransitioningDelegate = new ModalPickerTransitionDelegate(),
                ModalPresentationStyle = UIModalPresentationStyle.Custom
            };

            // Tipo de selector a mostrar.
            modalPicker.DatePicker.Mode = UIDatePickerMode.Date;

            // Evento en que la pantalla del controlador desaparece.
            modalPicker.OnModalPickerDismissed += (s, ea) =>
            {
                var dateFormatter = new NSDateFormatter()
                {
                    DateFormat = "dd/MM/yyyy"
                };

                fechaSeleccionada = dateFormatter.ToString(modalPicker.DatePicker.Date);

                btn_FechaEntrega.SetTitle(fechaSeleccionada, UIControlState.Normal);

                btn_Ordenar.Enabled = true;
            };

            btn_Ordenar.Enabled = false;

            // Mostrar la pantalla.
            await PresentViewControllerAsync(modalPicker, true);
        }

        /// <summary>
        /// Evento de click del bot�n btn_Ordenar.
        /// Se usa para iniciar el proceso de validaci�n del pedido y su eventual almacenamiento en la base de datos del 
        /// dispositivo.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_Ordenar_TouchUpInside(UIButton sender)
        {
            // Si la fecha de entrega no esta vacia.
            if (!string.IsNullOrEmpty(fechaSeleccionada))
            {
                InvokeOnMainThread(() =>
                {
                    try
                    {
                        // Se inicia la validaci�n del pedido.
                        ValidarDocumento();
                    }
                    catch (Exception e)
                    {
                        if (loadingOverlay != null)
                        {
                            loadingOverlay.Hide();
                        }

                        new UIAlertView("Proceso interrumpido", "No se ha podido guardar el pedido.\n" + e.Message, null, "OK", null).Show();
                    }
                });
            }
            else
            {
                // Notificar que debe seleccionarse una fecha.

                new UIAlertView("Faltan datos", "Debe seleccionar una fecha de entrega para el pedido.", null, "OK", null).Show();
            }
        }

        /// <summary>
        /// Evento de cambio valor del campo de texto txt_DatoDescuento2.
        /// Se usa para validar que no se pueda ingresar una cifra mayor a 100 ni menor a 0, adem�s de que sean solo n�meros.
        /// </summary>
        private void txt_DatoDescuento2_ValueChanged()
        {
            try
            {
                decimal desc2 = Convert.ToDecimal(txt_DatoDescuento2.Text.Replace(".", ","));

                if (desc2 > 100)
                {
                    txt_DatoDescuento2.Text = "100";
                }
                else
                {
                    if (desc2 < 0)
                    {
                        txt_DatoDescuento2.Text = "0";
                    }
                }
            }
            catch
            {
                // Si la conversi�n a int fall�, indica que se ingres� alg�n caracter no num�rico.
                if (txt_DatoDescuento2.Text.Length - 1 >= 0)
                {
                    txt_DatoDescuento2.Text = txt_DatoDescuento2.Text.Substring(0, txt_DatoDescuento2.Text.Length - 1);
                }
            }
        }

        /// <summary>
        /// Evento de inicio de edici�n del campo de texto txt_DatoDescuento2.
        /// Se usa para deshabilitar el bot�n de ordenar para evitar asi que se den problemas con el manejo de controles
        /// mostrados en pantalla.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void txt_DatoDescuento2_EditingDidBegin(object sender, EventArgs e)
        {
            btn_Ordenar.Enabled = false;
        }

        #endregion

        #region M�todos

        /// <summary>
        /// M�todo que sirve para mostrar la informaci�n del pedido en pantalla.
        /// Llamado cada vez que se hace alg�n cambio a los datos del pedido para refrescar la informaci�n.
        /// </summary>
        public void RefrescarDatosPedido()
        {
            Pedido pedido = GestorPedido.PedidosCollection.Buscar(GlobalUI.ClienteActual.Compania);
            
            // Si se pudo obtener la referencia al pedido.
            if (pedido != null)
            {
                foreach (Pedido Pedido in GestorPedido.PedidosCollection.Gestionados)
                {
                    Pedido.RecalcularImpuestos(Pedido.MontoSubTotal < Pedido.Configuracion.Compania.MontoMinimoExcento);
                }
                GestorPedido.PedidosCollection.SacarMontosTotales();

                decimal montoBrutoBon = pedido.MontoBrutoBonificaciones;
                decimal montoNetoRedondeo = 0;
                decimal diferenciaDescuento1 = 0;
                bool redondearFactura = pedido.Configuracion.Compania.UsaRedondeo && pedido.Configuracion.Compania.FactorRedondeo > 0;
                
                if (redondearFactura)
                {
                    montoNetoRedondeo = GestorUtilitario.TrunqueAFactor(pedido.MontoNeto, pedido.Configuracion.Compania.FactorRedondeo);
                    diferenciaDescuento1 = pedido.MontoNeto - montoNetoRedondeo;
                    diferenciaDescuento1 = Math.Abs(diferenciaDescuento1);
                }

                // Se muestran los precios seg�n la moneda del cliente.
                if (dolar)
                {
                    if (redondearFactura)
                        lbl_DatoTotalNeto.Text = GestorUtilitario.FormatNumeroDolar(montoNetoRedondeo);
                    else
                        lbl_DatoTotalNeto.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalNeto);

                    lbl_DatoTotalBruto.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalBruto + montoBrutoBon);

                    lbl_DatoVenta.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalImpuesto1);
                    lbl_DatoConsumo.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalImpuesto2);

                    lbl_DatoDescuentoDetalle.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalDescuentoLineas);
                    lbl_DatoDescuentoVolumen.Text = GestorUtilitario.FormatNumeroDolar(montoBrutoBon);


                    if (redondearFactura)
                        lbl_DatoDescuento1.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalDescuento1 + diferenciaDescuento1);
                    else
                        lbl_DatoDescuento1.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalDescuento1);
                    lbl_DatoDescuento2.Text = GestorUtilitario.FormatNumeroDolar(GestorPedido.PedidosCollection.TotalDescuento2);
                }
                else
                {
                    if (redondearFactura)
                        lbl_DatoTotalNeto.Text = GestorUtilitario.FormatNumero(montoNetoRedondeo, simboloMonetarioPedido);
                    else
                        lbl_DatoTotalNeto.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalNeto, simboloMonetarioPedido);

                    lbl_DatoTotalBruto.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalBruto + montoBrutoBon, simboloMonetarioPedido);

                    lbl_DatoVenta.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalImpuesto1, simboloMonetarioPedido);
                    lbl_DatoConsumo.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalImpuesto2, simboloMonetarioPedido);

                    lbl_DatoDescuentoDetalle.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalDescuentoLineas, simboloMonetarioPedido);
                    lbl_DatoDescuentoVolumen.Text = GestorUtilitario.FormatNumero(montoBrutoBon, simboloMonetarioPedido);

                    if (redondearFactura)
                        lbl_DatoDescuento1.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalDescuento1 + diferenciaDescuento1, simboloMonetarioPedido);
                    else
                        lbl_DatoDescuento1.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalDescuento1, simboloMonetarioPedido);
                    lbl_DatoDescuento2.Text = GestorUtilitario.FormatNumero(GestorPedido.PedidosCollection.TotalDescuento2, simboloMonetarioPedido);
                }

                txt_DatoDescuento1.Text = GestorUtilitario.FormatPorcentaje(GestorPedido.PedidosCollection.PorcDesc1);
                txt_DatoDescuento2.Text = GestorUtilitario.FormatPorcentaje(GestorPedido.PedidosCollection.PorcDesc2);
                
                txt_DatoDescuento2.Enabled = AppDelegate.CambiarDescuento;

                // Se muestra el nombre de Imp. de ventas y consumo seg�n la configuraci�n del cliente.
                lbl_Ventas.Text = pedido.Configuracion.Compania.Impuesto1Descripcion != ""
                    ? pedido.Configuracion.Compania.Impuesto1Descripcion + ":" : "Imp. Ventas:";
                lbl_Consumo.Text = pedido.Configuracion.Compania.Impuesto2Descripcion != ""
                    ? pedido.Configuracion.Compania.Impuesto2Descripcion + ":" : "Imp. Consumo:";
            }
            else
            {
                // En caso de que no se pudiera obtener el pedido se muestran valores por defecto.

                if (dolar)
                {
                    lbl_DatoTotalNeto.Text = GestorUtilitario.FormatNumeroDolar(0);

                    lbl_DatoTotalBruto.Text = GestorUtilitario.FormatNumeroDolar(0);
                    lbl_DatoDescuentoDetalle.Text = GestorUtilitario.FormatNumeroDolar(0);
                    lbl_DatoDescuentoVolumen.Text = GestorUtilitario.FormatNumeroDolar(0);

                    lbl_DatoDescuento1.Text = GestorUtilitario.FormatNumeroDolar(0);

                    lbl_DatoDescuento2.Text = GestorUtilitario.FormatNumeroDolar(0);

                    lbl_DatoVenta.Text = GestorUtilitario.FormatNumeroDolar(0);
                    lbl_DatoConsumo.Text = GestorUtilitario.FormatNumeroDolar(0);
                }
                else
                {
                    lbl_DatoTotalNeto.Text = GestorUtilitario.FormatNumero(0, simboloMonetarioPedido);

                    lbl_DatoTotalBruto.Text = GestorUtilitario.FormatNumero(0, simboloMonetarioPedido);
                    lbl_DatoDescuentoDetalle.Text = GestorUtilitario.FormatNumero(0, simboloMonetarioPedido);
                    lbl_DatoDescuentoVolumen.Text = GestorUtilitario.FormatNumero(0, simboloMonetarioPedido);

                    lbl_DatoDescuento1.Text = GestorUtilitario.FormatNumero(0, simboloMonetarioPedido);

                    lbl_DatoDescuento2.Text = GestorUtilitario.FormatNumero(0, simboloMonetarioPedido);

                    lbl_DatoVenta.Text = GestorUtilitario.FormatNumero(0, simboloMonetarioPedido);
                    lbl_DatoConsumo.Text = GestorUtilitario.FormatNumero(0, simboloMonetarioPedido);
                }

                txt_DatoDescuento1.Text = GestorUtilitario.FormatPorcentaje(0);
                txt_DatoDescuento2.Text = GestorUtilitario.FormatPorcentaje(0);
            }

            // Se muestra la hora actual del dispositivo.
            lbl_DatoHora.Text = GestorUtilitario.ObtenerHoraActual();
            // Se muestra la fecha actual del dispositivo.
            lbl_DatoFecha.Text = GestorUtilitario.ObtenerFechaActual();

            TableView.ReloadData();
        }


        /// <summary>
        /// M�todo que configura el campo de texto txt_DatoCompaniaDefault para ser usado como un controlador de selecci�n m�ltiple.
        /// Se crea un controlador de selecci�n m�ltiple que se mostrar� en pantalla una vez que el campo de texto 
        /// sea seleccionado y cuando la seleccion termine se le asignar� el valor elegido al campo de texto.
        /// </summary>
        private void configurarPickerConsecutivo()
        {
            // Se inicializa el modelo a utilizar por el campo de texto.
            PickerModel model = new PickerModel(consecutivos);

            // Evento de cambio de valor del selector.
            model.PickerChanged += (sender, e) =>
            {
                PickerChangedEventArgs obj = (PickerChangedEventArgs)e;

                // Se asigna la compa�ia seleccionada.
                Consec = obj.SelectedValue;
            };

            // Se inicializa el selector m�ltiple a utilizar por el campo de texto.
            UIPickerView picker = new UIPickerView();
            picker.BackgroundColor = UIColor.White;
            picker.TintColor = UIColor.White;
            picker.ShowSelectionIndicator = true;
            picker.Model = model;

            // Se crea una barra para colocar el bot�n de listo en el teclado que abre el control txt_DatoCompania.
            UIToolbar toolbar = new UIToolbar(new RectangleF(0.0f, 0.0f, (float)this.View.Frame.Size.Width, 44.0f));
            toolbar.TintColor = UIColor.White;
            toolbar.BarStyle = UIBarStyle.Black;
            toolbar.Translucent = true;

            // Se crea un bot�n para usarse como T�tulo de la barra y se deshabilita para que no tenga funcionalidad.
            UIBarButtonItem TitleButton;
            TitleButton = new UIBarButtonItem("Seleccione un consecutivo:", UIBarButtonItemStyle.Plain, (s, e) => { });
            TitleButton.Enabled = false;

            // Se crean los botones de la barra y se asignan los Eventos de los mismos.
            toolbar.Items = new UIBarButtonItem[]{
                TitleButton,
                new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                new UIBarButtonItem(UIBarButtonSystemItem.Done, delegate {
                    // Se asigna al campo de texto la compa��a seleccionada por el usuario y se quita del Foco de atenci�n.
                    txt_DatoConsecutivo.Text = Consec;
                    txt_DatoConsecutivo.ResignFirstResponder();
                    
                    // Se asigna y guarda el nuevo consecutivo seleccionado por el usuario.
                    if (!AppDelegate.Consecutivo.Equals(Consec)) 
                    { 
                        AppDelegate.Consecutivo = Consec;

                        AppDelegate.SetConfigVar(AppDelegate.Consecutivo, "consecutivo");

                        // Validar para el usuario si seleccion� vac�o como Consecutivo.
                        if (!string.IsNullOrEmpty(AppDelegate.Consecutivo))
                        {
                            // Se limpia la marca de color en el campo de texto si la tuviera.
                            txt_DatoConsecutivo.BackgroundColor = UIColor.Clear;
                            txt_DatoConsecutivo.TextColor = UIColor.Black;
                        }
                        else
                        {
                            Toast.MakeToast(ParentViewController.View, "No puede completar el pedido sin antes seleccionar un consecutivo para obtener el n�mero de pedido."
                                , null, UIColor.Black, true, enumDuracionToast.Mediano);

                            // Se marca el campo de texto de color rojo para indicar donde est� el error.
                            txt_DatoConsecutivo.BackgroundColor = UIColor.Red;
                        }
                    }
                })
            };

            // Se le asigna al campo de texto el control de selecci�n m�ltiple en lugar de usar un teclado.
            txt_DatoConsecutivo.InputView = picker;

            // Se le asigna al campo de texto la barra a mostrar sobre el teclado.
            txt_DatoConsecutivo.InputAccessoryView = toolbar;
        }

        /// <summary>
        /// M�todo que sirve para refrescar en pantalla la informaci�n de datos generales a mostrar.
        /// Se usa tambien para validar que la compa��a general seleccionada est� entre la lista de compa��as disponibles.
        /// </summary>
        private void obtenerConsecutivos()
        {
            consecutivos = new List<string>();
            //consecutivos.Add("");

            // Se agrega el c�digo de los consecutivos disponibles en la compa��a.
            consecutivos.AddRange(ParametroSistema.Consecutivos.FindAll(x => x.Compania == GlobalUI.ClienteActual.Compania)
                .ConvertAll(x => x.CodigoNumeroPedido));

            // Se verifica que el consecutivo est� en la lista.
            if (consecutivos.Contains(AppDelegate.Consecutivo))
            {
                txt_DatoConsecutivo.Text = AppDelegate.Consecutivo;
                Consec = AppDelegate.Consecutivo;
            }
            else
            {
                // Si no se encuentra en la lista se reinicia la variable compa��a general y consecutivo y las variables en NSUserDefaults.

                if (consecutivos.Count > 0)
                {
                    AppDelegate.Consecutivo = consecutivos[0];

                    AppDelegate.SetConfigVar(AppDelegate.Consecutivo, "consecutivo");

                    txt_DatoConsecutivo.Text = consecutivos[0];

                    Consec = consecutivos[0];
                }
            }

            // Se llama a configurar el campo de texto txt_DatoConsecutivo para usarlo como selecci�n m�ltiple.
            configurarPickerConsecutivo();

            this.ReloadInputViews();
        }

        /// <summary>
        /// Valida que la informaci�n del pedido sea v�lida, de lo contrario cancela el proceso y notifica al usuario.
        /// </summary>
        private void ValidarDocumento()
        {
            try
            {
                Error = string.Empty;

                if (!ValidaCantidadDeLineas() || !ValidaDireccionDefinida()
                    || (PedidosCollection.CambiarPrecio && !ValidarUtilidad()))
                    return;

                if (GestorPedido.PedidosCollection.Gestionados.Count == 0)
                {
                    new UIAlertView("Advertencia", "No hay pedidos gestionados", null, "OK", null).Show();

                    return;
                }
                if (GestorPedido.PedidosCollection.Gestionados[0].inTotalRetenciones > GestorPedido.PedidosCollection.Gestionados[0].MontoBruto)
                {
                    new UIAlertView("Advertencia","El monto de la retenci�n supera el monto del documento.",null,"OK",null).Show();

                    return;
                }
                if (string.IsNullOrEmpty(AppDelegate.Consecutivo))
                {
                    new UIAlertView("Advertencia", "No puede completar el pedido sin antes seleccionar un consecutivo para obtener el n�mero de pedido.", null, "OK", null).Show();

                    // Se marca el campo de texto de color rojo para indicar donde est� el error.
                    txt_DatoConsecutivo.BackgroundColor = UIColor.Red;

                    this.ReloadInputViews();

                    return;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error validando documento." + ex.Message);
            }

            // Si la validaci�n se dio exitosamente se continua con el proceso.
            TerminarGestion();
        }

        /// <summary>
        /// M�todo que se utiliza para validar con el usuario si desea completar el pedido o no.
        /// </summary>
        private void TerminarGestion()
        {
            string mensaje = "�Desea completar " + (PedidosCollection.FacturarPedido ? "la factura?" : "el pedido?");

            UIAlertView alert = new UIAlertView("Completar pedido", mensaje, null, "Cancelar", "Aceptar");

            // Evento click del bot�n de Aceptar.
            alert.Clicked += (s, b) =>
            {
                if (b.ButtonIndex == 1)
                {
                    InvokeOnMainThread(async () =>
                    {
                        // Determine the correct size to start the overlay (depending on device orientation)
                        var bounds = UIScreen.MainScreen.Bounds; // portrait bounds
                        if (UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft || UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight)
                        {
                            bounds.Size = new SizeF((float)bounds.Size.Width, (float)bounds.Size.Height);
                        }
                        // show the loading overlay on the UI thread using the correct orientation sizing
                        loadingOverlay = new LoadingOverlay((RectangleF)bounds);
                        ParentViewController.Add(loadingOverlay);
                        loadingOverlay.UpdateMessage("Guardando pedido...");

                        // Se continua con el proceso de completar el pedido.
                        await VerificarImpresion();
                    });
                }
            };

            alert.Show();
        }

        /// <summary>
        /// M�todo as�ncrono que se encarga de cargar los consecutivos para asignarle el correcto al pedido antes de ser guardado.
        /// </summary>
        private Task VerificarImpresion()
        {
            return Task.Run(() =>
            {
                if (!FRmConfig.EnConsulta)
                {
                    try
                    {
                        // Carga el consecutivo del pedido y se lo asigna.
                        GestorPedido.CodigoConsecutivo = AppDelegate.Consecutivo;
                        GestorPedido.PedidosCollection.CargarConsecutivos();
                    }
                    catch (Exception ex)
                    {
                        InvokeOnMainThread(() => 
                        { 
                            loadingOverlay.Hide();

                            if (ex.Message.ToUpper().Contains("CONSECUTIVO"))
                            {
                                new UIAlertView("Adventencia", "No se puede obtener el n�mero de pedido, verifique que"+
                                    " ha seleccionado un consecutivo correcto para la compa��a "+ GlobalUI.ClienteActual.Compania 
                                    +" en la secci�n de Opciones.", null, "OK", null).Show();
                            }
                            else
                            {
                                new UIAlertView("Adventencia", ex.Message + " del pedido.", null, "OK", null).Show();
                            }
                        });

                        return;
                    }
                }

                // Por el momento la aplicaci�n no tiene la opci�n de imprimir, por lo que se asigna false.
                this.Imprimir = false;

                // Se continua con el proceso de completar el pedido.
                TerminarGestion2();
            });
        }

        /// <summary>
        /// Termina la gesti�n del documento
        /// </summary>
        private void TerminarGestion2()
        {
            // Bloque comentado por si a futuro se implementa impresi�n del pedido.
            #region Preguntar por impresion

            //if (Impresora.SugerirImprimir)
            //{
            //    //string msj = PedidosCollection.FacturarPedido ? "�Desea imprimir el detalle de las facturas realizadas?" : "�Desea imprimir el detalle de los pedidos realizados?";
            //    //string titulo = PedidosCollection.FacturarPedido ? "Impresi�n de Detalle de Facturas" : "Impresi�n de Detalle de Pedidos";
            //    //AlertDialog.Builder alert = new AlertDialog.Builder(this);
            //    //alert.SetTitle(titulo);
            //    //alert.SetMessage(msj);
            //    //alert.SetPositiveButton("Si", delegate
            //    //{
            //    //    Intent intent = new Intent(this, typeof(ImpresionViewX));
            //    //    intent.PutExtra(titulo, "titulo");
            //    //    intent.PutExtra("ImprimeFactura", Pedidos.FacturarPedido);
            //    //    intent.PutExtra("original", true);
            //    //    intent.PutExtra("retorno", 0);
            //    //    ImpresionViewX.OnImprimir = ImprimirDocumento;
            //    //    this.StartActivity(intent);
            //    //});
            //    //alert.SetNegativeButton("No", delegate
            //    //{
            //    //    if (this.GuardaDocumento())
            //    //    {
            //    //        this.LimpiarCerrarFormulario();
            //    //    }
            //    //    else
            //    //    {
            //    //        if (!string.IsNullOrEmpty(AplicarPedidoViewX.Error))
            //    //        {
            //    //            ShowMessage.MostrarConRetornoInfo(this, LimpiarCerrarFormulario, AplicarPedidoViewX.Error, true);
            //    //        }
            //    //    }
            //    //});
            //    //RunOnUiThread(() => { alert.Show(); });
            //}
            //else
            //{
            //    if (this.GuardaDocumento())
            //    {
            //        InvokeOnMainThread(async () => {
            //            nint taskID = UIApplication.SharedApplication.BeginBackgroundTask(() =>
            //            {
            //                // do cleanup or stop task
            //                double timeRemaining = UIApplication.SharedApplication.BackgroundTimeRemaining;

            //                if (timeRemaining < 20)
            //                {
            //                    Toast.MakeToast(ParentViewController.View, "La tarea de sincronizacion va a expirar", null, UIColor.Black, true, enumDuracionToast.Mediano);
            //                }
            //            });

            //            string result = await SincronizarPedidos();

            //            //UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;

            //            LimpiarCerrarFormulario();

            //            loadingOverlay.Hide();

            //            new UIAlertView("Resultado", result, null, "OK", null).Show();

            //            UIApplication.SharedApplication.EndBackgroundTask(taskID);

            //            UIAlertView alert2 = new UIAlertView("Proceso finalizado", "El pedido ha sido realizado.", null, "OK", null);
            //            alert2.Clicked += (s2, b2) =>
            //            {
            //                var myStoryboard = AppDelegate.Storyboard;
            //                //Instantiating View Controller with Storyboard ID 'PedidosView'

            //                pedidosView.crearOrden = false;

            //                GestorPedido.carrito = null;
            //                GestorPedido.PedidosCollection.Borrar(GlobalUI.ClienteActual.Compania);
            //                GestorPedido.PedidosCollection.LimpiarValores();

            //                GlobalUI.ClienteActual = null;

            //                NavigationController.PopToViewController(pedidosView, true);
            //            };
            //            alert2.Show();
            //        });
            //    }
            //    else
            //    {
            //        if (!string.IsNullOrEmpty(Error))
            //        {
            //            //ShowMessage.MostrarConRetornoInfo(this, LimpiarCerrarFormulario, Error, true);

            //            LimpiarCerrarFormulario();

            //            new UIAlertView("Resultado", Error, null, "OK", null).Show();
            //        }
            //    }
            //}

            #endregion

            // Antes de guardar el pedido, se le asigna el nombre del usuario y el c�digo del consecutivo.
            GestorPedido.PedidosCollection.Gestionados[0].Usuario = GestorDatos.NombreUsuario;

            // Si el pedido se almacen� correctamente en la base de datos.
            if (this.GuardaDocumento())
            {
                InvokeOnMainThread(() => 
                { 
                    loadingOverlay.Hide();

                    UIAlertView alert2 = new UIAlertView("Proceso finalizado", "El pedido ha sido realizado.", null, "OK", null);

                    // Evento click del bot�n de Aceptar.
                    alert2.Clicked += (s2, b2) =>
                    {
                        // Reiniciar todos los datos y variables relacionados a la gesti�n del pedido.
                        LimpiarCerrarFormulario();
                    };

                    alert2.Show();
                });
            }
            else
            {
                // Si se devolvi� alg�n error con el pedido, se notifica al usuario para que lo pueda solucionar o cancelar el pedido.
                if (!string.IsNullOrEmpty(Error))
                {
                    InvokeOnMainThread(() =>
                    {
                        loadingOverlay.Hide();

                        new UIAlertView("Adventencia", Error, null, "OK", null).Show();

                        if (Error.ToUpper().Contains("INV�LIDO"))
                        {
                            // Se marca el campo de texto de color rojo para indicar donde est� el error.
                            txt_DatoConsecutivo.TextColor = UIColor.Red;
                        }
                    });
                }
            }
        }

        /// <summary>
        /// M�todo que se encarga de reiniciar los datos y variables del pedido y devolver al usuario al inicio
        /// de la secci�n de pedidos.
        /// </summary>
        private void LimpiarCerrarFormulario()
        {
            Error = string.Empty;

            // Limpia y reinicia los datos del pedido global.
            PedidosCollection.FacturarPedido = false;
            GestorPedido.PedidosCollection = new PedidosCollection();
            Pedido.BorrarXMLPedido(GlobalUI.ClienteActual.Codigo);

            // Limpia el carrito de compras.
            GestorPedido.carrito = null;

            // Limpia al cliente del pedido.
            GlobalUI.ClienteActual = null; 
            GlobalUI.RutaActual = null;
            GlobalUI.RutasActuales = null;

            // Asigna que no se est� creando una orden.
            PedidosView.crearOrden = false;

            // Devuelve al usuario a la pantalla inicial de la secci�n de pedidos.
            InvokeOnMainThread(() => { NavigationController.PopToViewController(pedidosView, true); });
        }

        /// <summary>
        /// M�todo que se encarga de guardar el pedido en la base de datos.
        /// </summary>
        /// <returns>Retorna si se guard� el pedido o no.</returns>
        private bool GuardaDocumento()
        {
            bool result = true;

            try
            {
                // Se intenta guardar el pedido.
                GestorPedido.PedidosCollection.GuardarPedidos();
            }
            catch (Exception ex)
            {
                if (ex.Message.ToUpper().Contains("INVALIDO"))
                {
                    Error = "El n�mero de pedido generado por el consecutivo " + AppDelegate.Consecutivo +
                        " es inv�lido y no puede ser utilizado, cambie el consecutivo a utilizar y" + 
                        " notifique a su administrador del problema.";
                }
                else if (ex.Message.Contains("UNIQUE"))
                {
                    Error = "No se pudo guardar el pedido dado que el siguiente n�mero de pedido del consecutivo "+ AppDelegate.Consecutivo +
                        " ya ha sido registrado en otro pedido previamente, para solucionar este problema utilice " +
                        "  la opci�n de Sincronizar de la aplicaci�n antes de volver a intentarlo.";
                }
                else
                {
                    Error = "Error al guardar el pedido. " + ex.Message;
                }

                result = false;

            }

            return result;
        }

        /// <summary>
        /// M�todo que se encarga de validar que el pedido no tenga m�s l�neas que el limite m�ximo permitido.
        /// </summary>
        /// <returns>Retorna si la cantidad de l�neas es v�lida o no.</returns>
        private bool ValidaCantidadDeLineas()
        {
            try
            {
                foreach (Pedido pedido in GestorPedido.PedidosCollection.Gestionados)
                {
                    // Si la cantidad de l�neas del pedido supera el m�ximo se notifica al usuario.
                    if (pedido.Detalles.Lista.Count > PedidosCollection.MaximoLineasDetalle)
                    {
                        new UIAlertView("Advertencia", "La cantidad de l�neas del pedido (" + pedido.Detalles.Lista.Count 
                            + ") supera el m�ximo permitido (" + PedidosCollection.MaximoLineasDetalle + ").", null, "OK", null).Show();

                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                if (PedidosCollection.FacturarPedido)
                    throw new Exception("Error validando la cantidad de l�neas de la factura. " + ex.Message);
                else
                    throw new Exception("Error validando la cantidad de l�neas del pedido. " + ex.Message);
            }

            return true;
        }

        /// <summary>
        /// M�todo que se encarga de validar que la direccion de entrega est� asignada.
        /// </summary>
        /// <returns>Retorna si la direccion de entrega est� definida o no.</returns>
        private bool ValidaDireccionDefinida()
        {
            try
            {
                foreach (Pedido pedido in GestorPedido.PedidosCollection.Gestionados)
                {
                    // Si la direcci�n del entrega no est� definida se notifica al usuario.
                    if (pedido.DireccionEntrega == string.Empty)
                    {
                        new UIAlertView("Advertencia", "La direcci�n del entrega no est� definida.", null, "OK", null).Show();

                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error validando direcci�n de entrega. " + ex.Message);
            }

            return true;
        }

        /// <summary>
        /// M�todo que se encarga de validar que el monto del pedido no sea menor al costo total (que el c�lculo hecho sea correcto).
        /// </summary>
        /// <returns>Retorna si la cantidad de l�neas es v�lida o no.</returns>
        private bool ValidarUtilidad()
        {
            decimal costoTotal = 0;
            decimal montoPedido = 0;

            foreach (Pedido pedido in GestorPedido.PedidosCollection.Gestionados)
            {
                costoTotal = pedido.ObtenerCostoTotal();
                montoPedido = pedido.MontoBruto - pedido.MontoTotalDescuento;

                // Si monto del pedido es inferior al costo total se notifica al usuario.
                if (montoPedido < costoTotal)
                {
                    new UIAlertView("Advertencia", "No se puede vender por debajo del costo total.", null, "OK", null).Show();

                    return false;
                }
            }
            return true;
        }

        #endregion

        #region View lifecycle

        /// <summary>
        /// M�todo para liberaci�n de memoria de la pantalla cuando se cierra.
        /// </summary>
        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        /// <summary>
        /// M�todo que se ejecuta al crear la pantalla, se ejecuta una �nica vez a menos que la pantalla se remueva del stack.
        /// </summary>
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Inicializar las variables o asignar los datos que se utilizan en la pantalla.

            dolar = GlobalUI.ClienteActual.ClienteCompania[0].Moneda == TipoMoneda.DOLAR;

            // Se crea una barra para colocar el bot�n de listo en el teclado que abre el control txt_DatoDescuento2.
            UIToolbar toolbar = new UIToolbar(new RectangleF(0.0f, 0.0f, (float)this.View.Frame.Size.Width, 44.0f));
            toolbar.TintColor = UIColor.White;
            toolbar.BarStyle = UIBarStyle.Black;
            toolbar.Translucent = true;

            //Crea un custom view para mostrar la cantidad total de art�culos

            string cantidad = string.Empty;
            Pedido pedi = GestorPedido.PedidosCollection.Buscar(GlobalUI.ClienteActual.Compania);
            if (pedi != null && pedi.Detalles!=null)
            {
                cantidad = pedi.Detalles.TotalArticulos.ToString();
            }

            var myCustomView = new UIView(new RectangleF(0.0f, 0.0f, 75.0f, 44.0f));            
            UIImageView imageTotalArticulos = new UIImageView(UIImage.FromBundle("ic_shopping_cart_36pt.png"));
            UILabel labelTotalArticulos = new UILabel(new RectangleF(0.0f, 0.0f, 75.0f, 44.0f));
            labelTotalArticulos.Text = cantidad;
            labelTotalArticulos.TextAlignment = UITextAlignment.Right;
            labelTotalArticulos.TextColor = UIColor.Orange;
            myCustomView.AddSubview(labelTotalArticulos);
            myCustomView.AddSubview(imageTotalArticulos);            
            



            // Se crean los botones de la barra y se asignan los Eventos de los mismos.
            toolbar.Items = new UIBarButtonItem[]{                
                new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                new UIBarButtonItem(UIBarButtonSystemItem.Done, delegate {
                    // Si no se escribi� nada en el campo de texto se pone un 0 por defecto.
                    if (txt_DatoDescuento2.Text.Length == 0)
                    {
                        txt_DatoDescuento2.Text = "0";
                    }
                    else
                    {
                        decimal desc1 = Convert.ToDecimal(txt_DatoDescuento1.Text.Replace("%", "").Replace(".", ","));
                        decimal desc2 = Convert.ToDecimal(txt_DatoDescuento2.Text.Replace(".", ","));

                        if ((desc1 + desc2) > 100)
                        {
                            txt_DatoDescuento2.Text = Convert.ToString(100 - desc1);

                            Toast.MakeToast(ParentViewController.ParentViewController.View,
                                "La suma del Descuento 1 m�s el Descuento 2 no puede superar el 100%.",
                                null, UIColor.Black, true, enumDuracionToast.Largo);
                        }
                    }

                    Pedido pedido = GestorPedido.PedidosCollection.Buscar(GlobalUI.ClienteActual.Compania);

                    // Si se pudo obtener la referencia al pedido.
                    if (pedido != null)
                    {
                        // Aplicar el nuevo Descuento 2 al pedido y refrescar los datos en pantalla.
                        pedido.PorcentajeDescuento2 = Convert.ToDecimal(txt_DatoDescuento2.Text);
                        GestorPedido.PedidosCollection.SacarMontosTotales();

                        RefrescarDatosPedido();
                    }

                    btn_Ordenar.Enabled = true;

                    this.txt_DatoDescuento2.ResignFirstResponder();
                })
            };

            // Se le asigna al campo de texto la barra a mostrar sobre el teclado.
            txt_DatoDescuento2.InputAccessoryView = toolbar;
            txt_DatoDescuento2.EditingDidBegin += txt_DatoDescuento2_EditingDidBegin;

            // Se agrega un Observer para escuchar el evento de ValueChanged al cambiar el descuento 2.
            NSNotificationCenter.DefaultCenter.AddObserver
            (UITextField.TextFieldTextDidChangeNotification, (notification) =>
            {
                if (notification.Object == txt_DatoDescuento2)
                {
                    txt_DatoDescuento2_ValueChanged();
                }
            });

            btn_FechaEntrega.TouchUpInside += btn_FechaEntrega_TouchUpInside;

            fechaSeleccionada = DateTime.Today.AddDays(1).ToString("dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);

            btn_FechaEntrega.SetTitle(fechaSeleccionada, UIControlState.Normal);

            UIBarButtonItem btnTotalArticulos = new UIBarButtonItem(myCustomView);
            btnTotalArticulos.Style = UIBarButtonItemStyle.Plain;


            UIBarButtonItem[] items = new UIBarButtonItem[] {                
                new UIBarButtonItem(UIImage.FromFile("btn_Cancelar.png")
                , UIBarButtonItemStyle.Plain
                , (sender, args) =>
                {
                    // Evento de click del bot�n de cancelar pedido.

                    UIAlertView alert = new UIAlertView("Cancelar pedido", "Va a salir de la gesti�n del pedido.", null, "Salir", "Continuar");
                    alert.Clicked += (s, b) =>
                    {
                        if (b.ButtonIndex == 0)
                        {
                            // Limpia y reinicia los datos del pedido global.
                            PedidosCollection.FacturarPedido = false;
                            GestorPedido.PedidosCollection = new PedidosCollection();
                            Pedido.BorrarXMLPedido(GlobalUI.ClienteActual.Codigo);

                            // Limpia el carrito de compras.
                            GestorPedido.carrito = null;

                            // Limpia al cliente del pedido.
                            GlobalUI.ClienteActual = null;
                            GlobalUI.RutaActual = null;
                            GlobalUI.RutasActuales = null;

                            // Asigna que no se est� creando una orden.
                            PedidosView.crearOrden = false;

                            // Devuelve al usuario a la pantalla inicial de la secci�n de pedidos.
                            InvokeOnMainThread(() => { NavigationController.PopToViewController(pedidosView, true); });
                        }

                    };
                    alert.Show();
                }),
                new UIBarButtonItem(UIImage.FromFile("tab_Clientes.png")
                , UIBarButtonItemStyle.Plain
                , (sender, args) =>
                {
                    // Evento de click del bot�n de ver Cliente.

                    Toast.MakeToast(ParentViewController.ParentViewController.View, "Cliente: " + GlobalUI.ClienteActual.Nombre + " - C�digo: " + GlobalUI.ClienteActual.Codigo +
                        " - Compa��a: " + GlobalUI.ClienteActual.Compania + ".", null, UIColor.Black, true, enumDuracionToast.Mediano);
                }),
                btnTotalArticulos
            };

            // Agregar en la barra de Navegaci�n los botones para sincronizar la aplicaci�n y
            // para ingresar la direcci�n del servidor.
            this.NavigationItem.SetRightBarButtonItems(items, true);

            // Cargar los consecutivos disponibles para el pedido.
            obtenerConsecutivos();
        }
                
        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a aparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            var compania = Compania.Obtener(GlobalUI.ClienteActual.Compania);

            if (compania != null)
            {
                simboloMonetarioPedido = compania.SimboloMonetarioFunc;
            }

            RefrescarDatosPedido();
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla apareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            // Se inicia el proceso de intentar sincronizar pedidos pendientes.
            AppDelegate.IniciarSincronizacionPedidos(this.ParentViewController.ParentViewController);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a desaparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla desapareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
	}
}
