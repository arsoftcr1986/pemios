// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Softland.iOS.Pem
{
	[Register ("DetalleArticuloPedidoTableView")]
	partial class DetalleArticuloPedidoTableView
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_Bonificaciones { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIImageView img_Imagen { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Bodega { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Bonificacion { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Cantidad { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoBonExistencia { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoExistencia { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoImpuestos { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoMontoDescuento { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoPrecioAlmacen { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoPrecioDetalle { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoSubtotal { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoTotal { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Descuento { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Impuesto { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_MontoDescuento { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_PrecioAlmacen { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_PrecioDetalle { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Subtotal { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Total { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView TableView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel txt_BonBodega { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField txt_Bonificacion1 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField txt_Bonificacion2 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel txt_BonLocalizacion { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField txt_CantidadAlmacen { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField txt_CantidadDetalle { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField txt_DatoBodega { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField txt_DatoBonBodega { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField txt_DatoBonLocalizacion { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField txt_DatoLocalizacion { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField txt_Descuento { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel txt_Localizacion { get; set; }

		[Action ("btn_Bonificaciones_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_Bonificaciones_TouchUpInside (UIButton sender);

		[Action ("txt_Descuento_ValueChanged:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void txt_Descuento_ValueChanged (UITextField sender);

		void ReleaseDesignerOutlets ()
		{
			if (btn_Bonificaciones != null) {
				btn_Bonificaciones.Dispose ();
				btn_Bonificaciones = null;
			}
			if (img_Imagen != null) {
				img_Imagen.Dispose ();
				img_Imagen = null;
			}
			if (lbl_Bodega != null) {
				lbl_Bodega.Dispose ();
				lbl_Bodega = null;
			}
			if (lbl_Bonificacion != null) {
				lbl_Bonificacion.Dispose ();
				lbl_Bonificacion = null;
			}
			if (lbl_Cantidad != null) {
				lbl_Cantidad.Dispose ();
				lbl_Cantidad = null;
			}
			if (lbl_DatoBonExistencia != null) {
				lbl_DatoBonExistencia.Dispose ();
				lbl_DatoBonExistencia = null;
			}
			if (lbl_DatoExistencia != null) {
				lbl_DatoExistencia.Dispose ();
				lbl_DatoExistencia = null;
			}
			if (lbl_DatoImpuestos != null) {
				lbl_DatoImpuestos.Dispose ();
				lbl_DatoImpuestos = null;
			}
			if (lbl_DatoMontoDescuento != null) {
				lbl_DatoMontoDescuento.Dispose ();
				lbl_DatoMontoDescuento = null;
			}
			if (lbl_DatoPrecioAlmacen != null) {
				lbl_DatoPrecioAlmacen.Dispose ();
				lbl_DatoPrecioAlmacen = null;
			}
			if (lbl_DatoPrecioDetalle != null) {
				lbl_DatoPrecioDetalle.Dispose ();
				lbl_DatoPrecioDetalle = null;
			}
			if (lbl_DatoSubtotal != null) {
				lbl_DatoSubtotal.Dispose ();
				lbl_DatoSubtotal = null;
			}
			if (lbl_DatoTotal != null) {
				lbl_DatoTotal.Dispose ();
				lbl_DatoTotal = null;
			}
			if (lbl_Descuento != null) {
				lbl_Descuento.Dispose ();
				lbl_Descuento = null;
			}
			if (lbl_Impuesto != null) {
				lbl_Impuesto.Dispose ();
				lbl_Impuesto = null;
			}
			if (lbl_MontoDescuento != null) {
				lbl_MontoDescuento.Dispose ();
				lbl_MontoDescuento = null;
			}
			if (lbl_PrecioAlmacen != null) {
				lbl_PrecioAlmacen.Dispose ();
				lbl_PrecioAlmacen = null;
			}
			if (lbl_PrecioDetalle != null) {
				lbl_PrecioDetalle.Dispose ();
				lbl_PrecioDetalle = null;
			}
			if (lbl_Subtotal != null) {
				lbl_Subtotal.Dispose ();
				lbl_Subtotal = null;
			}
			if (lbl_Total != null) {
				lbl_Total.Dispose ();
				lbl_Total = null;
			}
			if (TableView != null) {
				TableView.Dispose ();
				TableView = null;
			}
			if (txt_BonBodega != null) {
				txt_BonBodega.Dispose ();
				txt_BonBodega = null;
			}
			if (txt_Bonificacion1 != null) {
				txt_Bonificacion1.Dispose ();
				txt_Bonificacion1 = null;
			}
			if (txt_Bonificacion2 != null) {
				txt_Bonificacion2.Dispose ();
				txt_Bonificacion2 = null;
			}
			if (txt_BonLocalizacion != null) {
				txt_BonLocalizacion.Dispose ();
				txt_BonLocalizacion = null;
			}
			if (txt_CantidadAlmacen != null) {
				txt_CantidadAlmacen.Dispose ();
				txt_CantidadAlmacen = null;
			}
			if (txt_CantidadDetalle != null) {
				txt_CantidadDetalle.Dispose ();
				txt_CantidadDetalle = null;
			}
			if (txt_DatoBodega != null) {
				txt_DatoBodega.Dispose ();
				txt_DatoBodega = null;
			}
			if (txt_DatoBonBodega != null) {
				txt_DatoBonBodega.Dispose ();
				txt_DatoBonBodega = null;
			}
			if (txt_DatoBonLocalizacion != null) {
				txt_DatoBonLocalizacion.Dispose ();
				txt_DatoBonLocalizacion = null;
			}
			if (txt_DatoLocalizacion != null) {
				txt_DatoLocalizacion.Dispose ();
				txt_DatoLocalizacion = null;
			}
			if (txt_Descuento != null) {
				txt_Descuento.Dispose ();
				txt_Descuento = null;
			}
			if (txt_Localizacion != null) {
				txt_Localizacion.Dispose ();
				txt_Localizacion = null;
			}
		}
	}
}
