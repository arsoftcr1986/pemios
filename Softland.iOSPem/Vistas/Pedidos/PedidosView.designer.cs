// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Softland.iOS.Pem
{
	[Register ("PedidosView")]
	partial class PedidosView
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_Buscar { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_Cancelar { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_Crear { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_PedidosPendientes { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView TableView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITabBarItem tabPedidos { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView tbv_PedidosPendientes { get; set; }

		[Action ("btn_Cancelar_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_Cancelar_TouchUpInside (UIButton sender);

		[Action ("btn_Crear_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_Crear_TouchUpInside (UIButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (btn_Buscar != null) {
				btn_Buscar.Dispose ();
				btn_Buscar = null;
			}
			if (btn_Cancelar != null) {
				btn_Cancelar.Dispose ();
				btn_Cancelar = null;
			}
			if (btn_Crear != null) {
				btn_Crear.Dispose ();
				btn_Crear = null;
			}
			if (lbl_PedidosPendientes != null) {
				lbl_PedidosPendientes.Dispose ();
				lbl_PedidosPendientes = null;
			}
			if (TableView != null) {
				TableView.Dispose ();
				TableView = null;
			}
			if (tabPedidos != null) {
				tabPedidos.Dispose ();
				tabPedidos = null;
			}
			if (tbv_PedidosPendientes != null) {
				tbv_PedidosPendientes.Dispose ();
				tbv_PedidosPendientes = null;
			}
		}
	}
}
