// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Softland.iOS.Pem
{
	[Register ("DetallePedidoView")]
	partial class DetallePedidoView
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_agregarArticulos { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_Cliente { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_Ordenar { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_Vaciar { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoTotal { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Total { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView TableView { get; set; }

		[Action ("btn_Agregar_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_Agregar_TouchUpInside (UIButton sender);

		[Action ("btn_Cliente_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_Cliente_TouchUpInside (UIButton sender);

		[Action ("btn_Ordenar_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_Ordenar_TouchUpInside (UIButton sender);

		[Action ("btn_Vaciar_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_Vaciar_TouchUpInside (UIButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (btn_agregarArticulos != null) {
				btn_agregarArticulos.Dispose ();
				btn_agregarArticulos = null;
			}
			if (btn_Cliente != null) {
				btn_Cliente.Dispose ();
				btn_Cliente = null;
			}
			if (btn_Ordenar != null) {
				btn_Ordenar.Dispose ();
				btn_Ordenar = null;
			}
			if (btn_Vaciar != null) {
				btn_Vaciar.Dispose ();
				btn_Vaciar = null;
			}
			if (lbl_DatoTotal != null) {
				lbl_DatoTotal.Dispose ();
				lbl_DatoTotal = null;
			}
			if (lbl_Total != null) {
				lbl_Total.Dispose ();
				lbl_Total = null;
			}
			if (TableView != null) {
				TableView.Dispose ();
				TableView = null;
			}
		}
	}
}
