using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;

using Pedidos.Core;
using BI.Shared;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;

namespace Softland.iOS.Pem
{
	partial class PedidosView : UIViewController
    {

        #region Propiedades y Contructor de instancia

        /// <summary>
        /// Referencia a la vista de carga que es mostrada al realizar la carga de clientes.
        /// </summary>
        public static LoadingOverlay loadingOverlay;

        /// <summary>
        /// Lista de clientes a cargar en el controlador de tabla, se llena con las b�squedas r�pida y avanzada.
        /// </summary>
        public List<Cliente> clientes;
        
        /// <summary>
        /// Lista de pedidos pendientes de sincronizar.
        /// </summary>
        public List<Pedido> pedidosPendientes;

        /// <summary>
        /// Referencia a DetallePedidoView para tener acceso a sus variables antes de moverse a esa nueva pantalla.
        /// </summary>
        public DetallePedidoView detallePedidoView;

        /// <summary>
        /// Lista de compa��as disponibles para realizar las b�squedas.
        /// </summary>
        public List<string> companias;

        /// <summary>
        /// Variable que almacena el valor de la compa�ia seleccionada para la b�squeda.
        /// </summary>
        public string CIA = "";

        /// <summary>
        /// Variable que almacena la compan�a de los clientes actualmente cargados.
        /// </summary>
        public static string clientesCompania = string.Empty;

        /// <summary>
        /// Variable que almacena si se est� creando o no un pedido.
        /// </summary>
        public static bool crearOrden = false;

        /// <summary>
        /// Variable que almacena si se est� creando o no un pedido desde la opci�n en la secci�n de clientes.
        /// </summary>
        public bool pedidoDesdeClientes = false;

        /// <summary>
        /// Controlador para generar la barra de b�squeda r�pida.
        /// </summary>
        private UISearchBar searchBar;

        /// <summary>
        /// Costructor por defecto.
        /// </summary>
        /// <param name="handle"></param>
		public PedidosView (IntPtr handle) : base (handle)
		{
		}

        #endregion

        #region Tabla

        /// <summary>
        /// Clase que se utilizar� para manejar la informaci�n de la tabla de pedidos pendientes.
        /// </summary>
        public class TableSourcePedidos : UITableViewSource
        {

            #region Propiedades y Contructor de instancia

            /// <summary>
            /// Variable para identificar las celdas que utiliza este TableSource.
            /// </summary>
            string cellIdentifier = "TableCell";

            /// <summary>
            /// Referencia a la pantalla para poder acceder a sus propiedades desde otra clase. 
            /// </summary>
            PedidosView VistaPadre;

            /// <summary>
            /// Constructor por defecto. 
            /// </summary>
            /// <param name="padre"></param>
            public TableSourcePedidos(PedidosView padre)
            {
                VistaPadre = padre;
            }

            #endregion

            #region Eventos

            // Evento requerido por la clase, retorna la cantidad de registros en la fuente de datos.
            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return VistaPadre.pedidosPendientes.Count;
            }

            /// <summary>
            /// Evento que se ejecuta al cargar las celdas de la tabla seg�n vayan siendo mostradas.
            /// </summary>
            /// <param name="tableView"></param>
            /// <param name="indexPath">�ndice de la celda por cargar.</param>
            /// <returns>Retorna la celda cargada.</returns>
            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                // Busca la celda creada para ahorrar memoria.
                UITableViewCell cell = tableView.DequeueReusableCell(cellIdentifier);

                // Si no hay celdas por reutilizar, se crea una nueva, en este caso se usa una celda predefinida Subtitle.
                if (cell == null)
                {
                    cell = new UITableViewCell(UITableViewCellStyle.Subtitle, cellIdentifier);

                    cell.TextLabel.Font = UIFont.FromName("Helvetica-Bold", 14f);
                    cell.DetailTextLabel.AdjustsFontSizeToFitWidth = true;
                    cell.DetailTextLabel.MinimumFontSize = 6;

                    cell.DetailTextLabel.Font = UIFont.FromName("Helvetica", 12f);
                    cell.DetailTextLabel.AdjustsFontSizeToFitWidth = true;
                    cell.DetailTextLabel.MinimumFontSize = 6;
                }

                cell.TextLabel.TextColor = UIColor.Black;

                string simboloMonetarioPedido = GestorUtilitario.SimboloMonetario;

                Pedido pedido = VistaPadre.pedidosPendientes[indexPath.Row];
                Cliente cliente = new Cliente();
                string total = "";
                
                cliente.Codigo = pedido.Cliente;
                cliente.Compania = pedido.Compania;
                cliente.Cargar();
                cliente.ObtenerClientesCia();

                var compania = Compania.Obtener(cliente.Compania);

                if (compania != null)
                {
                    simboloMonetarioPedido = compania.SimboloMonetarioFunc;
                }

                pedido.Configuracion.ClienteCia = cliente.ObtenerClienteCia(pedido.Compania);
                pedido.Configuracion.Cargar();
                pedido.Configuracion.ClienteCia.ObtenerDireccionesEntrega();

                if (pedido.Configuracion.ClienteCia.Moneda == TipoMoneda.LOCAL)
                {
                    total = GestorUtilitario.FormatNumero(pedido.MontoNeto, simboloMonetarioPedido);
                }
                else
                {
                    total = GestorUtilitario.FormatNumeroDolar(pedido.MontoNeto);
                }

                if (!string.IsNullOrEmpty(pedido.Motivo))
                {
                    cell.TextLabel.TextColor = UIColor.FromRGB(225, 106, 12);
                }

                // Se actualiza la informaci�n de la celda.
                cell.TextLabel.Text = "Consecutivo: " + pedido.Codigo + " | Compa��a: " + pedido.Compania + " | Fecha: " + pedido.FechaRealizacion.ToString("MM/dd/yyyy");
                cell.DetailTextLabel.Text = "Cliente: " + cliente.Nombre + " | Monto Total: " + total;

                return cell;
            }

            /// <summary>
            /// Evento que se ejecuta al seleccionar una celda de la tabla.
            /// </summary>
            /// <param name="tableView"></param>
            /// <param name="indexPath">�ndice de la celda seleccionada.</param>
            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                Pedido pedido = VistaPadre.pedidosPendientes[indexPath.Row];

                string mensaje = "";

                if (string.IsNullOrEmpty(pedido.Motivo))
                {
                    mensaje = "El pedido a�n no ha sido sincronizado �Desea eliminar el pedido " + pedido.Numero + "?";
                }
                else
                {
                    mensaje = "Parece haber alg�n problema al intentar sincronizar el pedido. Contacte a su administrador para m�s informaci�n. �Desea eliminar el pedido " + pedido.Numero + "?";
                }

                UIAlertView alert = new UIAlertView("Eliminar pedido", mensaje, null, "Eliminar", "Cancelar");

                // Evento click del bot�n de Aceptar.
                alert.Clicked += (s, b) =>
                {
                    if (b.ButtonIndex == 0)
                    {
                        try
                        {
                            pedido.CancelarPedido();

                            VistaPadre.CargarPedidosPendientes();
                        }
                        catch (Exception ex)
                        {
                            Toast.MakeToast(VistaPadre.View, "No se ha podido eliminar el pedido seleccionado, intente nuevamente.",
                                null, UIColor.Black, true, enumDuracionToast.Mediano);
                        }
                    }

                };
                alert.Show();
            }

            #endregion
        }

        /// <summary>
        /// Clase que se utilizar� para manejar la informaci�n de la tabla de selecci�n de cliente.
        /// </summary>
        public class TableSource : UITableViewSource
        {

            #region Propiedades y Contructor de instancia

            /// <summary>
            /// Variable para identificar las celdas que utiliza este TableSource.
            /// </summary>
            string cellIdentifier = "TableCell";

            /// <summary>
            /// Referencia a la pantalla para poder acceder a sus propiedades desde otra clase. 
            /// </summary>
            PedidosView VistaOpciones;
            
            /// <summary>
            /// Constructor por defecto. 
            /// </summary>
            /// <param name="padre"></param>
            public TableSource(PedidosView padre)
            {
                VistaOpciones = padre;
            }

            #endregion

            #region Eventos

            // Evento requerido por la clase, retorna la cantidad de registros en la fuente de datos.
            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return VistaOpciones.clientes.Count;
            }

            /// <summary>
            /// Evento que se ejecuta al cargar las celdas de la tabla seg�n vayan siendo mostradas.
            /// </summary>
            /// <param name="tableView"></param>
            /// <param name="indexPath">�ndice de la celda por cargar.</param>
            /// <returns>Retorna la celda cargada.</returns>
            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                // Busca la celda creada para ahorrar memoria.
                UITableViewCell cell = tableView.DequeueReusableCell(cellIdentifier);

                // Si no hay celdas por reutilizar, se crea una nueva, en este caso se usa una celda predefinida Subtitle.
                if (cell == null)
                {
                    cell = new UITableViewCell(UITableViewCellStyle.Subtitle, cellIdentifier);

                    cell.TextLabel.Font = UIFont.FromName("Helvetica-Bold", 14f);
                    cell.TextLabel.AdjustsFontSizeToFitWidth = false;

                    cell.DetailTextLabel.Font = UIFont.FromName("Helvetica", 12f);
                    cell.DetailTextLabel.AdjustsFontSizeToFitWidth = true;
                    cell.DetailTextLabel.MinimumFontSize = 8;
                }

                // Se actualiza la informaci�n de la celda.
                cell.TextLabel.Text = VistaOpciones.clientes[indexPath.Row].Nombre;
                cell.DetailTextLabel.Text = "C�digo: " + VistaOpciones.clientes[indexPath.Row].Codigo;

                return cell;
            }

            /// <summary>
            /// Evento que se ejecuta al seleccionar una celda de la tabla.
            /// </summary>
            /// <param name="tableView"></param>
            /// <param name="indexPath">�ndice de la celda seleccionada.</param>
            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                // Referencia al cliente de la celda seleccionada.
                var ClienteSeleccionado = VistaOpciones.clientes[indexPath.Row];

                // Mensaje de error a mostrar al usuario.
                string error = "";

                // Variable para validar si ocurrio un error al cargar la informaci�n.
                bool errorValidacion = false;

                // Si el cliente seleccionado no es nulo, se intentar� validar el nivel de precio definido del cliente
                // y as� poder continuar con la creaci�n del pedido.
                if (ClienteSeleccionado != null)
                {
                    // Se cargan la bodega actual y bodegas del cliente (Rutas = Bodegas).
                    GlobalUI.RutaActual = Ruta.ObtenerRuta(GlobalUI.Rutas, ClienteSeleccionado.Zona);
                    GlobalUI.RutasActuales = Ruta.ObtenerRutas(GlobalUI.Rutas, ClienteSeleccionado.Zona);
                    
                    try
                    {
                        // Se asigna el cliente seleccionado a la referencia global de GlobalUI.ClienteActual y su c�digo de bodega.
                        GlobalUI.ClienteActual = ClienteSeleccionado;
                        GlobalUI.ClienteActual.Zona = GlobalUI.RutaActual.Codigo;

                        // Se carga la informaci�n de las compa��as del cliente.
                        GlobalUI.ClienteActual.ObtenerClientesCia();

                        // Se valida si se pudo cargar los datos del cliente CIA del cliente.
                        if (GlobalUI.ClienteActual.ClienteCompania.Count == 0)
                        {
                            error = "No se pudo cargar los datos de los clientes en compa��a para el cliente " + GlobalUI.NameCliente;

                            errorValidacion = true;
                        }

                        // Se llama a cargar los datos requeridos para iniciar el pedido.
                        if (!errorValidacion && !VistaOpciones.InicializarPedido())
                        {
                            error = "Error al inicializar el pedido con el cliente " + GlobalUI.NameCliente;

                            errorValidacion = true;
                        }

                        if (!errorValidacion && string.IsNullOrEmpty(GlobalUI.ClienteActual.ClienteCompania[0].DireccionEntregaDefault))
                        {
                            error += "Al cliente " + GlobalUI.NameCliente + " no se le ha asignado una direcci�n de entrega por defecto.";

                            errorValidacion = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        error = "No se pudo cargar los datos del cliente " + GlobalUI.NameCliente + ". " + ex.Message;

                        errorValidacion = true;
                    }

                    // (Des)Habilita las opciones para guardar la posici�n del cliente.
                    //HabilitarOpcionesGPS(GlobalUI.ClienteActual);
                }

                // Si ocurri� alg�n problema al cargar toda la informaci�n del cliente seleccionado se notifica al usuario.
                if (errorValidacion)
                {
                    // Dado que el cliente seleccionado no es valido para crear un pedido se reinician las variables utilizadas.
                    GlobalUI.RutaActual = null;
                    GlobalUI.RutasActuales = null;
                    GlobalUI.ClienteActual = null;
                    GestorPedido.PedidosCollection = new PedidosCollection();

                    Toast.MakeToast(VistaOpciones.View, "Parece haber un problema con la informaci�n del cliente, " + error, null, UIColor.Black, true, enumDuracionToast.Mediano);
                }
                else
                {
                    // Se referencia al Storyboard del app.
                    var myStoryboard = AppDelegate.Storyboard;

                    // Se instancia un ViewController con el ID del Storyboard y su clase respectiva.
                    VistaOpciones.detallePedidoView = myStoryboard.InstantiateViewController("DetallePedidoView") as DetallePedidoView;

                    // Se asignan los valores que se desean pasar a la nueva pantalla.
                    VistaOpciones.detallePedidoView.pedidosView = VistaOpciones;

                    // Se navega a la pantalla instanciada agregandola al stack.
                    VistaOpciones.NavigationController.PushViewController(VistaOpciones.detallePedidoView, true);

                    // Se quita el resaltado que se le da a una celda al ser seleccionada (simula comportamiento de iOS).
                    tableView.DeselectRow(indexPath, true);
                }
            }

            #endregion
        }

        #endregion

        #region Eventos

        /// <summary>
        /// Evento de click del bot�n btn_Crear.
        /// Se usa para mostrar todos los controles de selecci�n de cliente y refrescar las variables (mostrar el Paso 1).
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_Crear_TouchUpInside(UIButton sender)
        {
            // Se oculta el bot�n de crear pedido.
            btn_Crear.Hidden = true;
            lbl_PedidosPendientes.Hidden = true;
            tbv_PedidosPendientes.Hidden = true;

            // Se muestran los demas controles y se refrescan las variables involucradas.
            btn_Buscar.Hidden = false;
            TableView.Hidden = false;
            btn_Cancelar.Hidden = false;

            this.NavigationItem.TitleView = searchBar;

            if (!pedidoDesdeClientes)
            {
                this.Title = "Paso 1";
            }
            else
            {
                this.Title = "Detalle de Cliente";
            }

            this.TabBarItem.Title = "Pedidos";

            crearOrden = true;

            // Cargar las compa��as disponibles para buscar clientes.
            companias = new List<string>();
            companias.Add("Todos");

            // Se obtiene la lista de compa��as de la base de datos.
            companias.AddRange(Compania.Obtener().ConvertAll(x => x.Codigo));

            if (companias.Count == 2)
            {
                btn_Buscar.Enabled = false;
            }
            else
            {
                btn_Buscar.Enabled = true;
            }

            // Cargar todos los clientes disponibles si a�n no han sido cargados.
            if (ClientesView.clientesCargados.Count == 0)
            {
                CargaInicialClientes();
            }
            else
            {
                searchBar.Placeholder = "B�squeda r�pida de Clientes";

                searchBar.UserInteractionEnabled = true;

                QuickSearch();
            }
            
            // Se notifica al usuario de que seleccione un cliente.
            Toast.MakeToast(View, "Seleccione primero un Cliente para el pedido.", null, UIColor.Black, true, enumDuracionToast.Corto);
        }

        /// <summary>
        /// Evento de click del bot�n btn_Cancelar.
        /// Se usa para ocultar todos los controles de selecci�n de cliente y refrescar las variables (reiniciar la pantalla).
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_Cancelar_TouchUpInside(UIButton sender)
        {
            // Se muestra el bot�n de crear pedido.
            btn_Crear.Hidden = false;
            lbl_PedidosPendientes.Hidden = false;
            tbv_PedidosPendientes.Hidden = false;

            // Se ocultan los demas controles y se refrescan las variables involucradas.
            btn_Buscar.Hidden = true;

            clientes.Clear();

            TableView.Hidden = true;
            TableView.ReloadData();

            searchBar.Text = "";

            clientesCompania = AppDelegate.Compania;

            this.NavigationItem.TitleView = null;

            btn_Cancelar.Hidden = true;

            this.Title = "Pedidos";

            crearOrden = false;

            CargarPedidosPendientes();
        }

        /// <summary>
        /// Evento de click del bot�n btn_Buscar.
        /// Se usa para que el usuario pueda elegir la compa��a por la cual filtrar clientes, despliega una pantalla para elegir 
        /// una compa��a.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void btn_Buscar_TouchUpInside(object sender, EventArgs e)
        {
            // Controlador que mostrar� el selector de fecha en pantalla.
            var modalPicker = new ModalPickerViewController(ModalPickerType.Custom, "Seleccione una compa��a:", this)
            {
                HeaderBackgroundColor = UIColor.DarkGray,
                InternalBackgroundColor = UIColor.White,
                HeaderTextColor = UIColor.White,
                TransitioningDelegate = new ModalPickerTransitionDelegate(),
                ModalPresentationStyle = UIModalPresentationStyle.Custom
            };

            // Se crea el Model para el selector.
            modalPicker.PickerView.Model = new PickerModel(companias);

            // Evento en que la pantalla del controlador desaparece.
            modalPicker.OnModalPickerDismissed += (s, ea) =>
            {
                var index = modalPicker.PickerView.SelectedRowInComponent(0);
                CIA = companias[(int)index];

                this.QuickSearch();
            };

            // Mostrar la pantalla.
            await PresentViewControllerAsync(modalPicker, true);
        }

        #endregion

        #region Metodos

        /// <summary>
        /// M�todo que se ejecuta al iniciar una b�squeda r�pida por alg�n cliente.
        /// Busca por clientes seg�n la descripci�n de estos y el dato ingresado en el campo de texto.
        /// </summary>
        void QuickSearch()
        {
            CIA = string.IsNullOrEmpty(CIA) ? "Todos" : CIA;

            if (CIA.Equals("Todos"))
            {
                clientesCompania = "";

                btn_Buscar.SetTitle(" Todas las compa��as", UIControlState.Normal);
            }
            else
            {
                clientesCompania = CIA;

                btn_Buscar.SetTitle(" Compa��a " + CIA, UIControlState.Normal);
            }

            clientes.Clear();

            // Se obtienen los clientes encontrados, y se agregan a la lista.
            clientes.AddRange(ClientesView.clientesCargados.FindAll(
                x =>

                (
                    x.Nombre.ToUpper().Contains(searchBar.Text.ToUpper()) ||
                    x.Codigo.ToUpper().Contains(searchBar.Text.ToUpper())
                )
                    &&
                (
                    x.Compania.ToUpper().Equals(clientesCompania) ||
                    string.IsNullOrEmpty(clientesCompania)
                )
            ));

            TableView.ReloadData();

            // Se quita el Foco de atenci�n del searchBar.
            searchBar.ResignFirstResponder();
        }
        
        /// <summary>
        /// M�todo que llama a cargar los datos de configuraci�n para el cliente y el pedido que se va a realizar.
        /// </summary>
        /// <returns>Retorna si el m�todo complet� o no su ejecuci�n.</returns>
        public bool InicializarPedido()
        {
            try
            {
                ConfigDocCia config = CargarConfiguracionCliente(GlobalUI.ClienteActual.Compania);
                GlobalUI.ClienteActual.ClienteCompania[0].ObtenerDireccionesEntrega();
                config.ClienteCia = GlobalUI.ClienteActual.ClienteCompania[0];
                GestorPedido.PedidosCollection.CargarConfiguracionVenta(GlobalUI.ClienteActual.Compania, config);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// M�todo que carga la configuraci�n del cliente seleccionado para el pedido seg�n la compa��a deseada.
        /// </summary>
        /// <param name="compania">Compa��a por la que buscar la configuraci�n.</param>
        /// <returns>Retorna la configuraci�n del cliente.</returns>
        private ConfigDocCia CargarConfiguracionCliente(string compania)
        {
            return GestorPedido.PedidosCollection.CargarConfiguracionCliente(GlobalUI.ClienteActual, compania);
        }

        /// <summary>
        /// M�todo que se usa para cargar todos los pedidos pendientes y mostrarlos en la tabla.
        /// </summary>
        public void CargarPedidosPendientes()
        {
            pedidosPendientes.Clear();

            pedidosPendientes.AddRange(Pedido.ObtenerPedidos());

            tbv_PedidosPendientes.ReloadData();
        }

        /// <summary>
        /// M�todo que se ejecuta al abrir la secci�n clientes.
        /// Carga todos los clientes disponibles en la base de datos.
        /// </summary>
        public void CargaInicialClientes()
        {
            // Se referencia a la conexi�n con la base de datos para asegurar que est� activa.
            GestorDatos.cnx = AppDelegate.cnx;

            // Determine the correct size to start the overlay (depending on device orientation)
            var bounds = UIScreen.MainScreen.Bounds; // portrait bounds
            if (UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft || UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight)
            {
                bounds.Size = new SizeF((float)bounds.Size.Width, (float)bounds.Size.Height);
            }

            // show the loading overlay on the UI thread using the correct orientation sizing
            loadingOverlay = new LoadingOverlay((RectangleF)bounds);

            ParentViewController.ParentViewController.Add(loadingOverlay);

            loadingOverlay.UpdateMessage("Cargando todos los clientes...");

            InvokeOnMainThread(async () =>
            {
                await Task.Run(() =>
                {
                    ClientesView.clientesCargados.Clear();

                    // Se obtienen los clientes encontrados, y se agregan a la lista.
                    ClientesView.clientesCargados.AddRange(Cliente.CargarClientes(new CriterioBusquedaCliente(CriterioCliente.Ninguno, "", false, false), ""));
                    
                    // Si no hay una compa��a general seleccionada se selecciona autom�ticamente por el usuario.
                    if (string.IsNullOrEmpty(AppDelegate.Compania))
                    {
                        AppDelegate.Compania = Compania.Obtener().ConvertAll(x => x.Codigo)[0];

                        AppDelegate.SetConfigVar(AppDelegate.Compania, "compania");
                    }

                    // Se establece la compa��a que se seleccion� para cargar clientes.
                    CIA = AppDelegate.Compania;

                    InvokeOnMainThread(() =>
                    {
                        // Si no se pudo cargar clientes, se bloquea la b�squeda r�pida de lo contrario se habilita.
                        if (ClientesView.clientesCargados.Count > 0)
                        {
                            searchBar.Placeholder = "B�squeda r�pida de Clientes";

                            searchBar.UserInteractionEnabled = true;
                        }
                        else
                        {
                            searchBar.Placeholder = "No hay clientes disponibles";

                            searchBar.UserInteractionEnabled = false;
                        }

                        QuickSearch();

                        loadingOverlay.Hide();

                        CargarPedidosPendientes();
                    });
                });
            });
        }

        #endregion

        #region View lifecycle

        /// <summary>
        /// M�todo para liberaci�n de memoria de la pantalla cuando se cierra.
        /// </summary>
        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        /// <summary>
        /// M�todo que se ejecuta al crear la pantalla, se ejecuta una �nica vez a menos que la pantalla se remueva del stack.
        /// </summary>
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Inicializar las variables o asignar los datos que se utilizan en la pantalla.

            clientes = new List<Cliente>();

            pedidosPendientes = new List<Pedido>();

            TableView.Source = new TableSource(this);
            TableView.Editing = false;

            tbv_PedidosPendientes.Source = new TableSourcePedidos(this);
            tbv_PedidosPendientes.SeparatorColor = UIColor.FromRGB(225, 106, 12);
            tbv_PedidosPendientes.Layer.BorderColor = UIColor.FromRGB(225, 106, 12).CGColor;
            tbv_PedidosPendientes.Layer.BorderWidth = 2f;
            tbv_PedidosPendientes.Layer.CornerRadius = 3f;
            tbv_PedidosPendientes.Layer.MasksToBounds = true;
            tbv_PedidosPendientes.Editing = false;

            // Se crea un UISearchBar para la b�squeda r�pida de clientes.
            searchBar = new UISearchBar(new RectangleF(0, 0, (float)View.Frame.Width, 50));
            searchBar.SizeToFit();
            searchBar.AutocorrectionType = UITextAutocorrectionType.No;
            searchBar.AutocapitalizationType = UITextAutocapitalizationType.None;
            searchBar.EnablesReturnKeyAutomatically = false;

            // Se asigna el Evento de click en la barra.
            searchBar.SearchButtonClicked += (sender, e) =>
            {
                QuickSearch();
            };

            // Se crea una barra para colocar el bot�n de cancelar en el teclado que abre el control searchBar.
            UIToolbar toolbar = new UIToolbar(new RectangleF(0.0f, 0.0f, (float)this.View.Frame.Size.Width, 44.0f));
            toolbar.TintColor = UIColor.White;
            toolbar.BarStyle = UIBarStyle.Black;
            toolbar.Translucent = true;

            // Se crean los botones de la barra y se asignan los Eventos de los mismos.
            toolbar.Items = new UIBarButtonItem[]{
                new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                new UIBarButtonItem(UIBarButtonSystemItem.Cancel, delegate {
                    searchBar.ResignFirstResponder();
                })
            };

            // Se asgrega la barra para cancelar al teclado, y se asigna el searchBar como el componente del titulo.
            searchBar.InputAccessoryView = toolbar;

            btn_Buscar.TouchUpInside += btn_Buscar_TouchUpInside;
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a aparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            // Si no hay cliente seleccionado para hacer un pedido, se muestra el estado correcto de la pantalla.
            if (GlobalUI.ClienteActual == null)
            {
                // Se valida si se eligi� crear un pedido para mostrar los controles apropiados y refrescar las variables.
                if (crearOrden)
                {
                    btn_Crear_TouchUpInside(btn_Crear);
                }
                else
                {
                    btn_Cancelar_TouchUpInside(btn_Cancelar);
                }
            }
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla apareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            // Si hay un cliente seleccionado para hacer un pedido, se procede a moverse a la pantalla de DetallePedidoView.
            if (GlobalUI.ClienteActual != null)
            {
                btn_Crear_TouchUpInside(btn_Crear);
                
                // Se referencia al Storyboard del app.
                var myStoryboard = AppDelegate.Storyboard;

                // Se instancia un ViewController con el ID del Storyboard y su clase respectiva.
                detallePedidoView = myStoryboard.InstantiateViewController("DetallePedidoView") as DetallePedidoView;

                // Se asignan los valores que se desean pasar a la nueva pantalla.
                detallePedidoView.pedidosView = this;

                // Se navega a la pantalla instanciada agregandola al stack.
                NavigationController.PushViewController(detallePedidoView, true);
            }
            else
            {
                // Si se lleg� originalmente a Pedidos por medio de la secci�n de Clientes.
                if (pedidoDesdeClientes)
                {
                    // Se obtiene la referencia al TabView.
                    var tabController = (UITabBarController)ParentViewController.ParentViewController;

                    try
                    {
                        // Se selecciona que se muestre el tab de Pedidos.
                        tabController.SelectedIndex = 0;

                        // Establecer que ya no se est� en Pedidos por medio de la secci�n de Clientes.
                        pedidoDesdeClientes = false;
                    }
                    catch (Exception ex)
                    {
                        tabController.SelectedIndex = 2;

                        Toast.MakeToast(ParentViewController.View, "Parece haber un problema al cargar la pantalla de Clientes, utilice la barra de navegaci�n para ir a Clientes.", null, UIColor.Black, true, enumDuracionToast.Mediano);
                    }
                }
                else
                {
                    // Se inicia el proceso de intentar sincronizar pedidos pendientes.
                    AppDelegate.IniciarSincronizacionPedidos(this.ParentViewController.ParentViewController);
                }
            }
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a desaparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla desapareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
	}
}
