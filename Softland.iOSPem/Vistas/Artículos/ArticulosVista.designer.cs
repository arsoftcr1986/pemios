// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Softland.iOS.Pem
{
	[Register ("ArticulosVista")]
	partial class ArticulosVista
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_AplicarBusqueda { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_Buscar { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_CancelarBuscar { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_RegresarAPedido { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn_RegresarCriterio { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UICollectionView col_Articulos { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UISegmentedControl seg_Vistas { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView tab_Articulos { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITabBarItem tabArticulos { get; set; }

		[Action ("btn_AplicarBusqueda_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_AplicarBusqueda_TouchUpInside (UIButton sender);

		[Action ("btn_Buscar_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_Buscar_TouchUpInside (UIButton sender);

		[Action ("btn_CancelarBuscar_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_CancelarBuscar_TouchUpInside (UIButton sender);

		[Action ("btn_RegresarAPedido_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_RegresarAPedido_TouchUpInside (UIButton sender);

		[Action ("btn_RegresarCriterio_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void btn_RegresarCriterio_TouchUpInside (UIButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (btn_AplicarBusqueda != null) {
				btn_AplicarBusqueda.Dispose ();
				btn_AplicarBusqueda = null;
			}
			if (btn_Buscar != null) {
				btn_Buscar.Dispose ();
				btn_Buscar = null;
			}
			if (btn_CancelarBuscar != null) {
				btn_CancelarBuscar.Dispose ();
				btn_CancelarBuscar = null;
			}
			if (btn_RegresarAPedido != null) {
				btn_RegresarAPedido.Dispose ();
				btn_RegresarAPedido = null;
			}
			if (btn_RegresarCriterio != null) {
				btn_RegresarCriterio.Dispose ();
				btn_RegresarCriterio = null;
			}
			if (col_Articulos != null) {
				col_Articulos.Dispose ();
				col_Articulos = null;
			}
			if (seg_Vistas != null) {
				seg_Vistas.Dispose ();
				seg_Vistas = null;
			}
			if (tab_Articulos != null) {
				tab_Articulos.Dispose ();
				tab_Articulos = null;
			}
			if (tabArticulos != null) {
				tabArticulos.Dispose ();
				tabArticulos = null;
			}
		}
	}
}
