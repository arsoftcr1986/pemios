// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Softland.iOS.Pem
{
	[Register ("DetalleArticuloTableView")]
	partial class DetalleArticuloTableView
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIImageView img_Imagen { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Codigo { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Compania { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoCodigo { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoCompania { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoDescripcion { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoFamilia { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_DatoSubfamilia { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Descripcion { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel lbl_Subfamilia { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView TableView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView tbv_Bodegas { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView tbv_Precios { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableViewCell tvc_CeldaImagen { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (img_Imagen != null) {
				img_Imagen.Dispose ();
				img_Imagen = null;
			}
			if (lbl_Codigo != null) {
				lbl_Codigo.Dispose ();
				lbl_Codigo = null;
			}
			if (lbl_Compania != null) {
				lbl_Compania.Dispose ();
				lbl_Compania = null;
			}
			if (lbl_DatoCodigo != null) {
				lbl_DatoCodigo.Dispose ();
				lbl_DatoCodigo = null;
			}
			if (lbl_DatoCompania != null) {
				lbl_DatoCompania.Dispose ();
				lbl_DatoCompania = null;
			}
			if (lbl_DatoDescripcion != null) {
				lbl_DatoDescripcion.Dispose ();
				lbl_DatoDescripcion = null;
			}
			if (lbl_DatoFamilia != null) {
				lbl_DatoFamilia.Dispose ();
				lbl_DatoFamilia = null;
			}
			if (lbl_DatoSubfamilia != null) {
				lbl_DatoSubfamilia.Dispose ();
				lbl_DatoSubfamilia = null;
			}
			if (lbl_Descripcion != null) {
				lbl_Descripcion.Dispose ();
				lbl_Descripcion = null;
			}
			if (lbl_Subfamilia != null) {
				lbl_Subfamilia.Dispose ();
				lbl_Subfamilia = null;
			}
			if (TableView != null) {
				TableView.Dispose ();
				TableView = null;
			}
			if (tbv_Bodegas != null) {
				tbv_Bodegas.Dispose ();
				tbv_Bodegas = null;
			}
			if (tbv_Precios != null) {
				tbv_Precios.Dispose ();
				tbv_Precios = null;
			}
			if (tvc_CeldaImagen != null) {
				tvc_CeldaImagen.Dispose ();
				tvc_CeldaImagen = null;
			}
		}
	}
}
