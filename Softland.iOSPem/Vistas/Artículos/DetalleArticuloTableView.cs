using Foundation;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.CodeDom.Compiler;
using UIKit;

using Pedidos.Core;
using BI.Shared;
using System.IO;
using CoreGraphics;

namespace Softland.iOS.Pem
{
	partial class DetalleArticuloTableView : UITableViewController
    {

        #region Propiedades y Contructor de instancia

        /// <summary>
        /// Referencia al art�culo del que se est� viendo su detalle.
        /// </summary>
        public Articulo articulo;

        /// <summary>
        /// Lista de las bodegas en las que se encuentra el art�culo.
        /// </summary>
        public List<string> Bodegas;

        /// <summary>
        /// Lista de referencias de los niveles de precio que tiene el art�culo.
        /// </summary>
        public List<NivelPrecio> NivelesPrecio;

        /// <summary>
        /// Variable que almacena si la moneda del cliente es el dolar.
        /// </summary>
        public bool dolar;

        /// <summary>
        /// Variable que almacena el S�mbolo Monetario de la compa��a del cliente actual del pedido.
        /// </summary>
        public string simboloMonetarioPedido = string.Empty;

        /// <summary>
        /// Costructor por defecto.
        /// </summary>
        /// <param name="handle"></param>
		public DetalleArticuloTableView (IntPtr handle) : base (handle)
		{
        }

        #endregion

        #region Tabla

        /// <summary>
        /// Clase que se utilizar� para manejar la informaci�n de la tabla de Bodegas y Niveles de Precio.
        /// </summary>
        public class TableSource : UITableViewSource
        {

            #region Propiedades y Contructor de instancia

            /// <summary>
            /// Variable para identificar las celdas que utiliza este TableSource.
            /// </summary>
            string cellIdentifier = "";

            /// <summary>
            /// Referencia a la pantalla para poder acceder a sus propiedades desde otra clase. 
            /// </summary>
            DetalleArticuloTableView VistaPadre;

            /// <summary>
            /// Constructor por defecto. 
            /// </summary>
            /// <param name="padre"></param>
            public TableSource(DetalleArticuloTableView padre, string CellID)
            {
                VistaPadre = padre;
                cellIdentifier = CellID;
            }

            #endregion

            #region Eventos

            // Evento requerido por la clase, retorna la cantidad de registros en la fuente de datos.
            public override nint RowsInSection(UITableView tableview, nint section)
            {
                if (cellIdentifier == "BodegaCell")
                {
                    return VistaPadre.Bodegas.Count;
                }
                else
                {
                    return VistaPadre.NivelesPrecio.Count;
                }
            }

            /// <summary>
            /// Evento que se ejecuta al cargar las celdas de la tabla seg�n vayan siendo mostradas.
            /// </summary>
            /// <param name="tableView"></param>
            /// <param name="indexPath">�ndice de la celda por cargar.</param>
            /// <returns>Retorna la celda cargada.</returns>
            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                // Busca la celda creada para ahorrar memoria.
                UITableViewCell cell = tableView.DequeueReusableCell(cellIdentifier);

                // Si no hay celdas por reutilizar, se crea una nueva, en este caso se usa una celda predefinida Subtitle.
                if (cell == null)
                {
                    cell = new UITableViewCell(UITableViewCellStyle.Subtitle, cellIdentifier);

                    cell.TextLabel.Font = UIFont.FromName("Helvetica-Bold", 17f);
                    cell.DetailTextLabel.AdjustsFontSizeToFitWidth = true;
                    cell.DetailTextLabel.MinimumFontSize = 8;

                    cell.DetailTextLabel.Font = UIFont.FromName("Helvetica", 15f);
                    cell.DetailTextLabel.AdjustsFontSizeToFitWidth = true;
                    cell.DetailTextLabel.MinimumFontSize = 8;
                }

                // Se actualiza la informaci�n de la celda seg�n el tipo de celda a mostrar.
                if (cellIdentifier == "BodegaCell")
                {
                    cell.TextLabel.Text = VistaPadre.Bodegas[indexPath.Row].Split(';')[0];
                    cell.DetailTextLabel.Text = "Existencias: " + VistaPadre.Bodegas[indexPath.Row].Split(';')[1];
                }
                else
                {
                    NivelPrecio nivel = VistaPadre.NivelesPrecio[indexPath.Row];

                    // Si la moneda del nivel de precio es dolar, asignar dolar como verdadero.
                    VistaPadre.dolar = nivel.Moneda == TipoMoneda.DOLAR;

                    // Se carga el precio del art�culo.
                    VistaPadre.articulo.CargarPrecio(nivel);

                    cell.TextLabel.Text = nivel.Nivel;

                    // Se muestran los precios seg�n la moneda del cliente.
                    if (VistaPadre.dolar)
                    {
                        cell.DetailTextLabel.Text = "Alm: " + GestorUtilitario.FormatNumeroDolar(VistaPadre.articulo.Precio.Maximo) +
                        " | Det: " + GestorUtilitario.FormatNumeroDolar(VistaPadre.articulo.Precio.Minimo);
                    }
                    else
                    {
                        cell.DetailTextLabel.Text = "Alm: " + GestorUtilitario.FormatNumero(VistaPadre.articulo.Precio.Maximo, 
                                                                                                VistaPadre.simboloMonetarioPedido) +
                        " | Det: " + GestorUtilitario.FormatNumero(VistaPadre.articulo.Precio.Minimo, VistaPadre.simboloMonetarioPedido);
                    }
                }

                return cell;
            }

            #endregion
        }

        #endregion

        #region Eventos

        /// <summary>
        /// Evento que se ejecuta al cambiar la orientaci�n del dispositivo.
        /// Se usa para reacomodar visualmente los controladores, en este caso por medio del m�todo de seg_Vistas.
        /// </summary>
        /// <param name="fromInterfaceOrientation"></param>
        public override void DidRotate(UIInterfaceOrientation fromInterfaceOrientation)
        {
            base.DidRotate(fromInterfaceOrientation);

            TableView.ReloadData();
        }

        #endregion

        #region M�todos

        /// <summary>
        /// M�todo que se encarga de obtener una imagen desde un URL Web y devolverla como objeto UIImage.
        /// </summary>
        /// <param name="uri">Direcci�n Web donde se encuentra la imagen.</param>
        /// <returns>Imagen en formato UIImage</returns>
        static UIImage FromUrl(string uri)
        {
            UIImage imagen;

            using (var url = new NSUrl(uri))
            {
                using (var data = NSData.FromUrl(url))
                {
                    try
                    {
                        imagen = UIImage.LoadFromData(data);
                    }
                    catch (Exception e)
                    {
                        imagen = UIImage.FromFile("imagen_no_disponible.jpg");
                    }
                }

                return imagen;
            }
        }

        /// <summary>
        /// M�todo que se encarga de obtener una imagen desde un directorio en el dispositivo y devolverla como objeto UIImage.
        /// </summary>
        /// <param name="compania">Compa��a del art�culo</param>
        /// <param name="codigo">C�digo del art�culo</param>
        /// <returns>Imagen en formato UIImage</returns>
        static UIImage FromFile(string compania, string codigo)
        {
            UIImage imagen;

            // Directorio de la app en el dispotivo, agregando el nombre de la compa��a para obtener la imagen espec�fica.
            string directorio = Path.Combine(AppDelegate.RutaSD, compania);

            // Si la imagen existe retornarla de lo contrario devolver la imagen b�sica de no disponible.
            if (File.Exists(Path.Combine(directorio, codigo + ".jpg")))
            {
                imagen = UIImage.FromFile(Path.Combine(directorio, codigo + ".jpg"));
            }
            else
            {
                imagen = UIImage.FromFile("imagen_no_disponible.jpg");
            }

            return imagen;
        }
        
        /// <summary>
        /// M�todo que se usa para cargar las bodegas del art�culo y agregar a la lista de bodegas �nicamente
        /// aquellas que al menos tengan al art�culo registrado, selecciona automaticamente la bodega que el art�culo
        /// trae previamente definida.
        /// </summary>
        private void CargarBodegasDisponibles()
        {
            try
            {
                // Variable para almacenar las existencias del art�culo.
                decimal existencias = -1;

                // Se revisa en cada bodega si el art�culo existe, de ser as� se agrega a la lista.
                foreach (Ruta ruta in GlobalUI.Rutas)
                {
                    // Se obtienen las existencias del art�culo en la bodega.
                    articulo.CargarExistenciaPedido(ruta.Bodega);
                    existencias = articulo.Bodega.Existencia;

                    // Si almenos hay 0 o m�s en existencia indica que el art�culo est� registrado en la bodega.
                    if (existencias >= 0)
                    {
                        Bodegas.Add(ruta.Bodega + ";" + GestorUtilitario.FormatMiles(existencias));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error cargando las existencias en planta. " + ex.Message);
            }
        }

        /// <summary>
        /// M�todo que se usa para cargar los niveles de precio del art�culo y agregarlos a la lista,
        /// selecciona automaticamente el primer nivel de precio que se encuentra del art�culo.
        /// </summary>
        private void CargarNivelesPrecio()
        {
            try
            {
                // Se obtiene una lista de todos los niveles de precio establecidos por la compa��a.
                // Se revisa que cada nivel de precio obtenido pueda cargarse el precio del art�culo y se agregan
                // �nicamente los que es posible obtener el precio del art�culo.
                foreach (NivelPrecio precio in NivelPrecio.ObtenerNivelesPrecio(articulo.Compania))
                {
                    // Se intenta cargar el precio del art�culo.
                    if (articulo.CargarPrecio(precio))
                    {
                        NivelesPrecio.Add(precio);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error cargando las existencias en planta. " + ex.Message);
            }
        }

        /// <summary>
        /// M�todo que sirve para mostrar la informaci�n del art�culo en pantalla.
        /// Llamado cada vez que se carga informaci�n diferente del art�culo para refrescar la informaci�n.
        /// </summary>
        public void RefrescarDatosArticulo()
        {
            lbl_DatoCompania.Text = articulo.Compania;
            lbl_DatoCodigo.Text = articulo.Codigo;
            lbl_DatoDescripcion.Text = articulo.Descripcion;

            if (Clasificacion.HayClasificaciones(articulo.Compania, 1, GestorDatos.cnx))
            {
                var clasificacion = Clasificacion.ObtenerClasificaciones(articulo.Compania, 1, GestorDatos.cnx).
                    Find(x => x.Codigo.Equals(articulo.Familia));

                if (clasificacion != null)
                {
                    lbl_DatoFamilia.Text = clasificacion.Descripcion + " | " + articulo.Familia;
                }
                else
                {
                    lbl_DatoFamilia.Text = string.IsNullOrEmpty(articulo.Familia) ? "No disponible." : articulo.Familia;
                }
            }
            else
            {
                lbl_DatoFamilia.Text = string.IsNullOrEmpty(articulo.Familia) ? "No disponible." : articulo.Familia;
            }

            if (Clasificacion.HayClasificaciones(articulo.Compania, 2, GestorDatos.cnx))
            {
                var clasificacion = Clasificacion.ObtenerClasificaciones(articulo.Compania, 2, GestorDatos.cnx).
                    Find(x => x.Codigo.Equals(articulo.Clase));

                if (clasificacion != null)
                {
                    lbl_DatoSubfamilia.Text = clasificacion.Descripcion + " | " + articulo.Clase;
                }
                else
                {
                    lbl_DatoSubfamilia.Text = string.IsNullOrEmpty(articulo.Clase) ? "No disponible." : articulo.Clase;
                }
            }
            else
            {
                lbl_DatoSubfamilia.Text = string.IsNullOrEmpty(articulo.Clase) ? "No disponible." : articulo.Clase;
            }

            TableView.ReloadData();
        }
        
        /// <summary>
        /// M�todo de la clase UITableViewController para definir la altura de las celdas.
        /// Se usa para cambiar la altura de la celda que contiene la imagen al tama�o de la altura de la imagen.
        /// </summary>
        /// <param name="tableView"></param>
        /// <param name="indexPath"></param>
        /// <returns></returns>
        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            // Se acomoda el tama�o de la celda seg�n la celda que sea, Imagen, lista de bodegas y lista de precios.
            if (indexPath.Section == 0 && indexPath.Row == 0)
            {
                return img_Imagen.Bounds.Height+30;
            }
            else if (indexPath.Section == 2 && indexPath.Row == 0)
            {
                return 48 * Bodegas.Count;
            }
            else if (indexPath.Section == 3 && indexPath.Row == 0)
            {
                return 48 * NivelesPrecio.Count;
            }
            else
            {
                return 44;
            }
        }

        #endregion

        #region View lifecycle

        /// <summary>
        /// M�todo para liberaci�n de memoria de la pantalla cuando se cierra.
        /// </summary>
        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        /// <summary>
        /// M�todo que se ejecuta al crear la pantalla, se ejecuta una �nica vez a menos que la pantalla se remueva del stack.
        /// </summary>
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Inicializar las variables o asignar los datos que se utilizan en la pantalla.

            dolar = false;
            
            // Se configura la imagen del art�culo.
            //img_Imagen.Image = FromUrl(string.Format("{0}images/xamarin/{1}/{2}.jpg",
            //      AppDelegate.Servidor, articulo.Compania, articulo.Codigo));
            img_Imagen.Layer.BorderColor = UIColor.DarkGray.CGColor;
            img_Imagen.Layer.BorderWidth = 2f;
            img_Imagen.Layer.CornerRadius = 3f;
            img_Imagen.Layer.MasksToBounds = true;
            img_Imagen.ContentMode = UIViewContentMode.ScaleToFill;
            img_Imagen.Image = FromFile(articulo.Compania, articulo.Codigo);

            Bodegas = new List<string>();

            CargarBodegasDisponibles();

            NivelesPrecio = new List<NivelPrecio>();

            CargarNivelesPrecio();

            tbv_Bodegas.Source = new TableSource(this, "BodegaCell");
            tbv_Bodegas.SeparatorColor = UIColor.FromRGB(225, 106, 12);
            tbv_Bodegas.Layer.BorderColor = UIColor.FromRGB(225, 106, 12).CGColor;
            tbv_Bodegas.Layer.BorderWidth = 2f;
            tbv_Bodegas.Layer.CornerRadius = 3f;
            tbv_Bodegas.Layer.MasksToBounds = true;
            tbv_Bodegas.Bounces = false;

            tbv_Precios.Source = new TableSource(this, "PrecioCell");
            tbv_Precios.SeparatorColor = UIColor.FromRGB(225, 106, 12);
            tbv_Precios.Layer.BorderColor = UIColor.FromRGB(225, 106, 12).CGColor;
            tbv_Precios.Layer.BorderWidth = 2f;
            tbv_Precios.Layer.CornerRadius = 3f;
            tbv_Precios.Layer.MasksToBounds = true;
            tbv_Precios.Bounces = false;

            // Se cargan en pantalla todos los datos del art�culo.
            RefrescarDatosArticulo();
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a aparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            var compania = Compania.Obtener(articulo.Compania);

            if (compania != null)
            {
                simboloMonetarioPedido = compania.SimboloMonetarioFunc;
            }

        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla apareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            // Refrescar la lista para que reacomode la celda de la imagen.
            TableView.ReloadData();

            // Se inicia el proceso de intentar sincronizar pedidos pendientes.
            AppDelegate.IniciarSincronizacionPedidos(this.ParentViewController.ParentViewController);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a desaparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla desapareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
    }
}
