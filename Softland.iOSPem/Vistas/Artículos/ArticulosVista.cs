using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using Pedidos.Core;
using BI.Shared;

using CoreGraphics;
using CoreAnimation;
using Foundation;
using UIKit;

namespace Softland.iOS.Pem
{
    partial class ArticulosVista : UIViewController
    {

        #region Propiedades y Contructor de instancia

        /// <summary>
        /// Referencia a la vista de carga que es mostrada al realizar la carga de art�culos.
        /// </summary>
        public static LoadingOverlay loadingOverlay;

        /// <summary>
        /// Variable que almacena una referencia al identificador de las celdas de la lista.
        /// </summary>
        public NSString cellIdentifier = new NSString("ArticuloCell");

        /// <summary>
        /// Layout que permite acomodar las imagenes en cuadr�cula.
        /// </summary>
        public UICollectionViewFlowLayout LayoutGrid;

        /// <summary>
        /// Layout que permite acomodar las imagenes como una tira y verlas una por una.
        /// </summary>
        public LineLayout LayoutDetalle;

        /// <summary>
        /// Variable que almacena la secci�n o vista seleccionada, 0 Lista, 1 Cuadr�cula o 2 Detalle.
        /// </summary>
        public int vista;

        /// <summary>
        /// Lista de art�culos que almancenar� todos los art�culos disponibles en la compa��a cargada seg�n articulosCompania.
        /// </summary>
        public static List<Articulo> articulosCargados = new List<Articulo>();

        /// <summary>
        /// Lista de art�culos a cargar en los controladores de tabla o colecci�n, se llena con las b�squedas r�pida y avanzada.
        /// </summary>
        public List<Articulo> articulos;

        /// <summary>
        /// Variable que almacena si se lleg� a esta pantalla creando un pedido o por el TabView.
        /// </summary>
        public bool desdeCarrito = false;

        /// <summary>
        /// Variable que almacena el S�mbolo Monetario de la compa��a del cliente actual del pedido al que se le agregan art�culos.
        /// </summary>
        public string simboloMonetarioPedido = string.Empty;

        /// <summary>
        /// Variable que almacena si se est�n cargando m�s art�culos a la lista o no.
        /// </summary>
        public bool cargandoArticulos = false;

        /// <summary>
        /// Variable que almacena si se van a cargar art�culos de una compa��a diferente de la ya cargada en articulosCompania.
        /// </summary>
        public bool cambioCompania = false;

        /// <summary>
        /// Variable que almacena el tama�o de letra a usar para los controladores Label.
        /// </summary>
        public float fontSize;

        /// <summary>
        /// Variable que almacena el tama�o a usar para las imagenes de los art�culos.
        /// </summary>
        public static float itemSize;

        /// <summary>
        /// Variable que almacena la compan�a de los art�culos actualmente cargados.
        /// </summary>
        public static string articulosCompania = string.Empty;

        /// <summary>
        /// Variable que almacena el c�digo del cliente al cu�l se le cargaron los precios de los art�culos actuales.
        /// </summary>
        public static string preciosCliente = string.Empty;

        /// <summary>
        /// Referencia a si mismo para tener acceso a sus variables desde otras clases.
        /// </summary>
        public static ArticulosVista VistaPadre;

        /// <summary>
        /// Variable que almacena los valores para generar el color verde en los mensajes de pantalla de exito.
        /// </summary>
        public static UIColor verdeOscuro = UIColor.FromRGB(0, 200, 0);

        /// <summary>
        /// Controlador para generar la barra de b�squeda r�pida.
        /// </summary>
        private UISearchBar searchBar;

        /// <summary>
        /// Lista de criterios a utilizar para las b�squedas r�pida y avanzada.
        /// </summary>
        public List<Criterios> Filtros1;

        /// <summary>
        /// Lista de subcriterios original a utilizar para las b�squedas r�pida y avanzada.
        /// </summary>
        public List<Clasificacion> Filtros2Inicial;

        /*
         * Caso agregar clasificaciones CR6-24402-L14Y
         * jguzmanc
         * Inicio cambios
         */
        /// <summary>
        /// Lista de subcriterios original a utilizar para las b�squedas r�pida y avanzada.
        /// </summary>
        public List<Clasificacion> Filtros3Inicial;

        /// <summary>
        /// Lista de subcriterios original a utilizar para las b�squedas r�pida y avanzada.
        /// </summary>
        public List<Clasificacion> Filtros4Inicial;

        /// <summary>
        /// Lista de subcriterios original a utilizar para las b�squedas r�pida y avanzada.
        /// </summary>
        public List<Clasificacion> Filtros5Inicial;

        /// <summary>
        /// Lista de subcriterios original a utilizar para las b�squedas r�pida y avanzada.
        /// </summary>
        public List<Clasificacion> Filtros6Inicial;

        /*
         * Caso agregar clasificaciones CR6-24402-L14Y
         * jguzmanc
         * Fin cambios
         */

        /// <summary>
        /// Lista de subcriterios filtrados a utilizar para las b�squedas r�pida y avanzada.
        /// </summary>
        public List<Clasificacion> Filtros2Filtrada;

        /// <summary>
        /// Variable para almacenar el Criterio de b�squeda 1 elegido por el usuario.
        /// </summary>
        public string Criterio1;

        /// <summary>
        /// Variable para almacenar el Criterio de b�squeda 2 elegido por el usuario.
        /// </summary>
        public Clasificacion Criterio2;

        /*
         * Caso agregar clasificaciones CR6-24402-L14Y
         * jguzmanc
         * Inicio cambios
         */

        /// <summary>
        /// Variable para almacenar el Criterio de b�squeda 3 elegido por el usuario.
        /// </summary>
        public Clasificacion Criterio3;

        /// <summary>
        /// Variable para almacenar el Criterio de b�squeda 4 elegido por el usuario.
        /// </summary>
        public Clasificacion Criterio4;

        /// <summary>
        /// Variable para almacenar el Criterio de b�squeda 5 elegido por el usuario.
        /// </summary>
        public Clasificacion Criterio5;

        /// <summary>
        /// Variable para almacenar el Criterio de b�squeda 6 elegido por el usuario.
        /// </summary>
        public Clasificacion Criterio6;

        /*
         * Caso agregar clasificaciones CR6-24402-L14Y
         * jguzmanc
         * Fin cambios
         */

        /// <summary>
        /// Variable para almacenar la lista que debe ser mostrada en mantalla, Filtro1, Filtro2 o Art�culos.
        /// </summary>
        public int listaMostrada;

        /// <summary>
        /// Costructor por defecto.
        /// </summary>
        /// <param name="handle"></param>
		public ArticulosVista(IntPtr handle) : base(handle)
        {
        }

        #endregion

        #region Clase Criterios

        public class Criterios
        {
            public string Nombre { get; set; }
            public List<Clasificacion> SubCriterios { get; set; }
        }

        #endregion

        #region Tabla y Celdas

        /// <summary>
        /// Clase que se utilizar� para manejar la informaci�n de la tabla de art�culos en la vista Lista.
        /// </summary>
        public class TableSource : UITableViewSource
        {

            #region Propiedades y Contructor de instancia

            /// <summary>
            /// Variable para identificar las celdas que utiliza este TableSource.
            /// </summary>
            string cellIdentifier = "TableCell";

            /// <summary>
            /// Diccionario de botones ligados a un filtro aplicado, para poder cancelar del Filtro1.
            /// </summary>
            Dictionary<UIButton, string> botonesCancelarFiltro1 = new Dictionary<UIButton, string>();

            /// <summary>
            /// Diccionario de botones ligados a un filtro aplicado, para poder cancelar del Filtro2.
            /// </summary>
            Dictionary<UIButton, Clasificacion> botonesCancelarFiltro2 = new Dictionary<UIButton, Clasificacion>();

            #endregion

            #region Eventos

            /// <summary>
            /// Evento de click del bot�n btn_CancelarFiltro de las celdas de Filtro1.
            /// Se usa para cancelar un filtro aplicado.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            public void btn_CancelarFiltro1_TouchUpInside(object sender, EventArgs e)
            {
                VistaPadre.Filtros1.Find(x => x.Nombre == botonesCancelarFiltro1[(UIButton)sender]).SubCriterios.Clear();

                botonesCancelarFiltro1.Remove((UIButton)sender);

                VistaPadre.tab_Articulos.ReloadData();
            }

            /// <summary>
            /// Evento de click del bot�n btn_CancelarFiltro de las celdas de Filtro2.
            /// Se usa para cancelar un filtro aplicado.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            public void btn_CancelarFiltro2_TouchUpInside(object sender, EventArgs e)
            {
                VistaPadre.Filtros1.Find(x => x.Nombre == VistaPadre.Criterio1).
                    SubCriterios.Remove(botonesCancelarFiltro2[(UIButton)sender]);

                botonesCancelarFiltro2.Remove((UIButton)sender);

                VistaPadre.tab_Articulos.ReloadData();
            }

            // Evento requerido por la clase, retorna la cantidad de registros en la fuente de datos.
            public override nint RowsInSection(UITableView tableview, nint section)
            {
                switch (VistaPadre.listaMostrada)
                {
                    case 0:
                        return VistaPadre.Filtros1.Count;
                    case 1:
                        return VistaPadre.Filtros2Filtrada.Count;
                    case 2:
                        return VistaPadre.articulos.Count;
                    default:
                        return 0;
                }
            }

            /// <summary>
            /// M�todo de la clase UITableViewController para definir la altura de las celdas.
            /// Se usa para cambiar la altura de la celda que contiene la imagen al tama�o de la altura de la imagen.
            /// </summary>
            /// <param name="tableView"></param>
            /// <param name="indexPath"></param>
            /// <returns></returns>
            public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
            {
                // Se acomoda el tama�o de la celda seg�n la celda que sea, Imagen, lista de bodegas y lista de precios.

                // Se valida el dispositivo actual, iPhone o iPad.
                if (UIDevice.CurrentDevice.Model.Contains("iPhone"))
                {
                    if (VistaPadre.listaMostrada == 0)
                    {
                        return 90;
                    }
                    else
                    {
                        return 75;
                    }
                }
                else
                {
                    if (VistaPadre.listaMostrada == 0)
                    {
                        return 110;
                    }
                    else
                    {
                        return 83;
                    }
                }
            }

            /// <summary>
            /// Evento que se ejecuta al cargar las celdas de la tabla seg�n vayan siendo mostradas.
            /// </summary>
            /// <param name="tableView"></param>
            /// <param name="indexPath">�ndice de la celda por cargar.</param>
            /// <returns>Retorna la celda cargada.</returns>
            public override UITableViewCell GetCell(UITableView tableView, Foundation.NSIndexPath indexPath)
            {
                CustomArticuloCell CeldaArticulo;
                UITableViewCell CeldaFiltro;

                if (VistaPadre.listaMostrada != 2)
                {
                    string titulo = "";
                    string detalle = "";

                    CeldaFiltro = tableView.DequeueReusableCell(cellIdentifier);

                    // Si no hay celdas por reutilizar, se crea una nueva, en este caso se usa una celda predefinida Subtitle.
                    if (CeldaFiltro == null || CeldaFiltro.GetType() == typeof(CustomArticuloCell))
                    {
                        CeldaFiltro = new UITableViewCell(UITableViewCellStyle.Subtitle, cellIdentifier);

                        CeldaFiltro.TextLabel.Font = UIFont.FromName("Helvetica-Bold", 17f);
                        CeldaFiltro.DetailTextLabel.AdjustsFontSizeToFitWidth = true;
                        CeldaFiltro.DetailTextLabel.MinimumFontSize = 8;

                        CeldaFiltro.DetailTextLabel.Font = UIFont.FromName("Helvetica", 15f);
                        CeldaFiltro.DetailTextLabel.AdjustsFontSizeToFitWidth = true;
                        CeldaFiltro.DetailTextLabel.MinimumFontSize = 8;
                    }

                    CeldaFiltro.TextLabel.TextColor = UIColor.Black;
                    CeldaFiltro.DetailTextLabel.TextColor = UIColor.Black;
                    CeldaFiltro.ImageView.Image = null;
                    CeldaFiltro.AccessoryView = null;

                    if (VistaPadre.listaMostrada == 0)
                    {
                        titulo = VistaPadre.Filtros1[indexPath.Row].Nombre;

                        foreach (Clasificacion subcriterio in VistaPadre.Filtros1[indexPath.Row].SubCriterios)
                        {
                            if (!string.IsNullOrEmpty(detalle)) { detalle += ", "; }
                            detalle += subcriterio.Descripcion;
                        }

                        if (!string.IsNullOrEmpty(detalle))
                        {
                            UIButton btn_CancelarFiltro = new UIButton();
                            btn_CancelarFiltro.SetImage(UIImage.FromFile("btn_Cancelar.png"), UIControlState.Normal);
                            btn_CancelarFiltro.TouchUpInside += btn_CancelarFiltro1_TouchUpInside;
                            btn_CancelarFiltro.Frame = new RectangleF((float)tableView.Bounds.Width - (15 - 30),
                                (float)tableView.Bounds.Height / 2 - 15, 30, 30);

                            CeldaFiltro.AccessoryView = btn_CancelarFiltro;

                            botonesCancelarFiltro1.Add(btn_CancelarFiltro, titulo);

                            //CeldaFiltro.BackgroundColor = UIColor.FromRGB(0, 122, 255);
                            CeldaFiltro.TextLabel.TextColor = UIColor.FromRGB(225, 106, 12);
                            CeldaFiltro.DetailTextLabel.TextColor = UIColor.FromRGB(225, 106, 12);
                        }

                        switch (titulo)
                        {
                            case "Familias":
                                CeldaFiltro.ImageView.Image = UIImage.FromFile("tab_familia.png");
                                break;
                            case "Subfamilias":
                                CeldaFiltro.ImageView.Image = UIImage.FromFile("tab_subfamilia.png");
                                break;


                            case "Clasificacion 3":
                                CeldaFiltro.ImageView.Image = UIImage.FromFile("tab_subfamilia.png");
                                break;
                            case "Clasificacion 4":
                                CeldaFiltro.ImageView.Image = UIImage.FromFile("tab_subfamilia.png");
                                break;
                            case "Clasificacion 5":
                                CeldaFiltro.ImageView.Image = UIImage.FromFile("tab_subfamilia.png");
                                break;
                            case "Clasificacion 6":
                                CeldaFiltro.ImageView.Image = UIImage.FromFile("tab_subfamilia.png");
                                break;
                            default:
                                CeldaFiltro.ImageView.Image = UIImage.FromFile("tab_companias.png");
                                break;
                        }
                    }
                    else
                    {
                        titulo = VistaPadre.Filtros2Filtrada[indexPath.Row].Descripcion;

                        detalle = VistaPadre.Filtros2Filtrada[indexPath.Row].Codigo;

                        if (VistaPadre.Filtros1.Find(x => x.Nombre == VistaPadre.Criterio1).SubCriterios.Find(x => x.Descripcion == titulo) != null)
                        {
                            UIButton btn_CancelarFiltro = new UIButton();
                            btn_CancelarFiltro.SetImage(UIImage.FromFile("btn_Cancelar.png"), UIControlState.Normal);
                            btn_CancelarFiltro.TouchUpInside += btn_CancelarFiltro2_TouchUpInside;
                            btn_CancelarFiltro.Frame = new RectangleF((float)tableView.Bounds.Width - (15 - 30),
                                (float)tableView.Bounds.Height / 2 - 15, 30, 30);

                            CeldaFiltro.AccessoryView = btn_CancelarFiltro;

                            botonesCancelarFiltro2.Add(btn_CancelarFiltro, VistaPadre.Filtros2Filtrada[indexPath.Row]);

                            //CeldaFiltro.BackgroundColor = UIColor.FromRGB(0, 122, 255);
                            CeldaFiltro.TextLabel.TextColor = UIColor.FromRGB(225, 106, 12);
                            CeldaFiltro.DetailTextLabel.TextColor = UIColor.FromRGB(225, 106, 12);
                        }
                    }

                    // Se actualiza la informaci�n de la celda.
                    CeldaFiltro.TextLabel.Text = titulo;
                    CeldaFiltro.DetailTextLabel.Text = detalle;

                    return CeldaFiltro;
                }
                else
                {
                    // Busca la celda creada para ahorrar memoria.
                    CeldaArticulo = tableView.DequeueReusableCell(cellIdentifier) as CustomArticuloCell;

                    // Si no hay celdas por reutilizar, se crea una nueva, en este caso se usa una celda predefinida Subtitle.
                    if (CeldaArticulo == null)
                    {
                        CeldaArticulo = new CustomArticuloCell((NSString)cellIdentifier);
                    }

                    CeldaArticulo.BackgroundColor = UIColor.Clear;
                    CeldaArticulo.TextLabel.Text = "";
                    CeldaArticulo.TextLabel.TextColor = UIColor.Black;
                    if (CeldaArticulo.DetailTextLabel != null)
                    {
                        CeldaArticulo.DetailTextLabel.Text = "";
                        CeldaArticulo.DetailTextLabel.TextColor = UIColor.Black;
                    }
                    CeldaArticulo.AccessoryView = null;
                    CeldaArticulo.ImageView.Image = null;

                    // Referencia al art�culo de la celda.
                    var articulo = VistaPadre.articulos[indexPath.Row];

                    // Si se llam� desde pedido valida si el art�culo ha sido seleccionado para agregar al carrito
                    // de ser as� se cambia el color de fondo de la celda para se�alar que esta seleccionado,
                    // verde si no hay problemas o rojo si al ejecutar los c�lculos origin� alg�n problema.
                    if (VistaPadre.desdeCarrito)
                    {
                        bool seleccionado = false;

                        if (VistaPadre.buscarArticuloEnCarrito(articulo) != -1)
                        {
                            seleccionado = true;
                        }

                        if (seleccionado)
                        {
                            // Valida si el art�culo gener� alg�n problema al intentar agregarlo al carrito.
                            if (articulo.Codigo == GestorPedido.codigoArticuloInvalido)
                            {
                                CeldaArticulo.BackgroundColor = UIColor.FromRGB(220, 0, 0);
                            }
                            else
                            {
                                CeldaArticulo.BackgroundColor = verdeOscuro;
                            }
                        }
                    }

                    // Se actualiza la informaci�n de la celda.
                    CeldaArticulo.articulo = indexPath.Row;
                    CeldaArticulo.UpdateCell(articulo.Descripcion, articulo.Codigo, articulo.CodigoBarras,
                        articulo.PrecioMaximo, articulo.PrecioMinimo);

                    return CeldaArticulo;
                }
            }

            /// <summary>
            /// Evento que se ejecuta al seleccionar una celda de la tabla.
            /// </summary>
            /// <param name="tableView"></param>
            /// <param name="indexPath">�ndice de la celda seleccionada.</param>
            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {

                if (VistaPadre.listaMostrada != 2)
                {
                    if (VistaPadre.listaMostrada == 0)
                    {
                        int agrupacion = 0;

                        // Se referencia a la conexi�n con la base de datos para asegurar que est� activa.
                        GestorDatos.cnx = AppDelegate.cnx;

                        VistaPadre.Criterio1 = VistaPadre.Filtros1[indexPath.Row].Nombre;

                        if (VistaPadre.Criterio1.Equals("Compa��as"))
                        {
                            var companias = Compania.Obtener().ConvertAll(x => x.Codigo);

                            // Cargar subcriterios en Filtros2Inicial.
                            VistaPadre.Filtros2Inicial.Clear();
                            foreach (string compania in companias)
                            {
                                VistaPadre.Filtros2Inicial.Add(new Clasificacion("", compania, 0));
                            }

                            VistaPadre.Filtros2Filtrada.Clear();
                            VistaPadre.Filtros2Filtrada.AddRange(VistaPadre.Filtros1.Find(x => x.Nombre == VistaPadre.Criterio1).SubCriterios);
                            VistaPadre.Filtros2Filtrada.AddRange(VistaPadre.Filtros2Inicial.FindAll(x =>
                                VistaPadre.Filtros2Filtrada.Find(y => y.Descripcion.Equals(x.Descripcion)) == null));
                        }
                        else
                        {
                            string CIA = "";

                            if (VistaPadre.desdeCarrito)
                            {
                                CIA = GlobalUI.ClienteActual.Compania;
                            }
                            else
                            {
                                if (VistaPadre.Filtros1.Find(y => y.Nombre == "Compa��as") == null ||
                                    VistaPadre.Filtros1.Find(y => y.Nombre == "Compa��as").SubCriterios.Count == 0 ||
                                    VistaPadre.Filtros1.Find(y => y.Nombre == "Compa��as").SubCriterios[0].Descripcion.Equals(articulosCompania))
                                {
                                    CIA = articulosCompania;
                                }
                                else
                                {
                                    CIA = VistaPadre.Filtros1.Find(y => y.Nombre == "Compa��as").SubCriterios[0].Descripcion;
                                }
                            }

                            switch (VistaPadre.Criterio1)
                            {
                                case "Familias":
                                    agrupacion = 1;
                                    break;
                                case "Subfamilias":
                                    agrupacion = 2;
                                    break;
                                case "Clasificacion 3":
                                    agrupacion = 3;
                                    break;
                                case "Clasificacion 4":
                                    agrupacion = 4;
                                    break;
                                case "Clasificacion 5":
                                    agrupacion = 5;
                                    break;
                                case "Clasificacion 6":
                                    agrupacion = 6;
                                    break;
                            }

                            if (Clasificacion.HayClasificaciones(CIA, agrupacion, GestorDatos.cnx))
                            {
                                // Cargar subcriterios en Filtros2Inicial.
                                VistaPadre.Filtros2Inicial.Clear();
                                VistaPadre.Filtros2Inicial.AddRange(Clasificacion.ObtenerClasificaciones(CIA, agrupacion, GestorDatos.cnx));

                                VistaPadre.Filtros2Filtrada.Clear();
                                VistaPadre.Filtros2Filtrada.AddRange(VistaPadre.Filtros1.Find(x => x.Nombre == VistaPadre.Criterio1).SubCriterios);
                                VistaPadre.Filtros2Filtrada.AddRange(VistaPadre.Filtros2Inicial.FindAll(x =>
                                    VistaPadre.Filtros2Filtrada.Find(y => y.Codigo.Equals(x.Codigo)) == null));
                            }
                            else
                            {
                                VistaPadre.Filtros2Inicial.Clear();
                                VistaPadre.Filtros2Filtrada.Clear();

                                Toast.MakeToast(VistaPadre.View, "No se encuentran los datos de clasificaci�n de " +
                                    (agrupacion == 1 ? "Familias" : "Subfamilias") +
                                    (!string.IsNullOrEmpty(CIA) ? " en la compa��a " + CIA + "." : "."),
                                    null, UIColor.Black, true, enumDuracionToast.Largo);
                            }
                        }

                        VistaPadre.listaMostrada = 1;

                        VistaPadre.searchBar.Text = "";

                        VistaPadre.RefrescarBarraBusqueda();

                        VistaPadre.btn_RegresarCriterio.Hidden = false;

                        botonesCancelarFiltro1.Clear();
                        tableView.ReloadData();
                    }
                    else
                    {
                        VistaPadre.Criterio2 = VistaPadre.Filtros2Filtrada[indexPath.Row];

                        if (VistaPadre.Criterio1.Equals("Compa��as"))
                        {
                            //VistaPadre.Filtros1.Find(x => x.Nombre == VistaPadre.Criterio1).SubCriterios.Clear();
                            foreach (Criterios criterio in VistaPadre.Filtros1)
                            {
                                criterio.SubCriterios.Clear();
                            }

                            VistaPadre.Filtros1.Find(x => x.Nombre == VistaPadre.Criterio1).SubCriterios.Add(VistaPadre.Criterio2);

                            botonesCancelarFiltro2.Clear();
                            tableView.ReloadData();
                        }
                        else
                        {
                            if (VistaPadre.Filtros1.Find(x => x.Nombre == VistaPadre.Criterio1).
                                SubCriterios.Find(x => x.Codigo == VistaPadre.Criterio2.Codigo) == null)
                            {
                                VistaPadre.Filtros1.Find(x => x.Nombre == VistaPadre.Criterio1).SubCriterios.Add(VistaPadre.Criterio2);

                                botonesCancelarFiltro2.Clear();
                                tableView.ReloadData();
                            }
                            else
                            {
                                VistaPadre.Filtros1.Find(x => x.Nombre == VistaPadre.Criterio1).SubCriterios.Remove(VistaPadre.Criterio2);

                                botonesCancelarFiltro2.Clear();
                                tableView.ReloadData();
                            }
                        }

                        VistaPadre.Criterio2 = null;
                    }
                }
                else
                {
                    // Si no se llam� desde pedido, se navega a la pantalla de detalle del art�culo, de lo contrario
                    // se selecciona el art�culo para agregarlo al carrito de compras.
                    if (!VistaPadre.desdeCarrito)
                    {
                        // Se referencia al Storyboard del app.
                        var myStoryboard = AppDelegate.Storyboard;

                        // Se instancia un ViewController con el ID del Storyboard y su clase respectiva.
                        DetalleArticuloTableView detalleArticuloView = myStoryboard.InstantiateViewController("DetalleArticuloTableView") as DetalleArticuloTableView;

                        // Se asignan los valores que se desean pasar a la nueva pantalla.
                        detalleArticuloView.articulo = VistaPadre.articulos[indexPath.Row];

                        // Se navega a la pantalla instanciada agregandola al stack.
                        VistaPadre.NavigationController.PushViewController(detalleArticuloView, true);
                    }
                    else
                    {
                        // Se busca si el art�culo est� ya en el carrito y se obtiene su �ndice en la lista.
                        int index = VistaPadre.buscarArticuloEnCarrito(VistaPadre.articulos[indexPath.Row]);

                        // Si se encuentra en el carrito, se procede a retirar el art�culo del mismo, "Deseleccionarlo",
                        // de lo contrario se procede a agregar el art�culo al carrito, "Seleccionarlo".
                        if (index != -1)
                        {
                            GestorPedido.RetirarDetalle(VistaPadre.articulos[indexPath.Row]);
                            GestorPedido.carrito.RemoveAt(index);

                            VistaPadre.tab_Articulos.ReloadData();
                            //VistaPadre.col_Articulos.ReloadData();

                            Toast.MakeToast(VistaPadre.View, "Art�culo removido del carrito", null, verdeOscuro, true, enumDuracionToast.Corto);
                        }
                        else
                        {
                            UIAlertView alert = new UIAlertView();
                            alert.Title = "Cantidad Art�culo";
                            alert.AddButton("Cancelar");
                            alert.AddButton("Agregar");
                            alert.Message = "Ingrese la cantidad de unidades almac�n del art�culo a agregar:";
                            alert.AlertViewStyle = UIAlertViewStyle.PlainTextInput;
                            alert.GetTextField(0).Placeholder = "1";
                            alert.GetTextField(0).KeyboardType = UIKeyboardType.NumberPad;

                            // Evento click del bot�n de Guardar.
                            alert.Clicked += (object s, UIButtonEventArgs ev) =>
                            {
                                if (ev.ButtonIndex == 1)
                                {
                                    try
                                    {
                                        if (Convert.ToInt64(alert.GetTextField(0).Text.Replace(".", ",")) > 0)
                                        {
                                            VistaPadre.articulos[indexPath.Row].CantidadAlmacen = Convert.ToInt64(alert.GetTextField(0).Text.Replace(".", ","));
                                        }
                                    }
                                    catch
                                    {

                                    }

                                    GestorPedido.carrito.Add(VistaPadre.articulos[indexPath.Row]);

                                    VistaPadre.tab_Articulos.ReloadData();
                                    //VistaPadre.col_Articulos.ReloadData();

                                    Toast.MakeToast(VistaPadre.View, "Art�culo agregado al carrito", null, verdeOscuro, true, enumDuracionToast.Corto);
                                }
                            };

                            alert.Show();
                        }
                    }
                }

                // Se quita el resaltado que se le da a una celda al ser seleccionada (simula comportamiento de iOS).
                VistaPadre.tab_Articulos.DeselectRow(indexPath, true);
            }

            #endregion
        }

        /// <summary>
        /// Clase que se utilizar� para manejar la informaci�n de la celda de la tabla de art�culos en el pedido.
        /// </summary>
        public class CustomArticuloCell : UITableViewCell
        {

            #region Propiedades y Contructor de instancia

            /// <summary>
            /// Controladores para mostrar el nombre y la cantidad en el carrito del art�culo.
            /// </summary>
            UILabel lbl_Codigos, lbl_Descripcion, lbl_Precio;

            /// <summary>
            /// �ndice del art�culo en el carrito.
            /// </summary>
            public int articulo;

            /// <summary>
            /// Constructor por defecto.
            /// </summary>
            /// <param name="cellId"></param>
            public CustomArticuloCell(NSString cellId)
                : base(UITableViewCellStyle.Default, cellId)
            {
                SelectionStyle = UITableViewCellSelectionStyle.Blue;
                ContentView.BackgroundColor = UIColor.Clear;

                lbl_Descripcion = new UILabel()
                {
                    TextAlignment = UITextAlignment.Left,
                    BackgroundColor = UIColor.Clear,
                    Font = UIFont.FromName("Helvetica-Bold", 14f),
                    AdjustsFontSizeToFitWidth = false
                };

                lbl_Codigos = new UILabel()
                {
                    BackgroundColor = UIColor.Clear,
                    TextAlignment = UITextAlignment.Left,
                    Font = UIFont.FromName("Helvetica", 12f),
                    AdjustsFontSizeToFitWidth = true,
                    MinimumFontSize = 8
                };

                lbl_Precio = new UILabel()
                {
                    BackgroundColor = UIColor.Clear,
                    TextAlignment = UITextAlignment.Left,
                    Font = UIFont.FromName("Helvetica", 12f),
                    AdjustsFontSizeToFitWidth = true,
                    MinimumFontSize = 8
                };

                // Se agregan los controladores a la celda.
                ContentView.AddSubviews(new UIView[] { lbl_Codigos, lbl_Descripcion, lbl_Precio });
            }

            #endregion

            #region Eventos

            /// <summary>
            /// Evento que permite acomodar las dimensiones y posici�n de las Subvistas en la celda.
            /// </summary>
            public override void LayoutSubviews()
            {
                base.LayoutSubviews();
                float posicionY = 0;

                // Variable que ayuda para posicionar los controles lo m�s compactos y centrados seg�n el
                // tama�o de la celda y los controladores visibles.
                posicionY = 5 + (string.IsNullOrEmpty(lbl_Precio.Text) ? 15 : 0);

                lbl_Descripcion.Frame = new RectangleF(10, posicionY, (float)ContentView.Bounds.Width - 10, 21);

                posicionY += 21 + 5;

                lbl_Codigos.Frame = new RectangleF(10, posicionY, (float)ContentView.Bounds.Width - 10, 21);

                posicionY += 21 + 5;

                if (!string.IsNullOrEmpty(lbl_Precio.Text))
                {
                    lbl_Precio.Frame = new RectangleF(10, posicionY, (float)ContentView.Bounds.Width - 10, 21);
                }
            }

            #endregion

            #region M�todos

            /// <summary>
            /// M�todo que sirve para actualizar los datos de la celda, llamado al cargar cada celda. 
            /// </summary>
            /// <param name="nombre">Nombre del art�culo.</param>
            /// <param name="cantidad">Cantidad seleccionada del art�culo.</param>
            public void UpdateCell(string descripcion, string codigo, string codigoBarras, decimal precioAlm, decimal precioDet)
            {
                lbl_Descripcion.Text = descripcion;

                lbl_Codigos.Text = "C�digo: " + codigo + " | C�digo de Barras: " + (!string.IsNullOrEmpty(codigoBarras) ? codigoBarras : "No disponible.");

                if (VistaPadre.desdeCarrito)
                {
                    if (GlobalUI.ClienteActual.ClienteCompania[0].Moneda == TipoMoneda.DOLAR)
                    {
                        lbl_Precio.Text = "Precio Alm: " + GestorUtilitario.FormatNumeroDolar(precioAlm) +
                            " | Precio Det: " + GestorUtilitario.FormatNumeroDolar(precioDet);
                    }
                    else
                    {
                        lbl_Precio.Text = "Precio Alm: " + GestorUtilitario.FormatNumero(precioAlm, VistaPadre.simboloMonetarioPedido) +
                            " | Precio Det: " + GestorUtilitario.FormatNumero(precioDet, VistaPadre.simboloMonetarioPedido);
                    }
                }
                else
                {
                    lbl_Precio.Text = "";
                }
            }

            #endregion
        }

        #endregion

        #region ViewSource, Cell & Layout para CollectionView

        /// <summary>
        /// Clase que se utilizar� para manejar la informaci�n de la colecci�n de art�culos en las vistas Cuadr�cula y Detalle.
        /// </summary>
        public class ArticuloSource : UICollectionViewSource
        {

            #region Propiedades y Contructor de instancia

            /// <summary>
            /// Lista que hace referencia a los art�culos obtenidos por las b�squedas.
            /// </summary>
            public List<Articulo> Rows { get; set; }

            /// <summary>
            /// Constructor por defecto. 
            /// </summary>
            public ArticuloSource()
            {
                Rows = new List<Articulo>();
            }

            #endregion

            #region Eventos

            // Evento requerido por la clase, se retorna 1 para tener solo una secci�n.
            public override nint NumberOfSections(UICollectionView collectionView)
            {
                return 1;
            }

            // Evento requerido por la clase, retorna la cantidad de registros en la fuente de datos.
            public override nint GetItemsCount(UICollectionView collectionView, nint section)
            {
                return Rows.Count;
            }

            // Evento requerido por la clase, retorna true para que de un efecto de click en las imagenes.
            public override Boolean ShouldHighlightItem(UICollectionView collectionView, NSIndexPath indexPath)
            {
                return true;
            }

            // Evento requerido por la clase, cambia el valor de Alpha de la imagen para dar un efecto de seleccionado.
            public override void ItemHighlighted(UICollectionView collectionView, NSIndexPath indexPath)
            {
                var cell = (ArticuloCell)collectionView.CellForItem(indexPath);
                cell.ImageView.Alpha = 0.5f;
            }

            // Evento requerido por la clase, cambia el valor de Alpha de la imagen para quitar el efecto de seleccionado.
            public override void ItemUnhighlighted(UICollectionView collectionView, NSIndexPath indexPath)
            {
                var cell = (ArticuloCell)collectionView.CellForItem(indexPath);
                cell.ImageView.Alpha = 1;
            }

            /// <summary>
            /// Evento que se ejecuta al cargar las celdas de la colecci�n seg�n vayan siendo mostradas.
            /// </summary>
            /// <param name="collectionView"></param>
            /// <param name="indexPath">�ndice de la celda por cargar.</param>
            /// <returns>Retorna la celda cargada.</returns>
            public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
            {
                // Busca la celda creada para ahorrar memoria.
                var cell = (ArticuloCell)collectionView.DequeueReusableCell(ArticuloCell.CellID, indexPath);

                // Color b�sico del borde de la imagen si no est� seleccionada.
                UIColor colorTexto = UIColor.DarkGray;

                // Referencia al art�culo de la celda.
                Articulo row = Rows[indexPath.Row];

                // Si se llam� desde pedido valida si el art�culo ha sido seleccionado para agregar al carrito
                // de ser as� se cambia el color de fondo de la celda para se�alar que esta seleccionado,
                // verde si no hay problemas o rojo si al ejecutar los c�lculos origin� alg�n problema.
                if (VistaPadre.desdeCarrito)
                {
                    bool seleccionado = false;

                    if (VistaPadre.buscarArticuloEnCarrito(row) != -1)
                    {
                        seleccionado = true;
                    }

                    if (seleccionado)
                    {
                        if (row.Codigo == GestorPedido.codigoArticuloInvalido)
                        {
                            colorTexto = UIColor.FromRGB(220, 0, 0);
                        }
                        else
                        {
                            colorTexto = verdeOscuro;
                        }
                    }
                }

                // Se actualiza la informaci�n de la celda.
                cell.UpdateRow(row, colorTexto, VistaPadre.fontSize);

                return cell;
            }

            /// <summary>
            /// Evento que se ejecuta al seleccionar una celda de la tabla.
            /// </summary>
            /// <param name="collectionView"></param>
            /// <param name="indexPath">�ndice de la celda seleccionada.</param>
            public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
            {
                // Si no se llam� desde pedido, se navega a la pantalla de detalle del art�culo, de lo contrario
                // se selecciona el art�culo para agregarlo al carrito de compras.
                if (!VistaPadre.desdeCarrito)
                {
                    // Se referencia al Storyboard del app.
                    var myStoryboard = AppDelegate.Storyboard;

                    // Se instancia un ViewController con el ID del Storyboard y su clase respectiva.
                    DetalleArticuloTableView detalleArticuloView = myStoryboard.InstantiateViewController("DetalleArticuloTableView") as DetalleArticuloTableView;

                    // Se asignan los valores que se desean pasar a la nueva pantalla.
                    detalleArticuloView.articulo = Rows[indexPath.Row];

                    // Se navega a la pantalla instanciada agregandola al stack.
                    VistaPadre.NavigationController.PushViewController(detalleArticuloView, true);
                }
                else
                {
                    //int index = GestorPedido.carrito.IndexOf(VistaPadre.articulos[indexPath.Row]);
                    int index = VistaPadre.buscarArticuloEnCarrito(VistaPadre.articulos[indexPath.Row]);

                    // Si se encuentra en el carrito, se procede a retirar el art�culo del mismo, "Deseleccionarlo",
                    // de lo contrario se procede a agregar el art�culo al carrito, "Seleccionarlo".
                    if (index != -1)
                    {
                        GestorPedido.RetirarDetalle(VistaPadre.articulos[indexPath.Row]);
                        GestorPedido.carrito.RemoveAt(index);

                        //VistaPadre.tab_Articulos.ReloadData();
                        VistaPadre.col_Articulos.ReloadData();

                        Toast.MakeToast(VistaPadre.View, "Art�culo removido del carrito", null, verdeOscuro, true, enumDuracionToast.Corto);
                    }
                    else
                    {
                        UIAlertView alert = new UIAlertView();
                        alert.Title = "Cantidad Art�culo";
                        alert.AddButton("Cancelar");
                        alert.AddButton("Agregar");
                        alert.Message = "Ingrese la cantidad de unidades almac�n del art�culo a agregar:";
                        alert.AlertViewStyle = UIAlertViewStyle.PlainTextInput;
                        alert.GetTextField(0).Placeholder = "1";
                        alert.GetTextField(0).KeyboardType = UIKeyboardType.NumberPad;

                        // Evento click del bot�n de Guardar.
                        alert.Clicked += (object s, UIButtonEventArgs ev) =>
                        {
                            if (ev.ButtonIndex == 1)
                            {
                                try
                                {
                                    if (Convert.ToInt64(alert.GetTextField(0).Text.Replace(".", ",")) > 0)
                                    {
                                        VistaPadre.articulos[indexPath.Row].CantidadAlmacen = Convert.ToInt64(alert.GetTextField(0).Text.Replace(".", ","));
                                    }
                                }
                                catch
                                {

                                }

                                GestorPedido.carrito.Add(VistaPadre.articulos[indexPath.Row]);

                                //VistaPadre.tab_Articulos.ReloadData();
                                VistaPadre.col_Articulos.ReloadData();

                                Toast.MakeToast(VistaPadre.View, "Art�culo agregado al carrito", null, verdeOscuro, true, enumDuracionToast.Corto);
                            }
                        };

                        alert.Show();
                    }
                }

                // Se quita el resaltado que se le da a una celda al ser seleccionada (simula comportamiento de iOS).
                VistaPadre.col_Articulos.DeselectItem(indexPath, true);
            }

            #endregion
        }

        /// <summary>
        /// Clase que se utilizar� para manejar la informaci�n de la celda del controlador de colecci�n.
        /// </summary>
        public class ArticuloCell : UICollectionViewCell
        {

            #region Propiedades y Contructor de instancia

            /// <summary>
            /// Variable para identificar las celdas que utiliza este TableSource.
            /// </summary>
            public static NSString CellID = new NSString("ArticuloCell");

            /// <summary>
            /// Controlador que tendr� la imagen del art�culo.
            /// </summary>
            public UIImageView ImageView { get; private set; }

            /// <summary>
            /// Controlador para mostrar la descripci�n del art�culo.
            /// </summary>
            public UILabel LabelViewDescrip { get; private set; }

            /// <summary>
            /// Controlador para mostrar el precio del art�culo.
            /// </summary>
            public UILabel LabelViewPrecio { get; private set; }

            /// <summary>
            /// Constructor por defecto.
            /// </summary>
            /// <param name="frame"></param>
            [Export("initWithFrame:")]
            public ArticuloCell(RectangleF frame)
                : base(frame)
            {
                ImageView = new UIImageView();
                ImageView.Layer.BorderColor = UIColor.Gray.CGColor;
                ImageView.Layer.BorderWidth = 2f;
                ImageView.Layer.CornerRadius = 3f;
                ImageView.Layer.MasksToBounds = true;
                ImageView.ContentMode = UIViewContentMode.ScaleToFill;

                ContentView.AddSubview(ImageView);

                LabelViewDescrip = new UILabel();
                LabelViewDescrip.BackgroundColor = UIColor.Clear;
                LabelViewDescrip.TextColor = UIColor.DarkGray;
                LabelViewDescrip.TextAlignment = UITextAlignment.Left;
                LabelViewDescrip.AdjustsFontSizeToFitWidth = true;
                LabelViewDescrip.MinimumFontSize = 14;

                ContentView.AddSubview(LabelViewDescrip);

                LabelViewPrecio = new UILabel();
                LabelViewPrecio.BackgroundColor = UIColor.Clear;
                LabelViewPrecio.TextColor = UIColor.DarkGray;
                LabelViewPrecio.TextAlignment = UITextAlignment.Left;
                LabelViewPrecio.AdjustsFontSizeToFitWidth = true;
                LabelViewPrecio.MinimumFontSize = 8;

                ContentView.AddSubview(LabelViewPrecio);
            }

            #endregion

            #region M�todos

            /// <summary>
            /// M�todo que sirve para actualizar los datos de la celda, llamado al cargar cada celda. 
            /// </summary>
            /// <param name="element">Art�culo de la celda.</param>
            /// <param name="color">Color del borde de la celda.</param>
            /// <param name="fontSize">Tama�o de letra de la celda.</param>
            public void UpdateRow(Articulo element, UIColor color, Single fontSize)
            {
                LabelViewDescrip.Text = element.Descripcion;
                LabelViewDescrip.TextColor = color;
                LabelViewDescrip.Font = UIFont.FromName("Helvetica-Bold", fontSize);

                if (VistaPadre.desdeCarrito)
                {
                    if (GlobalUI.ClienteActual.ClienteCompania[0].Moneda == TipoMoneda.DOLAR)
                    {
                        LabelViewPrecio.Text = "Alm: " + GestorUtilitario.FormatNumeroDolar(element.PrecioMaximo) +
                            " | Det: " + GestorUtilitario.FormatNumeroDolar(element.PrecioMinimo);
                    }
                    else
                    {
                        LabelViewPrecio.Text = "Alm: " + GestorUtilitario.FormatNumero(element.PrecioMaximo, VistaPadre.simboloMonetarioPedido) +
                            " | Det: " + GestorUtilitario.FormatNumero(element.PrecioMinimo, VistaPadre.simboloMonetarioPedido);
                    }
                }
                else
                {
                    LabelViewPrecio.Text = "";
                }

                LabelViewPrecio.TextColor = color;
                LabelViewPrecio.Font = UIFont.FromName("Helvetica", fontSize);

                ImageView.Image = FromFile(element.Compania, element.Codigo);
                ImageView.Layer.BorderColor = color.CGColor;

                ImageView.Frame = new RectangleF(0, 0, itemSize, itemSize);
                LabelViewDescrip.Frame = new RectangleF(0, (float)ImageView.Frame.Bottom + 5f, itemSize,
                    (float)LabelViewDescrip.Font.CapHeight + 3f);

                if (!string.IsNullOrEmpty(LabelViewPrecio.Text))
                {
                    LabelViewPrecio.Frame = new RectangleF(0, (float)LabelViewDescrip.Frame.Bottom + 5f, itemSize,
                        (float)LabelViewPrecio.Font.CapHeight + 3f);
                }
            }

            #endregion
        }

        /// <summary>
        /// Clase que se utilizar� para manejar el Layout de la colecci�n en la secci�n o vista de Detalle.
        /// </summary>
        public class LineLayout : UICollectionViewFlowLayout
        {

            #region Propiedades y Contructor de instancia

            /// <summary>
            /// Variable que determina el acercamiento en las celdas.
            /// </summary>
            public const float ZOOM_FACTOR = 0f;

            /// <summary>
            /// Constructor por defecto.
            /// </summary>
            public LineLayout()
            {
                ItemSize = new SizeF(itemSize, itemSize);
                ScrollDirection = UICollectionViewScrollDirection.Horizontal;

                float leftRightInset = (float)((VistaPadre.col_Articulos.Bounds.Size.Width / 2.0) - (itemSize / 2));

                // Crea los bordes para centrar la imagen y acomodar sus dimenciones.
                SectionInset = new UIEdgeInsets((float)((VistaPadre.col_Articulos.Bounds.Size.Height / 2.0) - (itemSize / 2)),
                    (float)((VistaPadre.col_Articulos.Bounds.Size.Width / 2.0) - (itemSize / 2)),
                    (float)((VistaPadre.col_Articulos.Bounds.Size.Height / 2.0) - ((itemSize / 2) - 50)),
                    (float)((VistaPadre.col_Articulos.Bounds.Size.Width / 2.0) - (itemSize / 2)));

                // Se define el espacio entre una imagen y la otra, ser el doble del borde
                MinimumLineSpacing = leftRightInset * 2;
            }

            #endregion

            #region Eventos

            // Evento requerido por la clase, retorna true para evitar que el layout reacomode las celdas y mantenga
            // el patr�n de tira de imagenes.
            public override bool ShouldInvalidateLayoutForBoundsChange(CGRect newBounds)
            {
                return true;
            }

            // Evento requerido por la clase, ayuda a comodar las celdas en linea recta y su separacion una de otra.
            public override UICollectionViewLayoutAttributes[] LayoutAttributesForElementsInRect(CGRect rect)
            {
                var array = base.LayoutAttributesForElementsInRect(rect);
                var visibleRect = new RectangleF((PointF)CollectionView.ContentOffset, (SizeF)CollectionView.Bounds.Size);

                foreach (var attributes in array)
                {
                    if (attributes.Frame.IntersectsWith(rect))
                    {
                        float distance = (visibleRect.Width / 2) - (float)attributes.Center.X;
                        float normalizedDistance = distance / itemSize;
                        if (Math.Abs(distance) < itemSize)
                        {
                            float zoom = 1 + ZOOM_FACTOR * (1 - Math.Abs(normalizedDistance));
                            attributes.Transform3D = CATransform3D.MakeScale(zoom, zoom, 1.0f);
                            attributes.ZIndex = 1;
                        }
                    }
                }

                return array;
            }

            // Evento requerido por la clase, ayuda a establecer la posicion de la imagen en el controlador.
            public override CGPoint TargetContentOffset(CGPoint proposedContentOffset, CGPoint scrollingVelocity)
            {
                float offSetAdjustment = float.MaxValue;
                float horizontalCenter = (float)(proposedContentOffset.X + (this.CollectionView.Bounds.Size.Width / 2.0));

                // Crea los bordes para centrar la imagen y acomodar sus dimenciones.
                SectionInset = new UIEdgeInsets((float)((this.CollectionView.Bounds.Size.Height / 2.0) - (itemSize / 2)),
                    (float)((this.CollectionView.Bounds.Size.Width / 2.0) - (itemSize / 2)),
                    (float)((this.CollectionView.Bounds.Size.Height / 2.0) - ((itemSize / 2) - 50)),
                    (float)((this.CollectionView.Bounds.Size.Width / 2.0) - (itemSize / 2)));

                RectangleF targetRect = new RectangleF((float)proposedContentOffset.X, 0.0f, (float)this.CollectionView.Bounds.Size.Width, (float)this.CollectionView.Bounds.Size.Height);
                var array = base.LayoutAttributesForElementsInRect(targetRect);

                foreach (var layoutAttributes in array)
                {
                    float itemHorizontalCenter = (float)layoutAttributes.Center.X;
                    if (Math.Abs(itemHorizontalCenter - horizontalCenter) < Math.Abs(offSetAdjustment))
                    {
                        offSetAdjustment = itemHorizontalCenter - horizontalCenter;
                    }
                }

                return new CGPoint(proposedContentOffset.X + offSetAdjustment, proposedContentOffset.Y);
            }

            #endregion
        }

        #endregion

        #region Metodos

        /// <summary>
        /// M�todo que se encarga de obtener una imagen desde un URL Web y devolverla como objeto UIImage.
        /// </summary>
        /// <param name="uri">Direcci�n Web donde se encuentra la imagen.</param>
        /// <returns>Imagen en formato UIImage</returns>
        static UIImage FromUrl(string uri)
        {
            UIImage imagen;

            using (var url = new NSUrl(uri))
            {
                using (var data = NSData.FromUrl(url))
                {
                    try
                    {
                        imagen = UIImage.LoadFromData(data);
                    }
                    catch (Exception e)
                    {
                        imagen = UIImage.FromFile("imagen_no_disponible.jpg");
                    }
                }

                return imagen;
            }
        }

        /// <summary>
        /// M�todo que se encarga de obtener una imagen desde un directorio en el dispositivo y devolverla como objeto UIImage.
        /// </summary>
        /// <param name="compania">Compa��a del art�culo</param>
        /// <param name="codigo">C�digo del art�culo</param>
        /// <returns>Imagen en formato UIImage</returns>
        static UIImage FromFile(string compania, string codigo)
        {
            UIImage imagen;

            // Directorio de la app en el dispotivo, agregando el nombre de la compa��a para obtener la imagen espec�fica.
            string directorio = Path.Combine(AppDelegate.RutaSD, compania);

            // Si la imagen existe retornarla de lo contrario devolver la imagen b�sica de no disponible.
            //revisi�n para aplicar carga de imagenes caso CR7-25645-HV46
            string codImage = codigo.Replace('/', '-');
            //string pathImage = System.IO.Path.Combine(App.PathImagen, codImage + ".jpg");
            if (File.Exists(Path.Combine(directorio, codImage + ".jpg")))
            {
                imagen = UIImage.FromFile(Path.Combine(directorio, codImage + ".jpg"));
            }
            else
            {
                imagen = UIImage.FromFile("imagen_no_disponible.jpg");
            }

            return imagen;
        }

        /// <summary>
        /// M�todo que se encarga de cambiar el t�ma�o de una imagen a las dimenciones deseadas.
        /// </summary>
        /// <param name="imageData"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public static UIImage ResizeImageIOS(UIImage imageData, float width, float height)
        {
            UIImage originalImage = imageData;

            //create a 24bit RGB image
            using (CGBitmapContext context = new CGBitmapContext(IntPtr.Zero,
                (int)width, (int)height, 8,
                (int)(4 * width), CGColorSpace.CreateDeviceRGB(),
                CGImageAlphaInfo.PremultipliedFirst))
            {

                RectangleF imageRect = new RectangleF(0, 0, width, height);

                // draw the image
                context.DrawImage(imageRect, originalImage.CGImage);

                UIImage resizedImage = UIImage.FromImage(context.ToImage());

                return resizedImage;
            }
        }

        /// <summary>
        /// M�todo que se encarga de buscar si un art�culo se encuentra en el carrito.
        /// </summary>
        /// <param name="pArticulo">Art�culo a buscar.</param>
        /// <returns>�ndice donde se encuentra el art�culo o -1 si no existe.</returns>
        private int buscarArticuloEnCarrito(Articulo pArticulo)
        {
            int index = -1;
            int contador = 0;

            foreach (Articulo art in GestorPedido.carrito)
            {
                if (art.Codigo.Equals(pArticulo.Codigo))
                {
                    index = contador;
                    break;
                }

                contador++;
            }

            return index;
        }

        /// <summary>
        /// M�todo que se ejecuta al iniciar una b�squeda r�pida por alg�n art�culo.
        /// Busca por art�culos seg�n la descripci�n de estos y el dato ingresado en el campo de texto.
        /// </summary>
        public void QuickSearch(bool limpiarLista)
        {
            if (listaMostrada == 2)
            {
                if (Filtros1.Find(y => y.Nombre == "Compa��as") == null ||
                    Filtros1.Find(y => y.Nombre == "Compa��as").SubCriterios.Count == 0 ||
                    Filtros1.Find(y => y.Nombre == "Compa��as").SubCriterios[0].Descripcion.Equals(articulosCompania))
                {
                    VistaPadre.cargandoArticulos = true;

                    articulos.Clear();

                    // Si esta agregando articulos al carrito, agregar al resultado primero los art�culos elegidos.
                    if (desdeCarrito)
                    {
                        articulos.AddRange(GestorPedido.carrito);
                    }

                    // Realizar un filtrado de los art�culos cargados tomando en cuenta que si hay algo escrito en la barra
                    // de b�squeda, aplicando los filtros de familias y subfamilias y por �ltimo validando no agregar los art�culos
                    // que ya estan en el carrito.

                    /*
                    * Caso agregar clasificaciones CR6-24402-L14Y
                    * jguzmanc
                    * Inicio cambios
                    */
                    if (//para saber si se tiene que filtrar
                        Filtros1.Find(y => y.Nombre == "Familias").SubCriterios.Count == 0 &&   
                        Filtros1.Find(y => y.Nombre == "Subfamilias").SubCriterios.Count == 0 &&           
                        Filtros1.Find(y => y.Nombre == "Clasificacion 3").SubCriterios.Count == 0 &&         
                        Filtros1.Find(y => y.Nombre == "Clasificacion 4").SubCriterios.Count == 0 &&          
                        Filtros1.Find(y => y.Nombre == "Clasificacion 5").SubCriterios.Count == 0 &&         
                        Filtros1.Find(y => y.Nombre == "Clasificacion 6").SubCriterios.Count == 0
                        )
                    {
                        foreach (var item in articulosCargados)
                        {
                            if (item.Descripcion.ToUpper().Contains(searchBar.Text.ToUpper()) ||
                                item.Codigo.ToUpper().Contains(searchBar.Text.ToUpper()))
                            {
                                    articulos.Add(item);
                            }
                        }
                        //articulos.AddRange(articulosCargados);
                    }
                    else
                    {
                        foreach (var item in articulosCargados)
                        {
                            if(item.Descripcion.ToUpper().Contains(searchBar.Text.ToUpper()) ||
                                item.Codigo.ToUpper().Contains(searchBar.Text.ToUpper()))
                            {
                                if (Filtros1.Find(y => y.Nombre == "Familias").SubCriterios.Find(f => f.Codigo.Equals(item.Familia)) != null
                                   || Filtros1.Find(y => y.Nombre == "Subfamilias").SubCriterios.Find(f => f.Codigo.Equals(item.Clase)) != null
                                   || Filtros1.Find(y => y.Nombre == "Clasificacion 3").SubCriterios.Find(f => f.Codigo.Equals(item.Clasificacion3)) != null
                                   || Filtros1.Find(y => y.Nombre == "Clasificacion 4").SubCriterios.Find(f => f.Codigo.Equals(item.Clasificacion4)) != null
                                   || Filtros1.Find(y => y.Nombre == "Clasificacion 5").SubCriterios.Find(f => f.Codigo.Equals(item.Clasificacion5)) != null
                                   || Filtros1.Find(y => y.Nombre == "Clasificacion 6").SubCriterios.Find(f => f.Codigo.Equals(item.Clasificacion6)) != null
                                   )
                                {
                                    articulos.Add(item);
                                }
                            }
                        }
                    }

             /*
             * Caso agregar clasificaciones CR6-24402-L14Y
             * jguzmanc
             * Fin cambios
             */

                    //articulos.AddRange(articulosCargados.FindAll
                    //    (
                    //        x =>
                    //        (
                    //            x.Descripcion.ToUpper().Contains(searchBar.Text.ToUpper()) ||
                    //            x.Codigo.ToUpper().Contains(searchBar.Text.ToUpper())
                    //        )
                    //            &&
                    //        (
                    //            (
                    //                Filtros1.Find(y => y.Nombre == "Familias").SubCriterios.Count == 0 ||
                    //                Filtros1.Find(y => y.Nombre == "Familias").SubCriterios.Find(f => f.Codigo.Equals(x.Familia)) != null
                    //            )
                    //                &&
                    //            (
                    //                Filtros1.Find(y => y.Nombre == "Subfamilias").SubCriterios.Count == 0 ||
                    //                Filtros1.Find(y => y.Nombre == "Subfamilias").SubCriterios.Find(s => s.Codigo.Equals(x.Clase)) != null
                    //            )
                    //        )
                    //            &&
                    //        (
                    //            articulos.Find(a => a.Codigo.Equals(x.Codigo)) == null
                    //        )
                    //    )
                    //);

                    VistaPadre.cargandoArticulos = false;

                    // Refrescar los controladores en pantalla.
                    tab_Articulos.ReloadData();
                    col_Articulos.ReloadData();

                    NSIndexPath[] indices = col_Articulos.IndexPathsForVisibleItems;

                    try
                    {
                        // Si hay celdas visibles refrescarlas.
                        if (indices.Length > 0)
                        {
                            if (articulos.Count >= indices.Length)
                            {
                                col_Articulos.ReloadItems(indices);
                            }
                        }
                    }
                    catch (Exception ex)
                    { }
                }
                else
                {
                    // Ya que se seleccion� mostrar y filtrar art�culos por una compa�ia diferente a la general se procede a cargar los datos.
                    CargaInicialArticulos();
                }
            }
            else
            {
                Filtros2Filtrada.Clear();

                // Se agregan primero los subcriterios ya elegidos.
                Filtros2Filtrada.AddRange(Filtros1.Find(x => x.Nombre == Criterio1).SubCriterios);

                // Se agregan los subcriterios que contienen lo escrito en la barra de b�squeda y que no est�n ya agregados.
                Filtros2Filtrada.AddRange(Filtros2Inicial.FindAll(x =>
                    (x.Descripcion.ToUpper().Contains(searchBar.Text.ToUpper()) || x.Codigo.ToUpper().Contains(searchBar.Text.ToUpper()))
                    && Filtros2Filtrada.Find(y => y.Codigo.Equals(x.Codigo)) == null));

                // Refrescar el controlador en pantalla.
                tab_Articulos.ReloadData();
            }

            // Se quita el Foco de atenci�n del searchBar.
            searchBar.ResignFirstResponder();
        }

        /// <summary>
        /// M�todo que se ejecuta al abrir la secci�n art�culos.
        /// Carga todos los articulos disponibles en la compa�ia general o del cliente.
        /// </summary>
        public async void CargaInicialArticulos()
        {
            // Se referencia a la conexi�n con la base de datos para asegurar que est� activa.
            GestorDatos.cnx = AppDelegate.cnx;

            var CIA = "";
            var cargarPrecios = false;
            var grupoArticulos = AppDelegate.UUID;

            if (desdeCarrito)
            {
                CIA = GlobalUI.ClienteActual.Compania;

                cargarPrecios = true;
            }
            else
            {
                // Se establece la compa��a que se seleccion� para cargar art�culos.
                if (Filtros1.Find(y => y.Nombre == "Compa��as") == null ||
                    Filtros1.Find(y => y.Nombre == "Compa��as").SubCriterios.Count == 0 ||
                    Filtros1.Find(y => y.Nombre == "Compa��as").SubCriterios[0].Descripcion.Equals(articulosCompania))
                {
                    if (string.IsNullOrEmpty(articulosCompania))
                    {
                        // Si no hay una compa��a general seleccionada se selecciona autom�ticamente por el usuario.
                        if (string.IsNullOrEmpty(AppDelegate.Compania))
                        {
                            AppDelegate.Compania = Compania.Obtener().ConvertAll(x => x.Codigo)[0];

                            AppDelegate.SetConfigVar(AppDelegate.Compania, "compania");
                        }

                        CIA = AppDelegate.Compania;
                    }
                    else
                    {
                        CIA = articulosCompania;
                    }
                }
                else
                {
                    CIA = Filtros1.Find(y => y.Nombre == "Compa��as").SubCriterios[0].Descripcion;
                }
            }

            // Si la compa��a a cargar no es vac�a.
            if (!string.IsNullOrEmpty(CIA))
            {
                // Si la compa��a a cargar es diferente de la ya cargada o si hay que cargar el precio de los art�culos.
                if (articulosCompania != CIA || (cargarPrecios && !preciosCliente.Equals(GlobalUI.ClienteActual.Codigo)) ||
                    articulosCargados.Count == 0 || (!cargarPrecios && !string.IsNullOrEmpty(preciosCliente)))
                {
                    // Determine the correct size to start the overlay (depending on device orientation)
                    var bounds = UIScreen.MainScreen.Bounds; // portrait bounds
                    if (UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft || UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight)
                    {
                        bounds.Size = new SizeF((float)bounds.Size.Width, (float)bounds.Size.Height);
                    }
                    // show the loading overlay on the UI thread using the correct orientation sizing
                    loadingOverlay = new LoadingOverlay((RectangleF)bounds);

                    // Se agrega la vista de carga seg�n desde d�nde haya sido llamado Art�culos.
                    if (desdeCarrito)
                    {
                        ParentViewController.Add(loadingOverlay);
                    }
                    else
                    {
                        ParentViewController.ParentViewController.Add(loadingOverlay);
                    }

                    // Se muestra el mensaje correspondiente seg�n la carga de datos.
                    if (articulosCompania != CIA)
                    {
                        loadingOverlay.UpdateMessage("Cargando art�culos en " + CIA + "...");
                    }
                    else
                    {
                        loadingOverlay.UpdateMessage("Calculando precio de art�culos...");
                    }

                    cargandoArticulos = true;

                    await Task.Run(() =>
                    {
                        // Cargar todos los art�culos.
                        articulosCargados.Clear();
                        articulosCargados.AddRange(Articulo.ObtenerArticulos(new CriterioBusquedaArticulo(CriterioArticulo.Ninguno, "", false), CIA, grupoArticulos, string.Empty, cargarPrecios));
                        //articulosCargados.AddRange(Articulo.ObtenerArticulosBatch(new CriterioBusquedaArticulo(CriterioArticulo.Ninguno, "", false), CIA, grupoArticulos, string.Empty, cargarPrecios, 1000, 0));

                        cargandoArticulos = false;

                        // Se establece la nueva compa��a de los articulos cargados.
                        articulosCompania = CIA;

                        // Si se carg� los art�culos por el precio, guardar el c�digo del cliente al que se le cargaron.
                        if (cargarPrecios)
                        {
                            preciosCliente = GlobalUI.ClienteActual.Codigo;
                        }
                        else
                        {
                            preciosCliente = string.Empty;
                        }

                        InvokeOnMainThread(() =>
                        {
                            btn_CancelarBuscar_TouchUpInside(btn_CancelarBuscar);

                            QuickSearch(true);

                            //seg_Vistas_ValueChanged(seg_Vistas, null);

                            loadingOverlay.Hide();
                        });
                    });
                }
            }
            else
            {
                string infoAdicional = "";

                if (Filtros1.Find(x => x.Nombre.Equals("Compa��as")) != null)
                {
                    infoAdicional = " o una compa��a en los filtros de b�squeda";
                }

                Toast.MakeToast(View, "Seleccione la compa��a general" + infoAdicional + " para obtener una lista de art�culos.", null, UIColor.Black, true,
                    enumDuracionToast.Largo);
            }
        }

        /// <summary>
        /// M�todo para refrescar si se debe mostrar la barra de b�squeda r�pida y la indicaci�n a mostrar por defecto.
        /// </summary>
        public void RefrescarBarraBusqueda()
        {
            this.NavigationItem.TitleView = searchBar;

            string compania = "";

            if (desdeCarrito)
            {
                compania = GlobalUI.ClienteActual.Compania;
            }
            else
            {
                if (Filtros1.Find(y => y.Nombre == "Compa��as") == null ||
                    Filtros1.Find(y => y.Nombre == "Compa��as").SubCriterios.Count == 0 ||
                    Filtros1.Find(y => y.Nombre == "Compa��as").SubCriterios[0].Descripcion.Equals(articulosCompania))
                {
                    compania = string.IsNullOrEmpty(articulosCompania) ? AppDelegate.Compania : articulosCompania;
                }
                else
                {
                    compania = Filtros1.Find(y => y.Nombre == "Compa��as").SubCriterios[0].Descripcion;
                }
            }

            if (listaMostrada == 2)
            {
                if (string.IsNullOrEmpty(compania))
                {
                    searchBar.Placeholder = "Configure la compa��a default.";

                    searchBar.UserInteractionEnabled = false;
                }
                else
                {
                    if (Filtros1.Find(x => x.SubCriterios.Count > 0) != null)
                    {
                        searchBar.Placeholder = "B�squeda filtrada en " + compania + ".";

                        searchBar.UserInteractionEnabled = true;
                    }
                    else
                    {
                        searchBar.Placeholder = "B�squeda r�pida en " + compania + ".";

                        searchBar.UserInteractionEnabled = true;
                    }
                }
            }
            else if (listaMostrada == 1)
            {
                searchBar.Placeholder = "B�squeda r�pida en " + Criterio1 + ".";

                searchBar.UserInteractionEnabled = true;
            }
            else
            {
                this.NavigationItem.TitleView = null;
            }
        }

        #endregion

        #region Eventos

        /// <summary>
        /// Evento que se ejecuta al cambiar la orientaci�n del dispositivo.
        /// Se usa para reacomodar visualmente los controladores, en este caso por medio del m�todo de seg_Vistas.
        /// </summary>
        /// <param name="fromInterfaceOrientation"></param>
        public override void DidRotate(UIInterfaceOrientation fromInterfaceOrientation)
        {
            base.DidRotate(fromInterfaceOrientation);

            seg_Vistas_ValueChanged(this, null);
        }


        /// <summary>
        /// Evento de click del bot�n btn_RegresarAPedido.
        /// Se usa para regresar a la pantalla del pedido actual y por este medio procesar los art�culos seleccionados
        /// para ser agregados al pedido y que todos los c�lculos requeridos sean ejecutados.
        /// </summary>
        /// <param name="sender">Quien llamo al evento.</param>
        async partial void btn_RegresarAPedido_TouchUpInside(UIButton sender)
        {
            // Determine the correct size to start the overlay (depending on device orientation)
            var bounds = UIScreen.MainScreen.Bounds; // portrait bounds
            if (UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft || UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight)
            {
                bounds.Size = new SizeF((float)bounds.Size.Width, (float)bounds.Size.Height);
            }

            // show the loading overlay on the UI thread using the correct orientation sizing
            loadingOverlay = new LoadingOverlay((RectangleF)bounds);

            ParentViewController.Add(loadingOverlay);

            loadingOverlay.UpdateMessage("Procesando pedido...");

            await Task.Run(() =>
            {
                // Se procesan los art�culos seleccionados y son agregados al pedido, si la ejecuci�n termina exitosamente
                // se cancela el proceso de agregar art�culos y se devuelve a la pantalla del pedido actual.
                if (GestorPedido.AgregarModificarDetalles())
                {
                    GestorPedido.agregandoArticulos = false;
                    desdeCarrito = false;

                    InvokeOnMainThread(() =>
                    {
                        loadingOverlay.Hide();

                        DismissViewController(true, null);
                    });
                }
                else
                {
                    InvokeOnMainThread(() =>
                    {
                        loadingOverlay.Hide();

                        // Se muestra en pantalla un mensaje de error.
                        Toast.MakeToast(View, "Hubo un error al calcular el monto del pedido, retire el art�culo \"" +
                            GestorPedido.descripcionArticuloInvalido + "\" con c�digo \"" + GestorPedido.codigoArticuloInvalido +
                            "\" del carrito. (" + GestorPedido.errorArticuloInvalido + ")", null, UIColor.FromRGB(220, 0, 0), true,
                            enumDuracionToast.Largo);

                        // Refrescar los controladores en pantalla.
                        tab_Articulos.ReloadData();
                        col_Articulos.ReloadData();
                    });
                }
            });
        }

        /// <summary>
        /// Evento que se ejecuta al cambiar el valor de la secci�n seleccionada del control seg_Vistas.
        /// Se usa para actualizar ciertos valores de los controles en la pantalla dependiendo de la secci�n que
        /// el usuario desea ver o de la orientaci�n del dispositivo.
        /// </summary>
        /// <param name="sender">Quien llamo al evento.</param>
        /// <param name="e">Parametos del evento.</param>
        private void seg_Vistas_ValueChanged(object sender, EventArgs e)
        {
            if (listaMostrada == 2)
            {
                // Se valida que solo si el m�todo fue ejecutado por una acci�n del usuario, no por rotaci�n
                // del dispositivo, se actualize el valor de vista.
                if ((int)seg_Vistas.SelectedSegment >= 0)
                {
                    vista = (int)seg_Vistas.SelectedSegment;
                }

                NSIndexPath[] indices = col_Articulos.IndexPathsForVisibleItems;

                try
                {
                    switch (vista)
                    {
                        case 0: // Lista.

                            tab_Articulos.Hidden = false;
                            tab_Articulos.ReloadData();

                            col_Articulos.Hidden = true;

                            break;
                        case 1: // Cuadr�cula.

                            tab_Articulos.Hidden = true;

                            col_Articulos.Hidden = false;

                            // Se calcula el tama�o de las imagenes.
                            itemSize = (((float)col_Articulos.Bounds.Width / 3) * 0.7f);

                            LayoutGrid = new UICollectionViewFlowLayout();

                            // Se valida el dispositivo actual, iPhone o iPad.
                            if (UIDevice.CurrentDevice.Model.Contains("iPhone"))
                            {
                                fontSize = 9f;
                                LayoutGrid.MinimumInteritemSpacing = 20.0f;
                                LayoutGrid.SectionInset = new UIEdgeInsets(20, 20, 40, 20);
                                LayoutGrid.MinimumLineSpacing = 40.0f;
                            }
                            else
                            {
                                fontSize = 17f;
                                LayoutGrid.MinimumInteritemSpacing = 25.0f;
                                LayoutGrid.SectionInset = new UIEdgeInsets(25, 25, 50, 25);
                                LayoutGrid.MinimumLineSpacing = 50.0f;
                            }

                            // Se asigna el tama�o de las imagenes al Layout.
                            LayoutGrid.ItemSize = new CGSize(itemSize, itemSize);

                            // Se actualizan ciertos valores para lograr la visualizacion de las imagenes en cuadr�cula.

                            col_Articulos.SetCollectionViewLayout(LayoutGrid, true);
                            col_Articulos.ShowsHorizontalScrollIndicator = false;
                            col_Articulos.ShowsVerticalScrollIndicator = true;

                            col_Articulos.ReloadItems(indices);

                            break;
                        case 2: // Detalle.

                            tab_Articulos.Hidden = true;

                            col_Articulos.Hidden = false;

                            // Se valida el dispositivo actual, iPhone o iPad.
                            if (UIDevice.CurrentDevice.Model.Contains("iPhone"))
                            {
                                fontSize = 17f;
                            }
                            else
                            {
                                fontSize = 22f;
                            }

                            // Se calcula el tama�o de la imagen seg�n la orientaci�n del dispositivo tomando como prioridad
                            // cu�l dimenci�n hace que la imagen se vea mejor.
                            if (((float)col_Articulos.Bounds.Height * 0.85f) < ((float)col_Articulos.Bounds.Width * 0.85f))
                            {
                                itemSize = ((float)col_Articulos.Bounds.Height * 0.85f);
                            }
                            else
                            {
                                itemSize = ((float)col_Articulos.Bounds.Width * 0.85f);
                            }

                            // Se asigna el tama�o de las imagenes al Layout.
                            LayoutDetalle = new LineLayout();

                            // Se actualizan ciertos valores para lograr la visualizacion de las imagenes en cuadr�cula.

                            col_Articulos.SetCollectionViewLayout(LayoutDetalle, true);
                            col_Articulos.ShowsHorizontalScrollIndicator = true;
                            col_Articulos.ShowsVerticalScrollIndicator = false;

                            col_Articulos.ReloadItems(indices);

                            break;
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        /// <summary>
        /// Evento de click del bot�n btn_CancelarBuscar.
        /// Se usa para cancelar la selecci�n de criterios de b�squeda y regresar a las vistas de art�culos.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_CancelarBuscar_TouchUpInside(UIButton sender)
        {
            listaMostrada = 2;

            seg_Vistas.Hidden = false;
            btn_Buscar.Hidden = false;

            btn_RegresarCriterio.Hidden = true;
            btn_CancelarBuscar.Hidden = true;
            btn_AplicarBusqueda.Hidden = true;

            searchBar.Text = "";

            Criterio1 = null;

            RefrescarBarraBusqueda();

            seg_Vistas_ValueChanged(btn_CancelarBuscar, null);
        }

        /// <summary>
        /// Evento de click del bot�n btn_AplicarBusqueda.
        /// Se usa para cancelar la selecci�n de criterios de b�squeda y regresar a las vistas de art�culos.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_AplicarBusqueda_TouchUpInside(UIButton sender)
        {
            listaMostrada = 2;

            seg_Vistas.Hidden = false;
            btn_Buscar.Hidden = false;

            btn_RegresarCriterio.Hidden = true;
            btn_CancelarBuscar.Hidden = true;
            btn_AplicarBusqueda.Hidden = true;

            searchBar.Text = "";

            Criterio1 = null;

            RefrescarBarraBusqueda();

            // Cargar art�culos seg�n criterios seleccionados.
            QuickSearch(true);

            seg_Vistas_ValueChanged(btn_AplicarBusqueda, null);
        }

        /// <summary>
        /// Evento de click del bot�n btn_RegresarCriterio.
        /// Se usa para regresar al Filtro1 sin cambiar o seleccionar alg�n subcriterio.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_RegresarCriterio_TouchUpInside(UIButton sender)
        {
            listaMostrada = 0;

            btn_RegresarCriterio.Hidden = true;

            RefrescarBarraBusqueda();

            tab_Articulos.ReloadData();
        }

        /// <summary>
        /// Evento de click del bot�n btn_Buscar.
        /// Se usa para mostrar las listas de Filtros para realizar b�squedas filtradas, oculta los controles de art�culos.
        /// </summary>
        /// <param name="sender"></param>
        partial void btn_Buscar_TouchUpInside(UIButton sender)
        {
            listaMostrada = 0;

            col_Articulos.Hidden = true;
            seg_Vistas.Hidden = true;
            btn_Buscar.Hidden = true;

            btn_CancelarBuscar.Hidden = false;
            btn_AplicarBusqueda.Hidden = false;
            tab_Articulos.Hidden = false;

            RefrescarBarraBusqueda();

            tab_Articulos.ReloadData();
        }

        #endregion

        #region View lifecycle

        /// <summary>
        /// M�todo para liberaci�n de memoria de la pantalla cuando se cierra.
        /// </summary>
        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        /// <summary>
        /// M�todo que se ejecuta al crear la pantalla, se ejecuta una �nica vez a menos que la pantalla se remueva del stack.
        /// </summary>
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Inicializar las variables o asignar los datos que se utilizan en la pantalla.

            // Se hace referencia al controlador de la pantalla.
            VistaPadre = this;

            listaMostrada = 2;

            articulos = new List<Articulo>();
            articulos.AddRange(articulosCargados);

            Filtros1 = new List<Criterios>();

            Filtros2Inicial = new List<Clasificacion>();

            Filtros2Filtrada = new List<Clasificacion>();

            tab_Articulos.Source = new TableSource();
            tab_Articulos.Editing = false;

            col_Articulos.RegisterClassForCell(typeof(ArticuloCell), ArticuloCell.CellID);
            col_Articulos.ShowsHorizontalScrollIndicator = false;
            col_Articulos.BackgroundColor = UIColor.Clear;

            var articulosSource = new ArticuloSource();
            articulosSource.Rows = articulos;

            col_Articulos.Source = articulosSource;

            tab_Articulos.Hidden = true;
            col_Articulos.Hidden = false;

            btn_CancelarBuscar.Hidden = true;
            btn_RegresarCriterio.Hidden = true;
            btn_AplicarBusqueda.Hidden = true;

            // Se asigna el Evento de cambio de valor en el selector de vistas.
            seg_Vistas.ValueChanged += seg_Vistas_ValueChanged;

            // Se selecciona la vista de Cuadr�cula como vista inicial.
            seg_Vistas.SelectedSegment = vista = 1;
            seg_Vistas_ValueChanged(seg_Vistas, null);

            // Se crea un UISearchBar para la b�squeda r�pida de art�culos.
            searchBar = new UISearchBar(new RectangleF(0, 0, (float)View.Frame.Width, 50));
            searchBar.SizeToFit();
            searchBar.AutocorrectionType = UITextAutocorrectionType.No;
            searchBar.AutocapitalizationType = UITextAutocapitalizationType.None;
            searchBar.EnablesReturnKeyAutomatically = false;

            // Se asigna el Evento de click en la barra.
            searchBar.SearchButtonClicked += (sender, e) =>
            {
                InvokeOnMainThread(() => { QuickSearch(true); });
            };

            // Se crea una barra para colocar el bot�n de cancelar en el teclado que abre el control searchBar.
            UIToolbar toolbar = new UIToolbar(new RectangleF(0.0f, 0.0f, (float)this.View.Frame.Size.Width, 44.0f));
            toolbar.TintColor = UIColor.White;
            toolbar.BarStyle = UIBarStyle.Black;
            toolbar.Translucent = true;

            // Se crean los botones de la barra y se asignan los Eventos de los mismos.
            toolbar.Items = new UIBarButtonItem[]{
                new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                new UIBarButtonItem(UIBarButtonSystemItem.Cancel, delegate {
                    searchBar.ResignFirstResponder();
                })
            };

            // Se asgrega la barra para cancelar al teclado, y se asigna el searchBar como el componente del t�tulo.
            searchBar.InputAccessoryView = toolbar;
            this.NavigationItem.TitleView = searchBar;
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a aparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            // Se refresca la informaci�n del controlador de la pantalla.
            VistaPadre = this;

            // Se establece si se abri� esta pantalla desde el TabView o desde Pedidos.
            desdeCarrito = GestorPedido.agregandoArticulos;

            // Si se abri� desde pedido, habilitar el bot�n para regresar al pedido y el searchBar se define en la Compa��a
            // del cliente seleccionado.
            if (desdeCarrito)
            {
                btn_RegresarAPedido.Hidden = false;

                if (GestorPedido.carrito == null)
                {
                    GestorPedido.carrito = new List<Articulo>();
                }

                var compania = Compania.Obtener(GlobalUI.ClienteActual.Compania);

                if (compania != null)
                {
                    simboloMonetarioPedido = compania.SimboloMonetarioFunc;
                }
            }
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla apareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            // Agregar opciones de filtros para escoger en la lista de Filtros 1.
            if (Filtros1.Count == 0)
            {
                if (!desdeCarrito)
                {
                    // Si hay m�s de una compa��a para escoger se agrega el filtro de compa��as.
                    if (Compania.Obtener().ConvertAll(x => x.Codigo).Count > 1)
                    {
                        Filtros1.Add(new Criterios() { Nombre = "Compa��as", SubCriterios = new List<Clasificacion>() });
                    }
                }

                Filtros1.Add(new Criterios() { Nombre = "Familias", SubCriterios = new List<Clasificacion>() });
                Filtros1.Add(new Criterios() { Nombre = "Subfamilias", SubCriterios = new List<Clasificacion>() });

                Filtros1.Add(new Criterios() { Nombre = "Clasificacion 3", SubCriterios = new List<Clasificacion>() });
                Filtros1.Add(new Criterios() { Nombre = "Clasificacion 4", SubCriterios = new List<Clasificacion>() });
                Filtros1.Add(new Criterios() { Nombre = "Clasificacion 5", SubCriterios = new List<Clasificacion>() });
                Filtros1.Add(new Criterios() { Nombre = "Clasificacion 6", SubCriterios = new List<Clasificacion>() });

            }

            // Se cargan de ser necesario todos los art�culos disponibles para el usuario.
            CargaInicialArticulos();

            RefrescarBarraBusqueda();

            // Refrescar la informaci�n de los art�culos y las vistas.
            seg_Vistas_ValueChanged(seg_Vistas, null);

            // Se inicia el proceso de intentar sincronizar pedidos pendientes.
            if (desdeCarrito)
            {
                AppDelegate.IniciarSincronizacionPedidos(this);
            }
            else
            {
                AppDelegate.IniciarSincronizacionPedidos(this.ParentViewController.ParentViewController);
            }
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla va a desaparecer.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        /// <summary>
        /// M�todo que se ejecuta cuando la pantalla desapareci�.
        /// </summary>
        /// <param name="animated">Si debe tener animaci�n.</param>
        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
    }
}
