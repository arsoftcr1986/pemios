using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using ExternalAccessory;

namespace Softland.iOS.Pem
{
    public class ZebraPrinter : NSStreamDelegate
    {
        static public EASession PrinterSession { get; set; }

        private string printCommand;

        public override void HandleEvent(NSStream theStream, NSStreamEvent streamEvent)
        {

            if (printCommand == null)
                return;
            if (streamEvent == NSStreamEvent.HasSpaceAvailable)
            {
                var stream = theStream as NSOutputStream;
                if (stream != null)
                {

                    byte[] mybytes = Encoding.Default.GetBytes(printCommand);
                    stream.Write(mybytes, (uint)mybytes.Length);
                    printCommand = null;
                }
            }
        }

        // This is THE method :)
        public void PrintIt(string printingString)
        {
            printCommand = printingString;
            if (ZebraPrinter.PrinterSession == null)
            {
                CreatePrinterSession();
            }
            else
            {
                HandleEvent(ZebraPrinter.PrinterSession.OutputStream, NSStreamEvent.HasSpaceAvailable);
            }
        }

        class EAAccessoryDelegateHandler : EAAccessoryDelegate
        {

            public void DisposeSession()
            {
                var session = ZebraPrinter.PrinterSession;
                if (session == null)
                {
                    return;
                }
                try
                {
                    if (session.OutputStream != null)
                    {
                        session.OutputStream.Close();
                        session.OutputStream.Unschedule(NSRunLoop.Current, NSRunLoop.NSDefaultRunLoopMode);
                        session.OutputStream.Delegate = null;
                    }
                    session.Dispose();
                    ZebraPrinter.PrinterSession = null;
                }
                catch (Exception ee)
                {
                    //Logger.CreateLog(ee);
                }
            }

            public override void Disconnected(EAAccessory accessory)
            {
                DisposeSession();
                accessory.Delegate = null;
            }
        }

        void CreatePrinterSession()
        {
            try
            {
                var _accessoryList = EAAccessoryManager.SharedAccessoryManager.ConnectedAccessories;

                EAAccessory accessory = null;
                foreach (var obj in _accessoryList)
                {
                    EAAccessory a = obj;
                    if (obj.ProtocolStrings.Contains("com.zebra.rawport"))
                    {
                        accessory = obj;
                        break;
                    }
                }
                if (accessory == null)
                {
                    UIAlertView alert = new UIAlertView();
                    alert.Title = "Error";
                    alert.Message = "no printer found";
                    alert.AddButton("Ok");
                    alert.Show();
                    return;
                }
                try
                {                    
                    accessory.Delegate = new EAAccessoryDelegateHandler();
                    ZebraPrinter.PrinterSession = new EASession(accessory, "com.zebra.rawport");
                    var mysession = ZebraPrinter.PrinterSession;
                    if (mysession.OutputStream == null) {}
                        //Logger.CreateLog("output is null");
                    else{}
                        //Logger.CreateLog("output not null");
                    mysession.OutputStream.Delegate = this;
                    mysession.OutputStream.Schedule(NSRunLoop.Current, NSRunLoop.NSDefaultRunLoopMode);
                    mysession.OutputStream.Open();
                    //Logger.CreateLog("mysession.OutputStream.HasSpaceAvailable() :" + mysession.OutputStream.HasSpaceAvailable().ToString());
                    if (mysession.OutputStream.Error != null) { }
                        //Logger.CreateLog(mysession.OutputStream.Error.DebugDescription + " | " + mysession.OutputStream.Error.Description + " | " + mysession.OutputStream.Error.LocalizedDescription + " | ");
                }
                catch (Exception ee)
                {
                    //Logger.CreateLog(ee);
                }
            }
            catch (Exception ex)
            {
                //Logger.CreateLog(ex);
            }
            //Logger.CreateLog("Finished creating session");
        }
    }
}