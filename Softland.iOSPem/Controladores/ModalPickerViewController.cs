﻿/*
 * Copyright (C) 2014 
 * Author: Ruben Macias
 * http://sharpmobilecode.com @SharpMobileCode
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

using System;
using UIKit;
using System.Drawing;
using Foundation;

namespace Softland.iOS.Pem
{
    public delegate void ModalPickerDimissedEventHandler(object sender, EventArgs e);

    public class ModalPickerViewController : UIViewController
    {
        public event ModalPickerDimissedEventHandler OnModalPickerDismissed;
        const float _headerBarHeight = 40;

        public UIColor HeaderBackgroundColor { get; set; }
        public UIColor InternalBackgroundColor { get; set; }
        public UIColor HeaderTextColor { get; set; }
        public string HeaderText { get; set; }

        public UIDatePicker DatePicker { get; set; }
        public UIPickerView PickerView { get; set; }
        private ModalPickerType _pickerType;
        public ModalPickerType PickerType 
        { 
            get { return _pickerType; }
            set
            {
                switch(value)
                {
                    case ModalPickerType.Date:
                        DatePicker = new UIDatePicker(RectangleF.Empty);
                        PickerView = null;
                        break;
                    case ModalPickerType.Custom:
                        DatePicker = null;
                        PickerView = new UIPickerView(RectangleF.Empty);
                        break;
                    default:
                        break;
                }

                _pickerType = value;
            }
        }

        UILabel _headerLabel;
        UIButton _doneButton;
        UIViewController _parent;
        UIView _internalView;
        UIView _headerView;

        private bool tamanoHeaderCalculado = false;

        public ModalPickerViewController(ModalPickerType pickerType, string headerText, UIViewController parent)
        {
            HeaderBackgroundColor = UIColor.White;
            HeaderTextColor = UIColor.Black;
            HeaderText = headerText;
            PickerType = pickerType;
            _parent = parent;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            InitializeControls();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            Show();
        }

        void InitializeControls()
        {
            View.BackgroundColor = UIColor.Clear;
            _internalView = new UIView();
            _headerView = new UIView();

            _headerLabel = new UILabel(new RectangleF(0, 0, 320/2, 44));
            _headerLabel.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            _headerLabel.BackgroundColor = HeaderBackgroundColor;
            _headerLabel.TextColor = HeaderTextColor;
            _headerLabel.Text = HeaderText;

            var lang = NSLocale.PreferredLanguages[0];

            var textoBotonDone = lang.Equals("en") ? "Done" : "Listo";

            _doneButton = UIButton.FromType(UIButtonType.System);
            _doneButton.SetTitleColor(HeaderTextColor, UIControlState.Normal);
            _doneButton.BackgroundColor = HeaderBackgroundColor;
            _doneButton.SetTitle(textoBotonDone, UIControlState.Normal);
            _doneButton.TouchUpInside += DoneButtonTapped;

            switch(PickerType)
            {
                case ModalPickerType.Date:
                    DatePicker.BackgroundColor = UIColor.White;
                    _internalView.AddSubview(DatePicker);
                    break;
                case ModalPickerType.Custom:
                    PickerView.BackgroundColor = UIColor.White;
                    _internalView.AddSubview(PickerView);
                    break;
                default:
                    break;
            }

            _internalView.BackgroundColor = InternalBackgroundColor;
            _headerView.BackgroundColor = HeaderBackgroundColor;

            _headerView.AddSubview(_headerLabel);
            _headerView.AddSubview(_doneButton);

            Add(_internalView);
            Add(_headerView);
        }

        void Show(bool onRotate = false)
        {
            var doneButtonSize = new Size(71, 30);

            // Se obtiene un tamaño relavito a la Orientación para el ancho del control Picker interno.
            var width = UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.Portrait ?
                _parent.View.Frame.Width : _parent.View.Frame.Height;

            // Variable que guardará temporalmente la altura maxima de todo el control incluyendo la barra del Header.
            float height = 0;

            // Tamaño del panel interno que contendrá el Picker.
            var internalViewSize = SizeF.Empty;
            var headerViewSize = SizeF.Empty;

            switch(_pickerType)
            {
                case ModalPickerType.Date:
                    height = (float)DatePicker.Frame.Height;

                    if (!tamanoHeaderCalculado)
                    {
                        height += _headerBarHeight;
                        tamanoHeaderCalculado = true;
                    }

                    internalViewSize = new SizeF((float)width, height);
                    DatePicker.Frame = RectangleF.Empty;
                    break;
                case ModalPickerType.Custom:
                    height = (float)PickerView.Frame.Height;

                    if (!tamanoHeaderCalculado)
                    {
                        height += _headerBarHeight;
                        tamanoHeaderCalculado = true;
                    }

                    internalViewSize = new SizeF((float)width, height);
                    PickerView.Frame = RectangleF.Empty;
                    break;
                default:
                    break;
            }

            headerViewSize = new SizeF((float)width, _headerBarHeight);

            // Tamaño del panel que contendrá al panel que se le asigna el Picker.
            var internalViewFrame = RectangleF.Empty;
            var headerViewFrame = RectangleF.Empty;

            internalViewFrame = new RectangleF(0, (float)View.Bounds.Height - internalViewSize.Height,
                                                       (float)View.Bounds.Width, internalViewSize.Height);

            headerViewFrame = new RectangleF(0, (float)View.Bounds.Height - internalViewSize.Height,
                                                       (float)View.Bounds.Width, headerViewSize.Height);

            _internalView.Frame = internalViewFrame;
            _headerView.Frame = headerViewFrame;

            // Se crea el Frame para posicionar el Picker dentro de su panel, centrado y con un tamaño relativo al panel que lo contiene
            // para mostrarlo centrado y no que rellena todo el espacio.
            switch(_pickerType)
            {
                case ModalPickerType.Date:
                    DatePicker.Frame = new RectangleF((float)View.Bounds.Width / 2 - (float)width / 2,
                        _headerBarHeight, (float)width, (float)_internalView.Frame.Height);
                    break;
                case ModalPickerType.Custom:
                    PickerView.Frame = new RectangleF((float)View.Bounds.Width / 2 - (float)width / 2,
                        _headerBarHeight, (float)width, (float)_internalView.Frame.Height);
                    break;
                default:
                    break;
            }

            _headerLabel.Frame = new RectangleF(10, 4, (float)_parent.View.Frame.Width - 100, 35);
            _doneButton.Frame = new RectangleF(internalViewFrame.Width - doneButtonSize.Width - 10, 7, 
                doneButtonSize.Width, doneButtonSize.Height);
        }

        void DoneButtonTapped (object sender, EventArgs e)
        {
            DismissViewController(true, null);
            if(OnModalPickerDismissed != null)
            {
                OnModalPickerDismissed(sender, e);
            }
        }

        public override void DidRotate(UIInterfaceOrientation fromInterfaceOrientation)
        {
            base.DidRotate(fromInterfaceOrientation);

            if (InterfaceOrientation == UIInterfaceOrientation.Portrait ||
                InterfaceOrientation == UIInterfaceOrientation.LandscapeLeft ||
                InterfaceOrientation == UIInterfaceOrientation.LandscapeRight)
            {
                Show(true);
                View.SetNeedsDisplay();
            }
        }
    }

    public enum ModalPickerType
    {
        Date = 0,
        Custom = 1
    }
}

