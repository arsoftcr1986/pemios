using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using System.Collections;

namespace Softland.iOS.Pem
{
    public class PickerChangedEventArgs : EventArgs
    {
        public string SelectedValue { get; set; }
    }

    public class PickerModel : UIPickerViewModel
    {
        public readonly IList values;

        public event EventHandler PickerChanged;

        public PickerModel(IList values)
        {
            this.values = values;
        }

        public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
        {
            return values.Count;
        }

        public override string GetTitle(UIPickerView pickerView, nint row, nint component)
        {
            return (string)values[(int)row];
        }

        public override nfloat GetRowHeight(UIPickerView pickerView, nint component)
        {
            return 40f;
        }

        public override nint GetComponentCount(UIPickerView pickerView)
        {
            return 1;
        }

        public override void Selected(UIPickerView pickerView, nint row, nint component)
        {
            if (this.PickerChanged != null)
            {
                this.PickerChanged(this, new PickerChangedEventArgs { SelectedValue = (string)values[(int)row] });
            }
        }
    }
}