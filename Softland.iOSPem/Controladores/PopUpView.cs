using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using Foundation;
using UIKit;

namespace Softland.iOS.Pem
{
    public class PopUpView : UIView
    {

        #region Propiedades

        /// <summary>
        /// Referencia al control Vista principal en la que se muestra la informaci�n de la ventana PopUp.
        /// </summary>
        private UIView VistaPrincipal;

        /// <summary>
        /// Referencia al control Vista que se muestra para el fondo de la ventana PopUp.
        /// </summary>
        private UIView VistaFondo;

        /// <summary>
        /// Referencia al control Vista sobre la que se muestra el bot�n de cerrar la ventana PopUp.
        /// </summary>
        private UIView VistaBotonCerrar;

        /// <summary>
        /// Referencia al control del Bot�n para cerrar la ventana PopUp.
        /// </summary>
        private UIButton btn_CerrarVentana;

        /// <summary>
        /// Variable que almacena el valor en seg�ndos de la duraci�n de la animaci�n de cerrar la ventana PopUp..
        /// </summary>
        private float TiempoAnimacionHide = 0.5f;

        /// <summary>
        /// Variable que almacena el valor del radio del borde de la vista principal de la ventana.
        /// </summary>
        private float RadioBordePrincipal = 10f;

        /// <summary>
        /// Constructor de la clase que recibe un par�metro b�sico que es la vista a mostrar en el PopUp.
        /// </summary>
        /// <param name="frame">Dimensiones de la pantalla.</param>
        /// <param name="VistaMostrar">Referencia a la UIView a mostrar como vista del PopUp.</param>
        public PopUpView(RectangleF frame, UIInterfaceOrientation Orientacion, UIView VistaMostrar)
            : base(frame)
        {
            Inicializar(frame, Orientacion, VistaMostrar, 0.5f, 0.5f, 2.5f, 0.9f, UIColor.DarkGray, UIColor.White, UIColor.Black);
        }

        /// <summary>
        /// Constructor de la clase que recibe por par�metro la vista a mostrar en el PopUp, duraci�n de la animaci�n, 
        /// transparencia del fondo, ancho de los bordes y tama�o relativo a las dimensiones de la pantalla.
        /// </summary>
        /// <param name="frame">Dimensiones de la pantalla.</param>
        /// <param name="VistaMostrar">Referencia a la UIView a mostrar como vista del PopUp.</param>
        /// <param name="animacionHide">Valor en segundos de la duraci�n de la animaci�n de cerrar la ventana PopUp.</param>
        /// <param name="trasparenciaFondo">Valor entre 0 y 1 de la transparencia que tendr�a el fondo del PopUp.</param>
        /// <param name="anchoBorde">Valor del ancho de los bordes de las vistas predeterminadas del PopUp.</param>
        /// <param name="tamanoRelativo">Valor entre 0 y 1 del tama�o proporcional a calcular sobre las dimensiones de la 
        /// pantalla para el PopUp.</param>
        public PopUpView(RectangleF frame, UIInterfaceOrientation Orientacion, UIView VistaMostrar, float animacionHide, float trasparenciaFondo, float anchoBorde,
            float tamanoRelativo)
            : base(frame)
        {
            Inicializar(frame, Orientacion, VistaMostrar, animacionHide, trasparenciaFondo, anchoBorde, tamanoRelativo, 
                UIColor.DarkGray, UIColor.White, UIColor.Black);
        }

        /// <summary>
        /// Constructor de la clase que recibe por par�metro la vista a mostrar en el PopUp, color de los bordes, 
        /// color de fondo de la vista principal y color de fondo de la vista de fondo.
        /// </summary>
        /// <param name="frame">Dimensiones de la pantalla.</param>
        /// <param name="VistaMostrar">Referencia a la UIView a mostrar como vista del PopUp.</param>
        /// <param name="colorBordeVistaPrincipal">UIColor que representa el color de los bordes de las vistas predeterminadas del PopUp.</param>
        /// <param name="colorVistaPopUp">UIColor que representa el color de fondo de la vistas principal del PopUp.</param>
        /// <param name="colorFondoPopUp">UIColor que representa el color de fondo de la vistas de fondo del PopUp.</param>
        public PopUpView(RectangleF frame, UIInterfaceOrientation Orientacion, UIView VistaMostrar, UIColor colorBordeVistaPrincipal, UIColor colorVistaPopUp,
            UIColor colorFondoPopUp)
            : base(frame)
        {
            Inicializar(frame, Orientacion, VistaMostrar, 0.5f, 0.5f, 2.5f, 0.9f, colorBordeVistaPrincipal, colorVistaPopUp, colorFondoPopUp);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="frame">Dimensiones de la pantalla.</param>
        /// <param name="VistaMostrar">Referencia a la UIView a mostrar como vista del PopUp.</param>
        /// <param name="animacionHide">Valor en segundos de la duraci�n de la animaci�n de cerrar la ventana PopUp.</param>
        /// <param name="trasparenciaFondo">Valor entre 0 y 1 de la transparencia que tendr�a el fondo del PopUp.</param>
        /// <param name="anchoBorde">Valor del ancho de los bordes de las vistas predeterminadas del PopUp.</param>
        /// <param name="tamanoRelativo">Valor entre 0 y 1 del tama�o proporcional a calcular sobre las dimensiones de la 
        /// pantalla para el PopUp.</param>
        /// <param name="colorBordeVistaPrincipal">UIColor que representa el color de los bordes de las vistas predeterminadas del PopUp.</param>
        /// <param name="colorVistaPopUp">UIColor que representa el color de fondo de la vistas principal del PopUp.</param>
        /// <param name="colorFondoPopUp">UIColor que representa el color de fondo de la vistas de fondo del PopUp.</param>
        public PopUpView(RectangleF frame, UIInterfaceOrientation Orientacion, UIView VistaMostrar, float animacionHide, float trasparenciaFondo, float anchoBorde,
            float tamanoRelativo, UIColor colorBordeVistaPrincipal, UIColor colorVistaPopUp, UIColor colorFondoPopUp)
            : base(frame)
        {
            Inicializar(frame, Orientacion, VistaMostrar, animacionHide, trasparenciaFondo, anchoBorde, tamanoRelativo,
                colorBordeVistaPrincipal, colorVistaPopUp, colorFondoPopUp);
        }

        #endregion

        #region M�todos

        /// <summary>
        /// M�todo para inicializar el PopUp seg�n los parametros enviados.
        /// </summary>
        /// <param name="frame">Dimensiones de la pantalla.</param>
        /// <param name="VistaMostrar">Referencia a la UIView a mostrar como vista del PopUp.</param>
        /// <param name="animacionHide">Valor en segundos de la duraci�n de la animaci�n de cerrar la ventana PopUp.</param>
        /// <param name="trasparenciaFondo">Valor entre 0 y 1 de la transparencia que tendr�a el fondo del PopUp.</param>
        /// <param name="anchoBorde">Valor del ancho de los bordes de las vistas predeterminadas del PopUp.</param>
        /// <param name="tamanoRelativo">Valor entre 0 y 1 del tama�o proporcional a calcular sobre las dimensiones de la 
        /// pantalla para el PopUp.</param>
        /// <param name="colorBordeVistaPrincipal">UIColor que representa el color de los bordes de las vistas predeterminadas del PopUp.</param>
        /// <param name="colorVistaPopUp">UIColor que representa el color de fondo de la vistas principal del PopUp.</param>
        /// <param name="colorFondoPopUp">UIColor que representa el color de fondo de la vistas de fondo del PopUp.</param>
        private void Inicializar(RectangleF frame, UIInterfaceOrientation Orientacion, UIView VistaMostrar, float animacionHide,
            float trasparenciaFondo, float anchoBorde, float tamanoRelativo, UIColor colorBordeVistaPrincipal, UIColor colorVistaPopUp,
            UIColor colorFondoPopUp)
        {
            // Variables para determinar el ancho y alto de la ventana principal.
            float VPAlto = (float)frame.Height * tamanoRelativo;    // Alto de la vista principal.
            float VPAncho = (float)frame.Width * tamanoRelativo;    // Ancho de la vista principal.
            float VBAlto = 40;

            float reduccionDispositivo = 1f;

            // Se valida el dispositivo actual, iPhone o iPad.
            if (UIDevice.CurrentDevice.Model.Contains("iPhone"))
            {
                reduccionDispositivo = 0.95f;

                VBAlto = 30;
            }

            if (Orientacion == UIInterfaceOrientation.LandscapeLeft || Orientacion == UIInterfaceOrientation.LandscapeRight)
            {
                VPAlto = (float)frame.Height * ((tamanoRelativo * reduccionDispositivo) * 0.85f);    // Alto de la vista principal.
                VPAncho = (float)frame.Width * ((tamanoRelativo * reduccionDispositivo) * 1.05f);    // Ancho de la vista principal.
            }
            else
            {
                VPAlto = (float)frame.Height * (tamanoRelativo * reduccionDispositivo);    // Alto de la vista principal.
                VPAncho = (float)frame.Width * (tamanoRelativo * reduccionDispositivo);    // Ancho de la vista principal.
            }

            // Se establece la duraci�n de la animaci�n.
            TiempoAnimacionHide = animacionHide;

            // Se establece que la ventana PopUp debe cambiar de tama�o si el dispositivo cambia de orientaci�n.
            AutoresizingMask = UIViewAutoresizing.FlexibleDimensions;

            // Se inicializa la vista de fondo del PopUp.
            VistaFondo = new UIView(frame);
            VistaFondo.BackgroundColor = colorFondoPopUp;
            VistaFondo.Alpha = trasparenciaFondo;
            VistaFondo.AutoresizingMask = UIViewAutoresizing.FlexibleDimensions;

            // Se termina de configurar la vista principal del PopUp.
            VistaPrincipal = VistaMostrar;
            VistaPrincipal.Layer.BorderColor = colorBordeVistaPrincipal.CGColor;
            VistaPrincipal.Layer.BorderWidth = anchoBorde;
            VistaPrincipal.Layer.CornerRadius = RadioBordePrincipal;
            VistaPrincipal.BackgroundColor = colorVistaPopUp;
            VistaPrincipal.Alpha = 1;
            VistaPrincipal.AutoresizingMask = UIViewAutoresizing.FlexibleDimensions;

            // Se inicializa la vista sobre la que se pondr� el bot�n de cerrar la ventana PopUp.
            VistaBotonCerrar = new UIView(new RectangleF((float)frame.X + (VPAncho + (((float)frame.Width - VPAncho) / 2) - (VBAlto / 2)),
                (float)frame.Y + (((float)frame.Height - VPAlto) / 2) - (VBAlto / 2), VBAlto, VBAlto));
            VistaBotonCerrar.Layer.BorderColor = colorBordeVistaPrincipal.CGColor;
            VistaBotonCerrar.Layer.BorderWidth = anchoBorde;
            VistaBotonCerrar.Layer.CornerRadius = (VBAlto / 2);
            VistaBotonCerrar.BackgroundColor = colorVistaPopUp;
            VistaBotonCerrar.Alpha = 1;
            VistaBotonCerrar.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin;

            // Se inicializa el bot�n de cerrar la ventana PopUp.
            btn_CerrarVentana = new UIButton();
            btn_CerrarVentana.SetImage(UIImage.FromFile("btn_Cancelar.png"), UIControlState.Normal);
            btn_CerrarVentana.TintColor = UIColor.FromRGB(0, 122, 255);
            btn_CerrarVentana.SetTitleColor(UIColor.FromRGB(0, 122, 255), UIControlState.Normal);
            btn_CerrarVentana.TouchUpInside += btn_CerrarVentana_TouchUpInside;
            btn_CerrarVentana.Frame = new RectangleF((VBAlto / 2) - ((VBAlto - 10) / 2), 
                (VBAlto / 2) - ((VBAlto - 10) / 2), (VBAlto - 10), (VBAlto - 10));

            // Se agrega el bot�n a la vista apropiada.
            VistaBotonCerrar.AddSubview(btn_CerrarVentana);

            // Se agregan al PopUp todas las vistas necesarias en el orden que deben ser mostrados.
            AddSubview(VistaFondo);
            AddSubview(VistaPrincipal);
            AddSubview(VistaBotonCerrar);
        }

        /// <summary>
        /// M�todo para calcula las dimensiones que tendr�a la pantalla del PopUp.
        /// </summary>
        /// <param name="X">Variable a la cual asignar el valor X de la ventana.</param>
        /// <param name="Y">Variable a la cual asignar el valor Y de la ventana.</param>
        /// <param name="Width">Variable a la cual asignar el valor del ancho de la ventana.</param>
        /// <param name="Heigth">Variable a la cual asignar el valor de la altura de la ventana.</param>
        public static void ObtenerDimensiones(RectangleF frame, float tamano, ref float X, ref float Y, ref float Width, ref float Height, UIInterfaceOrientation Orientacion)
        {
            // Se valida el dispositivo actual, iPhone o iPad.
            if (UIDevice.CurrentDevice.Model.Contains("iPhone"))
            {
                tamano = (tamano * 0.95f);
            }

            if (Orientacion == UIInterfaceOrientation.LandscapeLeft || Orientacion == UIInterfaceOrientation.LandscapeRight)
            {
                Height = (float)frame.Height * (tamano * 0.85f);
                Width = (float)frame.Width * (tamano * 1.05f);
                X = (float)frame.X + (((float)frame.Width - Width) / 2);
                Y = (float)frame.Y + (((float)frame.Height - Height) / 2);
            }
            else
            {
                Height = (float)frame.Height * tamano;
                Width = (float)frame.Width * tamano;
                X = (float)frame.X + (((float)frame.Width - Width) / 2);
                Y = (float)frame.Y + (((float)frame.Height - Height) / 2);
            }
        }

        /// <summary>
        /// Desvanece el control y luego lo remueve de la super vista.
        /// </summary>
        public void Hide()
        {
            UIView.Animate(
                TiempoAnimacionHide, // Duraci�n.
                () => { Alpha = 0; },
                () => { RemoveFromSuperview(); }
            );
        }

        #endregion

        #region Eventos
        
        /// <summary>
        /// Evento de click del bot�n btn_CerrarVentana.
        /// Se usa para cerrar la ventana popup.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btn_CerrarVentana_TouchUpInside(object sender, EventArgs e)
        {
            this.Hide();
        }

        #endregion
    }
}