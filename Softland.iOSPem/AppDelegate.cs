﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SQLite;
using Pedidos.Core;
using BI.iOS;

using Foundation;
using UIKit;
using System.Threading.Tasks;
using BI.Shared;
using System.IO;
using System.Drawing;
using CoreLocation;

namespace Softland.iOS.Pem
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : UIApplicationDelegate
    {

        #region Propiedades
        
        /// <summary>
        /// Global que almacena una referencia al Storyboard de la app.
        /// </summary>
        public static UIStoryboard Storyboard = UIStoryboard.FromName("MainStoryboard", null);

        /// <summary>
        /// Global que almacena la direccion del servidor guardada en los NSUserDefaults.
        /// </summary>
        public static string Servidor = "";

        /// <summary>
        /// Global que almacena el nombre del archivo de la base de datos.
        /// </summary>
        public static string BaseDatos = "TPEL_DB.db";

        /// <summary>
        /// Variable a utilizar para hacer referencia cuando se ocupe validar o mostrar la versión de la aplicación.
        /// </summary>
        public const string Version = "7.0.104";

        /// <summary>
        /// Global que almacena la ruta del directorio de la app en la carpeta Libreria del dispositivo.
        /// </summary>
        public static string RutaSD = "";

        /// <summary>
        /// Global que almacena el UUID del dispositivo.
        /// </summary>
        public static string UUID = UIDevice.CurrentDevice.IdentifierForVendor.AsString();

        /// <summary>
        /// Global que almacena una referencia a la conexión que se hace con la base de datos.
        /// </summary>
        public static Connection cnx = null;

        /// <summary>
        /// Global que almacena una referencia a la conexión que se hace con la base de datos para la sincronizacion de pedidos.
        /// </summary>
        public static Connection cnxSincro = null;

        /// <summary>
        /// Global que almacena la compañía general guardada en los NSUserDefaults.
        /// </summary>
        public static string Compania = "";

        /// <summary>
        /// Global que almacena el consecutivo guardado en los NSUserDefaults.
        /// </summary>
        public static string Consecutivo = "";

        /// <summary>
        /// Global que almacena la dirección web de soporte para la aplicación según el cliente en los NSUserDefaults.
        /// </summary>
        public static string webSoporte = "";

        /// <summary>
        /// Global que almacena si se debe recordar la contraseña guardado en los NSUserDefaults.
        /// </summary>
        public static bool AlmacenarContrasena = true;

        /// <summary>
        /// Global que almacena si se puede o no cambiar los descuentos durante la gestión de un pedido en los NSUserDefaults.
        /// </summary>
        public static bool CambiarDescuento = true;

        /// <summary>
        /// Global que almacena si la aplicación acaba de entrar a primer plano.
        /// </summary>
        public static bool EntroAPrimerPlano = false;

        /// <summary>
        /// Global que almacena si se están sincronizando pedidos o no.
        /// </summary>
        public static bool SincronizandoPedidos = false;

        /// <summary>
        /// Global que almacena si se están sincronizando geolocalizaciones o no.
        /// </summary>
        public static bool SincronizandoGeoLocalizaciones = false;

        public override UIWindow Window
        {
            get;
            set;
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Método para iniciar la conexión con la base de datos.
        /// </summary>
        /// <param name="rutaCompleta">Ruta al archivo de la base de datos.</param>
        /// <returns>Retorna la conexión establecida con la base de datos.</returns>
        public static Connection crearConexion(string rutaCompleta)
        {
            // Si el archivo de la base de datos existe, permite la conexión.
            if (System.IO.File.Exists(rutaCompleta))
            {
                return new Connection(rutaCompleta);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Método para crear el directorio de la aplicación.
        /// </summary>
        /// <param name="rutaCompleta">Ruta al archivo de la base de datos.</param>
        /// <returns>Retorna la conexión establecida con la base de datos.</returns>
        public static void crearDirectorioApp(string rutaCompleta)
        {
            // Si el directorio de la aplicación no existe, lo crea.
            if (!Directory.Exists(rutaCompleta))
            {
                Directory.CreateDirectory(rutaCompleta);
            }
        }

        /// <summary>
        /// Método para borrar la base de datos del dispositivo.
        /// </summary>
        /// <param name="rutaCompleta">Ruta al archivo de la base de datos.</param>
        /// <returns>Retorna la conexión establecida con la base de datos.</returns>
        public static bool borrarBaseDatos()
        {
            try
            {
                // Se elimina la carpeta de la aplicación.
                Directory.Delete(RutaSD, true);

                // Limpia los defaults de la aplicación por si no se pueden borrar. 
                AppDelegate.SetConfigVar("", "compania");
                AppDelegate.SetConfigVar("", "servidor");
                AppDelegate.SetConfigVar("", "consecutivo");
                AppDelegate.SetConfigVar("", "websoporte");
                AppDelegate.SetConfigVar(true, "almacenarcontrasena");
                AppDelegate.SetConfigVar(false, "cambiardescuento");
                AppDelegate.SetConfigVar("", "usuario");
                AppDelegate.SetConfigVar("", "contrasena");
                
                // Se eliminan los defaults de la aplicación.
                var defaults = NSUserDefaults.StandardUserDefaults;
                defaults.RemoveObject("compania");
                defaults.RemoveObject("servidor");
                defaults.RemoveObject("consecutivo");
                defaults.RemoveObject("websoporte");
                defaults.RemoveObject("almacenarcontrasena");
                defaults.RemoveObject("cambiardescuento");
                defaults.RemoveObject("usuario");
                defaults.RemoveObject("contrasena");
                
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Método para obtener la ruta de al directorio Libreria y agregar un directorio específico para la app.
        /// </summary>
        /// <returns>Ruta del directorio establecido.</returns>
        public static string ObtenerRutaSD()
        {
            // Se obtiene una referencia NSFileManager del directorio Library del dispositivo.
            var directorio = NSFileManager.DefaultManager.GetUrls(NSSearchPathDirectory.LibraryDirectory, NSSearchPathDomain.User)[0];

            // Se combina la ruta de Libreria con la carpeta específica a la app.
            string appPath = Path.Combine(directorio.Path, "TPEL");

            // Se intenta crear el directorio de la aplicación en caso de que este no exista aún.
            AppDelegate.crearDirectorioApp(appPath);

            return appPath;
        }

        /// <summary>
        /// Método para asignar valores a diferentes variables en NSUserDefaults que utiliza la app.
        /// </summary>
        /// <param name="valor">Valor que almacenará en la variable.</param>
        /// <param name="variable">Variable registrada en NSUserDefaults.</param>
        public static void SetConfigVar(object valor, string variable)
        {
            var defaults = NSUserDefaults.StandardUserDefaults;
            
            if(valor.GetType()==typeof(string))
            {
                defaults.SetString((string)valor, variable);
            }
            else
            {
                defaults.SetBool((bool)valor, variable);
            }

            defaults.Synchronize();
        }

        /// <summary>
        /// Método asíncrono que se encarga de inicializar la sincronización de pedidos pendientes.
        /// Se ejecuta completo únicamente si no hay otro hilo donde se esté procesando ya una sincronización usando como
        /// validación la Global de AppDelegate.SincronizandoPedidos.
        /// </summary>
        /// <param name="context">El contexto de la pantalla que ejecutó el método.</param>
        public static void IniciarSincronizacionPedidos(UIViewController context)
        {
            context.InvokeOnMainThread(async () =>
            {
                UIColor verdeOscuro = UIColor.FromRGB(0, 200, 0);

                // Se registra un ID para la tarea a ejecutar y así poder tener una referencia a la misma (requerimiento iOS).
                nint taskID = UIApplication.SharedApplication.BeginBackgroundTask(() =>
                {
                    // Evento que se ejecuta cuando la tarea esta por agotar su tiempo límite, 10 minutos,
                    // se usa para guardar progreso y limpiar datos o memoria que este en uso antes que la tarea sea terminada.

                    double timeRemaining = UIApplication.SharedApplication.BackgroundTimeRemaining;

                    if (timeRemaining < 20)
                    {
                        Toast.MakeToast(context.View, "La tarea de sincronizacion va a expirar", null, UIColor.Black, true, enumDuracionToast.Mediano);
                    }
                });

                // Si no hay otro hilo sincronizando los pedidos actualmente se intenta ejecutar el proceso.
                if (!AppDelegate.SincronizandoPedidos)
                {
                    // Se muestra el indicador de actividad con la red.
                    UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;

                    AppDelegate.SincronizandoPedidos = true;

                    // Se llama al método que se encarga de realizar la sincronización.
                    string result = await AppDelegate.SincronizarPedidos();

                    // Se oculta el indicador de actividad con la red.
                    UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;

                    // Si el resultado no es nulo o vacio, quiere decir que se sincronizaron pedidos exitosamente.
                    if (!string.IsNullOrEmpty(result) && result.ToUpper().Contains("SE HAN SINCRONIZADO"))
                    {
                        Toast.MakeToast(context.View, result, null, verdeOscuro, true, enumDuracionToast.Mediano);
                    }

                    // Si se sincronizaron los pedidos pendientes y se está en la sección Pedidos, refrescar la lista de pedidos.
                    try
                    {
                        var vistatabs = (VistaTabs)context;

                        if (vistatabs.SelectedIndex == 2 && vistatabs.SelectedViewController.ChildViewControllers[0].GetType() == typeof(PedidosView))
                        {
                            var vistaPedidos = (PedidosView)vistatabs.SelectedViewController.ChildViewControllers[0];

                            vistaPedidos.CargarPedidosPendientes();
                        }
                    }
                    catch
                    {
                        // No hacer nada en especial si da algún problema al intentar realizar el procedimiento.
                    }

                    // Se notifica que éste método concluyó el proceso de sincronización.
                    AppDelegate.SincronizandoPedidos = false;

                }

                //Sincronización de Geolocalizaciones
                // Si no hay otro hilo sincronizando las geolocalizaciones actualmente se intenta ejecutar el proceso.
                if (!AppDelegate.SincronizandoGeoLocalizaciones)
                {
                    // Se muestra el indicador de actividad con la red.
                    UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;

                    AppDelegate.SincronizandoGeoLocalizaciones = true;

                    // Se llama al método que se encarga de realizar la sincronización.
                    string result = await AppDelegate.SincronizarGeolocalizaciones();

                    // Se oculta el indicador de actividad con la red.
                    UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;

                    // Si el resultado no es nulo o vacio, quiere decir que se sincronizaron pedidos exitosamente.
                    if (!string.IsNullOrEmpty(result) && result.ToUpper().Contains("SE HAN SINCRONIZADO"))
                    {
                        Toast.MakeToast(context.View, result, null, verdeOscuro, true, enumDuracionToast.Mediano);
                    }

                    // Se notifica que éste método concluyó el proceso de sincronización.
                    AppDelegate.SincronizandoGeoLocalizaciones = false;

                }

                // Se finaliza la tarea registrada con el ID para evitar un cierre de la app (requerimiento iOS).
                UIApplication.SharedApplication.EndBackgroundTask(taskID);
            });
        }

        /// <summary>
        /// Método asíncrono que se encarga de intentar sincronizar los pedidos pendientes con el servidor.
        /// </summary>
        /// <returns>Retorna un mensaje con el resultado de la operación.</returns>
        public static Task<string> SincronizarPedidos()
        {
            // Se retorna un Task para ejecutarlo de manera asíncrona.
            return Task.Run(() =>
            {
                try
                {
                    // Se obtiene el UUID (identificador único del dispositivo).
                    NSUuid identifier = UIDevice.CurrentDevice.IdentifierForVendor;
                    string uuid = identifier.AsString();

                    // Se crea una referencia para conectar con el servidor.
                    conexion con = new conexion(uuid, AppDelegate.Servidor, GestorDatos.NombreUsuario);

                    // se asigna una referencia a la conexión con la base de datos.
                    AppDelegate.cnxSincro = new Connection(System.IO.Path.Combine(AppDelegate.RutaSD, AppDelegate.BaseDatos));

                    string error = string.Empty;

                    int cantidadPedidos = 0;
                    Sentencias sentencias = new Sentencias();

                    // Se obtienen los pedidos pendientes en forma de sentencias para enviar al servidor.
                    GestorPedido.CrearSentenciasPedidos(AppDelegate.cnxSincro,ref sentencias, ref cantidadPedidos);

                    if (sentencias.listaSentencias.Count > 0)
                    {
                        // Lista de pedidos que serán devueltos por el servidor para evaluar su estado.
                        List<PedidosSincronizados> pedidosSinc = new List<PedidosSincronizados>();

                        // Se envian las sentencias y se evalua si el proceso termino correctamente.
                        if (con.EnviarPedidos(ref error, sentencias, ref pedidosSinc))
                        {
                            error = "";

                            if (pedidosSinc.Where(x => x.sincronizado).ToList().Count > 0)
                            {
                                if (cantidadPedidos == pedidosSinc.Where(x=>x.sincronizado).ToList().Count)
                                {
                                    error = "Se han sincronizado todos los pedidos pendientes.";
                                }
                                else
                                {                                    
                                    error = string.Concat("Se han sincronizado ", pedidosSinc.Where(x => x.sincronizado).ToList().Count, " de ", cantidadPedidos, " pedidos pendientes.");
                                }
                            }

                            // Se actualizan los pedidos sincronizados, exitosos o no.
                            Pedido.ActualizarSincronizados(pedidosSinc, AppDelegate.cnxSincro);
                        }
                    }

                    // Se cierra la conexión extra para sincronización y se limpia la referencia.
                    if (AppDelegate.cnxSincro != null)
                    {
                        if (AppDelegate.cnxSincro.isOpen)
                            AppDelegate.cnxSincro.Close();
                        AppDelegate.cnxSincro = null;
                    }

                    return error;
                }
                catch(Exception ex)
                {
                    // Se cierra la conexión extra para sincronización y se limpia la referencia.
                    if (AppDelegate.cnxSincro != null)
                    {
                        if (AppDelegate.cnxSincro.isOpen)
                            AppDelegate.cnxSincro.Close();
                        AppDelegate.cnxSincro = null;
                    }

                    return "Ocurrió un error al enviar los pedidos pendientes, inténtelo nuevamente.";
                }
            });
        }

        /// <summary>
        /// Método asíncrono que se encarga de intentar sincronizar las geolocalizaciones de los clientes pendientes con el servidor.
        /// </summary>
        /// <returns>Retorna un mensaje con el resultado de la operación.</returns>
        public static Task<string> SincronizarGeolocalizaciones()
        {
            // Se retorna un Task para ejecutarlo de manera asíncrona.
            return Task.Run(() =>
            {
                try
                {
                    // Se obtiene el UUID (identificador único del dispositivo).
                    NSUuid identifier = UIDevice.CurrentDevice.IdentifierForVendor;
                    string uuid = identifier.AsString();

                    // Se crea una referencia para conectar con el servidor.
                    conexion con = new conexion(uuid, AppDelegate.Servidor, GestorDatos.NombreUsuario);

                    // se asigna una referencia a la conexión con la base de datos.
                    AppDelegate.cnxSincro = new Connection(System.IO.Path.Combine(AppDelegate.RutaSD, AppDelegate.BaseDatos));

                    string error = string.Empty;

                    // Se obtienen las geolocalizaciones pendientes para enviar al servidor.
                    var listaPorSync = Cliente.CargarListaGeolocalizaciones(AppDelegate.cnxSincro);

                    if (listaPorSync.Count > 0)
                    {
                        // Lista de clientesGeo que no se sincronizaron y serán devueltos por el servidor para evaluar su estado.
                        List<ClienteGeoLocaizacion> clienteGeoNoSinc = new List<ClienteGeoLocaizacion>();
                        var formulario = new FormularioGeoLocalizacion();
                        formulario.Dispositivo = uuid;
                        formulario.Usuario = GestorDatos.NombreUsuario;
                        formulario.Origen = "Pem";
                        formulario.GeoLocalizaciones = listaPorSync;

                        // Se envian las sentencias y se evalua si el proceso termino correctamente.
                        if (con.EnviarGeolocalizaciones(ref error, clienteGeoNoSinc, formulario))
                        {
                            error = "";
                            var ClientesGeoPorActualizar = listaPorSync.FindAll(x => clienteGeoNoSinc.Find(y => y.Cliente.Equals(x.Cliente) && y.Compania.Equals(y.Compania)) == null );

                            if (ClientesGeoPorActualizar.Count > 0)
                            {
                                Cliente.ActualizarGeolocalizaionesSinc(ClientesGeoPorActualizar, AppDelegate.cnxSincro);
                            }
                        }
                    }

                    // Se cierra la conexión extra para sincronización y se limpia la referencia.
                    if (AppDelegate.cnxSincro != null)
                    {
                        if (AppDelegate.cnxSincro.isOpen)
                            AppDelegate.cnxSincro.Close();
                        AppDelegate.cnxSincro = null;
                    }

                    return error;
                }
                catch (Exception ex)
                {
                    // Se cierra la conexión extra para sincronización y se limpia la referencia.
                    if (AppDelegate.cnxSincro != null)
                    {
                        if (AppDelegate.cnxSincro.isOpen)
                            AppDelegate.cnxSincro.Close();
                        AppDelegate.cnxSincro = null;
                    }

                    return "Ocurrió un error al enviar los pedidos pendientes, inténtelo nuevamente.";
                }
            });
        }

        #endregion

        #region View lifecycle

        //
        // This method is invoked when the application is about to move from active to inactive state.
        //
        // OpenGL applications should use this method to pause.
        //
        public override void OnResignActivation(UIApplication application)
        {
        }

        // This method should be used to release shared resources and it should store the application state.
        // If your application supports background exection this method is called instead of WillTerminate
        // when the user quits.
        public override void DidEnterBackground(UIApplication application)
        {
            EntroAPrimerPlano = true;

            // Se verifica si el servicio de localización está disponible.
            //if (CLLocationManager.LocationServicesEnabled)
            //{
            //    if (MapaView.locationManager != null)
            //    {
            //        MapaView.locationManager.StopUpdatingLocation();
            //    }
            //}
        }

        // This method is called as part of the transiton from background to active state.
        public override void WillEnterForeground(UIApplication application)
        {
        }

        // This method is called when the application is about to terminate. Save data, if needed. 
        public override void WillTerminate(UIApplication application)
        {
            SetConfigVar(true, "cerrado");
        }

        #endregion
    }
}