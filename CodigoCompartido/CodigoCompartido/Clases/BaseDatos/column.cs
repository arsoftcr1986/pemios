﻿using System;

namespace BI.Shared
{
	public class column
	{
		public string Name {
			get;
			set;
		}
		public enumTypesColumn type {
			get;
			set;
		}
		public bool Autoincrement {
			get;
			set;
		}
		public bool allowNulls {
			get;
			set;
		}
		public bool isPrimary {
			get;
			set;
		}
		public object defaultValue {
			get;
			set;
		}
		public enumTypesConflict onConflict {
			get;
			set;
		}
		public enumTypesRelationship onUpdate {
			get;
			set;
		}
		public enumTypesRelationship onDelete {
			get;
			set;
		}
		public enumTypesMatch onMatch {
			get;
			set;
		}
		public string relationName {
			get;
			set;
		}
		public string tableName {
			get;
			set;
		}
		public string columnRelation {
			get;
			set;
		}
		public int sizeColumn {
			get;
			set;
		}
		public int numDecimals {
			get;
			set;
		}
		/// <summary>
		/// Construye una columna primaria.
		/// Nota: El parámetro AUTOINCREMENT solo acepta el tipo de datos INTEGER
		/// </summary>
		/// <param name="pName">Nombre de la columna</param>
		/// <param name="typeColumn">Tipo de la calumna a agregar</param>
		/// <param name="pAutoincrement">Si el valor es autoincremental</param>
		/// <param name="pallowNulls">Si permite nulos</param>
		/// <param name="pdefaultValue">El valor por defecto</param>
		/// <param name="Conflict">Acción en caso de error</param>
		public column (string pName, enumTypesColumn typeColumn, bool pAutoincrement, 
			bool pallowNulls, object pdefaultValue, enumTypesConflict Conflict)
		{
			Name = pName;
			type = typeColumn;
			Autoincrement = pAutoincrement;
			allowNulls = pallowNulls;
			isPrimary = true;
			defaultValue = pdefaultValue;
			onConflict = Conflict;
			onUpdate = enumTypesRelationship.NULL;
			onDelete = enumTypesRelationship.NULL;
			onMatch = enumTypesMatch.NULL;
		}
		/// <summary>
		/// Construye una columna sencilla
		/// Nota: El parámetro AUTOINCREMENT solo acepta el tipo de datos INTEGER
		/// </summary>
		/// <param name="pName">Nombre de la columna</param>
		/// <param name="typeColumn">Tipo de la calumna a agregar</param>
		/// <param name="pAutoincrement">Si el valor es autoincremental</param>
		/// <param name="pallowNulls">Si permite nulos</param>
		/// <param name="pdefaultValue">El valor por defecto</param>
		public column (string pName, enumTypesColumn typeColumn, bool pAutoincrement, 
						bool pallowNulls, object pdefaultValue)
		{
			Name = pName;
			type = typeColumn;
			Autoincrement = pAutoincrement;
			allowNulls = pallowNulls;
			isPrimary = false;
			defaultValue = pdefaultValue;
			onConflict = enumTypesConflict.NULL;
			onUpdate = enumTypesRelationship.NULL;
			onDelete = enumTypesRelationship.NULL;
			onMatch = enumTypesMatch.NULL;
		}
		/// <summary>
		/// Construye una columna sencilla
		/// Nota: El parámetro AUTOINCREMENT solo acepta el tipo de datos INTEGER
		/// </summary>
		/// <param name="pName">Nombre de la columna</param>
		/// <param name="typeColumn">Tipo de la calumna a agregar</param>
		/// <param name="pAutoincrement">Si el valor es autoincremental</param>
		/// <param name="pallowNulls">Si permite nulos</param>
		/// <param name="pdefaultValue">El valor por defecto</param>
		/// <param name="psizeColumn">Tamaño de la columna</param>
		public column (string pName, enumTypesColumn typeColumn, bool pAutoincrement, 
			bool pallowNulls, object pdefaultValue, int psizeColumn)
		{
			Name = pName;
			type = pAutoincrement ? enumTypesColumn.INTEGER : typeColumn;
			Autoincrement = pAutoincrement;
			allowNulls = pallowNulls;
			isPrimary = false;
			defaultValue = pdefaultValue;
			sizeColumn = psizeColumn;
			onConflict = enumTypesConflict.NULL;
			onUpdate = enumTypesRelationship.NULL;
			onDelete = enumTypesRelationship.NULL;
			onMatch = enumTypesMatch.NULL;
		}
		/// <summary>
		/// Construye una columna sencilla de tipo NUMERIC
		/// Nota: El parámetro AUTOINCREMENT solo acepta el tipo de datos INTEGER
		/// </summary>
		/// <param name="pName">Nombre de la columna</param>
		/// <param name="typeColumn">Tipo de la calumna a agregar</param>
		/// <param name="pAutoincrement">Si el valor es autoincremental</param>
		/// <param name="pallowNulls">Si permite nulos</param>
		/// <param name="pdefaultValue">El valor por defecto</param>
		/// <param name="psizeColumn">Tamaño de la columna</param>
		/// <param name="decimals">Número de decimales de la columna</param>
		public column (string pName, 
			bool pallowNulls, object pdefaultValue, int psizeColumn, int decimals)
		{
			Name = pName;
			type = enumTypesColumn.NUMERIC;
			Autoincrement = false;
			allowNulls = pallowNulls;
			isPrimary = false;
			defaultValue = pdefaultValue;
			sizeColumn = psizeColumn;
			numDecimals = decimals;
			onConflict = enumTypesConflict.NULL;
			onUpdate = enumTypesRelationship.NULL;
			onDelete = enumTypesRelationship.NULL;
			onMatch = enumTypesMatch.NULL;
		}
		/// <summary>
		/// Construye una columna foránea.
		/// </summary>
		/// <param name="pName">Nombre de la columna</param>
		/// <param name="typeColumn">Tipo de la calumna a agregar</param>
		/// <param name="pAutoincrement">Si el valor es autoincremental</param>
		/// <param name="pallowNulls">Si permite nulos</param>
		/// <param name="pdefaultValue">El valor por defecto</param>
		/// <param name="Conflict">Acción en caso de error</param>
		/// <param name="Update">Acción al actualizar</param>
		/// <param name="Delete">Acción al eliminar</param>
		/// <param name="Match">Acción en coincidencia</param>
		/// <param name="pRelationName">Nombre de la relación</param>
		/// <param name="ptableRelation">Tabla de la relación</param>
		/// <param name="pcolumnRelation">Columna de a relación</param>
		public column (string pName, enumTypesColumn typeColumn,
			bool pallowNulls, object pdefaultValue, enumTypesConflict Conflict,
			enumTypesRelationship Update, enumTypesRelationship Delete, enumTypesMatch Match,
			string pRelationName, string ptableRelation, string pcolumnRelation)
		{
			Name = pName;
			type = typeColumn;
			Autoincrement = false;
			allowNulls = pallowNulls;
			isPrimary = false;
			defaultValue = pdefaultValue;
			onConflict = Conflict;
			onUpdate = Update;
			onDelete = Delete;
			onMatch = Match;
			relationName = pRelationName;
			tableName = ptableRelation;
			columnRelation = pcolumnRelation;
		}
		/// <summary>
		/// Construye una columna sencilla con acciones en edición y eliminación.
		/// </summary>
		/// <param name="pName">Nombre de la columna</param>
		/// <param name="typeColumn">Tipo de la calumna a agregar</param>
		/// <param name="pAutoincrement">Si el valor es autoincremental</param>
		/// <param name="pallowNulls">Si permite nulos</param>
		/// <param name="pdefaultValue">El valor por defecto</param>
		/// <param name="Conflict">Acción en caso de error</param>
		/// <param name="Update">Acción al actualizar</param>
		/// <param name="Delete">Acción al eliminar</param>
		/// <param name="psizeColumn">Tamaño de la columna</param>
		/// <param name="pDecimal">número de decimales que acepta la columna. Nota: solo para tipo de datos NUMERIC</param>
		public column (string pName, enumTypesColumn typeColumn, bool pAutoincrement, 
			bool pallowNulls, object pdefaultValue, 
			enumTypesRelationship Update, enumTypesRelationship Delete, int psizeColumn, int pDecimal)
		{
			Name = pName;
			type = pAutoincrement ? enumTypesColumn.INTEGER : typeColumn;
			Autoincrement = pAutoincrement;
			allowNulls = pallowNulls;
			isPrimary = false;
			sizeColumn = psizeColumn;
			numDecimals = type == enumTypesColumn.NUMERIC ? pDecimal : 0;
			defaultValue = pdefaultValue;
			onConflict = enumTypesConflict.NULL;
			onUpdate = Update;
			onDelete = Delete;
			onMatch = enumTypesMatch.NULL;
		}
	}
}

