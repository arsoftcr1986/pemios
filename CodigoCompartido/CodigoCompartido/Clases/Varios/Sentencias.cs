using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BI.Shared
{
    public class Sentencias
    {
        public static List<string> cias=new List<string>();

        public static string alFacPAIS
        {
            get
            {
                string result=string.Empty;
                string sentencia = "SELECT PAIS,'{0}' AS CIA,NOMBRE,ETIQUETA_DIV_GEO1,ETIQUETA_DIV_GEO2 FROM {0}.PAIS ";
                int cont=0;
                foreach (string str in cias)
                {
                    if (cont > 0)
                    {
                        result += " UNION ALL ";
                    }
                    result += string.Format(sentencia, str);
                    cont++;
                }
                return result;
            }
        }
    }
}