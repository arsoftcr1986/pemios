using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace BI.Shared
{
    public static class FileTools
    {
        public static string ReadFileString(string path)
        {
            // Use StreamReader to consume the entire text file.
            using (StreamReader reader = new StreamReader(path))
            {
                return reader.ReadToEnd();
            }
        }
    }
}