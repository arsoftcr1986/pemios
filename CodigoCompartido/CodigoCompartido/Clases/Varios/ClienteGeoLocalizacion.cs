﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BI.Shared
{
    public class ClienteGeoLocaizacion
    {
        public string Compania { set; get; }
        public string Cliente { set; get; }
        public decimal Latitud { set; get; }
        public decimal Longitud { set; get; }
    }
}
