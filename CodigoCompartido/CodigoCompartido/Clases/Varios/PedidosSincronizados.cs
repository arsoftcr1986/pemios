﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BI.Shared
{
    public class PedidosSincronizados
    {
        public string compania { get; set; }
        public string pedido { get; set; }
        public bool sincronizado { get; set; }
        public string error { get; set; }
        public string consecutivoERP { get; set; }
    }
}
