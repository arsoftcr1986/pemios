﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BI.Shared
{
    public class FormularioGeoLocalizacion
    {
        public string Dispositivo { set; get; }
        public string Usuario { set; get; }
        public List<ClienteGeoLocaizacion> GeoLocalizaciones { set; get; }
        public string Origen { set; get; }
    }
}
