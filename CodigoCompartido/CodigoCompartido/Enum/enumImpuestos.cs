﻿using System;

namespace BI.Shared
{
    public enum FormaCalcImpuesto1
    {
        /// <summary>
        /// Imp1 = suma(imp1 * (cant * precio - (desc1 + desc2)))
        /// </summary>
        Total,

        /// <summary>
        /// Imp1 = suma(imp1 * (cant * precio - descLinea))
        /// </summary>
        Lineas,

        /// <summary>
        /// Imp1 = suma(imp1 * (cant * precio - (desc1 + desc2 + descLinea)))
        /// </summary>
        Ambas,

        /// <summary>
        /// Imp1 = suma(imp1 * cant * precio)
        /// </summary>
        Ninguno,

        NoDefinido
    }

    public enum Impuesto1Incluido
    {

        /// <summary>
        /// Los precios incluye impuesto 1
        /// </summary>
        SI = 'S',

        /// <summary>
        /// Los precios no incluyen impuesto 1
        /// </summary>
        NO = 'N'
    }
}

