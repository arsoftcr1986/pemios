﻿using System;
using System.ComponentModel;

namespace BI.Shared
{
    public enum Rdl
    {
        //Resumenes
        ResumenFacturas,
        ResumenPedidos,
        ResumenDevoluciones,
        DetalleDevolucion,
        DetalleDevolucionRes,
        ResumenRecibos,

        //Detalles
        Factura,
        FacturaRes,
        Garantia,
        Pedido,
        VentaConsignacion,
        DetalleRecibos,

        ReporteVisita,
        ReporteMontos,
        ReporteCierre,
        ReporteInventario,
        ReporteDevoluciones,
        ReporteJornada,
        ReporteCierreCaja,
        ReporteLiquidacionInventario,
        ReporteInventarioTomaFisica
    }
}

