﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BI.Shared
{
    public enum enumDuracionToast
    {
        Corto = 0,
        Mediano = 1,
        Largo = 2,
        MuyLargo = 3
    }
}
