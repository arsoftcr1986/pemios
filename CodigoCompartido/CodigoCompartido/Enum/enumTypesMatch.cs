﻿using System;

namespace BI.Shared
{
	public enum enumTypesMatch
	{
		NONE = 0,
		PARTIAL = 1,
		FULL = 2,
		NULL = 3
	}
}

