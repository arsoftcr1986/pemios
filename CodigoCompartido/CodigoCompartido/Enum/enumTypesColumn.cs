﻿using System;

namespace BI.Shared
{
	public enum enumTypesColumn
	{
		NULL = 0,
		INTEGER = 1,
		REAL = 2,
		TEXT = 3,
		BLOB = 4,
		BOOLEAN = 5,
		DATETIME = 6,
		INT = 7,
		NUMERIC = 8,
		VARCHAR = 9,
		DATE = 10
	}
}

