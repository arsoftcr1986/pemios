﻿using System.ComponentModel;

namespace BI.Shared
{
    public enum TipoCobro
    {
        [Description("Selección Documentos")]
        SeleccionFacturas,
        [Description("Recibo Sin Aplicación")]
        Recibo,
        [Description("Monto Total")]
        MontoTotal,
        FacturaActual, // Agregado KFC Facturas Contado
        Ninguno
    }
}
