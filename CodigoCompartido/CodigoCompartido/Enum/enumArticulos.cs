﻿using System;
using System.ComponentModel;

namespace BI.Shared
{
    /// <summary>
    /// Filtros de busqueda para articulos
    /// </summary>
    public enum FiltroArticulo
    {
        Existencia,
        NivelPrecio,
        GrupoArticulo
    }

    /// <summary>
    /// Posibles criterios de busqueda de un articulo
    /// </summary>
    public enum CriterioArticulo
    {
        [Description("Código")]
        Codigo,
        [Description("Código Barras")]
        Barras,
        [Description("Descripción")]
        Descripcion,
        [Description("Clase")]
        Clase,
        [Description("Familia")]
        Familia,
        [Description("Ninguno")]
        Ninguno,
        [Description("Pedido Actual")]
        PedidoActual,
        [Description("Factura Actual")]
        FacturaActual,
        [Description("Venta Actual")]
        VentaActual,
        [Description("Boleta Actual")]
        BoletaActual
    }
}

