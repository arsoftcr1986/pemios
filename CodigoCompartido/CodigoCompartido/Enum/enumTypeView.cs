﻿using System;

namespace BI.Shared
{
	public enum enumTypeView
	{
		String = 0,
		Float = 1,
		Long = 2,
		Double = 3,
		Int = 4,
		Short = 5,
		Blob = 6
	}
}

