﻿using System;
using System.ComponentModel;

namespace BI.Shared
{
    public enum TipoDocumento
    {
        /// <summary>
        /// Factura de Contado
        /// </summary>
        FacturaContado = 0,

        /// <summary>
        /// Factura
        /// </summary>
        Factura = 1,

        /// <summary>
        /// Nota de Debito
        /// </summary>
        NotaDebito = 2,

        /// <summary>
        /// Nota de Credito Aux = 3
        /// </summary>
        /// <remarks>
        /// Solamente se usa en FRm para identificar los movimientos de N/C a N/C.
        /// O sea, ver como quedaron los saldos de las N/C luego de ser 
        /// aplicarlas a las facturas en un recibo de cobro.
        /// </remarks>
        NotaCreditoAux = 3,

        /// <summary>
        /// Recibo = 5
        /// </summary>
        Recibo = 5,

        /// <summary>
        /// NotaCredito = 7
        /// </summary>
        NotaCredito = 7,

        /// <summary>
        /// NotaCredito = 9.
        /// Se utiliza cuando se quiere crear una nueva nota de crédito desde la pocket
        /// </summary>
        NotaCreditoCrear = 9,

        /// <summary>
        /// Letra Cambio       
        /// </summary>
        LetraCambio = 10,

        /// <summary>
        /// Otro Debito       
        /// </summary>
        OtroDebito = 11,

        /// <summary>
        /// Intereses      
        /// </summary>
        Intereses = 12,

        /// <summary>
        /// Boleta de Venta       
        /// </summary>
        BoletaVenta = 13,

        /// <summary>
        /// Interes Corriente       
        /// </summary>
        InteresCorriente = 14,

        /// <summary>
        /// Nigun documento        
        /// </summary>
        TodosDocumentosDebito = -1,

        /// <summary>
        /// Nota Credito generada en caliente NO APLICADA       
        /// </summary>
        NotaCreditoNueva = 15,

        //Proyecto Gas Z

        Garantia = 15,
        NotaCreditoNuevaGarantia = 17,

        /// <summary>
        /// ReciboSinAplicacion = 18
        /// </summary>
        ReciboSinAplicacion = 18
    }
}

