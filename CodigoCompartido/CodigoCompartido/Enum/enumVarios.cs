﻿using System;

namespace BI.Shared
{

    public enum TipoMoneda
    {
        /// <summary>
        /// LOCAL = 'L'
        /// </summary>
        LOCAL = 'L',

        /// <summary>
        /// DOLAR = 'D'
        /// </summary>
        DOLAR = 'D'
    }
    public enum FijarColumna
    {
        /// <summary>
        /// No fija ninguna columna. = 'N'
        /// </summary>
        Ninguno = 'N',

        /// <summary>
        /// Fija la columna de Codigo. = 'C'
        /// </summary>
        Codigo = 'C',
        /// <summary>
        /// Fija la columna de Descripcion. = 'D'
        /// </summary>
        Descripcion = 'D'
    }

    public enum PedidosLabels
    {
        Cantidad,
        Descuentos,
        Bonificacion,
        Adicional,
        PrecioAlmacen,
        PrecioDetalle,
        SubTotal,
        Total
    }

    public enum TypePrinter
    {
        Generica
    }

    public enum Consecutivo
    {
        Factura,
        Garantia,
        Inventario,
        Devolucion,
        PedidoDescuento,
        Recibo,
        Pedido,
        NotaCredito
    }

    public enum ColumnasPedido
    {
        COD,
        DESC,
        EMP,
        PREA,
        PRED,
        CANTD,
        CANTA
    }

    public enum EstadoDocumento
    {
        /// <summary>
        /// Activo = 'A'
        /// </summary>
        Activo = 'A',

        /// <summary>
        /// Anulado = 'N'
        /// </summary>
        Anulado = 'N',

        /// <summary> KFC
        /// Contado = 'C'
        /// </summary>
        Contado = 'C'


    }

    public enum Dias
    {
        L = 'L', K = 'K', M = 'M', J = 'J', V = 'V', S = 'S', D = 'D', T
    }

    public enum EstadoPedido
    {
        Aprobado = 'A',
        Facturado = 'F',
        Normal = 'N',
        Backorder = 'B',
        Cancelado = 'C'
    }

    public enum TipoDescuento
    {
        /// <summary>
        /// Porcentual = 'P'. Indica que el descuento es por porcentaje.
        /// </summary>
        Porcentual = 'P',

        /// <summary>
        /// Fijo = 'F'. Indica que el descuento es por un monto fijo.
        /// </summary>
        Fijo = 'F'
    }

    public enum TipoPedido
    {
        Prefactura = '1',
        Factura = 'F'
    }

    public enum EDescuento
    {
        DESC1 = 1,
        DESC2 = 2
    }

    public enum ClaseDoc
    {
        Normal = 'N',
        CreditoFiscal = 'C'
    }
}

